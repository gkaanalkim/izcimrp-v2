package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Customer;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 14/08/15.
 */
public class CustomerDAO extends BaseDAO<Customer> {
    public CustomerDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Customer> getByContains(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.like("name", keyword, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<Customer> getByContainsInTitle(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.like("name", keyword, MatchMode.ANYWHERE)).addOrder(Order.asc("name")).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<Customer> getByRepresenative(String representative, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.like("representative", representative, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));


        return list(criteria);
    }

    public Customer getByName(String name, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.eq("name", name)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));


        return uniqueResult(criteria);
    }

    public  List<Customer> getByAddressContains(String address, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.like("address", address, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));


        return list(criteria);
    }

    public List<Customer> getBySectorContains(String sector, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.like("sector", sector, MatchMode.ANYWHERE))
                .add(Restrictions.eq("company.oid", company.getOid()));


        return list(criteria);
    }

    public List<Customer> getAll(Class<Customer> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", Boolean.FALSE)).addOrder(Order.asc("name"))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Customer getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid",oid))
                .add(Restrictions.eq("company.oid", company.getOid()));


        return uniqueResult(criteria);
    }

    @Override
    public Customer delete(Customer entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }
    public Customer getByPhone(String phone, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("phone", phone))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }
    public Customer getByEmail(String email, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("email", email))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }

    public Customer getByTaxNo(String taxNo, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("taxNo", taxNo))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }
    public Customer getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Customer.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }


}
