package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Permission;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
public class PermissionDAO extends BaseDAO<Permission> {
    private String adminUpdateMenu = "CompanyUpdate";
    private String adminDeleteMenu = "CompanyDelete";
    private String adminCreateMenu = "CompanyCreate";

    public PermissionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Permission> getAll(String project, Class<Permission> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
        .add(Restrictions.eq("projectCode", project));

        return list(criteria);
    }

    public Permission getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Permission.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }
    public Permission getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Permission.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("code", code));

        return uniqueResult(criteria);
    }
    public List<Permission> getByCodeContains(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Permission.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.like("code", code, MatchMode.ANYWHERE));

        return list(criteria);
    }

    @Override
    public Permission delete(Permission entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public List<Permission> getAllExceptAdminMenus(String project, Class<Permission> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.ne("code", this.adminUpdateMenu))
                .add(Restrictions.ne("code", this.adminDeleteMenu))
                .add(Restrictions.eq("projectCode", project))
                .add(Restrictions.ne("code", this.adminCreateMenu));

        return list(criteria);
    }
}
