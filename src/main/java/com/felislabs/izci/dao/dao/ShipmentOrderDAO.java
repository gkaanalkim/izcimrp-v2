package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ShipmentOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.util.List;

public class ShipmentOrderDAO extends BaseDAO<ShipmentOrder> {
    public ShipmentOrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public ShipmentOrder getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ShipmentOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ShipmentOrder> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ShipmentOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ShipmentOrder> getByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(ShipmentOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.between("creationDateTime", start, end))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
