package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.AccountingCustomer;
import com.felislabs.izci.representation.entities.AccountingCustomerAddress;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/03/2017.
 */
public class AccountingCustomerAddressDAO extends BaseDAO<AccountingCustomerAddress> {
    public AccountingCustomerAddressDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<AccountingCustomerAddress> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(AccountingCustomerAddress.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<AccountingCustomerAddress> getByCustomer(AccountingCustomer accountingCustomer, Company company) {
        Criteria criteria = currentSession().createCriteria(AccountingCustomerAddress.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomer.oid", accountingCustomer.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public AccountingCustomerAddress getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(AccountingCustomerAddress.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
