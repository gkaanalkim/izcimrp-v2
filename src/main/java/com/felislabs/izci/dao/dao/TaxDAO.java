package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Tax;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 08/03/2017.
 */
public class TaxDAO extends BaseDAO<Tax> {
    public TaxDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Tax getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Tax.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid",oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Tax> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Tax.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
