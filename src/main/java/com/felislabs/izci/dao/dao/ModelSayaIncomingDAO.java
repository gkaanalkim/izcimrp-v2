package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ModelOrder;
import com.felislabs.izci.representation.entities.ModelSaya;
import com.felislabs.izci.representation.entities.ModelSayaIncoming;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import javax.persistence.criteria.Order;
import java.util.List;

/**
 * Created by gunerkaanalkim on 02/02/2017.
 */
public class ModelSayaIncomingDAO extends BaseDAO<ModelSayaIncoming> {
    public ModelSayaIncomingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelSayaIncoming> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSayaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(org.hibernate.criterion.Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<ModelSayaIncoming> getBySaya(ModelSaya modelSaya, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSayaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelSaya.oid", modelSaya.getOid()));

        return list(criteria);
    }

    public List<ModelSayaIncoming> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSayaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()));

        return list(criteria);
    }

    public ModelSayaIncoming getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSayaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }
}
