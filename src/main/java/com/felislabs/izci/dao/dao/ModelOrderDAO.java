package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Customer;
import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by gunerkaanalkim on 29/01/2017.
 */
public class ModelOrderDAO extends BaseDAO<ModelOrder> {
    public ModelOrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelOrder> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.desc("creationDateTime"));
        return list(criteria);
    }
    public ModelOrder getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public ModelOrder getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public Integer getOrderNumber(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        int count = ((Number) criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .setProjection(Projections.rowCount()).uniqueResult()).intValue();

        return count;
    }

    public List<ModelOrder> getByCustomer(Customer customer,Company company) {
        Criteria criteria = currentSession().createCriteria(ModelOrder.class);
        criteria.createAlias("model", "m");
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("m.customer",customer))
        .addOrder(Order.asc("m.modelCode"));
        return list(criteria);
    }

    public List<ModelOrder> getByContainsInCode(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelOrder.class);
        criteria.createAlias("model", "m");
        criteria.add(Restrictions.like("m.modelCode", keyword, MatchMode.ANYWHERE))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
