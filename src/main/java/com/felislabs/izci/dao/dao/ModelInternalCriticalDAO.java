package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelInternalCritical;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 25/01/2017.
 */
public class ModelInternalCriticalDAO extends BaseDAO<ModelInternalCritical> {
    public ModelInternalCriticalDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelInternalCritical> getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelInternalCritical.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }
}
