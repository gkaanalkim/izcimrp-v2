package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ExtraExpenses;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by dogukanyilmaz on 25/04/2017.
 */
public class ExtraExpensesDAO extends BaseDAO<ExtraExpenses> {
    public ExtraExpensesDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ExtraExpenses> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ExtraExpenses.class);
        criteria.add(Restrictions.eq("deleteStatus", Boolean.FALSE)).addOrder(Order.asc("title"))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ExtraExpenses getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ExtraExpenses.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public ExtraExpenses getByTitle(String title, Company company) {
        Criteria criteria = currentSession().createCriteria(ExtraExpenses.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("title", title))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }

}
