package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.FairOrder;
import com.felislabs.izci.representation.entities.FairOrderAssort;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 23/03/2017.
 */
public class FairOrderAssortDAO extends BaseDAO<FairOrderAssort> {
    public FairOrderAssortDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<FairOrderAssort> getByFairOrder(FairOrder fairOrder) {
        Criteria criteria = currentSession().createCriteria(FairOrderAssort.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("fairOrder.oid", fairOrder.getOid()));

        return list(criteria);
    }
}
