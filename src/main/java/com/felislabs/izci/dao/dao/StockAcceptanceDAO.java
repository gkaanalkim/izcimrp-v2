package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.StockAcceptance;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by dogukanyilmaz on 16/02/2017.
 */
public class StockAcceptanceDAO extends BaseDAO<StockAcceptance> {

    public StockAcceptanceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<StockAcceptance> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(StockAcceptance.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public StockAcceptance getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(StockAcceptance.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
