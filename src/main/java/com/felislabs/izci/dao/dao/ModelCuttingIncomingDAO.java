package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ModelCuttingIncoming;
import com.felislabs.izci.representation.entities.ModelCuttingOrder;
import com.felislabs.izci.representation.entities.ModelOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 09/02/2017.
 */
public class ModelCuttingIncomingDAO extends BaseDAO<ModelCuttingIncoming> {
    public ModelCuttingIncomingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelCuttingIncoming> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(org.hibernate.criterion.Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<ModelCuttingIncoming> getBySaya(ModelCuttingOrder modelCuttingOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelCuttingOrder.oid", modelCuttingOrder.getOid()));

        return list(criteria);
    }

    public List<ModelCuttingIncoming> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()));

        return list(criteria);
    }

    public ModelCuttingIncoming getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid",oid));

        return uniqueResult(criteria);
    }
    public List<ModelCuttingIncoming> getByModelCuttingOrder(ModelCuttingOrder modelCuttingOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelCuttingOrder.oid", modelCuttingOrder.getOid()));
        return list(criteria);
    }
}
