package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.InternalCritical;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by dogukanyilmaz on 06/12/2016.
 */
public class InternalCriticalDAO extends BaseDAO<InternalCritical> {

    public InternalCriticalDAO(SessionFactory sessionFactory){ super(sessionFactory);}

    public List<InternalCritical> getAll(Class<InternalCritical> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public InternalCritical getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(InternalCritical.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }

    public List<InternalCritical> getByTitle(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(InternalCritical.class);
        criteria.add(Restrictions.like("title", keyword, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public List<InternalCritical> getByOidforSearch(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(InternalCritical.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }
}
