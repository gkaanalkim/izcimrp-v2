package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.ConsignmentInvoice;
import com.felislabs.izci.representation.entities.ConsignmentInvoiceDetail;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
public class ConsignmentInvoiceDetailDAO extends BaseDAO<ConsignmentInvoiceDetail> {
    public ConsignmentInvoiceDetailDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ConsignmentInvoiceDetail> getByConsignmentInvoice(ConsignmentInvoice consignmentInvoice) {
        Criteria criteria = currentSession().createCriteria(ConsignmentInvoiceDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("consignmentInvoice.oid", consignmentInvoice.getOid()));

        return list(criteria);
    }

    public ConsignmentInvoiceDetail getByOid(String oid) {
        Criteria criteria = currentSession().createCriteria(ConsignmentInvoiceDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }
}

