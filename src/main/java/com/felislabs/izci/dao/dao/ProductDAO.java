package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Product;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 08/03/2017.
 */
public class ProductDAO extends BaseDAO<Product> {
    public ProductDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Product getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Product> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Product getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
