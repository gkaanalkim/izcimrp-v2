package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Department;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by arricie on 21.03.2017.
 */
public class DepartmentDAO  extends BaseDAO<Department> {
    public DepartmentDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Department getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Department.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Department> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Department.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Department getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Department.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
