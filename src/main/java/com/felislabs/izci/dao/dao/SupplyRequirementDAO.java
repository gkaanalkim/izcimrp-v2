package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.*;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.*;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by dogukanyilmaz on 14/03/2017.
 */
public class SupplyRequirementDAO extends BaseDAO<SupplyRequirement> {
    public SupplyRequirementDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }


    public List<SupplyRequirement> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<SupplyRequirement> getSupplyRequirement(Model model, Stock stock, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("model.oid", model.getOid()));
        return list(criteria);
    }

    public SupplyRequirement getByAllCriteria(Model model, Stock stock, String documentNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("documentNumber", documentNumber));

        return uniqueResult(criteria);
    }

    public List<SupplyRequirement> getByAllCriteriaDetail(Model model, Stock stock, StockDetail stockDetail, String documentNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("stockDetail.oid", stockDetail.getOid()))
                .add(Restrictions.eq("documentNumber", documentNumber));

        return list(criteria);
    }

    public List<SupplyRequirement> getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("model.oid", model.getOid()));

        return list(criteria);
    }

    public List<SupplyRequirement> getBulkSupply(Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("type", "BS"));
        return list(criteria);
    }
    public List<SupplyRequirement> getSupply(Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("type", "SS"));
        return list(criteria);
    }

    public List<SupplyRequirement> getSupplyByDocumentNumber(String documentNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("documentNumber", documentNumber))
                .addOrder(Order.desc("creationDateTime"));
        return list(criteria);

    }public List<SupplyRequirement> getByDocumentNumber(String documentNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("documentNumber", documentNumber))
                .addOrder(Order.desc("creationDateTime"));
        return list(criteria);
    }

    public List<SupplyRequirement> getByBetweenDate(DateTime startDate, DateTime endDate, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.between("terminDate", startDate, endDate));
        return list(criteria);
    }

    public List<SupplyRequirement> getBySupplier(Supplier supplier, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("supplier.oid", supplier.getOid()));

        return list(criteria);
    }

    public SupplyRequirement readBulkDetail(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid",oid));
        return uniqueResult(criteria);
    }
    public SupplyRequirement readSupplyDetail(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid",oid));
        return uniqueResult(criteria);
    }
}
