package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.MaterialType;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by aykutarici on 06/12/16.
 */

public class MaterialTypeDAO extends BaseDAO<MaterialType> {
    public MaterialTypeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<MaterialType> getByInContainsTitle(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(MaterialType.class);
        criteria.add(Restrictions.like("title", keyword, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public List<MaterialType> getAll(Class<MaterialType> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public MaterialType getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(MaterialType.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }

    @Override
    public MaterialType delete(MaterialType entity) {
        entity.setDeleteStatus(true);
        return super.persist(entity);
    }

    public MaterialType getByTitle(String title, Company company) {
        Criteria criteria = currentSession().createCriteria(MaterialType.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("title", title))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }

    public List<MaterialType> getByOidforSearch(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(MaterialType.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

}

