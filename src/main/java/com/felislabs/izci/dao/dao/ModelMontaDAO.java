package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ModelMonta;
import com.felislabs.izci.representation.entities.ModelOrder;
import com.felislabs.izci.representation.entities.ProductionLine;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 02/02/2017.
 */
public class ModelMontaDAO extends BaseDAO<ModelMonta> {
    public ModelMontaDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelMonta> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMonta.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()));

        return list(criteria);
    }

    public List<ModelMonta> getByProductionLine(ProductionLine productionLine, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMonta.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("productionLine.oid", productionLine.getOid()));

        return list(criteria);
    }

    public List<ModelMonta> getByProductionLineAndCode(ProductionLine productionLine, String type, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMonta.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("productionLine.oid", productionLine.getOid()))
                .add(Restrictions.eq("type", type));

        return list(criteria);
    }

    public List<ModelMonta> getByType(String type, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMonta.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("type", type));

        return list(criteria);
    }

    public List<ModelMonta> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMonta.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(org.hibernate.criterion.Order.desc("creationDateTime"));

        return list(criteria);
    }

    public ModelMonta getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMonta.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public ModelMonta getByDocumentNumber(String documentNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMonta.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("documentNumber", documentNumber));

        return uniqueResult(criteria);
    }
}
