package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.*;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 24/01/2017.
 */
public class ModelMaterialDAO extends BaseDAO<ModelMaterial> {
    public ModelMaterialDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelMaterial> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ModelMaterial getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ModelMaterial> getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ModelMaterial> getByStock(Stock stock, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ModelMaterial> getByModelForSupply(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ModelMaterial> getBySupplierAndModel(String supplierCode, Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.createAlias("stock.supplier", "supplier");
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("supplier.code", supplierCode))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ModelMaterial getByUid(Model model, String uid, String placeOfUse, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("uid", uid))
                .add(Restrictions.eq("placeOfUse", placeOfUse));

        return uniqueResult(criteria);
    }

    public List<ModelMaterial> getByStockDetail(StockDetail stockDetail, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.add(Restrictions.eq("stockDetail.oid", stockDetail.getOid()))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ModelMaterial getByPlaceOfUse(String placeOfUse, Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMaterial.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("placeOfUse", placeOfUse))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
