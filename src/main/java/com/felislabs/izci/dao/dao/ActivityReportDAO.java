package com.felislabs.izci.dao.dao;


import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.ActivityReport;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by dogukanyilmaz on 06/02/2017.
 */
public class ActivityReportDAO extends BaseDAO<ActivityReport> {
    public ActivityReportDAO(SessionFactory sessionFactory){
        super(sessionFactory);
    }

    public List<ActivityReport> getByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(ActivityReport.class);
        Criteria add = criteria.add(Restrictions.lt("end",end)).add(Restrictions.gt("start",start)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
    public List<ActivityReport> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ActivityReport.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ActivityReport getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ActivityReport.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }


}
