package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Sayaci;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 28/12/2016.
 */
public class SayaciDAO extends BaseDAO<Sayaci> {
    public SayaciDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Sayaci getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Sayaci.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("code", code));

        return uniqueResult(criteria);
    }

    public Sayaci getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Sayaci.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public List<Sayaci> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Sayaci.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<Sayaci> readByName(String name, Company company) {
        Criteria criteria = currentSession().createCriteria(Sayaci.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.like("name", name, MatchMode.ANYWHERE));

        return list(criteria);
    }
}
