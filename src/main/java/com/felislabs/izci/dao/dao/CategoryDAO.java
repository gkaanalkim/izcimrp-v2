package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Category;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 08/03/2017.
 */
public class CategoryDAO extends BaseDAO<Category> {
    public CategoryDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Category getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Category.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid",oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
    public Category getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Category.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code",code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Category> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Category.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.asc("name"));

        return list(criteria);
    }
}
