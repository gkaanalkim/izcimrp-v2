package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
public class UserDAO extends BaseDAO<User> {
    public UserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public User findByEmail(String email, String password) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("email", email)).add(Restrictions.eq("password", password)).add(Restrictions.eq("deleteStatus", false));
        return uniqueResult(criteria);
    }

    public List<User> getByNameContains(String name, Company company) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public User getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public User getByEmail(String email, Company company) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("email", email))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public User getByCodeAndCompany(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    /**
     * Overrides
     */


    public List<User> getAll(Class<User> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }


    public User getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    @Override
    public User delete(User entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }
}
