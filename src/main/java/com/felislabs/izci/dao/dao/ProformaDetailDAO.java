package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Proforma;
import com.felislabs.izci.representation.entities.ProformaDetail;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
public class ProformaDetailDAO extends BaseDAO<ProformaDetail> {
    public ProformaDetailDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ProformaDetail> getByProforma(Proforma proforma) {
        Criteria criteria = currentSession().createCriteria(ProformaDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("proforma.oid", proforma.getOid()));

        return list(criteria);
    }

    public ProformaDetail getByOid(String oid) {
        Criteria criteria = currentSession().createCriteria(ProformaDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }
}
