package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Message;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by dogukanyilmaz on 10/11/2016.
 */
public class MessageDAO extends BaseDAO<Message> {
    public MessageDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Message> getAll(Class<Message> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Message getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Message.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Message> getByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(Message.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.between("creationDateTime", start, end))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.asc("creationDateTime"));
        return list(criteria);
    }
}