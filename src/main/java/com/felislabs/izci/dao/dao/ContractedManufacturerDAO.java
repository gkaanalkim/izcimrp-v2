package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ContractedManufacturer;
import com.felislabs.izci.representation.entities.Processing;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by arricie on 21.03.2017.
 */
public class ContractedManufacturerDAO  extends BaseDAO<ContractedManufacturer> {
    public ContractedManufacturerDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public ContractedManufacturer getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ContractedManufacturer.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ContractedManufacturer> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ContractedManufacturer.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ContractedManufacturer getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(ContractedManufacturer.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
    public List<ContractedManufacturer> readByProcessing(Processing processing, Company company) {
        Criteria criteria = currentSession().createCriteria(ContractedManufacturer.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("processing.oid", processing.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
