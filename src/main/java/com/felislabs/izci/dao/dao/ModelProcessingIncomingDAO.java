package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ModelOrder;
import com.felislabs.izci.representation.entities.ModelProcessing;
import com.felislabs.izci.representation.entities.ModelProcessingIncoming;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by arricie on 21.03.2017.
 */
public class ModelProcessingIncomingDAO  extends BaseDAO<ModelProcessingIncoming> {
    public ModelProcessingIncomingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public ModelProcessingIncoming getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }

    public List<ModelProcessingIncoming> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public List<ModelProcessingIncoming> getByModelProcessing(ModelProcessing modelProcessing, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelProcessing.oid", modelProcessing.getOid()));
        return list(criteria);
    }

    public List<ModelProcessingIncoming> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessingIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()));
        return list(criteria);
    }

}
