package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.core.BaseEntity;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.FairModelAssort;
import com.felislabs.izci.representation.entities.Model;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 18/03/2017.
 */
public class FairModelAssortDAO extends BaseDAO<FairModelAssort> {
    public FairModelAssortDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<FairModelAssort> getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(FairModelAssort.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<FairModelAssort> getByUidAndModel(String uid,Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(FairModelAssort.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("uid", uid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
