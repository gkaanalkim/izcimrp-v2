package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelOrder;
import com.felislabs.izci.representation.entities.ModelSupply;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 30/01/2017.
 */
public class ModelSupplyDAO extends BaseDAO<ModelSupply> {
    public ModelSupplyDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelSupply> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSupply.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
