package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ModelMonta;
import com.felislabs.izci.representation.entities.MontaRequirement;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by dogukanyilmaz on 04/05/2017.
 */
public class MontaRequirementDAO extends BaseDAO<MontaRequirement> {
    public MontaRequirementDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<MontaRequirement> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(MontaRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }


    public MontaRequirement getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(MontaRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public List<MontaRequirement> getByModelMonta(ModelMonta modelMonta, Company company) {
        Criteria criteria = currentSession().createCriteria(MontaRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelMonta.oid", modelMonta.getOid()));
        return list(criteria);
    }
}
