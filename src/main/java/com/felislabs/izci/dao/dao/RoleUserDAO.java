package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.RoleUser;
import com.felislabs.izci.representation.entities.User;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
public class RoleUserDAO extends BaseDAO<RoleUser> {
    public RoleUserDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public RoleUser findByUser(User user) {
        Criteria criteria = currentSession().createCriteria(RoleUser.class);
        criteria.add(Restrictions.eq("user.oid", user.getOid())).add(Restrictions.eq("deleteStatus", false));

        return uniqueResult(criteria);
    }

    @Override
    public List<RoleUser> findAll(Class<RoleUser> t) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    @Override
    public RoleUser findById(String oid) {
        Criteria criteria = currentSession().createCriteria(RoleUser.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    @Override
    public RoleUser delete(RoleUser entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public RoleUser getByUser(User user, Company company) {
        Criteria criteria = currentSession().createCriteria(RoleUser.class);
        criteria.add(Restrictions.eq("user.oid", user.getOid()))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
