package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Staff;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by arricie on 21.03.2017.
 */
public class StaffDAO  extends BaseDAO<Staff> {
    public StaffDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Staff getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Staff.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Staff> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Staff.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Staff getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Staff.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
