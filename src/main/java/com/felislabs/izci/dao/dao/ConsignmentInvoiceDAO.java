package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.*;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
public class ConsignmentInvoiceDAO extends BaseDAO<ConsignmentInvoice> {
    public ConsignmentInvoiceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ConsignmentInvoice> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ConsignmentInvoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ConsignmentInvoice getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ConsignmentInvoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ConsignmentInvoice> getByRepresentative(AccountingCustomerRepresentative accountingCustomerRepresentative, Company company) {
        Criteria criteria = currentSession().createCriteria(ConsignmentInvoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomerRepresentative.oid", accountingCustomerRepresentative.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ConsignmentInvoice> getByAccountingCustomer(AccountingCustomer accountingCustomer, Company company) {
        Criteria criteria = currentSession().createCriteria(ConsignmentInvoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomer.oid", accountingCustomer.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ConsignmentInvoice getByBid(Bid bid, Company company) {
        Criteria criteria = currentSession().createCriteria(ConsignmentInvoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("bid.oid", bid.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}

