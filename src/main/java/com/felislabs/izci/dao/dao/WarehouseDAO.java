package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Warehouse;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by dogukanyilmaz on 04/05/2017.
 */
public class WarehouseDAO extends BaseDAO<Warehouse> {
    public WarehouseDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Warehouse> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Warehouse.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Warehouse getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Warehouse.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public Warehouse getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Warehouse.class);
        criteria.add(Restrictions.eq("code", code)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

}
