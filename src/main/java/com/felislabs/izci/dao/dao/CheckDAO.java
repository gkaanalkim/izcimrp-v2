package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Account;
import com.felislabs.izci.representation.entities.AccountingCustomer;
import com.felislabs.izci.representation.entities.CheckProceeds;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 14/03/2017.
 */
public class CheckDAO extends BaseDAO<CheckProceeds> {
    public CheckDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<CheckProceeds> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(CheckProceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public CheckProceeds getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(CheckProceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<CheckProceeds> getByAccountingCustomer(AccountingCustomer accountingCustomer, Company company) {
        Criteria criteria = currentSession().createCriteria(CheckProceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomer.oid", accountingCustomer.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<CheckProceeds> getByAccount(Account account, Company company) {
        Criteria criteria = currentSession().createCriteria(CheckProceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accounting.oid", account.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
