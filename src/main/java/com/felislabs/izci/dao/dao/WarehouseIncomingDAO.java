package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Stock;
import com.felislabs.izci.representation.entities.StockDetail;
import com.felislabs.izci.representation.entities.WarehouseIncoming;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 23/06/2017.
 */
public class WarehouseIncomingDAO extends BaseDAO<WarehouseIncoming> {
    public WarehouseIncomingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<WarehouseIncoming> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(WarehouseIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.desc("creationDateTime"));

        return criteria.list();
    }

    public WarehouseIncoming getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(WarehouseIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public List<WarehouseIncoming> getByPoNumberAndStockAndStockDetail(String poNumber, Stock stock, StockDetail stockDetail, Company company) {
        Criteria criteria = currentSession().createCriteria(WarehouseIncoming.class);
        criteria.createAlias("modelPurcaseOrder", "modelPurcaseOrder");
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("stockDetail.oid", stockDetail.getOid()))
                .add(Restrictions.eq("modelPurcaseOrder.poNumber", poNumber));

        return list(criteria);
    }

    public List<WarehouseIncoming> getByPoNumberAndStock(String poNumber, Stock stock, Company company) {
        Criteria criteria = currentSession().createCriteria(WarehouseIncoming.class);
        criteria.createAlias("modelPurcaseOrder", "modelPurcaseOrder");
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("modelPurcaseOrder.poNumber", poNumber));

        return list(criteria);
    }
}
