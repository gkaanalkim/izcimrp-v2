package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Processing;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by arricie on 21.03.2017.
 */
public class ProcessingDAO extends BaseDAO<Processing> {
    public ProcessingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Processing getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Processing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Processing> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Processing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
    public List<Processing> getAllReadOutCompany(Company company) {
        Criteria criteria = currentSession().createCriteria(Processing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("insideCompany", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Processing getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Processing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
