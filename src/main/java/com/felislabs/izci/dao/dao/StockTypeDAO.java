package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.StockType;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by aykutarici on 26/12/16.
 */
public class StockTypeDAO extends BaseDAO<StockType> {
    public StockTypeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<StockType> getAll(Class<StockType> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public StockType getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(StockType.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    @Override
    public StockType delete(StockType entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public List<StockType> getByNameContains(String name, Company company) {
        Criteria criteria = currentSession().createCriteria(StockType.class);
        criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public StockType getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(StockType.class);
        criteria.add(Restrictions.eq("code", code)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<StockType> getByCodeContains(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(StockType.class);
        criteria.add(Restrictions.like("code", code, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
