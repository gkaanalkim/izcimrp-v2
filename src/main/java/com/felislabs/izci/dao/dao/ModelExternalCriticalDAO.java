package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelExternalCritical;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 25/01/2017.
 */
public class ModelExternalCriticalDAO extends BaseDAO<ModelExternalCritical> {
    public ModelExternalCriticalDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelExternalCritical> getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelExternalCritical.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }
}
