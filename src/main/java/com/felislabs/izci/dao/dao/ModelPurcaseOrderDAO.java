package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelPurcaseOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 21/06/2017.
 */
public class ModelPurcaseOrderDAO extends BaseDAO<ModelPurcaseOrder> {
    public ModelPurcaseOrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelPurcaseOrder> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelPurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }

    public ModelPurcaseOrder getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelPurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ModelPurcaseOrder> getByModel(String modelOid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelPurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.like("models", modelOid, MatchMode.ANYWHERE))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ModelPurcaseOrder getByPONumber(String poNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelPurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("poNumber", poNumber))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

}
