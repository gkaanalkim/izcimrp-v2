package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Customer;
import com.felislabs.izci.representation.entities.Model;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;

/**
 * Created by gunerkaanalkim on 20/01/2017.
 */
public class ModelDAO extends BaseDAO<Model> {
    public ModelDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Model> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
//                .add(Restrictions.between("creationDateTime", now.minusYears(1), now)) //TODO kolej ayakkabı için tekrar açılması gerekiyor
                .addOrder(Order.desc("modelCode"));
        return list(criteria);
    }

    public List<Model> readAllModelsAsSupplyCompleted(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("supplyCompleted", false))
                .add(Restrictions.between("creationDateTime", now.minusYears(1), now))
                .addOrder(Order.desc("modelCode"))
                .setMaxResults(1);
        return list(criteria);
    }

    public Integer getAllCount(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        int count = ((Number) criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .setProjection(Projections.rowCount()).uniqueResult()).intValue();

        return count;
    }

    public Integer getConfirmedCount(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        int count = ((Number) criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("confirm", true))
                .setProjection(Projections.rowCount()).uniqueResult()).intValue();

        return count;
    }

    public Integer getNotConfirmedCount(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        int count = ((Number) criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("confirm", false))
                .setProjection(Projections.rowCount()).uniqueResult()).intValue();

        return count;
    }

    public Model getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid", oid))
                .addOrder(Order.desc("creationDateTime"));

        return uniqueResult(criteria);
    }

    public List<Model> getBySystemCode(String modelCode, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelCode", modelCode));

        return list(criteria);
    }

    public List<Model> getByContainsInCode(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.add(Restrictions.like("modelCode", keyword, MatchMode.ANYWHERE))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<Model> getByCustomerAndDateRange(Customer customer, DateTime startDate, DateTime endDate, Company company, Integer maxResult) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.add(Restrictions.between("startDate", startDate, endDate))
                .add(Restrictions.between("endDate", startDate, endDate))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("customer.oid", customer.getOid()))
                .add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .addOrder(Order.desc("creationDateTime"))
                .setMaxResults(maxResult);
        return list(criteria);
    }

    public List<Model> getByCustomer(String customerName, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.createAlias("customer", "customer");

        criteria.add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.like("customer.name", customerName, MatchMode.ANYWHERE))
                .add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<Model> getByModelCodeLike(String modelCode, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);

        criteria.add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.like("modelCode", modelCode, MatchMode.ANYWHERE))
                .add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<Model> getByCustomerModelCodeLike(String customerModelCode, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);

        criteria.add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.like("customerModelCode", customerModelCode, MatchMode.ANYWHERE))
                .add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<Model> getByColor(String color, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);

        criteria.add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.like("color", color, MatchMode.ANYWHERE))
                .add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<Model> getByDateRange(DateTime startDate, DateTime endDate, Company company, Integer maxResult) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.add(Restrictions.between("startDate", startDate, endDate))
                .add(Restrictions.between("endDate", startDate, endDate))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .addOrder(Order.desc("creationDateTime"))
                .setMaxResults(maxResult);
        return list(criteria);
    }

    public List<Model> getByCreationDateTime(DateTime startDate, DateTime endDate, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.add(Restrictions.between("creationDateTime", startDate, endDate))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .addOrder(Order.desc("creationDateTime"));
        return list(criteria);
    }

    public List<Model> getIsOrder(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isOrderEntry", true))
                .add(Restrictions.between("creationDateTime", now.minusYears(1), now))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .setMaxResults(1);
        return list(criteria);
    }

    public List<Model> getIsOrderByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isOrderEntry", true))
                .add(Restrictions.between("creationDateTime", start, end))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public List<Model> getIsOrderAsSupplyCompleted(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isOrderEntry", true))
                .add(Restrictions.eq("supplyCompleted", false))
                .add(Restrictions.between("creationDateTime", now.minusYears(1), now))
                .add(Restrictions.eq("company.oid", company.getOid())).setMaxResults(1);
        return list(criteria);
    }

    public List<Model> getIsOrderAsSupplyCompletedByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isOrderEntry", true))
                .add(Restrictions.eq("supplyCompleted", false))
                .add(Restrictions.between("creationDateTime", start, end))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public List<Model> getIsPrice(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isPricingEntry", true))
                .add(Restrictions.between("creationDateTime", now.minusYears(1), now))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .setMaxResults(1);

        return list(criteria);
    }

    public List<Model> getIsPriceByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isPricingEntry", true))
                .add(Restrictions.between("creationDateTime", start, end))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<Model> getIsMaterial(Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isMaterialEntry", true))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.between("creationDateTime", now.minusYears(1), now))
                .addOrder(Order.desc("modelCode"))
                .setMaxResults(1);

        return list(criteria);
    }

    public List<Model> getIsMaterialByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isMaterialEntry", true))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.between("creationDateTime", start, end))
                .addOrder(Order.desc("modelCode"));

        return list(criteria);
    }

    public List<Model> getByModelCode(String modelCode, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelCode", modelCode))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<Model> getSeasonAndYear(String season, Integer year, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        criteria.add(Restrictions.eq("season", season))
                .add(Restrictions.eq("year", year))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("deleteStatus", Boolean.FALSE))
                .addOrder(Order.desc("creationDateTime"));
        return list(criteria);
    }

    public List<Model> getIsMaterialBySeasonAndYear(String season, Integer year, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isMaterialEntry", true))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("season", season))
                .add(Restrictions.eq("year", year))
                .addOrder(Order.desc("modelCode"));

        return list(criteria);
    }

    public List<Model> getIsPriceBySeasonAndYear(String season, Integer year, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isPricingEntry", true))
                .add(Restrictions.eq("season", season))
                .add(Restrictions.eq("year", year))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }


    public List<Model> getIsOrderAsSupplyCompletedBySeasonAndYear(String season, Integer year, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isOrderEntry", true))
                .add(Restrictions.eq("supplyCompleted", false))
                .add(Restrictions.eq("season", season))
                .add(Restrictions.eq("year", year))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public List<Model> getIsOrderBySeasonAndYear(String season, Integer year, Company company) {
        Criteria criteria = currentSession().createCriteria(Model.class);
        DateTime now = new DateTime();

        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("isOrderEntry", true))
                .add(Restrictions.eq("season", season))
                .add(Restrictions.eq("year", year))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

}
