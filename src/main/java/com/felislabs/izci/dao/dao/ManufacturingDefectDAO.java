package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ManufacturingDefect;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 25/06/2017.
 */
public class ManufacturingDefectDAO extends BaseDAO<ManufacturingDefect> {
    public ManufacturingDefectDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ManufacturingDefect> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ManufacturingDefect.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }

    public ManufacturingDefect getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ManufacturingDefect.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
