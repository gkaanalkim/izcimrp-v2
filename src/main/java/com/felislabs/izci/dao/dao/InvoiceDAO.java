package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.*;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
public class InvoiceDAO extends BaseDAO<Invoice> {
    public InvoiceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Invoice> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Invoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Invoice getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Invoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Invoice> getByRepresentative(AccountingCustomerRepresentative accountingCustomerRepresentative, Company company) {
        Criteria criteria = currentSession().createCriteria(Invoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomerRepresentative.oid", accountingCustomerRepresentative.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<Invoice> getByAccountingCustomer(AccountingCustomer accountingCustomer, Company company) {
        Criteria criteria = currentSession().createCriteria(Invoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomer.oid", accountingCustomer.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Invoice getByBid(Bid bid, Company company) {
        Criteria criteria = currentSession().createCriteria(Invoice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("bid.oid", bid.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
