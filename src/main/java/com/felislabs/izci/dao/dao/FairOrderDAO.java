package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Customer;
import com.felislabs.izci.representation.entities.FairOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by gunerkaanalkim on 23/03/2017.
 */
public class FairOrderDAO extends BaseDAO<FairOrder> {
    public FairOrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<FairOrder> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(FairOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<FairOrder> getByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(FairOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.between("creationDateTime", start, end));

        return list(criteria);
    }

    public List<FairOrder> getByCustomer(Customer customer, Company company) {
        Criteria criteria = currentSession().createCriteria(FairOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("customer.oid", customer.getOid()));

        return list(criteria);
    }

    public List<FairOrder> getByCustomerAndDate(Customer customer, DateTime startDate, DateTime endDate, Company company) {
        Criteria criteria = currentSession().createCriteria(FairOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.between("creationDateTime", endDate, startDate))
                .add(Restrictions.eq("customer.oid", customer.getOid()));

        return list(criteria);
    }

    public FairOrder getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(FairOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }
}
