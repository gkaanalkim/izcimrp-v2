package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ModelCuttingOrder;
import com.felislabs.izci.representation.entities.ModelOrder;
import com.felislabs.izci.representation.entities.Staff;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 01/02/2017.
 */
public class ModelCuttingOrderDAO extends BaseDAO<ModelCuttingOrder> {
    public ModelCuttingOrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelCuttingOrder> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ModelCuttingOrder getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ModelCuttingOrder> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(org.hibernate.criterion.Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<ModelCuttingOrder> getByStaff(Staff staff, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("staff.oid", staff.getOid()))
                .addOrder(org.hibernate.criterion.Order.desc("creationDateTime"));

        return list(criteria);
    }

    public ModelCuttingOrder getByDocumentNumber(String documentNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelCuttingOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("documentNumber", documentNumber));

        return uniqueResult(criteria);
    }
}
