package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.*;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 14/06/2017.
 */
public class PurcaseOrderDAO extends BaseDAO<PurcaseOrder> {
    public PurcaseOrderDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<PurcaseOrder> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<PurcaseOrder> getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("model.oid", model.getOid()));

        return list(criteria);
    }

    public List<PurcaseOrder> getBySupplier(Supplier supplier, Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("model.oid", supplier.getOid()));

        return list(criteria);
    }

    public List<PurcaseOrder> getByUser(User user, Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("updater.oid", user.getOid()));

        return list(criteria);
    }

    public List<PurcaseOrder> getByStock(Stock stock, Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()));

        return list(criteria);
    }

    public PurcaseOrder getByStockAndPoNumber(Stock stock, String poNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("poNumber", poNumber));

        return uniqueResult(criteria);
    }

    public PurcaseOrder getByStockAndStockDetailAndPoNumber(Stock stock, StockDetail stockDetail, String poNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("stockDetail.oid", stockDetail.getOid()))
                .add(Restrictions.like("poNumber", poNumber, MatchMode.ANYWHERE));

        return uniqueResult(criteria);
    }

    public List<PurcaseOrder> getByStockDetail(StockDetail stockDetail, Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("stockDetail.oid", stockDetail.getOid()));

        return list(criteria);
    }

    public List<PurcaseOrder> getByPONumber(String poNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(PurcaseOrder.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("poNumber", poNumber));

        return list(criteria);
    }
}
