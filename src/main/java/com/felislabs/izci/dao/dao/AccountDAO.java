package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Account;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 13/03/2017.
 */
public class AccountDAO extends BaseDAO<Account> {
    public AccountDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Account> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Account.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Account getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Account.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
