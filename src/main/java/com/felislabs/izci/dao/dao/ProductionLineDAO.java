package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ProductionLine;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 01/12/2016.
 */
public class ProductionLineDAO extends BaseDAO<ProductionLine> {
    public ProductionLineDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ProductionLine> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ProductionLine.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ProductionLine getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ProductionLine.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ProductionLine> getByOidforSearch(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ProductionLine.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public List<ProductionLine> getByCode(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(ProductionLine.class);
        criteria.add(Restrictions.like("code", keyword, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }


    public ProductionLine findByCode(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(ProductionLine.class);
        criteria.add(Restrictions.eq("code", keyword)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }

    public List<ProductionLine> getByName(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(ProductionLine.class);
        criteria.add(Restrictions.like("name", keyword, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public ProductionLine findByLineOrder(Integer keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(ProductionLine.class);
        criteria.add(Restrictions.eq("lineOrder", keyword)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return uniqueResult(criteria);
    }

    public List<ProductionLine> findByContainsCode(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(ProductionLine.class);
        criteria.add(Restrictions.like("code", keyword,MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }
}
