package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.CuttingRequirement;
import com.felislabs.izci.representation.entities.ModelCuttingOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by dogukanyilmaz on 04/05/2017.
 */
public class CuttingRequirementDAO extends BaseDAO<CuttingRequirement> {
    public CuttingRequirementDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<CuttingRequirement> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(CuttingRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }


    public CuttingRequirement getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(CuttingRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public List<CuttingRequirement> getByModelCuttingOrder(ModelCuttingOrder modelCuttingOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(CuttingRequirement.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelCuttingOrder.oid", modelCuttingOrder.getOid()));
        return list(criteria);
    }

}
