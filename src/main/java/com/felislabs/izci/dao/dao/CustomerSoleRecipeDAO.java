package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.CustomerSoleRecipe;
import com.felislabs.izci.representation.entities.FairOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 27/04/2017.
 */
public class CustomerSoleRecipeDAO extends BaseDAO<CustomerSoleRecipe> {
    public CustomerSoleRecipeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<CustomerSoleRecipe> getByFairOrder(FairOrder fairOrder) {
        Criteria criteria = currentSession().createCriteria(CustomerSoleRecipe.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("fairOrder.oid", fairOrder.getOid()));

        return list(criteria);
    }
}
