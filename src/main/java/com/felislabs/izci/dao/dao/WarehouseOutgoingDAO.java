package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.Stock;
import com.felislabs.izci.representation.entities.WarehouseOutgoing;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 24/06/2017.
 */
public class WarehouseOutgoingDAO extends BaseDAO<WarehouseOutgoing> {
    public WarehouseOutgoingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<WarehouseOutgoing> getByOutgoingTypeAndModel(String outgoingType, Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(WarehouseOutgoing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("outgoingType", outgoingType))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<WarehouseOutgoing> getByOutgoingTypeAndModelStock(String outgoingType, Model model, Stock stock, Company company) {
        Criteria criteria = currentSession().createCriteria(WarehouseOutgoing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("outgoingType", outgoingType))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<WarehouseOutgoing> getByOutgoingType(String outgoingType, Company company) {
        Criteria criteria = currentSession().createCriteria(WarehouseOutgoing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("outgoingType", outgoingType))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public WarehouseOutgoing getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(WarehouseOutgoing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
