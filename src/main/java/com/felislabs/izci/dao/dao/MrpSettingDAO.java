package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.MrpSetting;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 26/10/2016.
 */
public class MrpSettingDAO extends BaseDAO<MrpSetting> {
    public MrpSettingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<MrpSetting> get(Company company) {
        Criteria criteria = currentSession().createCriteria(MrpSetting.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
