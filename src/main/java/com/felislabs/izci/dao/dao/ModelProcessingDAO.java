package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.*;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by arricie on 21.03.2017.
 */
public class ModelProcessingDAO  extends BaseDAO<ModelProcessing> {
    public ModelProcessingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public ModelProcessing getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ModelProcessing> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
    public List<ModelProcessing> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ModelProcessing> getIncoming(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.isNotNull("incomingProcessingQuantity"));

        return list(criteria);
    }

    public ModelProcessing getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
    public List<ModelProcessing> getByContractedManufacturer(ContractedManufacturer c, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("contractedManufacturer.oid", c.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
    public ModelProcessing getByDocumentNumber(String documentNumber, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelProcessing.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("documentNumber", documentNumber));
        return uniqueResult(criteria);
    }
}
