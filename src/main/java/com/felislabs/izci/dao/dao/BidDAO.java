package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.AccountingCustomer;
import com.felislabs.izci.representation.entities.AccountingCustomerRepresentative;
import com.felislabs.izci.representation.entities.Bid;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 09/03/2017.
 */
public class BidDAO extends BaseDAO<Bid> {
    public BidDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Bid> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Bid.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Bid getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Bid.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Bid> getByRepresentative(AccountingCustomerRepresentative accountingCustomerRepresentative, Company company) {
        Criteria criteria = currentSession().createCriteria(Bid.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomerRepresentative.oid", accountingCustomerRepresentative.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<Bid> getByAccountingCustomer(AccountingCustomer accountingCustomer, Company company) {
        Criteria criteria = currentSession().createCriteria(Bid.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomer.oid", accountingCustomer.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
