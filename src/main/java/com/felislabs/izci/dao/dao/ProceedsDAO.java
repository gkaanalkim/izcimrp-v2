package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Account;
import com.felislabs.izci.representation.entities.AccountingCustomer;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Proceeds;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 14/03/2017.
 */
public class ProceedsDAO extends BaseDAO<Proceeds> {
    public ProceedsDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Proceeds> getByAccount(Account account, Company company) {
        Criteria criteria = currentSession().createCriteria(Proceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("account.oid", account.getOid()));

        return list(criteria);
    }

    public List<Proceeds> getByAccountingCustomer(AccountingCustomer accountingCustomer, Company company) {
        Criteria criteria = currentSession().createCriteria(Proceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("accountingCustomer.oid", accountingCustomer.getOid()))
                .addOrder(Order.asc("creationDateTime"));

        return list(criteria);
    }

    public List<Proceeds> getByAccountingCustomerAndType(AccountingCustomer accountingCustomer, String type, Company company) {
        Criteria criteria = currentSession().createCriteria(Proceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.like("type", type, MatchMode.ANYWHERE))
                .add(Restrictions.eq("accountingCustomer.oid", accountingCustomer.getOid()));

        return list(criteria);
    }

    public List<Proceeds> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Proceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Proceeds getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Proceeds.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
