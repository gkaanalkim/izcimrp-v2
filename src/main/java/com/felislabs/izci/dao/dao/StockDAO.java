package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Stock;
import com.felislabs.izci.representation.entities.StockType;
import com.felislabs.izci.representation.entities.Supplier;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by aykutarici on 19/12/16.
 */

public class StockDAO extends BaseDAO<Stock> {

    public StockDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Stock> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Stock.class);
        criteria.createAlias("stockType", "s");
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.ne("s.code", "M_AYK"));

        return list(criteria);
    }

    public Stock getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Stock.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Stock> stockList(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Stock.class);
        criteria.createAlias("stockType", "s");
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.ne("s.code", "M_AYK"));

        return list(criteria);
    }

    public Stock getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Stock.class);
        criteria.createAlias("stockType", "s");
        criteria.add(Restrictions.eq("code", code)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.ne("s.code", "M_AYK"));

        return uniqueResult(criteria);
    }

    public List<Stock> getByContainsInCode(String keyword, Company company) {
        Criteria criteria = currentSession().createCriteria(Stock.class);
        criteria.createAlias("stockType", "s");
        criteria.add(Restrictions.like("code", keyword, MatchMode.ANYWHERE))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.ne("s.code", "M_AYK"));

        return list(criteria);
    }

    public Stock getByCodeAndStockType(String code, StockType stockType, Company company) {
        Criteria criteria = currentSession().createCriteria(Stock.class);
        criteria.add(Restrictions.eq("code", code))
                .add(Restrictions.eq("stockType.oid", stockType.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("deleteStatus", false));

        return uniqueResult(criteria);
    }

    public List<Stock> getFinishedStock(Company company) {
        Criteria criteria = currentSession().createCriteria(Stock.class);
        criteria.createAlias("stockType", "s");
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("s.code", "M_AYK"));

        return list(criteria);
    }
}
