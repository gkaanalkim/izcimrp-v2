package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.*;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 01/02/2017.
 */
public class ModelSayaDAO extends BaseDAO<ModelSaya> {
    public ModelSayaDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelSaya> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSaya.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ModelSaya> getIncoming(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSaya.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.isNotNull("incomingSayaQuantity"));

        return list(criteria);
    }

    public ModelSaya getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSaya.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ModelSaya> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSaya.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(org.hibernate.criterion.Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<ModelSaya> getBySayaci(Sayaci sayaci, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelSaya.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("sayaci.oid", sayaci.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
