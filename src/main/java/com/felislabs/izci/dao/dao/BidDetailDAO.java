package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Bid;
import com.felislabs.izci.representation.entities.BidDetail;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 10/03/2017.
 */
public class BidDetailDAO extends BaseDAO<BidDetail> {
    public BidDetailDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<BidDetail> getByBid(Bid bid) {
        Criteria criteria = currentSession().createCriteria(BidDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("bid.oid", bid.getOid()));

        return list(criteria);
    }

    public BidDetail getByOid(String oid) {
        Criteria criteria = currentSession().createCriteria(BidDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }
}
