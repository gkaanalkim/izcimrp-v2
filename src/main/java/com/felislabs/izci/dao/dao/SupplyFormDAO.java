package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.SupplyForm;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 20/02/2017.
 */
public class SupplyFormDAO extends BaseDAO<SupplyForm> {
    public SupplyFormDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<SupplyForm> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyForm.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<SupplyForm> getByType(String type, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyForm.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("type", type));

        return list(criteria);
    }

    public SupplyForm getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(SupplyForm.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));


        return uniqueResult(criteria);
    }
}
