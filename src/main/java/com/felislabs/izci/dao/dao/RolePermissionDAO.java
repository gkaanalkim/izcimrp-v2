package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Permission;
import com.felislabs.izci.representation.entities.Role;
import com.felislabs.izci.representation.entities.RolePermission;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
public class RolePermissionDAO extends BaseDAO<RolePermission> {
    public RolePermissionDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public RolePermission getByRoleAndPermission(Role role, Permission permission) {
        Criteria criteria = currentSession().createCriteria(RolePermission.class);
        criteria.add(Restrictions.eq("role.oid", role.getOid())).add(Restrictions.eq("permission.oid", permission.getOid())).add(Restrictions.eq("deleteStatus", false));

        return uniqueResult(criteria);
    }

    public List<RolePermission> getByRole(Role role) {
        Criteria criteria = currentSession().createCriteria(RolePermission.class);
        criteria.add(Restrictions.eq("role.oid", role.getOid())).add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    public List<RolePermission> getByRoleUndeleted(Role role) {
        Criteria criteria = currentSession().createCriteria(RolePermission.class);
        criteria.add(Restrictions.eq("role.oid", role.getOid()));

        return list(criteria);
    }

    public List<RolePermission> getByPermission(Permission permission) {
        Criteria criteria = currentSession().createCriteria(RolePermission.class);
        criteria.add(Restrictions.eq("permission.oid", permission.getOid())).add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    public List<RolePermission> getAll(Class<RolePermission> t) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    public RolePermission getById(String oid) {
        Criteria criteria = currentSession().createCriteria(RolePermission.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));
        return uniqueResult(criteria);
    }

    @Override
    public RolePermission delete(RolePermission entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }
}
