package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.SystemActivity;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Created by gunerkaanalkim on 25/01/16.
 */
public class SystemActivityDAO extends BaseDAO<SystemActivity> {
    public SystemActivityDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<SystemActivity> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(SystemActivity.class);
        criteria.add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.desc("creationDateTime"))
                .setMaxResults(5000);

        return list(criteria);
    }

    public List<SystemActivity> getByDateRange(DateTime start, DateTime end, Company company) {
        Criteria criteria = currentSession().createCriteria(SystemActivity.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.between("creationDateTime", start, end))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }
}
