package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.core.BaseEntity;
import com.felislabs.izci.representation.entities.AccountingCustomer;
import com.felislabs.izci.representation.entities.AccountingCustomerContact;
import com.felislabs.izci.representation.entities.AccountingCustomerRepresentative;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/03/2017.
 */
public class AccountingCustomerRepresentativeDAO extends BaseDAO<AccountingCustomerRepresentative> {
    public AccountingCustomerRepresentativeDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<AccountingCustomerRepresentative> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(AccountingCustomerRepresentative.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<AccountingCustomerRepresentative> getByCustomer(AccountingCustomer accountingCustomer, Company company) {
        Criteria criteria = currentSession().createCriteria(AccountingCustomerRepresentative.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("accountingCustomer.oid", accountingCustomer.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public AccountingCustomerRepresentative getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(AccountingCustomerRepresentative.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
