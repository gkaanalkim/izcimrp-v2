package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.FairModelPrice;
import com.felislabs.izci.representation.entities.Model;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 18/03/2017.
 */
public class FairModelPriceDAO extends BaseDAO<FairModelPrice> {
    public FairModelPriceDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<FairModelPrice> getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(FairModelPrice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.asc("name"));

        return list(criteria);
    }

    public List<FairModelPrice> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(FairModelPrice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.asc("name"));

        return list(criteria);
    }


    public FairModelPrice getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(FairModelPrice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<FairModelPrice> getByPriceName(String name, Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(FairModelPrice.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("name", name))
                .add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
