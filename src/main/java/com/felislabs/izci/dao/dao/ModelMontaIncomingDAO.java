package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ModelMonta;
import com.felislabs.izci.representation.entities.ModelMontaIncoming;
import com.felislabs.izci.representation.entities.ModelOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;

import java.util.List;

/**
 * Created by gunerkaanalkim on 27/02/2017.
 */
public class ModelMontaIncomingDAO extends BaseDAO<ModelMontaIncoming> {
    public ModelMontaIncomingDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelMontaIncoming> getByModelMonta(ModelMonta modelMonta, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMontaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelMonta.oid", modelMonta.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ModelMontaIncoming> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMontaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public ModelMontaIncoming getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMontaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<ModelMontaIncoming> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMontaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(org.hibernate.criterion.Order.desc("creationDateTime"));

        return list(criteria);
    }

    public List<ModelMontaIncoming> getByCount(Integer count, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMontaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .addOrder(Order.desc("creationDateTime"))
                .add(Restrictions.eq("company.oid", company.getOid())).setMaxResults(count);

        return list(criteria);
    }

    public List<ModelMontaIncoming> getByModelOrderAndQuantity(Integer quantity, ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelMontaIncoming.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("quantity", quantity))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()));

        return list(criteria);
    }
}
