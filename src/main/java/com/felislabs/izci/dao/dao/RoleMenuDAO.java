package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Menu;
import com.felislabs.izci.representation.entities.Role;
import com.felislabs.izci.representation.entities.RoleMenu;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 19/09/15.
 */
public class RoleMenuDAO extends BaseDAO<RoleMenu> {
    public RoleMenuDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<RoleMenu> getByRole(String project, Role role, Company company) {
        Criteria criteria = currentSession().createCriteria(RoleMenu.class);
        criteria.createAlias("menu", "m");
        criteria.add(Restrictions.eq("role.oid", role.getOid()))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("m.projectCode", project))
                .addOrder(Order.asc("m.menuOrder"))
                .add(Restrictions.eq("company.oid", company.getOid()));
        return list(criteria);
    }

    public RoleMenu getByRoleAndMenu(Role role, Menu menu, Company company) {
        Criteria criteria = currentSession().createCriteria(RoleMenu.class);
        criteria.createAlias("menu", "m");

        criteria.add(Restrictions.eq("role.oid", role.getOid()))
                .add(Restrictions.eq("menu.oid", menu.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));


        return uniqueResult(criteria);
    }

    public List<RoleMenu> getAll(Class<RoleMenu> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public RoleMenu getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(RoleMenu.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    @Override
    public RoleMenu delete(RoleMenu entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }
}
