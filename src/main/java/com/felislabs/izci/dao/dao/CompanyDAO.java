package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/12/15.
 */
public class CompanyDAO extends BaseDAO<Company> {
    public CompanyDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public Company findById(String oid) {
        Criteria criteria = currentSession().createCriteria(Company.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    @Override
    public List<Company> findAll(Class<Company> t) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false));

        return list(criteria);
    }

    @Override
    public Company delete(Company entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public Company getByEmail(String email) {
        Criteria criteria = currentSession().createCriteria(Company.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("email", email));
        return uniqueResult(criteria);
    }

    public List<Company> getByContainsInTitle(String keyword) {
        Criteria criteria = currentSession().createCriteria(Company.class);
        criteria.add(Restrictions.like("name", keyword, MatchMode.ANYWHERE)).addOrder(Order.asc("name"))
                .add(Restrictions.eq("deleteStatus", false))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }
    public List<Company> getByCodeList(String keyword) {
        Criteria criteria = currentSession().createCriteria(Company.class);
        criteria.add(Restrictions.like("code", keyword, MatchMode.ANYWHERE)).addOrder(Order.asc("code"))
                .add(Restrictions.eq("deleteStatus", false))
                .addOrder(Order.desc("creationDateTime"));

        return list(criteria);
    }
}
