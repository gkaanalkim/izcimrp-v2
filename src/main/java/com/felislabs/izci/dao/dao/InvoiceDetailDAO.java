package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Invoice;
import com.felislabs.izci.representation.entities.InvoiceDetail;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
public class InvoiceDetailDAO extends BaseDAO<InvoiceDetail> {
    public InvoiceDetailDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<InvoiceDetail> getByInvoice(Invoice invoice) {
        Criteria criteria = currentSession().createCriteria(InvoiceDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("invoice.oid", invoice.getOid()));

        return list(criteria);
    }

    public InvoiceDetail getByOid(String oid) {
        Criteria criteria = currentSession().createCriteria(InvoiceDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }
}
