package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Stock;
import com.felislabs.izci.representation.entities.StockDetail;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 26/01/2017.
 */
public class StockDetailDAO extends BaseDAO<StockDetail> {
    public StockDetailDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<StockDetail> getByStock(Stock stock, Company company) {
        Criteria criteria = currentSession().createCriteria(StockDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("stock.oid", stock.getOid()));

        return list(criteria);
    }

    public StockDetail getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(StockDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public StockDetail getByNameAndStock(String name, Stock stock, Company company) {
        Criteria criteria = currentSession().createCriteria(StockDetail.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("stock.oid", stock.getOid()))
                .add(Restrictions.eq("name", name));

        return uniqueResult(criteria);
    }
}
