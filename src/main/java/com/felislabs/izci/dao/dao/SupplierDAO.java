package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Supplier;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by aykutarici on 26/12/16.
 */
public class SupplierDAO extends BaseDAO<Supplier> {

    public SupplierDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<Supplier> getAll(Class<Supplier> t, Company company) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Supplier getById(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Supplier.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    @Override
    public Supplier delete(Supplier entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public List<Supplier> getByNameContains(String name, Company company) {
        Criteria criteria = currentSession().createCriteria(Supplier.class);
        criteria.add(Restrictions.like("name", name, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public Supplier getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Supplier.class);
        criteria.add(Restrictions.eq("code", code)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Supplier> getByCodeContains(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(Supplier.class);
        criteria.add(Restrictions.like("code", code, MatchMode.ANYWHERE)).add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
