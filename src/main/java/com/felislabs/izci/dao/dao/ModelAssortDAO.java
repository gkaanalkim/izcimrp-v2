package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelAssort;
import com.felislabs.izci.representation.entities.ModelOrder;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 29/01/2017.
 */
public class ModelAssortDAO extends BaseDAO<ModelAssort> {
    public ModelAssortDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelAssort> getByModelOrder(ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelAssort.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()));

        return list(criteria);
    }

    public List<ModelAssort> getByUid(String uid, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelAssort.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("uid", uid));

        return list(criteria);
    }

    public List<ModelAssort> getByModelOrderAndUid(String uid, ModelOrder modelOrder, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelAssort.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .add(Restrictions.eq("uid", uid))
                .add(Restrictions.eq("modelOrder.oid", modelOrder.getOid()));

        return list(criteria);
    }
}
