package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Brand;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 08/03/2017.
 */
public class BrandDAO extends BaseDAO<Brand> {
    public BrandDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Brand getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(Brand.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<Brand> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(Brand.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.asc("name"));

        return list(criteria);
    }
}
