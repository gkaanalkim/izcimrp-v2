package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Menu;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 19/09/15.
 */
public class MenuDAO extends BaseDAO<Menu> {
    public MenuDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    private String adminMenuCode = "Company";

    public Menu getByCode(String project, String code) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("code", code))
                .add(Restrictions.eq("projectCode", project));
        return uniqueResult(criteria);
    }

    public List<Menu> getAllUndeleted(String project) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("projectCode", project));

        return list(criteria);
    }

    public List<Menu> getAll(Class<Menu> t) {
        Criteria criteria = currentSession().createCriteria(t);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .addOrder(Order.asc("menuOrder"));

        return list(criteria);
    }

    public List<Menu> getAllByProject(String project) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("projectCode", project))
                .addOrder(Order.asc("menuOrder"));

        return list(criteria);
    }

    public List<Menu> getAllExceptCompany(String project) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.ne("code", this.adminMenuCode))
                .add(Restrictions.eq("projectCode", project))
                .addOrder(Order.asc("menuOrder"));

        return list(criteria);
    }

    public Menu getById(String oid) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false)).add(Restrictions.eq("oid", oid));

        return uniqueResult(criteria);
    }

    public Menu getByIdAndProject(String oid, String project) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("projectCode", project));

        return uniqueResult(criteria);
    }

    public Menu delete(Menu entity) {
        entity.setDeleteStatus(true);

        return super.persist(entity);
    }

    public Menu getByUrl(String url) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("url", url));

        return uniqueResult(criteria);
    }


    public List<Menu> getByCodeContains(String project, String code) {
        Criteria criteria = currentSession().createCriteria(Menu.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("projectCode", project))
                .add(Restrictions.like("code", code, MatchMode.ANYWHERE));

        return list(criteria);
    }

}
