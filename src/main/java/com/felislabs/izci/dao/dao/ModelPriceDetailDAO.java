package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelPriceDetail;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 27/01/2017.
 */
public class ModelPriceDetailDAO extends BaseDAO<ModelPriceDetail> {
    public ModelPriceDetailDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<ModelPriceDetail> getByModel(Model model, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelPriceDetail.class);
        criteria.add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public List<ModelPriceDetail> getByModelAndDescription(Model model, String description, Company company) {
        Criteria criteria = currentSession().createCriteria(ModelPriceDetail.class);
        criteria.add(Restrictions.eq("model.oid", model.getOid()))
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.like("description", description, MatchMode.ANYWHERE))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }
}
