package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.AccountingCustomer;
import com.felislabs.izci.representation.entities.Company;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/03/2017.
 */
public class AccountingCustomerDAO extends BaseDAO<AccountingCustomer> {
    public AccountingCustomerDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<AccountingCustomer> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(AccountingCustomer.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()))
                .addOrder(Order.asc("title"));

        return list(criteria);
    }

    public AccountingCustomer getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(AccountingCustomer.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }
}
