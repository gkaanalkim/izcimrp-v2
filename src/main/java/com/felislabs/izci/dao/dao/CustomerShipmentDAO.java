package com.felislabs.izci.dao.dao;

import com.felislabs.izci.dao.core.BaseDAO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Customer;
import com.felislabs.izci.representation.entities.CustomerShipment;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by gunerkaanalkim on 14/01/2017.
 */
public class CustomerShipmentDAO extends BaseDAO<CustomerShipment> {
    public CustomerShipmentDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<CustomerShipment> getAll(Company company) {
        Criteria criteria = currentSession().createCriteria(CustomerShipment.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

    public CustomerShipment getByOid(String oid, Company company) {
        Criteria criteria = currentSession().createCriteria(CustomerShipment.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("oid", oid))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public CustomerShipment getByCode(String code, Company company) {
        Criteria criteria = currentSession().createCriteria(CustomerShipment.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("code", code))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return uniqueResult(criteria);
    }

    public List<CustomerShipment> getByCustomer(Customer customer, Company company) {
        Criteria criteria = currentSession().createCriteria(CustomerShipment.class);
        criteria.add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("customer.oid", customer.getOid()))
                .add(Restrictions.eq("company.oid", company.getOid()));

        return list(criteria);
    }

}
