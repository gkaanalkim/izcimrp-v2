package com.felislabs.izci.dao.core;

import com.felislabs.izci.representation.core.BaseEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by gunerkaanalkim on 14/08/15.
 */
public class BaseDAO<T extends BaseEntity> extends AbstractDAO<T> {

    public BaseDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public List<T> findAll(Class<T> t) {
        Criteria criteria = currentSession().createCriteria(t);
        return list(criteria);
    }

    public T create(T entity) {
        return persist(entity);
    }

    public  T findByCode(String code){
        return get(code);
    }

    public T findById(String oid) {
        return get(oid);
    }

    public T update(T entity) {
        return persist(entity);
    }

    public T delete(T entity) {
        currentSession().delete(entity);
        return entity;
    }

    public void flush() {
        currentSession().flush();
    }

    public T merge(T entity) {
        return (T) currentSession().merge(entity);
    }

    public T detach(T entity) {
        currentSession().evict(entity);
        return entity;
    }
}