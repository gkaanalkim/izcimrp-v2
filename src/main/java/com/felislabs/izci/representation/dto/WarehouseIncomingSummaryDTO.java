package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by gunerkaanalkim on 24/06/2017.
 */
public class WarehouseIncomingSummaryDTO {
    private String oid;
    private String supplierName;
    private String stockCode;
    private String stockName;
    private String stockDetailName;
    private Double quantity;
    private String poNumber;

    public WarehouseIncomingSummaryDTO(String oid, String supplierName, String stockCode, String stockName, String stockDetailName, Double quantity, String poNumber) {
        this.oid = oid;
        this.supplierName = supplierName;
        this.stockCode = stockCode;
        this.stockName = stockName;
        this.stockDetailName = stockDetailName;
        this.quantity = quantity;
        this.poNumber = poNumber;
    }

    @JsonProperty("spn")
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @JsonProperty("o")
    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @JsonProperty("sc")
    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    @JsonProperty("sn")
    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    @JsonProperty("sdn")
    public String getStockDetailName() {
        return stockDetailName;
    }

    public void setStockDetailName(String stockDetailName) {
        this.stockDetailName = stockDetailName;
    }

    @JsonProperty("q")
    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("pon")
    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }
}
