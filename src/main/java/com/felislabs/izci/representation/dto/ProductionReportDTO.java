package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.*;

import java.util.List;

/**
 * Created by gunerkaanalkim on 21/02/2017.
 */
public class ProductionReportDTO {
    private ModelOrder modelOrder;
    private List<ModelMonta> modelMontas;
    private List<ModelCuttingOrder> modelCuttingOrders;
    private List<ModelCuttingIncoming> modelCuttingIncomings;
    private List<ModelSaya> modelSayas;
    private List<ModelSayaIncoming> modelSayaIncomings;
    private List<SupplyForm> supplyForms;

    public ProductionReportDTO(ModelOrder modelOrder, List<ModelMonta> modelMontas, List<ModelCuttingOrder> modelCuttingOrders, List<ModelCuttingIncoming> modelCuttingIncomings, List<ModelSaya> modelSayas, List<ModelSayaIncoming> modelSayaIncomings, List<SupplyForm> supplyForms) {
        this.modelOrder = modelOrder;
        this.modelMontas = modelMontas;
        this.modelCuttingOrders = modelCuttingOrders;
        this.modelCuttingIncomings = modelCuttingIncomings;
        this.modelSayas = modelSayas;
        this.modelSayaIncomings = modelSayaIncomings;
        this.supplyForms = supplyForms;
    }

    public List<SupplyForm> getSupplyForms() {
        return supplyForms;
    }

    public void setSupplyForms(List<SupplyForm> supplyForms) {
        this.supplyForms = supplyForms;
    }

    public List<ModelMonta> getModelMontas() {
        return modelMontas;
    }

    public void setModelMontas(List<ModelMonta> modelMontas) {
        this.modelMontas = modelMontas;
    }

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }

    public List<ModelSaya> getModelSayas() {
        return modelSayas;
    }

    public void setModelSayas(List<ModelSaya> modelSayas) {
        this.modelSayas = modelSayas;
    }

    public List<ModelSayaIncoming> getModelSayaIncomings() {
        return modelSayaIncomings;
    }

    public void setModelSayaIncomings(List<ModelSayaIncoming> modelSayaIncomings) {
        this.modelSayaIncomings = modelSayaIncomings;
    }

    public List<ModelCuttingOrder> getModelCuttingOrders() {
        return modelCuttingOrders;
    }

    public void setModelCuttingOrders(List<ModelCuttingOrder> modelCuttingOrders) {
        this.modelCuttingOrders = modelCuttingOrders;
    }

    public List<ModelCuttingIncoming> getModelCuttingIncomings() {
        return modelCuttingIncomings;
    }

    public void setModelCuttingIncomings(List<ModelCuttingIncoming> modelCuttingIncomings) {
        this.modelCuttingIncomings = modelCuttingIncomings;
    }
}
