package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 16/05/2017.
 */
public class SayaOrderSummary {
    private String sayaciName;
    private String modelCode;
    private String modelColor;
    private String customerModelCode;
    private String defaultPhoto;
    private String sayaDocumentNumber;
    private DateTime creationDatetime;
    private Integer sayaOrderQuantity;
    private Integer sayaIncomingQuantity;
    private Integer orderQuantity;
    private Double sayaPrice;
    private Integer year;
    private String season;

    @JsonProperty("siq")
    public Integer getSayaIncomingQuantity() {
        return sayaIncomingQuantity;
    }

    public void setSayaIncomingQuantity(Integer sayaIncomingQuantity) {
        this.sayaIncomingQuantity = sayaIncomingQuantity;
    }

    @JsonProperty("sn")
    public String getSayaciName() {
        return sayaciName;
    }

    public void setSayaciName(String sayaciName) {
        this.sayaciName = sayaciName;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("c")
    public String getModelColor() {
        return modelColor;
    }

    public void setModelColor(String modelColor) {
        this.modelColor = modelColor;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("sdn")
    public String getSayaDocumentNumber() {
        return sayaDocumentNumber;
    }

    public void setSayaDocumentNumber(String sayaDocumentNumber) {
        this.sayaDocumentNumber = sayaDocumentNumber;
    }

    @JsonProperty("cdt")
    public DateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(DateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("soq")
    public Integer getSayaOrderQuantity() {
        return sayaOrderQuantity;
    }

    public void setSayaOrderQuantity(Integer sayaOrderQuantity) {
        this.sayaOrderQuantity = sayaOrderQuantity;
    }

    @JsonProperty("oq")
    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    @JsonProperty("sayap")
    public Double getSayaPrice() {
        return sayaPrice;
    }

    public void setSayaPrice(Double sayaPrice) {
        this.sayaPrice = sayaPrice;
    }

    @JsonProperty("y")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @JsonProperty("s")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }
}
