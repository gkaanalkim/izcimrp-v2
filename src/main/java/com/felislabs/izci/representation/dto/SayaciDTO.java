package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.ModelSaya;
import com.felislabs.izci.representation.entities.ModelSayaIncoming;

/**
 * Created by dogukanyilmaz on 16/03/2017.
 */
public class SayaciDTO {
    private ModelSaya modelSaya;
    private ModelSayaIncoming modelSayaIncoming;
    private String onTime = "";
    private Integer modelSayaIncomingQuantity;

    public ModelSaya getModelSaya() {
        return modelSaya;
    }

    public void setModelSaya(ModelSaya modelSaya) {
        this.modelSaya = modelSaya;
    }

    public ModelSayaIncoming getModelSayaIncoming() {
        return modelSayaIncoming;
    }

    public void setModelSayaIncoming(ModelSayaIncoming modelSayaIncoming) {
        this.modelSayaIncoming = modelSayaIncoming;
    }

    public String getOnTime() {
        return onTime;
    }

    public void setOnTime(String onTime) {
        this.onTime = onTime;
    }

    public Integer getModelSayaIncomingQuantity() {
        return modelSayaIncomingQuantity;
    }

    public void setModelSayaIncomingQuantity(Integer modelSayaIncomingQuantity) {
        this.modelSayaIncomingQuantity = modelSayaIncomingQuantity;
    }
}
