package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.Stock;
import com.felislabs.izci.representation.entities.StockDetail;

import java.util.List;

/**
 * Created by gunerkaanalkim on 23/06/2017.
 */
public class StockAndStockDetailDTO {
    private Stock stock;
    private List<StockDetail> stockDetails;

    public StockAndStockDetailDTO(Stock stock, List<StockDetail> stockDetails) {
        this.stock = stock;
        this.stockDetails = stockDetails;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public List<StockDetail> getStockDetails() {
        return stockDetails;
    }

    public void setStockDetails(List<StockDetail> stockDetails) {
        this.stockDetails = stockDetails;
    }
}
