package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.ModelAssort;
import com.felislabs.izci.representation.entities.ModelCuttingOrder;
import com.felislabs.izci.representation.entities.ModelMaterial;

import java.util.List;

/**
 * Created by gunerkaanalkim on 01/02/2017.
 */
public class CuttingOrderDTO {
    private ModelCuttingOrder modelCuttingOrder;
    private List<ModelMaterial> modelMaterials;
    private List<ModelAssort> modelAssorts;

    public ModelCuttingOrder getModelCuttingOrder() {
        return modelCuttingOrder;
    }

    public void setModelCuttingOrder(ModelCuttingOrder modelCuttingOrder) {
        this.modelCuttingOrder = modelCuttingOrder;
    }

    public List<ModelMaterial> getModelMaterials() {
        return modelMaterials;
    }

    public void setModelMaterials(List<ModelMaterial> modelMaterials) {
        this.modelMaterials = modelMaterials;
    }

    public List<ModelAssort> getModelAssorts() {
        return modelAssorts;
    }

    public void setModelAssorts(List<ModelAssort> modelAssorts) {
        this.modelAssorts = modelAssorts;
    }
}
