package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.ModelAlarm;
import com.felislabs.izci.representation.entities.ModelAssort;
import com.felislabs.izci.representation.entities.ModelOrder;

import java.util.List;

/**
 * Created by gunerkaanalkim on 29/01/2017.
 */
public class ModelOrderDTO {
    private List<ModelAlarm> modelAlarms;
    private List<ModelAssort> modelAssorts;

    public List<ModelAlarm> getModelAlarms() {
        return modelAlarms;
    }

    public void setModelAlarms(List<ModelAlarm> modelAlarms) {
        this.modelAlarms = modelAlarms;
    }

    public List<ModelAssort> getModelAssorts() {
        return modelAssorts;
    }

    public void setModelAssorts(List<ModelAssort> modelAssorts) {
        this.modelAssorts = modelAssorts;
    }
}
