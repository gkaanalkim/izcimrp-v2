package com.felislabs.izci.representation.dto;

import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 14/03/2017.
 */
public class AccountingCustomerAccountStatemensDTO {
    private DateTime proceedsDate;
    private String proceedsType;
    private String description;
    private String type;
    private Double debt;
    private Double due;
    private Double balance;

    public DateTime getProceedsDate() {
        return proceedsDate;
    }

    public void setProceedsDate(DateTime proceedsDate) {
        this.proceedsDate = proceedsDate;
    }

    public String getProceedsType() {
        return proceedsType;
    }

    public void setProceedsType(String proceedsType) {
        this.proceedsType = proceedsType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getDebt() {
        return debt;
    }

    public void setDebt(Double debt) {
        this.debt = debt;
    }

    public Double getDue() {
        return due;
    }

    public void setDue(Double due) {
        this.due = due;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
