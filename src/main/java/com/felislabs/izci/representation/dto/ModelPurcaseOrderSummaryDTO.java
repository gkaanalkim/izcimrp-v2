package com.felislabs.izci.representation.dto;

import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 21/06/2017.
 */
public class ModelPurcaseOrderSummaryDTO {
    private String oid;
    private String poNumber;
    private DateTime creationDateTime;

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public DateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(DateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
}
