package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by gunerkaanalkim on 01/06/2017.
 */
public class ModelOrderSummary {
    private Integer orderQuantity;

    @JsonProperty("oq")
    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }
}
