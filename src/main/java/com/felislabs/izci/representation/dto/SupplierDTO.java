package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.SupplyRequirement;

/**
 * Created by dogukanyilmaz on 18/03/2017.
 */
public class SupplierDTO {
    private SupplyRequirement supplyRequirement;
    private Integer onTerminDate;

    public SupplyRequirement getSupplyRequirement() {
        return supplyRequirement;
    }

    public void setSupplyRequirement(SupplyRequirement supplyRequirement) {
        this.supplyRequirement = supplyRequirement;
    }

    public Integer getOnTerminDate() {
        return onTerminDate;
    }

    public void setOnTerminDate(Integer onTerminDate) {
        this.onTerminDate = onTerminDate;
    }
}
