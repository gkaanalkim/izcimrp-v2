package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.CustomerSoleRecipe;
import com.felislabs.izci.representation.entities.FairOrder;

import java.util.List;

/**
 * Created by gunerkaanalkim on 28/04/2017.
 */
public class SoleFairOrderDTO {
    private FairOrder fairOrder;
    private List<CustomerSoleRecipe> customerSoleRecipes;

    public SoleFairOrderDTO(FairOrder fairOrder, List<CustomerSoleRecipe> customerSoleRecipes) {
        this.fairOrder = fairOrder;
        this.customerSoleRecipes = customerSoleRecipes;
    }

    public FairOrder getFairOrder() {
        return fairOrder;
    }

    public void setFairOrder(FairOrder fairOrder) {
        this.fairOrder = fairOrder;
    }

    public List<CustomerSoleRecipe> getCustomerSoleRecipes() {
        return customerSoleRecipes;
    }

    public void setCustomerSoleRecipes(List<CustomerSoleRecipe> customerSoleRecipes) {
        this.customerSoleRecipes = customerSoleRecipes;
    }
}
