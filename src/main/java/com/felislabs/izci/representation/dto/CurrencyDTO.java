package com.felislabs.izci.representation.dto;

/**
 * Created by gunerkaanalkim on 03/11/15.
 */
public class CurrencyDTO {
    private String code;
    private String name;
    private String buying;
    private String selling;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBuying() {
        return buying;
    }

    public void setBuying(String buying) {
        this.buying = buying;
    }

    public String getSelling() {
        return selling;
    }

    public void setSelling(String selling) {
        this.selling = selling;
    }
}
