package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 23/05/2017.
 */
public class SystemActivitySummaryDTO {
    private DateTime dateTime;
    private String userName;
    private String activityDescription;

    public SystemActivitySummaryDTO(DateTime dateTime, String userName, String activityDescription) {
        this.dateTime = dateTime;
        this.userName = userName;
        this.activityDescription = activityDescription;
    }

    @JsonProperty("cdt")
    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }

    @JsonProperty("un")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("ad")
    public String getActivityDescription() {
        return activityDescription;
    }

    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }
}
