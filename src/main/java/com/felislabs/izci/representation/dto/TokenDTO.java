package com.felislabs.izci.representation.dto;

/**
 * Created by gunerkaanalkim on 09/09/15.
 */
public class TokenDTO {
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
