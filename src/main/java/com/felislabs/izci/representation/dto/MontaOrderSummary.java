package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 16/05/2017.
 */
public class MontaOrderSummary {
    private String montaOid;
    private String modelOid;
    private String modelCode;
    private String modelColor;
    private String customer;
    private String customerModelCode;
    private String defaultPhoto;
    private String montaDocumentNumber;
    private String montaDescription;
    private String productionLine;
    private DateTime creationDatetime;
    private DateTime startDate;
    private DateTime endDate;
    private Integer montaOrderQuantity;
    private Integer orderQuantity;
    private Integer montaIncomingQuantity;
    private String selectedAssortUid;
    private String pictogram;
    private Integer year;
    private String season;

    @JsonProperty("mmo")
    public String getMontaOid() {
        return montaOid;
    }

    public void setMontaOid(String montaOid) {
        this.montaOid = montaOid;
    }

    @JsonProperty("dsc")
    public String getMontaDescription() {
        return montaDescription;
    }

    public void setMontaDescription(String montaDescription) {
        this.montaDescription = montaDescription;
    }

    @JsonProperty("sau")
    public String getSelectedAssortUid() {
        return selectedAssortUid;
    }

    public void setSelectedAssortUid(String selectedAssortUid) {
        this.selectedAssortUid = selectedAssortUid;
    }

    @JsonProperty("miq")
    public Integer getMontaIncomingQuantity() {
        return montaIncomingQuantity;
    }

    public void setMontaIncomingQuantity(Integer montaIncomingQuantity) {
        this.montaIncomingQuantity = montaIncomingQuantity;
    }

    @JsonProperty("mo")
    public String getModelOid() {
        return modelOid;
    }

    public void setModelOid(String modelOid) {
        this.modelOid = modelOid;
    }

    @JsonProperty("pl")
    public String getProductionLine() {
        return productionLine;
    }

    public void setProductionLine(String productionLine) {
        this.productionLine = productionLine;
    }

    @JsonProperty("cs")
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @JsonProperty("sd")
    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("ed")
    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("c")
    public String getModelColor() {
        return modelColor;
    }

    public void setModelColor(String modelColor) {
        this.modelColor = modelColor;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("mdn")
    public String getMontaDocumentNumber() {
        return montaDocumentNumber;
    }

    public void setMontaDocumentNumber(String montaDocumentNumber) {
        this.montaDocumentNumber = montaDocumentNumber;
    }

    @JsonProperty("cdt")
    public DateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(DateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("moq")
    public Integer getMontaOrderQuantity() {
        return montaOrderQuantity;
    }

    public void setMontaOrderQuantity(Integer montaOrderQuantity) {
        this.montaOrderQuantity = montaOrderQuantity;
    }

    @JsonProperty("oq")
    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    @JsonProperty("pc")
    public String getPictogram() {
        return pictogram;
    }

    public void setPictogram(String pictogram) {
        this.pictogram = pictogram;
    }


    @JsonProperty("y")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @JsonProperty("s")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }
}
