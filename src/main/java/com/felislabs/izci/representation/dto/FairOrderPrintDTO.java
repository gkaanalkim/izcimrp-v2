package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.CustomerSoleRecipe;
import com.felislabs.izci.representation.entities.FairOrder;
import com.felislabs.izci.representation.entities.FairOrderAssort;

import java.util.List;

/**
 * Created by gunerkaanalkim on 01/04/2017.
 */
public class FairOrderPrintDTO {
    private FairOrder fairOrder;
    private List<FairOrderAssort> fairOrderAssorts;
    private List<CustomerSoleRecipe> customerSoleRecipes;

    public FairOrder getFairOrder() {
        return fairOrder;
    }

    public void setFairOrder(FairOrder fairOrder) {
        this.fairOrder = fairOrder;
    }

    public List<FairOrderAssort> getFairOrderAssorts() {
        return fairOrderAssorts;
    }

    public void setFairOrderAssorts(List<FairOrderAssort> fairOrderAssorts) {
        this.fairOrderAssorts = fairOrderAssorts;
    }

    public List<CustomerSoleRecipe> getCustomerSoleRecipes() {
        return customerSoleRecipes;
    }

    public void setCustomerSoleRecipes(List<CustomerSoleRecipe> customerSoleRecipes) {
        this.customerSoleRecipes = customerSoleRecipes;
    }
}
