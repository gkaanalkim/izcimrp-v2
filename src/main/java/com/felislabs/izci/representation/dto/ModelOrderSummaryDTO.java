package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 23/05/2017.
 */
public class ModelOrderSummaryDTO {
    private String defaultPhoto;
    private DateTime creationDateTime;
    private DateTime terminDate;
    private String customerName;
    private String modelCode;
    private String customerModelCode;
    private Integer orderQuantity;

    public ModelOrderSummaryDTO(String defaultPhoto, DateTime creationDateTime, DateTime terminDate, String customerName, String modelCode, String customerModelCode, Integer orderQuantity) {
        this.defaultPhoto = defaultPhoto;
        this.creationDateTime = creationDateTime;
        this.terminDate = terminDate;
        this.customerName = customerName;
        this.modelCode = modelCode;
        this.customerModelCode = customerModelCode;
        this.orderQuantity = orderQuantity;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("cdt")
    public DateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(DateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @JsonProperty("td")
    public DateTime getTerminDate() {
        return terminDate;
    }

    public void setTerminDate(DateTime terminDate) {
        this.terminDate = terminDate;
    }

    @JsonProperty("cn")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("oq")
    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }
}
