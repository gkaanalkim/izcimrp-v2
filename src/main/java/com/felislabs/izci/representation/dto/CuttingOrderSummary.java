package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 16/05/2017.
 */
public class CuttingOrderSummary {
    private String oid;
    private String modelOid;
    private String modelCode;
    private String modelColor;
    private String customerModelCode;
    private String defaultPhoto;
    private String cuttingDocumentNumber;
    private DateTime creationDatetime;
    private DateTime startDate;
    private DateTime endDate;
    private Integer cuttingOrderQuantity;
    private Integer orderQuantity;
    private Integer incomingQuantity;
    private String staffName;
    private String customerName;
    private String selectedAssortUid;
    private String description;
    private String season;
    private Integer year;

    @JsonProperty("iq")
    public Integer getIncomingQuantity() {
        return incomingQuantity;
    }

    public void setIncomingQuantity(Integer incomingQuantity) {
        this.incomingQuantity = incomingQuantity;
    }

    @JsonProperty("dsc")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("sau")
    public String getSelectedAssortUid() {
        return selectedAssortUid;
    }

    public void setSelectedAssortUid(String selectedAssortUid) {
        this.selectedAssortUid = selectedAssortUid;
    }

    @JsonProperty("mo")
    public String getModelOid() {
        return modelOid;
    }

    public void setModelOid(String modelOid) {
        this.modelOid = modelOid;
    }

    @JsonProperty("cs")
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @JsonProperty("sd")
    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("ed")
    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    @JsonProperty("o")
    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @JsonProperty("stn")
    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("c")
    public String getModelColor() {
        return modelColor;
    }

    public void setModelColor(String modelColor) {
        this.modelColor = modelColor;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("cdt")
    public DateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(DateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("oq")
    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    @JsonProperty("cdn")
    public String getCuttingDocumentNumber() {
        return cuttingDocumentNumber;
    }

    public void setCuttingDocumentNumber(String cuttingDocumentNumber) {
        this.cuttingDocumentNumber = cuttingDocumentNumber;
    }

    @JsonProperty("coq")
    public Integer getCuttingOrderQuantity() {
        return cuttingOrderQuantity;
    }

    public void setCuttingOrderQuantity(Integer cuttingOrderQuantity) {
        this.cuttingOrderQuantity = cuttingOrderQuantity;
    }

    @JsonProperty("s")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @JsonProperty("y")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
