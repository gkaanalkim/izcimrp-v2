package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.ModelAssort;
import com.felislabs.izci.representation.entities.ModelMaterial;
import com.felislabs.izci.representation.entities.ModelOrder;

import java.util.List;

/**
 * Created by gunerkaanalkim on 09/02/2017.
 */
public class BulkSupplyDTOForForm {
    private ModelMaterial modelMaterial;
    private ModelOrder modelOrder;
    private List<ModelAssort> modelAssorts;

    public BulkSupplyDTOForForm(ModelMaterial modelMaterial, ModelOrder modelOrder, List<ModelAssort> modelAssorts) {
        this.modelMaterial = modelMaterial;
        this.modelOrder = modelOrder;
        this.modelAssorts = modelAssorts;
    }

    public ModelMaterial getModelMaterial() {
        return modelMaterial;
    }

    public void setModelMaterial(ModelMaterial modelMaterial) {
        this.modelMaterial = modelMaterial;
    }

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }

    public List<ModelAssort> getModelAssorts() {
        return modelAssorts;
    }

    public void setModelAssorts(List<ModelAssort> modelAssorts) {
        this.modelAssorts = modelAssorts;
    }
}
