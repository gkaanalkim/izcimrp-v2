package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.*;

import java.util.List;

/**
 * Created by gunerkaanalkim on 24/01/2017.
 */
public class ModelDTO {
    private Model model;
    private ModelOrder modelOrder;
    private List<ModelMaterial> modelMaterials;
    private List<ModelAssort> modelAssorts;
    private List<ModelInternalCritical> modelInternalCriticals;
    private List<ModelExternalCritical> modelExternalCriticals;

    public List<ModelInternalCritical> getModelInternalCriticals() {
        return modelInternalCriticals;
    }

    public void setModelInternalCriticals(List<ModelInternalCritical> modelInternalCriticals) {
        this.modelInternalCriticals = modelInternalCriticals;
    }

    public List<ModelExternalCritical> getModelExternalCriticals() {
        return modelExternalCriticals;
    }

    public void setModelExternalCriticals(List<ModelExternalCritical> modelExternalCriticals) {
        this.modelExternalCriticals = modelExternalCriticals;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public List<ModelMaterial> getModelMaterials() {
        return modelMaterials;
    }

    public void setModelMaterials(List<ModelMaterial> modelMaterials) {
        this.modelMaterials = modelMaterials;
    }

    public List<ModelAssort> getModelAssorts() {
        return modelAssorts;
    }

    public void setModelAssorts(List<ModelAssort> modelAssorts) {
        this.modelAssorts = modelAssorts;
    }

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }
}

