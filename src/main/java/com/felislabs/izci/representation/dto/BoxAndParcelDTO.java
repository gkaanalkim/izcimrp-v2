package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by gunerkaanalkim on 21/04/2017.
 */
public class BoxAndParcelDTO {
    private String boxCode;
    private String parcelCode;
    private String baseCode;

    @JsonProperty("bc")
    public String getBoxCode() {
        return boxCode;
    }

    public void setBoxCode(String boxCode) {
        this.boxCode = boxCode;
    }

    @JsonProperty("pc")
    public String getParcelCode() {
        return parcelCode;
    }

    public void setParcelCode(String parcelCode) {
        this.parcelCode = parcelCode;
    }

    @JsonProperty("bs")
    public String getBaseCode() {
        return baseCode;
    }

    public void setBaseCode(String baseCode) {
        this.baseCode = baseCode;
    }
}
