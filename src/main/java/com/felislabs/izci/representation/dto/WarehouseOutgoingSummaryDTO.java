package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by gunerkaanalkim on 25/06/2017.
 */
public class WarehouseOutgoingSummaryDTO {
    private String oid;
    private String supplierName;
    private String stockCode;
    private String stockName;
    private String stockDetailName;
    private Double quantity;
    private String modelCode;
    private String customerModelCode;

    public WarehouseOutgoingSummaryDTO(String oid, String supplierName, String stockCode, String stockName, String stockDetailName, Double quantity, String modelCode, String customerModelCode) {
        this.oid = oid;
        this.supplierName = supplierName;
        this.stockCode = stockCode;
        this.stockName = stockName;
        this.stockDetailName = stockDetailName;
        this.quantity = quantity;
        this.modelCode = modelCode;
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("spn")
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @JsonProperty("o")
    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @JsonProperty("sc")
    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    @JsonProperty("sn")
    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    @JsonProperty("sdn")
    public String getStockDetailName() {
        return stockDetailName;
    }

    public void setStockDetailName(String stockDetailName) {
        this.stockDetailName = stockDetailName;
    }

    @JsonProperty("q")
    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }
}
