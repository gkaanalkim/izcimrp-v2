package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelOrder;
import com.felislabs.izci.representation.entities.Stock;

/**
 * Created by arricie on 23.02.2017.
 */
public class StockRequirementDTO {
    private Model model;
    private Stock stock;
    private ModelOrder modelOrder;
    private Double quantity;

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
