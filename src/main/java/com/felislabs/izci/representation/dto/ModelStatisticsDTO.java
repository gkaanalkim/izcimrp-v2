package com.felislabs.izci.representation.dto;

/**
 * Created by gunerkaanalkim on 09/02/2017.
 */
public class ModelStatisticsDTO {
    private Integer totalModel;
    private Integer confirmedModel;
    private Integer notConfirmedModel;
    private Integer totalOrder;
    private Integer orderPairSum;

    public Integer getOrderPairSum() {
        return orderPairSum;
    }

    public void setOrderPairSum(Integer orderPairSum) {
        this.orderPairSum = orderPairSum;
    }

    public Integer getTotalOrder() {
        return totalOrder;
    }

    public void setTotalOrder(Integer totalOrder) {
        this.totalOrder = totalOrder;
    }

    public Integer getTotalModel() {
        return totalModel;
    }

    public void setTotalModel(Integer totalModel) {
        this.totalModel = totalModel;
    }

    public Integer getConfirmedModel() {
        return confirmedModel;
    }

    public void setConfirmedModel(Integer confirmedModel) {
        this.confirmedModel = confirmedModel;
    }

    public Integer getNotConfirmedModel() {
        return notConfirmedModel;
    }

    public void setNotConfirmedModel(Integer notConfirmedModel) {
        this.notConfirmedModel = notConfirmedModel;
    }
}
