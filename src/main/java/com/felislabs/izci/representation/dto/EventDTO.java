package com.felislabs.izci.representation.dto;

import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 06/02/2017.
 */
public class EventDTO {
    private String oid;
    private DateTime start;
    private DateTime end;
    private String title;

    public EventDTO(String oid, DateTime start, DateTime end, String title) {
        this.oid = oid;
        this.start = start;
        this.end = end;
        this.title = title;
    }

    public DateTime getEnd() {
        return end;
    }

    public void setEnd(DateTime end) {
        this.end = end;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public DateTime getStart() {
        return start;
    }

    public void setStart(DateTime start) {
        this.start = start;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
