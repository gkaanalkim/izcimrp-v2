package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.ModelAssort;
import com.felislabs.izci.representation.entities.ModelMaterial;
import com.felislabs.izci.representation.entities.ModelOrder;

import java.util.List;

/**
 * Created by gunerkaanalkim on 31/01/2017.
 */
public class BulkSupplyDTO {
    private ModelOrder modelOrder;
    private List<ModelMaterial> modelMaterials;
    private List<ModelAssort> modelAssorts;

    public List<ModelAssort> getModelAssorts() {
        return modelAssorts;
    }

    public void setModelAssorts(List<ModelAssort> modelAssorts) {
        this.modelAssorts = modelAssorts;
    }

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }

    public List<ModelMaterial> getModelMaterials() {
        return modelMaterials;
    }

    public void setModelMaterials(List<ModelMaterial> modelMaterials) {
        this.modelMaterials = modelMaterials;
    }
}
