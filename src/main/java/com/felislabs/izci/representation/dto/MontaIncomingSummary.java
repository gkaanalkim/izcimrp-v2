package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 16/05/2017.
 */
public class MontaIncomingSummary {
    private String montaIncomingOid;
    private String modelCode;
    private String modelColor;
    private String customerModelCode;
    private String customer;
    private String defaultPhoto;
    private String montaDocumentNumber;
    private String productionLine;
    private DateTime creationDatetime;
    private Integer montaIncomingQuantity;
    private Integer montaOrderQuantity;
    private Integer orderQuantity;
    private String extraAttribute;
    private Integer year;
    private String season;

    @JsonProperty("mio")
    public String getMontaIncomingOid() {
        return montaIncomingOid;
    }

    public void setMontaIncomingOid(String montaIncomingOid) {
        this.montaIncomingOid = montaIncomingOid;
    }

    @JsonProperty("moq")
    public Integer getMontaOrderQuantity() {
        return montaOrderQuantity;
    }

    public void setMontaOrderQuantity(Integer montaOrderQuantity) {
        this.montaOrderQuantity = montaOrderQuantity;
    }

    @JsonProperty("pl")
    public String getProductionLine() {
        return productionLine;
    }

    public void setProductionLine(String productionLine) {
        this.productionLine = productionLine;
    }

    @JsonProperty("cs")
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("c")
    public String getModelColor() {
        return modelColor;
    }

    public void setModelColor(String modelColor) {
        this.modelColor = modelColor;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("mdn")
    public String getMontaDocumentNumber() {
        return montaDocumentNumber;
    }

    public void setMontaDocumentNumber(String montaDocumentNumber) {
        this.montaDocumentNumber = montaDocumentNumber;
    }

    @JsonProperty("cdt")
    public DateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(DateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("miq")
    public Integer getMontaIncomingQuantity() {
        return montaIncomingQuantity;
    }

    public void setMontaIncomingQuantity(Integer montaIncomingQuantity) {
        this.montaIncomingQuantity = montaIncomingQuantity;
    }

    @JsonProperty("oq")
    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    @JsonProperty("ea")
    public String getExtraAttribute() {
        return extraAttribute;
    }

    public void setExtraAttribute(String extraAttribute) {
        this.extraAttribute = extraAttribute;
    }


    @JsonProperty("y")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    @JsonProperty("s")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }
}
