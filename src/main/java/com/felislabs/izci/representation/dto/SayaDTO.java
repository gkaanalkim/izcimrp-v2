package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.ModelAssort;
import com.felislabs.izci.representation.entities.ModelOrder;
import com.felislabs.izci.representation.entities.ModelSaya;

import java.util.List;

/**
 * Created by gunerkaanalkim on 01/02/2017.
 */
public class SayaDTO {
    private ModelSaya modelSaya;
    private List<ModelAssort> modelAssorts;

    public ModelSaya getModelSaya() {
        return modelSaya;
    }

    public void setModelSaya(ModelSaya modelSaya) {
        this.modelSaya = modelSaya;
    }

    public List<ModelAssort> getModelAssorts() {
        return modelAssorts;
    }

    public void setModelAssorts(List<ModelAssort> modelAssorts) {
        this.modelAssorts = modelAssorts;
    }
}
