package com.felislabs.izci.representation.dto;

/**
 * Created by dogukanyilmaz on 23/03/2017.
 */
public class ModelNameGenaratorDTO {
    private String modelCode;
    private String oid;

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }
}
