package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.FairOrder;

/**
 * Created by gunerkaanalkim on 23/03/2017.
 */
public class FairOrderDTO {
    private FairOrder fairOrder;
    private Integer quantity;
    private Integer totalQuantity;
    private Double totalAmount;
    private String currency;

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public FairOrder getFairOrder() {
        return fairOrder;
    }

    public void setFairOrder(FairOrder fairOrder) {
        this.fairOrder = fairOrder;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
