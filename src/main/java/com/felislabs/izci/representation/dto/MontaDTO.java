package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.*;

import java.util.List;

/**
 * Created by gunerkaanalkim on 04/02/2017.
 */
public class MontaDTO {
    private ModelMonta modelMonta;
    private List<ModelAssort> modelAssorts;
    private List<ModelAlarm> modelAlarms;
    private List<ModelInternalCritical> modelInternalCriticals;
    private List<ModelExternalCritical> modelExternalCriticals;

    public MontaDTO(ModelMonta modelMonta, List<ModelAssort> modelAssorts, List<ModelAlarm> modelAlarms, List<ModelInternalCritical> modelInternalCriticals, List<ModelExternalCritical> modelExternalCriticals) {
        this.modelMonta = modelMonta;
        this.modelAssorts = modelAssorts;
        this.modelAlarms = modelAlarms;
        this.modelInternalCriticals = modelInternalCriticals;
        this.modelExternalCriticals = modelExternalCriticals;
    }

    public ModelMonta getModelMonta() {
        return modelMonta;
    }

    public void setModelMonta(ModelMonta modelMonta) {
        this.modelMonta = modelMonta;
    }

    public List<ModelAssort> getModelAssorts() {
        return modelAssorts;
    }

    public void setModelAssorts(List<ModelAssort> modelAssorts) {
        this.modelAssorts = modelAssorts;
    }

    public List<ModelAlarm> getModelAlarms() {
        return modelAlarms;
    }

    public void setModelAlarms(List<ModelAlarm> modelAlarms) {
        this.modelAlarms = modelAlarms;
    }

    public List<ModelInternalCritical> getModelInternalCriticals() {
        return modelInternalCriticals;
    }

    public void setModelInternalCriticals(List<ModelInternalCritical> modelInternalCriticals) {
        this.modelInternalCriticals = modelInternalCriticals;
    }

    public List<ModelExternalCritical> getModelExternalCriticals() {
        return modelExternalCriticals;
    }

    public void setModelExternalCriticals(List<ModelExternalCritical> modelExternalCriticals) {
        this.modelExternalCriticals = modelExternalCriticals;
    }
}
