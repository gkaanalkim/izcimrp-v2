package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.Bid;
import com.felislabs.izci.representation.entities.BidDetail;

import java.util.List;

/**
 * Created by gunerkaanalkim on 13/03/2017.
 */
public class BidDTO {
    private Bid bid;
    private List<BidDetail> bidDetails;

    public Bid getBid() {
        return bid;
    }

    public void setBid(Bid bid) {
        this.bid = bid;
    }

    public List<BidDetail> getBidDetails() {
        return bidDetails;
    }

    public void setBidDetails(List<BidDetail> bidDetails) {
        this.bidDetails = bidDetails;
    }
}
