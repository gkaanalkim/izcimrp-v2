package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

public class ShipmentOrderSummaryDTO {
    private String oid;
    private String defaultPhoto;
    private String customer;
    private String modelOid;
    private String modelCode;
    private String modelColor;
    private String customerModelCode;
    private String parcelType;
    private String parcelQuantity;
    private String warehouseShipTo;
    private DateTime shipmentDate;

    public ShipmentOrderSummaryDTO(String oid, String defaultPhoto, String customer, String modelOid, String modelCode, String modelColor, String customerModelCode, String parcelType, String parcelQuantity, String warehouseShipTo, DateTime shipmentDate) {
        this.oid = oid;
        this.defaultPhoto = defaultPhoto;
        this.customer = customer;
        this.modelOid = modelOid;
        this.modelCode = modelCode;
        this.modelColor = modelColor;
        this.customerModelCode = customerModelCode;
        this.parcelType = parcelType;
        this.parcelQuantity = parcelQuantity;
        this.warehouseShipTo = warehouseShipTo;
        this.shipmentDate = shipmentDate;
    }

    @JsonProperty("o")
    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("mo")
    public String getModelOid() {
        return modelOid;
    }

    public void setModelOid(String modelOid) {
        this.modelOid = modelOid;
    }

    @JsonProperty("pt")
    public String getParcelType() {
        return parcelType;
    }

    public void setParcelType(String parcelType) {
        this.parcelType = parcelType;
    }

    @JsonProperty("pq")
    public String getParcelQuantity() {
        return parcelQuantity;
    }

    public void setParcelQuantity(String parcelQuantity) {
        this.parcelQuantity = parcelQuantity;
    }

    @JsonProperty("wht")
    public String getWarehouseShipTo() {
        return warehouseShipTo;
    }

    public void setWarehouseShipTo(String warehouseShipTo) {
        this.warehouseShipTo = warehouseShipTo;
    }

    @JsonProperty("sd")
    public DateTime getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(DateTime shipmentDate) {
        this.shipmentDate = shipmentDate;
    }

    @JsonProperty("c")
    public String getModelColor() {
        return modelColor;
    }

    public void setModelColor(String modelColor) {
        this.modelColor = modelColor;
    }

    @JsonProperty("cs")
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }
}
