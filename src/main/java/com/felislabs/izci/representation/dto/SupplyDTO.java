package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.Model;
import com.felislabs.izci.representation.entities.ModelMaterial;
import com.felislabs.izci.representation.entities.ModelOrder;

import java.util.List;

/**
 * Created by gunerkaanalkim on 29/01/2017.
 */
public class SupplyDTO {
    private ModelOrder modelOrder;
    private List<ModelMaterial> modelMaterials;

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }

    public List<ModelMaterial> getModelMaterials() {
        return modelMaterials;
    }

    public void setModelMaterials(List<ModelMaterial> modelMaterials) {
        this.modelMaterials = modelMaterials;
    }
}
