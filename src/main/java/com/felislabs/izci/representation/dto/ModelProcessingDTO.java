package com.felislabs.izci.representation.dto;

import com.felislabs.izci.representation.entities.ModelAssort;
import com.felislabs.izci.representation.entities.ModelProcessing;

import java.util.List;

/**
 * Created by arricie on 21.03.2017.
 */
public class ModelProcessingDTO {
    private ModelProcessing modelProcessing;
    private List<ModelAssort> modelAssorts;

    public ModelProcessing getModelProcessing() {
        return modelProcessing;
    }

    public void setModelProcessing(ModelProcessing modelProcessing) {
        this.modelProcessing = modelProcessing;
    }

    public List<ModelAssort> getModelAssorts() {
        return modelAssorts;
    }

    public void setModelAssorts(List<ModelAssort> modelAssorts) {
        this.modelAssorts = modelAssorts;
    }
}
