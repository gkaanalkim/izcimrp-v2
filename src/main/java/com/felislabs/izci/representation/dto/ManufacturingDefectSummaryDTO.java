package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by gunerkaanalkim on 25/06/2017.
 */
public class ManufacturingDefectSummaryDTO {
    private String oid;
    private String defaultPhoto;
    private String montaDocumentNumber;
    private String modelCode;
    private String customerModelCode;
    private String number;
    private String quantity;
    private String sayaDefect;
    private String montaDefect;

    public ManufacturingDefectSummaryDTO(String oid, String defaultPhoto, String montaDocumentNumber, String modelCode, String customerModelCode, String number, String quantity, String sayaDefect, String montaDefect) {
        this.oid = oid;
        this.defaultPhoto = defaultPhoto;
        this.montaDocumentNumber = montaDocumentNumber;
        this.modelCode = modelCode;
        this.customerModelCode = customerModelCode;
        this.number = number;
        this.quantity = quantity;
        this.sayaDefect = sayaDefect;
        this.montaDefect = montaDefect;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("o")
    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @JsonProperty("mdn")
    public String getMontaDocumentNumber() {
        return montaDocumentNumber;
    }

    public void setMontaDocumentNumber(String montaDocumentNumber) {
        this.montaDocumentNumber = montaDocumentNumber;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("n")
    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    @JsonProperty("q")
    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("sd")
    public String getSayaDefect() {
        return sayaDefect;
    }

    public void setSayaDefect(String sayaDefect) {
        this.sayaDefect = sayaDefect;
    }

    @JsonProperty("md")
    public String getMontaDefect() {
        return montaDefect;
    }

    public void setMontaDefect(String montaDefect) {
        this.montaDefect = montaDefect;
    }
}
