package com.felislabs.izci.representation.dto;

/**
 * Created by gunerkaanalkim on 20/10/2016.
 */
public class WorkOrderImageDTO {
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
