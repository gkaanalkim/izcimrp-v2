package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 25/06/2017.
 */
public class PurcaseOrderCompareSummaryDTO {
    private DateTime poDate;
    private String poNumber;
    private String supplierName;
    private String stockName;
    private String stockCode;
    private String stockDetailName;
    private Double calculatedRequirement;
    private Double requirement;
    private Double incomingQuantity;
    private Double remainder;
    private Double surplus;

    public PurcaseOrderCompareSummaryDTO(DateTime poDate, String poNumber, String supplierName, String stockName, String stockCode, String stockDetailName, Double calculatedRequirement, Double requirement, Double incomingQuantity, Double remainder, Double surplus) {
        this.poDate = poDate;
        this.poNumber = poNumber;
        this.supplierName = supplierName;
        this.stockName = stockName;
        this.stockCode = stockCode;
        this.stockDetailName = stockDetailName;
        this.calculatedRequirement = calculatedRequirement;
        this.requirement = requirement;
        this.incomingQuantity = incomingQuantity;
        this.remainder = remainder;
        this.surplus = surplus;
    }

    @JsonProperty("pod")
    public DateTime getPoDate() {
        return poDate;
    }

    public void setPoDate(DateTime poDate) {
        this.poDate = poDate;
    }

    @JsonProperty("rm")
    public Double getRemainder() {
        return remainder;
    }

    public void setRemainder(Double remainder) {
        this.remainder = remainder;
    }

    @JsonProperty("pon")
    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    @JsonProperty("sp")
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @JsonProperty("n")
    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    @JsonProperty("c")
    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    @JsonProperty("sdn")
    public String getStockDetailName() {
        return stockDetailName;
    }

    public void setStockDetailName(String stockDetailName) {
        this.stockDetailName = stockDetailName;
    }

    @JsonProperty("cr")
    public Double getCalculatedRequirement() {
        return calculatedRequirement;
    }

    public void setCalculatedRequirement(Double calculatedRequirement) {
        this.calculatedRequirement = calculatedRequirement;
    }

    @JsonProperty("r")
    public Double getRequirement() {
        return requirement;
    }

    public void setRequirement(Double requirement) {
        this.requirement = requirement;
    }

    @JsonProperty("iq")
    public Double getIncomingQuantity() {
        return incomingQuantity;
    }

    public void setIncomingQuantity(Double incomingQuantity) {
        this.incomingQuantity = incomingQuantity;
    }

    @JsonProperty("srp")
    public Double getSurplus() {
        return surplus;
    }

    public void setSurplus(Double surplus) {
        this.surplus = surplus;
    }
}
