package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 23/05/2017.
 */
public class StockAcceptanceDTO {
    private Double quantity;
    private String unit;
    private String modelCode;
    private String customerModelCode;
    private String modelColor;
    private String stockCode;
    private String stockName;
    private String stockDetailName;
    private String stockDetailDescription;
    private String userName;
    private DateTime creationDateTime;

    public StockAcceptanceDTO(Double quantity, String unit, String modelCode, String customerModelCode, String modelColor, String stockCode, String stockName, String stockDetailName, String stockDetailDescription, String userName, DateTime creationDateTime) {
        this.quantity = quantity;
        this.unit = unit;
        this.modelCode = modelCode;
        this.customerModelCode = customerModelCode;
        this.modelColor = modelColor;
        this.stockCode = stockCode;
        this.stockName = stockName;
        this.stockDetailName = stockDetailName;
        this.stockDetailDescription = stockDetailDescription;
        this.userName = userName;
        this.creationDateTime = creationDateTime;
    }

    @JsonProperty("q")
    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("un")
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    @JsonProperty("c")
    public String getModelColor() {
        return modelColor;
    }

    public void setModelColor(String modelColor) {
        this.modelColor = modelColor;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("sc")
    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    @JsonProperty("sn")
    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    @JsonProperty("sdn")
    public String getStockDetailName() {
        return stockDetailName;
    }

    public void setStockDetailName(String stockDetailName) {
        this.stockDetailName = stockDetailName;
    }

    @JsonProperty("sdd")
    public String getStockDetailDescription() {
        return stockDetailDescription;
    }

    public void setStockDetailDescription(String stockDetailDescription) {
        this.stockDetailDescription = stockDetailDescription;
    }

    @JsonProperty("u")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @JsonProperty("cdt")
    public DateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(DateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }
}
