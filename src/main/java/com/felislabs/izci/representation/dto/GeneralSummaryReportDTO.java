package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 10/07/2017.
 */
public class GeneralSummaryReportDTO {
    private DateTime creationDateTime;
    private DateTime terminDate;
    private String customer;
    private String defaultPhoto;
    private String modelCode;
    private String customerModelCode;
    private String color;
    private Integer orderQuantity;
    private Integer cuttingQuantity;
    private Integer sayaQuantity;
    private Integer montaQuantity;
    private String season;
    private Integer year;

    public GeneralSummaryReportDTO(DateTime creationDateTime, DateTime terminDate, String customer, String defaultPhoto, String modelCode, String customerModelCode, String color, Integer orderQuantity, Integer cuttingQuantity, Integer sayaQuantity, Integer montaQuantity, String season, Integer year) {
        this.creationDateTime = creationDateTime;
        this.terminDate = terminDate;
        this.customer = customer;
        this.defaultPhoto = defaultPhoto;
        this.modelCode = modelCode;
        this.customerModelCode = customerModelCode;
        this.color = color;
        this.orderQuantity = orderQuantity;
        this.cuttingQuantity = cuttingQuantity;
        this.sayaQuantity = sayaQuantity;
        this.montaQuantity = montaQuantity;
        this.season = season;
        this.year = year;
    }

    @JsonProperty("cs")
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @JsonProperty("cdt")
    public DateTime getCreationDateTime() {
        return creationDateTime;
    }

    public void setCreationDateTime(DateTime creationDateTime) {
        this.creationDateTime = creationDateTime;
    }

    @JsonProperty("td")
    public DateTime getTerminDate() {
        return terminDate;
    }

    public void setTerminDate(DateTime terminDate) {
        this.terminDate = terminDate;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("oq")
    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    @JsonProperty("coq")
    public Integer getCuttingQuantity() {
        return cuttingQuantity;
    }

    public void setCuttingQuantity(Integer cuttingQuantity) {
        this.cuttingQuantity = cuttingQuantity;
    }

    @JsonProperty("soq")
    public Integer getSayaQuantity() {
        return sayaQuantity;
    }

    public void setSayaQuantity(Integer sayaQuantity) {
        this.sayaQuantity = sayaQuantity;
    }

    @JsonProperty("moq")
    public Integer getMontaQuantity() {
        return montaQuantity;
    }

    public void setMontaQuantity(Integer montaQuantity) {
        this.montaQuantity = montaQuantity;
    }

    @JsonProperty("c")
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @JsonProperty("s")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @JsonProperty("y")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
