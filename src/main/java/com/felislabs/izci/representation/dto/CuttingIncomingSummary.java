package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 16/05/2017.
 */
public class CuttingIncomingSummary {
    private String oid;
    private String modelCode;
    private String modelColor;
    private String customerModelCode;
    private String defaultPhoto;
    private String cuttingDocumentNumber;
    private String customer;
    private String staffName;
    private DateTime creationDatetime;
    private Integer cuttingIncomingQuantity;
    private Integer orderQuantity;
    private String season;
    private Integer year;

    public CuttingIncomingSummary() {
    }

    public CuttingIncomingSummary(String oid, String modelCode, String modelColor, String customerModelCode, String defaultPhoto, String cuttingDocumentNumber, String customer, String staffName, DateTime creationDatetime, Integer cuttingIncomingQuantity, Integer orderQuantity) {
        this.oid = oid;
        this.modelCode = modelCode;
        this.modelColor = modelColor;
        this.customerModelCode = customerModelCode;
        this.defaultPhoto = defaultPhoto;
        this.cuttingDocumentNumber = cuttingDocumentNumber;
        this.customer = customer;
        this.staffName = staffName;
        this.creationDatetime = creationDatetime;
        this.cuttingIncomingQuantity = cuttingIncomingQuantity;
        this.orderQuantity = orderQuantity;
    }

    @JsonProperty("stn")
    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    @JsonProperty("o")
    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    @JsonProperty("cs")
    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @JsonProperty("mc")
    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    @JsonProperty("c")
    public String getModelColor() {
        return modelColor;
    }

    public void setModelColor(String modelColor) {
        this.modelColor = modelColor;
    }

    @JsonProperty("cmc")
    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    @JsonProperty("cdt")
    public DateTime getCreationDatetime() {
        return creationDatetime;
    }

    public void setCreationDatetime(DateTime creationDatetime) {
        this.creationDatetime = creationDatetime;
    }

    @JsonProperty("dp")
    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    @JsonProperty("oq")
    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    @JsonProperty("cdn")
    public String getCuttingDocumentNumber() {
        return cuttingDocumentNumber;
    }

    public void setCuttingDocumentNumber(String cuttingDocumentNumber) {
        this.cuttingDocumentNumber = cuttingDocumentNumber;
    }

    @JsonProperty("ciq")
    public Integer getCuttingIncomingQuantity() {
        return cuttingIncomingQuantity;
    }

    public void setCuttingIncomingQuantity(Integer cuttingIncomingQuantity) {
        this.cuttingIncomingQuantity = cuttingIncomingQuantity;
    }


    @JsonProperty("s")
    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    @JsonProperty("y")
    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
