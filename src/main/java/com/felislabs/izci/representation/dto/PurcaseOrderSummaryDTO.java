package com.felislabs.izci.representation.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.joda.time.DateTime;

/**
 * Created by gunerkaanalkim on 21/06/2017.
 */
public class PurcaseOrderSummaryDTO {
    private DateTime terminDate;
    private String stockName;
    private String stockDetailName;
    private String supplierName;
    private String stockDetailText;
    private String stockType;
    private Double requirement;
    private Double calculatedRequirement;

    public PurcaseOrderSummaryDTO(DateTime terminDate, String stockName, String stockDetailName, String supplierName, String stockDetailText, String stockType, Double requirement, Double calculatedRequirement) {
        this.terminDate = terminDate;
        this.stockName = stockName;
        this.stockDetailName = stockDetailName;
        this.supplierName = supplierName;
        this.stockDetailText = stockDetailText;
        this.stockType = stockType;
        this.requirement = requirement;
        this.calculatedRequirement = calculatedRequirement;
    }

    @JsonProperty("td")
    public DateTime getTerminDate() {
        return terminDate;
    }

    public void setTerminDate(DateTime terminDate) {
        this.terminDate = terminDate;
    }

    @JsonProperty("sn")
    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    @JsonProperty("sdn")
    public String getStockDetailName() {
        return stockDetailName;
    }

    public void setStockDetailName(String stockDetailName) {
        this.stockDetailName = stockDetailName;
    }

    @JsonProperty("spn")
    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    @JsonProperty("sdt")
    public String getStockDetailText() {
        return stockDetailText;
    }

    public void setStockDetailText(String stockDetailText) {
        this.stockDetailText = stockDetailText;
    }

    @JsonProperty("st")
    public String getStockType() {
        return stockType;
    }

    public void setStockType(String stockType) {
        this.stockType = stockType;
    }

    @JsonProperty("r")
    public Double getRequirement() {
        return requirement;
    }

    public void setRequirement(Double requirement) {
        this.requirement = requirement;
    }

    @JsonProperty("cr")
    public Double getCalculatedRequirement() {
        return calculatedRequirement;
    }

    public void setCalculatedRequirement(Double calculatedRequirement) {
        this.calculatedRequirement = calculatedRequirement;
    }
}
