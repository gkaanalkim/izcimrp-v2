package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import java.util.Date;

/**
 * Created by gunerkaanalkim on 07/12/15.
 */
@Entity
public class Company extends BaseEntity {
    @Type(type = "text")
    private String bankInfo;
    private String name;
    private String code;
    private DateTime licenseStartDate;
    private String language;
    private String logoUrl;
    @Type(type = "text")
    private String address;
    private String phone;
    private String fax;
    private String email;
    private String website;
    private String facebook;
    private String twitter;
    private String instagram;
    private String imageFolder;

    public String getImageFolder() {
        return imageFolder;
    }

    public void setImageFolder(String imageFolder) {
        this.imageFolder = imageFolder;
    }

    @Type(type = "text")
    private String orderSpecialNotes;

    public String getOrderSpecialNotes() {
        return orderSpecialNotes;
    }

    public void setOrderSpecialNotes(String orderSpecialNotes) {
        this.orderSpecialNotes = orderSpecialNotes;
    }

    public DateTime getLicenseStartDate() {
        return licenseStartDate;
    }

    public void setLicenseStartDate(DateTime licenseStartDate) {
        this.licenseStartDate = licenseStartDate;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getBankInfo() {
        return bankInfo;
    }

    public void setBankInfo(String bankInfo) {
        this.bankInfo = bankInfo;
    }
}
