package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 02/02/2017.
 */
@Entity
public class ModelMonta extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User updater;
    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "modelOrderOid", referencedColumnName = "oid")
    private ModelOrder modelOrder;

    @ManyToOne
    @JoinColumn(name = "productionLineOid", referencedColumnName = "oid")
    private ProductionLine productionLine;

    private String documentNumber;

    private DateTime formalStart;
    private DateTime formalEnd;

    private Integer quantity;
    private String type;

    @Type(type = "text")
    private String description;

    private String shoesLace;
    private String pictogram="65";
    private String article;

    private String selectedAssortUid;

    public String getShoesLace() {
        return shoesLace;
    }

    public void setShoesLace(String shoesLace) {
        this.shoesLace = shoesLace;
    }

    public String getPictogram() {
        return pictogram;
    }

    public void setPictogram(String pictogram) {
        this.pictogram = pictogram;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getSelectedAssortUid() {
        return selectedAssortUid;
    }

    public void setSelectedAssortUid(String selectedAssortUid) {
        this.selectedAssortUid = selectedAssortUid;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }

    public ProductionLine getProductionLine() {
        return productionLine;
    }

    public void setProductionLine(ProductionLine productionLine) {
        this.productionLine = productionLine;
    }

    public DateTime getFormalStart() {
        return formalStart;
    }

    public void setFormalStart(DateTime formalStart) {
        this.formalStart = formalStart;
    }

    public DateTime getFormalEnd() {
        return formalEnd;
    }

    public void setFormalEnd(DateTime formalEnd) {
        this.formalEnd = formalEnd;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
