package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 14/03/2017.
 */
@Entity
public class Proceeds extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "accountingCustomerOid", referencedColumnName = "oid")
    private AccountingCustomer accountingCustomer;

    @ManyToOne
    @JoinColumn(name = "accountOid", referencedColumnName = "oid")
    private Account account;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User user;
    private DateTime updateDate;

    private String type;
    private String proceedsType;
    private Double amount;
    private DateTime proceedsDate;

    @Type(type = "text")
    private String description;

    public String getProceedsType() {
        return proceedsType;
    }

    public void setProceedsType(String proceedsType) {
        this.proceedsType = proceedsType;
    }

    public AccountingCustomer getAccountingCustomer() {
        return accountingCustomer;
    }

    public void setAccountingCustomer(AccountingCustomer accountingCustomer) {
        this.accountingCustomer = accountingCustomer;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public DateTime getProceedsDate() {
        return proceedsDate;
    }

    public void setProceedsDate(DateTime proceedsDate) {
        this.proceedsDate = proceedsDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
