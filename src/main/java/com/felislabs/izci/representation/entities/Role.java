package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
@Entity
public class Role extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    private String name;
    private String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }


}
