package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
@Entity
public class InvoiceDetail extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User user;
    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "taxOid", referencedColumnName = "oid")
    private Tax tax;

    @ManyToOne
    @JoinColumn(name = "bidOid", referencedColumnName = "oid")
    private Invoice invoice;

    @ManyToOne
    @JoinColumn(name = "productOid", referencedColumnName = "oid")
    private Product product;

    private Double quantity;
    private Double price;
    private Double lineTotal;
    private Double discount;
    private Double discountIncluded;
    private Double taxIncluded;
    private String deadline;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(Double lineTotal) {
        this.lineTotal = lineTotal;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getDiscountIncluded() {
        return discountIncluded;
    }

    public void setDiscountIncluded(Double discountIncluded) {
        this.discountIncluded = discountIncluded;
    }

    public Double getTaxIncluded() {
        return taxIncluded;
    }

    public void setTaxIncluded(Double taxIncluded) {
        this.taxIncluded = taxIncluded;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
}