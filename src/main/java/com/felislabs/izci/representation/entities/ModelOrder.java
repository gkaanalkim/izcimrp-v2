package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 29/01/2017.
 */
@Entity
public class ModelOrder extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User updater;
    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "modelOid", referencedColumnName = "oid")
    private Model model;

    @ManyToOne
    @JoinColumn(name = "customerShipmentOid", referencedColumnName = "oid")
    private CustomerShipment customerShipment;

    private Integer orderQuantity;

    @Type(type = "text")
    private String shipmentInfo;

    private DateTime terminDate;

    public DateTime getTerminDate() {
        return terminDate;
    }

    public void setTerminDate(DateTime terminDate) {
        this.terminDate = terminDate;
    }

    public CustomerShipment getCustomerShipment() {
        return customerShipment;
    }

    public void setCustomerShipment(CustomerShipment customerShipment) {
        this.customerShipment = customerShipment;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Integer getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Integer orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getShipmentInfo() {
        return shipmentInfo;
    }

    public void setShipmentInfo(String shipmentInfo) {
        this.shipmentInfo = shipmentInfo;
    }
}
