package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 20/02/2017.
 */
@Entity
public class SupplyForm extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "updaterOid", referencedColumnName = "oid")
    private User updater;

    @ManyToOne
    @JoinColumn(name = "supplierOid", referencedColumnName = "oid")
    private Supplier supplier;

    private DateTime updateDate;

    private DateTime terminDate;

    @Type(type = "text")
    private String description;

    @Type(type = "text")
    private String materials;

    @Type(type = "text")
    private String models;

    private String documentNumber;

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public DateTime getTerminDate() {
        return terminDate;
    }

    public void setTerminDate(DateTime terminDate) {
        this.terminDate = terminDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMaterials() {
        return materials;
    }

    public void setMaterials(String materials) {
        this.materials = materials;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getModels() {
        return models;
    }

    public void setModels(String models) {
        this.models = models;
    }
}
