package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class ShipmentOrder extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "updaterOid", referencedColumnName = "oid")
    private User updater;
    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "modelOid", referencedColumnName = "oid")
    private Model model;

    private String parcelType;
    private String parcelQuantity;
    private String warehouseShipTo;
    private DateTime shipmentDate;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getParcelType() {
        return parcelType;
    }

    public void setParcelType(String parcelType) {
        this.parcelType = parcelType;
    }

    public String getParcelQuantity() {
        return parcelQuantity;
    }

    public void setParcelQuantity(String parcelQuantity) {
        this.parcelQuantity = parcelQuantity;
    }

    public String getWarehouseShipTo() {
        return warehouseShipTo;
    }

    public void setWarehouseShipTo(String warehouseShipTo) {
        this.warehouseShipTo = warehouseShipTo;
    }

    public DateTime getShipmentDate() {
        return shipmentDate;
    }

    public void setShipmentDate(DateTime shipmentDate) {
        this.shipmentDate = shipmentDate;
    }
}
