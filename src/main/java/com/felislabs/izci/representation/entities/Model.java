package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 20/01/2017.
 */
@Entity
public class Model extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "customerOid", referencedColumnName = "oid")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User updater;
    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    private String modelCode;
    private String customerModelCode;
    //    Kalıp Kodu
    private String moldCode;
    //    Taban Kodu
    private String footbedCode;
    //    Ayakkabı Numarası
    private String number;
    //    Bıçak kodu
    private String knifeCode;
    //    Mostra Bıçağı kodu
    private String mostraKnifeCode;
    //    Salpa Bıçağı kodu
    private String salpaKnifeCode;

    // Seriler
    private String moldSerial;
    private String footbedSerial;
    private String knifeSerial;
    private String mostraKnifeSerial;
    private String salpaKnifeSerial;

    private DateTime startDate;
    private DateTime endDate;
    private boolean confirm = false;
    private String series;

    @Type(type = "text")
    private String description;

    @Type(type = "text")
    private String deleteReason;

    @Type(type = "text")
    private String imagePaths;

    @Type(type = "text")
    private String defaultPhoto;

    @Type(type = "text")
    private String modelImagePaths;

    private String color;

    private Double profitRatio;

    private Double modelPrice;

    private String calculationType;

    private Boolean isOrderEntry;

    private Boolean isPricingEntry = false;

    private Boolean isMaterialEntry = false;

    private boolean supplyCompleted = false;

    private String season;
    private Integer year;

    public Boolean getOrderEntry() {
        return isOrderEntry;
    }

    public void setOrderEntry(Boolean orderEntry) {
        isOrderEntry = orderEntry;
    }

    public boolean isSupplyCompleted() {
        return supplyCompleted;
    }

    public void setSupplyCompleted(boolean supplyCompleted) {
        this.supplyCompleted = supplyCompleted;
    }

    public Boolean getPricingEntry() {
        return isPricingEntry;
    }

    public void setPricingEntry(Boolean pricingEntry) {
        isPricingEntry = pricingEntry;
    }

    public Boolean getMaterialEntry() {
        return isMaterialEntry;
    }

    public void setMaterialEntry(Boolean materialEntry) {
        isMaterialEntry = materialEntry;
    }

    public Boolean getIsOrderEntry() {
        return isOrderEntry;
    }

    public void setIsOrderEntry(Boolean isOrderEntry) {
        this.isOrderEntry = isOrderEntry;
    }

    public String getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(String calculationType) {
        this.calculationType = calculationType;
    }

    public Double getProfitRatio() {
        return profitRatio;
    }

    public void setProfitRatio(Double profitRatio) {
        this.profitRatio = profitRatio;
    }

    public Double getModelPrice() {
        return modelPrice;
    }

    public void setModelPrice(Double modelPrice) {
        this.modelPrice = modelPrice;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getModelCode() {
        return modelCode;
    }

    public void setModelCode(String modelCode) {
        this.modelCode = modelCode;
    }

    public String getCustomerModelCode() {
        return customerModelCode;
    }

    public void setCustomerModelCode(String customerModelCode) {
        this.customerModelCode = customerModelCode;
    }

    public String getMoldCode() {
        return moldCode;
    }

    public void setMoldCode(String moldCode) {
        this.moldCode = moldCode;
    }

    public String getFootbedCode() {
        return footbedCode;
    }

    public void setFootbedCode(String footbedCode) {
        this.footbedCode = footbedCode;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getKnifeCode() {
        return knifeCode;
    }

    public void setKnifeCode(String knifeCode) {
        this.knifeCode = knifeCode;
    }

    public String getMostraKnifeCode() {
        return mostraKnifeCode;
    }

    public void setMostraKnifeCode(String mostraKnifeCode) {
        this.mostraKnifeCode = mostraKnifeCode;
    }

    public String getSalpaKnifeCode() {
        return salpaKnifeCode;
    }

    public void setSalpaKnifeCode(String salpaKnifeCode) {
        this.salpaKnifeCode = salpaKnifeCode;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public boolean isConfirm() {
        return confirm;
    }

    public void setConfirm(boolean confirm) {
        this.confirm = confirm;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public String getImagePaths() {
        return imagePaths;
    }

    public void setImagePaths(String imagePaths) {
        this.imagePaths = imagePaths;
    }

    public String getDefaultPhoto() {
        return defaultPhoto;
    }

    public void setDefaultPhoto(String defaultPhoto) {
        this.defaultPhoto = defaultPhoto;
    }

    public String getModelImagePaths() {
        return modelImagePaths;
    }

    public void setModelImagePaths(String modelImagePaths) {
        this.modelImagePaths = modelImagePaths;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getMoldSerial() {
        return moldSerial;
    }

    public void setMoldSerial(String moldSerial) {
        this.moldSerial = moldSerial;
    }

    public String getFootbedSerial() {
        return footbedSerial;
    }

    public void setFootbedSerial(String footbedSerial) {
        this.footbedSerial = footbedSerial;
    }

    public String getKnifeSerial() {
        return knifeSerial;
    }

    public void setKnifeSerial(String knifeSerial) {
        this.knifeSerial = knifeSerial;
    }

    public String getMostraKnifeSerial() {
        return mostraKnifeSerial;
    }

    public void setMostraKnifeSerial(String mostraKnifeSerial) {
        this.mostraKnifeSerial = mostraKnifeSerial;
    }

    public String getSalpaKnifeSerial() {
        return salpaKnifeSerial;
    }

    public void setSalpaKnifeSerial(String salpaKnifeSerial) {
        this.salpaKnifeSerial = salpaKnifeSerial;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }
}
