package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 01/02/2017.
 */
@Entity
public class ModelSaya extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User updater;
    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "modelOrderOid", referencedColumnName = "oid")
    private ModelOrder modelOrder;

    @ManyToOne
    @JoinColumn(name = "sayaciOid", referencedColumnName = "oid")
    private Sayaci sayaci;

    private Integer quantity;

    private String documentNumber;

    private DateTime startDate;
    private DateTime endDate;

    @Type(type = "text")
    private String description;

    private String selectedAssortUid;

    public String getSelectedAssortUid() {
        return selectedAssortUid;
    }

    public void setSelectedAssortUid(String selectedAssortUid) {
        this.selectedAssortUid = selectedAssortUid;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }

    public Sayaci getSayaci() {
        return sayaci;
    }

    public void setSayaci(Sayaci sayaci) {
        this.sayaci = sayaci;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
