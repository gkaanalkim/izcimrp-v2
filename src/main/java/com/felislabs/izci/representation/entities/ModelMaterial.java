package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 24/01/2017.
 */
@Entity
public class ModelMaterial extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User updater;
    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "modelOid", referencedColumnName = "oid")
    private Model model;

    @ManyToOne
    @JoinColumn(name = "stockOid", referencedColumnName = "oid")
    private Stock stock;

    @ManyToOne
    @JoinColumn(name = "stockDetailOid", referencedColumnName = "oid")
    private StockDetail stockDetail;

    private String materialName;

    private Double constant;
    private Double unitPrice;
    private Double lineTotal;
    private String uid;

    @Type(type = "text")
    private String placeOfUse;

    private String lineUnit=" ";
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public StockDetail getStockDetail() {
        return stockDetail;
    }

    public void setStockDetail(StockDetail stockDetail) {
        this.stockDetail = stockDetail;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public String getPlaceOfUse() {
        return placeOfUse;
    }

    public void setPlaceOfUse(String placeOfUse) {
        this.placeOfUse = placeOfUse;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Double getConstant() {
        return constant;
    }

    public void setConstant(Double constant) {
        this.constant = constant;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(Double lineTotal) {
        this.lineTotal = lineTotal;
    }

    public String getLineUnit() {
        return lineUnit==null?" ":lineUnit;
    }

    public void setLineUnit(String lineUnit) {
        this.lineUnit = lineUnit;
    }

    @Override
    public String toString() {
        return "ModelMaterial{" +
                "updater=" + updater.getName() +
                ", updateDate=" + updateDate +
                ", company=" + company.getCode() +
                ", model=" + model.getModelCode() +
                ", materialName='" + materialName + '\'' +
                ", constant=" + constant +
                ", unitPrice=" + unitPrice +
                ", lineTotal=" + lineTotal +
                ", uid='" + uid + '\'' +
                ", placeOfUse='" + placeOfUse + '\'' +
                '}';
    }
}
