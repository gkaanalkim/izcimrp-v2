package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by arricie on 20.03.2017.
 */
@Entity
public class ModelProcessing extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;
    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User updater;
    private DateTime updateDate;


    @ManyToOne
    @JoinColumn(name = "modelOrderOid", referencedColumnName = "oid")
    private ModelOrder modelOrder;

    @ManyToOne
    @JoinColumn(name = "staffOid", referencedColumnName = "oid")
    private Staff staff;

    @ManyToOne
    @JoinColumn(name = "contractedManufacturerOid", referencedColumnName = "oid")
    private ContractedManufacturer contractedManufacturer;

    private Integer quantity;

    private String documentNumber;

    private DateTime startDate;
    private DateTime endDate;

    @Type(type = "text")
    private String description;

    private String selectedAssortUid;

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public ModelOrder getModelOrder() {
        return modelOrder;
    }

    public void setModelOrder(ModelOrder modelOrder) {
        this.modelOrder = modelOrder;
    }

    public ContractedManufacturer getContractedManufacturer() {
        return contractedManufacturer;
    }

    public void setContractedManufacturer(ContractedManufacturer contractedManufacturer) {
        this.contractedManufacturer = contractedManufacturer;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(DateTime startDate) {
        this.startDate = startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(DateTime endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSelectedAssortUid() {
        return selectedAssortUid;
    }

    public void setSelectedAssortUid(String selectedAssortUid) {
        this.selectedAssortUid = selectedAssortUid;
    }
}
