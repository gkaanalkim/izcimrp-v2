package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
@Entity
public class RoleUser extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User user;

    @ManyToOne
    @JoinColumn(name = "roleOid", referencedColumnName = "oid")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
