package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
@Entity
public class Invoice extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "bidOid", referencedColumnName = "oid")
    private Bid bid;

    @ManyToOne
    @JoinColumn(name = "accountingCustomerOid", referencedColumnName = "oid")
    private AccountingCustomer accountingCustomer;

    @ManyToOne
    @JoinColumn(name = "accountingCustomerRepresentativeOid", referencedColumnName = "oid")
    private AccountingCustomerRepresentative accountingCustomerRepresentative;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User user;

    private DateTime updateDate;

    private DateTime invoiceDate;

    private String deliveryNote;
    private String paymentNote;
    private String currencyNote;
    private String currency;
    private Double subTotal;
    private Double discountIncluded;
    private Double taxIncluded;

    @Type(type = "text")
    private String description;

    private String documentNumber;
    private String invoiceNumber;

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public AccountingCustomer getAccountingCustomer() {
        return accountingCustomer;
    }

    public void setAccountingCustomer(AccountingCustomer accountingCustomer) {
        this.accountingCustomer = accountingCustomer;
    }

    public AccountingCustomerRepresentative getAccountingCustomerRepresentative() {
        return accountingCustomerRepresentative;
    }

    public void setAccountingCustomerRepresentative(AccountingCustomerRepresentative accountingCustomerRepresentative) {
        this.accountingCustomerRepresentative = accountingCustomerRepresentative;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public DateTime getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(DateTime invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public String getPaymentNote() {
        return paymentNote;
    }

    public void setPaymentNote(String paymentNote) {
        this.paymentNote = paymentNote;
    }

    public String getCurrencyNote() {
        return currencyNote;
    }

    public void setCurrencyNote(String currencyNote) {
        this.currencyNote = currencyNote;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getDiscountIncluded() {
        return discountIncluded;
    }

    public void setDiscountIncluded(Double discountIncluded) {
        this.discountIncluded = discountIncluded;
    }

    public Double getTaxIncluded() {
        return taxIncluded;
    }

    public void setTaxIncluded(Double taxIncluded) {
        this.taxIncluded = taxIncluded;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Bid getBid() {
        return bid;
    }

    public void setBid(Bid bid) {
        this.bid = bid;
    }
}
