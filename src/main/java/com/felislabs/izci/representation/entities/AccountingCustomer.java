package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 07/03/2017.
 */
@Entity
public class AccountingCustomer extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User user;
    private DateTime updateDate;

    private String title;
    private String code;
    private String logoUrl;
    private String taxNo;
    private String taxOffice;
    private Double balanceLimit;
    private Double balance;
    private Integer maturity;

    @Type(type = "text")
    private String description;

    private Integer bidNumber = 0;
    private Integer invoiceNumber = 0;
    private Integer consignmentInvoiceNumber = 0;

    public Double getBalance() {
        return balance == null ? 0.0 : balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getTaxOffice() {
        return taxOffice;
    }

    public void setTaxOffice(String taxOffice) {
        this.taxOffice = taxOffice;
    }

    public Integer getBidNumber() {
        return bidNumber == null ? 1 : bidNumber;
    }

    public Integer getInvoiceNumber() {
        return invoiceNumber == null ? 1 : invoiceNumber;
    }

    public Integer getConsignmentInvoiceNumber() {
        return consignmentInvoiceNumber == null ? 1 : consignmentInvoiceNumber;
    }

    public void setBidNumber(Integer bidNumber) {
        this.bidNumber = bidNumber;
    }

    public void setInvoiceNumber(Integer invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public void setConsignmentInvoiceNumber(Integer consignmentInvoiceNumber) {
        this.consignmentInvoiceNumber = consignmentInvoiceNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public Double getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(Double balanceLimit) {
        this.balanceLimit = balanceLimit;
    }

    public Integer getMaturity() {
        return maturity;
    }

    public void setMaturity(Integer maturity) {
        this.maturity = maturity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
