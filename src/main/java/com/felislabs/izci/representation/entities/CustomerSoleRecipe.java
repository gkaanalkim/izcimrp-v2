package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 27/04/2017.
 */
@Entity
public class CustomerSoleRecipe extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "fairOrderOid", referencedColumnName = "oid")
    private FairOrder fairOrder;

    @ManyToOne
    @JoinColumn(name = "modelMaterialOid", referencedColumnName = "oid")
    private ModelMaterial modelMaterial;

    private String customerValue;

    public FairOrder getFairOrder() {
        return fairOrder;
    }

    public void setFairOrder(FairOrder fairOrder) {
        this.fairOrder = fairOrder;
    }

    public ModelMaterial getModelMaterial() {
        return modelMaterial;
    }

    public void setModelMaterial(ModelMaterial modelMaterial) {
        this.modelMaterial = modelMaterial;
    }

    public String getCustomerValue() {
        return customerValue;
    }

    public void setCustomerValue(String customerValue) {
        this.customerValue = customerValue;
    }
}
