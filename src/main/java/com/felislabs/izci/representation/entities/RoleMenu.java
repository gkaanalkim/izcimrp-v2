package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 19/09/15.
 */
@Entity
public class RoleMenu extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "roleOid", referencedColumnName = "oid")
    private Role role;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "menuOid", referencedColumnName = "oid")
    private Menu menu;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
}
