package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by arricie on 20.03.2017.
 */
@Entity
public class ContractedManufacturer extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;
    @ManyToOne
    @JoinColumn(name = "processingOid", referencedColumnName = "oid")
    private Processing processing;

    private String name;
    private String code;
    private String phone;
    private String representative;
    @Type(type = "text")
    private String description;
    @Type(type = "text")
    private String address;
    private Integer processingOrderCounter = 1;

    public Processing getProcessing() {
        return processing;
    }

    public void setProcessing(Processing processing) {
        this.processing = processing;
    }

    public Integer getProcessingOrderCounter() {
        return processingOrderCounter;
    }

    public void setProcessingOrderCounter(Integer processingOrderCounter) {
        this.processingOrderCounter = processingOrderCounter;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRepresentative() {
        return representative;
    }

    public void setRepresentative(String representative) {
        this.representative = representative;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
