package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 25/06/2017.
 */
@Entity
public class ManufacturingDefect extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "updaterOid", referencedColumnName = "oid")
    private User updater;
    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "modelMontaOid", referencedColumnName = "oid")
    private ModelMonta modelMonta;

    private String number;
    private String quantity;

    @Type(type = "text")
    private String sayaDefect;

    @Type(type = "text")
    private String montaDefect;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public ModelMonta getModelMonta() {
        return modelMonta;
    }

    public void setModelMonta(ModelMonta modelMonta) {
        this.modelMonta = modelMonta;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getSayaDefect() {
        return sayaDefect;
    }

    public void setSayaDefect(String sayaDefect) {
        this.sayaDefect = sayaDefect;
    }

    public String getMontaDefect() {
        return montaDefect;
    }

    public void setMontaDefect(String montaDefect) {
        this.montaDefect = montaDefect;
    }
}
