package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by dogukanyilmaz on 14/03/2017.
 */
@Entity
public class SupplyRequirement extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "updaterOid", referencedColumnName = "oid")
    private User updater;

    @ManyToOne
    @JoinColumn(name = "supplierOid", referencedColumnName = "oid")
    private Supplier supplier;
    @ManyToOne
    @JoinColumn(name = "modelOid", referencedColumnName = "oid")
    private Model model;
    @ManyToOne
    @JoinColumn(name = "stockOid", referencedColumnName = "oid")
    private Stock stock;
    @ManyToOne
    @JoinColumn(name = "stockDetailOid", referencedColumnName = "oid")
    private StockDetail stockDetail;

    private DateTime updateDate;

    private DateTime terminDate;

    @Type(type = "text")
    private String description;

    @Type(type = "text")
    private String outerHtml;

    private String documentNumber;

    private String type;

    private Double orderQuantity;

    private Double comingQuantity = 0.0;
    private Double totalQuantity = 0.0;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public StockDetail getStockDetail() {
        return stockDetail;
    }

    public void setStockDetail(StockDetail stockDetail) {
        this.stockDetail = stockDetail;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public Double getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Double totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public DateTime getTerminDate() {
        return terminDate;
    }

    public void setTerminDate(DateTime terminDate) {
        this.terminDate = terminDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(Double orderQuantity) {
        this.orderQuantity = orderQuantity;
    }


    public Double getComingQuantity() {
        return comingQuantity;
    }

    public void setComingQuantity(Double comingQuantity) {
        this.comingQuantity = comingQuantity;
    }

    public String getOuterHtml() {
        return outerHtml;
    }

    public void setOuterHtml(String outerHtml) {
        this.outerHtml = outerHtml;
    }
}
