package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import com.sun.org.apache.xpath.internal.operations.Bool;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by dogukanyilmaz on 04/05/2017.
 */
@Entity
public class CuttingRequirement extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User updater;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "stockOid", referencedColumnName = "oid")
    private Stock stock;

    @ManyToOne
    @JoinColumn(name = "stockDetailOid", referencedColumnName = "oid")
    private StockDetail stockDetail;

    @ManyToOne
    @JoinColumn(name = "modelCuttingOrderOid", referencedColumnName = "oid")
    private ModelCuttingOrder modelCuttingOrder;

    private Double quantity;

    private Boolean completed = false;

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public ModelCuttingOrder getModelCuttingOrder() {
        return modelCuttingOrder;
    }

    public void setModelCuttingOrder(ModelCuttingOrder modelCuttingOrder) {
        this.modelCuttingOrder = modelCuttingOrder;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public StockDetail getStockDetail() {
        return stockDetail;
    }

    public void setStockDetail(StockDetail stockDetail) {
        this.stockDetail = stockDetail;
    }
}
