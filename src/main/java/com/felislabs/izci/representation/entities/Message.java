package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

/**
 * Created by dogukanyilmaz on 10/11/2016.
 */
@Entity
public class Message extends BaseEntity{
    @ManyToOne
    @JoinColumn(name = "userOid", referencedColumnName = "oid")
    private User from;

    String title;

    @NotNull
    @Type(type ="text")
    String description;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
