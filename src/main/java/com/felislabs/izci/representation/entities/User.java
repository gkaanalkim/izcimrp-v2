package com.felislabs.izci.representation.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.felislabs.izci.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
@Entity
public class User extends BaseEntity {
    @NotNull
    @Size(max = 50)
    private String name;

    @NotNull
    @Size(max = 50)
    private String surname;

    @NotNull
    @Size(max = 5)
    private String code;

    @Size(max = 15)
    private String mobilePhone;

    @Size(max = 10)
    private String internalPhoneCode;

    @NotNull
    @Size(max = 100)
    private String email;

    @NotNull
    @JsonIgnore
    private String password;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getInternalPhoneCode() {
        return internalPhoneCode;
    }

    public void setInternalPhoneCode(String internalPhoneCode) {
        this.internalPhoneCode = internalPhoneCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getOid(){return super.getOid();}
}
