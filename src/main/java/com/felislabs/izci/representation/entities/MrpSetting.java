package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;

import javax.persistence.*;

/**
 * Created by gunerkaanalkim on 26/10/2016.
 */
@Entity
public class MrpSetting extends BaseEntity {
    private Integer workOrderCount = 1;
    private Integer stockCodeCounter = 1;
    private Integer montaOrderCounter;
    private Integer cuttingOrderCounter;
    private Integer supplyFormCounter;
    private String calculationType;
    private Integer fairOrderNumber;
    private Integer poNumber;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    public Integer getMontaOrderCounter() {
        return montaOrderCounter == null ? 1 : montaOrderCounter;
    }

    public Integer getCuttingOrderCounter() {
        return cuttingOrderCounter == null ? 1 : cuttingOrderCounter;
    }

    public Integer getSupplyFormCounter() {
        return supplyFormCounter == null ? 1 : supplyFormCounter;
    }

    public void setSupplyFormCounter(Integer supplyFormCounter) {
        this.supplyFormCounter = supplyFormCounter;
    }

    public void setCuttingOrderCounter(Integer cuttingOrderCounter) {
        this.cuttingOrderCounter = cuttingOrderCounter;
    }

    public void setMontaOrderCounter(Integer montaOrderCounter) {
        this.montaOrderCounter = montaOrderCounter;
    }

    public Integer getStockCodeCounter() {
        return stockCodeCounter;
    }

    public void setStockCodeCounter(Integer stockCodeCounter) {
        this.stockCodeCounter = stockCodeCounter;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Integer getWorkOrderCount() {
        return workOrderCount;
    }

    public void setWorkOrderCount(Integer workOrderCount) {
        this.workOrderCount = workOrderCount;
    }

    public String getCalculationType() {
        return calculationType;
    }

    public void setCalculationType(String calculationType) {
        this.calculationType = calculationType;
    }

    public Integer getFairOrderNumber() {
        return fairOrderNumber == null ? 1 : fairOrderNumber;
    }

    public void setFairOrderNumber(Integer fairOrderNumber) {
        this.fairOrderNumber = fairOrderNumber;
    }

    public Integer getPoNumber() {
        return poNumber == null ? 1 : poNumber;
    }

    public void setPoNumber(Integer poNumber) {
        this.poNumber = poNumber;
    }
}
