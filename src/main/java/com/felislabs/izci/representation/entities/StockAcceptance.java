package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by dogukanyilmaz on 16/02/2017.
 */
@Entity

public class StockAcceptance extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "stockOid", referencedColumnName = "oid")
    private Stock stock;

    @ManyToOne
    @JoinColumn(name = "stockDetailOid", referencedColumnName = "oid")
    private StockDetail stockDetail;

    @ManyToOne
    @JoinColumn(name = "modelOid", referencedColumnName = "oid")
    private Model model;

    @ManyToOne
    @JoinColumn(name = "updaterOid", referencedColumnName = "oid")
    private User updater;

    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    private Double quantity;

    public User getUpdater() {
        return updater;
    }

    public StockDetail getStockDetail() {
        return stockDetail;
    }

    public void setStockDetail(StockDetail stockDetail) {
        this.stockDetail = stockDetail;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}
