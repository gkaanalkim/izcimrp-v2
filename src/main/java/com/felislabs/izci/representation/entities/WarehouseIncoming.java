package com.felislabs.izci.representation.entities;

import com.felislabs.izci.representation.core.BaseEntity;
import org.joda.time.DateTime;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by gunerkaanalkim on 23/06/2017.
 */
@Entity
public class WarehouseIncoming extends BaseEntity {
    @ManyToOne
    @JoinColumn(name = "companyOid", referencedColumnName = "oid")
    private Company company;

    @ManyToOne
    @JoinColumn(name = "updaterOid", referencedColumnName = "oid")
    private User updater;

    private DateTime updateDate;

    @ManyToOne
    @JoinColumn(name = "stockOid", referencedColumnName = "oid")
    private Stock stock;

    @ManyToOne
    @JoinColumn(name = "stockDetailOid", referencedColumnName = "oid")
    private StockDetail stockDetail;

    @ManyToOne
    @JoinColumn(name = "modelPurcaseOrderOid", referencedColumnName = "oid")
    private ModelPurcaseOrder modelPurcaseOrder;

    private Double incomingQuantity;

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public User getUpdater() {
        return updater;
    }

    public void setUpdater(User updater) {
        this.updater = updater;
    }

    public DateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(DateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Stock getStock() {
        return stock;
    }

    public void setStock(Stock stock) {
        this.stock = stock;
    }

    public StockDetail getStockDetail() {
        return stockDetail;
    }

    public void setStockDetail(StockDetail stockDetail) {
        this.stockDetail = stockDetail;
    }

    public ModelPurcaseOrder getModelPurcaseOrder() {
        return modelPurcaseOrder;
    }

    public void setModelPurcaseOrder(ModelPurcaseOrder modelPurcaseOrder) {
        this.modelPurcaseOrder = modelPurcaseOrder;
    }

    public Double getIncomingQuantity() {
        return incomingQuantity;
    }

    public void setIncomingQuantity(Double incomingQuantity) {
        this.incomingQuantity = incomingQuantity;
    }
}
