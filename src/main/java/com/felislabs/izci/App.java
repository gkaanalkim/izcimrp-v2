package com.felislabs.izci;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.cli.AccountingInitializeCommand;
import com.felislabs.izci.cli.SoleInitializeCommand;
import com.felislabs.izci.cli.InitializeCommand;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.representation.entities.*;
import com.felislabs.izci.resources.*;
import de.spinscale.dropwizard.jobs.JobsBundle;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

/**
 * Created by gunerkaanalkim on 18/01/2017.
 */
public class App extends Application<MrpConfiguration> {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private final HibernateBundle<MrpConfiguration> hibernateBundle = new HibernateBundle<MrpConfiguration>(
            Company.class,
            Customer.class,
            Menu.class,
            MrpSetting.class,
            Permission.class,
            Role.class,
            RoleMenu.class,
            RolePermission.class,
            RoleUser.class,
            SystemActivity.class,
            User.class,
            UserToken.class,
            Supplier.class,
            StockType.class,
            MaterialType.class,
            Stock.class,
            ProductionLine.class,
            InternalCritical.class,
            Sayaci.class,
            CustomerShipment.class,
            Message.class,
            Model.class,
            ModelMaterial.class,
            ModelInternalCritical.class,
            ModelExternalCritical.class,
            StockDetail.class,
            ModelPriceDetail.class,
            ModelOrder.class,
            ModelAssort.class,
            ModelAlarm.class,
            ModelSupply.class,
            ModelCuttingOrder.class,
            ModelSaya.class,
            ModelSayaIncoming.class,
            ModelMonta.class,
            ActivityReport.class,
            ModelCuttingIncoming.class,
            StockAcceptance.class,
            SupplyForm.class,
            ModelMontaIncoming.class,
            AccountingCustomer.class,
            AccountingCustomerAddress.class,
            AccountingCustomerContact.class,
            AccountingCustomerRepresentative.class,
            Tax.class,
            Category.class,
            Brand.class,
            Product.class,
            Bid.class,
            BidDetail.class,
            Invoice.class,
            InvoiceDetail.class,
            Proforma.class,
            ProformaDetail.class,
            ConsignmentInvoice.class,
            ConsignmentInvoiceDetail.class,
            Account.class,
            Proceeds.class,
            CheckProceeds.class,
            Account.class,
            SupplyRequirement.class,
            FairModelAssort.class,
            FairModelPrice.class,
            Department.class,
            Staff.class,
            ContractedManufacturer.class,
            Processing.class,
            ModelProcessing.class,
            ModelProcessingIncoming.class,
            FairOrder.class,
            FairOrderAssort.class,
            ExtraExpenses.class,
            CustomerSoleRecipe.class,
            CuttingRequirement.class,
            MontaRequirement.class,
            Warehouse.class,
            PurcaseOrder.class,
            ModelPurcaseOrder.class,
            WarehouseIncoming.class,
            WarehouseOutgoing.class,
            ManufacturingDefect.class,
            ShipmentOrder.class
    ) {
        @Override
        public DataSourceFactory getDataSourceFactory(MrpConfiguration mrpConfiguration) {
            return mrpConfiguration.getDataSourceFactory();
        }
    };


    public static void main(String[] args) throws Exception {
        new App().run(args);
    }

    @Override
    public void initialize(io.dropwizard.setup.Bootstrap<MrpConfiguration> bootstrap) {
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addCommand(new InitializeCommand<MrpConfiguration>(this, hibernateBundle));
        bootstrap.addCommand(new AccountingInitializeCommand<MrpConfiguration>(this, hibernateBundle));
        bootstrap.addCommand(new SoleInitializeCommand<MrpConfiguration>(this, hibernateBundle));
//        bootstrap.addBundle(new JobsBundle("com.felislabs.izci"));
    }

    @Override
    public void run(MrpConfiguration mrpConfiguration, Environment environment) throws Exception {
        LOGGER.info("Services are run.");

        //  CORS Settings
        FilterRegistration.Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_HEADERS_PARAM, "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        //  CORS Settings

        final CompanyDAO companyDAO = new CompanyDAO(hibernateBundle.getSessionFactory());
        final CustomerDAO customerDAO = new CustomerDAO(hibernateBundle.getSessionFactory());
        final MenuDAO menuDAO = new MenuDAO(hibernateBundle.getSessionFactory());
        final MrpSettingDAO mrpSettingDAO = new MrpSettingDAO(hibernateBundle.getSessionFactory());
        final PermissionDAO permissionDAO = new PermissionDAO(hibernateBundle.getSessionFactory());
        final RoleDAO roleDAO = new RoleDAO(hibernateBundle.getSessionFactory());
        final RoleMenuDAO roleMenuDAO = new RoleMenuDAO(hibernateBundle.getSessionFactory());
        final RolePermissionDAO rolePermissionDAO = new RolePermissionDAO(hibernateBundle.getSessionFactory());
        final RoleUserDAO roleUserDAO = new RoleUserDAO(hibernateBundle.getSessionFactory());
        final SystemActivityDAO systemActivityDAO = new SystemActivityDAO(hibernateBundle.getSessionFactory());
        final UserDAO userDAO = new UserDAO(hibernateBundle.getSessionFactory());
        final UserTokenDAO userTokenDAO = new UserTokenDAO(hibernateBundle.getSessionFactory());
        final SupplierDAO supplierDAO = new SupplierDAO(hibernateBundle.getSessionFactory());
        final StockTypeDAO stockTypeDAO = new StockTypeDAO(hibernateBundle.getSessionFactory());
        final MaterialTypeDAO materialTypeDAO = new MaterialTypeDAO(hibernateBundle.getSessionFactory());
        final StockDAO stockDAO = new StockDAO(hibernateBundle.getSessionFactory());
        final ProductionLineDAO productionLineDAO = new ProductionLineDAO(hibernateBundle.getSessionFactory());
        final InternalCriticalDAO internalCriticalDAO = new InternalCriticalDAO(hibernateBundle.getSessionFactory());
        final SayaciDAO sayaciDAO = new SayaciDAO(hibernateBundle.getSessionFactory());
        final CustomerShipmentDAO customerShipmentDAO = new CustomerShipmentDAO(hibernateBundle.getSessionFactory());
        final MessageDAO messageDAO = new MessageDAO(hibernateBundle.getSessionFactory());
        final ModelDAO modelDAO = new ModelDAO(hibernateBundle.getSessionFactory());
        final ModelMaterialDAO modelMaterialDAO = new ModelMaterialDAO(hibernateBundle.getSessionFactory());
        final ModelInternalCriticalDAO modelInternalCriticalDAO = new ModelInternalCriticalDAO(hibernateBundle.getSessionFactory());
        final ModelExternalCriticalDAO modelExternalCriticalDAO = new ModelExternalCriticalDAO(hibernateBundle.getSessionFactory());
        final StockDetailDAO stockDetailDAO = new StockDetailDAO(hibernateBundle.getSessionFactory());
        final ModelPriceDetailDAO modelPriceDetailDAO = new ModelPriceDetailDAO(hibernateBundle.getSessionFactory());
        final ModelAssortDAO modelAssortDAO = new ModelAssortDAO(hibernateBundle.getSessionFactory());
        final ModelAlarmDAO modelAlarmDAO = new ModelAlarmDAO(hibernateBundle.getSessionFactory());
        final ModelOrderDAO modelOrderDAO = new ModelOrderDAO(hibernateBundle.getSessionFactory());
        final ModelSupplyDAO modelSupplyDAO = new ModelSupplyDAO(hibernateBundle.getSessionFactory());
        final ModelCuttingOrderDAO modelCuttingOrderDAO = new ModelCuttingOrderDAO(hibernateBundle.getSessionFactory());
        final ModelSayaDAO modelSayaDAO = new ModelSayaDAO(hibernateBundle.getSessionFactory());
        final ModelSayaIncomingDAO modelSayaIncomingDAO = new ModelSayaIncomingDAO(hibernateBundle.getSessionFactory());
        final ModelMontaDAO modelMontaDAO = new ModelMontaDAO(hibernateBundle.getSessionFactory());
        final ActivityReportDAO activityReportDAO = new ActivityReportDAO(hibernateBundle.getSessionFactory());
        final ModelCuttingIncomingDAO modelCuttingIncomingDAO = new ModelCuttingIncomingDAO(hibernateBundle.getSessionFactory());
        final StockAcceptanceDAO stockAcceptanceDAO = new StockAcceptanceDAO(hibernateBundle.getSessionFactory());
        final SupplyFormDAO supplyFormDAO = new SupplyFormDAO(hibernateBundle.getSessionFactory());
        final ModelMontaIncomingDAO modelMontaIncomingDAO = new ModelMontaIncomingDAO(hibernateBundle.getSessionFactory());
        final FairModelAssortDAO fairModelAssortDAO = new FairModelAssortDAO(hibernateBundle.getSessionFactory());
        final FairModelPriceDAO fairModelPriceDAO = new FairModelPriceDAO(hibernateBundle.getSessionFactory());
        final DepartmentDAO departmentDAO = new DepartmentDAO(hibernateBundle.getSessionFactory());
        final StaffDAO staffDAO = new StaffDAO(hibernateBundle.getSessionFactory());
        final ContractedManufacturerDAO contractedManufacturerDAO = new ContractedManufacturerDAO(hibernateBundle.getSessionFactory());
        final ProcessingDAO processingDAO = new ProcessingDAO(hibernateBundle.getSessionFactory());
        final ModelProcessingDAO modelProcessingDAO = new ModelProcessingDAO(hibernateBundle.getSessionFactory());
        final ModelProcessingIncomingDAO modelProcessingIncomingDAO = new ModelProcessingIncomingDAO(hibernateBundle.getSessionFactory());
        final FairOrderDAO fairOrderDAO = new FairOrderDAO(hibernateBundle.getSessionFactory());
        final FairOrderAssortDAO fairOrderAssortDAO = new FairOrderAssortDAO(hibernateBundle.getSessionFactory());
        final CuttingRequirementDAO cuttingRequirementDAO = new CuttingRequirementDAO(hibernateBundle.getSessionFactory());
        final MontaRequirementDAO montaRequirementDAO = new MontaRequirementDAO(hibernateBundle.getSessionFactory());
        final WarehouseDAO warehouseDAO = new WarehouseDAO((hibernateBundle.getSessionFactory()));
        final PurcaseOrderDAO purcaseOrderDAO = new PurcaseOrderDAO((hibernateBundle.getSessionFactory()));
        final ModelPurcaseOrderDAO modelPurcaseOrderDAO = new ModelPurcaseOrderDAO((hibernateBundle.getSessionFactory()));
        final WarehouseIncomingDAO warehouseIncomingDAO = new WarehouseIncomingDAO((hibernateBundle.getSessionFactory()));
        final WarehouseOutgoingDAO warehouseOutgoingDAO = new WarehouseOutgoingDAO((hibernateBundle.getSessionFactory()));
        final ManufacturingDefectDAO manufacturingDefectDAO = new ManufacturingDefectDAO((hibernateBundle.getSessionFactory()));

        //  Muhasebe
        final AccountingCustomerDAO accountingCustomerDAO = new AccountingCustomerDAO(hibernateBundle.getSessionFactory());
        final AccountingCustomerAddressDAO accountingCustomerAddressDAO = new AccountingCustomerAddressDAO(hibernateBundle.getSessionFactory());
        final AccountingCustomerContactDAO accountingCustomerContactDAO = new AccountingCustomerContactDAO(hibernateBundle.getSessionFactory());
        final AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO = new AccountingCustomerRepresentativeDAO(hibernateBundle.getSessionFactory());
        final TaxDAO taxDAO = new TaxDAO(hibernateBundle.getSessionFactory());
        final CategoryDAO categoryDAO = new CategoryDAO(hibernateBundle.getSessionFactory());
        final BrandDAO brandDAO = new BrandDAO(hibernateBundle.getSessionFactory());
        final ProductDAO productDAO = new ProductDAO(hibernateBundle.getSessionFactory());
        final BidDAO bidDAO = new BidDAO(hibernateBundle.getSessionFactory());
        final BidDetailDAO bidDetailDAO = new BidDetailDAO(hibernateBundle.getSessionFactory());
        final InvoiceDAO invoiceDAO = new InvoiceDAO(hibernateBundle.getSessionFactory());
        final InvoiceDetailDAO invoiceDetailDAO = new InvoiceDetailDAO(hibernateBundle.getSessionFactory());
        final ProformaDAO proformaDAO = new ProformaDAO(hibernateBundle.getSessionFactory());
        final ProformaDetailDAO proformaDetailDAO = new ProformaDetailDAO(hibernateBundle.getSessionFactory());
        final ConsignmentInvoiceDAO consignmentInvoiceDAO = new ConsignmentInvoiceDAO(hibernateBundle.getSessionFactory());
        final ConsignmentInvoiceDetailDAO consignmentInvoiceDetailDAO = new ConsignmentInvoiceDetailDAO(hibernateBundle.getSessionFactory());
        final AccountDAO accountDAO = new AccountDAO(hibernateBundle.getSessionFactory());
        final ProceedsDAO proceedsDAO = new ProceedsDAO(hibernateBundle.getSessionFactory());
        final CheckDAO checkDAO = new CheckDAO(hibernateBundle.getSessionFactory());
        final SupplyRequirementDAO supplyRequirementDAO = new SupplyRequirementDAO(hibernateBundle.getSessionFactory());
        final ExtraExpensesDAO extraExpensesDAO = new ExtraExpensesDAO(hibernateBundle.getSessionFactory());
        final CustomerSoleRecipeDAO customerSoleRecipeDAO = new CustomerSoleRecipeDAO(hibernateBundle.getSessionFactory());
        final ShipmentOrderDAO shipmentOrderDAO = new ShipmentOrderDAO(hibernateBundle.getSessionFactory());

        final AuthUtil authUtil = new AuthUtil(roleDAO, userDAO, permissionDAO, roleUserDAO, rolePermissionDAO, userTokenDAO, systemActivityDAO);

        environment.jersey().register(new AuthResource(userDAO, userTokenDAO, authUtil));
        environment.jersey().register(new MenuResource(userDAO, menuDAO, roleDAO, roleMenuDAO, roleUserDAO, authUtil, companyDAO));
        environment.jersey().register(new UserResource(userDAO, authUtil));
        environment.jersey().register(new CustomerResource(customerDAO, authUtil, systemActivityDAO, customerShipmentDAO));
        environment.jersey().register(new SupplierResource(supplierDAO, authUtil));
        environment.jersey().register(new StockTypeResources(authUtil, stockTypeDAO));
        environment.jersey().register(new MaterialTypeResource(materialTypeDAO, authUtil));
        environment.jersey().register(new StockResources(stockDAO, authUtil, supplierDAO, stockTypeDAO, mrpSettingDAO, stockDetailDAO, warehouseDAO, cuttingRequirementDAO, montaRequirementDAO, modelCuttingOrderDAO, modelMontaDAO));
        environment.jersey().register(new ProductionLineResource(authUtil, productionLineDAO));
        environment.jersey().register(new InternalCriticalResources(authUtil, internalCriticalDAO));
        environment.jersey().register(new SayaciResource(authUtil, sayaciDAO));
        environment.jersey().register(new CustomerShipmentResource(authUtil, customerShipmentDAO, customerDAO));
        environment.jersey().register(new MessageResource(messageDAO, authUtil, userDAO));
        environment.jersey().register(new ActivityReportResources(activityReportDAO, companyDAO, authUtil));
        environment.jersey().register(new ModelResource(
                authUtil,
                modelDAO,
                customerDAO,
                mrpSettingDAO,
                modelMaterialDAO,
                modelInternalCriticalDAO,
                modelExternalCriticalDAO,
                stockDAO,
                modelPriceDetailDAO,
                modelOrderDAO,
                stockDetailDAO,
                modelAssortDAO,
                modelSayaDAO,
                modelSayaIncomingDAO,
                modelCuttingOrderDAO,
                modelCuttingIncomingDAO,
                modelMontaDAO,
                modelMontaIncomingDAO
        ));

        environment.jersey().register(new OrderResource(authUtil, modelDAO, modelAlarmDAO, modelAssortDAO, modelOrderDAO, customerShipmentDAO, modelMaterialDAO));
        environment.jersey().register(new SupplyResource(
                authUtil,
                modelDAO,
                modelOrderDAO,
                modelSupplyDAO,
                stockDAO,
                modelMaterialDAO,
                modelAssortDAO,
                supplierDAO,
                supplyFormDAO,
                mrpSettingDAO,
                supplyRequirementDAO,
                stockDetailDAO
        ));
        environment.jersey().register(new CuttingResource(authUtil, modelDAO, modelOrderDAO, modelCuttingOrderDAO, modelMaterialDAO, modelAssortDAO, mrpSettingDAO, modelCuttingIncomingDAO, stockDAO, stockDetailDAO, cuttingRequirementDAO, staffDAO));
        environment.jersey().register(new SayaResource(authUtil, modelDAO, modelOrderDAO, modelSayaDAO, sayaciDAO, modelAssortDAO, modelSayaIncomingDAO, modelPriceDetailDAO));
        environment.jersey().register(new MontaResource(
                authUtil,
                modelOrderDAO,
                modelMontaDAO,
                productionLineDAO,
                mrpSettingDAO,
                modelDAO,
                modelAssortDAO,
                modelAlarmDAO,
                modelInternalCriticalDAO,
                modelExternalCriticalDAO,
                modelMontaIncomingDAO,
                stockDAO,
                stockDetailDAO,
                modelMaterialDAO,
                stockTypeDAO,
                supplierDAO,
                montaRequirementDAO,
                manufacturingDefectDAO
        ));

        environment.jersey().register(new StatisticResource(
                authUtil,
                userDAO,
                userTokenDAO,
                systemActivityDAO,
                modelDAO,
                modelOrderDAO
        ));
        environment.jersey().register(new CompanyResource(
                authUtil,
                companyDAO,
                menuDAO,
                roleDAO,
                permissionDAO,
                roleUserDAO,
                rolePermissionDAO,
                roleMenuDAO,
                mrpSettingDAO,
                userDAO,
                materialTypeDAO,
                stockTypeDAO,
                supplierDAO,
                warehouseDAO
        ));
        environment.jersey().register(new RoleResource(
                roleDAO,
                userDAO,
                roleUserDAO,
                authUtil
        ));
        environment.jersey().register(new PermissionResource(
                permissionDAO,
                roleDAO,
                rolePermissionDAO,
                authUtil,
                roleUserDAO
        ));
        environment.jersey().register(new StockAcceptanceResources(
                authUtil,
                companyDAO,
                stockAcceptanceDAO,
                stockDAO,
                modelDAO,
                stockDetailDAO,
                supplyRequirementDAO
        ));
        environment.jersey().register(new ReportResource(
                authUtil,
                modelDAO,
                customerDAO,
                modelOrderDAO,
                modelAssortDAO,
                modelMontaDAO,
                modelMontaIncomingDAO,
                modelCuttingOrderDAO,
                modelCuttingIncomingDAO,
                modelSayaDAO,
                modelSayaIncomingDAO,
                supplyFormDAO,
                systemActivityDAO,
                stockDAO,
                modelMaterialDAO,
                supplyRequirementDAO,
                sayaciDAO,
                supplierDAO));
        environment.jersey().register(new FairResource(authUtil, fairModelAssortDAO, fairModelPriceDAO, modelDAO));
        environment.jersey().register(new DepartmentResource(authUtil, departmentDAO));
        environment.jersey().register(new StaffResource(authUtil, staffDAO, departmentDAO));
        environment.jersey().register(new ContractedManufacturerResource(authUtil, contractedManufacturerDAO, processingDAO));
        environment.jersey().register(new ProcessingResource(authUtil, processingDAO));
        environment.jersey().register(new ModelProcessingResource(
                authUtil,
                modelDAO,
                modelOrderDAO,
                modelProcessingDAO,
                contractedManufacturerDAO,
                modelAssortDAO,
                modelProcessingIncomingDAO,
                staffDAO
        ));

        environment.jersey().register(new FairOrderResource(authUtil, fairOrderDAO, fairModelAssortDAO, fairModelPriceDAO, modelDAO, customerDAO, fairOrderAssortDAO, modelMaterialDAO, customerSoleRecipeDAO, mrpSettingDAO));
        environment.jersey().register(new CuttingRequirementResource(authUtil, companyDAO, cuttingRequirementDAO, modelCuttingOrderDAO, stockDAO, stockDetailDAO));
        environment.jersey().register(new MontaRequirementResource(authUtil, companyDAO, montaRequirementDAO, modelMontaDAO, stockDAO, stockDetailDAO));
        environment.jersey().register(new WarehouseResource(authUtil, companyDAO, warehouseDAO, stockDetailDAO, stockDAO, warehouseIncomingDAO, modelPurcaseOrderDAO, modelDAO, warehouseOutgoingDAO, purcaseOrderDAO));
        environment.jersey().register(new ExtraExpensesResource(authUtil, extraExpensesDAO));
        environment.jersey().register(new PurcaseOrderResource(authUtil, stockDAO, stockDetailDAO, modelDAO, purcaseOrderDAO, supplierDAO, mrpSettingDAO, modelPurcaseOrderDAO, warehouseIncomingDAO));
        environment.jersey().register(new ShipmentOrderResource(authUtil, shipmentOrderDAO, modelDAO));

        //  Muhasebe Servisleri
        environment.jersey().register(new AccountingCustomerResource(authUtil, accountingCustomerDAO, accountingCustomerAddressDAO, accountingCustomerContactDAO, accountingCustomerRepresentativeDAO));
        environment.jersey().register(new TaxResource(authUtil, taxDAO));
        environment.jersey().register(new BrandResource(authUtil, brandDAO));
        environment.jersey().register(new CategoryResource(authUtil, categoryDAO));
        environment.jersey().register(new ProductResource(authUtil, productDAO, categoryDAO, brandDAO));
        environment.jersey().register(new BidResource(
                authUtil,
                bidDAO,
                accountingCustomerDAO,
                accountingCustomerRepresentativeDAO,
                taxDAO,
                productDAO,
                bidDetailDAO,
                invoiceDAO,
                invoiceDetailDAO,
                proformaDAO,
                proformaDetailDAO,
                consignmentInvoiceDAO,
                consignmentInvoiceDetailDAO
        ));
        environment.jersey().register(new InvoiceResource(authUtil, bidDAO, invoiceDAO, accountingCustomerDAO, accountingCustomerRepresentativeDAO, taxDAO, productDAO, invoiceDetailDAO));
        environment.jersey().register(new ProformaResource(authUtil, bidDAO, proformaDAO, accountingCustomerDAO, accountingCustomerRepresentativeDAO, taxDAO, productDAO, proformaDetailDAO));
        environment.jersey().register(new ConsignmentInvoiceResource(authUtil, bidDAO, consignmentInvoiceDAO, accountingCustomerDAO, accountingCustomerRepresentativeDAO, taxDAO, productDAO, consignmentInvoiceDetailDAO));
        environment.jersey().register(new AccountResource(authUtil, accountDAO));
        environment.jersey().register(new ProceedsResource(authUtil, proceedsDAO, accountingCustomerDAO, accountDAO));
        environment.jersey().register(new CheckProceedsResource(authUtil, checkDAO, accountingCustomerDAO, accountDAO));

        //  Sole
        environment.jersey().register(new SoleModelResource(
                authUtil,
                modelDAO,
                customerDAO,
                mrpSettingDAO,
                modelMaterialDAO,
                modelInternalCriticalDAO,
                modelExternalCriticalDAO,
                stockDAO,
                modelPriceDetailDAO,
                modelOrderDAO,
                stockDetailDAO,
                modelAssortDAO
        ));

        environment.jersey().register(MultiPartFeature.class);
    }
}
