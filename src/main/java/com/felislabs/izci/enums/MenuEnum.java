package com.felislabs.izci.enums;

/**
 * Created by gunerkaanalkim on 19/09/15.
 */
public enum MenuEnum {
    DEFINITION_MAIN("DefinitionMain"),
    USER_MAIN("UserMain"),
    CUSTOMER("Customer"),
    PRODUCTION_LINE("ProductionLine"),
    ROL_PERMISSION("RolePermission"),
    USER_PROCESS("UserProcess"),
    MENU_OPERATIONS("MenuOperations"),
    MODELLING_MAIN("Modelling"),
    PLANNING("Planning"),
    PRICING("Pricing"),
    ORDER("Order"),
    SUPPLY("Supply"),
    BULK_SUPPLY("BulkSupply"),
    PRODUCTION("Production"),
    PRODUCTION_PLANNING("ProductionPlaning"),
    PRODUCTION_LIST("ProductionList"),
    MATERIAL_TYPE("MaterialType"),
    INTERNAL_CRITICAL("InternalCritical"),
    CUT("Cut"),
    SUPPLIER("Supplier"),
    STOCK_MAIN("StockMain"),
    STOCK("Stock"),
    STOCK_TYPE("StockType"),
    SAYA("Saya"),
    SAYACI("Sayaci"),
    MONTA("Monta"),
    SHIPMENT_INFO("ShipmentInfo"),
    ACTIVITY_REPORT("ActivityReport"),
    REPORTS("Reports"),

    //    V2 MENU
    MODEL_CREATE("ModelCreate"),
    OWN_MODEL("OwnModel"),
    COMPANY_UPDATE("CompanyUpdate"),
    COMPANY("Company"),
    PRODUCTION_REPORT("ProductionReport"),
    STOCK_ACCEPTANCE("StockAcceptance"),
    WAREHOUSE_INCOMING("WarehouseIncoming"),
    WAREHOUSE_OUTGOING("WarehouseOutgoing"),
    FORWARDING("Forwarding"),
    FORWARDING_INFO("ForwardingInfo"),
    FAIR_DEFINITION("FairDefinition"),
    FAIR_REPORT("FairReport"),
    FAIR_OPERATION("FairOperation"),
    SUPPLY_REPORT("SupplyReport"),
    SAYACI_REPORT("SayaciReport"),
    SUPPLIER_REPORT("SupplierReport"),
    USER_REPORT("UserReport"),
    DEPARTMENT("Department"),
    STAFF("Staff"),
    CONTRACTED_MANUFACTURER("ContractedManufacturer"),
    PROCESSING("Processing"),
    MODEL_PROCESSING("ModelProcessing"),
    EXTRA_EXPENSES("ExtraExpenses"),
    WAREHOUSE("Warehouse"),
    WAREHOUSEOUTGOING("WarehouseOutgoing"),
    CUTTING_TERMINAL("CuttingTerminal"),
    MONTA_TERMINAL("MontaTerminal"),
    SHIPMENT_ORDER("ShipmentOrder");


    private String menuAlias;

    private MenuEnum(String menuAlias) {
        this.menuAlias = menuAlias;
    }

    public String getMenuAlias() {
        return this.menuAlias;
    }
}
