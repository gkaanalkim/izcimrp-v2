package com.felislabs.izci.enums;

/**
 * Created by gunerkaanalkim on 07/03/2017.
 */
public enum AccountingMenuEnum {
    ACCOUNTING_DEFINITION("AccountingDefinition"),
    ACCOUNTING_CUSTOMER_DEFINITION("AccountingCustomerDefinition"),
    ACCOUNTING_CUSTOMER_CURRENT_ACOUNTS("AccountingCustomerCurrentAccounts"),
    SUPPLIER_DEFINITION("SupplierCustomerDefinition"),
    SUPPLIER_CURRENT_ACOUNTS("SupplierCurrentAccounts"),
    TAX("Tax"),
    BRAND("Brand"),
    CATEGORY("Category"),
    PRODUCT("Product"),
    ACCOUNT("Account"),

    SELLING_DEFINITION("SellingDefinition"),
    BID("Bid"),
    BILLING("Billing"),
    ACCOUNTING_CUSTOMER("AccountingCustomer"),
    ACCOUNTING_SUPPLIER("AccountingSupplier"),

    BUYING_DEFINITION("BuyingDefinition"),
    BUYING_OPERATION("BuyingOperation"),

    RETURN_DEFINITION("ReturnDefinition"),
    RETURN_OPERATION("ReturnOperation");


    private String menuAlias;

    private AccountingMenuEnum(String menuAlias) {
        this.menuAlias = menuAlias;
    }

    public String getMenuAlias() {
        return this.menuAlias;
    }
}
