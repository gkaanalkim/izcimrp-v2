package com.felislabs.izci.enums;

/**
 * Created by gunerkaanalkim on 09/09/15.
 */
public enum PermissionEnum {
    ROLE_CREATE("RoleCreate"),
    ROLE_READ("RoleRead"),
    ROLE_UPDATE("RoleUpdate"),
    ROLE_DELETE("RoleDelete"),
    ROLE_ASSING("RoleAssing"),

    CUSTOMER_CREATE("CustomerCreate"),
    CUSTOMER_READ("CustomerRead"),
    CUSTOMER_UPDATE("CustomerUpdate"),
    CUSTOMER_DELETE("CustomerDelete"),

    STOCK_CREATE("StockCreate"),
    STOCK_READ("StockRead"),
    STOCK_UPDATE("StockUpdate"),
    STOCK_DELETE("StockDelete"),

    USER_CREATE("UserCreate"),
    USER_READ("UserRead"),
    USER_UPDATE("UserUpdate"),
    USER_DELETE("UserDelete"),

    MENU_READ("MenuRead"),
    MENU_CREATE("MenuCreate"),
    MENU_UPDATE("MenuUpdate"),
    MENU_DELETE("MenuDelete"),
    MENU_ASSING("MenuAssing"),
    MENU_ASSING_BACK("MenuAssingBack"),

    SETTING_CREATE("SettingCreate"),
    SETTING_READ("SettingRead"),
    SETTING_UPDATE("SettingUpdate"),
    SETTING_DELETE("SettingDelete"),

    PROFILE_READ("ProfileRead"),
    PROFILE_UPDATE("ProfileUpdate"),

    CURRENCY_READ("CurrencyRead"),

    COMPANY_CREATE("CompanyCreate"),
    COMPANY_READ("CompanyRead"),
    COMPANY_UPDATE("CompanyUpdate"),
    COMPANY_DELETE("CompanyDelete"),
    COMPANY_SELF_UPDATE("CompanySelfDelete"),

    PERMISSION_READ("PERMISSION_READ"),
    PERMISSION_ASSING("PERMISSION_ASSING"),

    MODEL_CREATE("ModelCreate"),
    MODEL_READ("ModelRead"),
    MODEL_UPDATE("ModelUpdate"),
    MODEL_DELETE("Modelelete"),
    MODEL_PRICE("ModelPrice"),

    MESSAGE_CREATE("MessageCreate"),
    MESSAGE_READ("MessageRead"),
    MESSAGE_UPDATE("MessageUpdate"),
    MESSAGE_DELETE("MessageDelete"),

    SUPPLY("Supply"),

    PRODUCTION_LINE_CREATE("ProdustionLineCreate"),
    PRODUCTION_LINE_READ("ProdustionLineRead"),
    PRODUCTION_LINE_UPDATE("ProdustionLineUpdate"),
    PRODUCTION_LINE_DELETE("ProdustionLineDelete"),

    MATERIAL_TYPE_CREATE("MaterialTypeCreate"),
    MATERIAL_TYPE_READ("MaterialTypeRead"),
    MATERIAL_TYPE_UPDATE("MaterialTypeUpdate"),
    MATERIAL_TYPE_DELETE("MaterialTypeDelete"),

    INTERNAL_CRITICAL_CREATE("InternalCriticalCreate"),
    INTERNAL_CRITICAL_READ("InternalCriticalRead"),
    INTERNAL_CRITICAL_UPDATE("InternalCriticalUpdate"),
    INTERNAL_CRITICAL_DELETE("InternalCriticalDelete"),

    SUPPLIER_CREATE("SupplierCreate"),
    SUPPLIER_READ("SupplierRead"),
    SUPPLIER_UPDATE("SupplierUpdate"),
    SUPPLIER_DELETE("SupplierDelete"),

    STOCK_TYPE_CREATE("RawMaterialTypeCreate"),
    STOCK_TYPE_READ("RawMaterialTypeRead"),
    STOCK_TYPE_UPDATE("RawMaterialTypeUpdate"),
    STOCK_TYPE_DELETE("RawMaterialTypeDelete"),

    PRODUCTION_SCHEDULE_CREATE("ProductionScheduleCreate"),
    PRODUCTION_SCHEDULE_READ("ProductionScheduleRead"),
    PRODUCTION_SCHEDULE_UPDATE("ProductionScheduleUpdate"),
    PRODUCTION_SCHEDULE_DELETE("ProductionScheduleDelete"),

    SAYACI_CREATE("SayaciCreate"),
    SAYACI_READ("SayaciRead"),
    SAYACI_UPDATE("SayaciUpdate"),
    SAYACI_DELETE("SayaciDelete"),

    SAYA_CREATE("SayaCreate"),
    SAYA_READ("SayaRead"),
    SAYA_UPDATE("SayaUpdate"),
    SAYA_DELETE("SayaDelete"),

    SUPPLY_CREATE("SupplyCreate"),
    SUPPLY_READ("SupplyRead"),
    SUPPLY_UPDATE("SupplyUpdate"),
    SUPPLY_DELETE("SupplyDelete"),

    ACTIVITY_REPORT_CREATE("ActivityReportCreate"),
    ACTIVITY_REPORT_READ("ActivityReportRead"),
    ACTIVITY_REPORT_UPDATE("ActivityReportUpdate"),
    ACTIVITY_REPORT_DELETE("ActivityReportDelete"),

    SHIPMENT_INFO_CREATE("ShipmentInfoCreate"),
    SHIPMENT_INFO_READ("ShipmentInfoRead"),
    SHIPMENT_INFO_UPDATE("ShipmentInfoUpdate"),
    SHIPMENT_INFO_DELETE("ShipmentInfoDelete"),

    CUTTING_ORDER_UPDATE("CuttingOrderUpdate"),
    CUTTING_ORDER_DELETE("CuttingOrderDelete"),

    MONTA_CREATE("MontaCreate"),
    MONTA_READ("MontaRead"),
    MONTA_UPDATE("MontaUpdate"),
    MONTA_DELETE("MontaDelete"),

    STOCK_ACCEPTANCE_CREATE("StockAcceptanceCreate"),
    STOCK_ACCEPTANCE_READ("StockAcceptanceRead"),
    STOCK_ACCEPTANCE_UPDATE("StockAcceptanceUpdate"),
    STOCK_ACCEPTANCE_DELETE("StockAcceptanceDelete"),

    REPORT_READ("ReportRead"),

    FAIR_MODEL_ASSORT_CREATE("FairModelAssortCreate"),
    FAIR_MODEL_ASSORT_READ("FairModelAssortRead"),
    FAIR_MODEL_ASSORT_UPDATE("FairModelAssortUpdate"),
    FAIR_MODEL_ASSORT_DELETE("FairModelAssortDelete"),

    FAIR_MODEL_PRICE_CREATE("FairModelPriceCreate"),
    FAIR_MODEL_PRICE_READ("FairModelPriceRead"),
    FAIR_MODEL_PRICE_UPDATE("FairModelPriceUpdate"),
    FAIR_MODEL_PRICE_DELETE("FairModelPriceDelete"),

    COPY_ASSORT("CopyAssort"),
    COPY_ASSORT_TO_ALL("CopyAssortToAll"),
    COPY_PRICE("CopyPrice"),

    PROCESSING_CREATE("ProcessingCreate"),
    PROCESSING_READ("ProcessingRead"),
    PROCESSING_UPDATE("ProcessingUpdate"),
    PROCESSING_DELETE("ProcessingDelete"),

    CONTRACTED_MANUFACTURER_CREATE("ContactedManufacturerCreate"),
    CONTRACTED_MANUFACTURER_READ("ContactedManufacturerRead"),
    CONTRACTED_MANUFACTURER_UPDATE("ContactedManufacturerUpdate"),
    CONTRACTED_MANUFACTURER_DELETE("ContactedManufacturerDelete"),

    DEPARTMENT_CREATE("DepartmentCreate"),
    DEPARTMENT_READ("DepartmentRead"),
    DEPARTMENT_UPDATE("DepartmentUpdate"),
    DEPARTMENT_DELETE("DepartmentDelete"),

    STAFF_CREATE("StaffCreate"),
    STAFF_READ("StaffRead"),
    STAFF_UPDATE("StaffUpdate"),
    STAFF_DELETE("StaffDelete"),

    MODEL_PROCESSING_CREATE("ModelProcessingCreate"),
    MODEL_PROCESSING_READ("ModelProcessingRead"),
    MODEL_PROCESSING_UPDATE("ModelProcessingUpdate"),
    MODEL_PROCESSING_DELETE("ModelProcessingDelete"),

    MODEL_PROCESSING_INCOMING_CREATE("ModelProcessingIncomingCreate"),
    MODEL_PROCESSING_INCOMING_READ("ModelProcessingIncomingRead"),
    MODEL_PROCESSING_INCOMING_UPDATE("ModelProcessingIncomingUpdate"),
    MODEL_PROCESSING_INCOMING_DELETE("ModelProcessingIncomingDelete"),

    FAIR_ORDER_CREATE("FairOrderCreate"),
    FAIR_ORDER_READ("FairOrderRead"),
    FAIR_ORDER_UPDATE("FairOrderUpdate"),
    FAIR_ORDER_DELETE("FairOrderDelete"),


    EXTRA_EXPENSES_CREATE("ExtraExpensesCreate"),
    EXTRA_EXPENSES_READ("ExtraExpensesRead"),
    EXTRA_EXPENSES_UPDATE("ExtraExpensesUpdate"),
    EXTRA_EXPENSES_DELETE("ExtraExpensesDelete"),

    CUTTING_REQUIREMENT_CREATE("CuttingRequiermentCreate"),
    CUTTING_REQUIREMENT_READ("CuttingRequiermentRead"),

    MONTA_REQUIREMENT_CREATE("MontaRequiermentCreate"),
    MONTA_REQUIREMENT_READ("MontaRequiermentRead"),

    WAREHOUSE_CREATE("WarehouseCreate"),
    WAREHOUSE_READ("WarehouseRead"),
    WAREHOUSE_UPDATE("WarehouseUpdate"),
    WAREHOUSE_DELETE("WarehouseDelete"),

    PURCASE_ORDER_CREATE("PurcaseOrderCreate"),
    PURCASE_ORDER_READ("PurcaseOrderRead"),
    PURCASE_ORDER_UPDATE("PurcaseOrderUpdate"),
    PURCASE_ORDER_DELETE("PurcaseOrderDelete"),

    STOCK_INCREASE("StockIncrease"),
    STOCK_DECREASE("StockDecrease"),

    SHIPMENT_ORDER_CREATE("ShipmentOrderCreate"),
    SHIPMENT_ORDER_READ("ShipmentOrderRead"),
    SHIPMENT_ORDER_UPDATE("ShipmentOrderUpdate"),
    SHIPMENT_ORDER_DELETE("ShipmentOrderDelete");

    private String permissionAlias;

    private PermissionEnum(String permissionAlias) {
        this.permissionAlias = permissionAlias;
    }

    public String getPermissionAlias() {
        return permissionAlias;
    }
}
