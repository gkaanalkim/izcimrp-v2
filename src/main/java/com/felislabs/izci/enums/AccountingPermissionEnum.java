package com.felislabs.izci.enums;

/**
 * Created by gunerkaanalkim on 07/03/2017.
 */
public enum AccountingPermissionEnum {
    ACCOUNTING_CUSTOMER_CREATE("AccountingCustomerCreate"),
    ACCOUNTING_CUSTOMER_READ("AccountingCustomerRead"),
    ACCOUNTING_CUSTOMER_UPDATE("AccountingCustomerUpdate"),
    ACCOUNTING_CUSTOMER_DELETE("AccountingCustomerDelete"),

    TAX_CREATE("TaxCreate"),
    TAX_READ("TaxRead"),
    TAX_UPDATE("TaxUpdate"),
    TAX_DELETE("TaxDelete"),

    BRAND_CREATE("BrandCreate"),
    BRAND_READ("BrandRead"),
    BRAND_UPDATE("BrandUpdate"),
    BRAND_DELETE("BrandDelete"),

    CATEGORY_CREATE("CategoryCreate"),
    CATEGORY_READ("CategoryRead"),
    CATEGORY_UPDATE("CategoryUpdate"),
    CATEGORY_DELETE("CategoryDelete"),

    PRODUCT_CREATE("ProductCreate"),
    PRODUCT_READ("ProductRead"),
    PRODUCT_UPDATE("ProductUpdate"),
    PRODUCT_DELETE("ProductDelete"),

    BID_CREATE("BidCreate"),
    BID_READ("BidRead"),
    BID_UPDATE("BidUpdate"),
    BID_DELETE("BidDelete"),

    INVOICE_CREATE("InvoiceCreate"),
    INVOICE_READ("InvoiceRead"),
    INVOICE_UPDATE("InvoiceUpdate"),
    INVOICE_DELETE("InvoiceDelete"),

    PROFORMA_CREATE("ProformaCreate"),
    PROFORMA_READ("ProformaRead"),
    PROFORMA_UPDATE("ProformaUpdate"),
    PROFORMA_DELETE("ProformaDelete"),

    CONSIGNMENT_INVOCE_CREATE("ConsignmentInvoiceCreate"),
    CONSIGNMENT_INVOCE_READ("ConsignmentInvoiceRead"),
    CONSIGNMENT_INVOCE_UPDATE("ConsignmentInvoiceUpdate"),
    CONSIGNMENT_INVOCE_DELETE("ConsignmentInvoiceDelete"),

    ACCOUNT_CREATE("AccountCreate"),
    ACCOUNT_READ("AccountRead"),
    ACCOUNT_UPDATE("AccountUpdate"),
    ACCOUNT_DELETE("AccountDelete"),

    PROCEEDS_CREATE("ProceedsCreate"),
    PROCEEDS_READ("ProceedsRead"),
    PROCEEDS_UPDATE("ProceedsUpdate"),
    PROCEEDS_DELETE("ProceedsDelete"),

    CHECK_AND_BILL_CREATE("CheckAndBillCreate"),
    CHECK_AND_BILL_READ("CheckAndBillRead"),
    CHECK_AND_BILL_UPDATE("CheckAndBillUpdate"),
    CHECK_AND_BILL_DELETE("CheckAndBillDelete");

    private String permissionAlias;

    private AccountingPermissionEnum(String permissionAlias) {
        this.permissionAlias = permissionAlias;
    }

    public String getPermissionAlias() {
        return permissionAlias;
    }
}
