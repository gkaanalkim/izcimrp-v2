package com.felislabs.izci.cli;

import com.felislabs.izci.MrpConfiguration;
import com.felislabs.izci.enums.AccountingMenuEnum;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/03/2017.
 */
public class AccountingInitializeCommand<T extends MrpConfiguration> extends EnvironmentCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InitializeCommand.class);
    private HibernateBundle hibernateBundle;

    public AccountingInitializeCommand(Application application, HibernateBundle hibernameBundle) {
        super(application, "accounting", "Runs accounting initial command for initial process.");
        this.hibernateBundle = hibernameBundle;
    }

    @Override
    protected void run(Environment environment, Namespace namespace, T t) throws Exception {
        LOGGER.info("Accounting Initialize Starting...");
        LOGGER.info("Starting to create accounting initial data.");
        execute(t);
    }

    @UnitOfWork
    public void execute(Configuration configuration) {
        final Session session = hibernateBundle.getSessionFactory().openSession();

        this.createAccountingMenu(session);
        this.createAccountingPermission(session);
        session.close();
    }

    public void createAccountingMenu(Session session) {
        int counter = 0;

        Menu definitionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.ACCOUNTING_DEFINITION.getMenuAlias())).uniqueResult();

        if (definitionMenu == null) {
            definitionMenu = new Menu();
            definitionMenu.setName("Tanımlamalar");
            definitionMenu.setCode(AccountingMenuEnum.ACCOUNTING_DEFINITION.getMenuAlias());
            definitionMenu.setDescription("Muhasebe tanımlamaları ekranlarının en üst adımıdır.");
            definitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(definitionMenu);
        } else {
            definitionMenu.setName("Tanımlamalar");
            definitionMenu.setCode(AccountingMenuEnum.ACCOUNTING_DEFINITION.getMenuAlias());
            definitionMenu.setDescription("Muhasebe tanımlamaları ekranlarının en üst adımıdır.");
            definitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(definitionMenu);
        }

        Menu customerDefinitionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.ACCOUNTING_CUSTOMER_DEFINITION.getMenuAlias())).uniqueResult();

        if (customerDefinitionMenu == null) {
            customerDefinitionMenu = new Menu();
            customerDefinitionMenu.setName("Müşteriler");
            customerDefinitionMenu.setCode(AccountingMenuEnum.ACCOUNTING_CUSTOMER_DEFINITION.getMenuAlias());
            customerDefinitionMenu.setDescription("Muhasebe > Muhasebe müşteri ekleme, güncelleme, silme ve arama sayfasıdır.");
            customerDefinitionMenu.setUrl("musteri-tanimla.html");
            customerDefinitionMenu.setParentOid(definitionMenu.getOid());
            customerDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(customerDefinitionMenu);
        } else {
            customerDefinitionMenu.setName("Müşteriler");
            customerDefinitionMenu.setCode(AccountingMenuEnum.ACCOUNTING_CUSTOMER_DEFINITION.getMenuAlias());
            customerDefinitionMenu.setDescription("Muhasebe müşteri ekleme, güncelleme, silme ve arama sayfasıdır.");
            customerDefinitionMenu.setUrl("musteri-tanimla.html");
            customerDefinitionMenu.setParentOid(definitionMenu.getOid());
            customerDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(customerDefinitionMenu);
        }

        Menu supplierDefinitionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.SUPPLIER_DEFINITION.getMenuAlias())).uniqueResult();

        if (supplierDefinitionMenu == null) {
            supplierDefinitionMenu = new Menu();
            supplierDefinitionMenu.setName("Tedarikçiler");
            supplierDefinitionMenu.setCode(AccountingMenuEnum.SUPPLIER_DEFINITION.getMenuAlias());
            supplierDefinitionMenu.setDescription("Muhasebe > Tedarikçi ekleme, güncelleme, silme ve arama sayfasıdır.");
            supplierDefinitionMenu.setUrl("tedarikci-tanimla.html");
            supplierDefinitionMenu.setParentOid(definitionMenu.getOid());
            supplierDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(supplierDefinitionMenu);
        } else {
            supplierDefinitionMenu.setName("Tedarikçiler");
            supplierDefinitionMenu.setCode(AccountingMenuEnum.SUPPLIER_DEFINITION.getMenuAlias());
            supplierDefinitionMenu.setDescription("Muhasebe > Tedarikçi ekleme, güncelleme, silme ve arama sayfasıdır.");
            supplierDefinitionMenu.setUrl("tedarikci-tanimla.html");
            supplierDefinitionMenu.setParentOid(definitionMenu.getOid());
            supplierDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(supplierDefinitionMenu);
        }

        Menu taxMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.TAX.getMenuAlias())).uniqueResult();

        if (taxMenu == null) {
            taxMenu = new Menu();
            taxMenu.setName("Vergiler");
            taxMenu.setCode(AccountingMenuEnum.TAX.getMenuAlias());
            taxMenu.setDescription("Muhasebe > Vergi ekleme, güncelleme, silme ve arama sayfasıdır.");
            taxMenu.setUrl("vergi.html");
            taxMenu.setParentOid(definitionMenu.getOid());
            taxMenu.setMenuOrder(counter);
            counter++;
            session.persist(taxMenu);
        } else {
            taxMenu.setName("Vergiler");
            taxMenu.setCode(AccountingMenuEnum.TAX.getMenuAlias());
            taxMenu.setDescription("Muhasebe > Vergi ekleme, güncelleme, silme ve arama sayfasıdır.");
            taxMenu.setUrl("vergi.html");
            taxMenu.setParentOid(definitionMenu.getOid());
            taxMenu.setMenuOrder(counter);
            counter++;
            session.persist(taxMenu);
        }

        Menu brandMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.BRAND.getMenuAlias())).uniqueResult();

        if (brandMenu == null) {
            brandMenu = new Menu();
            brandMenu.setName("Markalar");
            brandMenu.setCode(AccountingMenuEnum.BRAND.getMenuAlias());
            brandMenu.setDescription("Muhasebe > Marka ekleme, güncelleme, silme ve arama sayfasıdır.");
            brandMenu.setUrl("marka.html");
            brandMenu.setParentOid(definitionMenu.getOid());
            brandMenu.setMenuOrder(counter);
            counter++;
            session.persist(brandMenu);
        } else {
            brandMenu.setName("Markalar");
            brandMenu.setCode(AccountingMenuEnum.BRAND.getMenuAlias());
            brandMenu.setDescription("Muhasebe > Marka ekleme, güncelleme, silme ve arama sayfasıdır.");
            brandMenu.setUrl("marka.html");
            brandMenu.setParentOid(definitionMenu.getOid());
            brandMenu.setMenuOrder(counter);
            counter++;
            session.persist(brandMenu);
        }

        Menu categoryMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.CATEGORY.getMenuAlias())).uniqueResult();

        if (categoryMenu == null) {
            categoryMenu = new Menu();
            categoryMenu.setName("Kategoriler");
            categoryMenu.setCode(AccountingMenuEnum.CATEGORY.getMenuAlias());
            categoryMenu.setDescription("Muhasebe > Kategori ekleme, güncelleme, silme ve arama sayfasıdır.");
            categoryMenu.setUrl("kategori.html");
            categoryMenu.setParentOid(definitionMenu.getOid());
            categoryMenu.setMenuOrder(counter);
            counter++;
            session.persist(categoryMenu);
        } else {
            categoryMenu.setName("Kategoriler");
            categoryMenu.setCode(AccountingMenuEnum.CATEGORY.getMenuAlias());
            categoryMenu.setDescription("Muhasebe > Kategori ekleme, güncelleme, silme ve arama sayfasıdır.");
            categoryMenu.setUrl("kategori.html");
            categoryMenu.setParentOid(definitionMenu.getOid());
            categoryMenu.setMenuOrder(counter);
            counter++;
            session.persist(categoryMenu);
        }

        Menu productMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.PRODUCT.getMenuAlias())).uniqueResult();

        if (productMenu == null) {
            productMenu = new Menu();
            productMenu.setName("Ürünler");
            productMenu.setCode(AccountingMenuEnum.PRODUCT.getMenuAlias());
            productMenu.setDescription("Muhasebe > Ürün ekleme, güncelleme, silme ve arama sayfasıdır.");
            productMenu.setUrl("urun.html");
            productMenu.setParentOid(definitionMenu.getOid());
            productMenu.setMenuOrder(counter);
            counter++;
            session.persist(productMenu);
        } else {
            productMenu.setName("Ürünler");
            productMenu.setCode(AccountingMenuEnum.PRODUCT.getMenuAlias());
            productMenu.setDescription("Muhasebe > Ürün ekleme, güncelleme, silme ve arama sayfasıdır.");
            productMenu.setUrl("urun.html");
            productMenu.setParentOid(definitionMenu.getOid());
            productMenu.setMenuOrder(counter);
            counter++;
            session.persist(productMenu);
        }

        Menu accountMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.ACCOUNT.getMenuAlias())).uniqueResult();

        if (accountMenu == null) {
            accountMenu = new Menu();
            accountMenu.setName("Hesaplar");
            accountMenu.setCode(AccountingMenuEnum.ACCOUNT.getMenuAlias());
            accountMenu.setDescription("Muhasebe > Hesap ekleme, güncelleme, silme ve arama sayfasıdır.");
            accountMenu.setUrl("hesap.html");
            accountMenu.setParentOid(definitionMenu.getOid());
            accountMenu.setMenuOrder(counter);
            counter++;
            session.persist(accountMenu);
        } else {
            accountMenu.setName("Hesaplar");
            accountMenu.setCode(AccountingMenuEnum.ACCOUNT.getMenuAlias());
            accountMenu.setDescription("Muhasebe > Hesap ekleme, güncelleme, silme ve arama sayfasıdır.");
            accountMenu.setUrl("hesap.html");
            accountMenu.setParentOid(definitionMenu.getOid());
            accountMenu.setMenuOrder(counter);
            counter++;
            session.persist(accountMenu);
        }

        Menu accountingCustomerMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.ACCOUNTING_CUSTOMER.getMenuAlias())).uniqueResult();

        if (accountingCustomerMenu == null) {
            accountingCustomerMenu = new Menu();
            accountingCustomerMenu.setName("Müşteri");
            accountingCustomerMenu.setCode(AccountingMenuEnum.ACCOUNTING_CUSTOMER.getMenuAlias());
            accountingCustomerMenu.setDescription("Muhasebe > Müşteri cari işlemleri en üst adımıdır.");
            accountingCustomerMenu.setMenuOrder(counter);
            counter++;
            session.persist(accountingCustomerMenu);
        } else {
            accountingCustomerMenu.setName("Müşteri");
            accountingCustomerMenu.setCode(AccountingMenuEnum.ACCOUNTING_CUSTOMER.getMenuAlias());
            accountingCustomerMenu.setDescription("Muhasebe > Müşteri cari işlemleri en üst adımıdır.");
            accountingCustomerMenu.setMenuOrder(counter);
            counter++;
            session.persist(accountingCustomerMenu);
        }

        Menu accountCustomerCurrentAccounts = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.ACCOUNTING_CUSTOMER_CURRENT_ACOUNTS.getMenuAlias())).uniqueResult();

        if (accountCustomerCurrentAccounts == null) {
            accountCustomerCurrentAccounts = new Menu();
            accountCustomerCurrentAccounts.setName("Hesap İşlemleri");
            accountCustomerCurrentAccounts.setCode(AccountingMenuEnum.ACCOUNTING_CUSTOMER_CURRENT_ACOUNTS.getMenuAlias());
            accountCustomerCurrentAccounts.setDescription("Muhasebe > Müşterinin hesap işlemleri sayfasıdır.");
            accountCustomerCurrentAccounts.setUrl("musteri-hesap-islemleri.html");
            accountCustomerCurrentAccounts.setParentOid(accountingCustomerMenu.getOid());
            accountCustomerCurrentAccounts.setMenuOrder(counter);
            counter++;
            session.persist(accountCustomerCurrentAccounts);
        } else {
            accountCustomerCurrentAccounts.setName("Hesap İşlemleri");
            accountCustomerCurrentAccounts.setCode(AccountingMenuEnum.ACCOUNTING_CUSTOMER_CURRENT_ACOUNTS.getMenuAlias());
            accountCustomerCurrentAccounts.setDescription("Muhasebe > Müşterinin hesap işlemleri sayfasıdır.");
            accountCustomerCurrentAccounts.setUrl("musteri-hesap-islemleri.html");
            accountCustomerCurrentAccounts.setParentOid(accountingCustomerMenu.getOid());
            accountCustomerCurrentAccounts.setMenuOrder(counter);
            counter++;
            session.persist(accountCustomerCurrentAccounts);
        }

        Menu accountingSupplierMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.ACCOUNTING_SUPPLIER.getMenuAlias())).uniqueResult();

        if (accountingSupplierMenu == null) {
            accountingSupplierMenu = new Menu();
            accountingSupplierMenu.setName("Tedarikçi");
            accountingSupplierMenu.setCode(AccountingMenuEnum.ACCOUNTING_SUPPLIER.getMenuAlias());
            accountingSupplierMenu.setDescription("Muhasebe > Tedarikçi cari işlemleri en üst adımıdır.");
            accountingSupplierMenu.setMenuOrder(counter);
            counter++;
            session.persist(accountingSupplierMenu);
        } else {
            accountingSupplierMenu.setName("Tedarikçi");
            accountingSupplierMenu.setCode(AccountingMenuEnum.ACCOUNTING_SUPPLIER.getMenuAlias());
            accountingSupplierMenu.setDescription("Muhasebe > Tedarikçi cari işlemleri en üst adımıdır.");
            accountingSupplierMenu.setMenuOrder(counter);
            counter++;
            session.persist(accountingSupplierMenu);
        }

        Menu supplierCurrentAccounts = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.SUPPLIER_CURRENT_ACOUNTS.getMenuAlias())).uniqueResult();

        if (supplierCurrentAccounts == null) {
            supplierCurrentAccounts = new Menu();
            supplierCurrentAccounts.setName("Hesap İşlemleri");
            supplierCurrentAccounts.setCode(AccountingMenuEnum.SUPPLIER_CURRENT_ACOUNTS.getMenuAlias());
            supplierCurrentAccounts.setDescription("Muhasebe > Tedarikçinin hesap işlemleri sayfasıdır.");
            supplierCurrentAccounts.setUrl("tedarikci-hesap-islemleri.html");
            supplierCurrentAccounts.setParentOid(accountingSupplierMenu.getOid());
            supplierCurrentAccounts.setMenuOrder(counter);
            counter++;
            session.persist(supplierCurrentAccounts);
        } else {
            supplierCurrentAccounts.setName("Hesap İşlemleri");
            supplierCurrentAccounts.setCode(AccountingMenuEnum.SUPPLIER_CURRENT_ACOUNTS.getMenuAlias());
            supplierCurrentAccounts.setDescription("Muhasebe > Tedarikçinin hesap işlemleri sayfasıdır.");
            supplierCurrentAccounts.setUrl("tedarikci-hesap-islemleri.html");
            supplierCurrentAccounts.setParentOid(accountingSupplierMenu.getOid());
            supplierCurrentAccounts.setMenuOrder(counter);
            counter++;
            session.persist(supplierCurrentAccounts);
        }

        Menu sellingDefinitionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.SELLING_DEFINITION.getMenuAlias())).uniqueResult();

        if (sellingDefinitionMenu == null) {
            sellingDefinitionMenu = new Menu();
            sellingDefinitionMenu.setName("Satış");
            sellingDefinitionMenu.setCode(AccountingMenuEnum.SELLING_DEFINITION.getMenuAlias());
            sellingDefinitionMenu.setDescription("Muhasebe > Satış işlemleri en üst adımıdır.");
            sellingDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(sellingDefinitionMenu);
        } else {
            sellingDefinitionMenu.setName("Satış");
            sellingDefinitionMenu.setCode(AccountingMenuEnum.SELLING_DEFINITION.getMenuAlias());
            sellingDefinitionMenu.setDescription("Muhasebe > Satış işlemleri en üst adımıdır.");
            sellingDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(sellingDefinitionMenu);
        }

        Menu bid = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.BID.getMenuAlias())).uniqueResult();

        if (bid == null) {
            bid = new Menu();
            bid.setName("Fiyat Teklifi");
            bid.setCode(AccountingMenuEnum.BID.getMenuAlias());
            bid.setDescription("Muhasebe > Fiyat teklifi hazırlama, güncelleme, silme ve arama sayfasıdır.");
            bid.setUrl("fiyat-teklifi.html");
            bid.setParentOid(sellingDefinitionMenu.getOid());
            bid.setMenuOrder(counter);
            counter++;
            session.persist(bid);
        } else {
            bid.setName("Fiyat Teklifi");
            bid.setCode(AccountingMenuEnum.BID.getMenuAlias());
            bid.setDescription("Muhasebe > Fiyat teklifi hazırlama, güncelleme, silme ve arama sayfasıdır.");
            bid.setUrl("fiyat-teklifi.html");
            bid.setParentOid(sellingDefinitionMenu.getOid());
            bid.setMenuOrder(counter);
            counter++;
            session.persist(bid);
        }

        Menu billing = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.BILLING.getMenuAlias())).uniqueResult();

        if (billing == null) {
            billing = new Menu();
            billing.setName("Faturalandırma");
            billing.setCode(AccountingMenuEnum.BILLING.getMenuAlias());
            billing.setDescription("Muhasebe > Fatura hazırlama, güncelleme, silme ve arama sayfasıdır.");
            billing.setUrl("faturalandirma.html");
            billing.setParentOid(sellingDefinitionMenu.getOid());
            billing.setMenuOrder(counter);
            counter++;
            session.persist(billing);
        } else {
            billing.setName("Faturalandırma");
            billing.setCode(AccountingMenuEnum.BILLING.getMenuAlias());
            billing.setDescription("Muhasebe > Fatura hazırlama, güncelleme, silme ve arama sayfasıdır.");
            billing.setUrl("faturalandirma.html");
            billing.setParentOid(sellingDefinitionMenu.getOid());
            billing.setMenuOrder(counter);
            counter++;
            session.persist(billing);
        }

        Menu buyingDefinitionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.BUYING_DEFINITION.getMenuAlias())).uniqueResult();

        if (buyingDefinitionMenu == null) {
            buyingDefinitionMenu = new Menu();
            buyingDefinitionMenu.setName("Alış");
            buyingDefinitionMenu.setCode(AccountingMenuEnum.BUYING_DEFINITION.getMenuAlias());
            buyingDefinitionMenu.setDescription("Muhasebe > Alış işlemleri en üst adımıdır.");
            buyingDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(buyingDefinitionMenu);
        } else {
            buyingDefinitionMenu.setName("Alış");
            buyingDefinitionMenu.setCode(AccountingMenuEnum.BUYING_DEFINITION.getMenuAlias());
            buyingDefinitionMenu.setDescription("Muhasebe > Alış işlemleri en üst adımıdır.");
            buyingDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(buyingDefinitionMenu);
        }

        Menu buyingOperation = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.BUYING_OPERATION.getMenuAlias())).uniqueResult();

        if (buyingOperation == null) {
            buyingOperation = new Menu();
            buyingOperation.setName("Alış İşlemleri");
            buyingOperation.setCode(AccountingMenuEnum.BUYING_OPERATION.getMenuAlias());
            buyingOperation.setDescription("Muhasebe > Alış işlemleri sayfasıdır.");
            buyingOperation.setUrl("alis-islemleri.html");
            buyingOperation.setParentOid(buyingDefinitionMenu.getOid());
            buyingOperation.setMenuOrder(counter);
            counter++;
            session.persist(buyingOperation);
        } else {
            buyingOperation.setName("Alış İşlemleri");
            buyingOperation.setCode(AccountingMenuEnum.BUYING_OPERATION.getMenuAlias());
            buyingOperation.setDescription("Muhasebe > Alış işlemleri sayfasıdır.");
            buyingOperation.setUrl("alis-islemleri.html");
            buyingOperation.setParentOid(buyingDefinitionMenu.getOid());
            buyingOperation.setMenuOrder(counter);
            counter++;
            session.persist(buyingOperation);
        }

        Menu returnDefinitionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.RETURN_DEFINITION.getMenuAlias())).uniqueResult();

        if (returnDefinitionMenu == null) {
            returnDefinitionMenu = new Menu();
            returnDefinitionMenu.setName("İade");
            returnDefinitionMenu.setCode(AccountingMenuEnum.RETURN_DEFINITION.getMenuAlias());
            returnDefinitionMenu.setDescription("Muhasebe > İade işlemleri en üst adımıdır.");
            returnDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(returnDefinitionMenu);
        } else {
            returnDefinitionMenu.setName("İade");
            returnDefinitionMenu.setCode(AccountingMenuEnum.RETURN_DEFINITION.getMenuAlias());
            returnDefinitionMenu.setDescription("Muhasebe > İade işlemleri en üst adımıdır.");
            returnDefinitionMenu.setMenuOrder(counter);
            counter++;
            session.persist(returnDefinitionMenu);
        }

        Menu returnOperation = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("code", AccountingMenuEnum.RETURN_OPERATION.getMenuAlias())).uniqueResult();

        if (returnOperation == null) {
            returnOperation = new Menu();
            returnOperation.setName("İade İşlemleri");
            returnOperation.setCode(AccountingMenuEnum.RETURN_OPERATION.getMenuAlias());
            returnOperation.setDescription("Muhasebe > İade işlemleri sayfasıdır.");
            returnOperation.setUrl("alis-islemleri.html");
            returnOperation.setParentOid(returnDefinitionMenu.getOid());
            returnOperation.setMenuOrder(counter);
            counter++;
            session.persist(returnOperation);
        } else {
            returnOperation.setName("İade İşlemleri");
            returnOperation.setCode(AccountingMenuEnum.RETURN_OPERATION.getMenuAlias());
            returnOperation.setDescription("Muhasebe > İade işlemleri sayfasıdır.");
            returnOperation.setUrl("alis-islemleri.html");
            returnOperation.setParentOid(returnDefinitionMenu.getOid());
            returnOperation.setMenuOrder(counter);
            counter++;
            session.persist(returnOperation);
        }

        session.flush();
    }

    public void createAccountingPermission(Session session) {
        Permission accountingCustomer = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.ACCOUNTING_CUSTOMER_CREATE.getPermissionAlias())).uniqueResult();

        if (accountingCustomer == null) {
            accountingCustomer = new Permission();
            accountingCustomer.setName("Muhasebe müşterisi yaratır.");
            accountingCustomer.setCode(AccountingPermissionEnum.ACCOUNTING_CUSTOMER_CREATE.getPermissionAlias());
            session.persist(accountingCustomer);
        } else {
            accountingCustomer.setName("Muhasebe müşterisi yaratır.");
            accountingCustomer.setCode(AccountingPermissionEnum.ACCOUNTING_CUSTOMER_CREATE.getPermissionAlias());
            session.persist(accountingCustomer);
        }

        Permission accountingCustomerRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ.getPermissionAlias())).uniqueResult();

        if (accountingCustomerRead == null) {
            accountingCustomerRead = new Permission();
            accountingCustomerRead.setName("Muhasebe müşterisi görüntüler.");
            accountingCustomerRead.setCode(AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ.getPermissionAlias());
            session.persist(accountingCustomerRead);
        } else {
            accountingCustomerRead.setName("Muhasebe müşterisi görüntüler.");
            accountingCustomerRead.setCode(AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ.getPermissionAlias());
            session.persist(accountingCustomerRead);
        }

        Permission accountingCustomerUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.ACCOUNTING_CUSTOMER_UPDATE.getPermissionAlias())).uniqueResult();

        if (accountingCustomerUpdate == null) {
            accountingCustomerUpdate = new Permission();
            accountingCustomerUpdate.setName("Muhasebe müşterisi görüntüler.");
            accountingCustomerUpdate.setCode(AccountingPermissionEnum.ACCOUNTING_CUSTOMER_UPDATE.getPermissionAlias());
            session.persist(accountingCustomerUpdate);
        } else {
            accountingCustomerUpdate.setName("Muhasebe müşterisi görüntüler.");
            accountingCustomerUpdate.setCode(AccountingPermissionEnum.ACCOUNTING_CUSTOMER_UPDATE.getPermissionAlias());
            session.persist(accountingCustomerUpdate);
        }

        Permission accountingDeleteUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.ACCOUNTING_CUSTOMER_DELETE.getPermissionAlias())).uniqueResult();

        if (accountingDeleteUpdate == null) {
            accountingDeleteUpdate = new Permission();
            accountingDeleteUpdate.setName("Muhasebe müşterisi siler.");
            accountingDeleteUpdate.setCode(AccountingPermissionEnum.ACCOUNTING_CUSTOMER_DELETE.getPermissionAlias());
            session.persist(accountingDeleteUpdate);
        } else {
            accountingDeleteUpdate.setName("Muhasebe müşterisi siler.");
            accountingDeleteUpdate.setCode(AccountingPermissionEnum.ACCOUNTING_CUSTOMER_DELETE.getPermissionAlias());
            session.persist(accountingDeleteUpdate);
        }

        Permission taxCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.TAX_CREATE.getPermissionAlias())).uniqueResult();

        if (taxCreate == null) {
            taxCreate = new Permission();
            taxCreate.setName("Vergi kalemi oluşturabilir.");
            taxCreate.setCode(AccountingPermissionEnum.TAX_CREATE.getPermissionAlias());
            session.persist(taxCreate);
        } else {
            taxCreate.setName("Vergi kalemi oluşturabilir.");
            taxCreate.setCode(AccountingPermissionEnum.TAX_CREATE.getPermissionAlias());
            session.persist(taxCreate);
        }

        Permission taxRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.TAX_READ.getPermissionAlias())).uniqueResult();

        if (taxRead == null) {
            taxRead = new Permission();
            taxRead.setName("Vergi kalemi görüntüleyebilir.");
            taxRead.setCode(AccountingPermissionEnum.TAX_READ.getPermissionAlias());
            session.persist(taxRead);
        } else {
            taxRead.setName("Vergi kalemi görüntüleyebilir.");
            taxRead.setCode(AccountingPermissionEnum.TAX_READ.getPermissionAlias());
            session.persist(taxRead);
        }

        Permission taxUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.TAX_UPDATE.getPermissionAlias())).uniqueResult();

        if (taxUpdate == null) {
            taxUpdate = new Permission();
            taxUpdate.setName("Vergi kalemi güncelleyebilir.");
            taxUpdate.setCode(AccountingPermissionEnum.TAX_UPDATE.getPermissionAlias());
            session.persist(taxUpdate);
        } else {
            taxUpdate.setName("Vergi kalemi güncelleyebilir.");
            taxUpdate.setCode(AccountingPermissionEnum.TAX_UPDATE.getPermissionAlias());
            session.persist(taxUpdate);
        }

        Permission taxDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.TAX_DELETE.getPermissionAlias())).uniqueResult();

        if (taxDelete == null) {
            taxDelete = new Permission();
            taxDelete.setName("Vergi kalemi silebilir.");
            taxDelete.setCode(AccountingPermissionEnum.TAX_DELETE.getPermissionAlias());
            session.persist(taxDelete);
        } else {
            taxDelete.setName("Vergi kalemi silebilir.");
            taxDelete.setCode(AccountingPermissionEnum.TAX_DELETE.getPermissionAlias());
            session.persist(taxDelete);
        }

        Permission brandCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.BRAND_CREATE.getPermissionAlias())).uniqueResult();

        if (brandCreate == null) {
            brandCreate = new Permission();
            brandCreate.setName("Marka oluşturabilir.");
            brandCreate.setCode(AccountingPermissionEnum.BRAND_CREATE.getPermissionAlias());
            session.persist(brandCreate);
        } else {
            brandCreate.setName("Marka oluşturabilir.");
            brandCreate.setCode(AccountingPermissionEnum.BRAND_CREATE.getPermissionAlias());
            session.persist(brandCreate);
        }

        Permission brandRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.BRAND_READ.getPermissionAlias())).uniqueResult();

        if (brandRead == null) {
            brandRead = new Permission();
            brandRead.setName("Marka görüntüleyebilir.");
            brandRead.setCode(AccountingPermissionEnum.BRAND_READ.getPermissionAlias());
            session.persist(brandRead);
        } else {
            brandRead.setName("Marka görüntüleyebilir.");
            brandRead.setCode(AccountingPermissionEnum.BRAND_READ.getPermissionAlias());
            session.persist(brandRead);
        }

        Permission brandUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.BRAND_UPDATE.getPermissionAlias())).uniqueResult();

        if (brandUpdate == null) {
            brandUpdate = new Permission();
            brandUpdate.setName("Marka güncelleyebilir.");
            brandUpdate.setCode(AccountingPermissionEnum.BRAND_UPDATE.getPermissionAlias());
            session.persist(brandUpdate);
        } else {
            brandUpdate.setName("Marka güncelleyebilir.");
            brandUpdate.setCode(AccountingPermissionEnum.BRAND_UPDATE.getPermissionAlias());
            session.persist(brandUpdate);
        }

        Permission brandDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.BRAND_DELETE.getPermissionAlias())).uniqueResult();

        if (brandDelete == null) {
            brandDelete = new Permission();
            brandDelete.setName("Marka silebilir.");
            brandDelete.setCode(AccountingPermissionEnum.BRAND_DELETE.getPermissionAlias());
            session.persist(brandDelete);
        } else {
            brandDelete.setName("Marka silebilir.");
            brandDelete.setCode(AccountingPermissionEnum.BRAND_DELETE.getPermissionAlias());
            session.persist(brandDelete);
        }

        Permission categoryCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CATEGORY_CREATE.getPermissionAlias())).uniqueResult();

        if (categoryCreate == null) {
            categoryCreate = new Permission();
            categoryCreate.setName("Kategori oluşturabilir.");
            categoryCreate.setCode(AccountingPermissionEnum.CATEGORY_CREATE.getPermissionAlias());
            session.persist(categoryCreate);
        } else {
            categoryCreate.setName("Kategori oluşturabilir.");
            categoryCreate.setCode(AccountingPermissionEnum.CATEGORY_CREATE.getPermissionAlias());
            session.persist(categoryCreate);
        }

        Permission categoryRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CATEGORY_READ.getPermissionAlias())).uniqueResult();

        if (categoryRead == null) {
            categoryRead = new Permission();
            categoryRead.setName("Kategori görüntüleyebilir.");
            categoryRead.setCode(AccountingPermissionEnum.CATEGORY_READ.getPermissionAlias());
            session.persist(categoryRead);
        } else {
            categoryRead.setName("Kategori görüntüleyebilir.");
            categoryRead.setCode(AccountingPermissionEnum.CATEGORY_READ.getPermissionAlias());
            session.persist(categoryRead);
        }

        Permission categoryUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CATEGORY_UPDATE.getPermissionAlias())).uniqueResult();

        if (categoryUpdate == null) {
            categoryUpdate = new Permission();
            categoryUpdate.setName("Kategori güncelleyebilir.");
            categoryUpdate.setCode(AccountingPermissionEnum.CATEGORY_UPDATE.getPermissionAlias());
            session.persist(categoryUpdate);
        } else {
            categoryUpdate.setName("Kategori güncelleyebilir.");
            categoryUpdate.setCode(AccountingPermissionEnum.CATEGORY_UPDATE.getPermissionAlias());
            session.persist(categoryUpdate);
        }

        Permission categoryDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CATEGORY_DELETE.getPermissionAlias())).uniqueResult();

        if (categoryDelete == null) {
            categoryDelete = new Permission();
            categoryDelete.setName("Kategori silebilir.");
            categoryDelete.setCode(AccountingPermissionEnum.CATEGORY_DELETE.getPermissionAlias());
            session.persist(categoryDelete);
        } else {
            categoryDelete.setName("Kategori silebilir.");
            categoryDelete.setCode(AccountingPermissionEnum.CATEGORY_DELETE.getPermissionAlias());
            session.persist(categoryDelete);
        }

        Permission productCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PRODUCT_CREATE.getPermissionAlias())).uniqueResult();

        if (productCreate == null) {
            productCreate = new Permission();
            productCreate.setName("Ürün/Hizmet oluşturabilir.");
            productCreate.setCode(AccountingPermissionEnum.PRODUCT_CREATE.getPermissionAlias());
            session.persist(productCreate);
        } else {
            productCreate.setName("Ürün/Hizmet oluşturabilir.");
            productCreate.setCode(AccountingPermissionEnum.PRODUCT_CREATE.getPermissionAlias());
            session.persist(productCreate);
        }

        Permission productRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PRODUCT_READ.getPermissionAlias())).uniqueResult();

        if (productRead == null) {
            productRead = new Permission();
            productRead.setName("Ürün/Hizmet görüntüleyebilir.");
            productRead.setCode(AccountingPermissionEnum.PRODUCT_READ.getPermissionAlias());
            session.persist(productRead);
        } else {
            productRead.setName("Ürün/Hizmet görüntüleyebilir.");
            productRead.setCode(AccountingPermissionEnum.PRODUCT_READ.getPermissionAlias());
            session.persist(productRead);
        }

        Permission productUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PRODUCT_UPDATE.getPermissionAlias())).uniqueResult();

        if (productUpdate == null) {
            productUpdate = new Permission();
            productUpdate.setName("Ürün/Hizmet güncelleyebilir.");
            productUpdate.setCode(AccountingPermissionEnum.PRODUCT_UPDATE.getPermissionAlias());
            session.persist(productUpdate);
        } else {
            productUpdate.setName("Ürün/Hizmet güncelleyebilir.");
            productUpdate.setCode(AccountingPermissionEnum.PRODUCT_UPDATE.getPermissionAlias());
            session.persist(productUpdate);
        }

        Permission productDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PRODUCT_DELETE.getPermissionAlias())).uniqueResult();

        if (productDelete == null) {
            productDelete = new Permission();
            productDelete.setName("Ürün/Hizmet silebilir.");
            productDelete.setCode(AccountingPermissionEnum.PRODUCT_DELETE.getPermissionAlias());
            session.persist(productDelete);
        } else {
            productDelete.setName("Ürün/Hizmet silebilir.");
            productDelete.setCode(AccountingPermissionEnum.PRODUCT_DELETE.getPermissionAlias());
            session.persist(productDelete);
        }

        Permission bidCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.BID_CREATE.getPermissionAlias())).uniqueResult();

        if (bidCreate == null) {
            bidCreate = new Permission();
            bidCreate.setName("Teklif oluşturabilir.");
            bidCreate.setCode(AccountingPermissionEnum.BID_CREATE.getPermissionAlias());
            session.persist(bidCreate);
        } else {
            bidCreate.setName("Teklif oluşturabilir.");
            bidCreate.setCode(AccountingPermissionEnum.BID_CREATE.getPermissionAlias());
            session.persist(bidCreate);
        }

        Permission bidRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.BID_READ.getPermissionAlias())).uniqueResult();

        if (bidRead == null) {
            bidRead = new Permission();
            bidRead.setName("Teklif görüntüleyebilir.");
            bidRead.setCode(AccountingPermissionEnum.BID_READ.getPermissionAlias());
            session.persist(bidRead);
        } else {
            bidRead.setName("Teklif görüntüleyebilir.");
            bidRead.setCode(AccountingPermissionEnum.BID_READ.getPermissionAlias());
            session.persist(bidRead);
        }

        Permission bidUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.BID_UPDATE.getPermissionAlias())).uniqueResult();

        if (bidUpdate == null) {
            bidUpdate = new Permission();
            bidUpdate.setName("Teklif güncelleyebilir.");
            bidUpdate.setCode(AccountingPermissionEnum.BID_UPDATE.getPermissionAlias());
            session.persist(bidUpdate);
        } else {
            bidUpdate.setName("Teklif güncelleyebilir.");
            bidUpdate.setCode(AccountingPermissionEnum.BID_UPDATE.getPermissionAlias());
            session.persist(bidUpdate);
        }

        Permission bidDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.BID_DELETE.getPermissionAlias())).uniqueResult();

        if (bidDelete == null) {
            bidDelete = new Permission();
            bidDelete.setName("Teklif silebilir.");
            bidDelete.setCode(AccountingPermissionEnum.BID_DELETE.getPermissionAlias());
            session.persist(bidDelete);
        } else {
            bidDelete.setName("Teklif silebilir.");
            bidDelete.setCode(AccountingPermissionEnum.BID_DELETE.getPermissionAlias());
            session.persist(bidDelete);
        }

        Permission invoiceCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.INVOICE_CREATE.getPermissionAlias())).uniqueResult();

        if (invoiceCreate == null) {
            invoiceCreate = new Permission();
            invoiceCreate.setName("Fatura oluşturabilir.");
            invoiceCreate.setCode(AccountingPermissionEnum.INVOICE_CREATE.getPermissionAlias());
            session.persist(invoiceCreate);
        } else {
            invoiceCreate.setName("Fatura oluşturabilir.");
            invoiceCreate.setCode(AccountingPermissionEnum.INVOICE_CREATE.getPermissionAlias());
            session.persist(invoiceCreate);
        }

        Permission invoiceRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.INVOICE_READ.getPermissionAlias())).uniqueResult();

        if (invoiceRead == null) {
            invoiceRead = new Permission();
            invoiceRead.setName("Fatura görüntüleyebilir.");
            invoiceRead.setCode(AccountingPermissionEnum.INVOICE_READ.getPermissionAlias());
            session.persist(invoiceRead);
        } else {
            invoiceRead.setName("Fatura görüntüleyebilir.");
            invoiceRead.setCode(AccountingPermissionEnum.INVOICE_READ.getPermissionAlias());
            session.persist(invoiceRead);
        }

        Permission invoiceUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.INVOICE_UPDATE.getPermissionAlias())).uniqueResult();

        if (invoiceUpdate == null) {
            invoiceUpdate = new Permission();
            invoiceUpdate.setName("Fatura güncelleyebilir.");
            invoiceUpdate.setCode(AccountingPermissionEnum.INVOICE_UPDATE.getPermissionAlias());
            session.persist(invoiceUpdate);
        } else {
            invoiceUpdate.setName("Fatura güncelleyebilir.");
            invoiceUpdate.setCode(AccountingPermissionEnum.INVOICE_UPDATE.getPermissionAlias());
            session.persist(invoiceUpdate);
        }

        Permission invoiceDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.INVOICE_DELETE.getPermissionAlias())).uniqueResult();

        if (invoiceDelete == null) {
            invoiceDelete = new Permission();
            invoiceDelete.setName("Fatura silebilir.");
            invoiceDelete.setCode(AccountingPermissionEnum.INVOICE_DELETE.getPermissionAlias());
            session.persist(invoiceDelete);
        } else {
            invoiceDelete.setName("Fatura silebilir.");
            invoiceDelete.setCode(AccountingPermissionEnum.INVOICE_DELETE.getPermissionAlias());
            session.persist(invoiceDelete);
        }

        Permission proformaCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PROFORMA_CREATE.getPermissionAlias())).uniqueResult();

        if (proformaCreate == null) {
            proformaCreate = new Permission();
            proformaCreate.setName("Proforma oluşturabilir.");
            proformaCreate.setCode(AccountingPermissionEnum.PROFORMA_CREATE.getPermissionAlias());
            session.persist(proformaCreate);
        } else {
            proformaCreate.setName("Proforma oluşturabilir.");
            proformaCreate.setCode(AccountingPermissionEnum.PROFORMA_CREATE.getPermissionAlias());
            session.persist(proformaCreate);
        }

        Permission proformaRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PROFORMA_READ.getPermissionAlias())).uniqueResult();

        if (proformaRead == null) {
            proformaRead = new Permission();
            proformaRead.setName("Proforma görüntüleyebilir.");
            proformaRead.setCode(AccountingPermissionEnum.PROFORMA_READ.getPermissionAlias());
            session.persist(proformaRead);
        } else {
            proformaRead.setName("Proforma görüntüleyebilir.");
            proformaRead.setCode(AccountingPermissionEnum.PROFORMA_READ.getPermissionAlias());
            session.persist(proformaRead);
        }

        Permission proformaUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PROFORMA_UPDATE.getPermissionAlias())).uniqueResult();

        if (proformaUpdate == null) {
            proformaUpdate = new Permission();
            proformaUpdate.setName("Proforma güncelleyebilir.");
            proformaUpdate.setCode(AccountingPermissionEnum.PROFORMA_UPDATE.getPermissionAlias());
            session.persist(proformaUpdate);
        } else {
            proformaUpdate.setName("Proforma güncelleyebilir.");
            proformaUpdate.setCode(AccountingPermissionEnum.PROFORMA_UPDATE.getPermissionAlias());
            session.persist(proformaUpdate);
        }

        Permission proformaDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PROFORMA_DELETE.getPermissionAlias())).uniqueResult();

        if (proformaDelete == null) {
            proformaDelete = new Permission();
            proformaDelete.setName("Proforma silebilir.");
            proformaDelete.setCode(AccountingPermissionEnum.PROFORMA_DELETE.getPermissionAlias());
            session.persist(proformaDelete);
        } else {
            proformaDelete.setName("Proforma silebilir.");
            proformaDelete.setCode(AccountingPermissionEnum.PROFORMA_DELETE.getPermissionAlias());
            session.persist(proformaDelete);
        }

        Permission consignmentInvoiceCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CONSIGNMENT_INVOCE_CREATE.getPermissionAlias())).uniqueResult();

        if (consignmentInvoiceCreate == null) {
            consignmentInvoiceCreate = new Permission();
            consignmentInvoiceCreate.setName("İrsaliyeli fatura oluşturabilir.");
            consignmentInvoiceCreate.setCode(AccountingPermissionEnum.CONSIGNMENT_INVOCE_CREATE.getPermissionAlias());
            session.persist(consignmentInvoiceCreate);
        } else {
            consignmentInvoiceCreate.setName("İrsaliyeli fatura oluşturabilir.");
            consignmentInvoiceCreate.setCode(AccountingPermissionEnum.CONSIGNMENT_INVOCE_CREATE.getPermissionAlias());
            session.persist(consignmentInvoiceCreate);
        }

        Permission consignmentInvoiceRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CONSIGNMENT_INVOCE_READ.getPermissionAlias())).uniqueResult();

        if (consignmentInvoiceRead == null) {
            consignmentInvoiceRead = new Permission();
            consignmentInvoiceRead.setName("İrsaliyeli fatura görüntüleyebilir.");
            consignmentInvoiceRead.setCode(AccountingPermissionEnum.CONSIGNMENT_INVOCE_READ.getPermissionAlias());
            session.persist(consignmentInvoiceRead);
        } else {
            consignmentInvoiceRead.setName("İrsaliyeli fatura görüntüleyebilir.");
            consignmentInvoiceRead.setCode(AccountingPermissionEnum.CONSIGNMENT_INVOCE_READ.getPermissionAlias());
            session.persist(consignmentInvoiceRead);
        }

        Permission consignmentInvoiceUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CONSIGNMENT_INVOCE_UPDATE.getPermissionAlias())).uniqueResult();

        if (consignmentInvoiceUpdate == null) {
            consignmentInvoiceUpdate = new Permission();
            consignmentInvoiceUpdate.setName("İrsaliyeli fatura güncelleyebilir.");
            consignmentInvoiceUpdate.setCode(AccountingPermissionEnum.CONSIGNMENT_INVOCE_UPDATE.getPermissionAlias());
            session.persist(consignmentInvoiceUpdate);
        } else {
            consignmentInvoiceUpdate.setName("İrsaliyeli fatura güncelleyebilir.");
            consignmentInvoiceUpdate.setCode(AccountingPermissionEnum.CONSIGNMENT_INVOCE_UPDATE.getPermissionAlias());
            session.persist(consignmentInvoiceUpdate);
        }

        Permission consignmentInvoiceDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CONSIGNMENT_INVOCE_DELETE.getPermissionAlias())).uniqueResult();

        if (consignmentInvoiceDelete == null) {
            consignmentInvoiceDelete = new Permission();
            consignmentInvoiceDelete.setName("İrsaliyeli fatura silebilir.");
            consignmentInvoiceDelete.setCode(AccountingPermissionEnum.CONSIGNMENT_INVOCE_DELETE.getPermissionAlias());
            session.persist(consignmentInvoiceDelete);
        } else {
            consignmentInvoiceDelete.setName("İrsaliyeli fatura silebilir.");
            consignmentInvoiceDelete.setCode(AccountingPermissionEnum.CONSIGNMENT_INVOCE_DELETE.getPermissionAlias());
            session.persist(consignmentInvoiceDelete);
        }

        Permission accountCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.ACCOUNT_CREATE.getPermissionAlias())).uniqueResult();

        if (accountCreate == null) {
            accountCreate = new Permission();
            accountCreate.setName("Para hesabı oluşturabilir.");
            accountCreate.setCode(AccountingPermissionEnum.ACCOUNT_CREATE.getPermissionAlias());
            session.persist(accountCreate);
        } else {
            accountCreate.setName("Para hesabı oluşturabilir.");
            accountCreate.setCode(AccountingPermissionEnum.ACCOUNT_CREATE.getPermissionAlias());
            session.persist(accountCreate);
        }

        Permission accountRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.ACCOUNT_READ.getPermissionAlias())).uniqueResult();

        if (accountRead == null) {
            accountRead = new Permission();
            accountRead.setName("Para hesabı görüntüleyebilir.");
            accountRead.setCode(AccountingPermissionEnum.ACCOUNT_READ.getPermissionAlias());
            session.persist(accountRead);
        } else {
            accountRead.setName("Para hesabı görüntüleyebilir.");
            accountRead.setCode(AccountingPermissionEnum.ACCOUNT_READ.getPermissionAlias());
            session.persist(accountRead);
        }

        Permission accountUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.ACCOUNT_UPDATE.getPermissionAlias())).uniqueResult();

        if (accountUpdate == null) {
            accountUpdate = new Permission();
            accountUpdate.setName("Para hesabı güncelleyebilir.");
            accountUpdate.setCode(AccountingPermissionEnum.ACCOUNT_UPDATE.getPermissionAlias());
            session.persist(accountUpdate);
        } else {
            accountUpdate.setName("Para hesabı güncelleyebilir.");
            accountUpdate.setCode(AccountingPermissionEnum.ACCOUNT_UPDATE.getPermissionAlias());
            session.persist(accountRead);
        }

        Permission accountDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.ACCOUNT_DELETE.getPermissionAlias())).uniqueResult();

        if (accountDelete == null) {
            accountDelete = new Permission();
            accountDelete.setName("Para hesabı silebilir.");
            accountDelete.setCode(AccountingPermissionEnum.ACCOUNT_DELETE.getPermissionAlias());
            session.persist(accountDelete);
        } else {
            accountDelete.setName("Para hesabı silebilir.");
            accountDelete.setCode(AccountingPermissionEnum.ACCOUNT_DELETE.getPermissionAlias());
            session.persist(accountDelete);
        }

        Permission proceedsCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PROCEEDS_CREATE.getPermissionAlias())).uniqueResult();

        if (proceedsCreate == null) {
            proceedsCreate = new Permission();
            proceedsCreate.setName("Tahsilat oluşturabilir.");
            proceedsCreate.setCode(AccountingPermissionEnum.PROCEEDS_CREATE.getPermissionAlias());
            session.persist(proceedsCreate);
        } else {
            proceedsCreate.setName("Tahsilat oluşturabilir.");
            proceedsCreate.setCode(AccountingPermissionEnum.PROCEEDS_CREATE.getPermissionAlias());
            session.persist(proceedsCreate);
        }

        Permission proceedsRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PROCEEDS_READ.getPermissionAlias())).uniqueResult();

        if (proceedsRead == null) {
            proceedsRead = new Permission();
            proceedsRead.setName("Tahsilat görüntüleyebilir.");
            proceedsRead.setCode(AccountingPermissionEnum.PROCEEDS_READ.getPermissionAlias());
            session.persist(proceedsRead);
        } else {
            proceedsRead.setName("Tahsilat görüntüleyebilir.");
            proceedsRead.setCode(AccountingPermissionEnum.PROCEEDS_READ.getPermissionAlias());
            session.persist(proceedsRead);
        }

        Permission proceedsUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PROCEEDS_UPDATE.getPermissionAlias())).uniqueResult();

        if (proceedsUpdate == null) {
            proceedsUpdate = new Permission();
            proceedsUpdate.setName("Tahsilat güncelleyebilir.");
            proceedsUpdate.setCode(AccountingPermissionEnum.PROCEEDS_UPDATE.getPermissionAlias());
            session.persist(proceedsUpdate);
        } else {
            proceedsUpdate.setName("Tahsilat güncelleyebilir.");
            proceedsUpdate.setCode(AccountingPermissionEnum.PROCEEDS_UPDATE.getPermissionAlias());
            session.persist(proceedsUpdate);
        }

        Permission proceedsDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.PROCEEDS_DELETE.getPermissionAlias())).uniqueResult();

        if (proceedsDelete == null) {
            proceedsDelete = new Permission();
            proceedsDelete.setName("Tahsilat silebilir.");
            proceedsDelete.setCode(AccountingPermissionEnum.PROCEEDS_DELETE.getPermissionAlias());
            session.persist(proceedsDelete);
        } else {
            proceedsDelete.setName("Tahsilat silebilir.");
            proceedsDelete.setCode(AccountingPermissionEnum.PROCEEDS_DELETE.getPermissionAlias());
            session.persist(proceedsDelete);
        }

        Permission checkAndBillCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CHECK_AND_BILL_CREATE.getPermissionAlias())).uniqueResult();

        if (checkAndBillCreate == null) {
            checkAndBillCreate = new Permission();
            checkAndBillCreate.setName("Çek/Senet oluşturabilir.");
            checkAndBillCreate.setCode(AccountingPermissionEnum.CHECK_AND_BILL_CREATE.getPermissionAlias());
            session.persist(checkAndBillCreate);
        } else {
            checkAndBillCreate.setName("Çek/Senet oluşturabilir.");
            checkAndBillCreate.setCode(AccountingPermissionEnum.CHECK_AND_BILL_CREATE.getPermissionAlias());
            session.persist(checkAndBillCreate);
        }

        Permission checkAndBillRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CHECK_AND_BILL_READ.getPermissionAlias())).uniqueResult();

        if (checkAndBillRead == null) {
            checkAndBillRead = new Permission();
            checkAndBillRead.setName("Çek/Senet görüntüleyebilir.");
            checkAndBillRead.setCode(AccountingPermissionEnum.CHECK_AND_BILL_READ.getPermissionAlias());
            session.persist(checkAndBillRead);
        } else {
            checkAndBillRead.setName("Çek/Senet görüntüleyebilir.");
            checkAndBillRead.setCode(AccountingPermissionEnum.CHECK_AND_BILL_READ.getPermissionAlias());
            session.persist(checkAndBillRead);
        }

        Permission checkAndBillUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CHECK_AND_BILL_UPDATE.getPermissionAlias())).uniqueResult();

        if (checkAndBillUpdate == null) {
            checkAndBillUpdate = new Permission();
            checkAndBillUpdate.setName("Çek/Senet güncelleyebilir.");
            checkAndBillUpdate.setCode(AccountingPermissionEnum.CHECK_AND_BILL_UPDATE.getPermissionAlias());
            session.persist(checkAndBillUpdate);
        } else {
            checkAndBillUpdate.setName("Çek/Senet güncelleyebilir.");
            checkAndBillUpdate.setCode(AccountingPermissionEnum.CHECK_AND_BILL_UPDATE.getPermissionAlias());
            session.persist(checkAndBillUpdate);
        }

        Permission checkAndBillDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("code", AccountingPermissionEnum.CHECK_AND_BILL_DELETE.getPermissionAlias())).uniqueResult();

        if (checkAndBillDelete == null) {
            checkAndBillDelete = new Permission();
            checkAndBillDelete.setName("Çek/Senet silebilir.");
            checkAndBillDelete.setCode(AccountingPermissionEnum.CHECK_AND_BILL_DELETE.getPermissionAlias());
            session.persist(checkAndBillDelete);
        } else {
            checkAndBillDelete.setName("Çek/Senet silebilir.");
            checkAndBillDelete.setCode(AccountingPermissionEnum.CHECK_AND_BILL_DELETE.getPermissionAlias());
            session.persist(checkAndBillDelete);
        }


        session.flush();
    }
}
