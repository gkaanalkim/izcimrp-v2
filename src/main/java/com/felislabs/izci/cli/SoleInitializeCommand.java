package com.felislabs.izci.cli;

import com.felislabs.izci.MrpConfiguration;
import com.felislabs.izci.enums.MenuEnum;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by gunerkaanalkim on 25/04/2017.
 */
public class SoleInitializeCommand<T extends MrpConfiguration> extends EnvironmentCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(SoleInitializeCommand.class);
    private HibernateBundle hibernateBundle;

    public SoleInitializeCommand(Application application, HibernateBundle hibernateBundle) {
        super(application, "sole", "Runs initial command for initial process.");
        this.hibernateBundle = hibernateBundle;
    }

    @Override
    protected void run(Environment environment, Namespace namespace, T t) throws Exception {
        LOGGER.info("Initialize Starting...");
        LOGGER.info("Starting to create initial data.");
        execute(t);
    }

    @UnitOfWork
    public void execute(Configuration configuration) {
        final Session session = hibernateBundle.getSessionFactory().openSession();

        this.createPermission(session);
        this.createMenu(session);
        session.close();
    }

    public void createMenu(Session session) {
        int counter = 0;
        Menu definitionMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.DEFINITION_MAIN.getMenuAlias())).uniqueResult();

        if (definitionMenu == null) {
            definitionMenu = new Menu();
            definitionMenu.setName("Tanımlamalar");
            definitionMenu.setCode(MenuEnum.DEFINITION_MAIN.getMenuAlias());
            definitionMenu.setDescription("Tanımlama Ekranlarının en üst adımıdır.");
            definitionMenu.setMenuOrder(counter);
            definitionMenu.setProjectCode("SOLE");
            counter++;
            session.persist(definitionMenu);
        } else {
            definitionMenu.setName("Tanımlamalar");
            definitionMenu.setCode(MenuEnum.DEFINITION_MAIN.getMenuAlias());
            definitionMenu.setDescription("Tanımlama Ekranlarının en üst adımıdır.");
            definitionMenu.setMenuOrder(counter);
            definitionMenu.setProjectCode("SOLE");
            counter++;
            session.persist(definitionMenu);
        }
        Menu customerMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.CUSTOMER.getMenuAlias())).uniqueResult();

        if (customerMenu == null) {
            customerMenu = new Menu();
            customerMenu.setName("Müşteriler");
            customerMenu.setCode(MenuEnum.CUSTOMER.getMenuAlias());
            customerMenu.setDescription("Müşteri ekleme, güncelleme, silme ve arama sayfasıdır.");
            customerMenu.setUrl("musteri.html");
            customerMenu.setParentOid(definitionMenu.getOid());
            customerMenu.setMenuOrder(counter);
            customerMenu.setProjectCode("SOLE");
            counter++;
            session.persist(customerMenu);
        } else {
            customerMenu.setName("Müşteriler");
            customerMenu.setCode(MenuEnum.CUSTOMER.getMenuAlias());
            customerMenu.setDescription("Müşteri ekleme, güncelleme, silme ve arama sayfasıdır.");
            customerMenu.setUrl("musteri.html");
            customerMenu.setParentOid(definitionMenu.getOid());
            customerMenu.setMenuOrder(counter);
            customerMenu.setProjectCode("SOLE");
            counter++;
            session.persist(customerMenu);
        }
        Menu supplierMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.SUPPLIER.getMenuAlias())).uniqueResult();

        if (supplierMenu == null) {
            supplierMenu = new Menu();
            supplierMenu.setName("Tedarikçiler");
            supplierMenu.setCode(MenuEnum.SUPPLIER.getMenuAlias());
            supplierMenu.setDescription("Tedarikçi ekleme, güncelleme, silme ve arama sayfasıdır.");
            supplierMenu.setUrl("tedarikci.html");
            supplierMenu.setParentOid(definitionMenu.getOid());
            supplierMenu.setMenuOrder(counter);
            supplierMenu.setProjectCode("SOLE");
            counter++;
            session.persist(supplierMenu);
        } else {
            supplierMenu.setName("Tedarikçiler");
            supplierMenu.setCode(MenuEnum.SUPPLIER.getMenuAlias());
            supplierMenu.setDescription("Tedarikçi ekleme, güncelleme, silme ve arama sayfasıdır.");
            supplierMenu.setUrl("tedarikci.html");
            supplierMenu.setParentOid(definitionMenu.getOid());
            supplierMenu.setMenuOrder(counter);
            supplierMenu.setProjectCode("SOLE");
            counter++;
            session.persist(supplierMenu);
        }

        Menu materialType = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.MATERIAL_TYPE.getMenuAlias())).uniqueResult();

        if (materialType == null) {
            materialType = new Menu();
            materialType.setName("Malzeme Tipi");
            materialType.setCode(MenuEnum.MATERIAL_TYPE.getMenuAlias());
            materialType.setDescription("Malzeme türü ekleme, güncelleme, silme ve arama sayfasıdır.");
            materialType.setUrl("malzeme-tipi.html");
            materialType.setParentOid(definitionMenu.getOid());
            materialType.setMenuOrder(counter);
            materialType.setProjectCode("SOLE");
            counter++;
            session.persist(materialType);
        } else {
            materialType.setName("Malzeme Tipi");
            materialType.setCode(MenuEnum.MATERIAL_TYPE.getMenuAlias());
            materialType.setDescription("Malzeme türü  ekleme, güncelleme, silme ve arama sayfasıdır.");
            materialType.setUrl("malzeme-tipi.html");
            materialType.setParentOid(definitionMenu.getOid());
            materialType.setMenuOrder(counter);
            materialType.setProjectCode("SOLE");
            counter++;
            session.persist(materialType);
        }

        Menu productionLine = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.PRODUCTION_LINE.getMenuAlias())).uniqueResult();

        if (productionLine == null) {
            productionLine = new Menu();
            productionLine.setName("Üretim Hattı");
            productionLine.setCode(MenuEnum.PRODUCTION_LINE.getMenuAlias());
            productionLine.setDescription("Üretim hattı ekleme, güncelleme, silme ve arama sayfasıdır.");
            productionLine.setUrl("uretim-hatti.html");
            productionLine.setParentOid(definitionMenu.getOid());
            productionLine.setMenuOrder(counter);
            productionLine.setProjectCode("SOLE");
            counter++;
            session.persist(productionLine);
        } else {
            productionLine.setName("Üretim Hattı");
            productionLine.setCode(MenuEnum.PRODUCTION_LINE.getMenuAlias());
            productionLine.setDescription("Üretim hattı ekleme, güncelleme, silme ve arama sayfasıdır.");
            productionLine.setUrl("uretim-hatti.html");
            productionLine.setParentOid(definitionMenu.getOid());
            productionLine.setMenuOrder(counter);
            productionLine.setProjectCode("SOLE");
            counter++;
            session.persist(productionLine);
        }

        Menu internalCritical = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.INTERNAL_CRITICAL.getMenuAlias())).uniqueResult();

        if (internalCritical == null) {
            internalCritical = new Menu();
            internalCritical.setName("İç Kritik");
            internalCritical.setCode(MenuEnum.INTERNAL_CRITICAL.getMenuAlias());
            internalCritical.setDescription("İç kritik ekleme, güncelleme, silme ve arama sayfasıdır.");
            internalCritical.setUrl("ic-kritik.html");
            internalCritical.setParentOid(definitionMenu.getOid());
            internalCritical.setMenuOrder(counter);
            internalCritical.setProjectCode("SOLE");
            counter++;
            session.persist(internalCritical);
        } else {
            internalCritical.setName("İç Kritik");
            internalCritical.setCode(MenuEnum.INTERNAL_CRITICAL.getMenuAlias());
            internalCritical.setDescription("İç kritik  ekleme, güncelleme, silme ve arama sayfasıdır.");
            internalCritical.setUrl("ic-kritik.html");
            internalCritical.setParentOid(definitionMenu.getOid());
            internalCritical.setMenuOrder(counter);
            internalCritical.setProjectCode("SOLE");
            counter++;
            session.persist(internalCritical);
        }

        Menu shipmentInfo = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.SHIPMENT_INFO.getMenuAlias())).uniqueResult();

        if (shipmentInfo == null) {
            shipmentInfo = new Menu();
            shipmentInfo.setName("Sevkiyat Bilgileri");
            shipmentInfo.setCode(MenuEnum.SHIPMENT_INFO.getMenuAlias());
            shipmentInfo.setDescription("Firmalara ait sevkiyat bilgisi ekleme, güncelleme, silme ve arama sayfasıdır.");
            shipmentInfo.setUrl("sevkiyat-bilgisi.html");
            shipmentInfo.setParentOid(definitionMenu.getOid());
            shipmentInfo.setMenuOrder(counter);
            shipmentInfo.setProjectCode("SOLE");
            counter++;
            session.persist(shipmentInfo);
        } else {
            shipmentInfo.setName("Sevkiyat Bilgileri");
            shipmentInfo.setCode(MenuEnum.SHIPMENT_INFO.getMenuAlias());
            shipmentInfo.setDescription("Firmalara ait sevkiyat bilgisi ekleme, güncelleme, silme ve arama sayfasıdır.");
            shipmentInfo.setUrl("sevkiyat-bilgisi.html");
            shipmentInfo.setParentOid(definitionMenu.getOid());
            shipmentInfo.setMenuOrder(counter);
            shipmentInfo.setProjectCode("SOLE");
            counter++;
            session.persist(shipmentInfo);
        }

//        todo fronttan karşılanınca açılacak
        Menu department = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.DEPARTMENT.getMenuAlias())).uniqueResult();

        if (department == null) {
            department = new Menu();
            department.setName("Departman");
            department.setCode(MenuEnum.DEPARTMENT.getMenuAlias());
            department.setDescription("Departman ekleme, güncelleme, silme ve arama sayfasıdır.");
            department.setUrl("departman.html");
            department.setParentOid(definitionMenu.getOid());
            department.setMenuOrder(counter);
            department.setProjectCode("SOLE");
            counter++;
            session.persist(department);
        } else {
            department.setName("Departman");
            department.setCode(MenuEnum.DEPARTMENT.getMenuAlias());
            department.setDescription("Departman ekleme, güncelleme, silme ve arama sayfasıdır.");
            department.setUrl("departman.html");
            department.setParentOid(definitionMenu.getOid());
            department.setMenuOrder(counter);
            department.setProjectCode("SOLE");
            counter++;
            session.persist(department);
        }
        Menu staff = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.STAFF.getMenuAlias())).uniqueResult();

        if (staff == null) {
            staff = new Menu();
            staff.setName("Çalışan");
            staff.setCode(MenuEnum.STAFF.getMenuAlias());
            staff.setDescription("Çalışan ekleme, güncelleme, silme ve arama sayfasıdır.");
            staff.setUrl("calisan.html");
            staff.setParentOid(definitionMenu.getOid());
            staff.setMenuOrder(counter);
            staff.setProjectCode("SOLE");
            counter++;
            session.persist(staff);
        } else {
            staff.setName("Çalışan");
            staff.setCode(MenuEnum.STAFF.getMenuAlias());
            staff.setDescription("Çalışan ekleme, güncelleme, silme ve arama sayfasıdır.");
            staff.setUrl("calisan.html");
            staff.setParentOid(definitionMenu.getOid());
            staff.setMenuOrder(counter);
            staff.setProjectCode("SOLE");
            counter++;
            session.persist(staff);
        }

        Menu processing = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.PROCESSING.getMenuAlias())).uniqueResult();

        if (processing == null) {
            processing = new Menu();
            processing.setName("Proses");
            processing.setCode(MenuEnum.PROCESSING.getMenuAlias());
            processing.setDescription("Proses ekleme, güncelleme, silme ve arama sayfasıdır.");
            processing.setUrl("proses.html");
            processing.setParentOid(definitionMenu.getOid());
            processing.setMenuOrder(counter);
            processing.setProjectCode("SOLE");
            counter++;
            session.persist(processing);
        } else {
            processing.setName("Proses");
            processing.setCode(MenuEnum.PROCESSING.getMenuAlias());
            processing.setDescription("Proses ekleme, güncelleme, silme ve arama sayfasıdır.");
            processing.setUrl("proses.html");
            processing.setParentOid(definitionMenu.getOid());
            processing.setMenuOrder(counter);
            processing.setProjectCode("SOLE");
            counter++;
            session.persist(processing);
        }


        Menu stockDefinitionMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.STOCK_MAIN.getMenuAlias())).uniqueResult();

        if (stockDefinitionMenu == null) {
            stockDefinitionMenu = new Menu();
            stockDefinitionMenu.setName("Stok");
            stockDefinitionMenu.setCode(MenuEnum.STOCK_MAIN.getMenuAlias());
            stockDefinitionMenu.setDescription("Stok/Depo işlemleri en üst adımıdır.");
            stockDefinitionMenu.setMenuOrder(counter);
            stockDefinitionMenu.setProjectCode("SOLE");
            counter++;
            session.persist(stockDefinitionMenu);
        } else {
            stockDefinitionMenu.setName("Stok");
            stockDefinitionMenu.setCode(MenuEnum.STOCK_MAIN.getMenuAlias());
            stockDefinitionMenu.setDescription("Stok/Depo işlemleri en üst adımıdır.");
            stockDefinitionMenu.setMenuOrder(counter);
            stockDefinitionMenu.setProjectCode("SOLE");
            counter++;
            session.persist(stockDefinitionMenu);
        }

        Menu stockMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.STOCK.getMenuAlias())).uniqueResult();

        if (stockMenu == null) {
            stockMenu = new Menu();
            stockMenu.setName("Stoklar");
            stockMenu.setCode(MenuEnum.STOCK.getMenuAlias());
            stockMenu.setDescription("Stok ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockMenu.setUrl("stoklar.html");
            stockMenu.setParentOid(stockDefinitionMenu.getOid());
            stockMenu.setMenuOrder(counter);
            stockMenu.setProjectCode("SOLE");
            counter++;
            session.persist(stockMenu);
        } else {
            stockMenu.setName("Stoklar");
            stockMenu.setCode(MenuEnum.STOCK.getMenuAlias());
            stockMenu.setDescription("Stok ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockMenu.setUrl("stoklar.html");
            stockMenu.setParentOid(stockDefinitionMenu.getOid());
            stockMenu.setMenuOrder(counter);
            stockMenu.setProjectCode("SOLE");
            counter++;
            session.persist(stockMenu);
        }

        Menu stockTypeMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.STOCK_TYPE.getMenuAlias())).uniqueResult();

        if (stockTypeMenu == null) {
            stockTypeMenu = new Menu();
            stockTypeMenu.setName("Stok Tipi");
            stockTypeMenu.setCode(MenuEnum.STOCK_TYPE.getMenuAlias());
            stockTypeMenu.setDescription("Stok Tipi ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockTypeMenu.setUrl("stok-tipi.html");
            stockTypeMenu.setParentOid(stockDefinitionMenu.getOid());
            stockTypeMenu.setMenuOrder(counter);
            stockTypeMenu.setProjectCode("SOLE");
            counter++;
            session.persist(stockTypeMenu);
        } else {
            stockTypeMenu.setName("Stok Tipi");
            stockTypeMenu.setCode(MenuEnum.STOCK_TYPE.getMenuAlias());
            stockTypeMenu.setDescription("Stok Tipi ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockTypeMenu.setUrl("stok-tipi.html");
            stockTypeMenu.setParentOid(stockDefinitionMenu.getOid());
            stockTypeMenu.setMenuOrder(counter);
            stockTypeMenu.setProjectCode("SOLE");
            counter++;

            session.persist(stockTypeMenu);
        }
        Menu stockAcceptanceMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.STOCK_ACCEPTANCE.getMenuAlias())).uniqueResult();

        if (stockAcceptanceMenu == null) {
            stockAcceptanceMenu = new Menu();
            stockAcceptanceMenu.setName("Stok Kabul");
            stockAcceptanceMenu.setCode(MenuEnum.STOCK_ACCEPTANCE.getMenuAlias());
            stockAcceptanceMenu.setDescription("Stok Kabul ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockAcceptanceMenu.setUrl("stok-kabul.html");
            stockAcceptanceMenu.setParentOid(stockDefinitionMenu.getOid());
            stockAcceptanceMenu.setMenuOrder(counter);
            stockAcceptanceMenu.setProjectCode("SOLE");
            counter++;
            session.persist(stockAcceptanceMenu);
        } else {
            stockAcceptanceMenu.setName("Stok Kabul");
            stockAcceptanceMenu.setCode(MenuEnum.STOCK_ACCEPTANCE.getMenuAlias());
            stockAcceptanceMenu.setDescription("Stok Kabul ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockAcceptanceMenu.setUrl("stok-kabul.html");
            stockAcceptanceMenu.setParentOid(stockDefinitionMenu.getOid());
            stockAcceptanceMenu.setMenuOrder(counter);
            stockAcceptanceMenu.setProjectCode("SOLE");
            counter++;

            session.persist(stockAcceptanceMenu);
        }
        Menu modellingMainMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.MODELLING_MAIN.getMenuAlias())).uniqueResult();

        if (modellingMainMenu == null) {
            modellingMainMenu = new Menu();
            modellingMainMenu.setName("Modelleme");
            modellingMainMenu.setCode(MenuEnum.MODELLING_MAIN.getMenuAlias());
            modellingMainMenu.setDescription("Modelleme ekranlarının en üst adımıdır.");
            modellingMainMenu.setMenuOrder(counter);
            modellingMainMenu.setProjectCode("SOLE");
            counter++;
            session.persist(modellingMainMenu);
        } else {
            modellingMainMenu.setName("Modelleme");
            modellingMainMenu.setCode(MenuEnum.MODELLING_MAIN.getMenuAlias());
            modellingMainMenu.setDescription("Modelleme ekranlarının en üst adımıdır.");
            modellingMainMenu.setMenuOrder(counter);
            modellingMainMenu.setProjectCode("SOLE");
            counter++;
            session.persist(modellingMainMenu);
        }

        Menu modelCreateMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.MODEL_CREATE.getMenuAlias())).uniqueResult();

        if (modelCreateMenu == null) {
            modelCreateMenu = new Menu();
            modelCreateMenu.setName("Model Hazırla");
            modelCreateMenu.setCode(MenuEnum.MODEL_CREATE.getMenuAlias());
            modelCreateMenu.setDescription("Model oluşturma, güncelleme, silme ve arama sayfasıdır.");
            modelCreateMenu.setUrl("model-hazirla.html");
            modelCreateMenu.setParentOid(modellingMainMenu.getOid());
            modelCreateMenu.setMenuOrder(counter);
            modelCreateMenu.setProjectCode("SOLE");
            counter++;
            session.persist(modelCreateMenu);
        } else {
            modelCreateMenu.setName("Model Hazırla");
            modelCreateMenu.setCode(MenuEnum.MODEL_CREATE.getMenuAlias());
            modelCreateMenu.setDescription("Model oluşturma, güncelleme, silme ve arama sayfasıdır.");
            modelCreateMenu.setUrl("model-hazirla.html");
            modelCreateMenu.setParentOid(modellingMainMenu.getOid());
            modelCreateMenu.setMenuOrder(counter);
            modelCreateMenu.setProjectCode("SOLE");
            counter++;
            session.persist(modelCreateMenu);
        }

        Menu ownModels = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.OWN_MODEL.getMenuAlias())).uniqueResult();

        if (ownModels == null) {
            ownModels = new Menu();
            ownModels.setName("Modellerim");
            ownModels.setCode(MenuEnum.OWN_MODEL.getMenuAlias());
            ownModels.setDescription("Model görüntüleme, işleme ve güncelleme sayfasıdır.");
            ownModels.setUrl("modellerim.html");
            ownModels.setParentOid(modellingMainMenu.getOid());
            ownModels.setMenuOrder(counter);
            ownModels.setProjectCode("SOLE");
            counter++;
            session.persist(ownModels);
        } else {
            ownModels.setName("Modellerim");
            ownModels.setCode(MenuEnum.OWN_MODEL.getMenuAlias());
            ownModels.setDescription("Model görüntüleme, işleme ve güncelleme sayfasıdır.");
            ownModels.setUrl("modellerim.html");
            ownModels.setParentOid(modellingMainMenu.getOid());
            ownModels.setMenuOrder(counter);
            ownModels.setProjectCode("SOLE");
            counter++;
            session.persist(ownModels);
        }

        Menu planning = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.PLANNING.getMenuAlias())).uniqueResult();

        if (planning == null) {
            planning = new Menu();
            planning.setName("Planlama");
            planning.setCode(MenuEnum.PLANNING.getMenuAlias());
            planning.setDescription("Planlama Ekranlarının en üst adımıdır.");
            planning.setMenuOrder(counter);
            planning.setProjectCode("SOLE");
            counter++;

            session.persist(planning);
        } else {
            planning.setName("Planlama");
            planning.setCode(MenuEnum.PLANNING.getMenuAlias());
            planning.setDescription("Planlama Ekranlarının en üst adımıdır.");
            planning.setMenuOrder(counter);
            planning.setProjectCode("SOLE");
            counter++;

            session.persist(planning);
        }

        Menu pricing = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.PRICING.getMenuAlias())).uniqueResult();

        if (pricing == null) {
            pricing = new Menu();
            pricing.setName("Fiyatlandırma");
            pricing.setCode(MenuEnum.PRICING.getMenuAlias());
            pricing.setDescription("Fiyatlandırma ekrandır.");
            pricing.setUrl("model-fiyatlandirma.html");
            pricing.setParentOid(planning.getOid());
            pricing.setMenuOrder(counter);
            pricing.setProjectCode("SOLE");
            counter++;

            session.persist(pricing);
        } else {
            pricing.setName("Fiyatlandırma");
            pricing.setCode(MenuEnum.PRICING.getMenuAlias());
            pricing.setUrl("model-fiyatlandirma.html");
            pricing.setParentOid(planning.getOid());
            pricing.setMenuOrder(counter);
            pricing.setProjectCode("SOLE");
            counter++;

            session.persist(pricing);
        }

        Menu order = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.ORDER.getMenuAlias())).uniqueResult();

        if (order == null) {
            order = new Menu();
            order.setName("Müşteri Siparişi");
            order.setCode(MenuEnum.ORDER.getMenuAlias());
            order.setDescription("Müşteri Sipariş ekrandır.");
            order.setUrl("siparis.html");
            order.setParentOid(planning.getOid());
            order.setMenuOrder(counter);
            order.setProjectCode("SOLE");
            counter++;

            session.persist(order);
        } else {
            order.setName("Müşteri Siparişi");
            order.setCode(MenuEnum.ORDER.getMenuAlias());
            order.setDescription("Müşteri Sipariş ekrandır.");
            order.setUrl("siparis.html");
            order.setParentOid(planning.getOid());
            order.setMenuOrder(counter);
            order.setProjectCode("SOLE");
            counter++;

            session.persist(order);
        }

        Menu supply = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.SUPPLY.getMenuAlias())).uniqueResult();

        if (supply == null) {
            supply = new Menu();
            supply.setName("Malzeme Tedariği");
            supply.setCode(MenuEnum.SUPPLY.getMenuAlias());
            supply.setDescription("Tedarik ekrandır.");
            supply.setUrl("tedarik.html");
            supply.setParentOid(planning.getOid());
            supply.setMenuOrder(counter);
            supply.setProjectCode("SOLE");
            counter++;

            session.persist(supply);
        } else {
            supply.setName("Malzeme Tedariği");
            supply.setCode(MenuEnum.SUPPLY.getMenuAlias());
            supply.setDescription("Tedarik ekrandır.");
            supply.setUrl("tedarik.html");
            supply.setParentOid(planning.getOid());
            supply.setMenuOrder(counter);
            supply.setProjectCode("SOLE");
            counter++;

            session.persist(supply);
        }

        Menu bulkSupply = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.BULK_SUPPLY.getMenuAlias())).uniqueResult();

        if (bulkSupply == null) {
            bulkSupply = new Menu();
            bulkSupply.setName("Toplu Malzeme Tedariği");
            bulkSupply.setCode(MenuEnum.BULK_SUPPLY.getMenuAlias());
            bulkSupply.setDescription("Toplu Tedarik ekrandır.");
            bulkSupply.setUrl("toplu-tedarik.html");
            bulkSupply.setParentOid(planning.getOid());
            bulkSupply.setMenuOrder(counter);
            bulkSupply.setProjectCode("SOLE");
            counter++;

            session.persist(bulkSupply);
        } else {
            bulkSupply.setName("Toplu Malzeme Tedariği");
            bulkSupply.setCode(MenuEnum.BULK_SUPPLY.getMenuAlias());
            bulkSupply.setDescription("Toplu Tedarik ekrandır.");
            bulkSupply.setUrl("toplu-tedarik.html");
            bulkSupply.setParentOid(planning.getOid());
            bulkSupply.setMenuOrder(counter);
            bulkSupply.setProjectCode("SOLE");
            counter++;

            session.persist(bulkSupply);
        }

        //todo fronttan karşılanınca açılacak
        Menu modelProcessing = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.MODEL_PROCESSING.getMenuAlias())).uniqueResult();

        if (modelProcessing == null) {
            modelProcessing = new Menu();
            modelProcessing.setName("Proses Emri");
            modelProcessing.setCode(MenuEnum.MODEL_PROCESSING.getMenuAlias());
            modelProcessing.setDescription("Proses Emri ekleme, güncelleme, silme ve arama sayfasıdır.");
            modelProcessing.setUrl("proses-emri.html");
            modelProcessing.setParentOid(planning.getOid());
            modelProcessing.setMenuOrder(counter);
            modelProcessing.setProjectCode("SOLE");
            counter++;
            session.persist(modelProcessing);
        } else {
            modelProcessing.setName("Proses Emri");
            modelProcessing.setCode(MenuEnum.MODEL_PROCESSING.getMenuAlias());
            modelProcessing.setDescription("Proses Emri ekleme, güncelleme, silme ve arama sayfasıdır.");
            modelProcessing.setUrl("proses-emri.html");
            modelProcessing.setParentOid(planning.getOid());
            modelProcessing.setMenuOrder(counter);
            modelProcessing.setProjectCode("SOLE");
            counter++;
            session.persist(modelProcessing);
        }
        Menu production = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.PRODUCTION.getMenuAlias())).uniqueResult();

        if (production == null) {
            production = new Menu();
            production.setName("Üretim");
            production.setCode(MenuEnum.PRODUCTION.getMenuAlias());
            production.setDescription("Üretim Ekranlarının en üst adımıdır.");
            production.setMenuOrder(counter);
            production.setProjectCode("SOLE");
            counter++;

            session.persist(production);
        } else {
            production.setName("Üretim");
            production.setCode(MenuEnum.PRODUCTION.getMenuAlias());
            production.setDescription("Üretim Ekranlarının en üst adımıdır.");
            production.setMenuOrder(counter);
            production.setProjectCode("SOLE");
            counter++;

            session.persist(production);
        }

        Menu productionPlaning = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.PRODUCTION_PLANNING.getMenuAlias())).uniqueResult();

        if (productionPlaning == null) {
            productionPlaning = new Menu();
            productionPlaning.setName("Üretim Takvimi");
            productionPlaning.setCode(MenuEnum.PRODUCTION_PLANNING.getMenuAlias());
            productionPlaning.setDescription("Üretim Takvimi ekranıdır.");
            productionPlaning.setParentOid(production.getOid());
            productionPlaning.setUrl("uretim-takvimi.html");
            productionPlaning.setMenuOrder(counter);
            productionPlaning.setProjectCode("SOLE");
            counter++;

            session.persist(productionPlaning);
        } else {
            productionPlaning.setName("Üretim Takvimi");
            productionPlaning.setCode(MenuEnum.PRODUCTION_PLANNING.getMenuAlias());
            productionPlaning.setDescription("Üretim Takvimi ekranıdır.");
            productionPlaning.setParentOid(production.getOid());
            productionPlaning.setUrl("uretim-takvimi.html");
            productionPlaning.setMenuOrder(counter);
            productionPlaning.setProjectCode("SOLE");
            counter++;

            session.persist(productionPlaning);
        }

        Menu productionList = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.PRODUCTION_LIST.getMenuAlias())).uniqueResult();

        if (productionList == null) {
            productionList = new Menu();
            productionList.setName("Üretim Listeleri");
            productionList.setCode(MenuEnum.PRODUCTION_LIST.getMenuAlias());
            productionList.setDescription("Üretim Listesi ekrandır.");
            productionList.setParentOid(production.getOid());
            productionList.setUrl("uretim-listeleri.html");
            productionList.setMenuOrder(counter);
            productionList.setProjectCode("SOLE");
            counter++;

            session.persist(productionList);
        } else {
            productionList.setName("Üretim Listeleri");
            productionList.setCode(MenuEnum.PRODUCTION_LIST.getMenuAlias());
            productionList.setDescription("Üretim Listesi ekranıdır.");
            productionList.setParentOid(production.getOid());
            productionList.setUrl("uretim-listeleri.html");
            productionList.setMenuOrder(counter);
            productionList.setProjectCode("SOLE");
            counter++;

            session.persist(productionList);
        }


        Menu reports = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.REPORTS.getMenuAlias())).uniqueResult();

        if (reports == null) {
            reports = new Menu();
            reports.setName("Raporlar");
            reports.setCode(MenuEnum.REPORTS.getMenuAlias());
            reports.setDescription("Rapor Ekranlarının en üst adımıdır.");
            reports.setMenuOrder(counter);
            reports.setProjectCode("SOLE");
            counter++;

            session.persist(reports);
        } else {
            reports.setName("Raporlar");
            reports.setCode(MenuEnum.REPORTS.getMenuAlias());
            reports.setDescription("Rapor Ekranlarının en üst adımıdır.");
            reports.setMenuOrder(counter);
            reports.setProjectCode("SOLE");
            counter++;

            session.persist(reports);
        }

        Menu generalProductReport = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.PRODUCTION_REPORT.getMenuAlias())).uniqueResult();

        if (generalProductReport == null) {
            generalProductReport = new Menu();
            generalProductReport.setName("Üretim Raporları");
            generalProductReport.setCode(MenuEnum.PRODUCTION_REPORT.getMenuAlias());
            generalProductReport.setDescription("Üretim Raporları ekrandır.");
            generalProductReport.setParentOid(reports.getOid());
            generalProductReport.setUrl("uretim-raporlari.html");
            generalProductReport.setMenuOrder(counter);
            generalProductReport.setProjectCode("SOLE");
            counter++;

            session.persist(generalProductReport);
        } else {
            generalProductReport.setName("Üretim Raporları");
            generalProductReport.setCode(MenuEnum.PRODUCTION_REPORT.getMenuAlias());
            generalProductReport.setDescription("Üretim Raporları ekrandır.");
            generalProductReport.setParentOid(reports.getOid());
            generalProductReport.setUrl("uretim-raporlari.html");
            generalProductReport.setMenuOrder(counter);
            generalProductReport.setProjectCode("SOLE");
            counter++;

            session.persist(generalProductReport);
        }

        Menu userReport = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.USER_REPORT.getMenuAlias())).uniqueResult();

        if (userReport == null) {
            userReport = new Menu();
            userReport.setName("Kullanıcı Raporları");
            userReport.setCode(MenuEnum.USER_REPORT.getMenuAlias());
            userReport.setDescription("Kullanıcı raporları ekranıdır.");
            userReport.setParentOid(reports.getOid());
            userReport.setUrl("kullanici-raporlari.html");
            userReport.setMenuOrder(counter);
            userReport.setProjectCode("SOLE");
            counter++;

            session.persist(userReport);
        } else {
            userReport.setName("Kullanıcı Raporları");
            userReport.setCode(MenuEnum.USER_REPORT.getMenuAlias());
            userReport.setDescription("Kullanıcı raporları ekranıdır.");
            userReport.setParentOid(reports.getOid());
            userReport.setUrl("kullanici-raporlari.html");
            userReport.setMenuOrder(counter);
            userReport.setProjectCode("SOLE");
            counter++;

            session.persist(userReport);
        }
        Menu supplyReport = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.SUPPLY_REPORT.getMenuAlias())).uniqueResult();

        if (supplyReport == null) {
            supplyReport = new Menu();
            supplyReport.setName("Tedarik Raporları");
            supplyReport.setCode(MenuEnum.SUPPLY_REPORT.getMenuAlias());
            supplyReport.setDescription("Tedarik raporları ekranıdır.");
            supplyReport.setParentOid(reports.getOid());
            supplyReport.setUrl("tedarik-raporlari.html");
            supplyReport.setMenuOrder(counter);
            supplyReport.setProjectCode("SOLE");
            counter++;

            session.persist(supplyReport);
        } else {
            supplyReport.setName("Tedarik Raporları");
            supplyReport.setCode(MenuEnum.SUPPLY_REPORT.getMenuAlias());
            supplyReport.setDescription("Tedarik raporları ekranıdır.");
            supplyReport.setParentOid(reports.getOid());
            supplyReport.setUrl("tedarik-raporlari.html");
            supplyReport.setMenuOrder(counter);
            supplyReport.setProjectCode("SOLE");
            counter++;

            session.persist(supplyReport);
        }

        Menu supplierReport = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.SUPPLIER_REPORT.getMenuAlias())).uniqueResult();

        if (supplierReport == null) {
            supplierReport = new Menu();
            supplierReport.setName("Tedarikçi Raporları");
            supplierReport.setCode(MenuEnum.SUPPLIER_REPORT.getMenuAlias());
            supplierReport.setDescription("Tedarikçi raporları ekranıdır.");
            supplierReport.setParentOid(reports.getOid());
            supplierReport.setUrl("tedarikci-raporlari.html");
            supplierReport.setMenuOrder(counter);
            supplierReport.setProjectCode("SOLE");
            counter++;

            session.persist(supplierReport);
        } else {
            supplierReport.setName("Tedarikçi Raporları");
            supplierReport.setCode(MenuEnum.SUPPLIER_REPORT.getMenuAlias());
            supplierReport.setDescription("Tedarikçi raporları ekranıdır.");
            supplierReport.setParentOid(reports.getOid());
            supplierReport.setUrl("tedarikci-raporlari.html");
            supplierReport.setMenuOrder(counter);
            supplierReport.setProjectCode("SOLE");
            counter++;

            session.persist(supplierReport);
        }
        Menu shipmentMain = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.FORWARDING.getMenuAlias())).uniqueResult();

        if (shipmentMain == null) {
            shipmentMain = new Menu();
            shipmentMain.setName("Sevkiyat");
            shipmentMain.setCode(MenuEnum.FORWARDING.getMenuAlias());
            shipmentMain.setDescription("Sevkiyat ekranlarının en üst adımıdır.");
            shipmentMain.setMenuOrder(counter);
            shipmentMain.setProjectCode("SOLE");
            counter++;

            session.persist(shipmentMain);
        } else {
            shipmentMain.setName("Sevkiyat");
            shipmentMain.setCode(MenuEnum.FORWARDING.getMenuAlias());
            shipmentMain.setDescription("Sevkiyat ekranlarının en üst adımıdır.");
            shipmentMain.setMenuOrder(counter);
            shipmentMain.setProjectCode("SOLE");
            counter++;

            session.persist(shipmentMain);
        }

        Menu shipmentList = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.FORWARDING_INFO.getMenuAlias())).uniqueResult();

        if (shipmentList == null) {
            shipmentList = new Menu();
            shipmentList.setName("Sevkiyat Listeleri");
            shipmentList.setCode(MenuEnum.FORWARDING_INFO.getMenuAlias());
            shipmentList.setDescription("Sevkiyat listeleri ekrandır.");
            shipmentList.setParentOid(shipmentMain.getOid());
            shipmentList.setUrl("sevkiyat-listeleri.html");
            shipmentList.setMenuOrder(counter);
            shipmentList.setProjectCode("SOLE");
            counter++;

            session.persist(shipmentList);
        } else {
            shipmentList.setName("Sevkiyat Listeleri");
            shipmentList.setCode(MenuEnum.FORWARDING_INFO.getMenuAlias());
            shipmentList.setDescription("Sevkiyat listeleri ekrandır.");
            shipmentList.setParentOid(shipmentMain.getOid());
            shipmentList.setUrl("sevkiyat-listeleri.html");
            shipmentList.setMenuOrder(counter);
            shipmentList.setProjectCode("SOLE");
            counter++;

            session.persist(shipmentList);
        }

        Menu fairMain = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.FAIR_DEFINITION.getMenuAlias())).uniqueResult();

        if (fairMain == null) {
            fairMain = new Menu();
            fairMain.setName("Fuar");
            fairMain.setCode(MenuEnum.FAIR_DEFINITION.getMenuAlias());
            fairMain.setDescription("Fuar işlemleri ekranlarının en üst adımıdır.");
            fairMain.setMenuOrder(counter);
            fairMain.setProjectCode("SOLE");
            counter++;

            session.persist(fairMain);
        } else {
            fairMain.setName("Fuar");
            fairMain.setCode(MenuEnum.FAIR_DEFINITION.getMenuAlias());
            fairMain.setDescription("Fuar işlemleri ekranlarının en üst adımıdır.");
            fairMain.setMenuOrder(counter);
            fairMain.setProjectCode("SOLE");
            counter++;

            session.persist(fairMain);
        }

        Menu fairDefinition = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.FAIR_OPERATION.getMenuAlias())).uniqueResult();

        if (fairDefinition == null) {
            fairDefinition = new Menu();
            fairDefinition.setName("Fuar İşlemleri");
            fairDefinition.setCode(MenuEnum.FAIR_OPERATION.getMenuAlias());
            fairDefinition.setDescription("Fuar işlemleri sayfasıdır.");
            fairDefinition.setUrl("fuar-islemleri.html");
            fairDefinition.setParentOid(fairMain.getOid());
            fairDefinition.setMenuOrder(counter);
            fairDefinition.setProjectCode("SOLE");
            counter++;

            session.persist(fairDefinition);
        } else {
            fairDefinition.setName("Fuar İşlemleri");
            fairDefinition.setCode(MenuEnum.FAIR_OPERATION.getMenuAlias());
            fairDefinition.setDescription("Fuar işlemleri sayfasıdır.");
            fairDefinition.setUrl("fuar-islemleri.html");
            fairDefinition.setParentOid(fairMain.getOid());
            fairDefinition.setMenuOrder(counter);
            fairDefinition.setProjectCode("SOLE");
            counter++;

            session.persist(fairDefinition);
        }

        Menu fairReport = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.FAIR_REPORT.getMenuAlias())).uniqueResult();

        if (fairReport == null) {
            fairReport = new Menu();
            fairReport.setName("Fuar Raporları");
            fairReport.setCode(MenuEnum.FAIR_REPORT.getMenuAlias());
            fairReport.setDescription("Fuar raporları sayfasıdır.");
            fairReport.setUrl("fuar-raporlari.html");
            fairReport.setParentOid(fairMain.getOid());
            fairReport.setMenuOrder(counter);
            fairReport.setProjectCode("SOLE");
            counter++;

            session.persist(fairReport);
        } else {
            fairReport.setName("Fuar Raporları");
            fairReport.setCode(MenuEnum.FAIR_REPORT.getMenuAlias());
            fairReport.setDescription("Fuar raporları sayfasıdır.");
            fairReport.setUrl("fuar-raporlari.html");
            fairReport.setParentOid(fairMain.getOid());
            fairReport.setMenuOrder(counter);
            fairReport.setProjectCode("SOLE");
            counter++;

            session.persist(fairReport);
        }

        Menu userMainMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.USER_MAIN.getMenuAlias())).uniqueResult();

        if (userMainMenu == null) {
            userMainMenu = new Menu();
            userMainMenu.setName("Yönetim");
            userMainMenu.setCode(MenuEnum.USER_MAIN.getMenuAlias());
            userMainMenu.setDescription("Sistem operasyonlarının en üst adımıdır.");
            userMainMenu.setMenuOrder(100000);
            userMainMenu.setProjectCode("SOLE");
            session.persist(userMainMenu);
        } else {
            userMainMenu.setName("Yönetim");
            userMainMenu.setCode(MenuEnum.USER_MAIN.getMenuAlias());
            userMainMenu.setDescription("Sistem operasyonlarının en üst adımıdır.");
            userMainMenu.setMenuOrder(100000);
            userMainMenu.setProjectCode("SOLE");
            session.persist(userMainMenu);
        }

        Menu companyUpdateMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.COMPANY.getMenuAlias())).uniqueResult();

        if (companyUpdateMenu == null) {
            companyUpdateMenu = new Menu();
            companyUpdateMenu.setName("Şirket");
            companyUpdateMenu.setCode(MenuEnum.COMPANY.getMenuAlias());
            companyUpdateMenu.setDescription("Şirket bilgileri güncelleme sayfasıdır.");
            companyUpdateMenu.setUrl("sirket.html");
            companyUpdateMenu.setParentOid(userMainMenu.getOid());
            companyUpdateMenu.setMenuOrder(100001);
            companyUpdateMenu.setProjectCode("SOLE");
            session.persist(companyUpdateMenu);
        } else {
            companyUpdateMenu.setName("Şirket");
            companyUpdateMenu.setCode(MenuEnum.COMPANY.getMenuAlias());
            companyUpdateMenu.setDescription("Şirket bilgileri güncelleme sayfasıdır.");
            companyUpdateMenu.setUrl("sirket.html");
            companyUpdateMenu.setParentOid(userMainMenu.getOid());
            companyUpdateMenu.setMenuOrder(100001);
            companyUpdateMenu.setProjectCode("SOLE");
            session.persist(companyUpdateMenu);
        }

        Menu companyMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.COMPANY_UPDATE.getMenuAlias())).uniqueResult();

        if (companyMenu == null) {
            companyMenu = new Menu();
            companyMenu.setName("Firma Düzenle");
            companyMenu.setCode(MenuEnum.COMPANY_UPDATE.getMenuAlias());
            companyMenu.setDescription("Firma ekleme, güncelleme, silme sayfasıdır.");
            companyMenu.setUrl("firma-duzenle.html");
            companyMenu.setParentOid(userMainMenu.getOid());
            companyMenu.setMenuOrder(100002);
            companyMenu.setProjectCode("SOLE");
            session.persist(companyMenu);
        } else {
            companyMenu.setName("Firma Düzenle");
            companyMenu.setCode(MenuEnum.COMPANY_UPDATE.getMenuAlias());
            companyMenu.setDescription("Firma bilgileri güncelleme sayfasıdır.");
            companyMenu.setUrl("firma-duzenle.html");
            companyMenu.setParentOid(userMainMenu.getOid());
            companyMenu.setMenuOrder(100002);
            companyMenu.setProjectCode("SOLE");
            session.persist(companyMenu);
        }

        Menu rolePermissionMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.ROL_PERMISSION.getMenuAlias())).uniqueResult();

        if (rolePermissionMenu == null) {
            rolePermissionMenu = new Menu();
            rolePermissionMenu.setName("Roller ve İzinler");
            rolePermissionMenu.setCode(MenuEnum.ROL_PERMISSION.getMenuAlias());
            rolePermissionMenu.setDescription("Rol/İzin oluşturma, güncelleme, silme ve arama sayfasıdır.");
            rolePermissionMenu.setUrl("rolizin.html");
            rolePermissionMenu.setParentOid(userMainMenu.getOid());
            rolePermissionMenu.setMenuOrder(100002);
            rolePermissionMenu.setProjectCode("SOLE");
            session.persist(rolePermissionMenu);
        } else {
            rolePermissionMenu.setName("Roller ve İzinler");
            rolePermissionMenu.setCode(MenuEnum.ROL_PERMISSION.getMenuAlias());
            rolePermissionMenu.setDescription("Rol/İzin oluşturma, güncelleme, silme ve arama sayfasıdır.");
            rolePermissionMenu.setUrl("rolizin.html");
            rolePermissionMenu.setParentOid(userMainMenu.getOid());
            rolePermissionMenu.setMenuOrder(100002);
            rolePermissionMenu.setProjectCode("SOLE");
            session.persist(rolePermissionMenu);
        }
        Menu userProcessMenu = (Menu) session.createCriteria(Menu.class)
                .add(Restrictions.eq("projectCode", "SOLE"))
                .add(Restrictions.eq("code", MenuEnum.USER_PROCESS.getMenuAlias())).uniqueResult();

        if (userProcessMenu == null) {
            userProcessMenu = new Menu();
            userProcessMenu.setName("Kullanıcı İşlemleri");
            userProcessMenu.setCode(MenuEnum.USER_PROCESS.getMenuAlias());
            userProcessMenu.setDescription("Kullanıcı oluşturma, güncelleme, silme ve arama sayfasıdır.");
            userProcessMenu.setUrl("kullaniciislemleri.html");
            userProcessMenu.setParentOid(userMainMenu.getOid());
            userProcessMenu.setMenuOrder(100003);
            userProcessMenu.setProjectCode("SOLE");
            session.persist(userProcessMenu);
        } else {
            userProcessMenu.setName("Kullanıcı İşlemleri");
            userProcessMenu.setCode(MenuEnum.USER_PROCESS.getMenuAlias());
            userProcessMenu.setDescription("Kullanıcı oluşturma, güncelleme, silme ve arama sayfasıdır.");
            userProcessMenu.setUrl("kullaniciislemleri.html");
            userProcessMenu.setParentOid(userMainMenu.getOid());
            userProcessMenu.setMenuOrder(100003);
            userProcessMenu.setProjectCode("SOLE");
            session.persist(userProcessMenu);
        }

        session.flush();
    }

    public void createPermission(Session session) {
        // ROLE CREATE
        Permission roleCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ROLE_CREATE.getPermissionAlias())).uniqueResult();

        if (roleCreatePermission == null) {
            roleCreatePermission = new Permission();
            roleCreatePermission.setName("Sistem rolü yaratır.");
            roleCreatePermission.setCode(PermissionEnum.ROLE_CREATE.getPermissionAlias());
            roleCreatePermission.setProjectCode("SOLE");
            session.persist(roleCreatePermission);
        } else {
            roleCreatePermission.setName("Sistem rolü yaratır.");
            roleCreatePermission.setCode(PermissionEnum.ROLE_CREATE.getPermissionAlias());
            roleCreatePermission.setProjectCode("SOLE");
            session.persist(roleCreatePermission);
        }
        //ROLE READ
        Permission roleReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ROLE_READ.getPermissionAlias())).uniqueResult();

        if (roleReadPermission == null) {
            roleReadPermission = new Permission();
            roleReadPermission.setName("Sistem rolü görüntüler.");
            roleReadPermission.setCode(PermissionEnum.ROLE_READ.getPermissionAlias());
            roleReadPermission.setProjectCode("SOLE");
            session.persist(roleReadPermission);
        } else {
            roleReadPermission.setName("Sistem rolü görüntüler.");
            roleReadPermission.setCode(PermissionEnum.ROLE_READ.getPermissionAlias());
            roleReadPermission.setProjectCode("SOLE");
            session.persist(roleReadPermission);
        }
        //ROLE UPDATE
        Permission roleUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ROLE_UPDATE.getPermissionAlias())).uniqueResult();

        if (roleUpdatePermission == null) {
            roleUpdatePermission = new Permission();
            roleUpdatePermission.setName("Sistem rolü günceller.");
            roleUpdatePermission.setCode(PermissionEnum.ROLE_UPDATE.getPermissionAlias());
            roleUpdatePermission.setProjectCode("SOLE");
            session.persist(roleUpdatePermission);
        } else {
            roleUpdatePermission.setName("Sistem rolü günceller.");
            roleUpdatePermission.setCode(PermissionEnum.ROLE_UPDATE.getPermissionAlias());
            roleUpdatePermission.setProjectCode("SOLE");
            session.persist(roleUpdatePermission);
        }
        //ROLE UPDATE
        Permission roleDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ROLE_DELETE.getPermissionAlias())).uniqueResult();

        if (roleDeletePermission == null) {
            roleDeletePermission = new Permission();
            roleDeletePermission.setName("Sistem rolü siler.");
            roleDeletePermission.setCode(PermissionEnum.ROLE_DELETE.getPermissionAlias());
            roleDeletePermission.setProjectCode("SOLE");
            session.persist(roleDeletePermission);
        } else {
            roleDeletePermission.setName("Sistem rolü siler.");
            roleDeletePermission.setCode(PermissionEnum.ROLE_DELETE.getPermissionAlias());
            roleDeletePermission.setProjectCode("SOLE");
            session.persist(roleDeletePermission);
        }
        //ROLE UPDATE
        Permission roleAssingPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ROLE_ASSING.getPermissionAlias())).uniqueResult();

        if (roleAssingPermission == null) {
            roleAssingPermission = new Permission();
            roleAssingPermission.setName("Kullanıcıya rol atar.");
            roleAssingPermission.setCode(PermissionEnum.ROLE_ASSING.getPermissionAlias());
            roleAssingPermission.setProjectCode("SOLE");
            session.persist(roleAssingPermission);
        } else {
            roleAssingPermission.setName("Kullanıcıya rol atar.");
            roleAssingPermission.setCode(PermissionEnum.ROLE_ASSING.getPermissionAlias());
            roleAssingPermission.setProjectCode("SOLE");
            session.persist(roleAssingPermission);
        }

        //CUSTOMER CREATE
        Permission customerCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.CUSTOMER_CREATE.getPermissionAlias())).uniqueResult();

        if (customerCreatePermission == null) {
            customerCreatePermission = new Permission();
            customerCreatePermission.setName("Müşteri ekler.");
            customerCreatePermission.setCode(PermissionEnum.CUSTOMER_CREATE.getPermissionAlias());
            customerCreatePermission.setProjectCode("SOLE");
            session.persist(customerCreatePermission);
        } else {
            customerCreatePermission.setName("Müşteri ekler.");
            customerCreatePermission.setCode(PermissionEnum.CUSTOMER_CREATE.getPermissionAlias());
            customerCreatePermission.setProjectCode("SOLE");
            session.persist(customerCreatePermission);
        }
        //CUSTOMER READ
        Permission customerReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.CUSTOMER_READ.getPermissionAlias())).uniqueResult();

        if (customerReadPermission == null) {
            customerReadPermission = new Permission();
            customerReadPermission.setName("Müşteri görüntüler.");
            customerReadPermission.setCode(PermissionEnum.CUSTOMER_READ.getPermissionAlias());
            customerReadPermission.setProjectCode("SOLE");
            session.persist(customerReadPermission);
        } else {
            customerReadPermission.setName("Müşteri görüntüler.");
            customerReadPermission.setCode(PermissionEnum.CUSTOMER_READ.getPermissionAlias());
            customerReadPermission.setProjectCode("SOLE");
            session.persist(customerReadPermission);
        }
        //CUSTOMER UPDATE
        Permission customerUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.CUSTOMER_UPDATE.getPermissionAlias())).uniqueResult();

        if (customerUpdatePermission == null) {
            customerUpdatePermission = new Permission();
            customerUpdatePermission.setName("Müşteri günceller.");
            customerUpdatePermission.setCode(PermissionEnum.CUSTOMER_UPDATE.getPermissionAlias());
            customerUpdatePermission.setProjectCode("SOLE");
            session.persist(customerUpdatePermission);
        } else {
            customerUpdatePermission.setName("Müşteri günceller.");
            customerUpdatePermission.setCode(PermissionEnum.CUSTOMER_UPDATE.getPermissionAlias());
            customerUpdatePermission.setProjectCode("SOLE");
            session.persist(customerUpdatePermission);
        }
        //CUSTOMER DELETE
        Permission customerDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.CUSTOMER_DELETE.getPermissionAlias())).uniqueResult();

        if (customerDeletePermission == null) {
            customerDeletePermission = new Permission();
            customerDeletePermission.setName("Müşteri siler.");
            customerDeletePermission.setCode(PermissionEnum.CUSTOMER_DELETE.getPermissionAlias());
            customerDeletePermission.setProjectCode("SOLE");
            session.persist(customerDeletePermission);
        } else {
            customerDeletePermission.setName("Müşteri siler.");
            customerDeletePermission.setCode(PermissionEnum.CUSTOMER_DELETE.getPermissionAlias());
            customerDeletePermission.setProjectCode("SOLE");
            session.persist(customerDeletePermission);
        }

        //STOCK CREATE
        Permission stockCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_CREATE.getPermissionAlias())).uniqueResult();

        if (stockCreatePermission == null) {
            stockCreatePermission = new Permission();
            stockCreatePermission.setName("Stok ekler.");
            stockCreatePermission.setCode(PermissionEnum.STOCK_CREATE.getPermissionAlias());
            stockCreatePermission.setProjectCode("SOLE");
            session.persist(stockCreatePermission);
        } else {
            stockCreatePermission.setName("Stok ekler.");
            stockCreatePermission.setCode(PermissionEnum.STOCK_CREATE.getPermissionAlias());
            stockCreatePermission.setProjectCode("SOLE");
            session.persist(stockCreatePermission);
        }
        //STOCK READ
        Permission stockReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_READ.getPermissionAlias())).uniqueResult();

        if (stockReadPermission == null) {
            stockReadPermission = new Permission();
            stockReadPermission.setName("Stok görüntüler.");
            stockReadPermission.setCode(PermissionEnum.STOCK_READ.getPermissionAlias());
            stockReadPermission.setProjectCode("SOLE");
            session.persist(stockReadPermission);
        } else {
            stockReadPermission.setName("Stok görüntüler.");
            stockReadPermission.setCode(PermissionEnum.STOCK_READ.getPermissionAlias());
            stockReadPermission.setProjectCode("SOLE");
            session.persist(stockReadPermission);
        }
        //STOCK UPDATE
        Permission stockUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_UPDATE.getPermissionAlias())).uniqueResult();

        if (stockUpdatePermission == null) {
            stockUpdatePermission = new Permission();
            stockUpdatePermission.setName("Stok günceller.");
            stockUpdatePermission.setCode(PermissionEnum.STOCK_UPDATE.getPermissionAlias());
            stockUpdatePermission.setProjectCode("SOLE");
            session.persist(stockUpdatePermission);
        } else {
            stockUpdatePermission.setName("Stok günceller.");
            stockUpdatePermission.setCode(PermissionEnum.STOCK_UPDATE.getPermissionAlias());
            stockUpdatePermission.setProjectCode("SOLE");
            session.persist(stockUpdatePermission);
        }
        //STOCK DELETE
        Permission stockDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_DELETE.getPermissionAlias())).uniqueResult();

        if (stockDeletePermission == null) {
            stockDeletePermission = new Permission();
            stockDeletePermission.setName("Stok siler.");
            stockDeletePermission.setCode(PermissionEnum.STOCK_DELETE.getPermissionAlias());
            stockDeletePermission.setProjectCode("SOLE");
            session.persist(stockDeletePermission);
        } else {
            stockDeletePermission.setName("Stok siler.");
            stockDeletePermission.setCode(PermissionEnum.STOCK_DELETE.getPermissionAlias());
            stockDeletePermission.setProjectCode("SOLE");
            session.persist(stockDeletePermission);
        }

        //STOCK CREATE
        Permission stockAcceptanceCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_ACCEPTANCE_CREATE.getPermissionAlias())).uniqueResult();

        if (stockAcceptanceCreatePermission == null) {
            stockAcceptanceCreatePermission = new Permission();
            stockAcceptanceCreatePermission.setName("Stok Kabul ekler.");
            stockAcceptanceCreatePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_CREATE.getPermissionAlias());
            stockAcceptanceCreatePermission.setProjectCode("SOLE");
            session.persist(stockAcceptanceCreatePermission);
        } else {
            stockAcceptanceCreatePermission.setName("Stok Kabul ekler.");
            stockAcceptanceCreatePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_CREATE.getPermissionAlias());
            stockAcceptanceCreatePermission.setProjectCode("SOLE");
            session.persist(stockAcceptanceCreatePermission);
        }
        //STOCK READ
        Permission stockAcceptanceReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_ACCEPTANCE_READ.getPermissionAlias())).uniqueResult();

        if (stockAcceptanceReadPermission == null) {
            stockAcceptanceReadPermission = new Permission();
            stockAcceptanceReadPermission.setName("Stok Kabul görüntüler.");
            stockAcceptanceReadPermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_READ.getPermissionAlias());
            stockAcceptanceReadPermission.setProjectCode("SOLE");
            session.persist(stockAcceptanceReadPermission);
        } else {
            stockAcceptanceReadPermission.setName("Stok Kabul görüntüler.");
            stockAcceptanceReadPermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_READ.getPermissionAlias());
            stockAcceptanceReadPermission.setProjectCode("SOLE");
            session.persist(stockAcceptanceReadPermission);
        }
        //STOCK UPDATE
        Permission stockAcceptanceUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_ACCEPTANCE_UPDATE.getPermissionAlias())).uniqueResult();

        if (stockAcceptanceUpdatePermission == null) {
            stockAcceptanceUpdatePermission = new Permission();
            stockAcceptanceUpdatePermission.setName("Stok Kabul günceller.");
            stockAcceptanceUpdatePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_UPDATE.getPermissionAlias());
            stockAcceptanceUpdatePermission.setProjectCode("SOLE");
            session.persist(stockAcceptanceUpdatePermission);
        } else {
            stockAcceptanceUpdatePermission.setName("Stok Kabul günceller.");
            stockAcceptanceUpdatePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_UPDATE.getPermissionAlias());
            stockAcceptanceUpdatePermission.setProjectCode("SOLE");
            session.persist(stockAcceptanceUpdatePermission);
        }
        //STOCK DELETE
        Permission stockAcceptanceDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_ACCEPTANCE_DELETE.getPermissionAlias())).uniqueResult();

        if (stockAcceptanceDeletePermission == null) {
            stockAcceptanceDeletePermission = new Permission();
            stockAcceptanceDeletePermission.setName("Stok Kabul siler.");
            stockAcceptanceDeletePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_DELETE.getPermissionAlias());
            stockAcceptanceDeletePermission.setProjectCode("SOLE");
            session.persist(stockAcceptanceDeletePermission);
        } else {
            stockAcceptanceDeletePermission.setName("Stok Kabul siler.");
            stockAcceptanceDeletePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_DELETE.getPermissionAlias());
            stockAcceptanceDeletePermission.setProjectCode("SOLE");
            session.persist(stockAcceptanceDeletePermission);
        }
        //USER CREATE
        Permission userCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.USER_CREATE.getPermissionAlias())).uniqueResult();

        if (userCreatePermission == null) {
            userCreatePermission = new Permission();
            userCreatePermission.setName("Kullanıcı ekler.");
            userCreatePermission.setCode(PermissionEnum.USER_CREATE.getPermissionAlias());
            userCreatePermission.setProjectCode("SOLE");
            session.persist(userCreatePermission);
        } else {
            userCreatePermission.setName("Kullanıcı ekler.");
            userCreatePermission.setCode(PermissionEnum.USER_CREATE.getPermissionAlias());
            userCreatePermission.setProjectCode("SOLE");
            session.persist(userCreatePermission);
        }
        //USER READ
        Permission userReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.USER_READ.getPermissionAlias())).uniqueResult();

        if (userReadPermission == null) {
            userReadPermission = new Permission();
            userReadPermission.setName("Kullanıcı görüntüler.");
            userReadPermission.setCode(PermissionEnum.USER_READ.getPermissionAlias());
            userReadPermission.setProjectCode("SOLE");
            session.persist(userReadPermission);
        } else {
            userReadPermission.setName("Kullanıcı görüntüler.");
            userReadPermission.setCode(PermissionEnum.USER_READ.getPermissionAlias());
            userReadPermission.setProjectCode("SOLE");
            session.persist(userReadPermission);
        }
        //USER UPDATE
        Permission userUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.USER_UPDATE.getPermissionAlias())).uniqueResult();

        if (userUpdatePermission == null) {
            userUpdatePermission = new Permission();
            userUpdatePermission.setName("Kullanıcı günceller.");
            userUpdatePermission.setCode(PermissionEnum.USER_UPDATE.getPermissionAlias());
            userUpdatePermission.setProjectCode("SOLE");
            session.persist(userUpdatePermission);
        } else {
            userUpdatePermission.setName("Kullanıcı günceller.");
            userUpdatePermission.setCode(PermissionEnum.USER_UPDATE.getPermissionAlias());
            userUpdatePermission.setProjectCode("SOLE");
            session.persist(userUpdatePermission);
        }
        //USER DELETE
        Permission userDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.USER_DELETE.getPermissionAlias())).uniqueResult();

        if (userDeletePermission == null) {
            userDeletePermission = new Permission();
            userDeletePermission.setName("Kullanıcı siler.");
            userDeletePermission.setCode(PermissionEnum.USER_DELETE.getPermissionAlias());
            userDeletePermission.setProjectCode("SOLE");
            session.persist(userDeletePermission);
        } else {
            userDeletePermission.setName("Kullanıcı siler.");
            userDeletePermission.setCode(PermissionEnum.USER_DELETE.getPermissionAlias());
            userDeletePermission.setProjectCode("SOLE");
            session.persist(userDeletePermission);
        }

        //MENU READ
        Permission menuReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MENU_READ.getPermissionAlias())).uniqueResult();

        if (menuReadPermission == null) {
            menuReadPermission = new Permission();
            menuReadPermission.setName("Menü seçeneklerini görüntüler.");
            menuReadPermission.setCode(PermissionEnum.MENU_READ.getPermissionAlias());
            menuReadPermission.setProjectCode("SOLE");
            session.persist(menuReadPermission);
        } else {
            menuReadPermission.setName("Menü seçeneklerini görüntüler.");
            menuReadPermission.setCode(PermissionEnum.MENU_READ.getPermissionAlias());
            menuReadPermission.setProjectCode("SOLE");
            session.persist(menuReadPermission);
        }
        //MENU ASSIGN
        Permission menuAssingPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MENU_ASSING.getPermissionAlias())).uniqueResult();

        if (menuAssingPermission == null) {
            menuAssingPermission = new Permission();
            menuAssingPermission.setName("Menüye erişim izni verir.");
            menuAssingPermission.setCode(PermissionEnum.MENU_ASSING.getPermissionAlias());
            menuAssingPermission.setProjectCode("SOLE");
            session.persist(menuAssingPermission);
        } else {
            menuAssingPermission.setName("Menüye erişim izni verir.");
            menuAssingPermission.setCode(PermissionEnum.MENU_ASSING.getPermissionAlias());
            menuAssingPermission.setProjectCode("SOLE");
            session.persist(menuAssingPermission);
        }
        //MENU ASSIGNBACK
        Permission menuAssingBackPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MENU_ASSING_BACK.getPermissionAlias())).uniqueResult();

        if (menuAssingBackPermission == null) {
            menuAssingBackPermission = new Permission();
            menuAssingBackPermission.setName("Menüye erişim izni kaldırır.");
            menuAssingBackPermission.setCode(PermissionEnum.MENU_ASSING_BACK.getPermissionAlias());
            menuAssingBackPermission.setProjectCode("SOLE");
            session.persist(menuAssingBackPermission);
        } else {
            menuAssingBackPermission.setName("Menüye erişim izni kaldırır.");
            menuAssingBackPermission.setCode(PermissionEnum.MENU_ASSING_BACK.getPermissionAlias());
            menuAssingBackPermission.setProjectCode("SOLE");
            session.persist(menuAssingBackPermission);
        }
        //MENU CREATE
        Permission menuCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MENU_CREATE.getPermissionAlias())).uniqueResult();

        if (menuCreatePermission == null) {
            menuCreatePermission = new Permission();
            menuCreatePermission.setName("Menü ekleme izni verir.");
            menuCreatePermission.setCode(PermissionEnum.MENU_CREATE.getPermissionAlias());
            menuCreatePermission.setProjectCode("SOLE");
            session.persist(menuCreatePermission);
        } else {
            menuCreatePermission.setName("Menü ekleme izni verir.");
            menuCreatePermission.setCode(PermissionEnum.MENU_CREATE.getPermissionAlias());
            menuCreatePermission.setProjectCode("SOLE");
            session.persist(menuCreatePermission);
        }
        //MENU UPDATE
        Permission menuUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MENU_UPDATE.getPermissionAlias())).uniqueResult();

        if (menuUpdatePermission == null) {
            menuUpdatePermission = new Permission();
            menuUpdatePermission.setName("Menü güncelleme izni verir.");
            menuUpdatePermission.setCode(PermissionEnum.MENU_UPDATE.getPermissionAlias());
            menuUpdatePermission.setProjectCode("SOLE");
            session.persist(menuUpdatePermission);
        } else {
            menuUpdatePermission.setName("Menü güncelleme izni verir.");
            menuUpdatePermission.setCode(PermissionEnum.MENU_UPDATE.getPermissionAlias());
            menuUpdatePermission.setProjectCode("SOLE");
            session.persist(menuUpdatePermission);
        }
        //MENU DELETE
        Permission menuDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MENU_DELETE.getPermissionAlias())).uniqueResult();

        if (menuDeletePermission == null) {
            menuDeletePermission = new Permission();
            menuDeletePermission.setName("Menü silme izni verir.");
            menuDeletePermission.setCode(PermissionEnum.MENU_DELETE.getPermissionAlias());
            menuDeletePermission.setProjectCode("SOLE");
            session.persist(menuDeletePermission);
        } else {
            menuDeletePermission.setName("Menü silme izni verir.");
            menuDeletePermission.setCode(PermissionEnum.MENU_DELETE.getPermissionAlias());
            menuDeletePermission.setProjectCode("SOLE");
            session.persist(menuDeletePermission);
        }
        //PROFIL READ
        Permission profileReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PROFILE_READ.getPermissionAlias())).uniqueResult();

        if (profileReadPermission == null) {
            profileReadPermission = new Permission();
            profileReadPermission.setName("Profil görüntüleme iznini verir.");
            profileReadPermission.setCode(PermissionEnum.PROFILE_READ.getPermissionAlias());
            profileReadPermission.setProjectCode("SOLE");
            session.persist(profileReadPermission);
        } else {
            profileReadPermission.setName("Profil görüntüleme iznini verir.");
            profileReadPermission.setCode(PermissionEnum.PROFILE_READ.getPermissionAlias());
            profileReadPermission.setProjectCode("SOLE");
            session.persist(profileReadPermission);
        }
        //PROFIL Update
        Permission profileUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PROFILE_UPDATE.getPermissionAlias())).uniqueResult();

        if (profileUpdatePermission == null) {
            profileUpdatePermission = new Permission();
            profileUpdatePermission.setName("Profil güncelleme iznini verir.");
            profileUpdatePermission.setCode(PermissionEnum.PROFILE_UPDATE.getPermissionAlias());
            profileUpdatePermission.setProjectCode("SOLE");
            session.persist(profileUpdatePermission);
        } else {
            profileUpdatePermission.setName("Profil güncelleme iznini verir.");
            profileUpdatePermission.setCode(PermissionEnum.PROFILE_UPDATE.getPermissionAlias());
            profileUpdatePermission.setProjectCode("SOLE");
            session.persist(profileUpdatePermission);
        }
        //SETTING READ
        Permission settingReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SETTING_READ.getPermissionAlias())).uniqueResult();

        if (settingReadPermission == null) {
            settingReadPermission = new Permission();
            settingReadPermission.setName("Ayarları okuma iznini verir.");
            settingReadPermission.setCode(PermissionEnum.SETTING_READ.getPermissionAlias());
            settingReadPermission.setProjectCode("SOLE");
            session.persist(settingReadPermission);
        } else {
            settingReadPermission.setName("Ayarları okuma iznini verir.");
            settingReadPermission.setCode(PermissionEnum.SETTING_READ.getPermissionAlias());
            settingReadPermission.setProjectCode("SOLE");
            session.persist(settingReadPermission);
        }
        //SETTING CREATE
        Permission settingCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SETTING_CREATE.getPermissionAlias())).uniqueResult();

        if (settingCreatePermission == null) {
            settingCreatePermission = new Permission();
            settingCreatePermission.setName("Ayarları yazma iznini verir.");
            settingCreatePermission.setCode(PermissionEnum.SETTING_CREATE.getPermissionAlias());
            settingCreatePermission.setProjectCode("SOLE");
            session.persist(settingCreatePermission);
        } else {
            settingCreatePermission.setName("Ayarları yazma iznini verir.");
            settingCreatePermission.setCode(PermissionEnum.SETTING_CREATE.getPermissionAlias());
            settingCreatePermission.setProjectCode("SOLE");
            session.persist(settingCreatePermission);
        }
        //SETTING UPDATE
        Permission settingUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SETTING_UPDATE.getPermissionAlias())).uniqueResult();

        if (settingUpdatePermission == null) {
            settingUpdatePermission = new Permission();
            settingUpdatePermission.setName("Ayarları güncelleme iznini verir.");
            settingUpdatePermission.setCode(PermissionEnum.SETTING_UPDATE.getPermissionAlias());
            settingUpdatePermission.setProjectCode("SOLE");
            session.persist(settingUpdatePermission);
        } else {
            settingUpdatePermission.setName("Ayarları güncelleme iznini verir.");
            settingUpdatePermission.setCode(PermissionEnum.SETTING_UPDATE.getPermissionAlias());
            settingUpdatePermission.setProjectCode("SOLE");
            session.persist(settingUpdatePermission);
        }
        //SETTING DELETE
        Permission settingDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SETTING_DELETE.getPermissionAlias())).uniqueResult();

        if (settingDeletePermission == null) {
            settingDeletePermission = new Permission();
            settingDeletePermission.setName("Ayarları silme iznini verir.");
            settingDeletePermission.setCode(PermissionEnum.SETTING_DELETE.getPermissionAlias());
            settingDeletePermission.setProjectCode("SOLE");
            session.persist(settingDeletePermission);
        } else {
            settingDeletePermission.setName("Ayarları silme iznini verir.");
            settingDeletePermission.setCode(PermissionEnum.SETTING_DELETE.getPermissionAlias());
            settingDeletePermission.setProjectCode("SOLE");
            session.persist(settingDeletePermission);
        }
        //READ CURRENCY
        Permission currencyReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.CURRENCY_READ.getPermissionAlias())).uniqueResult();

        if (currencyReadPermission == null) {
            currencyReadPermission = new Permission();
            currencyReadPermission.setName("Döviz kurlarını görüntüleme iznini verir.");
            currencyReadPermission.setCode(PermissionEnum.CURRENCY_READ.getPermissionAlias());
            currencyReadPermission.setProjectCode("SOLE");
            session.persist(currencyReadPermission);
        } else {
            currencyReadPermission.setName("Döviz kurlarını görüntüleme iznini verir.");
            currencyReadPermission.setCode(PermissionEnum.CURRENCY_READ.getPermissionAlias());
            currencyReadPermission.setProjectCode("SOLE");
            session.persist(currencyReadPermission);
        }
        //READ COMPANY
        Permission companyReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.COMPANY_READ.getPermissionAlias())).uniqueResult();

        if (companyReadPermission == null) {
            companyReadPermission = new Permission();
            companyReadPermission.setName("Sistemdeki şirketleri görüntüleme izni verir.");
            companyReadPermission.setCode(PermissionEnum.COMPANY_READ.getPermissionAlias());
            companyReadPermission.setProjectCode("SOLE");
            session.persist(companyReadPermission);
        } else {
            companyReadPermission.setName("Sistemdeki şirketleri görüntüleme izni verir.");
            companyReadPermission.setCode(PermissionEnum.COMPANY_READ.getPermissionAlias());
            companyReadPermission.setProjectCode("SOLE");
            session.persist(companyReadPermission);
        }

        //CREATE COMPANY
        Permission companyCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.COMPANY_CREATE.getPermissionAlias())).uniqueResult();

        if (companyCreatePermission == null) {
            companyCreatePermission = new Permission();
            companyCreatePermission.setName("Sisteme şirket ekleme izni verir.");
            companyCreatePermission.setCode(PermissionEnum.COMPANY_CREATE.getPermissionAlias());
            companyCreatePermission.setProjectCode("SOLE");
            session.persist(companyCreatePermission);
        } else {
            companyCreatePermission.setName("Sisteme şirket ekleme izni verir.");
            companyCreatePermission.setCode(PermissionEnum.COMPANY_CREATE.getPermissionAlias());
            companyCreatePermission.setProjectCode("SOLE");
            session.persist(companyCreatePermission);
        }

        //CREATE UPDATE
        Permission companyUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.COMPANY_UPDATE.getPermissionAlias())).uniqueResult();

        if (companyUpdatePermission == null) {
            companyUpdatePermission = new Permission();
            companyUpdatePermission.setName("Sistemde şirket güncelleme izni verir.");
            companyUpdatePermission.setCode(PermissionEnum.COMPANY_UPDATE.getPermissionAlias());
            companyUpdatePermission.setProjectCode("SOLE");
            session.persist(companyUpdatePermission);
        } else {
            companyUpdatePermission.setName("Sistemde şirket güncelleme izni verir.");
            companyUpdatePermission.setCode(PermissionEnum.COMPANY_UPDATE.getPermissionAlias());
            companyUpdatePermission.setProjectCode("SOLE");
            session.persist(companyUpdatePermission);
        }

        //CREATE DELETE
        Permission companyDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.COMPANY_DELETE.getPermissionAlias())).uniqueResult();

        if (companyDeletePermission == null) {
            companyDeletePermission = new Permission();
            companyDeletePermission.setName("Sistemde şirket silme izni verir.");
            companyDeletePermission.setCode(PermissionEnum.COMPANY_DELETE.getPermissionAlias());
            companyDeletePermission.setProjectCode("SOLE");
            session.persist(companyDeletePermission);
        } else {
            companyDeletePermission.setName("Sistemde şirket silme izni verir.");
            companyDeletePermission.setCode(PermissionEnum.COMPANY_DELETE.getPermissionAlias());
            companyDeletePermission.setProjectCode("SOLE");
            session.persist(companyDeletePermission);
        }

        //PERMISSION READ
        Permission permissionReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PERMISSION_READ.getPermissionAlias())).uniqueResult();

        if (permissionReadPermission == null) {
            permissionReadPermission = new Permission();
            permissionReadPermission.setName("İzin görütüleyebilme izni verir.");
            permissionReadPermission.setCode(PermissionEnum.PERMISSION_READ.getPermissionAlias());
            permissionReadPermission.setProjectCode("SOLE");
            session.persist(permissionReadPermission);
        } else {
            permissionReadPermission.setName("İzin görütüleyebilme izni verir.");
            permissionReadPermission.setCode(PermissionEnum.PERMISSION_READ.getPermissionAlias());
            permissionReadPermission.setProjectCode("SOLE");
            session.persist(permissionReadPermission);
        }
        //PERMISSION ASSIGN
        Permission permissionAssingPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PERMISSION_ASSING.getPermissionAlias())).uniqueResult();

        if (permissionAssingPermission == null) {
            permissionAssingPermission = new Permission();
            permissionAssingPermission.setName("İzin atayabilme izni verir.");
            permissionAssingPermission.setCode(PermissionEnum.PERMISSION_ASSING.getPermissionAlias());
            permissionAssingPermission.setProjectCode("SOLE");
            session.persist(permissionAssingPermission);
        } else {
            permissionAssingPermission.setName("İzin atayabilme izni verir.");
            permissionAssingPermission.setCode(PermissionEnum.PERMISSION_ASSING.getPermissionAlias());
            permissionAssingPermission.setProjectCode("SOLE");
            session.persist(permissionAssingPermission);
        }

        //Model CREATE
        Permission modelCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_CREATE.getPermissionAlias())).uniqueResult();

        if (modelCreate == null) {
            modelCreate = new Permission();
            modelCreate.setName("Model oluşturma izni verir.");
            modelCreate.setCode(PermissionEnum.MODEL_CREATE.getPermissionAlias());
            modelCreate.setProjectCode("SOLE");
            session.persist(modelCreate);
        } else {
            modelCreate.setName("Model oluşturma izni verir.");
            modelCreate.setCode(PermissionEnum.MODEL_CREATE.getPermissionAlias());
            modelCreate.setProjectCode("SOLE");
            session.persist(modelCreate);
        }

        //Model READ
        Permission modelRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_READ.getPermissionAlias())).uniqueResult();

        if (modelRead == null) {
            modelRead = new Permission();
            modelRead.setName("Model görüntüleme izni verir.");
            modelRead.setCode(PermissionEnum.MODEL_READ.getPermissionAlias());
            modelRead.setProjectCode("SOLE");
            session.persist(modelRead);
        } else {
            modelRead.setName("Model görüntüleme izni verir.");
            modelRead.setCode(PermissionEnum.MODEL_READ.getPermissionAlias());
            modelRead.setProjectCode("SOLE");
            session.persist(modelRead);
        }

        //Model UPDATE
        Permission modelUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_UPDATE.getPermissionAlias())).uniqueResult();

        if (modelUpdate == null) {
            modelUpdate = new Permission();
            modelUpdate.setName("Model güncelleme izni verir.");
            modelUpdate.setCode(PermissionEnum.MODEL_UPDATE.getPermissionAlias());
            modelUpdate.setProjectCode("SOLE");
            session.persist(modelUpdate);
        } else {
            modelUpdate.setName("Model güncelleme izni verir.");
            modelUpdate.setCode(PermissionEnum.MODEL_UPDATE.getPermissionAlias());
            modelUpdate.setProjectCode("SOLE");
            session.persist(modelUpdate);
        }

        //Model DELETE
        Permission modelDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_DELETE.getPermissionAlias())).uniqueResult();

        if (modelDelete == null) {
            modelDelete = new Permission();
            modelDelete.setName("Model silme izni verir.");
            modelDelete.setCode(PermissionEnum.MODEL_DELETE.getPermissionAlias());
            modelDelete.setProjectCode("SOLE");
            session.persist(modelDelete);
        } else {
            modelDelete.setName("Model silme izni verir.");
            modelDelete.setCode(PermissionEnum.MODEL_DELETE.getPermissionAlias());
            modelDelete.setProjectCode("SOLE");
            session.persist(modelDelete);
        }

        //WORK ORDER PRICE
        Permission modelPrice = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PRICE.getPermissionAlias())).uniqueResult();

        if (modelPrice == null) {
            modelPrice = new Permission();
            modelPrice.setName("Model fiyatlandırma iznini verir.");
            modelPrice.setCode(PermissionEnum.MODEL_PRICE.getPermissionAlias());
            modelPrice.setProjectCode("SOLE");
            session.persist(modelPrice);
        } else {
            modelPrice.setName("Model fiyatlandırma iznini verir.");
            modelPrice.setCode(PermissionEnum.MODEL_PRICE.getPermissionAlias());
            modelPrice.setProjectCode("SOLE");
            session.persist(modelPrice);
        }

        //MESSAGE READ
        Permission messageRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MESSAGE_READ.getPermissionAlias())).uniqueResult();

        if (messageRead == null) {
            messageRead = new Permission();
            messageRead.setName("Mesajları okuma iznini verir.");
            messageRead.setCode(PermissionEnum.MESSAGE_READ.getPermissionAlias());
            messageRead.setProjectCode("SOLE");
            session.persist(messageRead);
        } else {
            messageRead.setName("Mesajları okuma iznini verir.");
            messageRead.setCode(PermissionEnum.MESSAGE_READ.getPermissionAlias());
            messageRead.setProjectCode("SOLE");
            session.persist(messageRead);
        }
        //MESSAGE CREATE
        Permission messageCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MESSAGE_CREATE.getPermissionAlias())).uniqueResult();

        if (messageCreate == null) {
            messageCreate = new Permission();
            messageCreate.setName("Mesaj yazma iznini verir.");
            messageCreate.setCode(PermissionEnum.MESSAGE_CREATE.getPermissionAlias());
            messageCreate.setProjectCode("SOLE");
            session.persist(messageCreate);
        } else {
            messageCreate.setName("Mesaj yazma iznini verir.");
            messageCreate.setCode(PermissionEnum.MESSAGE_CREATE.getPermissionAlias());
            messageCreate.setProjectCode("SOLE");
            session.persist(messageCreate);
        }

        //SUPPLY
        Permission supply = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLY.getPermissionAlias())).uniqueResult();

        if (supply == null) {
            supply = new Permission();
            supply.setName("Tedarik işlemlerini yapabilir.");
            supply.setCode(PermissionEnum.SUPPLY.getPermissionAlias());
            supply.setProjectCode("SOLE");
            session.persist(supply);
        } else {
            supply.setName("Tedarik işlemlerini yapabilir.");
            supply.setCode(PermissionEnum.SUPPLY.getPermissionAlias());
            supply.setProjectCode("SOLE");
            session.persist(supply);
        }

        //PRODUCTION LINE CREATE
        Permission productionLineCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_LINE_CREATE.getPermissionAlias())).uniqueResult();

        if (productionLineCreate == null) {
            productionLineCreate = new Permission();
            productionLineCreate.setName("Üretim hattı tanımlayabilir.");
            productionLineCreate.setCode(PermissionEnum.PRODUCTION_LINE_CREATE.getPermissionAlias());
            productionLineCreate.setProjectCode("SOLE");
            session.persist(productionLineCreate);
        } else {
            productionLineCreate.setName("Üretim hattı tanımlayabilir.");
            productionLineCreate.setCode(PermissionEnum.PRODUCTION_LINE_CREATE.getPermissionAlias());
            productionLineCreate.setProjectCode("SOLE");
            session.persist(productionLineCreate);
        }

        //PRODUCTION LINE READ
        Permission productionLineRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_LINE_READ.getPermissionAlias())).uniqueResult();

        if (productionLineRead == null) {
            productionLineRead = new Permission();
            productionLineRead.setName("Üretim hattı görüntüleyebilir.");
            productionLineRead.setCode(PermissionEnum.PRODUCTION_LINE_READ.getPermissionAlias());
            productionLineRead.setProjectCode("SOLE");
            session.persist(productionLineRead);
        } else {
            productionLineRead.setName("Üretim hattı görüntüleyebilir.");
            productionLineRead.setCode(PermissionEnum.PRODUCTION_LINE_READ.getPermissionAlias());
            productionLineRead.setProjectCode("SOLE");
            session.persist(productionLineRead);
        }

        //PRODUCTION LINE UPDATE
        Permission productionLineUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_LINE_UPDATE.getPermissionAlias())).uniqueResult();

        if (productionLineUpdate == null) {
            productionLineUpdate = new Permission();
            productionLineUpdate.setName("Üretim hattı güncelleyebilir.");
            productionLineUpdate.setCode(PermissionEnum.PRODUCTION_LINE_UPDATE.getPermissionAlias());
            productionLineUpdate.setProjectCode("SOLE");
            session.persist(productionLineUpdate);
        } else {
            productionLineUpdate.setName("Üretim hattı güncelleyebilir.");
            productionLineUpdate.setCode(PermissionEnum.PRODUCTION_LINE_UPDATE.getPermissionAlias());
            productionLineUpdate.setProjectCode("SOLE");
            session.persist(productionLineUpdate);
        }

        //PRODUCTION LINE DELETE
        Permission productionLineDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_LINE_DELETE.getPermissionAlias())).uniqueResult();

        if (productionLineDelete == null) {
            productionLineDelete = new Permission();
            productionLineDelete.setName("Üretim hattı silebilir.");
            productionLineDelete.setCode(PermissionEnum.PRODUCTION_LINE_DELETE.getPermissionAlias());
            productionLineDelete.setProjectCode("SOLE");
            session.persist(productionLineDelete);
        } else {
            productionLineDelete.setName("Üretim hattı silebilir.");
            productionLineDelete.setCode(PermissionEnum.PRODUCTION_LINE_DELETE.getPermissionAlias());
            productionLineDelete.setProjectCode("SOLE");
            session.persist(productionLineDelete);
        }

        //MATERIAL_TYPE LINE CREATE
        Permission materialTypeCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MATERIAL_TYPE_CREATE.getPermissionAlias())).uniqueResult();

        if (materialTypeCreate == null) {
            materialTypeCreate = new Permission();
            materialTypeCreate.setName("Malzeme Türü tanımlayabilir.");
            materialTypeCreate.setCode(PermissionEnum.MATERIAL_TYPE_CREATE.getPermissionAlias());
            materialTypeCreate.setProjectCode("SOLE");
            session.persist(materialTypeCreate);
        } else {
            materialTypeCreate.setName("Malzeme Türü tanımlayabilir.");
            materialTypeCreate.setCode(PermissionEnum.MATERIAL_TYPE_CREATE.getPermissionAlias());
            materialTypeCreate.setProjectCode("SOLE");
            session.persist(materialTypeCreate);
        }

        //MATERIAL_TYPE LINE READ
        Permission materialTypeRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MATERIAL_TYPE_READ.getPermissionAlias())).uniqueResult();

        if (materialTypeRead == null) {
            materialTypeRead = new Permission();
            materialTypeRead.setName("Malzeme Türü görüntüleyebilir.");
            materialTypeRead.setCode(PermissionEnum.MATERIAL_TYPE_READ.getPermissionAlias());
            materialTypeRead.setProjectCode("SOLE");
            session.persist(materialTypeRead);
        } else {
            materialTypeRead.setName("Malzeme Türü görüntüleyebilir.");
            materialTypeRead.setCode(PermissionEnum.MATERIAL_TYPE_READ.getPermissionAlias());
            materialTypeRead.setProjectCode("SOLE");
            session.persist(materialTypeRead);
        }

        //MATERIAL_TYPE LINE UPDATE
        Permission materialTypeUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MATERIAL_TYPE_UPDATE.getPermissionAlias())).uniqueResult();

        if (materialTypeUpdate == null) {
            materialTypeUpdate = new Permission();
            materialTypeUpdate.setName("Malzeme Türü güncelleyebilir.");
            materialTypeUpdate.setCode(PermissionEnum.MATERIAL_TYPE_UPDATE.getPermissionAlias());
            materialTypeUpdate.setProjectCode("SOLE");
            session.persist(materialTypeUpdate);
        } else {
            materialTypeUpdate.setName("Malzeme Türü güncelleyebilir.");
            materialTypeUpdate.setCode(PermissionEnum.MATERIAL_TYPE_UPDATE.getPermissionAlias());
            materialTypeUpdate.setProjectCode("SOLE");
            session.persist(materialTypeUpdate);
        }

        //MATERIAL_TYPE LINE DELETE
        Permission materialTypeDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MATERIAL_TYPE_DELETE.getPermissionAlias())).uniqueResult();

        if (materialTypeDelete == null) {
            materialTypeDelete = new Permission();
            materialTypeDelete.setName("Malzeme Türü silebilir.");
            materialTypeDelete.setCode(PermissionEnum.MATERIAL_TYPE_DELETE.getPermissionAlias());
            materialTypeDelete.setProjectCode("SOLE");
            session.persist(materialTypeDelete);
        } else {
            materialTypeDelete.setName("Malzeme Türü silebilir.");
            materialTypeDelete.setCode(PermissionEnum.MATERIAL_TYPE_DELETE.getPermissionAlias());
            materialTypeDelete.setProjectCode("SOLE");
            session.persist(materialTypeDelete);
        }

        //INTERNAL_CRITICAL LINE CREATE  internalCritical
        Permission internalCriticalCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.INTERNAL_CRITICAL_CREATE.getPermissionAlias())).uniqueResult();

        if (internalCriticalCreate == null) {
            internalCriticalCreate = new Permission();
            internalCriticalCreate.setName("İç Kritik tanımlayabilir.");
            internalCriticalCreate.setCode(PermissionEnum.INTERNAL_CRITICAL_CREATE.getPermissionAlias());
            internalCriticalCreate.setProjectCode("SOLE");
            session.persist(internalCriticalCreate);
        } else {
            internalCriticalCreate.setName("İç Kritik tanımlayabilir.");
            internalCriticalCreate.setCode(PermissionEnum.INTERNAL_CRITICAL_CREATE.getPermissionAlias());
            internalCriticalCreate.setProjectCode("SOLE");
            session.persist(internalCriticalCreate);
        }

        //INTERNAL_CRITICAL LINE READ
        Permission internalCriticalRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.INTERNAL_CRITICAL_READ.getPermissionAlias())).uniqueResult();

        if (internalCriticalRead == null) {
            internalCriticalRead = new Permission();
            internalCriticalRead.setName("İç Kritik görüntüleyebilir.");
            internalCriticalRead.setCode(PermissionEnum.INTERNAL_CRITICAL_READ.getPermissionAlias());
            internalCriticalRead.setProjectCode("SOLE");
            session.persist(internalCriticalRead);
        } else {
            internalCriticalRead.setName("İç Kritik görüntüleyebilir.");
            internalCriticalRead.setCode(PermissionEnum.INTERNAL_CRITICAL_READ.getPermissionAlias());
            internalCriticalRead.setProjectCode("SOLE");
            session.persist(internalCriticalRead);
        }

        //INTERNAL_CRITICAL LINE UPDATE
        Permission internalCriticalUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.INTERNAL_CRITICAL_UPDATE.getPermissionAlias())).uniqueResult();

        if (internalCriticalUpdate == null) {
            internalCriticalUpdate = new Permission();
            internalCriticalUpdate.setName("İç Kritik güncelleyebilir.");
            internalCriticalUpdate.setCode(PermissionEnum.INTERNAL_CRITICAL_UPDATE.getPermissionAlias());
            internalCriticalUpdate.setProjectCode("SOLE");
            session.persist(internalCriticalUpdate);
        } else {
            internalCriticalUpdate.setName("İç Kritik güncelleyebilir.");
            internalCriticalUpdate.setCode(PermissionEnum.INTERNAL_CRITICAL_UPDATE.getPermissionAlias());
            internalCriticalUpdate.setProjectCode("SOLE");
            session.persist(internalCriticalUpdate);
        }

        //INTERNAL_CRITICAL LINE DELETE
        Permission internalCriticalDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.INTERNAL_CRITICAL_DELETE.getPermissionAlias())).uniqueResult();

        if (internalCriticalDelete == null) {
            internalCriticalDelete = new Permission();
            internalCriticalDelete.setName("İç Kritik silebilir.");
            internalCriticalDelete.setCode(PermissionEnum.INTERNAL_CRITICAL_DELETE.getPermissionAlias());
            internalCriticalDelete.setProjectCode("SOLE");
            session.persist(internalCriticalDelete);
        } else {
            internalCriticalDelete.setName("İç Kritik silebilir.");
            internalCriticalDelete.setCode(PermissionEnum.INTERNAL_CRITICAL_DELETE.getPermissionAlias());
            internalCriticalDelete.setProjectCode("SOLE");
            session.persist(internalCriticalDelete);
        }

        //ProductSchedule Update
        Permission productionScheduleUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_SCHEDULE_UPDATE.getPermissionAlias())).uniqueResult();

        if (productionScheduleUpdate == null) {
            productionScheduleUpdate = new Permission();
            productionScheduleUpdate.setName("Üretim takvimini güncelleyebilir.");
            productionScheduleUpdate.setCode(PermissionEnum.PRODUCTION_SCHEDULE_UPDATE.getPermissionAlias());
            productionScheduleUpdate.setProjectCode("SOLE");
            session.persist(productionScheduleUpdate);
        } else {
            productionScheduleUpdate.setName("Üretim takvimini güncelleyebilir.");
            productionScheduleUpdate.setCode(PermissionEnum.PRODUCTION_SCHEDULE_UPDATE.getPermissionAlias());
            productionScheduleUpdate.setProjectCode("SOLE");
            session.persist(productionScheduleUpdate);
        }

        //SUPPLIER_CREATE  supplierCreate
        Permission supplierCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLIER_CREATE.getPermissionAlias())).uniqueResult();

        if (supplierCreate == null) {
            supplierCreate = new Permission();
            supplierCreate.setName("Tedarikçi tanımlayabilir.");
            supplierCreate.setCode(PermissionEnum.SUPPLIER_CREATE.getPermissionAlias());
            supplierCreate.setProjectCode("SOLE");
            session.persist(supplierCreate);
        } else {
            supplierCreate.setName("Tedarikçi tanımlayabilir.");
            supplierCreate.setCode(PermissionEnum.SUPPLIER_CREATE.getPermissionAlias());
            supplierCreate.setProjectCode("SOLE");
            session.persist(supplierCreate);
        }

        //SUPPLIER_READ supplierRead
        Permission supplierRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLIER_READ.getPermissionAlias())).uniqueResult();

        if (supplierRead == null) {
            supplierRead = new Permission();
            supplierRead.setName("Tedarikçi görüntüleyebilir.");
            supplierRead.setCode(PermissionEnum.SUPPLIER_READ.getPermissionAlias());
            supplierRead.setProjectCode("SOLE");
            session.persist(supplierRead);
        } else {
            supplierRead.setName("Tedarikçi görüntüleyebilir.");
            supplierRead.setCode(PermissionEnum.SUPPLIER_READ.getPermissionAlias());
            supplierRead.setProjectCode("SOLE");
            session.persist(supplierRead);
        }

        //SUPPLIER_UPDATE supplierUpdate
        Permission supplierUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLIER_UPDATE.getPermissionAlias())).uniqueResult();

        if (supplierUpdate == null) {
            supplierUpdate = new Permission();
            supplierUpdate.setName("Tedarikçi güncelleyebilir.");
            supplierUpdate.setCode(PermissionEnum.SUPPLIER_UPDATE.getPermissionAlias());
            supplierUpdate.setProjectCode("SOLE");
            session.persist(supplierUpdate);
        } else {
            supplierUpdate.setName("Tedarikçi güncelleyebilir.");
            supplierUpdate.setCode(PermissionEnum.SUPPLIER_UPDATE.getPermissionAlias());
            supplierUpdate.setProjectCode("SOLE");
            session.persist(supplierUpdate);
        }

        //SUPPLIER_DELETE supplierDelete
        Permission supplierDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLIER_DELETE.getPermissionAlias())).uniqueResult();

        if (supplierDelete == null) {
            supplierDelete = new Permission();
            supplierDelete.setName("Tedarikçi silebilir.");
            supplierDelete.setCode(PermissionEnum.SUPPLIER_DELETE.getPermissionAlias());
            supplierDelete.setProjectCode("SOLE");
            session.persist(supplierDelete);
        } else {
            supplierDelete.setName("Tedarikçi silebilir.");
            supplierDelete.setCode(PermissionEnum.SUPPLIER_DELETE.getPermissionAlias());
            supplierDelete.setProjectCode("SOLE");
            session.persist(supplierDelete);
        }


        //STOCK_TYPE_CREATE  stockTypeCreate
        Permission stockTypeCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_TYPE_CREATE.getPermissionAlias())).uniqueResult();

        if (stockTypeCreate == null) {
            stockTypeCreate = new Permission();
            stockTypeCreate.setName("Stok türü tanımlayabilir.");
            stockTypeCreate.setCode(PermissionEnum.STOCK_TYPE_CREATE.getPermissionAlias());
            stockTypeCreate.setProjectCode("SOLE");
            session.persist(stockTypeCreate);
        } else {
            stockTypeCreate.setName("Stok türü tanımlayabilir.");
            stockTypeCreate.setCode(PermissionEnum.STOCK_TYPE_CREATE.getPermissionAlias());
            stockTypeCreate.setProjectCode("SOLE");
            session.persist(stockTypeCreate);
        }

//STOCK_TYPE_READ stockTypeRead
        Permission stockTypeRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_TYPE_READ.getPermissionAlias())).uniqueResult();

        if (stockTypeRead == null) {
            stockTypeRead = new Permission();
            stockTypeRead.setName("Stok türü görüntüleyebilir.");
            stockTypeRead.setCode(PermissionEnum.STOCK_TYPE_READ.getPermissionAlias());
            stockTypeRead.setProjectCode("SOLE");
            session.persist(stockTypeRead);
        } else {
            stockTypeRead.setName("Stok türü görüntüleyebilir.");
            stockTypeRead.setCode(PermissionEnum.STOCK_TYPE_READ.getPermissionAlias());
            stockTypeRead.setProjectCode("SOLE");
            session.persist(stockTypeRead);
        }

//STOCK_TYPE_UPDATE stockTypeUpdate
        Permission stockTypeUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_TYPE_UPDATE.getPermissionAlias())).uniqueResult();

        if (stockTypeUpdate == null) {
            stockTypeUpdate = new Permission();
            stockTypeUpdate.setName("Stok türü güncelleyebilir.");
            stockTypeUpdate.setCode(PermissionEnum.STOCK_TYPE_UPDATE.getPermissionAlias());
            stockTypeUpdate.setProjectCode("SOLE");
            session.persist(stockTypeUpdate);
        } else {
            stockTypeUpdate.setName("Stok türü güncelleyebilir.");
            stockTypeUpdate.setCode(PermissionEnum.STOCK_TYPE_UPDATE.getPermissionAlias());
            stockTypeUpdate.setProjectCode("SOLE");
            session.persist(stockTypeUpdate);
        }

//STOCK_TYPE_DELETE stockTypeDelete
        Permission stockTypeDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STOCK_TYPE_DELETE.getPermissionAlias())).uniqueResult();

        if (stockTypeDelete == null) {
            stockTypeDelete = new Permission();
            stockTypeDelete.setName("Stok türü silebilir.");
            stockTypeDelete.setCode(PermissionEnum.STOCK_TYPE_DELETE.getPermissionAlias());
            stockTypeDelete.setProjectCode("SOLE");
            session.persist(stockTypeDelete);
        } else {
            stockTypeDelete.setName("Stok türü silebilir.");
            stockTypeDelete.setCode(PermissionEnum.STOCK_TYPE_DELETE.getPermissionAlias());
            stockTypeDelete.setProjectCode("SOLE");
            session.persist(stockTypeDelete);
        }

        Permission bulkSupplyCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLY_CREATE.getPermissionAlias())).uniqueResult();

        if (bulkSupplyCreate == null) {
            bulkSupplyCreate = new Permission();
            bulkSupplyCreate.setName("Tedarik oluşturabilir.");
            bulkSupplyCreate.setCode(PermissionEnum.SUPPLY_CREATE.getPermissionAlias());
            bulkSupplyCreate.setProjectCode("SOLE");
            session.persist(bulkSupplyCreate);
        } else {
            bulkSupplyCreate.setName("Tedarik oluşturabilir.");
            bulkSupplyCreate.setCode(PermissionEnum.SUPPLY_CREATE.getPermissionAlias());
            bulkSupplyCreate.setProjectCode("SOLE");
            session.persist(bulkSupplyCreate);
        }

        Permission bulkSupplyRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLY_READ.getPermissionAlias())).uniqueResult();

        if (bulkSupplyRead == null) {
            bulkSupplyRead = new Permission();
            bulkSupplyRead.setName("Tedarik görüntüleyebilir.");
            bulkSupplyRead.setCode(PermissionEnum.SUPPLY_READ.getPermissionAlias());
            bulkSupplyRead.setProjectCode("SOLE");
            session.persist(bulkSupplyRead);
        } else {
            bulkSupplyRead.setName("Tedarik görüntüleyebilir.");
            bulkSupplyRead.setCode(PermissionEnum.SUPPLY_READ.getPermissionAlias());
            bulkSupplyRead.setProjectCode("SOLE");
            session.persist(bulkSupplyRead);
        }

        Permission bulkSupplyUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLY_UPDATE.getPermissionAlias())).uniqueResult();

        if (bulkSupplyUpdate == null) {
            bulkSupplyUpdate = new Permission();
            bulkSupplyUpdate.setName("Tedarik güncelleyebilir.");
            bulkSupplyUpdate.setCode(PermissionEnum.SUPPLY_UPDATE.getPermissionAlias());
            bulkSupplyUpdate.setProjectCode("SOLE");
            session.persist(bulkSupplyUpdate);
        } else {
            bulkSupplyUpdate.setName("Tedarik güncelleyebilir.");
            bulkSupplyUpdate.setCode(PermissionEnum.SUPPLY_UPDATE.getPermissionAlias());
            bulkSupplyUpdate.setProjectCode("SOLE");
            session.persist(bulkSupplyUpdate);
        }

        Permission bulkSupplyDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SUPPLY_DELETE.getPermissionAlias())).uniqueResult();

        if (bulkSupplyDelete == null) {
            bulkSupplyDelete = new Permission();
            bulkSupplyDelete.setName("Tedarik silebilir.");
            bulkSupplyDelete.setCode(PermissionEnum.SUPPLY_DELETE.getPermissionAlias());
            bulkSupplyDelete.setProjectCode("SOLE");
            session.persist(bulkSupplyDelete);
        } else {
            bulkSupplyDelete.setName("Tedarik silebilir.");
            bulkSupplyDelete.setCode(PermissionEnum.SUPPLY_DELETE.getPermissionAlias());
            bulkSupplyDelete.setProjectCode("SOLE");
            session.persist(bulkSupplyDelete);
        }

        Permission shipmentInfoCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_INFO_CREATE.getPermissionAlias())).uniqueResult();

        if (shipmentInfoCreate == null) {
            shipmentInfoCreate = new Permission();
            shipmentInfoCreate.setName("Sevkiyat bilgisi tanımlayabilir.");
            shipmentInfoCreate.setCode(PermissionEnum.SHIPMENT_INFO_CREATE.getPermissionAlias());
            shipmentInfoCreate.setProjectCode("SOLE");
            session.persist(shipmentInfoCreate);
        } else {
            shipmentInfoCreate.setName("Sevkiyat bilgisi tanımlayabilir.");
            shipmentInfoCreate.setCode(PermissionEnum.SHIPMENT_INFO_CREATE.getPermissionAlias());
            shipmentInfoCreate.setProjectCode("SOLE");
            session.persist(shipmentInfoCreate);
        }

        Permission shipmentInfoRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_INFO_READ.getPermissionAlias())).uniqueResult();

        if (shipmentInfoRead == null) {
            shipmentInfoRead = new Permission();
            shipmentInfoRead.setName("Sevkiyat bilgisi görüntüleyebilir.");
            shipmentInfoRead.setCode(PermissionEnum.SHIPMENT_INFO_READ.getPermissionAlias());
            shipmentInfoRead.setProjectCode("SOLE");
            session.persist(shipmentInfoRead);
        } else {
            shipmentInfoRead.setName("Sevkiyat bilgisi görüntüleyebilir.");
            shipmentInfoRead.setCode(PermissionEnum.SHIPMENT_INFO_READ.getPermissionAlias());
            shipmentInfoRead.setProjectCode("SOLE");
            session.persist(shipmentInfoRead);
        }

        Permission shipmentInfoUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_INFO_UPDATE.getPermissionAlias())).uniqueResult();

        if (shipmentInfoUpdate == null) {
            shipmentInfoUpdate = new Permission();
            shipmentInfoUpdate.setName("Sevkiyat bilgisi güncelleyebilir.");
            shipmentInfoUpdate.setCode(PermissionEnum.SHIPMENT_INFO_UPDATE.getPermissionAlias());
            shipmentInfoUpdate.setProjectCode("SOLE");
            session.persist(shipmentInfoUpdate);
        } else {
            shipmentInfoUpdate.setName("Sevkiyat bilgisi güncelleyebilir.");
            shipmentInfoUpdate.setCode(PermissionEnum.SHIPMENT_INFO_UPDATE.getPermissionAlias());
            shipmentInfoUpdate.setProjectCode("SOLE");
            session.persist(shipmentInfoUpdate);
        }

        Permission shipmentInfoDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_INFO_DELETE.getPermissionAlias())).uniqueResult();

        if (shipmentInfoDelete == null) {
            shipmentInfoDelete = new Permission();
            shipmentInfoDelete.setName("Sevkiyat bilgisi silebilir.");
            shipmentInfoDelete.setCode(PermissionEnum.SHIPMENT_INFO_DELETE.getPermissionAlias());
            shipmentInfoDelete.setProjectCode("SOLE");
            session.persist(shipmentInfoDelete);
        } else {
            shipmentInfoDelete.setName("Sevkiyat bilgisi silebilir.");
            shipmentInfoDelete.setCode(PermissionEnum.SHIPMENT_INFO_DELETE.getPermissionAlias());
            shipmentInfoDelete.setProjectCode("SOLE");
            session.persist(shipmentInfoDelete);
        }

        Permission activityReportCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ACTIVITY_REPORT_CREATE.getPermissionAlias())).uniqueResult();

        if (activityReportCreate == null) {
            activityReportCreate = new Permission();
            activityReportCreate.setName("Faaliyet raporu tanımlayabilir.");
            activityReportCreate.setCode(PermissionEnum.ACTIVITY_REPORT_CREATE.getPermissionAlias());
            activityReportCreate.setProjectCode("SOLE");
            session.persist(activityReportCreate);
        } else {
            activityReportCreate.setName("Faaliyet raporu tanımlayabilir.");
            activityReportCreate.setCode(PermissionEnum.ACTIVITY_REPORT_CREATE.getPermissionAlias());
            activityReportCreate.setProjectCode("SOLE");
            session.persist(activityReportCreate);
        }

        Permission activityReportRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ACTIVITY_REPORT_READ.getPermissionAlias())).uniqueResult();

        if (activityReportRead == null) {
            activityReportRead = new Permission();
            activityReportRead.setName("Faaliyet raporu okuyabilir.");
            activityReportRead.setCode(PermissionEnum.ACTIVITY_REPORT_READ.getPermissionAlias());
            activityReportRead.setProjectCode("SOLE");
            session.persist(activityReportRead);
        } else {
            activityReportRead.setName("Faaliyet raporu okuyabilir.");
            activityReportRead.setCode(PermissionEnum.ACTIVITY_REPORT_READ.getPermissionAlias());
            activityReportRead.setProjectCode("SOLE");
            session.persist(activityReportRead);
        }

        Permission activityReportDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ACTIVITY_REPORT_DELETE.getPermissionAlias())).uniqueResult();

        if (activityReportDelete == null) {
            activityReportDelete = new Permission();
            activityReportDelete.setName("Faaliyet raporu silebilir.");
            activityReportDelete.setCode(PermissionEnum.ACTIVITY_REPORT_DELETE.getPermissionAlias());
            activityReportDelete.setProjectCode("SOLE");
            session.persist(activityReportDelete);
        } else {
            activityReportDelete.setName("Faaliyet raporu silebilir.");
            activityReportDelete.setCode(PermissionEnum.ACTIVITY_REPORT_DELETE.getPermissionAlias());
            activityReportDelete.setProjectCode("SOLE");
            session.persist(activityReportDelete);
        }

        Permission activityReportUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.ACTIVITY_REPORT_UPDATE.getPermissionAlias())).uniqueResult();

        if (activityReportUpdate == null) {
            activityReportUpdate = new Permission();
            activityReportUpdate.setName("Faaliyet raporu düzenleyebilir.");
            activityReportUpdate.setCode(PermissionEnum.ACTIVITY_REPORT_UPDATE.getPermissionAlias());
            activityReportUpdate.setProjectCode("SOLE");
            session.persist(activityReportUpdate);
        } else {
            activityReportUpdate.setName("Faaliyet raporu düzenleyebilir.");
            activityReportUpdate.setCode(PermissionEnum.ACTIVITY_REPORT_UPDATE.getPermissionAlias());
            activityReportUpdate.setProjectCode("SOLE");
            session.persist(activityReportUpdate);
        }

        Permission companySelfUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.COMPANY_SELF_UPDATE.getPermissionAlias())).uniqueResult();

        if (companySelfUpdate == null) {
            companySelfUpdate = new Permission();
            companySelfUpdate.setName("Şirket kendi bilgilerini güncelleyebilir.");
            companySelfUpdate.setCode(PermissionEnum.COMPANY_SELF_UPDATE.getPermissionAlias());
            companySelfUpdate.setProjectCode("SOLE");
            session.persist(companySelfUpdate);
        } else {
            companySelfUpdate.setName("Şirket kendi bilgilerini güncelleyebilir.");
            companySelfUpdate.setCode(PermissionEnum.COMPANY_SELF_UPDATE.getPermissionAlias());
            companySelfUpdate.setProjectCode("SOLE");
            session.persist(companySelfUpdate);
        }
        Permission generalProductReportRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.REPORT_READ.getPermissionAlias())).uniqueResult();

        if (generalProductReportRead == null) {
            generalProductReportRead = new Permission();
            generalProductReportRead.setName("Raporları görüntüyebilir.");
            generalProductReportRead.setCode(PermissionEnum.REPORT_READ.getPermissionAlias());
            generalProductReportRead.setProjectCode("SOLE");
            session.persist(generalProductReportRead);
        } else {
            generalProductReportRead.setName("Raporları görüntüyebilir.");
            generalProductReportRead.setCode(PermissionEnum.REPORT_READ.getPermissionAlias());
            generalProductReportRead.setProjectCode("SOLE");
            session.persist(generalProductReportRead);
        }

        Permission fairModelAssortCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_ASSORT_CREATE.getPermissionAlias())).uniqueResult();

        if (fairModelAssortCreate == null) {
            fairModelAssortCreate = new Permission();
            fairModelAssortCreate.setName("Fuar modeline asorti ekleyebilir.");
            fairModelAssortCreate.setCode(PermissionEnum.FAIR_MODEL_ASSORT_CREATE.getPermissionAlias());
            fairModelAssortCreate.setProjectCode("SOLE");
            session.persist(fairModelAssortCreate);
        } else {
            fairModelAssortCreate.setName("Fuar modeline asorti ekleyebilir.");
            fairModelAssortCreate.setCode(PermissionEnum.FAIR_MODEL_ASSORT_CREATE.getPermissionAlias());
            fairModelAssortCreate.setProjectCode("SOLE");
            session.persist(fairModelAssortCreate);
        }

        Permission fairModelAssortRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_ASSORT_READ.getPermissionAlias())).uniqueResult();

        if (fairModelAssortRead == null) {
            fairModelAssortRead = new Permission();
            fairModelAssortRead.setName("Fuar modeline asorti görüntüleyebilir.");
            fairModelAssortRead.setCode(PermissionEnum.FAIR_MODEL_ASSORT_READ.getPermissionAlias());
            fairModelAssortRead.setProjectCode("SOLE");
            session.persist(fairModelAssortRead);
        } else {
            fairModelAssortRead.setName("Fuar modeline asorti görüntüleyebilir.");
            fairModelAssortRead.setCode(PermissionEnum.FAIR_MODEL_ASSORT_READ.getPermissionAlias());
            fairModelAssortRead.setProjectCode("SOLE");
            session.persist(fairModelAssortRead);
        }

        Permission fairModelAssortUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_ASSORT_UPDATE.getPermissionAlias())).uniqueResult();

        if (fairModelAssortUpdate == null) {
            fairModelAssortUpdate = new Permission();
            fairModelAssortUpdate.setName("Fuar modeline asorti güncelleyebilir.");
            fairModelAssortUpdate.setCode(PermissionEnum.FAIR_MODEL_ASSORT_UPDATE.getPermissionAlias());
            fairModelAssortUpdate.setProjectCode("SOLE");
            session.persist(fairModelAssortUpdate);
        } else {
            fairModelAssortUpdate.setName("Fuar modeline asorti güncelleyebilir.");
            fairModelAssortUpdate.setCode(PermissionEnum.FAIR_MODEL_ASSORT_UPDATE.getPermissionAlias());
            fairModelAssortUpdate.setProjectCode("SOLE");
            session.persist(fairModelAssortUpdate);
        }

        Permission fairModelAssortDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_ASSORT_DELETE.getPermissionAlias())).uniqueResult();

        if (fairModelAssortDelete == null) {
            fairModelAssortDelete = new Permission();
            fairModelAssortDelete.setName("Fuar modeline asorti silebilir.");
            fairModelAssortDelete.setCode(PermissionEnum.FAIR_MODEL_ASSORT_DELETE.getPermissionAlias());
            fairModelAssortDelete.setProjectCode("SOLE");
            session.persist(fairModelAssortDelete);
        } else {
            fairModelAssortDelete.setName("Fuar modeline asorti silebilir.");
            fairModelAssortDelete.setCode(PermissionEnum.FAIR_MODEL_ASSORT_DELETE.getPermissionAlias());
            fairModelAssortDelete.setProjectCode("SOLE");
            session.persist(fairModelAssortDelete);
        }

        Permission fairModelPriceCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_PRICE_CREATE.getPermissionAlias())).uniqueResult();

        if (fairModelPriceCreate == null) {
            fairModelPriceCreate = new Permission();
            fairModelPriceCreate.setName("Fuar modeline fiyat ekleyebilir.");
            fairModelPriceCreate.setCode(PermissionEnum.FAIR_MODEL_PRICE_CREATE.getPermissionAlias());
            fairModelPriceCreate.setProjectCode("SOLE");
            session.persist(fairModelPriceCreate);
        } else {
            fairModelPriceCreate.setName("Fuar modeline fiyat ekleyebilir.");
            fairModelPriceCreate.setCode(PermissionEnum.FAIR_MODEL_PRICE_CREATE.getPermissionAlias());
            fairModelPriceCreate.setProjectCode("SOLE");
            session.persist(fairModelPriceCreate);
        }

        Permission fairModelPriceRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_PRICE_READ.getPermissionAlias())).uniqueResult();

        if (fairModelPriceRead == null) {
            fairModelPriceRead = new Permission();
            fairModelPriceRead.setName("Fuar modeline fiyat görüntüleyebilir.");
            fairModelPriceRead.setCode(PermissionEnum.FAIR_MODEL_PRICE_READ.getPermissionAlias());
            fairModelPriceRead.setProjectCode("SOLE");
            session.persist(fairModelPriceRead);
        } else {
            fairModelPriceRead.setName("Fuar modeline fiyat görüntüleyebilir.");
            fairModelPriceRead.setCode(PermissionEnum.FAIR_MODEL_PRICE_READ.getPermissionAlias());
            fairModelPriceRead.setProjectCode("SOLE");
            session.persist(fairModelPriceRead);
        }

        Permission fairModelPriceUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_PRICE_UPDATE.getPermissionAlias())).uniqueResult();

        if (fairModelPriceUpdate == null) {
            fairModelPriceUpdate = new Permission();
            fairModelPriceUpdate.setName("Fuar modeline fiyat güncelleyebilir.");
            fairModelPriceUpdate.setCode(PermissionEnum.FAIR_MODEL_PRICE_UPDATE.getPermissionAlias());
            fairModelPriceUpdate.setProjectCode("SOLE");
            session.persist(fairModelPriceUpdate);
        } else {
            fairModelPriceUpdate.setName("Fuar modeline fiyat güncelleyebilir.");
            fairModelPriceUpdate.setCode(PermissionEnum.FAIR_MODEL_PRICE_UPDATE.getPermissionAlias());
            fairModelPriceUpdate.setProjectCode("SOLE");
            session.persist(fairModelPriceUpdate);
        }

        Permission fairModelPriceDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_PRICE_DELETE.getPermissionAlias())).uniqueResult();

        if (fairModelPriceDelete == null) {
            fairModelPriceDelete = new Permission();
            fairModelPriceDelete.setName("Fuar modeline fiyat silebilir.");
            fairModelPriceDelete.setCode(PermissionEnum.FAIR_MODEL_PRICE_DELETE.getPermissionAlias());
            fairModelPriceDelete.setProjectCode("SOLE");
            session.persist(fairModelPriceDelete);
        } else {
            fairModelPriceDelete.setName("Fuar modeline fiyat silebilir.");
            fairModelPriceDelete.setCode(PermissionEnum.FAIR_MODEL_PRICE_DELETE.getPermissionAlias());
            fairModelPriceDelete.setProjectCode("SOLE");
            session.persist(fairModelPriceDelete);
        }

        Permission copyAssort = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.COPY_ASSORT.getPermissionAlias())).uniqueResult();

        if (copyAssort == null) {
            copyAssort = new Permission();
            copyAssort.setName("Aynı sistem kodlu modellere, seçilen modelin asortisini kopyalayabilir.");
            copyAssort.setCode(PermissionEnum.COPY_ASSORT.getPermissionAlias());
            copyAssort.setProjectCode("SOLE");
            session.persist(copyAssort);
        } else {
            copyAssort.setName("Aynı sistem kodlu modellere, seçilen modelin asortisini kopyalayabilir.");
            copyAssort.setCode(PermissionEnum.COPY_ASSORT.getPermissionAlias());
            copyAssort.setProjectCode("SOLE");
            session.persist(copyAssort);
        }

        Permission copyAssortToAll = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.COPY_ASSORT_TO_ALL.getPermissionAlias())).uniqueResult();

        if (copyAssortToAll == null) {
            copyAssortToAll = new Permission();
            copyAssortToAll.setName("Seçilen modelin asortilerini tüm modellere uygular.");
            copyAssortToAll.setCode(PermissionEnum.COPY_ASSORT_TO_ALL.getPermissionAlias());
            copyAssortToAll.setProjectCode("SOLE");
            session.persist(copyAssortToAll);
        } else {
            copyAssortToAll.setName("Seçilen modelin asortilerini tüm modellere uygular.");
            copyAssortToAll.setCode(PermissionEnum.COPY_ASSORT_TO_ALL.getPermissionAlias());
            copyAssortToAll.setProjectCode("SOLE");
            session.persist(copyAssortToAll);
        }

        Permission copyPrice = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.COPY_PRICE.getPermissionAlias())).uniqueResult();

        if (copyPrice == null) {
            copyPrice = new Permission();
            copyPrice.setName("Aynı sistem kodlu modellere, seçilen modelin fiyatlarını kopyalayabilir.");
            copyPrice.setCode(PermissionEnum.COPY_PRICE.getPermissionAlias());
            copyPrice.setProjectCode("SOLE");
            session.persist(copyPrice);
        } else {
            copyPrice.setName("Aynı sistem kodlu modellere, seçilen modelin fiyatlarını kopyalayabilir.");
            copyPrice.setCode(PermissionEnum.COPY_PRICE.getPermissionAlias());
            copyPrice.setProjectCode("SOLE");
            session.persist(copyPrice);
        }
        //Department CREATE
        Permission departmentCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.DEPARTMENT_CREATE.getPermissionAlias())).uniqueResult();

        if (departmentCreate == null) {
            departmentCreate = new Permission();
            departmentCreate.setName("Departman oluşturma izni verir.");
            departmentCreate.setCode(PermissionEnum.DEPARTMENT_CREATE.getPermissionAlias());
            departmentCreate.setProjectCode("SOLE");
            session.persist(departmentCreate);
        } else {
            departmentCreate.setName("Departman oluşturma izni verir.");
            departmentCreate.setCode(PermissionEnum.DEPARTMENT_CREATE.getPermissionAlias());
            departmentCreate.setProjectCode("SOLE");
            session.persist(departmentCreate);
        }

        //Department READ
        Permission departmentRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.DEPARTMENT_READ.getPermissionAlias())).uniqueResult();

        if (departmentRead == null) {
            departmentRead = new Permission();
            departmentRead.setName("Departman görüntüleme izni verir.");
            departmentRead.setCode(PermissionEnum.DEPARTMENT_READ.getPermissionAlias());
            departmentRead.setProjectCode("SOLE");
            session.persist(departmentRead);
        } else {
            departmentRead.setName("Departman görüntüleme izni verir.");
            departmentRead.setCode(PermissionEnum.DEPARTMENT_READ.getPermissionAlias());
            departmentRead.setProjectCode("SOLE");
            session.persist(departmentRead);
        }

        //Department UPDATE
        Permission departmentUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.DEPARTMENT_UPDATE.getPermissionAlias())).uniqueResult();

        if (departmentUpdate == null) {
            departmentUpdate = new Permission();
            departmentUpdate.setName("Departman güncelleme izni verir.");
            departmentUpdate.setCode(PermissionEnum.DEPARTMENT_UPDATE.getPermissionAlias());
            departmentUpdate.setProjectCode("SOLE");
            session.persist(departmentUpdate);
        } else {
            departmentUpdate.setName("Departman güncelleme izni verir.");
            departmentUpdate.setCode(PermissionEnum.DEPARTMENT_UPDATE.getPermissionAlias());
            departmentUpdate.setProjectCode("SOLE");
            session.persist(departmentUpdate);
        }

        //Department DELETE
        Permission departmentDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.DEPARTMENT_DELETE.getPermissionAlias())).uniqueResult();

        if (departmentDelete == null) {
            departmentDelete = new Permission();
            departmentDelete.setName("Departman silme izni verir.");
            departmentDelete.setCode(PermissionEnum.DEPARTMENT_DELETE.getPermissionAlias());
            departmentDelete.setProjectCode("SOLE");
            session.persist(departmentDelete);
        } else {
            departmentDelete.setName("Departman silme izni verir.");
            departmentDelete.setCode(PermissionEnum.DEPARTMENT_DELETE.getPermissionAlias());
            departmentDelete.setProjectCode("SOLE");
            session.persist(departmentDelete);
        }

        //Staff CREATE
        Permission staffCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STAFF_CREATE.getPermissionAlias())).uniqueResult();

        if (staffCreate == null) {
            staffCreate = new Permission();
            staffCreate.setName("Çalışan oluşturma izni verir.");
            staffCreate.setCode(PermissionEnum.STAFF_CREATE.getPermissionAlias());
            staffCreate.setProjectCode("SOLE");
            session.persist(staffCreate);
        } else {
            staffCreate.setName("Çalışan oluşturma izni verir.");
            staffCreate.setCode(PermissionEnum.STAFF_CREATE.getPermissionAlias());
            staffCreate.setProjectCode("SOLE");
            session.persist(staffCreate);
        }

        //Staff READ
        Permission staffRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STAFF_READ.getPermissionAlias())).uniqueResult();

        if (staffRead == null) {
            staffRead = new Permission();
            staffRead.setName("Çalışan görüntüleme izni verir.");
            staffRead.setCode(PermissionEnum.STAFF_READ.getPermissionAlias());
            staffRead.setProjectCode("SOLE");
            session.persist(staffRead);
        } else {
            staffRead.setName("Çalışan görüntüleme izni verir.");
            staffRead.setCode(PermissionEnum.STAFF_READ.getPermissionAlias());
            staffRead.setProjectCode("SOLE");
            session.persist(staffRead);
        }

        //Staff UPDATE
        Permission staffUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STAFF_UPDATE.getPermissionAlias())).uniqueResult();

        if (staffUpdate == null) {
            staffUpdate = new Permission();
            staffUpdate.setName("Çalışan güncelleme izni verir.");
            staffUpdate.setCode(PermissionEnum.STAFF_UPDATE.getPermissionAlias());
            staffUpdate.setProjectCode("SOLE");
            session.persist(staffUpdate);
        } else {
            staffUpdate.setName("Çalışan güncelleme izni verir.");
            staffUpdate.setCode(PermissionEnum.STAFF_UPDATE.getPermissionAlias());
            staffUpdate.setProjectCode("SOLE");
            session.persist(staffUpdate);
        }

        //Staff DELETE
        Permission staffDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.STAFF_DELETE.getPermissionAlias())).uniqueResult();

        if (staffDelete == null) {
            staffDelete = new Permission();
            staffDelete.setName("Çalışan silme izni verir.");
            staffDelete.setCode(PermissionEnum.STAFF_DELETE.getPermissionAlias());
            staffDelete.setProjectCode("SOLE");
            session.persist(staffDelete);
        } else {
            staffDelete.setName("Çalışan silme izni verir.");
            staffDelete.setCode(PermissionEnum.STAFF_DELETE.getPermissionAlias());
            staffDelete.setProjectCode("SOLE");
            session.persist(staffDelete);
        }

        //Processing CREATE
        Permission processingCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PROCESSING_CREATE.getPermissionAlias())).uniqueResult();

        if (processingCreate == null) {
            processingCreate = new Permission();
            processingCreate.setName("Proses oluşturma izni verir.");
            processingCreate.setCode(PermissionEnum.PROCESSING_CREATE.getPermissionAlias());
            processingCreate.setProjectCode("SOLE");
            session.persist(processingCreate);
        } else {
            processingCreate.setName("Proses oluşturma izni verir.");
            processingCreate.setCode(PermissionEnum.PROCESSING_CREATE.getPermissionAlias());
            processingCreate.setProjectCode("SOLE");
            session.persist(processingCreate);
        }

        //Processing READ
        Permission processingRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PROCESSING_READ.getPermissionAlias())).uniqueResult();

        if (processingRead == null) {
            processingRead = new Permission();
            processingRead.setName("Proses görüntüleme izni verir.");
            processingRead.setCode(PermissionEnum.PROCESSING_READ.getPermissionAlias());
            processingRead.setProjectCode("SOLE");
            session.persist(processingRead);
        } else {
            processingRead.setName("Proses görüntüleme izni verir.");
            processingRead.setCode(PermissionEnum.PROCESSING_READ.getPermissionAlias());
            processingRead.setProjectCode("SOLE");
            session.persist(processingRead);
        }

        //Processing UPDATE
        Permission processingUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PROCESSING_UPDATE.getPermissionAlias())).uniqueResult();

        if (processingUpdate == null) {
            processingUpdate = new Permission();
            processingUpdate.setName("Proses güncelleme izni verir.");
            processingUpdate.setCode(PermissionEnum.PROCESSING_UPDATE.getPermissionAlias());
            processingUpdate.setProjectCode("SOLE");
            session.persist(processingUpdate);
        } else {
            processingUpdate.setName("Proses güncelleme izni verir.");
            processingUpdate.setCode(PermissionEnum.PROCESSING_UPDATE.getPermissionAlias());
            processingUpdate.setProjectCode("SOLE");
            session.persist(processingUpdate);
        }

        //Processing DELETE
        Permission processingDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.PROCESSING_DELETE.getPermissionAlias())).uniqueResult();

        if (processingDelete == null) {
            processingDelete = new Permission();
            processingDelete.setName("Proses silme izni verir.");
            processingDelete.setCode(PermissionEnum.PROCESSING_DELETE.getPermissionAlias());
            processingDelete.setProjectCode("SOLE");
            session.persist(processingDelete);
        } else {
            processingDelete.setName("Proses silme izni verir.");
            processingDelete.setCode(PermissionEnum.PROCESSING_DELETE.getPermissionAlias());
            processingDelete.setProjectCode("SOLE");
            session.persist(processingDelete);
        }

        //ModelProcessing CREATE
        Permission modelProcessingCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_CREATE.getPermissionAlias())).uniqueResult();

        if (modelProcessingCreate == null) {
            modelProcessingCreate = new Permission();
            modelProcessingCreate.setName("Proses Emri oluşturma izni verir.");
            modelProcessingCreate.setCode(PermissionEnum.MODEL_PROCESSING_CREATE.getPermissionAlias());
            modelProcessingCreate.setProjectCode("SOLE");
            session.persist(modelProcessingCreate);
        } else {
            modelProcessingCreate.setName("Proses Emri oluşturma izni verir.");
            modelProcessingCreate.setCode(PermissionEnum.MODEL_PROCESSING_CREATE.getPermissionAlias());
            modelProcessingCreate.setProjectCode("SOLE");
            session.persist(modelProcessingCreate);
        }

        //ModelProcessing READ
        Permission modelProcessingRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_READ.getPermissionAlias())).uniqueResult();

        if (modelProcessingRead == null) {
            modelProcessingRead = new Permission();
            modelProcessingRead.setName("Proses Emri görüntüleme izni verir.");
            modelProcessingRead.setCode(PermissionEnum.MODEL_PROCESSING_READ.getPermissionAlias());
            modelProcessingRead.setProjectCode("SOLE");
            session.persist(modelProcessingRead);
        } else {
            modelProcessingRead.setName("Proses Emri görüntüleme izni verir.");
            modelProcessingRead.setCode(PermissionEnum.MODEL_PROCESSING_READ.getPermissionAlias());
            modelProcessingRead.setProjectCode("SOLE");
            session.persist(modelProcessingRead);
        }

        //ModelProcessing UPDATE
        Permission modelProcessingUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_UPDATE.getPermissionAlias())).uniqueResult();

        if (modelProcessingUpdate == null) {
            modelProcessingUpdate = new Permission();
            modelProcessingUpdate.setName("Proses Emri güncelleme izni verir.");
            modelProcessingUpdate.setCode(PermissionEnum.MODEL_PROCESSING_UPDATE.getPermissionAlias());
            modelProcessingUpdate.setProjectCode("SOLE");
            session.persist(modelProcessingUpdate);
        } else {
            modelProcessingUpdate.setName("Proses Emri güncelleme izni verir.");
            modelProcessingUpdate.setCode(PermissionEnum.MODEL_PROCESSING_UPDATE.getPermissionAlias());
            modelProcessingUpdate.setProjectCode("SOLE");
            session.persist(modelProcessingUpdate);
        }

        //ModelProcessing DELETE
        Permission modelProcessingDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_DELETE.getPermissionAlias())).uniqueResult();

        if (modelProcessingDelete == null) {
            modelProcessingDelete = new Permission();
            modelProcessingDelete.setName("Proses Emri silme izni verir.");
            modelProcessingDelete.setCode(PermissionEnum.MODEL_PROCESSING_DELETE.getPermissionAlias());
            modelProcessingDelete.setProjectCode("SOLE");
            session.persist(modelProcessingDelete);
        } else {
            modelProcessingDelete.setName("Proses Emri silme izni verir.");
            modelProcessingDelete.setCode(PermissionEnum.MODEL_PROCESSING_DELETE.getPermissionAlias());
            modelProcessingDelete.setProjectCode("SOLE");
            session.persist(modelProcessingDelete);
        }

        //ModelProcessingIncoming CREATE
        Permission modelProcessingIncomingCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_INCOMING_CREATE.getPermissionAlias())).uniqueResult();

        if (modelProcessingIncomingCreate == null) {
            modelProcessingIncomingCreate = new Permission();
            modelProcessingIncomingCreate.setName("Gelen Proses Emri oluşturma izni verir.");
            modelProcessingIncomingCreate.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_CREATE.getPermissionAlias());
            modelProcessingIncomingCreate.setProjectCode("SOLE");
            session.persist(modelProcessingIncomingCreate);
        } else {
            modelProcessingIncomingCreate.setName("Gelen Proses Emri oluşturma izni verir.");
            modelProcessingIncomingCreate.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_CREATE.getPermissionAlias());
            modelProcessingIncomingCreate.setProjectCode("SOLE");
            session.persist(modelProcessingIncomingCreate);
        }

        //ModelProcessingIncoming READ
        Permission modelProcessingIncomingRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_INCOMING_READ.getPermissionAlias())).uniqueResult();

        if (modelProcessingIncomingRead == null) {
            modelProcessingIncomingRead = new Permission();
            modelProcessingIncomingRead.setName("Gelen Proses Emri görüntüleme izni verir.");
            modelProcessingIncomingRead.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_READ.getPermissionAlias());
            modelProcessingIncomingRead.setProjectCode("SOLE");
            session.persist(modelProcessingIncomingRead);
        } else {
            modelProcessingIncomingRead.setName("Gelen Proses Emri görüntüleme izni verir.");
            modelProcessingIncomingRead.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_READ.getPermissionAlias());
            modelProcessingIncomingRead.setProjectCode("SOLE");
            session.persist(modelProcessingIncomingRead);
        }

        //ModelProcessingIncoming UPDATE
        Permission modelProcessingIncomingUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_INCOMING_UPDATE.getPermissionAlias())).uniqueResult();

        if (modelProcessingIncomingUpdate == null) {
            modelProcessingIncomingUpdate = new Permission();
            modelProcessingIncomingUpdate.setName("Gelen Proses Emri güncelleme izni verir.");
            modelProcessingIncomingUpdate.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_UPDATE.getPermissionAlias());
            modelProcessingIncomingUpdate.setProjectCode("SOLE");
            session.persist(modelProcessingIncomingUpdate);
        } else {
            modelProcessingIncomingUpdate.setName("Gelen Proses Emri güncelleme izni verir.");
            modelProcessingIncomingUpdate.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_UPDATE.getPermissionAlias());
            modelProcessingIncomingUpdate.setProjectCode("SOLE");
            session.persist(modelProcessingIncomingUpdate);
        }

        //ModelProcessingIncoming DELETE
        Permission modelProcessingIncomingDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_INCOMING_DELETE.getPermissionAlias())).uniqueResult();

        if (modelProcessingIncomingDelete == null) {
            modelProcessingIncomingDelete = new Permission();
            modelProcessingIncomingDelete.setName("Gelen Proses Emri silme izni verir.");
            modelProcessingIncomingDelete.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_DELETE.getPermissionAlias());
            modelProcessingIncomingDelete.setProjectCode("SOLE");
            session.persist(modelProcessingIncomingDelete);
        } else {
            modelProcessingIncomingDelete.setName("Gelen Proses Emri silme izni verir.");
            modelProcessingIncomingDelete.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_DELETE.getPermissionAlias());
            modelProcessingIncomingDelete.setProjectCode("SOLE");
            session.persist(modelProcessingIncomingDelete);
        }

        Permission fairOrderCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_ORDER_CREATE.getPermissionAlias())).uniqueResult();

        if (fairOrderCreate == null) {
            fairOrderCreate = new Permission();
            fairOrderCreate.setName("Fuar siparişi oluşturabilir.");
            fairOrderCreate.setCode(PermissionEnum.FAIR_ORDER_CREATE.getPermissionAlias());
            fairOrderCreate.setProjectCode("SOLE");
            session.persist(fairOrderCreate);
        } else {
            fairOrderCreate.setName("Fuar siparişi oluşturabilir.");
            fairOrderCreate.setCode(PermissionEnum.FAIR_ORDER_CREATE.getPermissionAlias());
            fairOrderCreate.setProjectCode("SOLE");
            session.persist(fairOrderCreate);
        }

        Permission fairOrderRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_ORDER_READ.getPermissionAlias())).uniqueResult();

        if (fairOrderRead == null) {
            fairOrderRead = new Permission();
            fairOrderRead.setName("Fuar siparişi görüntüleyebilir.");
            fairOrderRead.setCode(PermissionEnum.FAIR_ORDER_READ.getPermissionAlias());
            fairOrderRead.setProjectCode("SOLE");
            session.persist(fairOrderRead);
        } else {
            fairOrderRead.setName("Fuar siparişi görüntüleyebilir.");
            fairOrderRead.setCode(PermissionEnum.FAIR_ORDER_READ.getPermissionAlias());
            fairOrderRead.setProjectCode("SOLE");
            session.persist(fairOrderRead);
        }

        Permission fairOrderUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_ORDER_UPDATE.getPermissionAlias())).uniqueResult();

        if (fairOrderUpdate == null) {
            fairOrderUpdate = new Permission();
            fairOrderUpdate.setName("Fuar siparişi güncelleyebilir.");
            fairOrderUpdate.setCode(PermissionEnum.FAIR_ORDER_UPDATE.getPermissionAlias());
            fairOrderUpdate.setProjectCode("SOLE");
            session.persist(fairOrderUpdate);
        } else {
            fairOrderUpdate.setName("Fuar siparişi güncelleyebilir.");
            fairOrderUpdate.setCode(PermissionEnum.FAIR_ORDER_UPDATE.getPermissionAlias());
            fairOrderUpdate.setProjectCode("SOLE");
            session.persist(fairOrderUpdate);
        }

        Permission fairOrderDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "SOLE")).add(Restrictions.eq("code", PermissionEnum.FAIR_ORDER_DELETE.getPermissionAlias())).uniqueResult();

        if (fairOrderDelete == null) {
            fairOrderDelete = new Permission();
            fairOrderDelete.setName("Fuar siparişi silebilir.");
            fairOrderDelete.setCode(PermissionEnum.FAIR_ORDER_DELETE.getPermissionAlias());
            fairOrderDelete.setProjectCode("SOLE");
            session.persist(fairOrderDelete);
        } else {
            fairOrderDelete.setName("Fuar siparişi silebilir.");
            fairOrderDelete.setCode(PermissionEnum.FAIR_ORDER_DELETE.getPermissionAlias());
            fairOrderDelete.setProjectCode("SOLE");
            session.persist(fairOrderDelete);
        }

        session.flush();
    }
}
