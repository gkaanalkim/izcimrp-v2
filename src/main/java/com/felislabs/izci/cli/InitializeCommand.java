package com.felislabs.izci.cli;

import com.felislabs.izci.MrpConfiguration;
import com.felislabs.izci.enums.MenuEnum;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.cli.EnvironmentCommand;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.setup.Environment;
import net.sourceforge.argparse4j.inf.Namespace;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
public class InitializeCommand<T extends MrpConfiguration> extends EnvironmentCommand<T> {
    private static final Logger LOGGER = LoggerFactory.getLogger(InitializeCommand.class);
    private HibernateBundle hibernateBundle;

    public InitializeCommand(Application application, HibernateBundle hibernateBundle) {
        super(application, "initialize", "Runs initial command for initial process.");
        this.hibernateBundle = hibernateBundle;
    }

    @Override
    protected void run(Environment environment, Namespace namespace, T t) throws Exception {
        LOGGER.info("Initialize Starting...");
        LOGGER.info("Starting to create initial data.");
        execute(t);
    }

    @UnitOfWork
    public void execute(Configuration configuration) {
        final Session session = hibernateBundle.getSessionFactory().openSession();

        this.createPermission(session);
        this.createMenu(session);
        this.createTestCompanyAndItsDependencies(session);
        this.createFelisLabs(session);
        session.close();
    }

    public void createPermission(Session session) {
        // ROLE CREATE
        Permission roleCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ROLE_CREATE.getPermissionAlias())).uniqueResult();

        if (roleCreatePermission == null) {
            roleCreatePermission = new Permission();
            roleCreatePermission.setName("Sistem rolü yaratır.");
            roleCreatePermission.setCode(PermissionEnum.ROLE_CREATE.getPermissionAlias());
            roleCreatePermission.setProjectCode("MRP");
            session.persist(roleCreatePermission);
        } else {
            roleCreatePermission.setName("Sistem rolü yaratır.");
            roleCreatePermission.setCode(PermissionEnum.ROLE_CREATE.getPermissionAlias());
            roleCreatePermission.setProjectCode("MRP");
            session.persist(roleCreatePermission);
        }
        //ROLE READ
        Permission roleReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ROLE_READ.getPermissionAlias())).uniqueResult();

        if (roleReadPermission == null) {
            roleReadPermission = new Permission();
            roleReadPermission.setName("Sistem rolü görüntüler.");
            roleReadPermission.setCode(PermissionEnum.ROLE_READ.getPermissionAlias());
            roleReadPermission.setProjectCode("MRP");
            session.persist(roleReadPermission);
        } else {
            roleReadPermission.setName("Sistem rolü görüntüler.");
            roleReadPermission.setCode(PermissionEnum.ROLE_READ.getPermissionAlias());
            roleReadPermission.setProjectCode("MRP");
            session.persist(roleReadPermission);
        }
        //ROLE UPDATE
        Permission roleUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ROLE_UPDATE.getPermissionAlias())).uniqueResult();

        if (roleUpdatePermission == null) {
            roleUpdatePermission = new Permission();
            roleUpdatePermission.setName("Sistem rolü günceller.");
            roleUpdatePermission.setCode(PermissionEnum.ROLE_UPDATE.getPermissionAlias());
            roleUpdatePermission.setProjectCode("MRP");
            session.persist(roleUpdatePermission);
        } else {
            roleUpdatePermission.setName("Sistem rolü günceller.");
            roleUpdatePermission.setCode(PermissionEnum.ROLE_UPDATE.getPermissionAlias());
            roleUpdatePermission.setProjectCode("MRP");
            session.persist(roleUpdatePermission);
        }
        //ROLE UPDATE
        Permission roleDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ROLE_DELETE.getPermissionAlias())).uniqueResult();

        if (roleDeletePermission == null) {
            roleDeletePermission = new Permission();
            roleDeletePermission.setName("Sistem rolü siler.");
            roleDeletePermission.setCode(PermissionEnum.ROLE_DELETE.getPermissionAlias());
            roleDeletePermission.setProjectCode("MRP");
            session.persist(roleDeletePermission);
        } else {
            roleDeletePermission.setName("Sistem rolü siler.");
            roleDeletePermission.setCode(PermissionEnum.ROLE_DELETE.getPermissionAlias());
            roleDeletePermission.setProjectCode("MRP");
            session.persist(roleDeletePermission);
        }
        //ROLE UPDATE
        Permission roleAssingPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ROLE_ASSING.getPermissionAlias())).uniqueResult();

        if (roleAssingPermission == null) {
            roleAssingPermission = new Permission();
            roleAssingPermission.setName("Kullanıcıya rol atar.");
            roleAssingPermission.setCode(PermissionEnum.ROLE_ASSING.getPermissionAlias());
            roleAssingPermission.setProjectCode("MRP");
            session.persist(roleAssingPermission);
        } else {
            roleAssingPermission.setName("Kullanıcıya rol atar.");
            roleAssingPermission.setCode(PermissionEnum.ROLE_ASSING.getPermissionAlias());
            roleAssingPermission.setProjectCode("MRP");
            session.persist(roleAssingPermission);
        }

        //CUSTOMER CREATE
        Permission customerCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CUSTOMER_CREATE.getPermissionAlias())).uniqueResult();

        if (customerCreatePermission == null) {
            customerCreatePermission = new Permission();
            customerCreatePermission.setName("Müşteri ekler.");
            customerCreatePermission.setCode(PermissionEnum.CUSTOMER_CREATE.getPermissionAlias());
            customerCreatePermission.setProjectCode("MRP");
            session.persist(customerCreatePermission);
        } else {
            customerCreatePermission.setName("Müşteri ekler.");
            customerCreatePermission.setCode(PermissionEnum.CUSTOMER_CREATE.getPermissionAlias());
            customerCreatePermission.setProjectCode("MRP");
            session.persist(customerCreatePermission);
        }
        //CUSTOMER READ
        Permission customerReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CUSTOMER_READ.getPermissionAlias())).uniqueResult();

        if (customerReadPermission == null) {
            customerReadPermission = new Permission();
            customerReadPermission.setName("Müşteri görüntüler.");
            customerReadPermission.setCode(PermissionEnum.CUSTOMER_READ.getPermissionAlias());
            customerReadPermission.setProjectCode("MRP");
            session.persist(customerReadPermission);
        } else {
            customerReadPermission.setName("Müşteri görüntüler.");
            customerReadPermission.setCode(PermissionEnum.CUSTOMER_READ.getPermissionAlias());
            customerReadPermission.setProjectCode("MRP");
            session.persist(customerReadPermission);
        }
        //CUSTOMER UPDATE
        Permission customerUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CUSTOMER_UPDATE.getPermissionAlias())).uniqueResult();

        if (customerUpdatePermission == null) {
            customerUpdatePermission = new Permission();
            customerUpdatePermission.setName("Müşteri günceller.");
            customerUpdatePermission.setCode(PermissionEnum.CUSTOMER_UPDATE.getPermissionAlias());
            customerUpdatePermission.setProjectCode("MRP");
            session.persist(customerUpdatePermission);
        } else {
            customerUpdatePermission.setName("Müşteri günceller.");
            customerUpdatePermission.setCode(PermissionEnum.CUSTOMER_UPDATE.getPermissionAlias());
            customerUpdatePermission.setProjectCode("MRP");
            session.persist(customerUpdatePermission);
        }
        //CUSTOMER DELETE
        Permission customerDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CUSTOMER_DELETE.getPermissionAlias())).uniqueResult();

        if (customerDeletePermission == null) {
            customerDeletePermission = new Permission();
            customerDeletePermission.setName("Müşteri siler.");
            customerDeletePermission.setCode(PermissionEnum.CUSTOMER_DELETE.getPermissionAlias());
            customerDeletePermission.setProjectCode("MRP");
            session.persist(customerDeletePermission);
        } else {
            customerDeletePermission.setName("Müşteri siler.");
            customerDeletePermission.setCode(PermissionEnum.CUSTOMER_DELETE.getPermissionAlias());
            customerDeletePermission.setProjectCode("MRP");
            session.persist(customerDeletePermission);
        }

        //STOCK CREATE
        Permission stockCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_CREATE.getPermissionAlias())).uniqueResult();

        if (stockCreatePermission == null) {
            stockCreatePermission = new Permission();
            stockCreatePermission.setName("Stok ekler.");
            stockCreatePermission.setCode(PermissionEnum.STOCK_CREATE.getPermissionAlias());
            stockCreatePermission.setProjectCode("MRP");
            session.persist(stockCreatePermission);
        } else {
            stockCreatePermission.setName("Stok ekler.");
            stockCreatePermission.setCode(PermissionEnum.STOCK_CREATE.getPermissionAlias());
            stockCreatePermission.setProjectCode("MRP");
            session.persist(stockCreatePermission);
        }
        //STOCK READ
        Permission stockReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_READ.getPermissionAlias())).uniqueResult();

        if (stockReadPermission == null) {
            stockReadPermission = new Permission();
            stockReadPermission.setName("Stok görüntüler.");
            stockReadPermission.setCode(PermissionEnum.STOCK_READ.getPermissionAlias());
            stockReadPermission.setProjectCode("MRP");
            session.persist(stockReadPermission);
        } else {
            stockReadPermission.setName("Stok görüntüler.");
            stockReadPermission.setCode(PermissionEnum.STOCK_READ.getPermissionAlias());
            stockReadPermission.setProjectCode("MRP");
            session.persist(stockReadPermission);
        }
        //STOCK UPDATE
        Permission stockUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_UPDATE.getPermissionAlias())).uniqueResult();

        if (stockUpdatePermission == null) {
            stockUpdatePermission = new Permission();
            stockUpdatePermission.setName("Stok günceller.");
            stockUpdatePermission.setCode(PermissionEnum.STOCK_UPDATE.getPermissionAlias());
            stockUpdatePermission.setProjectCode("MRP");
            session.persist(stockUpdatePermission);
        } else {
            stockUpdatePermission.setName("Stok günceller.");
            stockUpdatePermission.setCode(PermissionEnum.STOCK_UPDATE.getPermissionAlias());
            stockUpdatePermission.setProjectCode("MRP");
            session.persist(stockUpdatePermission);
        }
        //STOCK DELETE
        Permission stockDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_DELETE.getPermissionAlias())).uniqueResult();

        if (stockDeletePermission == null) {
            stockDeletePermission = new Permission();
            stockDeletePermission.setName("Stok siler.");
            stockDeletePermission.setCode(PermissionEnum.STOCK_DELETE.getPermissionAlias());
            stockDeletePermission.setProjectCode("MRP");
            session.persist(stockDeletePermission);
        } else {
            stockDeletePermission.setName("Stok siler.");
            stockDeletePermission.setCode(PermissionEnum.STOCK_DELETE.getPermissionAlias());
            stockDeletePermission.setProjectCode("MRP");
            session.persist(stockDeletePermission);
        }

        //STOCK CREATE
        Permission stockAcceptanceCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_ACCEPTANCE_CREATE.getPermissionAlias())).uniqueResult();

        if (stockAcceptanceCreatePermission == null) {
            stockAcceptanceCreatePermission = new Permission();
            stockAcceptanceCreatePermission.setName("Stok Kabul ekler.");
            stockAcceptanceCreatePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_CREATE.getPermissionAlias());
            stockAcceptanceCreatePermission.setProjectCode("MRP");
            session.persist(stockAcceptanceCreatePermission);
        } else {
            stockAcceptanceCreatePermission.setName("Stok Kabul ekler.");
            stockAcceptanceCreatePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_CREATE.getPermissionAlias());
            stockAcceptanceCreatePermission.setProjectCode("MRP");
            session.persist(stockAcceptanceCreatePermission);
        }
        //STOCK READ
        Permission stockAcceptanceReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_ACCEPTANCE_READ.getPermissionAlias())).uniqueResult();

        if (stockAcceptanceReadPermission == null) {
            stockAcceptanceReadPermission = new Permission();
            stockAcceptanceReadPermission.setName("Stok Kabul görüntüler.");
            stockAcceptanceReadPermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_READ.getPermissionAlias());
            stockAcceptanceReadPermission.setProjectCode("MRP");
            session.persist(stockAcceptanceReadPermission);
        } else {
            stockAcceptanceReadPermission.setName("Stok Kabul görüntüler.");
            stockAcceptanceReadPermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_READ.getPermissionAlias());
            stockAcceptanceReadPermission.setProjectCode("MRP");
            session.persist(stockAcceptanceReadPermission);
        }
        //STOCK UPDATE
        Permission stockAcceptanceUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_ACCEPTANCE_UPDATE.getPermissionAlias())).uniqueResult();

        if (stockAcceptanceUpdatePermission == null) {
            stockAcceptanceUpdatePermission = new Permission();
            stockAcceptanceUpdatePermission.setName("Stok Kabul günceller.");
            stockAcceptanceUpdatePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_UPDATE.getPermissionAlias());
            stockAcceptanceUpdatePermission.setProjectCode("MRP");
            session.persist(stockAcceptanceUpdatePermission);
        } else {
            stockAcceptanceUpdatePermission.setName("Stok Kabul günceller.");
            stockAcceptanceUpdatePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_UPDATE.getPermissionAlias());
            stockAcceptanceUpdatePermission.setProjectCode("MRP");
            session.persist(stockAcceptanceUpdatePermission);
        }
        //STOCK DELETE
        Permission stockAcceptanceDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_ACCEPTANCE_DELETE.getPermissionAlias())).uniqueResult();

        if (stockAcceptanceDeletePermission == null) {
            stockAcceptanceDeletePermission = new Permission();
            stockAcceptanceDeletePermission.setName("Stok Kabul siler.");
            stockAcceptanceDeletePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_DELETE.getPermissionAlias());
            stockAcceptanceDeletePermission.setProjectCode("MRP");
            session.persist(stockAcceptanceDeletePermission);
        } else {
            stockAcceptanceDeletePermission.setName("Stok Kabul siler.");
            stockAcceptanceDeletePermission.setCode(PermissionEnum.STOCK_ACCEPTANCE_DELETE.getPermissionAlias());
            stockAcceptanceDeletePermission.setProjectCode("MRP");
            session.persist(stockAcceptanceDeletePermission);
        }

        //STOCK INCREASE
        Permission stockIncrease = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_INCREASE.getPermissionAlias())).uniqueResult();

        if (stockIncrease == null) {
            stockIncrease = new Permission();
            stockIncrease.setName("Stok miktarını arttırabilir.");
            stockIncrease.setCode(PermissionEnum.STOCK_INCREASE.getPermissionAlias());
            stockIncrease.setProjectCode("MRP");
            session.persist(stockIncrease);
        } else {
            stockIncrease.setName("Stok miktarını arttırabilir.");
            stockIncrease.setCode(PermissionEnum.STOCK_INCREASE.getPermissionAlias());
            stockIncrease.setProjectCode("MRP");
            session.persist(stockIncrease);
        }

        //STOCK DECREASE
        Permission stockDecrease = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_DECREASE.getPermissionAlias())).uniqueResult();

        if (stockDecrease == null) {
            stockDecrease = new Permission();
            stockDecrease.setName("Stok miktarını azaltabilir.");
            stockDecrease.setCode(PermissionEnum.STOCK_DECREASE.getPermissionAlias());
            stockDecrease.setProjectCode("MRP");
            session.persist(stockDecrease);
        } else {
            stockDecrease.setName("Stok miktarını azaltabilir.");
            stockDecrease.setCode(PermissionEnum.STOCK_DECREASE.getPermissionAlias());
            stockDecrease.setProjectCode("MRP");
            session.persist(stockDecrease);
        }


        //USER CREATE
        Permission userCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.USER_CREATE.getPermissionAlias())).uniqueResult();

        if (userCreatePermission == null) {
            userCreatePermission = new Permission();
            userCreatePermission.setName("Kullanıcı ekler.");
            userCreatePermission.setCode(PermissionEnum.USER_CREATE.getPermissionAlias());
            userCreatePermission.setProjectCode("MRP");
            session.persist(userCreatePermission);
        } else {
            userCreatePermission.setName("Kullanıcı ekler.");
            userCreatePermission.setCode(PermissionEnum.USER_CREATE.getPermissionAlias());
            userCreatePermission.setProjectCode("MRP");
            session.persist(userCreatePermission);
        }
        //USER READ
        Permission userReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.USER_READ.getPermissionAlias())).uniqueResult();

        if (userReadPermission == null) {
            userReadPermission = new Permission();
            userReadPermission.setName("Kullanıcı görüntüler.");
            userReadPermission.setCode(PermissionEnum.USER_READ.getPermissionAlias());
            userReadPermission.setProjectCode("MRP");
            session.persist(userReadPermission);
        } else {
            userReadPermission.setName("Kullanıcı görüntüler.");
            userReadPermission.setCode(PermissionEnum.USER_READ.getPermissionAlias());
            userReadPermission.setProjectCode("MRP");
            session.persist(userReadPermission);
        }
        //USER UPDATE
        Permission userUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.USER_UPDATE.getPermissionAlias())).uniqueResult();

        if (userUpdatePermission == null) {
            userUpdatePermission = new Permission();
            userUpdatePermission.setName("Kullanıcı günceller.");
            userUpdatePermission.setCode(PermissionEnum.USER_UPDATE.getPermissionAlias());
            userUpdatePermission.setProjectCode("MRP");
            session.persist(userUpdatePermission);
        } else {
            userUpdatePermission.setName("Kullanıcı günceller.");
            userUpdatePermission.setCode(PermissionEnum.USER_UPDATE.getPermissionAlias());
            userUpdatePermission.setProjectCode("MRP");
            session.persist(userUpdatePermission);
        }
        //USER DELETE
        Permission userDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.USER_DELETE.getPermissionAlias())).uniqueResult();

        if (userDeletePermission == null) {
            userDeletePermission = new Permission();
            userDeletePermission.setName("Kullanıcı siler.");
            userDeletePermission.setCode(PermissionEnum.USER_DELETE.getPermissionAlias());
            userDeletePermission.setProjectCode("MRP");
            session.persist(userDeletePermission);
        } else {
            userDeletePermission.setName("Kullanıcı siler.");
            userDeletePermission.setCode(PermissionEnum.USER_DELETE.getPermissionAlias());
            userDeletePermission.setProjectCode("MRP");
            session.persist(userDeletePermission);
        }

        //MENU READ
        Permission menuReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MENU_READ.getPermissionAlias())).uniqueResult();

        if (menuReadPermission == null) {
            menuReadPermission = new Permission();
            menuReadPermission.setName("Menü seçeneklerini görüntüler.");
            menuReadPermission.setCode(PermissionEnum.MENU_READ.getPermissionAlias());
            menuReadPermission.setProjectCode("MRP");
            session.persist(menuReadPermission);
        } else {
            menuReadPermission.setName("Menü seçeneklerini görüntüler.");
            menuReadPermission.setCode(PermissionEnum.MENU_READ.getPermissionAlias());
            menuReadPermission.setProjectCode("MRP");
            session.persist(menuReadPermission);
        }
        //MENU ASSIGN
        Permission menuAssingPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MENU_ASSING.getPermissionAlias())).uniqueResult();

        if (menuAssingPermission == null) {
            menuAssingPermission = new Permission();
            menuAssingPermission.setName("Menüye erişim izni verir.");
            menuAssingPermission.setCode(PermissionEnum.MENU_ASSING.getPermissionAlias());
            menuAssingPermission.setProjectCode("MRP");
            session.persist(menuAssingPermission);
        } else {
            menuAssingPermission.setName("Menüye erişim izni verir.");
            menuAssingPermission.setCode(PermissionEnum.MENU_ASSING.getPermissionAlias());
            menuAssingPermission.setProjectCode("MRP");
            session.persist(menuAssingPermission);
        }
        //MENU ASSIGNBACK
        Permission menuAssingBackPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MENU_ASSING_BACK.getPermissionAlias())).uniqueResult();

        if (menuAssingBackPermission == null) {
            menuAssingBackPermission = new Permission();
            menuAssingBackPermission.setName("Menüye erişim izni kaldırır.");
            menuAssingBackPermission.setCode(PermissionEnum.MENU_ASSING_BACK.getPermissionAlias());
            menuAssingBackPermission.setProjectCode("MRP");
            session.persist(menuAssingBackPermission);
        } else {
            menuAssingBackPermission.setName("Menüye erişim izni kaldırır.");
            menuAssingBackPermission.setCode(PermissionEnum.MENU_ASSING_BACK.getPermissionAlias());
            menuAssingBackPermission.setProjectCode("MRP");
            session.persist(menuAssingBackPermission);
        }
        //MENU CREATE
        Permission menuCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MENU_CREATE.getPermissionAlias())).uniqueResult();

        if (menuCreatePermission == null) {
            menuCreatePermission = new Permission();
            menuCreatePermission.setName("Menü ekleme izni verir.");
            menuCreatePermission.setCode(PermissionEnum.MENU_CREATE.getPermissionAlias());
            menuCreatePermission.setProjectCode("MRP");
            session.persist(menuCreatePermission);
        } else {
            menuCreatePermission.setName("Menü ekleme izni verir.");
            menuCreatePermission.setCode(PermissionEnum.MENU_CREATE.getPermissionAlias());
            menuCreatePermission.setProjectCode("MRP");
            session.persist(menuCreatePermission);
        }
        //MENU UPDATE
        Permission menuUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MENU_UPDATE.getPermissionAlias())).uniqueResult();

        if (menuUpdatePermission == null) {
            menuUpdatePermission = new Permission();
            menuUpdatePermission.setName("Menü güncelleme izni verir.");
            menuUpdatePermission.setCode(PermissionEnum.MENU_UPDATE.getPermissionAlias());
            menuUpdatePermission.setProjectCode("MRP");
            session.persist(menuUpdatePermission);
        } else {
            menuUpdatePermission.setName("Menü güncelleme izni verir.");
            menuUpdatePermission.setCode(PermissionEnum.MENU_UPDATE.getPermissionAlias());
            menuUpdatePermission.setProjectCode("MRP");
            session.persist(menuUpdatePermission);
        }
        //MENU DELETE
        Permission menuDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MENU_DELETE.getPermissionAlias())).uniqueResult();

        if (menuDeletePermission == null) {
            menuDeletePermission = new Permission();
            menuDeletePermission.setName("Menü silme izni verir.");
            menuDeletePermission.setCode(PermissionEnum.MENU_DELETE.getPermissionAlias());
            menuDeletePermission.setProjectCode("MRP");
            session.persist(menuDeletePermission);
        } else {
            menuDeletePermission.setName("Menü silme izni verir.");
            menuDeletePermission.setCode(PermissionEnum.MENU_DELETE.getPermissionAlias());
            menuDeletePermission.setProjectCode("MRP");
            session.persist(menuDeletePermission);
        }
        //PROFIL READ
        Permission profileReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PROFILE_READ.getPermissionAlias())).uniqueResult();

        if (profileReadPermission == null) {
            profileReadPermission = new Permission();
            profileReadPermission.setName("Profil görüntüleme iznini verir.");
            profileReadPermission.setCode(PermissionEnum.PROFILE_READ.getPermissionAlias());
            profileReadPermission.setProjectCode("MRP");
            session.persist(profileReadPermission);
        } else {
            profileReadPermission.setName("Profil görüntüleme iznini verir.");
            profileReadPermission.setCode(PermissionEnum.PROFILE_READ.getPermissionAlias());
            profileReadPermission.setProjectCode("MRP");
            session.persist(profileReadPermission);
        }
        //PROFIL Update
        Permission profileUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PROFILE_UPDATE.getPermissionAlias())).uniqueResult();

        if (profileUpdatePermission == null) {
            profileUpdatePermission = new Permission();
            profileUpdatePermission.setName("Profil güncelleme iznini verir.");
            profileUpdatePermission.setCode(PermissionEnum.PROFILE_UPDATE.getPermissionAlias());
            profileUpdatePermission.setProjectCode("MRP");
            session.persist(profileUpdatePermission);
        } else {
            profileUpdatePermission.setName("Profil güncelleme iznini verir.");
            profileUpdatePermission.setCode(PermissionEnum.PROFILE_UPDATE.getPermissionAlias());
            profileUpdatePermission.setProjectCode("MRP");
            session.persist(profileUpdatePermission);
        }
        //SETTING READ
        Permission settingReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SETTING_READ.getPermissionAlias())).uniqueResult();

        if (settingReadPermission == null) {
            settingReadPermission = new Permission();
            settingReadPermission.setName("Ayarları okuma iznini verir.");
            settingReadPermission.setCode(PermissionEnum.SETTING_READ.getPermissionAlias());
            settingReadPermission.setProjectCode("MRP");
            session.persist(settingReadPermission);
        } else {
            settingReadPermission.setName("Ayarları okuma iznini verir.");
            settingReadPermission.setCode(PermissionEnum.SETTING_READ.getPermissionAlias());
            settingReadPermission.setProjectCode("MRP");
            session.persist(settingReadPermission);
        }
        //SETTING CREATE
        Permission settingCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SETTING_CREATE.getPermissionAlias())).uniqueResult();

        if (settingCreatePermission == null) {
            settingCreatePermission = new Permission();
            settingCreatePermission.setName("Ayarları yazma iznini verir.");
            settingCreatePermission.setCode(PermissionEnum.SETTING_CREATE.getPermissionAlias());
            settingCreatePermission.setProjectCode("MRP");
            session.persist(settingCreatePermission);
        } else {
            settingCreatePermission.setName("Ayarları yazma iznini verir.");
            settingCreatePermission.setCode(PermissionEnum.SETTING_CREATE.getPermissionAlias());
            settingCreatePermission.setProjectCode("MRP");
            session.persist(settingCreatePermission);
        }
        //SETTING UPDATE
        Permission settingUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SETTING_UPDATE.getPermissionAlias())).uniqueResult();

        if (settingUpdatePermission == null) {
            settingUpdatePermission = new Permission();
            settingUpdatePermission.setName("Ayarları güncelleme iznini verir.");
            settingUpdatePermission.setCode(PermissionEnum.SETTING_UPDATE.getPermissionAlias());
            settingUpdatePermission.setProjectCode("MRP");
            session.persist(settingUpdatePermission);
        } else {
            settingUpdatePermission.setName("Ayarları güncelleme iznini verir.");
            settingUpdatePermission.setCode(PermissionEnum.SETTING_UPDATE.getPermissionAlias());
            settingUpdatePermission.setProjectCode("MRP");
            session.persist(settingUpdatePermission);
        }
        //SETTING DELETE
        Permission settingDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SETTING_DELETE.getPermissionAlias())).uniqueResult();

        if (settingDeletePermission == null) {
            settingDeletePermission = new Permission();
            settingDeletePermission.setName("Ayarları silme iznini verir.");
            settingDeletePermission.setCode(PermissionEnum.SETTING_DELETE.getPermissionAlias());
            settingDeletePermission.setProjectCode("MRP");
            session.persist(settingDeletePermission);
        } else {
            settingDeletePermission.setName("Ayarları silme iznini verir.");
            settingDeletePermission.setCode(PermissionEnum.SETTING_DELETE.getPermissionAlias());
            settingDeletePermission.setProjectCode("MRP");
            session.persist(settingDeletePermission);
        }
        //READ CURRENCY
        Permission currencyReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CURRENCY_READ.getPermissionAlias())).uniqueResult();

        if (currencyReadPermission == null) {
            currencyReadPermission = new Permission();
            currencyReadPermission.setName("Döviz kurlarını görüntüleme iznini verir.");
            currencyReadPermission.setCode(PermissionEnum.CURRENCY_READ.getPermissionAlias());
            currencyReadPermission.setProjectCode("MRP");
            session.persist(currencyReadPermission);
        } else {
            currencyReadPermission.setName("Döviz kurlarını görüntüleme iznini verir.");
            currencyReadPermission.setCode(PermissionEnum.CURRENCY_READ.getPermissionAlias());
            currencyReadPermission.setProjectCode("MRP");
            session.persist(currencyReadPermission);
        }
        //READ COMPANY
        Permission companyReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.COMPANY_READ.getPermissionAlias())).uniqueResult();

        if (companyReadPermission == null) {
            companyReadPermission = new Permission();
            companyReadPermission.setName("Sistemdeki şirketleri görüntüleme izni verir.");
            companyReadPermission.setCode(PermissionEnum.COMPANY_READ.getPermissionAlias());
            companyReadPermission.setProjectCode("MRP");
            session.persist(companyReadPermission);
        } else {
            companyReadPermission.setName("Sistemdeki şirketleri görüntüleme izni verir.");
            companyReadPermission.setCode(PermissionEnum.COMPANY_READ.getPermissionAlias());
            companyReadPermission.setProjectCode("MRP");
            session.persist(companyReadPermission);
        }

        //CREATE COMPANY
        Permission companyCreatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.COMPANY_CREATE.getPermissionAlias())).uniqueResult();

        if (companyCreatePermission == null) {
            companyCreatePermission = new Permission();
            companyCreatePermission.setName("Sisteme şirket ekleme izni verir.");
            companyCreatePermission.setCode(PermissionEnum.COMPANY_CREATE.getPermissionAlias());
            companyCreatePermission.setProjectCode("MRP");
            session.persist(companyCreatePermission);
        } else {
            companyCreatePermission.setName("Sisteme şirket ekleme izni verir.");
            companyCreatePermission.setCode(PermissionEnum.COMPANY_CREATE.getPermissionAlias());
            companyCreatePermission.setProjectCode("MRP");
            session.persist(companyCreatePermission);
        }

        //CREATE UPDATE
        Permission companyUpdatePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.COMPANY_UPDATE.getPermissionAlias())).uniqueResult();

        if (companyUpdatePermission == null) {
            companyUpdatePermission = new Permission();
            companyUpdatePermission.setName("Sistemde şirket güncelleme izni verir.");
            companyUpdatePermission.setCode(PermissionEnum.COMPANY_UPDATE.getPermissionAlias());
            companyUpdatePermission.setProjectCode("MRP");
            session.persist(companyUpdatePermission);
        } else {
            companyUpdatePermission.setName("Sistemde şirket güncelleme izni verir.");
            companyUpdatePermission.setCode(PermissionEnum.COMPANY_UPDATE.getPermissionAlias());
            companyUpdatePermission.setProjectCode("MRP");
            session.persist(companyUpdatePermission);
        }

        //CREATE DELETE
        Permission companyDeletePermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.COMPANY_DELETE.getPermissionAlias())).uniqueResult();

        if (companyDeletePermission == null) {
            companyDeletePermission = new Permission();
            companyDeletePermission.setName("Sistemde şirket silme izni verir.");
            companyDeletePermission.setCode(PermissionEnum.COMPANY_DELETE.getPermissionAlias());
            companyDeletePermission.setProjectCode("MRP");
            session.persist(companyDeletePermission);
        } else {
            companyDeletePermission.setName("Sistemde şirket silme izni verir.");
            companyDeletePermission.setCode(PermissionEnum.COMPANY_DELETE.getPermissionAlias());
            companyDeletePermission.setProjectCode("MRP");
            session.persist(companyDeletePermission);
        }

        //PERMISSION READ
        Permission permissionReadPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PERMISSION_READ.getPermissionAlias())).uniqueResult();

        if (permissionReadPermission == null) {
            permissionReadPermission = new Permission();
            permissionReadPermission.setName("İzin görütüleyebilme izni verir.");
            permissionReadPermission.setCode(PermissionEnum.PERMISSION_READ.getPermissionAlias());
            permissionReadPermission.setProjectCode("MRP");
            session.persist(permissionReadPermission);
        } else {
            permissionReadPermission.setName("İzin görütüleyebilme izni verir.");
            permissionReadPermission.setCode(PermissionEnum.PERMISSION_READ.getPermissionAlias());
            permissionReadPermission.setProjectCode("MRP");
            session.persist(permissionReadPermission);
        }
        //PERMISSION ASSIGN
        Permission permissionAssingPermission = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PERMISSION_ASSING.getPermissionAlias())).uniqueResult();

        if (permissionAssingPermission == null) {
            permissionAssingPermission = new Permission();
            permissionAssingPermission.setName("İzin atayabilme izni verir.");
            permissionAssingPermission.setCode(PermissionEnum.PERMISSION_ASSING.getPermissionAlias());
            permissionAssingPermission.setProjectCode("MRP");
            session.persist(permissionAssingPermission);
        } else {
            permissionAssingPermission.setName("İzin atayabilme izni verir.");
            permissionAssingPermission.setCode(PermissionEnum.PERMISSION_ASSING.getPermissionAlias());
            permissionAssingPermission.setProjectCode("MRP");
            session.persist(permissionAssingPermission);
        }

        //Model CREATE
        Permission modelCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_CREATE.getPermissionAlias())).uniqueResult();

        if (modelCreate == null) {
            modelCreate = new Permission();
            modelCreate.setName("Model oluşturma izni verir.");
            modelCreate.setCode(PermissionEnum.MODEL_CREATE.getPermissionAlias());
            modelCreate.setProjectCode("MRP");
            session.persist(modelCreate);
        } else {
            modelCreate.setName("Model oluşturma izni verir.");
            modelCreate.setCode(PermissionEnum.MODEL_CREATE.getPermissionAlias());
            modelCreate.setProjectCode("MRP");
            session.persist(modelCreate);
        }

        //Model READ
        Permission modelRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_READ.getPermissionAlias())).uniqueResult();

        if (modelRead == null) {
            modelRead = new Permission();
            modelRead.setName("Model görüntüleme izni verir.");
            modelRead.setCode(PermissionEnum.MODEL_READ.getPermissionAlias());
            modelRead.setProjectCode("MRP");
            session.persist(modelRead);
        } else {
            modelRead.setName("Model görüntüleme izni verir.");
            modelRead.setCode(PermissionEnum.MODEL_READ.getPermissionAlias());
            modelRead.setProjectCode("MRP");
            session.persist(modelRead);
        }

        //Model UPDATE
        Permission modelUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_UPDATE.getPermissionAlias())).uniqueResult();

        if (modelUpdate == null) {
            modelUpdate = new Permission();
            modelUpdate.setName("Model güncelleme izni verir.");
            modelUpdate.setCode(PermissionEnum.MODEL_UPDATE.getPermissionAlias());
            modelUpdate.setProjectCode("MRP");
            session.persist(modelUpdate);
        } else {
            modelUpdate.setName("Model güncelleme izni verir.");
            modelUpdate.setCode(PermissionEnum.MODEL_UPDATE.getPermissionAlias());
            modelUpdate.setProjectCode("MRP");
            session.persist(modelUpdate);
        }

        //Model DELETE
        Permission modelDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_DELETE.getPermissionAlias())).uniqueResult();

        if (modelDelete == null) {
            modelDelete = new Permission();
            modelDelete.setName("Model silme izni verir.");
            modelDelete.setCode(PermissionEnum.MODEL_DELETE.getPermissionAlias());
            modelDelete.setProjectCode("MRP");
            session.persist(modelDelete);
        } else {
            modelDelete.setName("Model silme izni verir.");
            modelDelete.setCode(PermissionEnum.MODEL_DELETE.getPermissionAlias());
            modelDelete.setProjectCode("MRP");
            session.persist(modelDelete);
        }

        //WORK ORDER PRICE
        Permission modelPrice = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PRICE.getPermissionAlias())).uniqueResult();

        if (modelPrice == null) {
            modelPrice = new Permission();
            modelPrice.setName("Model fiyatlandırma iznini verir.");
            modelPrice.setCode(PermissionEnum.MODEL_PRICE.getPermissionAlias());
            modelPrice.setProjectCode("MRP");
            session.persist(modelPrice);
        } else {
            modelPrice.setName("Model fiyatlandırma iznini verir.");
            modelPrice.setCode(PermissionEnum.MODEL_PRICE.getPermissionAlias());
            modelPrice.setProjectCode("MRP");
            session.persist(modelPrice);
        }

        //MESSAGE READ
        Permission messageRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MESSAGE_READ.getPermissionAlias())).uniqueResult();

        if (messageRead == null) {
            messageRead = new Permission();
            messageRead.setName("Mesajları okuma iznini verir.");
            messageRead.setCode(PermissionEnum.MESSAGE_READ.getPermissionAlias());
            messageRead.setProjectCode("MRP");
            session.persist(messageRead);
        } else {
            messageRead.setName("Mesajları okuma iznini verir.");
            messageRead.setCode(PermissionEnum.MESSAGE_READ.getPermissionAlias());
            messageRead.setProjectCode("MRP");
            session.persist(messageRead);
        }
        //MESSAGE CREATE
        Permission messageCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MESSAGE_CREATE.getPermissionAlias())).uniqueResult();

        if (messageCreate == null) {
            messageCreate = new Permission();
            messageCreate.setName("Mesaj yazma iznini verir.");
            messageCreate.setCode(PermissionEnum.MESSAGE_CREATE.getPermissionAlias());
            messageCreate.setProjectCode("MRP");
            session.persist(messageCreate);
        } else {
            messageCreate.setName("Mesaj yazma iznini verir.");
            messageCreate.setCode(PermissionEnum.MESSAGE_CREATE.getPermissionAlias());
            messageCreate.setProjectCode("MRP");
            session.persist(messageCreate);
        }

        //SUPPLY
        Permission supply = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLY.getPermissionAlias())).uniqueResult();

        if (supply == null) {
            supply = new Permission();
            supply.setName("Tedarik işlemlerini yapabilir.");
            supply.setCode(PermissionEnum.SUPPLY.getPermissionAlias());
            supply.setProjectCode("MRP");
            session.persist(supply);
        } else {
            supply.setName("Tedarik işlemlerini yapabilir.");
            supply.setCode(PermissionEnum.SUPPLY.getPermissionAlias());
            supply.setProjectCode("MRP");
            session.persist(supply);
        }

        //PRODUCTION LINE CREATE
        Permission productionLineCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_LINE_CREATE.getPermissionAlias())).uniqueResult();

        if (productionLineCreate == null) {
            productionLineCreate = new Permission();
            productionLineCreate.setName("Üretim hattı tanımlayabilir.");
            productionLineCreate.setCode(PermissionEnum.PRODUCTION_LINE_CREATE.getPermissionAlias());
            productionLineCreate.setProjectCode("MRP");
            session.persist(productionLineCreate);
        } else {
            productionLineCreate.setName("Üretim hattı tanımlayabilir.");
            productionLineCreate.setCode(PermissionEnum.PRODUCTION_LINE_CREATE.getPermissionAlias());
            productionLineCreate.setProjectCode("MRP");
            session.persist(productionLineCreate);
        }

        //PRODUCTION LINE READ
        Permission productionLineRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_LINE_READ.getPermissionAlias())).uniqueResult();

        if (productionLineRead == null) {
            productionLineRead = new Permission();
            productionLineRead.setName("Üretim hattı görüntüleyebilir.");
            productionLineRead.setCode(PermissionEnum.PRODUCTION_LINE_READ.getPermissionAlias());
            productionLineRead.setProjectCode("MRP");
            session.persist(productionLineRead);
        } else {
            productionLineRead.setName("Üretim hattı görüntüleyebilir.");
            productionLineRead.setCode(PermissionEnum.PRODUCTION_LINE_READ.getPermissionAlias());
            productionLineRead.setProjectCode("MRP");
            session.persist(productionLineRead);
        }

        //PRODUCTION LINE UPDATE
        Permission productionLineUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_LINE_UPDATE.getPermissionAlias())).uniqueResult();

        if (productionLineUpdate == null) {
            productionLineUpdate = new Permission();
            productionLineUpdate.setName("Üretim hattı güncelleyebilir.");
            productionLineUpdate.setCode(PermissionEnum.PRODUCTION_LINE_UPDATE.getPermissionAlias());
            productionLineUpdate.setProjectCode("MRP");
            session.persist(productionLineUpdate);
        } else {
            productionLineUpdate.setName("Üretim hattı güncelleyebilir.");
            productionLineUpdate.setCode(PermissionEnum.PRODUCTION_LINE_UPDATE.getPermissionAlias());
            productionLineUpdate.setProjectCode("MRP");
            session.persist(productionLineUpdate);
        }

        //PRODUCTION LINE DELETE
        Permission productionLineDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_LINE_DELETE.getPermissionAlias())).uniqueResult();

        if (productionLineDelete == null) {
            productionLineDelete = new Permission();
            productionLineDelete.setName("Üretim hattı silebilir.");
            productionLineDelete.setCode(PermissionEnum.PRODUCTION_LINE_DELETE.getPermissionAlias());
            productionLineDelete.setProjectCode("MRP");
            session.persist(productionLineDelete);
        } else {
            productionLineDelete.setName("Üretim hattı silebilir.");
            productionLineDelete.setCode(PermissionEnum.PRODUCTION_LINE_DELETE.getPermissionAlias());
            productionLineDelete.setProjectCode("MRP");
            session.persist(productionLineDelete);
        }

        //MATERIAL_TYPE LINE CREATE
        Permission materialTypeCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MATERIAL_TYPE_CREATE.getPermissionAlias())).uniqueResult();

        if (materialTypeCreate == null) {
            materialTypeCreate = new Permission();
            materialTypeCreate.setName("Malzeme Türü tanımlayabilir.");
            materialTypeCreate.setCode(PermissionEnum.MATERIAL_TYPE_CREATE.getPermissionAlias());
            materialTypeCreate.setProjectCode("MRP");
            session.persist(materialTypeCreate);
        } else {
            materialTypeCreate.setName("Malzeme Türü tanımlayabilir.");
            materialTypeCreate.setCode(PermissionEnum.MATERIAL_TYPE_CREATE.getPermissionAlias());
            materialTypeCreate.setProjectCode("MRP");
            session.persist(materialTypeCreate);
        }

        //MATERIAL_TYPE LINE READ
        Permission materialTypeRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MATERIAL_TYPE_READ.getPermissionAlias())).uniqueResult();

        if (materialTypeRead == null) {
            materialTypeRead = new Permission();
            materialTypeRead.setName("Malzeme Türü görüntüleyebilir.");
            materialTypeRead.setCode(PermissionEnum.MATERIAL_TYPE_READ.getPermissionAlias());
            materialTypeRead.setProjectCode("MRP");
            session.persist(materialTypeRead);
        } else {
            materialTypeRead.setName("Malzeme Türü görüntüleyebilir.");
            materialTypeRead.setCode(PermissionEnum.MATERIAL_TYPE_READ.getPermissionAlias());
            materialTypeRead.setProjectCode("MRP");
            session.persist(materialTypeRead);
        }

        //MATERIAL_TYPE LINE UPDATE
        Permission materialTypeUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MATERIAL_TYPE_UPDATE.getPermissionAlias())).uniqueResult();

        if (materialTypeUpdate == null) {
            materialTypeUpdate = new Permission();
            materialTypeUpdate.setName("Malzeme Türü güncelleyebilir.");
            materialTypeUpdate.setCode(PermissionEnum.MATERIAL_TYPE_UPDATE.getPermissionAlias());
            materialTypeUpdate.setProjectCode("MRP");
            session.persist(materialTypeUpdate);
        } else {
            materialTypeUpdate.setName("Malzeme Türü güncelleyebilir.");
            materialTypeUpdate.setCode(PermissionEnum.MATERIAL_TYPE_UPDATE.getPermissionAlias());
            materialTypeUpdate.setProjectCode("MRP");
            session.persist(materialTypeUpdate);
        }

        //MATERIAL_TYPE LINE DELETE
        Permission materialTypeDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MATERIAL_TYPE_DELETE.getPermissionAlias())).uniqueResult();

        if (materialTypeDelete == null) {
            materialTypeDelete = new Permission();
            materialTypeDelete.setName("Malzeme Türü silebilir.");
            materialTypeDelete.setCode(PermissionEnum.MATERIAL_TYPE_DELETE.getPermissionAlias());
            materialTypeDelete.setProjectCode("MRP");
            session.persist(materialTypeDelete);
        } else {
            materialTypeDelete.setName("Malzeme Türü silebilir.");
            materialTypeDelete.setCode(PermissionEnum.MATERIAL_TYPE_DELETE.getPermissionAlias());
            materialTypeDelete.setProjectCode("MRP");
            session.persist(materialTypeDelete);
        }

        //INTERNAL_CRITICAL LINE CREATE  internalCritical
        Permission internalCriticalCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.INTERNAL_CRITICAL_CREATE.getPermissionAlias())).uniqueResult();

        if (internalCriticalCreate == null) {
            internalCriticalCreate = new Permission();
            internalCriticalCreate.setName("İç Kritik tanımlayabilir.");
            internalCriticalCreate.setCode(PermissionEnum.INTERNAL_CRITICAL_CREATE.getPermissionAlias());
            internalCriticalCreate.setProjectCode("MRP");
            session.persist(internalCriticalCreate);
        } else {
            internalCriticalCreate.setName("İç Kritik tanımlayabilir.");
            internalCriticalCreate.setCode(PermissionEnum.INTERNAL_CRITICAL_CREATE.getPermissionAlias());
            internalCriticalCreate.setProjectCode("MRP");
            session.persist(internalCriticalCreate);
        }

        //INTERNAL_CRITICAL LINE READ
        Permission internalCriticalRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.INTERNAL_CRITICAL_READ.getPermissionAlias())).uniqueResult();

        if (internalCriticalRead == null) {
            internalCriticalRead = new Permission();
            internalCriticalRead.setName("İç Kritik görüntüleyebilir.");
            internalCriticalRead.setCode(PermissionEnum.INTERNAL_CRITICAL_READ.getPermissionAlias());
            internalCriticalRead.setProjectCode("MRP");
            session.persist(internalCriticalRead);
        } else {
            internalCriticalRead.setName("İç Kritik görüntüleyebilir.");
            internalCriticalRead.setCode(PermissionEnum.INTERNAL_CRITICAL_READ.getPermissionAlias());
            internalCriticalRead.setProjectCode("MRP");
            session.persist(internalCriticalRead);
        }

        //INTERNAL_CRITICAL LINE UPDATE
        Permission internalCriticalUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.INTERNAL_CRITICAL_UPDATE.getPermissionAlias())).uniqueResult();

        if (internalCriticalUpdate == null) {
            internalCriticalUpdate = new Permission();
            internalCriticalUpdate.setName("İç Kritik güncelleyebilir.");
            internalCriticalUpdate.setCode(PermissionEnum.INTERNAL_CRITICAL_UPDATE.getPermissionAlias());
            internalCriticalUpdate.setProjectCode("MRP");
            session.persist(internalCriticalUpdate);
        } else {
            internalCriticalUpdate.setName("İç Kritik güncelleyebilir.");
            internalCriticalUpdate.setCode(PermissionEnum.INTERNAL_CRITICAL_UPDATE.getPermissionAlias());
            internalCriticalUpdate.setProjectCode("MRP");
            session.persist(internalCriticalUpdate);
        }

        //INTERNAL_CRITICAL LINE DELETE
        Permission internalCriticalDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.INTERNAL_CRITICAL_DELETE.getPermissionAlias())).uniqueResult();

        if (internalCriticalDelete == null) {
            internalCriticalDelete = new Permission();
            internalCriticalDelete.setName("İç Kritik silebilir.");
            internalCriticalDelete.setCode(PermissionEnum.INTERNAL_CRITICAL_DELETE.getPermissionAlias());
            internalCriticalDelete.setProjectCode("MRP");
            session.persist(internalCriticalDelete);
        } else {
            internalCriticalDelete.setName("İç Kritik silebilir.");
            internalCriticalDelete.setCode(PermissionEnum.INTERNAL_CRITICAL_DELETE.getPermissionAlias());
            internalCriticalDelete.setProjectCode("MRP");
            session.persist(internalCriticalDelete);
        }

        //ProductSchedule Create
        Permission productionScheduleCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_SCHEDULE_CREATE.getPermissionAlias())).uniqueResult();

        if (productionScheduleCreate == null) {
            productionScheduleCreate = new Permission();
            productionScheduleCreate.setName("Monta emri verebilir.");
            productionScheduleCreate.setCode(PermissionEnum.PRODUCTION_SCHEDULE_CREATE.getPermissionAlias());
            productionScheduleCreate.setProjectCode("MRP");
            session.persist(productionScheduleCreate);
        } else {
            productionScheduleCreate.setName("Monta emri verebilir.");
            productionScheduleCreate.setCode(PermissionEnum.PRODUCTION_SCHEDULE_CREATE.getPermissionAlias());
            productionScheduleCreate.setProjectCode("MRP");
            session.persist(productionScheduleCreate);
        }

        //ProductSchedule Read
        Permission productionScheduleRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_SCHEDULE_READ.getPermissionAlias())).uniqueResult();

        if (productionScheduleRead == null) {
            productionScheduleRead = new Permission();
            productionScheduleRead.setName("Monta emri görüntüleyebilir.");
            productionScheduleRead.setCode(PermissionEnum.PRODUCTION_SCHEDULE_READ.getPermissionAlias());
            productionScheduleRead.setProjectCode("MRP");
            session.persist(productionScheduleRead);
        } else {
            productionScheduleRead.setName("Monta emri görüntüleyebilir.");
            productionScheduleRead.setCode(PermissionEnum.PRODUCTION_SCHEDULE_READ.getPermissionAlias());
            productionScheduleRead.setProjectCode("MRP");
            session.persist(productionScheduleRead);
        }

        //ProductSchedule Update
        Permission productionScheduleUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_SCHEDULE_UPDATE.getPermissionAlias())).uniqueResult();

        if (productionScheduleUpdate == null) {
            productionScheduleUpdate = new Permission();
            productionScheduleUpdate.setName("Üretim takvimini güncelleyebilir.");
            productionScheduleUpdate.setCode(PermissionEnum.PRODUCTION_SCHEDULE_UPDATE.getPermissionAlias());
            productionScheduleUpdate.setProjectCode("MRP");
            session.persist(productionScheduleUpdate);
        } else {
            productionScheduleUpdate.setName("Üretim takvimini güncelleyebilir.");
            productionScheduleUpdate.setCode(PermissionEnum.PRODUCTION_SCHEDULE_UPDATE.getPermissionAlias());
            productionScheduleUpdate.setProjectCode("MRP");
            session.persist(productionScheduleUpdate);
        }

        //ProductSchedule Update
        Permission productionScheduleDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PRODUCTION_SCHEDULE_DELETE.getPermissionAlias())).uniqueResult();

        if (productionScheduleDelete == null) {
            productionScheduleDelete = new Permission();
            productionScheduleDelete.setName("Monta emri silebilir.");
            productionScheduleDelete.setCode(PermissionEnum.PRODUCTION_SCHEDULE_DELETE.getPermissionAlias());
            productionScheduleDelete.setProjectCode("MRP");
            session.persist(productionScheduleDelete);
        } else {
            productionScheduleDelete.setName("Monta emri silebilir.");
            productionScheduleDelete.setCode(PermissionEnum.PRODUCTION_SCHEDULE_DELETE.getPermissionAlias());
            productionScheduleDelete.setProjectCode("MRP");
            session.persist(productionScheduleDelete);
        }

        //SUPPLIER_CREATE  supplierCreate
        Permission supplierCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLIER_CREATE.getPermissionAlias())).uniqueResult();

        if (supplierCreate == null) {
            supplierCreate = new Permission();
            supplierCreate.setName("Tedarikçi tanımlayabilir.");
            supplierCreate.setCode(PermissionEnum.SUPPLIER_CREATE.getPermissionAlias());
            supplierCreate.setProjectCode("MRP");
            session.persist(supplierCreate);
        } else {
            supplierCreate.setName("Tedarikçi tanımlayabilir.");
            supplierCreate.setCode(PermissionEnum.SUPPLIER_CREATE.getPermissionAlias());
            supplierCreate.setProjectCode("MRP");
            session.persist(supplierCreate);
        }

        //SUPPLIER_READ supplierRead
        Permission supplierRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLIER_READ.getPermissionAlias())).uniqueResult();

        if (supplierRead == null) {
            supplierRead = new Permission();
            supplierRead.setName("Tedarikçi görüntüleyebilir.");
            supplierRead.setCode(PermissionEnum.SUPPLIER_READ.getPermissionAlias());
            supplierRead.setProjectCode("MRP");
            session.persist(supplierRead);
        } else {
            supplierRead.setName("Tedarikçi görüntüleyebilir.");
            supplierRead.setCode(PermissionEnum.SUPPLIER_READ.getPermissionAlias());
            supplierRead.setProjectCode("MRP");
            session.persist(supplierRead);
        }

        //SUPPLIER_UPDATE supplierUpdate
        Permission supplierUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLIER_UPDATE.getPermissionAlias())).uniqueResult();

        if (supplierUpdate == null) {
            supplierUpdate = new Permission();
            supplierUpdate.setName("Tedarikçi güncelleyebilir.");
            supplierUpdate.setCode(PermissionEnum.SUPPLIER_UPDATE.getPermissionAlias());
            supplierUpdate.setProjectCode("MRP");
            session.persist(supplierUpdate);
        } else {
            supplierUpdate.setName("Tedarikçi güncelleyebilir.");
            supplierUpdate.setCode(PermissionEnum.SUPPLIER_UPDATE.getPermissionAlias());
            supplierUpdate.setProjectCode("MRP");
            session.persist(supplierUpdate);
        }

        //SUPPLIER_DELETE supplierDelete
        Permission supplierDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLIER_DELETE.getPermissionAlias())).uniqueResult();

        if (supplierDelete == null) {
            supplierDelete = new Permission();
            supplierDelete.setName("Tedarikçi silebilir.");
            supplierDelete.setCode(PermissionEnum.SUPPLIER_DELETE.getPermissionAlias());
            supplierDelete.setProjectCode("MRP");
            session.persist(supplierDelete);
        } else {
            supplierDelete.setName("Tedarikçi silebilir.");
            supplierDelete.setCode(PermissionEnum.SUPPLIER_DELETE.getPermissionAlias());
            supplierDelete.setProjectCode("MRP");
            session.persist(supplierDelete);
        }


        //STOCK_TYPE_CREATE  stockTypeCreate
        Permission stockTypeCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_TYPE_CREATE.getPermissionAlias())).uniqueResult();

        if (stockTypeCreate == null) {
            stockTypeCreate = new Permission();
            stockTypeCreate.setName("Stok türü tanımlayabilir.");
            stockTypeCreate.setCode(PermissionEnum.STOCK_TYPE_CREATE.getPermissionAlias());
            stockTypeCreate.setProjectCode("MRP");
            session.persist(stockTypeCreate);
        } else {
            stockTypeCreate.setName("Stok türü tanımlayabilir.");
            stockTypeCreate.setCode(PermissionEnum.STOCK_TYPE_CREATE.getPermissionAlias());
            stockTypeCreate.setProjectCode("MRP");
            session.persist(stockTypeCreate);
        }

//STOCK_TYPE_READ stockTypeRead
        Permission stockTypeRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_TYPE_READ.getPermissionAlias())).uniqueResult();

        if (stockTypeRead == null) {
            stockTypeRead = new Permission();
            stockTypeRead.setName("Stok türü görüntüleyebilir.");
            stockTypeRead.setCode(PermissionEnum.STOCK_TYPE_READ.getPermissionAlias());
            stockTypeRead.setProjectCode("MRP");
            session.persist(stockTypeRead);
        } else {
            stockTypeRead.setName("Stok türü görüntüleyebilir.");
            stockTypeRead.setCode(PermissionEnum.STOCK_TYPE_READ.getPermissionAlias());
            stockTypeRead.setProjectCode("MRP");
            session.persist(stockTypeRead);
        }

//STOCK_TYPE_UPDATE stockTypeUpdate
        Permission stockTypeUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_TYPE_UPDATE.getPermissionAlias())).uniqueResult();

        if (stockTypeUpdate == null) {
            stockTypeUpdate = new Permission();
            stockTypeUpdate.setName("Stok türü güncelleyebilir.");
            stockTypeUpdate.setCode(PermissionEnum.STOCK_TYPE_UPDATE.getPermissionAlias());
            stockTypeUpdate.setProjectCode("MRP");
            session.persist(stockTypeUpdate);
        } else {
            stockTypeUpdate.setName("Stok türü güncelleyebilir.");
            stockTypeUpdate.setCode(PermissionEnum.STOCK_TYPE_UPDATE.getPermissionAlias());
            stockTypeUpdate.setProjectCode("MRP");
            session.persist(stockTypeUpdate);
        }

//STOCK_TYPE_DELETE stockTypeDelete
        Permission stockTypeDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STOCK_TYPE_DELETE.getPermissionAlias())).uniqueResult();

        if (stockTypeDelete == null) {
            stockTypeDelete = new Permission();
            stockTypeDelete.setName("Stok türü silebilir.");
            stockTypeDelete.setCode(PermissionEnum.STOCK_TYPE_DELETE.getPermissionAlias());
            stockTypeDelete.setProjectCode("MRP");
            session.persist(stockTypeDelete);
        } else {
            stockTypeDelete.setName("Stok türü silebilir.");
            stockTypeDelete.setCode(PermissionEnum.STOCK_TYPE_DELETE.getPermissionAlias());
            stockTypeDelete.setProjectCode("MRP");
            session.persist(stockTypeDelete);
        }

        Permission sayaciCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SAYACI_CREATE.getPermissionAlias())).uniqueResult();

        if (sayaciCreate == null) {
            sayaciCreate = new Permission();
            sayaciCreate.setName("Sayacı yaratabilir.");
            sayaciCreate.setCode(PermissionEnum.SAYACI_CREATE.getPermissionAlias());
            sayaciCreate.setProjectCode("MRP");
            session.persist(sayaciCreate);
        } else {
            sayaciCreate.setName("Sayacı yaratabilir.");
            sayaciCreate.setCode(PermissionEnum.SAYACI_CREATE.getPermissionAlias());
            sayaciCreate.setProjectCode("MRP");
            session.persist(sayaciCreate);
        }

        Permission sayaciRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SAYACI_READ.getPermissionAlias())).uniqueResult();

        if (sayaciRead == null) {
            sayaciRead = new Permission();
            sayaciRead.setName("Sayacı görüntüleyebilir.");
            sayaciRead.setCode(PermissionEnum.SAYACI_READ.getPermissionAlias());
            sayaciRead.setProjectCode("MRP");
            session.persist(sayaciRead);
        } else {
            sayaciRead.setName("Sayacı görüntüleyebilir.");
            sayaciRead.setCode(PermissionEnum.SAYACI_READ.getPermissionAlias());
            sayaciRead.setProjectCode("MRP");
            session.persist(sayaciRead);
        }

        Permission sayaciUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SAYACI_UPDATE.getPermissionAlias())).uniqueResult();

        if (sayaciUpdate == null) {
            sayaciUpdate = new Permission();
            sayaciUpdate.setName("Sayacı güncelleyebilir.");
            sayaciUpdate.setCode(PermissionEnum.SAYACI_UPDATE.getPermissionAlias());
            sayaciUpdate.setProjectCode("MRP");
            session.persist(sayaciUpdate);
        } else {
            sayaciUpdate.setName("Sayacı güncelleyebilir.");
            sayaciUpdate.setCode(PermissionEnum.SAYACI_UPDATE.getPermissionAlias());
            sayaciUpdate.setProjectCode("MRP");
            session.persist(sayaciUpdate);
        }

        Permission sayaciDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SAYACI_DELETE.getPermissionAlias())).uniqueResult();

        if (sayaciDelete == null) {
            sayaciDelete = new Permission();
            sayaciDelete.setName("Sayacı silebilir.");
            sayaciDelete.setCode(PermissionEnum.SAYACI_DELETE.getPermissionAlias());
            sayaciDelete.setProjectCode("MRP");
            session.persist(sayaciDelete);
        } else {
            sayaciDelete.setName("Sayacı silebilir.");
            sayaciDelete.setCode(PermissionEnum.SAYACI_DELETE.getPermissionAlias());
            sayaciDelete.setProjectCode("MRP");
            session.persist(sayaciDelete);
        }

        Permission sayaCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SAYA_CREATE.getPermissionAlias())).uniqueResult();

        if (sayaCreate == null) {
            sayaCreate = new Permission();
            sayaCreate.setName("Saya emri oluşturabilir.");
            sayaCreate.setCode(PermissionEnum.SAYA_CREATE.getPermissionAlias());
            sayaCreate.setProjectCode("MRP");
            session.persist(sayaCreate);
        } else {
            sayaCreate.setName("Saya emri oluşturabilir.");
            sayaCreate.setCode(PermissionEnum.SAYA_CREATE.getPermissionAlias());
            sayaCreate.setProjectCode("MRP");
            session.persist(sayaCreate);
        }

        Permission sayaRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SAYA_READ.getPermissionAlias())).uniqueResult();

        if (sayaRead == null) {
            sayaRead = new Permission();
            sayaRead.setName("Saya emri görüntüleyebilir.");
            sayaRead.setCode(PermissionEnum.SAYA_READ.getPermissionAlias());
            sayaRead.setProjectCode("MRP");
            session.persist(sayaRead);
        } else {
            sayaRead.setName("Saya emri görüntüleyebilir.");
            sayaRead.setCode(PermissionEnum.SAYA_READ.getPermissionAlias());
            sayaRead.setProjectCode("MRP");
            session.persist(sayaRead);
        }

        Permission sayaUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SAYA_UPDATE.getPermissionAlias())).uniqueResult();

        if (sayaUpdate == null) {
            sayaUpdate = new Permission();
            sayaUpdate.setName("Saya emri güncelleyebilir.");
            sayaUpdate.setCode(PermissionEnum.SAYA_UPDATE.getPermissionAlias());
            sayaUpdate.setProjectCode("MRP");
            session.persist(sayaUpdate);
        } else {
            sayaUpdate.setName("Saya emri güncelleyebilir.");
            sayaUpdate.setCode(PermissionEnum.SAYA_UPDATE.getPermissionAlias());
            sayaUpdate.setProjectCode("MRP");
            session.persist(sayaUpdate);
        }

        Permission sayaDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SAYA_DELETE.getPermissionAlias())).uniqueResult();

        if (sayaDelete == null) {
            sayaDelete = new Permission();
            sayaDelete.setName("Saya emri silebilir.");
            sayaDelete.setCode(PermissionEnum.SAYA_DELETE.getPermissionAlias());
            sayaDelete.setProjectCode("MRP");
            session.persist(sayaDelete);
        } else {
            sayaDelete.setName("Saya emri silebilir.");
            sayaDelete.setCode(PermissionEnum.SAYA_DELETE.getPermissionAlias());
            sayaDelete.setProjectCode("MRP");
            session.persist(sayaDelete);
        }

        Permission bulkSupplyCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLY_CREATE.getPermissionAlias())).uniqueResult();

        if (bulkSupplyCreate == null) {
            bulkSupplyCreate = new Permission();
            bulkSupplyCreate.setName("Tedarik oluşturabilir.");
            bulkSupplyCreate.setCode(PermissionEnum.SUPPLY_CREATE.getPermissionAlias());
            bulkSupplyCreate.setProjectCode("MRP");
            session.persist(bulkSupplyCreate);
        } else {
            bulkSupplyCreate.setName("Tedarik oluşturabilir.");
            bulkSupplyCreate.setCode(PermissionEnum.SUPPLY_CREATE.getPermissionAlias());
            bulkSupplyCreate.setProjectCode("MRP");
            session.persist(bulkSupplyCreate);
        }

        Permission bulkSupplyRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLY_READ.getPermissionAlias())).uniqueResult();

        if (bulkSupplyRead == null) {
            bulkSupplyRead = new Permission();
            bulkSupplyRead.setName("Tedarik görüntüleyebilir.");
            bulkSupplyRead.setCode(PermissionEnum.SUPPLY_READ.getPermissionAlias());
            bulkSupplyRead.setProjectCode("MRP");
            session.persist(bulkSupplyRead);
        } else {
            bulkSupplyRead.setName("Tedarik görüntüleyebilir.");
            bulkSupplyRead.setCode(PermissionEnum.SUPPLY_READ.getPermissionAlias());
            bulkSupplyRead.setProjectCode("MRP");
            session.persist(bulkSupplyRead);
        }

        Permission bulkSupplyUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLY_UPDATE.getPermissionAlias())).uniqueResult();

        if (bulkSupplyUpdate == null) {
            bulkSupplyUpdate = new Permission();
            bulkSupplyUpdate.setName("Tedarik güncelleyebilir.");
            bulkSupplyUpdate.setCode(PermissionEnum.SUPPLY_UPDATE.getPermissionAlias());
            bulkSupplyUpdate.setProjectCode("MRP");
            session.persist(bulkSupplyUpdate);
        } else {
            bulkSupplyUpdate.setName("Tedarik güncelleyebilir.");
            bulkSupplyUpdate.setCode(PermissionEnum.SUPPLY_UPDATE.getPermissionAlias());
            bulkSupplyUpdate.setProjectCode("MRP");
            session.persist(bulkSupplyUpdate);
        }

        Permission bulkSupplyDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SUPPLY_DELETE.getPermissionAlias())).uniqueResult();

        if (bulkSupplyDelete == null) {
            bulkSupplyDelete = new Permission();
            bulkSupplyDelete.setName("Tedarik silebilir.");
            bulkSupplyDelete.setCode(PermissionEnum.SUPPLY_DELETE.getPermissionAlias());
            bulkSupplyDelete.setProjectCode("MRP");
            session.persist(bulkSupplyDelete);
        } else {
            bulkSupplyDelete.setName("Tedarik silebilir.");
            bulkSupplyDelete.setCode(PermissionEnum.SUPPLY_DELETE.getPermissionAlias());
            bulkSupplyDelete.setProjectCode("MRP");
            session.persist(bulkSupplyDelete);
        }

        Permission shipmentInfoCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_INFO_CREATE.getPermissionAlias())).uniqueResult();

        if (shipmentInfoCreate == null) {
            shipmentInfoCreate = new Permission();
            shipmentInfoCreate.setName("Sevkiyat bilgisi tanımlayabilir.");
            shipmentInfoCreate.setCode(PermissionEnum.SHIPMENT_INFO_CREATE.getPermissionAlias());
            shipmentInfoCreate.setProjectCode("MRP");
            session.persist(shipmentInfoCreate);
        } else {
            shipmentInfoCreate.setName("Sevkiyat bilgisi tanımlayabilir.");
            shipmentInfoCreate.setCode(PermissionEnum.SHIPMENT_INFO_CREATE.getPermissionAlias());
            shipmentInfoCreate.setProjectCode("MRP");
            session.persist(shipmentInfoCreate);
        }

        Permission shipmentInfoRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_INFO_READ.getPermissionAlias())).uniqueResult();

        if (shipmentInfoRead == null) {
            shipmentInfoRead = new Permission();
            shipmentInfoRead.setName("Sevkiyat bilgisi görüntüleyebilir.");
            shipmentInfoRead.setCode(PermissionEnum.SHIPMENT_INFO_READ.getPermissionAlias());
            shipmentInfoRead.setProjectCode("MRP");
            session.persist(shipmentInfoRead);
        } else {
            shipmentInfoRead.setName("Sevkiyat bilgisi görüntüleyebilir.");
            shipmentInfoRead.setCode(PermissionEnum.SHIPMENT_INFO_READ.getPermissionAlias());
            shipmentInfoRead.setProjectCode("MRP");
            session.persist(shipmentInfoRead);
        }

        Permission shipmentInfoUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_INFO_UPDATE.getPermissionAlias())).uniqueResult();

        if (shipmentInfoUpdate == null) {
            shipmentInfoUpdate = new Permission();
            shipmentInfoUpdate.setName("Sevkiyat bilgisi güncelleyebilir.");
            shipmentInfoUpdate.setCode(PermissionEnum.SHIPMENT_INFO_UPDATE.getPermissionAlias());
            shipmentInfoUpdate.setProjectCode("MRP");
            session.persist(shipmentInfoUpdate);
        } else {
            shipmentInfoUpdate.setName("Sevkiyat bilgisi güncelleyebilir.");
            shipmentInfoUpdate.setCode(PermissionEnum.SHIPMENT_INFO_UPDATE.getPermissionAlias());
            shipmentInfoUpdate.setProjectCode("MRP");
            session.persist(shipmentInfoUpdate);
        }

        Permission shipmentInfoDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_INFO_DELETE.getPermissionAlias())).uniqueResult();

        if (shipmentInfoDelete == null) {
            shipmentInfoDelete = new Permission();
            shipmentInfoDelete.setName("Sevkiyat bilgisi silebilir.");
            shipmentInfoDelete.setCode(PermissionEnum.SHIPMENT_INFO_DELETE.getPermissionAlias());
            shipmentInfoDelete.setProjectCode("MRP");
            session.persist(shipmentInfoDelete);
        } else {
            shipmentInfoDelete.setName("Sevkiyat bilgisi silebilir.");
            shipmentInfoDelete.setCode(PermissionEnum.SHIPMENT_INFO_DELETE.getPermissionAlias());
            shipmentInfoDelete.setProjectCode("MRP");
            session.persist(shipmentInfoDelete);
        }

        Permission activityReportCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ACTIVITY_REPORT_CREATE.getPermissionAlias())).uniqueResult();

        if (activityReportCreate == null) {
            activityReportCreate = new Permission();
            activityReportCreate.setName("Faaliyet raporu tanımlayabilir.");
            activityReportCreate.setCode(PermissionEnum.ACTIVITY_REPORT_CREATE.getPermissionAlias());
            activityReportCreate.setProjectCode("MRP");
            session.persist(activityReportCreate);
        } else {
            activityReportCreate.setName("Faaliyet raporu tanımlayabilir.");
            activityReportCreate.setCode(PermissionEnum.ACTIVITY_REPORT_CREATE.getPermissionAlias());
            activityReportCreate.setProjectCode("MRP");
            session.persist(activityReportCreate);
        }

        Permission activityReportRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ACTIVITY_REPORT_READ.getPermissionAlias())).uniqueResult();

        if (activityReportRead == null) {
            activityReportRead = new Permission();
            activityReportRead.setName("Faaliyet raporu okuyabilir.");
            activityReportRead.setCode(PermissionEnum.ACTIVITY_REPORT_READ.getPermissionAlias());
            activityReportRead.setProjectCode("MRP");
            session.persist(activityReportRead);
        } else {
            activityReportRead.setName("Faaliyet raporu okuyabilir.");
            activityReportRead.setCode(PermissionEnum.ACTIVITY_REPORT_READ.getPermissionAlias());
            activityReportRead.setProjectCode("MRP");
            session.persist(activityReportRead);
        }

        Permission activityReportDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ACTIVITY_REPORT_DELETE.getPermissionAlias())).uniqueResult();

        if (activityReportDelete == null) {
            activityReportDelete = new Permission();
            activityReportDelete.setName("Faaliyet raporu silebilir.");
            activityReportDelete.setCode(PermissionEnum.ACTIVITY_REPORT_DELETE.getPermissionAlias());
            activityReportDelete.setProjectCode("MRP");
            session.persist(activityReportDelete);
        } else {
            activityReportDelete.setName("Faaliyet raporu silebilir.");
            activityReportDelete.setCode(PermissionEnum.ACTIVITY_REPORT_DELETE.getPermissionAlias());
            activityReportDelete.setProjectCode("MRP");
            session.persist(activityReportDelete);
        }

        Permission activityReportUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.ACTIVITY_REPORT_UPDATE.getPermissionAlias())).uniqueResult();

        if (activityReportUpdate == null) {
            activityReportUpdate = new Permission();
            activityReportUpdate.setName("Faaliyet raporu düzenleyebilir.");
            activityReportUpdate.setCode(PermissionEnum.ACTIVITY_REPORT_UPDATE.getPermissionAlias());
            activityReportUpdate.setProjectCode("MRP");
            session.persist(activityReportUpdate);
        } else {
            activityReportUpdate.setName("Faaliyet raporu düzenleyebilir.");
            activityReportUpdate.setCode(PermissionEnum.ACTIVITY_REPORT_UPDATE.getPermissionAlias());
            activityReportUpdate.setProjectCode("MRP");
            session.persist(activityReportUpdate);
        }

        Permission cuttingOrderUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CUTTING_ORDER_UPDATE.getPermissionAlias())).uniqueResult();

        if (cuttingOrderUpdate == null) {
            cuttingOrderUpdate = new Permission();
            cuttingOrderUpdate.setName("Kesim emri oluşturabilir/görüntüleyebilir/güncelleyebilir.");
            cuttingOrderUpdate.setCode(PermissionEnum.CUTTING_ORDER_UPDATE.getPermissionAlias());
            cuttingOrderUpdate.setProjectCode("MRP");
            session.persist(cuttingOrderUpdate);
        } else {
            cuttingOrderUpdate.setName("Kesim emri oluşturabilir/görüntüleyebilir/güncelleyebilir.");
            cuttingOrderUpdate.setCode(PermissionEnum.CUTTING_ORDER_UPDATE.getPermissionAlias());
            cuttingOrderUpdate.setProjectCode("MRP");
            session.persist(cuttingOrderUpdate);
        }

        Permission cuttingOrderDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CUTTING_ORDER_DELETE.getPermissionAlias())).uniqueResult();

        if (cuttingOrderDelete == null) {
            cuttingOrderDelete = new Permission();
            cuttingOrderDelete.setName("Kesim emri silebilir.");
            cuttingOrderDelete.setCode(PermissionEnum.CUTTING_ORDER_DELETE.getPermissionAlias());
            cuttingOrderDelete.setProjectCode("MRP");
            session.persist(cuttingOrderDelete);
        } else {
            cuttingOrderDelete.setName("Kesim emri silebilir.");
            cuttingOrderDelete.setCode(PermissionEnum.CUTTING_ORDER_DELETE.getPermissionAlias());
            cuttingOrderDelete.setProjectCode("MRP");
            session.persist(cuttingOrderDelete);
        }

        Permission montaCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MONTA_CREATE.getPermissionAlias())).uniqueResult();

        if (montaCreate == null) {
            montaCreate = new Permission();
            montaCreate.setName("Monta emri verebilir.");
            montaCreate.setCode(PermissionEnum.MONTA_CREATE.getPermissionAlias());
            montaCreate.setProjectCode("MRP");
            session.persist(montaCreate);
        } else {
            montaCreate.setName("Monta emri verebilir.");
            montaCreate.setCode(PermissionEnum.MONTA_CREATE.getPermissionAlias());
            montaCreate.setProjectCode("MRP");
            session.persist(montaCreate);
        }

        Permission montaRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MONTA_READ.getPermissionAlias())).uniqueResult();

        if (montaRead == null) {
            montaRead = new Permission();
            montaRead.setName("Monta emri görüntüleyebilir.");
            montaRead.setCode(PermissionEnum.MONTA_READ.getPermissionAlias());
            montaRead.setProjectCode("MRP");
            session.persist(montaRead);
        } else {
            montaRead.setName("Monta emri görüntüleyebilir.");
            montaRead.setCode(PermissionEnum.MONTA_READ.getPermissionAlias());
            montaRead.setProjectCode("MRP");
            session.persist(montaRead);
        }

        Permission montaUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MONTA_UPDATE.getPermissionAlias())).uniqueResult();

        if (montaUpdate == null) {
            montaUpdate = new Permission();
            montaUpdate.setName("Monta emri güncelleyebilir.");
            montaUpdate.setCode(PermissionEnum.MONTA_UPDATE.getPermissionAlias());
            montaUpdate.setProjectCode("MRP");
            session.persist(montaUpdate);
        } else {
            montaUpdate.setName("Monta emri güncelleyebilir.");
            montaUpdate.setCode(PermissionEnum.MONTA_UPDATE.getPermissionAlias());
            montaUpdate.setProjectCode("MRP");
            session.persist(montaUpdate);
        }

        Permission montaDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MONTA_DELETE.getPermissionAlias())).uniqueResult();

        if (montaDelete == null) {
            montaDelete = new Permission();
            montaDelete.setName("Monta emri silebilir.");
            montaDelete.setCode(PermissionEnum.MONTA_DELETE.getPermissionAlias());
            montaDelete.setProjectCode("MRP");
            session.persist(montaDelete);
        } else {
            montaDelete.setName("Monta emri silebilir.");
            montaDelete.setCode(PermissionEnum.MONTA_DELETE.getPermissionAlias());
            montaDelete.setProjectCode("MRP");
            session.persist(montaDelete);
        }

        Permission companySelfUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.COMPANY_SELF_UPDATE.getPermissionAlias())).uniqueResult();

        if (companySelfUpdate == null) {
            companySelfUpdate = new Permission();
            companySelfUpdate.setName("Şirket kendi bilgilerini güncelleyebilir.");
            companySelfUpdate.setCode(PermissionEnum.COMPANY_SELF_UPDATE.getPermissionAlias());
            companySelfUpdate.setProjectCode("MRP");
            session.persist(companySelfUpdate);
        } else {
            companySelfUpdate.setName("Şirket kendi bilgilerini güncelleyebilir.");
            companySelfUpdate.setCode(PermissionEnum.COMPANY_SELF_UPDATE.getPermissionAlias());
            companySelfUpdate.setProjectCode("MRP");
            session.persist(companySelfUpdate);
        }
        Permission generalProductReportRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.REPORT_READ.getPermissionAlias())).uniqueResult();

        if (generalProductReportRead == null) {
            generalProductReportRead = new Permission();
            generalProductReportRead.setName("Raporları görüntüyebilir.");
            generalProductReportRead.setCode(PermissionEnum.REPORT_READ.getPermissionAlias());
            generalProductReportRead.setProjectCode("MRP");
            session.persist(generalProductReportRead);
        } else {
            generalProductReportRead.setName("Raporları görüntüyebilir.");
            generalProductReportRead.setCode(PermissionEnum.REPORT_READ.getPermissionAlias());
            generalProductReportRead.setProjectCode("MRP");
            session.persist(generalProductReportRead);
        }

        Permission fairModelAssortCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_ASSORT_CREATE.getPermissionAlias())).uniqueResult();

        if (fairModelAssortCreate == null) {
            fairModelAssortCreate = new Permission();
            fairModelAssortCreate.setName("Fuar modeline asorti ekleyebilir.");
            fairModelAssortCreate.setCode(PermissionEnum.FAIR_MODEL_ASSORT_CREATE.getPermissionAlias());
            fairModelAssortCreate.setProjectCode("MRP");
            session.persist(fairModelAssortCreate);
        } else {
            fairModelAssortCreate.setName("Fuar modeline asorti ekleyebilir.");
            fairModelAssortCreate.setCode(PermissionEnum.FAIR_MODEL_ASSORT_CREATE.getPermissionAlias());
            fairModelAssortCreate.setProjectCode("MRP");
            session.persist(fairModelAssortCreate);
        }

        Permission fairModelAssortRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_ASSORT_READ.getPermissionAlias())).uniqueResult();

        if (fairModelAssortRead == null) {
            fairModelAssortRead = new Permission();
            fairModelAssortRead.setName("Fuar modeline asorti görüntüleyebilir.");
            fairModelAssortRead.setCode(PermissionEnum.FAIR_MODEL_ASSORT_READ.getPermissionAlias());
            fairModelAssortRead.setProjectCode("MRP");
            session.persist(fairModelAssortRead);
        } else {
            fairModelAssortRead.setName("Fuar modeline asorti görüntüleyebilir.");
            fairModelAssortRead.setCode(PermissionEnum.FAIR_MODEL_ASSORT_READ.getPermissionAlias());
            fairModelAssortRead.setProjectCode("MRP");
            session.persist(fairModelAssortRead);
        }

        Permission fairModelAssortUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_ASSORT_UPDATE.getPermissionAlias())).uniqueResult();

        if (fairModelAssortUpdate == null) {
            fairModelAssortUpdate = new Permission();
            fairModelAssortUpdate.setName("Fuar modeline asorti güncelleyebilir.");
            fairModelAssortUpdate.setCode(PermissionEnum.FAIR_MODEL_ASSORT_UPDATE.getPermissionAlias());
            fairModelAssortUpdate.setProjectCode("MRP");
            session.persist(fairModelAssortUpdate);
        } else {
            fairModelAssortUpdate.setName("Fuar modeline asorti güncelleyebilir.");
            fairModelAssortUpdate.setCode(PermissionEnum.FAIR_MODEL_ASSORT_UPDATE.getPermissionAlias());
            fairModelAssortUpdate.setProjectCode("MRP");
            session.persist(fairModelAssortUpdate);
        }

        Permission fairModelAssortDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_ASSORT_DELETE.getPermissionAlias())).uniqueResult();

        if (fairModelAssortDelete == null) {
            fairModelAssortDelete = new Permission();
            fairModelAssortDelete.setName("Fuar modeline asorti silebilir.");
            fairModelAssortDelete.setCode(PermissionEnum.FAIR_MODEL_ASSORT_DELETE.getPermissionAlias());
            fairModelAssortDelete.setProjectCode("MRP");
            session.persist(fairModelAssortDelete);
        } else {
            fairModelAssortDelete.setName("Fuar modeline asorti silebilir.");
            fairModelAssortDelete.setCode(PermissionEnum.FAIR_MODEL_ASSORT_DELETE.getPermissionAlias());
            fairModelAssortDelete.setProjectCode("MRP");
            session.persist(fairModelAssortDelete);
        }

        Permission fairModelPriceCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_PRICE_CREATE.getPermissionAlias())).uniqueResult();

        if (fairModelPriceCreate == null) {
            fairModelPriceCreate = new Permission();
            fairModelPriceCreate.setName("Fuar modeline fiyat ekleyebilir.");
            fairModelPriceCreate.setCode(PermissionEnum.FAIR_MODEL_PRICE_CREATE.getPermissionAlias());
            fairModelPriceCreate.setProjectCode("MRP");
            session.persist(fairModelPriceCreate);
        } else {
            fairModelPriceCreate.setName("Fuar modeline fiyat ekleyebilir.");
            fairModelPriceCreate.setCode(PermissionEnum.FAIR_MODEL_PRICE_CREATE.getPermissionAlias());
            fairModelPriceCreate.setProjectCode("MRP");
            session.persist(fairModelPriceCreate);
        }

        Permission fairModelPriceRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_PRICE_READ.getPermissionAlias())).uniqueResult();

        if (fairModelPriceRead == null) {
            fairModelPriceRead = new Permission();
            fairModelPriceRead.setName("Fuar modeline fiyat görüntüleyebilir.");
            fairModelPriceRead.setCode(PermissionEnum.FAIR_MODEL_PRICE_READ.getPermissionAlias());
            fairModelPriceRead.setProjectCode("MRP");
            session.persist(fairModelPriceRead);
        } else {
            fairModelPriceRead.setName("Fuar modeline fiyat görüntüleyebilir.");
            fairModelPriceRead.setCode(PermissionEnum.FAIR_MODEL_PRICE_READ.getPermissionAlias());
            fairModelPriceRead.setProjectCode("MRP");
            session.persist(fairModelPriceRead);
        }

        Permission fairModelPriceUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_PRICE_UPDATE.getPermissionAlias())).uniqueResult();

        if (fairModelPriceUpdate == null) {
            fairModelPriceUpdate = new Permission();
            fairModelPriceUpdate.setName("Fuar modeline fiyat güncelleyebilir.");
            fairModelPriceUpdate.setCode(PermissionEnum.FAIR_MODEL_PRICE_UPDATE.getPermissionAlias());
            fairModelPriceUpdate.setProjectCode("MRP");
            session.persist(fairModelPriceUpdate);
        } else {
            fairModelPriceUpdate.setName("Fuar modeline fiyat güncelleyebilir.");
            fairModelPriceUpdate.setCode(PermissionEnum.FAIR_MODEL_PRICE_UPDATE.getPermissionAlias());
            fairModelPriceUpdate.setProjectCode("MRP");
            session.persist(fairModelPriceUpdate);
        }

        Permission fairModelPriceDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_MODEL_PRICE_DELETE.getPermissionAlias())).uniqueResult();

        if (fairModelPriceDelete == null) {
            fairModelPriceDelete = new Permission();
            fairModelPriceDelete.setName("Fuar modeline fiyat silebilir.");
            fairModelPriceDelete.setCode(PermissionEnum.FAIR_MODEL_PRICE_DELETE.getPermissionAlias());
            fairModelPriceDelete.setProjectCode("MRP");
            session.persist(fairModelPriceDelete);
        } else {
            fairModelPriceDelete.setName("Fuar modeline fiyat silebilir.");
            fairModelPriceDelete.setCode(PermissionEnum.FAIR_MODEL_PRICE_DELETE.getPermissionAlias());
            fairModelPriceDelete.setProjectCode("MRP");
            session.persist(fairModelPriceDelete);
        }

        Permission copyAssort = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.COPY_ASSORT.getPermissionAlias())).uniqueResult();

        if (copyAssort == null) {
            copyAssort = new Permission();
            copyAssort.setName("Aynı sistem kodlu modellere, seçilen modelin asortisini kopyalayabilir.");
            copyAssort.setCode(PermissionEnum.COPY_ASSORT.getPermissionAlias());
            copyAssort.setProjectCode("MRP");
            session.persist(copyAssort);
        } else {
            copyAssort.setName("Aynı sistem kodlu modellere, seçilen modelin asortisini kopyalayabilir.");
            copyAssort.setCode(PermissionEnum.COPY_ASSORT.getPermissionAlias());
            copyAssort.setProjectCode("MRP");
            session.persist(copyAssort);
        }

        Permission copyAssortToAll = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.COPY_ASSORT_TO_ALL.getPermissionAlias())).uniqueResult();

        if (copyAssortToAll == null) {
            copyAssortToAll = new Permission();
            copyAssortToAll.setName("Seçilen modelin asortilerini tüm modellere uygular.");
            copyAssortToAll.setCode(PermissionEnum.COPY_ASSORT_TO_ALL.getPermissionAlias());
            copyAssortToAll.setProjectCode("MRP");
            session.persist(copyAssortToAll);
        } else {
            copyAssortToAll.setName("Seçilen modelin asortilerini tüm modellere uygular.");
            copyAssortToAll.setCode(PermissionEnum.COPY_ASSORT_TO_ALL.getPermissionAlias());
            copyAssortToAll.setProjectCode("MRP");
            session.persist(copyAssortToAll);
        }

        Permission copyPrice = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.COPY_PRICE.getPermissionAlias())).uniqueResult();

        if (copyPrice == null) {
            copyPrice = new Permission();
            copyPrice.setName("Aynı sistem kodlu modellere, seçilen modelin fiyatlarını kopyalayabilir.");
            copyPrice.setCode(PermissionEnum.COPY_PRICE.getPermissionAlias());
            copyPrice.setProjectCode("MRP");
            session.persist(copyPrice);
        } else {
            copyPrice.setName("Aynı sistem kodlu modellere, seçilen modelin fiyatlarını kopyalayabilir.");
            copyPrice.setCode(PermissionEnum.COPY_PRICE.getPermissionAlias());
            copyPrice.setProjectCode("MRP");
            session.persist(copyPrice);
        }
        //Department CREATE
        Permission departmentCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.DEPARTMENT_CREATE.getPermissionAlias())).uniqueResult();

        if (departmentCreate == null) {
            departmentCreate = new Permission();
            departmentCreate.setName("Departman oluşturma izni verir.");
            departmentCreate.setCode(PermissionEnum.DEPARTMENT_CREATE.getPermissionAlias());
            departmentCreate.setProjectCode("MRP");
            session.persist(departmentCreate);
        } else {
            departmentCreate.setName("Departman oluşturma izni verir.");
            departmentCreate.setCode(PermissionEnum.DEPARTMENT_CREATE.getPermissionAlias());
            departmentCreate.setProjectCode("MRP");
            session.persist(departmentCreate);
        }

        //Department READ
        Permission departmentRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.DEPARTMENT_READ.getPermissionAlias())).uniqueResult();

        if (departmentRead == null) {
            departmentRead = new Permission();
            departmentRead.setName("Departman görüntüleme izni verir.");
            departmentRead.setCode(PermissionEnum.DEPARTMENT_READ.getPermissionAlias());
            departmentRead.setProjectCode("MRP");
            session.persist(departmentRead);
        } else {
            departmentRead.setName("Departman görüntüleme izni verir.");
            departmentRead.setCode(PermissionEnum.DEPARTMENT_READ.getPermissionAlias());
            departmentRead.setProjectCode("MRP");
            session.persist(departmentRead);
        }

        //Department UPDATE
        Permission departmentUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.DEPARTMENT_UPDATE.getPermissionAlias())).uniqueResult();

        if (departmentUpdate == null) {
            departmentUpdate = new Permission();
            departmentUpdate.setName("Departman güncelleme izni verir.");
            departmentUpdate.setCode(PermissionEnum.DEPARTMENT_UPDATE.getPermissionAlias());
            departmentUpdate.setProjectCode("MRP");
            session.persist(departmentUpdate);
        } else {
            departmentUpdate.setName("Departman güncelleme izni verir.");
            departmentUpdate.setCode(PermissionEnum.DEPARTMENT_UPDATE.getPermissionAlias());
            departmentUpdate.setProjectCode("MRP");
            session.persist(departmentUpdate);
        }

        //Department DELETE
        Permission departmentDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.DEPARTMENT_DELETE.getPermissionAlias())).uniqueResult();

        if (departmentDelete == null) {
            departmentDelete = new Permission();
            departmentDelete.setName("Departman silme izni verir.");
            departmentDelete.setCode(PermissionEnum.DEPARTMENT_DELETE.getPermissionAlias());
            departmentDelete.setProjectCode("MRP");
            session.persist(departmentDelete);
        } else {
            departmentDelete.setName("Departman silme izni verir.");
            departmentDelete.setCode(PermissionEnum.DEPARTMENT_DELETE.getPermissionAlias());
            departmentDelete.setProjectCode("MRP");
            session.persist(departmentDelete);
        }

        //Staff CREATE
        Permission staffCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STAFF_CREATE.getPermissionAlias())).uniqueResult();

        if (staffCreate == null) {
            staffCreate = new Permission();
            staffCreate.setName("Çalışan oluşturma izni verir.");
            staffCreate.setCode(PermissionEnum.STAFF_CREATE.getPermissionAlias());
            staffCreate.setProjectCode("MRP");
            session.persist(staffCreate);
        } else {
            staffCreate.setName("Çalışan oluşturma izni verir.");
            staffCreate.setCode(PermissionEnum.STAFF_CREATE.getPermissionAlias());
            staffCreate.setProjectCode("MRP");
            session.persist(staffCreate);
        }

        //Staff READ
        Permission staffRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STAFF_READ.getPermissionAlias())).uniqueResult();

        if (staffRead == null) {
            staffRead = new Permission();
            staffRead.setName("Çalışan görüntüleme izni verir.");
            staffRead.setCode(PermissionEnum.STAFF_READ.getPermissionAlias());
            staffRead.setProjectCode("MRP");
            session.persist(staffRead);
        } else {
            staffRead.setName("Çalışan görüntüleme izni verir.");
            staffRead.setCode(PermissionEnum.STAFF_READ.getPermissionAlias());
            staffRead.setProjectCode("MRP");
            session.persist(staffRead);
        }

        //Staff UPDATE
        Permission staffUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STAFF_UPDATE.getPermissionAlias())).uniqueResult();

        if (staffUpdate == null) {
            staffUpdate = new Permission();
            staffUpdate.setName("Çalışan güncelleme izni verir.");
            staffUpdate.setCode(PermissionEnum.STAFF_UPDATE.getPermissionAlias());
            staffUpdate.setProjectCode("MRP");
            session.persist(staffUpdate);
        } else {
            staffUpdate.setName("Çalışan güncelleme izni verir.");
            staffUpdate.setCode(PermissionEnum.STAFF_UPDATE.getPermissionAlias());
            staffUpdate.setProjectCode("MRP");
            session.persist(staffUpdate);
        }

        //Staff DELETE
        Permission staffDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.STAFF_DELETE.getPermissionAlias())).uniqueResult();

        if (staffDelete == null) {
            staffDelete = new Permission();
            staffDelete.setName("Çalışan silme izni verir.");
            staffDelete.setCode(PermissionEnum.STAFF_DELETE.getPermissionAlias());
            staffDelete.setProjectCode("MRP");
            session.persist(staffDelete);
        } else {
            staffDelete.setName("Çalışan silme izni verir.");
            staffDelete.setCode(PermissionEnum.STAFF_DELETE.getPermissionAlias());
            staffDelete.setProjectCode("MRP");
            session.persist(staffDelete);
        }

        //ContractedManufacturer CREATE
        Permission contractedManufacturerCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CONTRACTED_MANUFACTURER_CREATE.getPermissionAlias())).uniqueResult();

        if (contractedManufacturerCreate == null) {
            contractedManufacturerCreate = new Permission();
            contractedManufacturerCreate.setName("Fasoncu oluşturma izni verir.");
            contractedManufacturerCreate.setCode(PermissionEnum.CONTRACTED_MANUFACTURER_CREATE.getPermissionAlias());
            contractedManufacturerCreate.setProjectCode("MRP");
            session.persist(contractedManufacturerCreate);
        } else {
            contractedManufacturerCreate.setName("Fasoncu oluşturma izni verir.");
            contractedManufacturerCreate.setCode(PermissionEnum.CONTRACTED_MANUFACTURER_CREATE.getPermissionAlias());
            contractedManufacturerCreate.setProjectCode("MRP");
            session.persist(contractedManufacturerCreate);
        }

        //ContractedManufacturer READ
        Permission contractedManufacturerRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CONTRACTED_MANUFACTURER_READ.getPermissionAlias())).uniqueResult();

        if (contractedManufacturerRead == null) {
            contractedManufacturerRead = new Permission();
            contractedManufacturerRead.setName("Fasoncu görüntüleme izni verir.");
            contractedManufacturerRead.setCode(PermissionEnum.CONTRACTED_MANUFACTURER_READ.getPermissionAlias());
            contractedManufacturerRead.setProjectCode("MRP");
            session.persist(contractedManufacturerRead);
        } else {
            contractedManufacturerRead.setName("Fasoncu görüntüleme izni verir.");
            contractedManufacturerRead.setCode(PermissionEnum.CONTRACTED_MANUFACTURER_READ.getPermissionAlias());
            contractedManufacturerRead.setProjectCode("MRP");
            session.persist(contractedManufacturerRead);
        }

        //ContractedManufacturer UPDATE
        Permission contractedManufacturerUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CONTRACTED_MANUFACTURER_UPDATE.getPermissionAlias())).uniqueResult();

        if (contractedManufacturerUpdate == null) {
            contractedManufacturerUpdate = new Permission();
            contractedManufacturerUpdate.setName("Fasoncu güncelleme izni verir.");
            contractedManufacturerUpdate.setCode(PermissionEnum.CONTRACTED_MANUFACTURER_UPDATE.getPermissionAlias());
            contractedManufacturerUpdate.setProjectCode("MRP");
            session.persist(contractedManufacturerUpdate);
        } else {
            contractedManufacturerUpdate.setName("Fasoncu güncelleme izni verir.");
            contractedManufacturerUpdate.setCode(PermissionEnum.CONTRACTED_MANUFACTURER_UPDATE.getPermissionAlias());
            contractedManufacturerUpdate.setProjectCode("MRP");
            session.persist(contractedManufacturerUpdate);
        }

        //ContractedManufacturer DELETE
        Permission contractedManufacturerDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CONTRACTED_MANUFACTURER_DELETE.getPermissionAlias())).uniqueResult();

        if (contractedManufacturerDelete == null) {
            contractedManufacturerDelete = new Permission();
            contractedManufacturerDelete.setName("Fasoncu silme izni verir.");
            contractedManufacturerDelete.setCode(PermissionEnum.CONTRACTED_MANUFACTURER_DELETE.getPermissionAlias());
            contractedManufacturerDelete.setProjectCode("MRP");
            session.persist(contractedManufacturerDelete);
        } else {
            contractedManufacturerDelete.setName("Fasoncu silme izni verir.");
            contractedManufacturerDelete.setCode(PermissionEnum.CONTRACTED_MANUFACTURER_DELETE.getPermissionAlias());
            contractedManufacturerDelete.setProjectCode("MRP");
            session.persist(contractedManufacturerDelete);
        }

        //Processing CREATE
        Permission processingCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PROCESSING_CREATE.getPermissionAlias())).uniqueResult();

        if (processingCreate == null) {
            processingCreate = new Permission();
            processingCreate.setName("Proses oluşturma izni verir.");
            processingCreate.setCode(PermissionEnum.PROCESSING_CREATE.getPermissionAlias());
            processingCreate.setProjectCode("MRP");
            session.persist(processingCreate);
        } else {
            processingCreate.setName("Proses oluşturma izni verir.");
            processingCreate.setCode(PermissionEnum.PROCESSING_CREATE.getPermissionAlias());
            processingCreate.setProjectCode("MRP");
            session.persist(processingCreate);
        }

        //Processing READ
        Permission processingRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PROCESSING_READ.getPermissionAlias())).uniqueResult();

        if (processingRead == null) {
            processingRead = new Permission();
            processingRead.setName("Proses görüntüleme izni verir.");
            processingRead.setCode(PermissionEnum.PROCESSING_READ.getPermissionAlias());
            processingRead.setProjectCode("MRP");
            session.persist(processingRead);
        } else {
            processingRead.setName("Proses görüntüleme izni verir.");
            processingRead.setCode(PermissionEnum.PROCESSING_READ.getPermissionAlias());
            processingRead.setProjectCode("MRP");
            session.persist(processingRead);
        }

        //Processing UPDATE
        Permission processingUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PROCESSING_UPDATE.getPermissionAlias())).uniqueResult();

        if (processingUpdate == null) {
            processingUpdate = new Permission();
            processingUpdate.setName("Proses güncelleme izni verir.");
            processingUpdate.setCode(PermissionEnum.PROCESSING_UPDATE.getPermissionAlias());
            processingUpdate.setProjectCode("MRP");
            session.persist(processingUpdate);
        } else {
            processingUpdate.setName("Proses güncelleme izni verir.");
            processingUpdate.setCode(PermissionEnum.PROCESSING_UPDATE.getPermissionAlias());
            processingUpdate.setProjectCode("MRP");
            session.persist(processingUpdate);
        }

        //Processing DELETE
        Permission processingDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PROCESSING_DELETE.getPermissionAlias())).uniqueResult();

        if (processingDelete == null) {
            processingDelete = new Permission();
            processingDelete.setName("Proses silme izni verir.");
            processingDelete.setCode(PermissionEnum.PROCESSING_DELETE.getPermissionAlias());
            processingDelete.setProjectCode("MRP");
            session.persist(processingDelete);
        } else {
            processingDelete.setName("Proses silme izni verir.");
            processingDelete.setCode(PermissionEnum.PROCESSING_DELETE.getPermissionAlias());
            processingDelete.setProjectCode("MRP");
            session.persist(processingDelete);
        }

        //ModelProcessing CREATE
        Permission modelProcessingCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_CREATE.getPermissionAlias())).uniqueResult();

        if (modelProcessingCreate == null) {
            modelProcessingCreate = new Permission();
            modelProcessingCreate.setName("Proses Emri oluşturma izni verir.");
            modelProcessingCreate.setCode(PermissionEnum.MODEL_PROCESSING_CREATE.getPermissionAlias());
            modelProcessingCreate.setProjectCode("MRP");
            session.persist(modelProcessingCreate);
        } else {
            modelProcessingCreate.setName("Proses Emri oluşturma izni verir.");
            modelProcessingCreate.setCode(PermissionEnum.MODEL_PROCESSING_CREATE.getPermissionAlias());
            modelProcessingCreate.setProjectCode("MRP");
            session.persist(modelProcessingCreate);
        }

        //ModelProcessing READ
        Permission modelProcessingRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_READ.getPermissionAlias())).uniqueResult();

        if (modelProcessingRead == null) {
            modelProcessingRead = new Permission();
            modelProcessingRead.setName("Proses Emri görüntüleme izni verir.");
            modelProcessingRead.setCode(PermissionEnum.MODEL_PROCESSING_READ.getPermissionAlias());
            modelProcessingRead.setProjectCode("MRP");
            session.persist(modelProcessingRead);
        } else {
            modelProcessingRead.setName("Proses Emri görüntüleme izni verir.");
            modelProcessingRead.setCode(PermissionEnum.MODEL_PROCESSING_READ.getPermissionAlias());
            modelProcessingRead.setProjectCode("MRP");
            session.persist(modelProcessingRead);
        }

        //ModelProcessing UPDATE
        Permission modelProcessingUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_UPDATE.getPermissionAlias())).uniqueResult();

        if (modelProcessingUpdate == null) {
            modelProcessingUpdate = new Permission();
            modelProcessingUpdate.setName("Proses Emri güncelleme izni verir.");
            modelProcessingUpdate.setCode(PermissionEnum.MODEL_PROCESSING_UPDATE.getPermissionAlias());
            modelProcessingUpdate.setProjectCode("MRP");
            session.persist(modelProcessingUpdate);
        } else {
            modelProcessingUpdate.setName("Proses Emri güncelleme izni verir.");
            modelProcessingUpdate.setCode(PermissionEnum.MODEL_PROCESSING_UPDATE.getPermissionAlias());
            modelProcessingUpdate.setProjectCode("MRP");
            session.persist(modelProcessingUpdate);
        }

        //ModelProcessing DELETE
        Permission modelProcessingDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_DELETE.getPermissionAlias())).uniqueResult();

        if (modelProcessingDelete == null) {
            modelProcessingDelete = new Permission();
            modelProcessingDelete.setName("Proses Emri silme izni verir.");
            modelProcessingDelete.setCode(PermissionEnum.MODEL_PROCESSING_DELETE.getPermissionAlias());
            modelProcessingDelete.setProjectCode("MRP");
            session.persist(modelProcessingDelete);
        } else {
            modelProcessingDelete.setName("Proses Emri silme izni verir.");
            modelProcessingDelete.setCode(PermissionEnum.MODEL_PROCESSING_DELETE.getPermissionAlias());
            modelProcessingDelete.setProjectCode("MRP");
            session.persist(modelProcessingDelete);
        }

        //ModelProcessingIncoming CREATE
        Permission modelProcessingIncomingCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_INCOMING_CREATE.getPermissionAlias())).uniqueResult();

        if (modelProcessingIncomingCreate == null) {
            modelProcessingIncomingCreate = new Permission();
            modelProcessingIncomingCreate.setName("Gelen Proses Emri oluşturma izni verir.");
            modelProcessingIncomingCreate.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_CREATE.getPermissionAlias());
            modelProcessingIncomingCreate.setProjectCode("MRP");
            session.persist(modelProcessingIncomingCreate);
        } else {
            modelProcessingIncomingCreate.setName("Gelen Proses Emri oluşturma izni verir.");
            modelProcessingIncomingCreate.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_CREATE.getPermissionAlias());
            modelProcessingIncomingCreate.setProjectCode("MRP");
            session.persist(modelProcessingIncomingCreate);
        }

        //ModelProcessingIncoming READ
        Permission modelProcessingIncomingRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_INCOMING_READ.getPermissionAlias())).uniqueResult();

        if (modelProcessingIncomingRead == null) {
            modelProcessingIncomingRead = new Permission();
            modelProcessingIncomingRead.setName("Gelen Proses Emri görüntüleme izni verir.");
            modelProcessingIncomingRead.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_READ.getPermissionAlias());
            modelProcessingIncomingRead.setProjectCode("MRP");
            session.persist(modelProcessingIncomingRead);
        } else {
            modelProcessingIncomingRead.setName("Gelen Proses Emri görüntüleme izni verir.");
            modelProcessingIncomingRead.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_READ.getPermissionAlias());
            modelProcessingIncomingRead.setProjectCode("MRP");
            session.persist(modelProcessingIncomingRead);
        }

        //ModelProcessingIncoming UPDATE
        Permission modelProcessingIncomingUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_INCOMING_UPDATE.getPermissionAlias())).uniqueResult();

        if (modelProcessingIncomingUpdate == null) {
            modelProcessingIncomingUpdate = new Permission();
            modelProcessingIncomingUpdate.setName("Gelen Proses Emri güncelleme izni verir.");
            modelProcessingIncomingUpdate.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_UPDATE.getPermissionAlias());
            modelProcessingIncomingUpdate.setProjectCode("MRP");
            session.persist(modelProcessingIncomingUpdate);
        } else {
            modelProcessingIncomingUpdate.setName("Gelen Proses Emri güncelleme izni verir.");
            modelProcessingIncomingUpdate.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_UPDATE.getPermissionAlias());
            modelProcessingIncomingUpdate.setProjectCode("MRP");
            session.persist(modelProcessingIncomingUpdate);
        }

        //ModelProcessingIncoming DELETE
        Permission modelProcessingIncomingDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MODEL_PROCESSING_INCOMING_DELETE.getPermissionAlias())).uniqueResult();

        if (modelProcessingIncomingDelete == null) {
            modelProcessingIncomingDelete = new Permission();
            modelProcessingIncomingDelete.setName("Gelen Proses Emri silme izni verir.");
            modelProcessingIncomingDelete.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_DELETE.getPermissionAlias());
            modelProcessingIncomingDelete.setProjectCode("MRP");
            session.persist(modelProcessingIncomingDelete);
        } else {
            modelProcessingIncomingDelete.setName("Gelen Proses Emri silme izni verir.");
            modelProcessingIncomingDelete.setCode(PermissionEnum.MODEL_PROCESSING_INCOMING_DELETE.getPermissionAlias());
            modelProcessingIncomingDelete.setProjectCode("MRP");
            session.persist(modelProcessingIncomingDelete);
        }

        Permission fairOrderCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_ORDER_CREATE.getPermissionAlias())).uniqueResult();

        if (fairOrderCreate == null) {
            fairOrderCreate = new Permission();
            fairOrderCreate.setName("Fuar siparişi oluşturabilir.");
            fairOrderCreate.setCode(PermissionEnum.FAIR_ORDER_CREATE.getPermissionAlias());
            fairOrderCreate.setProjectCode("MRP");
            session.persist(fairOrderCreate);
        } else {
            fairOrderCreate.setName("Fuar siparişi oluşturabilir.");
            fairOrderCreate.setCode(PermissionEnum.FAIR_ORDER_CREATE.getPermissionAlias());
            fairOrderCreate.setProjectCode("MRP");
            session.persist(fairOrderCreate);
        }

        Permission fairOrderRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_ORDER_READ.getPermissionAlias())).uniqueResult();

        if (fairOrderRead == null) {
            fairOrderRead = new Permission();
            fairOrderRead.setName("Fuar siparişi görüntüleyebilir.");
            fairOrderRead.setCode(PermissionEnum.FAIR_ORDER_READ.getPermissionAlias());
            fairOrderRead.setProjectCode("MRP");
            session.persist(fairOrderRead);
        } else {
            fairOrderRead.setName("Fuar siparişi görüntüleyebilir.");
            fairOrderRead.setCode(PermissionEnum.FAIR_ORDER_READ.getPermissionAlias());
            fairOrderRead.setProjectCode("MRP");
            session.persist(fairOrderRead);
        }

        Permission fairOrderUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_ORDER_UPDATE.getPermissionAlias())).uniqueResult();

        if (fairOrderUpdate == null) {
            fairOrderUpdate = new Permission();
            fairOrderUpdate.setName("Fuar siparişi güncelleyebilir.");
            fairOrderUpdate.setCode(PermissionEnum.FAIR_ORDER_UPDATE.getPermissionAlias());
            fairOrderUpdate.setProjectCode("MRP");
            session.persist(fairOrderUpdate);
        } else {
            fairOrderUpdate.setName("Fuar siparişi güncelleyebilir.");
            fairOrderUpdate.setCode(PermissionEnum.FAIR_ORDER_UPDATE.getPermissionAlias());
            fairOrderUpdate.setProjectCode("MRP");
            session.persist(fairOrderUpdate);
        }

        Permission fairOrderDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.FAIR_ORDER_DELETE.getPermissionAlias())).uniqueResult();

        if (fairOrderDelete == null) {
            fairOrderDelete = new Permission();
            fairOrderDelete.setName("Fuar siparişi silebilir.");
            fairOrderDelete.setCode(PermissionEnum.FAIR_ORDER_DELETE.getPermissionAlias());
            fairOrderDelete.setProjectCode("MRP");
            session.persist(fairOrderDelete);
        } else {
            fairOrderDelete.setName("Fuar siparişi silebilir.");
            fairOrderDelete.setCode(PermissionEnum.FAIR_ORDER_DELETE.getPermissionAlias());
            fairOrderDelete.setProjectCode("MRP");
            session.persist(fairOrderDelete);
        }

        //ExtraExpenses CREATE
        Permission extraExpensesCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.EXTRA_EXPENSES_CREATE.getPermissionAlias())).uniqueResult();

        if (extraExpensesCreate == null) {
            extraExpensesCreate = new Permission();
            extraExpensesCreate.setName("Giderler oluşturma izni verir.");
            extraExpensesCreate.setCode(PermissionEnum.EXTRA_EXPENSES_CREATE.getPermissionAlias());
            extraExpensesCreate.setProjectCode("MRP");
            session.persist(extraExpensesCreate);
        } else {
            extraExpensesCreate.setName("Giderler oluşturma izni verir.");
            extraExpensesCreate.setCode(PermissionEnum.EXTRA_EXPENSES_CREATE.getPermissionAlias());
            extraExpensesCreate.setProjectCode("MRP");
            session.persist(extraExpensesCreate);
        }

        //ExtraExpenses READ
        Permission extraExpensesRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.EXTRA_EXPENSES_READ.getPermissionAlias())).uniqueResult();

        if (extraExpensesRead == null) {
            extraExpensesRead = new Permission();
            extraExpensesRead.setName("Giderler görüntüleme izni verir.");
            extraExpensesRead.setCode(PermissionEnum.EXTRA_EXPENSES_READ.getPermissionAlias());
            extraExpensesRead.setProjectCode("MRP");
            session.persist(extraExpensesRead);
        } else {
            extraExpensesRead.setName("Giderler görüntüleme izni verir.");
            extraExpensesRead.setCode(PermissionEnum.EXTRA_EXPENSES_READ.getPermissionAlias());
            extraExpensesRead.setProjectCode("MRP");
            session.persist(extraExpensesRead);
        }

        //ExtraExpenses UPDATE
        Permission extraExpensesUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.EXTRA_EXPENSES_UPDATE.getPermissionAlias())).uniqueResult();

        if (extraExpensesUpdate == null) {
            extraExpensesUpdate = new Permission();
            extraExpensesUpdate.setName("Giderler güncelleme izni verir.");
            extraExpensesUpdate.setCode(PermissionEnum.EXTRA_EXPENSES_UPDATE.getPermissionAlias());
            extraExpensesUpdate.setProjectCode("MRP");
            session.persist(extraExpensesUpdate);
        } else {
            extraExpensesUpdate.setName("Giderler güncelleme izni verir.");
            extraExpensesUpdate.setCode(PermissionEnum.EXTRA_EXPENSES_UPDATE.getPermissionAlias());
            extraExpensesUpdate.setProjectCode("MRP");
            session.persist(extraExpensesUpdate);
        }

        //ExtraExpenses DELETE
        Permission extraExpensesDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.EXTRA_EXPENSES_DELETE.getPermissionAlias())).uniqueResult();

        if (extraExpensesDelete == null) {
            extraExpensesDelete = new Permission();
            extraExpensesDelete.setName("Giderler silme izni verir.");
            extraExpensesDelete.setCode(PermissionEnum.EXTRA_EXPENSES_DELETE.getPermissionAlias());
            extraExpensesDelete.setProjectCode("MRP");
            session.persist(extraExpensesDelete);
        } else {
            extraExpensesDelete.setName("Giderler silme izni verir.");
            extraExpensesDelete.setCode(PermissionEnum.EXTRA_EXPENSES_DELETE.getPermissionAlias());
            extraExpensesDelete.setProjectCode("MRP");
            session.persist(extraExpensesDelete);
        }


        //CuttingRequirement READ
        Permission cuttingRequirementRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.CUTTING_REQUIREMENT_READ.getPermissionAlias())).uniqueResult();

        if (cuttingRequirementRead == null) {
            cuttingRequirementRead = new Permission();
            cuttingRequirementRead.setName("Kesim emir görüntüleme izni verir.");
            cuttingRequirementRead.setCode(PermissionEnum.CUTTING_REQUIREMENT_READ.getPermissionAlias());
            cuttingRequirementRead.setProjectCode("MRP");
            session.persist(cuttingRequirementRead);
        } else {
            cuttingRequirementRead.setName("Kesim emir görüntüleme izni verir.");
            cuttingRequirementRead.setCode(PermissionEnum.CUTTING_REQUIREMENT_READ.getPermissionAlias());
            cuttingRequirementRead.setProjectCode("MRP");
            session.persist(cuttingRequirementRead);
        }


        //MontaRequirement READ
        Permission montaRequirementRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.MONTA_REQUIREMENT_READ.getPermissionAlias())).uniqueResult();

        if (montaRequirementRead == null) {
            montaRequirementRead = new Permission();
            montaRequirementRead.setName("Monta emir görüntüleme izni verir.");
            montaRequirementRead.setCode(PermissionEnum.MONTA_REQUIREMENT_READ.getPermissionAlias());
            montaRequirementRead.setProjectCode("MRP");
            session.persist(montaRequirementRead);
        } else {
            montaRequirementRead.setName("Monta emir görüntüleme izni verir.");
            montaRequirementRead.setCode(PermissionEnum.MONTA_REQUIREMENT_READ.getPermissionAlias());
            montaRequirementRead.setProjectCode("MRP");
            session.persist(montaRequirementRead);
        }

//WAREHOUSE CREATE
        Permission warehouseCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.WAREHOUSE_CREATE.getPermissionAlias())).uniqueResult();

        if (warehouseCreate == null) {
            warehouseCreate = new Permission();
            warehouseCreate.setName("Depo oluşturma izni verir.");
            warehouseCreate.setCode(PermissionEnum.WAREHOUSE_CREATE.getPermissionAlias());
            warehouseCreate.setProjectCode("MRP");
            session.persist(warehouseCreate);
        } else {
            warehouseCreate.setName("Depo oluşturma izni verir.");
            warehouseCreate.setCode(PermissionEnum.WAREHOUSE_CREATE.getPermissionAlias());
            warehouseCreate.setProjectCode("MRP");
            session.persist(warehouseCreate);
        }

        //WAREHOUSE READ
        Permission warehouseRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.WAREHOUSE_READ.getPermissionAlias())).uniqueResult();

        if (warehouseRead == null) {
            warehouseRead = new Permission();
            warehouseRead.setName("Depo görüntüleme izni verir.");
            warehouseRead.setCode(PermissionEnum.WAREHOUSE_READ.getPermissionAlias());
            warehouseRead.setProjectCode("MRP");
            session.persist(warehouseRead);
        } else {
            warehouseRead.setName("Depo görüntüleme izni verir.");
            warehouseRead.setCode(PermissionEnum.WAREHOUSE_READ.getPermissionAlias());
            warehouseRead.setProjectCode("MRP");
            session.persist(warehouseRead);
        }

        //WAREHOUSE UPDATE
        Permission warehouseUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.WAREHOUSE_UPDATE.getPermissionAlias())).uniqueResult();

        if (warehouseUpdate == null) {
            warehouseUpdate = new Permission();
            warehouseUpdate.setName("Depo güncelleme izni verir.");
            warehouseUpdate.setCode(PermissionEnum.WAREHOUSE_UPDATE.getPermissionAlias());
            warehouseUpdate.setProjectCode("MRP");
            session.persist(warehouseUpdate);
        } else {
            warehouseUpdate.setName("Depo güncelleme izni verir.");
            warehouseUpdate.setCode(PermissionEnum.WAREHOUSE_UPDATE.getPermissionAlias());
            warehouseUpdate.setProjectCode("MRP");
            session.persist(warehouseUpdate);
        }

        //WAREHOUSE DELETE
        Permission warehouseDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.WAREHOUSE_DELETE.getPermissionAlias())).uniqueResult();

        if (warehouseDelete == null) {
            warehouseDelete = new Permission();
            warehouseDelete.setName("Depo silme izni verir.");
            warehouseDelete.setCode(PermissionEnum.WAREHOUSE_DELETE.getPermissionAlias());
            warehouseDelete.setProjectCode("MRP");
            session.persist(warehouseDelete);
        } else {
            warehouseDelete.setName("Depo silme izni verir.");
            warehouseDelete.setCode(PermissionEnum.WAREHOUSE_DELETE.getPermissionAlias());
            warehouseDelete.setProjectCode("MRP");
            session.persist(warehouseDelete);
        }

        //Purcase Order
        Permission poCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PURCASE_ORDER_CREATE.getPermissionAlias())).uniqueResult();

        if (poCreate == null) {
            poCreate = new Permission();
            poCreate.setName("Satınalma emri oluşturabilir.");
            poCreate.setCode(PermissionEnum.PURCASE_ORDER_CREATE.getPermissionAlias());
            poCreate.setProjectCode("MRP");
            session.persist(poCreate);
        } else {
            poCreate.setName("Satınalma emri oluşturabilir.");
            poCreate.setCode(PermissionEnum.PURCASE_ORDER_CREATE.getPermissionAlias());
            poCreate.setProjectCode("MRP");
            session.persist(poCreate);
        }

        Permission poRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PURCASE_ORDER_READ.getPermissionAlias())).uniqueResult();

        if (poRead == null) {
            poRead = new Permission();
            poRead.setName("Satınalma emri görüntüleyebilir.");
            poRead.setCode(PermissionEnum.PURCASE_ORDER_READ.getPermissionAlias());
            poRead.setProjectCode("MRP");
            session.persist(poRead);
        } else {
            poRead.setName("Satınalma emri görüntüleyebilir.");
            poRead.setCode(PermissionEnum.PURCASE_ORDER_READ.getPermissionAlias());
            poRead.setProjectCode("MRP");
            session.persist(poRead);
        }

        Permission poUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PURCASE_ORDER_UPDATE.getPermissionAlias())).uniqueResult();

        if (poUpdate == null) {
            poUpdate = new Permission();
            poUpdate.setName("Satınalma emri güncelleyebilir.");
            poUpdate.setCode(PermissionEnum.PURCASE_ORDER_UPDATE.getPermissionAlias());
            poUpdate.setProjectCode("MRP");
            session.persist(poUpdate);
        } else {
            poUpdate.setName("Satınalma emri güncelleyebilir.");
            poUpdate.setCode(PermissionEnum.PURCASE_ORDER_UPDATE.getPermissionAlias());
            poUpdate.setProjectCode("MRP");
            session.persist(poUpdate);
        }

        Permission poDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.PURCASE_ORDER_DELETE.getPermissionAlias())).uniqueResult();

        if (poDelete == null) {
            poDelete = new Permission();
            poDelete.setName("Satınalma emri silebilir.");
            poDelete.setCode(PermissionEnum.PURCASE_ORDER_DELETE.getPermissionAlias());
            poDelete.setProjectCode("MRP");
            session.persist(poDelete);
        } else {
            poDelete.setName("Satınalma emri silebilir.");
            poDelete.setCode(PermissionEnum.PURCASE_ORDER_DELETE.getPermissionAlias());
            poDelete.setProjectCode("MRP");
            session.persist(poDelete);
        }

        Permission shipmentOrderCreate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_ORDER_CREATE.getPermissionAlias())).uniqueResult();

        if (shipmentOrderCreate == null) {
            shipmentOrderCreate = new Permission();
            shipmentOrderCreate.setName("Sevkiyat emri oluşturabilir.");
            shipmentOrderCreate.setCode(PermissionEnum.SHIPMENT_ORDER_CREATE.getPermissionAlias());
            shipmentOrderCreate.setProjectCode("MRP");
            session.persist(shipmentOrderCreate);
        } else {
            shipmentOrderCreate.setName("Sevkiyat emri oluşturabilir.");
            shipmentOrderCreate.setCode(PermissionEnum.SHIPMENT_ORDER_CREATE.getPermissionAlias());
            shipmentOrderCreate.setProjectCode("MRP");
            session.persist(shipmentOrderCreate);
        }

        Permission shipmentOrderRead = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_ORDER_READ.getPermissionAlias())).uniqueResult();

        if (shipmentOrderRead == null) {
            shipmentOrderRead = new Permission();
            shipmentOrderRead.setName("Sevkiyat emri görüntüleyebilir.");
            shipmentOrderRead.setCode(PermissionEnum.SHIPMENT_ORDER_READ.getPermissionAlias());
            shipmentOrderRead.setProjectCode("MRP");
            session.persist(shipmentOrderRead);
        } else {
            shipmentOrderRead.setName("Sevkiyat emri görüntüleyebilir.");
            shipmentOrderRead.setCode(PermissionEnum.SHIPMENT_ORDER_READ.getPermissionAlias());
            shipmentOrderRead.setProjectCode("MRP");
            session.persist(shipmentOrderRead);
        }

        Permission shipmentOrderUpdate = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_ORDER_UPDATE.getPermissionAlias())).uniqueResult();

        if (shipmentOrderUpdate == null) {
            shipmentOrderUpdate = new Permission();
            shipmentOrderUpdate.setName("Sevkiyat emri güncelleyebilir.");
            shipmentOrderUpdate.setCode(PermissionEnum.SHIPMENT_ORDER_UPDATE.getPermissionAlias());
            shipmentOrderUpdate.setProjectCode("MRP");
            session.persist(shipmentOrderUpdate);
        } else {
            shipmentOrderUpdate.setName("Sevkiyat emri güncelleyebilir.");
            shipmentOrderUpdate.setCode(PermissionEnum.SHIPMENT_ORDER_UPDATE.getPermissionAlias());
            shipmentOrderUpdate.setProjectCode("MRP");
            session.persist(shipmentOrderUpdate);
        }

        Permission shipmentOrderDelete = (Permission) session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", PermissionEnum.SHIPMENT_ORDER_DELETE.getPermissionAlias())).uniqueResult();

        if (shipmentOrderDelete == null) {
            shipmentOrderDelete = new Permission();
            shipmentOrderDelete.setName("Sevkiyat emri silebilir.");
            shipmentOrderDelete.setCode(PermissionEnum.SHIPMENT_ORDER_DELETE.getPermissionAlias());
            shipmentOrderDelete.setProjectCode("MRP");
            session.persist(shipmentOrderDelete);
        } else {
            shipmentOrderDelete.setName("Sevkiyat emri silebilir.");
            shipmentOrderDelete.setCode(PermissionEnum.SHIPMENT_ORDER_DELETE.getPermissionAlias());
            shipmentOrderDelete.setProjectCode("MRP");
            session.persist(shipmentOrderDelete);
        }

        session.flush();
    }

    public void createMenu(Session session) {
        int counter = 0;
        Menu definitionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.DEFINITION_MAIN.getMenuAlias())).uniqueResult();

        if (definitionMenu == null) {
            definitionMenu = new Menu();
            definitionMenu.setName("Tanımlamalar");
            definitionMenu.setCode(MenuEnum.DEFINITION_MAIN.getMenuAlias());
            definitionMenu.setDescription("Tanımlama Ekranlarının en üst adımıdır.");
            definitionMenu.setMenuOrder(counter);
            definitionMenu.setProjectCode("MRP");
            counter++;
            session.persist(definitionMenu);
        } else {
            definitionMenu.setName("Tanımlamalar");
            definitionMenu.setCode(MenuEnum.DEFINITION_MAIN.getMenuAlias());
            definitionMenu.setDescription("Tanımlama Ekranlarının en üst adımıdır.");
            definitionMenu.setMenuOrder(counter);
            definitionMenu.setProjectCode("MRP");
            counter++;
            session.persist(definitionMenu);
        }
        Menu customerMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.CUSTOMER.getMenuAlias())).uniqueResult();

        if (customerMenu == null) {
            customerMenu = new Menu();
            customerMenu.setName("Müşteriler");
            customerMenu.setCode(MenuEnum.CUSTOMER.getMenuAlias());
            customerMenu.setDescription("Müşteri ekleme, güncelleme, silme ve arama sayfasıdır.");
            customerMenu.setUrl("musteri.html");
            customerMenu.setParentOid(definitionMenu.getOid());
            customerMenu.setMenuOrder(counter);
            customerMenu.setProjectCode("MRP");
            counter++;
            session.persist(customerMenu);
        } else {
            customerMenu.setName("Müşteriler");
            customerMenu.setCode(MenuEnum.CUSTOMER.getMenuAlias());
            customerMenu.setDescription("Müşteri ekleme, güncelleme, silme ve arama sayfasıdır.");
            customerMenu.setUrl("musteri.html");
            customerMenu.setParentOid(definitionMenu.getOid());
            customerMenu.setMenuOrder(counter);
            customerMenu.setProjectCode("MRP");
            counter++;
            session.persist(customerMenu);
        }
        Menu supplierMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SUPPLIER.getMenuAlias())).uniqueResult();

        if (supplierMenu == null) {
            supplierMenu = new Menu();
            supplierMenu.setName("Tedarikçiler");
            supplierMenu.setCode(MenuEnum.SUPPLIER.getMenuAlias());
            supplierMenu.setDescription("Tedarikçi ekleme, güncelleme, silme ve arama sayfasıdır.");
            supplierMenu.setUrl("tedarikci.html");
            supplierMenu.setParentOid(definitionMenu.getOid());
            supplierMenu.setMenuOrder(counter);
            supplierMenu.setProjectCode("MRP");
            counter++;
            session.persist(supplierMenu);
        } else {
            supplierMenu.setName("Tedarikçiler");
            supplierMenu.setCode(MenuEnum.SUPPLIER.getMenuAlias());
            supplierMenu.setDescription("Tedarikçi ekleme, güncelleme, silme ve arama sayfasıdır.");
            supplierMenu.setUrl("tedarikci.html");
            supplierMenu.setParentOid(definitionMenu.getOid());
            supplierMenu.setMenuOrder(counter);
            supplierMenu.setProjectCode("MRP");
            counter++;
            session.persist(supplierMenu);
        }

        Menu materialType = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.MATERIAL_TYPE.getMenuAlias())).uniqueResult();

        if (materialType == null) {
            materialType = new Menu();
            materialType.setName("Malzeme Tipi");
            materialType.setCode(MenuEnum.MATERIAL_TYPE.getMenuAlias());
            materialType.setDescription("Malzeme türü ekleme, güncelleme, silme ve arama sayfasıdır.");
            materialType.setUrl("malzeme-tipi.html");
            materialType.setParentOid(definitionMenu.getOid());
            materialType.setMenuOrder(counter);
            materialType.setProjectCode("MRP");
            counter++;
            session.persist(materialType);
        } else {
            materialType.setName("Malzeme Tipi");
            materialType.setCode(MenuEnum.MATERIAL_TYPE.getMenuAlias());
            materialType.setDescription("Malzeme türü  ekleme, güncelleme, silme ve arama sayfasıdır.");
            materialType.setUrl("malzeme-tipi.html");
            materialType.setParentOid(definitionMenu.getOid());
            materialType.setMenuOrder(counter);
            materialType.setProjectCode("MRP");
            counter++;
            session.persist(materialType);
        }
        Menu extraExpenses = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.EXTRA_EXPENSES.getMenuAlias())).uniqueResult();

        if (extraExpenses == null) {
            extraExpenses = new Menu();
            extraExpenses.setName("Giderler");
            extraExpenses.setCode(MenuEnum.EXTRA_EXPENSES.getMenuAlias());
            extraExpenses.setDescription("Giderler ekleme, güncelleme, silme ve arama sayfasıdır.");
            extraExpenses.setUrl("giderler.html");
            extraExpenses.setParentOid(definitionMenu.getOid());
            extraExpenses.setMenuOrder(counter);
            extraExpenses.setProjectCode("MRP");
            counter++;
            session.persist(extraExpenses);
        } else {
            extraExpenses.setName("Giderler");
            extraExpenses.setCode(MenuEnum.EXTRA_EXPENSES.getMenuAlias());
            extraExpenses.setDescription("Giderler  ekleme, güncelleme, silme ve arama sayfasıdır.");
            extraExpenses.setUrl("giderler.html");
            extraExpenses.setParentOid(definitionMenu.getOid());
            extraExpenses.setMenuOrder(counter);
            extraExpenses.setProjectCode("MRP");
            counter++;
            session.persist(extraExpenses);
        }
        Menu productionLine = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP"))
                .add(Restrictions.eq("code", MenuEnum.PRODUCTION_LINE.getMenuAlias())).uniqueResult();

        if (productionLine == null) {
            productionLine = new Menu();
            productionLine.setName("Üretim Hattı");
            productionLine.setCode(MenuEnum.PRODUCTION_LINE.getMenuAlias());
            productionLine.setDescription("Üretim hattı ekleme, güncelleme, silme ve arama sayfasıdır.");
            productionLine.setUrl("uretim-hatti.html");
            productionLine.setParentOid(definitionMenu.getOid());
            productionLine.setMenuOrder(counter);
            productionLine.setProjectCode("MRP");
            counter++;
            session.persist(productionLine);
        } else {
            productionLine.setName("Üretim Hattı");
            productionLine.setCode(MenuEnum.PRODUCTION_LINE.getMenuAlias());
            productionLine.setDescription("Üretim hattı ekleme, güncelleme, silme ve arama sayfasıdır.");
            productionLine.setUrl("uretim-hatti.html");
            productionLine.setParentOid(definitionMenu.getOid());
            productionLine.setMenuOrder(counter);
            productionLine.setProjectCode("MRP");
            counter++;
            session.persist(productionLine);
        }

        Menu internalCritical = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.INTERNAL_CRITICAL.getMenuAlias())).uniqueResult();

        if (internalCritical == null) {
            internalCritical = new Menu();
            internalCritical.setName("İç Kritik");
            internalCritical.setCode(MenuEnum.INTERNAL_CRITICAL.getMenuAlias());
            internalCritical.setDescription("İç kritik ekleme, güncelleme, silme ve arama sayfasıdır.");
            internalCritical.setUrl("ic-kritik.html");
            internalCritical.setParentOid(definitionMenu.getOid());
            internalCritical.setMenuOrder(counter);
            internalCritical.setProjectCode("MRP");
            counter++;
            session.persist(internalCritical);
        } else {
            internalCritical.setName("İç Kritik");
            internalCritical.setCode(MenuEnum.INTERNAL_CRITICAL.getMenuAlias());
            internalCritical.setDescription("İç kritik  ekleme, güncelleme, silme ve arama sayfasıdır.");
            internalCritical.setUrl("ic-kritik.html");
            internalCritical.setParentOid(definitionMenu.getOid());
            internalCritical.setMenuOrder(counter);
            internalCritical.setProjectCode("MRP");
            counter++;
            session.persist(internalCritical);
        }

        Menu sayaci = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SAYACI.getMenuAlias())).uniqueResult();

        if (sayaci == null) {
            sayaci = new Menu();
            sayaci.setName("Sayacı");
            sayaci.setCode(MenuEnum.SAYACI.getMenuAlias());
            sayaci.setDescription("Sayacı ekleme, güncelleme, silme ve arama sayfasıdır.");
            sayaci.setUrl("sayaci.html");
            sayaci.setParentOid(definitionMenu.getOid());
            sayaci.setMenuOrder(counter);
            sayaci.setProjectCode("MRP");
            counter++;
            session.persist(sayaci);
        } else {
            sayaci.setName("Sayacı");
            sayaci.setCode(MenuEnum.SAYACI.getMenuAlias());
            sayaci.setDescription("Sayacı ekleme, güncelleme, silme ve arama sayfasıdır.");
            sayaci.setUrl("sayaci.html");
            sayaci.setParentOid(definitionMenu.getOid());
            sayaci.setMenuOrder(counter);
            sayaci.setProjectCode("MRP");
            counter++;
            session.persist(sayaci);
        }

        Menu shipmentInfo = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SHIPMENT_INFO.getMenuAlias())).uniqueResult();

        if (shipmentInfo == null) {
            shipmentInfo = new Menu();
            shipmentInfo.setName("Sevkiyat Bilgileri");
            shipmentInfo.setCode(MenuEnum.SHIPMENT_INFO.getMenuAlias());
            shipmentInfo.setDescription("Firmalara ait sevkiyat bilgisi ekleme, güncelleme, silme ve arama sayfasıdır.");
            shipmentInfo.setUrl("sevkiyat-bilgisi.html");
            shipmentInfo.setParentOid(definitionMenu.getOid());
            shipmentInfo.setMenuOrder(counter);
            shipmentInfo.setProjectCode("MRP");
            counter++;
            session.persist(shipmentInfo);
        } else {
            shipmentInfo.setName("Sevkiyat Bilgileri");
            shipmentInfo.setCode(MenuEnum.SHIPMENT_INFO.getMenuAlias());
            shipmentInfo.setDescription("Firmalara ait sevkiyat bilgisi ekleme, güncelleme, silme ve arama sayfasıdır.");
            shipmentInfo.setUrl("sevkiyat-bilgisi.html");
            shipmentInfo.setParentOid(definitionMenu.getOid());
            shipmentInfo.setMenuOrder(counter);
            shipmentInfo.setProjectCode("MRP");
            counter++;
            session.persist(shipmentInfo);
        }

//        todo fronttan karşılanınca açılacak
        Menu department = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.DEPARTMENT.getMenuAlias())).uniqueResult();

        if (department == null) {
            department = new Menu();
            department.setName("Departman");
            department.setCode(MenuEnum.DEPARTMENT.getMenuAlias());
            department.setDescription("Departman ekleme, güncelleme, silme ve arama sayfasıdır.");
            department.setUrl("departman.html");
            department.setParentOid(definitionMenu.getOid());
            department.setMenuOrder(counter);
            department.setProjectCode("MRP");
            counter++;
            session.persist(department);
        } else {
            department.setName("Departman");
            department.setCode(MenuEnum.DEPARTMENT.getMenuAlias());
            department.setDescription("Departman ekleme, güncelleme, silme ve arama sayfasıdır.");
            department.setUrl("departman.html");
            department.setParentOid(definitionMenu.getOid());
            department.setMenuOrder(counter);
            department.setProjectCode("MRP");
            counter++;
            session.persist(department);
        }
        Menu staff = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.STAFF.getMenuAlias())).uniqueResult();

        if (staff == null) {
            staff = new Menu();
            staff.setName("Çalışan");
            staff.setCode(MenuEnum.STAFF.getMenuAlias());
            staff.setDescription("Çalışan ekleme, güncelleme, silme ve arama sayfasıdır.");
            staff.setUrl("calisan.html");
            staff.setParentOid(definitionMenu.getOid());
            staff.setMenuOrder(counter);
            staff.setProjectCode("MRP");
            counter++;
            session.persist(staff);
        } else {
            staff.setName("Çalışan");
            staff.setCode(MenuEnum.STAFF.getMenuAlias());
            staff.setDescription("Çalışan ekleme, güncelleme, silme ve arama sayfasıdır.");
            staff.setUrl("calisan.html");
            staff.setParentOid(definitionMenu.getOid());
            staff.setMenuOrder(counter);
            staff.setProjectCode("MRP");
            counter++;
            session.persist(staff);
        }
        Menu contractedManufacturer = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.CONTRACTED_MANUFACTURER.getMenuAlias())).uniqueResult();

        if (contractedManufacturer == null) {
            contractedManufacturer = new Menu();
            contractedManufacturer.setName("Fasoncu");
            contractedManufacturer.setCode(MenuEnum.CONTRACTED_MANUFACTURER.getMenuAlias());
            contractedManufacturer.setDescription("Fasoncu ekleme, güncelleme, silme ve arama sayfasıdır.");
            contractedManufacturer.setUrl("fasoncu.html");
            contractedManufacturer.setParentOid(definitionMenu.getOid());
            contractedManufacturer.setMenuOrder(counter);
            contractedManufacturer.setProjectCode("MRP");
            counter++;
            session.persist(contractedManufacturer);
        } else {
            contractedManufacturer.setName("Fasoncu");
            contractedManufacturer.setCode(MenuEnum.CONTRACTED_MANUFACTURER.getMenuAlias());
            contractedManufacturer.setDescription("Fasoncu ekleme, güncelleme, silme ve arama sayfasıdır.");
            contractedManufacturer.setUrl("fasoncu.html");
            contractedManufacturer.setParentOid(definitionMenu.getOid());
            contractedManufacturer.setMenuOrder(counter);
            contractedManufacturer.setProjectCode("MRP");
            counter++;
            session.persist(contractedManufacturer);
        }

        Menu processing = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.PROCESSING.getMenuAlias())).uniqueResult();

        if (processing == null) {
            processing = new Menu();
            processing.setName("Proses");
            processing.setCode(MenuEnum.PROCESSING.getMenuAlias());
            processing.setDescription("Proses ekleme, güncelleme, silme ve arama sayfasıdır.");
            processing.setUrl("proses.html");
            processing.setParentOid(definitionMenu.getOid());
            processing.setMenuOrder(counter);
            processing.setProjectCode("MRP");
            counter++;
            session.persist(processing);
        } else {
            processing.setName("Proses");
            processing.setCode(MenuEnum.PROCESSING.getMenuAlias());
            processing.setDescription("Proses ekleme, güncelleme, silme ve arama sayfasıdır.");
            processing.setUrl("proses.html");
            processing.setParentOid(definitionMenu.getOid());
            processing.setMenuOrder(counter);
            processing.setProjectCode("MRP");
            counter++;
            session.persist(processing);
        }
        Menu warehouse = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.WAREHOUSE.getMenuAlias())).uniqueResult();

        if (warehouse == null) {
            warehouse = new Menu();
            warehouse.setName("Depo");
            warehouse.setCode(MenuEnum.WAREHOUSE.getMenuAlias());
            warehouse.setDescription("Depo ekleme, güncelleme, silme ve arama sayfasıdır.");
            warehouse.setUrl("depo.html");
            warehouse.setParentOid(definitionMenu.getOid());
            warehouse.setMenuOrder(counter);
            warehouse.setProjectCode("MRP");
            counter++;
            session.persist(warehouse);
        } else {
            warehouse.setName("Depo");
            warehouse.setCode(MenuEnum.WAREHOUSE.getMenuAlias());
            warehouse.setDescription("Depo ekleme, güncelleme, silme ve arama sayfasıdır.");
            warehouse.setUrl("depo.html");
            warehouse.setParentOid(definitionMenu.getOid());
            warehouse.setMenuOrder(counter);
            warehouse.setProjectCode("MRP");
            counter++;
            session.persist(warehouse);
        }


        Menu stockDefinitionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.STOCK_MAIN.getMenuAlias())).uniqueResult();

        if (stockDefinitionMenu == null) {
            stockDefinitionMenu = new Menu();
            stockDefinitionMenu.setName("Stok");
            stockDefinitionMenu.setCode(MenuEnum.STOCK_MAIN.getMenuAlias());
            stockDefinitionMenu.setDescription("Stok/Depo işlemleri en üst adımıdır.");
            stockDefinitionMenu.setMenuOrder(counter);
            stockDefinitionMenu.setProjectCode("MRP");
            counter++;
            session.persist(stockDefinitionMenu);
        } else {
            stockDefinitionMenu.setName("Stok");
            stockDefinitionMenu.setCode(MenuEnum.STOCK_MAIN.getMenuAlias());
            stockDefinitionMenu.setDescription("Stok/Depo işlemleri en üst adımıdır.");
            stockDefinitionMenu.setMenuOrder(counter);
            stockDefinitionMenu.setProjectCode("MRP");
            counter++;
            session.persist(stockDefinitionMenu);
        }

        Menu stockMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.STOCK.getMenuAlias())).uniqueResult();

        if (stockMenu == null) {
            stockMenu = new Menu();
            stockMenu.setName("Stoklar");
            stockMenu.setCode(MenuEnum.STOCK.getMenuAlias());
            stockMenu.setDescription("Stok ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockMenu.setUrl("stoklar.html");
            stockMenu.setParentOid(stockDefinitionMenu.getOid());
            stockMenu.setMenuOrder(counter);
            stockMenu.setProjectCode("MRP");
            counter++;
            session.persist(stockMenu);
        } else {
            stockMenu.setName("Stoklar");
            stockMenu.setCode(MenuEnum.STOCK.getMenuAlias());
            stockMenu.setDescription("Stok ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockMenu.setUrl("stoklar.html");
            stockMenu.setParentOid(stockDefinitionMenu.getOid());
            stockMenu.setMenuOrder(counter);
            stockMenu.setProjectCode("MRP");
            counter++;
            session.persist(stockMenu);
        }

        Menu stockTypeMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.STOCK_TYPE.getMenuAlias())).uniqueResult();

        if (stockTypeMenu == null) {
            stockTypeMenu = new Menu();
            stockTypeMenu.setName("Stok Tipi");
            stockTypeMenu.setCode(MenuEnum.STOCK_TYPE.getMenuAlias());
            stockTypeMenu.setDescription("Stok Tipi ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockTypeMenu.setUrl("stok-tipi.html");
            stockTypeMenu.setParentOid(stockDefinitionMenu.getOid());
            stockTypeMenu.setMenuOrder(counter);
            stockTypeMenu.setProjectCode("MRP");
            counter++;
            session.persist(stockTypeMenu);
        } else {
            stockTypeMenu.setName("Stok Tipi");
            stockTypeMenu.setCode(MenuEnum.STOCK_TYPE.getMenuAlias());
            stockTypeMenu.setDescription("Stok Tipi ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockTypeMenu.setUrl("stok-tipi.html");
            stockTypeMenu.setParentOid(stockDefinitionMenu.getOid());
            stockTypeMenu.setMenuOrder(counter);
            stockTypeMenu.setProjectCode("MRP");
            counter++;

            session.persist(stockTypeMenu);
        }
        Menu stockAcceptanceMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.STOCK_ACCEPTANCE.getMenuAlias())).uniqueResult();

        if (stockAcceptanceMenu == null) {
            stockAcceptanceMenu = new Menu();
            stockAcceptanceMenu.setName("Depo Giriş");
            stockAcceptanceMenu.setCode(MenuEnum.STOCK_ACCEPTANCE.getMenuAlias());
            stockAcceptanceMenu.setDescription("Stok Kabul ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockAcceptanceMenu.setUrl("stok-kabul.html");
            stockAcceptanceMenu.setParentOid(stockDefinitionMenu.getOid());
            stockAcceptanceMenu.setMenuOrder(counter);
            stockAcceptanceMenu.setProjectCode("MRP");
            counter++;
            session.persist(stockAcceptanceMenu);
        } else {
            stockAcceptanceMenu.setName("Depo Giriş");
            stockAcceptanceMenu.setCode(MenuEnum.STOCK_ACCEPTANCE.getMenuAlias());
            stockAcceptanceMenu.setDescription("Stok Kabul ekleme, güncelleme, silme ve arama sayfasıdır.");
            stockAcceptanceMenu.setUrl("stok-kabul.html");
            stockAcceptanceMenu.setParentOid(stockDefinitionMenu.getOid());
            stockAcceptanceMenu.setMenuOrder(counter);
            stockAcceptanceMenu.setProjectCode("MRP");
            stockAcceptanceMenu.setDeleteStatus(true);
            counter++;

            session.persist(stockAcceptanceMenu);
        }

        Menu warehouseIncomingMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.WAREHOUSE_INCOMING.getMenuAlias())).uniqueResult();

        if (warehouseIncomingMenu == null) {
            warehouseIncomingMenu = new Menu();
            warehouseIncomingMenu.setName("Depo Giriş");
            warehouseIncomingMenu.setCode(MenuEnum.WAREHOUSE_INCOMING.getMenuAlias());
            warehouseIncomingMenu.setDescription("Depo giriş sayfasıdır.");
            warehouseIncomingMenu.setUrl("stok-kabul.html");
            warehouseIncomingMenu.setParentOid(stockDefinitionMenu.getOid());
            warehouseIncomingMenu.setMenuOrder(counter);
            warehouseIncomingMenu.setProjectCode("MRP");
            counter++;
            session.persist(warehouseIncomingMenu);
        } else {
            warehouseIncomingMenu.setName("Depo Giriş");
            warehouseIncomingMenu.setCode(MenuEnum.WAREHOUSE_INCOMING.getMenuAlias());
            warehouseIncomingMenu.setDescription("Depo giriş sayfasıdır.");
            warehouseIncomingMenu.setUrl("stok-kabul.html");
            warehouseIncomingMenu.setParentOid(stockDefinitionMenu.getOid());
            warehouseIncomingMenu.setMenuOrder(counter);
            warehouseIncomingMenu.setProjectCode("MRP");
            counter++;

            session.persist(warehouseIncomingMenu);
        }

        Menu warehouseOutgoingMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.WAREHOUSE_OUTGOING.getMenuAlias())).uniqueResult();

        if (warehouseOutgoingMenu == null) {
            warehouseOutgoingMenu = new Menu();
            warehouseOutgoingMenu.setName("Depo Çıkış");
            warehouseOutgoingMenu.setCode(MenuEnum.WAREHOUSE_OUTGOING.getMenuAlias());
            warehouseOutgoingMenu.setDescription("Depo çıkış sayfasıdır.");
            warehouseOutgoingMenu.setUrl("depo-cikis.html");
            warehouseOutgoingMenu.setParentOid(stockDefinitionMenu.getOid());
            warehouseOutgoingMenu.setMenuOrder(counter);
            warehouseOutgoingMenu.setProjectCode("MRP");
            counter++;
            session.persist(warehouseOutgoingMenu);
        } else {
            warehouseOutgoingMenu.setName("Depo Çıkış");
            warehouseOutgoingMenu.setCode(MenuEnum.WAREHOUSE_OUTGOING.getMenuAlias());
            warehouseOutgoingMenu.setDescription("Depo çıkış sayfasıdır.");
            warehouseOutgoingMenu.setUrl("depo-cikis.html");
            warehouseOutgoingMenu.setParentOid(stockDefinitionMenu.getOid());
            warehouseOutgoingMenu.setMenuOrder(counter);
            warehouseOutgoingMenu.setProjectCode("MRP");
            counter++;

            session.persist(warehouseOutgoingMenu);
        }

        Menu WarehouseOutgoingMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.WAREHOUSEOUTGOING.getMenuAlias())).uniqueResult();

        if (WarehouseOutgoingMenu == null) {
            WarehouseOutgoingMenu = new Menu();
            WarehouseOutgoingMenu.setName("Depo Çıkış");
            WarehouseOutgoingMenu.setCode(MenuEnum.WAREHOUSEOUTGOING.getMenuAlias());
            WarehouseOutgoingMenu.setDescription("Stok silme ve görüntüleme sayfasıdır.");
            WarehouseOutgoingMenu.setUrl("depo-cikis.html");
            WarehouseOutgoingMenu.setParentOid(stockDefinitionMenu.getOid());
            WarehouseOutgoingMenu.setMenuOrder(counter);
            WarehouseOutgoingMenu.setProjectCode("MRP");
            counter++;
            session.persist(WarehouseOutgoingMenu);
        } else {
            WarehouseOutgoingMenu.setName("Depo Çıkış");
            WarehouseOutgoingMenu.setCode(MenuEnum.WAREHOUSEOUTGOING.getMenuAlias());
            WarehouseOutgoingMenu.setDescription("Stok silme ve görüntüleme sayfasıdır.");
            WarehouseOutgoingMenu.setUrl("depo-cikis.html");
            WarehouseOutgoingMenu.setParentOid(stockDefinitionMenu.getOid());
            WarehouseOutgoingMenu.setMenuOrder(counter);
            WarehouseOutgoingMenu.setProjectCode("MRP");
            WarehouseOutgoingMenu.setDeleteStatus(true);
            counter++;

            session.persist(WarehouseOutgoingMenu);
        }
        Menu modellingMainMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.MODELLING_MAIN.getMenuAlias())).uniqueResult();

        if (modellingMainMenu == null) {
            modellingMainMenu = new Menu();
            modellingMainMenu.setName("Modelleme");
            modellingMainMenu.setCode(MenuEnum.MODELLING_MAIN.getMenuAlias());
            modellingMainMenu.setDescription("Modelleme ekranlarının en üst adımıdır.");
            modellingMainMenu.setMenuOrder(counter);
            modellingMainMenu.setProjectCode("MRP");
            counter++;
            session.persist(modellingMainMenu);
        } else {
            modellingMainMenu.setName("Modelleme");
            modellingMainMenu.setCode(MenuEnum.MODELLING_MAIN.getMenuAlias());
            modellingMainMenu.setDescription("Modelleme ekranlarının en üst adımıdır.");
            modellingMainMenu.setMenuOrder(counter);
            modellingMainMenu.setProjectCode("MRP");
            counter++;
            session.persist(modellingMainMenu);
        }

        Menu modelCreateMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.MODEL_CREATE.getMenuAlias())).uniqueResult();

        if (modelCreateMenu == null) {
            modelCreateMenu = new Menu();
            modelCreateMenu.setName("Model Hazırla");
            modelCreateMenu.setCode(MenuEnum.MODEL_CREATE.getMenuAlias());
            modelCreateMenu.setDescription("Model oluşturma, güncelleme, silme ve arama sayfasıdır.");
            modelCreateMenu.setUrl("model-hazirla.html");
            modelCreateMenu.setParentOid(modellingMainMenu.getOid());
            modelCreateMenu.setMenuOrder(counter);
            modelCreateMenu.setProjectCode("MRP");
            counter++;
            session.persist(modelCreateMenu);
        } else {
            modelCreateMenu.setName("Model Hazırla");
            modelCreateMenu.setCode(MenuEnum.MODEL_CREATE.getMenuAlias());
            modelCreateMenu.setDescription("Model oluşturma, güncelleme, silme ve arama sayfasıdır.");
            modelCreateMenu.setUrl("model-hazirla.html");
            modelCreateMenu.setParentOid(modellingMainMenu.getOid());
            modelCreateMenu.setMenuOrder(counter);
            modelCreateMenu.setProjectCode("MRP");
            counter++;
            session.persist(modelCreateMenu);
        }

        Menu ownModels = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.OWN_MODEL.getMenuAlias())).uniqueResult();

        if (ownModels == null) {
            ownModels = new Menu();
            ownModels.setName("Modellerim");
            ownModels.setCode(MenuEnum.OWN_MODEL.getMenuAlias());
            ownModels.setDescription("Model görüntüleme, işleme ve güncelleme sayfasıdır.");
            ownModels.setUrl("modellerim.html");
            ownModels.setParentOid(modellingMainMenu.getOid());
            ownModels.setMenuOrder(counter);
            ownModels.setProjectCode("MRP");
            counter++;
            session.persist(ownModels);
        } else {
            ownModels.setName("Modellerim");
            ownModels.setCode(MenuEnum.OWN_MODEL.getMenuAlias());
            ownModels.setDescription("Model görüntüleme, işleme ve güncelleme sayfasıdır.");
            ownModels.setUrl("modellerim.html");
            ownModels.setParentOid(modellingMainMenu.getOid());
            ownModels.setMenuOrder(counter);
            ownModels.setProjectCode("MRP");
            counter++;
            session.persist(ownModels);
        }

        Menu planning = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.PLANNING.getMenuAlias())).uniqueResult();

        if (planning == null) {
            planning = new Menu();
            planning.setName("Planlama");
            planning.setCode(MenuEnum.PLANNING.getMenuAlias());
            planning.setDescription("Planlama Ekranlarının en üst adımıdır.");
            planning.setMenuOrder(counter);
            planning.setProjectCode("MRP");
            counter++;

            session.persist(planning);
        } else {
            planning.setName("Planlama");
            planning.setCode(MenuEnum.PLANNING.getMenuAlias());
            planning.setDescription("Planlama Ekranlarının en üst adımıdır.");
            planning.setMenuOrder(counter);
            planning.setProjectCode("MRP");
            counter++;

            session.persist(planning);
        }

        Menu pricing = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.PRICING.getMenuAlias())).uniqueResult();

        if (pricing == null) {
            pricing = new Menu();
            pricing.setName("Fiyatlandırma");
            pricing.setCode(MenuEnum.PRICING.getMenuAlias());
            pricing.setDescription("Fiyatlandırma ekrandır.");
            pricing.setUrl("model-fiyatlandirma.html");
            pricing.setParentOid(planning.getOid());
            pricing.setMenuOrder(counter);
            pricing.setProjectCode("MRP");
            counter++;

            session.persist(pricing);
        } else {
            pricing.setName("Fiyatlandırma");
            pricing.setCode(MenuEnum.PRICING.getMenuAlias());
            pricing.setUrl("model-fiyatlandirma.html");
            pricing.setParentOid(planning.getOid());
            pricing.setMenuOrder(counter);
            pricing.setProjectCode("MRP");
            counter++;

            session.persist(pricing);
        }

        Menu order = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.ORDER.getMenuAlias())).uniqueResult();

        if (order == null) {
            order = new Menu();
            order.setName("Müşteri Siparişi");
            order.setCode(MenuEnum.ORDER.getMenuAlias());
            order.setDescription("Müşteri Sipariş ekrandır.");
            order.setUrl("siparis.html");
            order.setParentOid(planning.getOid());
            order.setMenuOrder(counter);
            order.setProjectCode("MRP");
            counter++;

            session.persist(order);
        } else {
            order.setName("Müşteri Siparişi");
            order.setCode(MenuEnum.ORDER.getMenuAlias());
            order.setDescription("Müşteri Sipariş ekrandır.");
            order.setUrl("siparis.html");
            order.setParentOid(planning.getOid());
            order.setMenuOrder(counter);
            order.setProjectCode("MRP");
            counter++;

            session.persist(order);
        }

        Menu supply = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SUPPLY.getMenuAlias())).uniqueResult();

        if (supply == null) {
            supply = new Menu();
            supply.setName("Malzeme Tedariği");
            supply.setCode(MenuEnum.SUPPLY.getMenuAlias());
            supply.setDescription("Tedarik ekrandır.");
            supply.setUrl("tedarik.html");
            supply.setParentOid(planning.getOid());
            supply.setMenuOrder(counter);
            supply.setProjectCode("MRP");
            counter++;

            session.persist(supply);
        } else {
            supply.setName("Malzeme Tedariği");
            supply.setCode(MenuEnum.SUPPLY.getMenuAlias());
            supply.setDescription("Tedarik ekrandır.");
            supply.setUrl("tedarik.html");
            supply.setParentOid(planning.getOid());
            supply.setMenuOrder(counter);
            supply.setProjectCode("MRP");
            counter++;

            session.persist(supply);
        }

        Menu bulkSupply = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.BULK_SUPPLY.getMenuAlias())).uniqueResult();

        if (bulkSupply == null) {
            bulkSupply = new Menu();
            bulkSupply.setName("Toplu Malzeme Tedariği");
            bulkSupply.setCode(MenuEnum.BULK_SUPPLY.getMenuAlias());
            bulkSupply.setDescription("Toplu Tedarik ekrandır.");
            bulkSupply.setUrl("toplu-tedarik.html");
            bulkSupply.setParentOid(planning.getOid());
            bulkSupply.setMenuOrder(counter);
            bulkSupply.setProjectCode("MRP");
            counter++;

            session.persist(bulkSupply);
        } else {
            bulkSupply.setName("Toplu Malzeme Tedariği");
            bulkSupply.setCode(MenuEnum.BULK_SUPPLY.getMenuAlias());
            bulkSupply.setDescription("Toplu Tedarik ekrandır.");
            bulkSupply.setUrl("toplu-tedarik.html");
            bulkSupply.setParentOid(planning.getOid());
            bulkSupply.setMenuOrder(counter);
            bulkSupply.setProjectCode("MRP");
            counter++;

            session.persist(bulkSupply);
        }

        Menu cut = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.CUT.getMenuAlias())).uniqueResult();

        if (cut == null) {
            cut = new Menu();
            cut.setName("Kesim");
            cut.setCode(MenuEnum.CUT.getMenuAlias());
            cut.setDescription("Kesim ekrandır.");
            cut.setUrl("kesim.html");
            cut.setParentOid(planning.getOid());
            cut.setMenuOrder(counter);
            cut.setProjectCode("MRP");
            counter++;

            session.persist(cut);
        } else {
            cut.setName("Kesim");
            cut.setCode(MenuEnum.CUT.getMenuAlias());
            cut.setDescription("Kesim ekrandır.");
            cut.setUrl("kesim.html");
            cut.setParentOid(planning.getOid());
            cut.setMenuOrder(counter);
            cut.setProjectCode("MRP");
            counter++;

            session.persist(cut);
        }

        Menu cuttingTerminal = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.CUTTING_TERMINAL.getMenuAlias())).uniqueResult();

        if (cuttingTerminal == null) {
            cuttingTerminal = new Menu();
            cuttingTerminal.setName("Kesim Terminali");
            cuttingTerminal.setCode(MenuEnum.CUTTING_TERMINAL.getMenuAlias());
            cuttingTerminal.setDescription("Kesim ekrandır.");
            cuttingTerminal.setUrl("kesim-terminali.html");
            cuttingTerminal.setParentOid(planning.getOid());
            cuttingTerminal.setMenuOrder(counter);
            cuttingTerminal.setProjectCode("MRP");
            counter++;

            session.persist(cuttingTerminal);
        } else {
            cuttingTerminal.setName("Kesim Terminali");
            cuttingTerminal.setCode(MenuEnum.CUTTING_TERMINAL.getMenuAlias());
            cuttingTerminal.setDescription("Kesim ekrandır.");
            cuttingTerminal.setUrl("kesim-terminali.html");
            cuttingTerminal.setParentOid(planning.getOid());
            cuttingTerminal.setMenuOrder(counter);
            cuttingTerminal.setProjectCode("MRP");
            counter++;

            session.persist(cuttingTerminal);
        }

        Menu saya = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SAYA.getMenuAlias())).uniqueResult();

        if (saya == null) {
            saya = new Menu();
            saya.setName("Saya");
            saya.setCode(MenuEnum.SAYA.getMenuAlias());
            saya.setDescription("Saya ekrandır.");
            saya.setUrl("saya.html");
            saya.setParentOid(planning.getOid());
            saya.setMenuOrder(counter);
            saya.setProjectCode("MRP");
            counter++;

            session.persist(saya);
        } else {
            saya.setName("Saya");
            saya.setCode(MenuEnum.SAYA.getMenuAlias());
            saya.setDescription("Saya ekrandır.");
            saya.setUrl("saya.html");
            saya.setParentOid(planning.getOid());
            saya.setMenuOrder(counter);
            saya.setProjectCode("MRP");
            counter++;

            session.persist(saya);
        }

        Menu monta = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.MONTA.getMenuAlias())).uniqueResult();

        if (monta == null) {
            monta = new Menu();
            monta.setName("Monta");
            monta.setCode(MenuEnum.MONTA.getMenuAlias());
            monta.setDescription("Monta ekrandır.");
            monta.setUrl("monta.html");
            monta.setParentOid(planning.getOid());
            monta.setMenuOrder(counter);
            monta.setProjectCode("MRP");
            counter++;

            session.persist(monta);
        } else {
            monta.setName("Monta");
            monta.setCode(MenuEnum.MONTA.getMenuAlias());
            monta.setDescription("Monta ekrandır.");
            monta.setUrl("monta.html");
            monta.setParentOid(planning.getOid());
            monta.setMenuOrder(counter);
            monta.setProjectCode("MRP");
            counter++;

            session.persist(monta);
        }

        Menu montaTerminal = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.MONTA_TERMINAL.getMenuAlias())).uniqueResult();

        if (montaTerminal == null) {
            montaTerminal = new Menu();
            montaTerminal.setName("Monta Terminali");
            montaTerminal.setCode(MenuEnum.MONTA_TERMINAL.getMenuAlias());
            montaTerminal.setDescription("Monta ekrandır.");
            montaTerminal.setUrl("monta-terminali.html");
            montaTerminal.setParentOid(planning.getOid());
            montaTerminal.setMenuOrder(counter);
            montaTerminal.setProjectCode("MRP");
            counter++;

            session.persist(montaTerminal);
        } else {
            montaTerminal.setName("Monta Terminali");
            montaTerminal.setCode(MenuEnum.MONTA_TERMINAL.getMenuAlias());
            montaTerminal.setDescription("Monta ekrandır.");
            montaTerminal.setUrl("monta-terminali.html");
            montaTerminal.setParentOid(planning.getOid());
            montaTerminal.setMenuOrder(counter);
            montaTerminal.setProjectCode("MRP");
            counter++;

            session.persist(montaTerminal);
        }

        Menu shipmentOrder = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SHIPMENT_ORDER.getMenuAlias())).uniqueResult();

        if (shipmentOrder == null) {
            shipmentOrder = new Menu();
            shipmentOrder.setName("Sevkiyat Emri");
            shipmentOrder.setCode(MenuEnum.SHIPMENT_ORDER.getMenuAlias());
            shipmentOrder.setDescription("Sevkiyat emri sayfasıdır.");
            shipmentOrder.setUrl("sevkiyat-emri.html");
            shipmentOrder.setParentOid(planning.getOid());
            shipmentOrder.setMenuOrder(counter);
            shipmentOrder.setProjectCode("MRP");
            counter++;

            session.persist(shipmentOrder);
        } else {
            shipmentOrder.setName("Sevkiyat Emri");
            shipmentOrder.setCode(MenuEnum.SHIPMENT_ORDER.getMenuAlias());
            shipmentOrder.setDescription("Sevkiyat emri sayfasıdır.");
            shipmentOrder.setUrl("sevkiyat-emri.html");
            shipmentOrder.setParentOid(planning.getOid());
            shipmentOrder.setMenuOrder(counter);
            shipmentOrder.setProjectCode("MRP");
            counter++;

            session.persist(shipmentOrder);
        }

        Menu modelProcessing = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.MODEL_PROCESSING.getMenuAlias())).uniqueResult();

        if (modelProcessing == null) {
            modelProcessing = new Menu();
            modelProcessing.setName("Proses Emri");
            modelProcessing.setCode(MenuEnum.MODEL_PROCESSING.getMenuAlias());
            modelProcessing.setDescription("Proses Emri ekleme, güncelleme, silme ve arama sayfasıdır.");
            modelProcessing.setUrl("proses-emri.html");
            modelProcessing.setParentOid(planning.getOid());
            modelProcessing.setMenuOrder(counter);
            modelProcessing.setProjectCode("MRP");
            counter++;
            session.persist(modelProcessing);
        } else {
            modelProcessing.setName("Proses Emri");
            modelProcessing.setCode(MenuEnum.MODEL_PROCESSING.getMenuAlias());
            modelProcessing.setDescription("Proses Emri ekleme, güncelleme, silme ve arama sayfasıdır.");
            modelProcessing.setUrl("proses-emri.html");
            modelProcessing.setParentOid(planning.getOid());
            modelProcessing.setMenuOrder(counter);
            modelProcessing.setProjectCode("MRP");
            counter++;
            session.persist(modelProcessing);
        }
        Menu production = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.PRODUCTION.getMenuAlias())).uniqueResult();

        if (production == null) {
            production = new Menu();
            production.setName("Üretim");
            production.setCode(MenuEnum.PRODUCTION.getMenuAlias());
            production.setDescription("Üretim Ekranlarının en üst adımıdır.");
            production.setMenuOrder(counter);
            production.setProjectCode("MRP");
            counter++;

            session.persist(production);
        } else {
            production.setName("Üretim");
            production.setCode(MenuEnum.PRODUCTION.getMenuAlias());
            production.setDescription("Üretim Ekranlarının en üst adımıdır.");
            production.setMenuOrder(counter);
            production.setProjectCode("MRP");
            counter++;

            session.persist(production);
        }

        Menu productionPlaning = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.PRODUCTION_PLANNING.getMenuAlias())).uniqueResult();

        if (productionPlaning == null) {
            productionPlaning = new Menu();
            productionPlaning.setName("Üretim Takvimi");
            productionPlaning.setCode(MenuEnum.PRODUCTION_PLANNING.getMenuAlias());
            productionPlaning.setDescription("Üretim Takvimi ekranıdır.");
            productionPlaning.setParentOid(production.getOid());
            productionPlaning.setUrl("uretim-takvimi.html");
            productionPlaning.setMenuOrder(counter);
            productionPlaning.setProjectCode("MRP");
            counter++;

            session.persist(productionPlaning);
        } else {
            productionPlaning.setName("Üretim Takvimi");
            productionPlaning.setCode(MenuEnum.PRODUCTION_PLANNING.getMenuAlias());
            productionPlaning.setDescription("Üretim Takvimi ekranıdır.");
            productionPlaning.setParentOid(production.getOid());
            productionPlaning.setUrl("uretim-takvimi.html");
            productionPlaning.setMenuOrder(counter);
            productionPlaning.setProjectCode("MRP");
            counter++;

            session.persist(productionPlaning);
        }

        Menu productionList = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.PRODUCTION_LIST.getMenuAlias())).uniqueResult();

        if (productionList == null) {
            productionList = new Menu();
            productionList.setName("Üretim Listeleri");
            productionList.setCode(MenuEnum.PRODUCTION_LIST.getMenuAlias());
            productionList.setDescription("Üretim Listesi ekrandır.");
            productionList.setParentOid(production.getOid());
            productionList.setUrl("uretim-listeleri.html");
            productionList.setMenuOrder(counter);
            productionList.setProjectCode("MRP");
            counter++;

            session.persist(productionList);
        } else {
            productionList.setName("Üretim Listeleri");
            productionList.setCode(MenuEnum.PRODUCTION_LIST.getMenuAlias());
            productionList.setDescription("Üretim Listesi ekranıdır.");
            productionList.setParentOid(production.getOid());
            productionList.setUrl("uretim-listeleri.html");
            productionList.setMenuOrder(counter);
            productionList.setProjectCode("MRP");
            counter++;

            session.persist(productionList);
        }


        Menu reports = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.REPORTS.getMenuAlias())).uniqueResult();

        if (reports == null) {
            reports = new Menu();
            reports.setName("Raporlar");
            reports.setCode(MenuEnum.REPORTS.getMenuAlias());
            reports.setDescription("Rapor Ekranlarının en üst adımıdır.");
            reports.setMenuOrder(counter);
            reports.setProjectCode("MRP");
            counter++;

            session.persist(reports);
        } else {
            reports.setName("Raporlar");
            reports.setCode(MenuEnum.REPORTS.getMenuAlias());
            reports.setDescription("Rapor Ekranlarının en üst adımıdır.");
            reports.setMenuOrder(counter);
            reports.setProjectCode("MRP");
            counter++;

            session.persist(reports);
        }

        Menu generalProductReport = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.PRODUCTION_REPORT.getMenuAlias())).uniqueResult();

        if (generalProductReport == null) {
            generalProductReport = new Menu();
            generalProductReport.setName("Üretim Raporları");
            generalProductReport.setCode(MenuEnum.PRODUCTION_REPORT.getMenuAlias());
            generalProductReport.setDescription("Üretim Raporları ekrandır.");
            generalProductReport.setParentOid(reports.getOid());
            generalProductReport.setUrl("uretim-raporlari.html");
            generalProductReport.setMenuOrder(counter);
            generalProductReport.setProjectCode("MRP");
            counter++;

            session.persist(generalProductReport);
        } else {
            generalProductReport.setName("Üretim Raporları");
            generalProductReport.setCode(MenuEnum.PRODUCTION_REPORT.getMenuAlias());
            generalProductReport.setDescription("Üretim Raporları ekrandır.");
            generalProductReport.setParentOid(reports.getOid());
            generalProductReport.setUrl("uretim-raporlari.html");
            generalProductReport.setMenuOrder(counter);
            generalProductReport.setProjectCode("MRP");
            counter++;

            session.persist(generalProductReport);
        }

        Menu userReport = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.USER_REPORT.getMenuAlias())).uniqueResult();

        if (userReport == null) {
            userReport = new Menu();
            userReport.setName("Kullanıcı Raporları");
            userReport.setCode(MenuEnum.USER_REPORT.getMenuAlias());
            userReport.setDescription("Kullanıcı raporları ekranıdır.");
            userReport.setParentOid(reports.getOid());
            userReport.setUrl("kullanici-raporlari.html");
            userReport.setMenuOrder(counter);
            userReport.setProjectCode("MRP");
            counter++;

            session.persist(userReport);
        } else {
            userReport.setName("Kullanıcı Raporları");
            userReport.setCode(MenuEnum.USER_REPORT.getMenuAlias());
            userReport.setDescription("Kullanıcı raporları ekranıdır.");
            userReport.setParentOid(reports.getOid());
            userReport.setUrl("kullanici-raporlari.html");
            userReport.setMenuOrder(counter);
            userReport.setProjectCode("MRP");
            counter++;

            session.persist(userReport);
        }
        Menu supplyReport = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SUPPLY_REPORT.getMenuAlias())).uniqueResult();

        if (supplyReport == null) {
            supplyReport = new Menu();
            supplyReport.setName("Tedarik Raporları");
            supplyReport.setCode(MenuEnum.SUPPLY_REPORT.getMenuAlias());
            supplyReport.setDescription("Tedarik raporları ekranıdır.");
            supplyReport.setParentOid(reports.getOid());
            supplyReport.setUrl("tedarik-raporlari.html");
            supplyReport.setMenuOrder(counter);
            supplyReport.setProjectCode("MRP");
            counter++;

            session.persist(supplyReport);
        } else {
            supplyReport.setName("Tedarik Raporları");
            supplyReport.setCode(MenuEnum.SUPPLY_REPORT.getMenuAlias());
            supplyReport.setDescription("Tedarik raporları ekranıdır.");
            supplyReport.setParentOid(reports.getOid());
            supplyReport.setUrl("tedarik-raporlari.html");
            supplyReport.setMenuOrder(counter);
            supplyReport.setProjectCode("MRP");
            counter++;

            session.persist(supplyReport);
        }
        Menu sayaciReport = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SAYACI_REPORT.getMenuAlias())).uniqueResult();

        if (sayaciReport == null) {
            sayaciReport = new Menu();
            sayaciReport.setName("Sayacı Raporları");
            sayaciReport.setCode(MenuEnum.SAYACI_REPORT.getMenuAlias());
            sayaciReport.setDescription("Sayacı raporları ekranıdır.");
            sayaciReport.setParentOid(reports.getOid());
            sayaciReport.setUrl("sayaci-raporlari.html");
            sayaciReport.setMenuOrder(counter);
            sayaciReport.setProjectCode("MRP");
            counter++;

            session.persist(sayaciReport);
        } else {
            sayaciReport.setName("Sayacı Raporları");
            sayaciReport.setCode(MenuEnum.SAYACI_REPORT.getMenuAlias());
            sayaciReport.setDescription("Sayacı raporları ekranıdır.");
            sayaciReport.setParentOid(reports.getOid());
            sayaciReport.setUrl("sayaci-raporlari.html");
            sayaciReport.setMenuOrder(counter);
            sayaciReport.setProjectCode("MRP");
            counter++;

            session.persist(sayaciReport);
        }
        Menu supplierReport = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.SUPPLIER_REPORT.getMenuAlias())).uniqueResult();

        if (supplierReport == null) {
            supplierReport = new Menu();
            supplierReport.setName("Tedarikçi Raporları");
            supplierReport.setCode(MenuEnum.SUPPLIER_REPORT.getMenuAlias());
            supplierReport.setDescription("Tedarikçi raporları ekranıdır.");
            supplierReport.setParentOid(reports.getOid());
            supplierReport.setUrl("tedarikci-raporlari.html");
            supplierReport.setMenuOrder(counter);
            supplierReport.setProjectCode("MRP");
            counter++;

            session.persist(supplierReport);
        } else {
            supplierReport.setName("Tedarikçi Raporları");
            supplierReport.setCode(MenuEnum.SUPPLIER_REPORT.getMenuAlias());
            supplierReport.setDescription("Tedarikçi raporları ekranıdır.");
            supplierReport.setParentOid(reports.getOid());
            supplierReport.setUrl("tedarikci-raporlari.html");
            supplierReport.setMenuOrder(counter);
            supplierReport.setProjectCode("MRP");
            counter++;

            session.persist(supplierReport);
        }
        Menu shipmentMain = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.FORWARDING.getMenuAlias())).uniqueResult();

        if (shipmentMain == null) {
            shipmentMain = new Menu();
            shipmentMain.setName("Sevkiyat");
            shipmentMain.setCode(MenuEnum.FORWARDING.getMenuAlias());
            shipmentMain.setDescription("Sevkiyat ekranlarının en üst adımıdır.");
            shipmentMain.setMenuOrder(counter);
            shipmentMain.setProjectCode("MRP");
            counter++;

            session.persist(shipmentMain);
        } else {
            shipmentMain.setName("Sevkiyat");
            shipmentMain.setCode(MenuEnum.FORWARDING.getMenuAlias());
            shipmentMain.setDescription("Sevkiyat ekranlarının en üst adımıdır.");
            shipmentMain.setMenuOrder(counter);
            shipmentMain.setProjectCode("MRP");
            counter++;

            session.persist(shipmentMain);
        }

        Menu shipmentList = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.FORWARDING_INFO.getMenuAlias())).uniqueResult();

        if (shipmentList == null) {
            shipmentList = new Menu();
            shipmentList.setName("Sevkiyat Listeleri");
            shipmentList.setCode(MenuEnum.FORWARDING_INFO.getMenuAlias());
            shipmentList.setDescription("Sevkiyat listeleri ekrandır.");
            shipmentList.setParentOid(shipmentMain.getOid());
            shipmentList.setUrl("sevkiyat-listeleri.html");
            shipmentList.setMenuOrder(counter);
            shipmentList.setProjectCode("MRP");
            counter++;

            session.persist(shipmentList);
        } else {
            shipmentList.setName("Sevkiyat Listeleri");
            shipmentList.setCode(MenuEnum.FORWARDING_INFO.getMenuAlias());
            shipmentList.setDescription("Sevkiyat listeleri ekrandır.");
            shipmentList.setParentOid(shipmentMain.getOid());
            shipmentList.setUrl("sevkiyat-listeleri.html");
            shipmentList.setMenuOrder(counter);
            shipmentList.setProjectCode("MRP");
            counter++;

            session.persist(shipmentList);
        }

        Menu fairMain = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.FAIR_DEFINITION.getMenuAlias())).uniqueResult();

        if (fairMain == null) {
            fairMain = new Menu();
            fairMain.setName("Fuar");
            fairMain.setCode(MenuEnum.FAIR_DEFINITION.getMenuAlias());
            fairMain.setDescription("Fuar işlemleri ekranlarının en üst adımıdır.");
            fairMain.setMenuOrder(counter);
            fairMain.setProjectCode("MRP");
            counter++;

            session.persist(fairMain);
        } else {
            fairMain.setName("Fuar");
            fairMain.setCode(MenuEnum.FAIR_DEFINITION.getMenuAlias());
            fairMain.setDescription("Fuar işlemleri ekranlarının en üst adımıdır.");
            fairMain.setMenuOrder(counter);
            fairMain.setProjectCode("MRP");
            counter++;

            session.persist(fairMain);
        }

        Menu fairDefinition = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.FAIR_OPERATION.getMenuAlias())).uniqueResult();

        if (fairDefinition == null) {
            fairDefinition = new Menu();
            fairDefinition.setName("Fuar İşlemleri");
            fairDefinition.setCode(MenuEnum.FAIR_OPERATION.getMenuAlias());
            fairDefinition.setDescription("Fuar işlemleri sayfasıdır.");
            fairDefinition.setUrl("fuar-islemleri.html");
            fairDefinition.setParentOid(fairMain.getOid());
            fairDefinition.setMenuOrder(counter);
            fairDefinition.setProjectCode("MRP");
            counter++;

            session.persist(fairDefinition);
        } else {
            fairDefinition.setName("Fuar İşlemleri");
            fairDefinition.setCode(MenuEnum.FAIR_OPERATION.getMenuAlias());
            fairDefinition.setDescription("Fuar işlemleri sayfasıdır.");
            fairDefinition.setUrl("fuar-islemleri.html");
            fairDefinition.setParentOid(fairMain.getOid());
            fairDefinition.setMenuOrder(counter);
            fairDefinition.setProjectCode("MRP");
            counter++;

            session.persist(fairDefinition);
        }

        Menu fairReport = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.FAIR_REPORT.getMenuAlias())).uniqueResult();

        if (fairReport == null) {
            fairReport = new Menu();
            fairReport.setName("Fuar Raporları");
            fairReport.setCode(MenuEnum.FAIR_REPORT.getMenuAlias());
            fairReport.setDescription("Fuar raporları sayfasıdır.");
            fairReport.setUrl("fuar-raporlari.html");
            fairReport.setParentOid(fairMain.getOid());
            fairReport.setMenuOrder(counter);
            fairReport.setProjectCode("MRP");
            counter++;

            session.persist(fairReport);
        } else {
            fairReport.setName("Fuar Raporları");
            fairReport.setCode(MenuEnum.FAIR_REPORT.getMenuAlias());
            fairReport.setDescription("Fuar raporları sayfasıdır.");
            fairReport.setUrl("fuar-raporlari.html");
            fairReport.setParentOid(fairMain.getOid());
            fairReport.setMenuOrder(counter);
            fairReport.setProjectCode("MRP");
            counter++;

            session.persist(fairReport);
        }

        Menu userMainMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.USER_MAIN.getMenuAlias())).uniqueResult();

        if (userMainMenu == null) {
            userMainMenu = new Menu();
            userMainMenu.setName("Yönetim");
            userMainMenu.setCode(MenuEnum.USER_MAIN.getMenuAlias());
            userMainMenu.setDescription("Sistem operasyonlarının en üst adımıdır.");
            userMainMenu.setMenuOrder(100000);
            userMainMenu.setProjectCode("MRP");
            session.persist(userMainMenu);
        } else {
            userMainMenu.setName("Yönetim");
            userMainMenu.setCode(MenuEnum.USER_MAIN.getMenuAlias());
            userMainMenu.setDescription("Sistem operasyonlarının en üst adımıdır.");
            userMainMenu.setMenuOrder(100000);
            userMainMenu.setProjectCode("MRP");
            session.persist(userMainMenu);
        }

        Menu companyUpdateMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.COMPANY.getMenuAlias())).uniqueResult();

        if (companyUpdateMenu == null) {
            companyUpdateMenu = new Menu();
            companyUpdateMenu.setName("Şirket");
            companyUpdateMenu.setCode(MenuEnum.COMPANY.getMenuAlias());
            companyUpdateMenu.setDescription("Şirket bilgileri güncelleme sayfasıdır.");
            companyUpdateMenu.setUrl("sirket.html");
            companyUpdateMenu.setParentOid(userMainMenu.getOid());
            companyUpdateMenu.setMenuOrder(100001);
            companyUpdateMenu.setProjectCode("MRP");
            session.persist(companyUpdateMenu);
        } else {
            companyUpdateMenu.setName("Şirket");
            companyUpdateMenu.setCode(MenuEnum.COMPANY.getMenuAlias());
            companyUpdateMenu.setDescription("Şirket bilgileri güncelleme sayfasıdır.");
            companyUpdateMenu.setUrl("sirket.html");
            companyUpdateMenu.setParentOid(userMainMenu.getOid());
            companyUpdateMenu.setMenuOrder(100001);
            companyUpdateMenu.setProjectCode("MRP");
            session.persist(companyUpdateMenu);
        }

        Menu companyMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.COMPANY_UPDATE.getMenuAlias())).uniqueResult();

        if (companyMenu == null) {
            companyMenu = new Menu();
            companyMenu.setName("Firma Düzenle");
            companyMenu.setCode(MenuEnum.COMPANY_UPDATE.getMenuAlias());
            companyMenu.setDescription("Firma ekleme, güncelleme, silme sayfasıdır.");
            companyMenu.setUrl("firma-duzenle.html");
            companyMenu.setParentOid(userMainMenu.getOid());
            companyMenu.setMenuOrder(100002);
            companyMenu.setProjectCode("MRP");
            session.persist(companyMenu);
        } else {
            companyMenu.setName("Firma Düzenle");
            companyMenu.setCode(MenuEnum.COMPANY_UPDATE.getMenuAlias());
            companyMenu.setDescription("Firma bilgileri güncelleme sayfasıdır.");
            companyMenu.setUrl("firma-duzenle.html");
            companyMenu.setParentOid(userMainMenu.getOid());
            companyMenu.setMenuOrder(100002);
            companyMenu.setProjectCode("MRP");
            session.persist(companyMenu);
        }

        Menu rolePermissionMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.ROL_PERMISSION.getMenuAlias())).uniqueResult();

        if (rolePermissionMenu == null) {
            rolePermissionMenu = new Menu();
            rolePermissionMenu.setName("Roller ve İzinler");
            rolePermissionMenu.setCode(MenuEnum.ROL_PERMISSION.getMenuAlias());
            rolePermissionMenu.setDescription("Rol/İzin oluşturma, güncelleme, silme ve arama sayfasıdır.");
            rolePermissionMenu.setUrl("rolizin.html");
            rolePermissionMenu.setParentOid(userMainMenu.getOid());
            rolePermissionMenu.setMenuOrder(100002);
            rolePermissionMenu.setProjectCode("MRP");
            session.persist(rolePermissionMenu);
        } else {
            rolePermissionMenu.setName("Roller ve İzinler");
            rolePermissionMenu.setCode(MenuEnum.ROL_PERMISSION.getMenuAlias());
            rolePermissionMenu.setDescription("Rol/İzin oluşturma, güncelleme, silme ve arama sayfasıdır.");
            rolePermissionMenu.setUrl("rolizin.html");
            rolePermissionMenu.setParentOid(userMainMenu.getOid());
            rolePermissionMenu.setMenuOrder(100002);
            rolePermissionMenu.setProjectCode("MRP");
            session.persist(rolePermissionMenu);
        }
        Menu userProcessMenu = (Menu) session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).add(Restrictions.eq("code", MenuEnum.USER_PROCESS.getMenuAlias())).uniqueResult();

        if (userProcessMenu == null) {
            userProcessMenu = new Menu();
            userProcessMenu.setName("Kullanıcı İşlemleri");
            userProcessMenu.setCode(MenuEnum.USER_PROCESS.getMenuAlias());
            userProcessMenu.setDescription("Kullanıcı oluşturma, güncelleme, silme ve arama sayfasıdır.");
            userProcessMenu.setUrl("kullaniciislemleri.html");
            userProcessMenu.setParentOid(userMainMenu.getOid());
            userProcessMenu.setMenuOrder(100003);
            userProcessMenu.setProjectCode("MRP");
            session.persist(userProcessMenu);
        } else {
            userProcessMenu.setName("Kullanıcı İşlemleri");
            userProcessMenu.setCode(MenuEnum.USER_PROCESS.getMenuAlias());
            userProcessMenu.setDescription("Kullanıcı oluşturma, güncelleme, silme ve arama sayfasıdır.");
            userProcessMenu.setUrl("kullaniciislemleri.html");
            userProcessMenu.setParentOid(userMainMenu.getOid());
            userProcessMenu.setMenuOrder(100003);
            userProcessMenu.setProjectCode("MRP");
            session.persist(userProcessMenu);
        }

        session.flush();
    }

    public void createTestCompanyAndItsDependencies(Session session) {
        //  COMPANY
        Company testCompany1 = (Company) session.createCriteria(Company.class).add(Restrictions.eq("code", "TCom1")).uniqueResult();

        if (testCompany1 == null) {
            testCompany1 = new Company();
            testCompany1.setName("Örnek Firma 1 LTD ŞTİ");
            testCompany1.setCode("TCom1");
            testCompany1.setBankInfo("Garanti :TR91 0001 0000 0000 0000 0000 00");
            testCompany1.setLogoUrl("libs/assets/logo/örnek.png");
            testCompany1.setLanguage("TR");
            testCompany1.setAddress("Örnek1 mahallesi Örnek1 Semti Örnek1 No Örnek1 kat.");
            testCompany1.setPhone("+90-0212-000-00-01");
            testCompany1.setEmail("TR");
            testCompany1.setFax("+90-0212-000-00-01");
            testCompany1.setWebsite("www.ornek1.com");
            testCompany1.setTwitter("twitter.com/ornekfirma1");
            testCompany1.setFacebook("facebook.com/ornekfirma1");
            testCompany1.setInstagram("instagram.com/ornekfirma1");
            session.persist(testCompany1);
        } else {
            testCompany1.setName("Örnek Firma 1 LTD ŞTİ");
            testCompany1.setCode("TCom1");
            testCompany1.setBankInfo("Garanti :TR91 0001 0000 0000 0000 0000 00");
            testCompany1.setLogoUrl("libs/assets/logo/örnek.png");
            testCompany1.setLanguage("TR");
            testCompany1.setAddress("Örnek1 mahallesi Örnek1 Semti Örnek1 No Örnek1 kat.");
            testCompany1.setPhone("+90-0212-000-00-01");
            testCompany1.setEmail("TR");
            testCompany1.setFax("+90-0212-000-00-01");
            testCompany1.setWebsite("www.ornek1.com");
            testCompany1.setTwitter("twitter.com/ornekfirma1");
            testCompany1.setFacebook("facebook.com/ornekfirma1");
            testCompany1.setInstagram("instagram.com/ornekfirma1");
            session.persist(testCompany1);
        }

        //  USER
        User demoUser = (User) session.createCriteria(User.class).add(Restrictions.eq("email", "demo@ayakkabi.com")).uniqueResult();

        if (demoUser == null) {
            demoUser = new User();
            demoUser.setName("Demo");
            demoUser.setSurname("Kullanıcı");
            demoUser.setCode("DEM");
            demoUser.setMobilePhone("05418847672");
            demoUser.setInternalPhoneCode("0999");
            demoUser.setEmail("demo@ayakkabi.com");
            demoUser.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");  // 1234
            demoUser.setCompany(testCompany1);

            session.persist(demoUser);
        } else {
            demoUser.setName("Demo");
            demoUser.setSurname("Kullanıcı");
            demoUser.setCode("DEM");
            demoUser.setMobilePhone("05418847672");
            demoUser.setInternalPhoneCode("0999");
            demoUser.setEmail("demo@ayakkabi.com");
            demoUser.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");  // 1234
            demoUser.setCompany(testCompany1);

            session.persist(demoUser);
        }

        //  ROLE
        Role demoAdminRole = (Role) session.createCriteria(Role.class)
                .add(Restrictions.eq("code", "DEMADMIN"))
                .add(Restrictions.eq("deleteStatus", false)).uniqueResult();

        if (demoAdminRole == null) {
            demoAdminRole = new Role();
            demoAdminRole.setName("Demo-Admin-Role");
            demoAdminRole.setCode("DEMADMIN");
            demoAdminRole.setCompany(testCompany1);
            session.persist(demoAdminRole);
        } else {
            demoAdminRole.setName("Demo-Admin-Role");
            demoAdminRole.setCode("DEMADMIN");
            demoAdminRole.setCompany(testCompany1);
            session.persist(demoAdminRole);
        }

        //  ASSING PERMISSION TO ROLE
        List<Permission> permissionList = session.createCriteria(Permission.class).list();
        for (Permission permission : permissionList) {
            RolePermission rolePermission = (RolePermission) session.createCriteria(RolePermission.class)
                    .add(Restrictions.eq("deleteStatus", false))
                    .add(Restrictions.eq("role.oid", demoAdminRole.getOid()))
                    .add(Restrictions.eq("permission.oid", permission.getOid()))
                    .add(Restrictions.eq("company.oid", testCompany1.getOid())).uniqueResult();

            if (rolePermission == null) {
                rolePermission = new RolePermission();
                rolePermission.setRole(demoAdminRole);
                rolePermission.setPermission(permission);
                rolePermission.setCompany(testCompany1);

                session.persist(rolePermission);
            }
        }

        //  ASSING MENU TO ROLE
        List<Menu> menuList = session.createCriteria(Menu.class).list();
        for (Menu menu : menuList) {
            RoleMenu roleMenu = (RoleMenu) session.createCriteria(RoleMenu.class)
                    .add(Restrictions.eq("deleteStatus", false))
                    .add(Restrictions.eq("role.oid", demoAdminRole.getOid()))
                    .add(Restrictions.eq("menu.oid", menu.getOid()))
                    .add(Restrictions.eq("company.oid", testCompany1.getOid())).uniqueResult();

            if (roleMenu == null) {
                roleMenu = new RoleMenu();
                roleMenu.setRole(demoAdminRole);
                roleMenu.setMenu(menu);
                roleMenu.setCompany(testCompany1);

                session.persist(roleMenu);
            }
        }

        //  ASSING ROLE TO USER
        RoleUser roleUser = (RoleUser) session.createCriteria(RoleUser.class)
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("role.oid", demoAdminRole.getOid()))
                .add(Restrictions.eq("user.oid", demoUser.getOid()))
                .add(Restrictions.eq("company.oid", testCompany1.getOid())).uniqueResult();

        if (roleUser == null) {
            roleUser = new RoleUser();
            roleUser.setRole(demoAdminRole);
            roleUser.setUser(demoUser);
            roleUser.setCompany(testCompany1);

            session.persist(roleUser);
        }

        //  MRP SETTING
        MrpSetting mrpSetting = (MrpSetting) session.createCriteria(MrpSetting.class)
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", testCompany1.getOid())).uniqueResult();

        if (mrpSetting == null) {
            mrpSetting = new MrpSetting();
            mrpSetting.setCompany(testCompany1);
            session.persist(mrpSetting);
        }

        session.persist(roleUser);

        session.flush();
    }

    public void createFelisLabs(Session session) {
        Company felisLabs = (Company) session.createCriteria(Company.class).add(Restrictions.eq("code", "FLS")).uniqueResult();

        if (felisLabs == null) {
            felisLabs = new Company();
            felisLabs.setName("FELİSLABS");
            felisLabs.setCode("FLS");
            felisLabs.setBankInfo("Garanti :TRXX XXXX XXXX XXXX XXXX XXXX XX");
            felisLabs.setLogoUrl("libs/assets/logo/felisLabs.png");
            felisLabs.setLanguage("TR");
            felisLabs.setAddress("<p>KULELİ</p><p> YENİBOSNA/İSTANBUL</p>");
            felisLabs.setPhone("+90-0212-434-00-01");
            felisLabs.setEmail("felisLabs@felisLabs.com");
            felisLabs.setFax("+90-0212-434-00-02");
            felisLabs.setWebsite("www.felisLabs.com");
            felisLabs.setTwitter("twitter.com/felisLabs");
            felisLabs.setFacebook("facebook.com/felisLabs");
            felisLabs.setInstagram("instagram.com/felisLabs");
            felisLabs.setOrderSpecialNotes("<p></p><style type=\"text/css\">li.li1 {margin: 0.0px 0.0px 10.0px 0.0px; font: 12.0px 'Trebuchet MS'; -webkit-text-stroke: #000000}\n" +
                    "span.s1 {font: 12.0px Helvetica; -webkit-text-stroke: 0px #000000}\n" +
                    "span.s2 {font-kerning: none}\n" +
                    "span.s3 {font: 12.0px Helvetica}\n" +
                    "ol.ol1 {list-style-type: decimal}\n" +
                    "</style>\n" +
                    "\n" +
                    "\n" +
                    "<ol class=\"ol1\"><li class=\"li1\"><span class=\"s2\">Anlaşılan fiyatlar dikkate alınır. Tedarik&ccedil;i talebi ile, anlaşılan sevk tarihinden &ouml;nce sevkiyat yapılsa bile, &ouml;deme vadesi anlaşılan sevkiyat tarihinden itibaren ge&ccedil;erli olacaktır.&nbsp;</span></li><li class=\"li1\"><span class=\"s2\">Tedarik&ccedil;i firma seri &uuml;retime ge&ccedil;meden &ouml;nce, her t&uuml;rl&uuml; numune &ccedil;alışmalarını onayını firmamızın yetkilisinden almak zorundadır.</span></li><li class=\"li1\"><span class=\"s2\">Siparişi verilen malzeme kalitesi anlaşılan &ouml;zelliklere uygun olmalı ve lot ve renk farkından doğabilecek sorunlarda malın tamamı iade edilecektir.</span></li><li class=\"li1\"><span class=\"s2\">Firmamız teyit edilen ancak teslim tarihinde gelmeyen &uuml;r&uuml;nleri iptal etme yetkisine sahip olup, termin tarihinden sonra gecikilen her g&uuml;n i&ccedil;in %10&rsquo;u kadar bir gecikme cezası uygulayabilir.</span></li><li class=\"li1\"><span class=\"s2\">&Uuml;r&uuml;n sevkiyatı hafta i&ccedil;i saat: 18:00 den &ouml;nce yapılmalıdır. (Acil durumlar hari&ccedil;)</span></li><li class=\"li1\"><span class=\"s2\">Tarafımıza gelecek olan &Uuml;r&uuml;nlerde herhangi bir kanserojen madde, fitalat, AZO boyar maddelerinin bulunmaması gerekmektedir. Aksi halde tedarik&ccedil;iye r&uuml;cu edilecektir.</span></li></ol><p></p>");


            session.persist(felisLabs);
        } else {
            felisLabs.setName("FELİSLABS");
            felisLabs.setCode("FLS");
            felisLabs.setBankInfo("Garanti :TRXX XXXX XXXX XXXX XXXX XXXX XX");
            felisLabs.setLogoUrl("libs/assets/logo/felisLabs.png");
            felisLabs.setLanguage("TR");
            felisLabs.setAddress("<p>KULELİ</p><p> YENİBOSNA/İSTANBUL</p>");
            felisLabs.setPhone("+90-0212-434-00-01");
            felisLabs.setEmail("felisLabs@felisLabs.com");
            felisLabs.setFax("+90-0212-434-00-02");
            felisLabs.setWebsite("www.felisLabs.com");
            felisLabs.setTwitter("twitter.com/felisLabs");
            felisLabs.setFacebook("facebook.com/felisLabs");
            felisLabs.setInstagram("instagram.com/felisLabs");
            felisLabs.setOrderSpecialNotes("<p></p><style type=\"text/css\">li.li1 {margin: 0.0px 0.0px 10.0px 0.0px; font: 12.0px 'Trebuchet MS'; -webkit-text-stroke: #000000}\n" +
                    "span.s1 {font: 12.0px Helvetica; -webkit-text-stroke: 0px #000000}\n" +
                    "span.s2 {font-kerning: none}\n" +
                    "span.s3 {font: 12.0px Helvetica}\n" +
                    "ol.ol1 {list-style-type: decimal}\n" +
                    "</style>\n" +
                    "\n" +
                    "\n" +
                    "<ol class=\"ol1\"><li class=\"li1\"><span class=\"s2\">Anlaşılan fiyatlar dikkate alınır. Tedarik&ccedil;i talebi ile, anlaşılan sevk tarihinden &ouml;nce sevkiyat yapılsa bile, &ouml;deme vadesi anlaşılan sevkiyat tarihinden itibaren ge&ccedil;erli olacaktır.&nbsp;</span></li><li class=\"li1\"><span class=\"s2\">Tedarik&ccedil;i firma seri &uuml;retime ge&ccedil;meden &ouml;nce, her t&uuml;rl&uuml; numune &ccedil;alışmalarını onayını firmamızın yetkilisinden almak zorundadır.</span></li><li class=\"li1\"><span class=\"s2\">Siparişi verilen malzeme kalitesi anlaşılan &ouml;zelliklere uygun olmalı ve lot ve renk farkından doğabilecek sorunlarda malın tamamı iade edilecektir.</span></li><li class=\"li1\"><span class=\"s2\">Firmamız teyit edilen ancak teslim tarihinde gelmeyen &uuml;r&uuml;nleri iptal etme yetkisine sahip olup, termin tarihinden sonra gecikilen her g&uuml;n i&ccedil;in %10&rsquo;u kadar bir gecikme cezası uygulayabilir.</span></li><li class=\"li1\"><span class=\"s2\">&Uuml;r&uuml;n sevkiyatı hafta i&ccedil;i saat: 18:00 den &ouml;nce yapılmalıdır. (Acil durumlar hari&ccedil;)</span></li><li class=\"li1\"><span class=\"s2\">Tarafımıza gelecek olan &Uuml;r&uuml;nlerde herhangi bir kanserojen madde, fitalat, AZO boyar maddelerinin bulunmaması gerekmektedir. Aksi halde tedarik&ccedil;iye r&uuml;cu edilecektir.</span></li></ol><p></p>");

            session.persist(felisLabs);
        }

        User felisLabsSuperAdminUser = (User) session.createCriteria(User.class).add(Restrictions.eq("email", "admin@felisLabs.com")).uniqueResult();

        if (felisLabsSuperAdminUser == null) {
            felisLabsSuperAdminUser = new User();
            felisLabsSuperAdminUser.setName("Super Admin");
            felisLabsSuperAdminUser.setSurname("User");
            felisLabsSuperAdminUser.setCode("SAU");
            felisLabsSuperAdminUser.setMobilePhone("2343423434");
            felisLabsSuperAdminUser.setInternalPhoneCode("0999");
            felisLabsSuperAdminUser.setEmail("admin@felisLabs.com");
            felisLabsSuperAdminUser.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");  // 1234
            felisLabsSuperAdminUser.setCompany(felisLabs);

            session.persist(felisLabsSuperAdminUser);
        } else {
            felisLabsSuperAdminUser.setName("Super Admin");
            felisLabsSuperAdminUser.setSurname("User");
            felisLabsSuperAdminUser.setCode("SAU");
            felisLabsSuperAdminUser.setMobilePhone("2343423434");
            felisLabsSuperAdminUser.setInternalPhoneCode("0999");
            felisLabsSuperAdminUser.setEmail("admin@felisLabs.com");
            felisLabsSuperAdminUser.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4");  // 1234
            felisLabsSuperAdminUser.setCompany(felisLabs);

            session.persist(felisLabsSuperAdminUser);
        }
        MaterialType materialTypeKoli = (MaterialType) session.createCriteria(MaterialType.class).add(Restrictions.eq("title", "KOLİ")).add(Restrictions.eq("company", felisLabs)).uniqueResult();
        MaterialType materialTypeKutu = (MaterialType) session.createCriteria(MaterialType.class).add(Restrictions.eq("title", "KUTU")).add(Restrictions.eq("company", felisLabs)).uniqueResult();

        if (materialTypeKoli == null) {
            materialTypeKoli = new MaterialType();
            materialTypeKoli.setTitle("KOLI");
            materialTypeKoli.setPutative(true);
            materialTypeKoli.setUpdater(felisLabsSuperAdminUser);
            materialTypeKoli.setGroupName("AMBALAJ");
            materialTypeKoli.setUpdateDate(new DateTime());
            materialTypeKoli.setCompany(felisLabs);
            session.persist(materialTypeKoli);
        } else {
            materialTypeKoli.setTitle("KOLI");
            materialTypeKoli.setPutative(true);
            materialTypeKoli.setGroupName("AMBALAJ");
            materialTypeKoli.setUpdateDate(new DateTime());
            materialTypeKoli.setUpdater(felisLabsSuperAdminUser);
            materialTypeKoli.setCompany(felisLabs);
            session.persist(materialTypeKoli);
        }

        if (materialTypeKutu == null) {
            materialTypeKutu = new MaterialType();
            materialTypeKutu.setTitle("KUTU");
            materialTypeKutu.setPutative(true);
            materialTypeKutu.setGroupName("AMBALAJ");
            materialTypeKutu.setUpdater(felisLabsSuperAdminUser);
            materialTypeKutu.setCompany(felisLabs);
            materialTypeKutu.setUpdateDate(new DateTime());
            session.persist(materialTypeKutu);
        } else {
            materialTypeKutu.setTitle("KUTU");
            materialTypeKutu.setGroupName("AMBALAJ");
            materialTypeKutu.setPutative(true);
            materialTypeKutu.setUpdater(felisLabsSuperAdminUser);
            materialTypeKutu.setCompany(felisLabs);
            materialTypeKutu.setUpdateDate(new DateTime());
            session.persist(materialTypeKutu);
        }

        MaterialType materialTypeIlac = (MaterialType) session.createCriteria(MaterialType.class).add(Restrictions.eq("title", "ILAC")).add(Restrictions.eq("company", felisLabs)).uniqueResult();

        if (materialTypeIlac == null) {
            materialTypeIlac = new MaterialType();
            materialTypeIlac.setTitle("ILAC");
            materialTypeIlac.setPutative(true);
            materialTypeIlac.setUpdater(felisLabsSuperAdminUser);
            materialTypeIlac.setGroupName("AMBALAJ");
            materialTypeIlac.setUpdateDate(new DateTime());
            materialTypeIlac.setCompany(felisLabs);
            session.persist(materialTypeIlac);
        } else {
            materialTypeIlac.setTitle("ILAC");
            materialTypeIlac.setPutative(true);
            materialTypeIlac.setGroupName("AMBALAJ");
            materialTypeIlac.setUpdateDate(new DateTime());
            materialTypeIlac.setUpdater(felisLabsSuperAdminUser);
            materialTypeIlac.setCompany(felisLabs);
            session.persist(materialTypeIlac);
        }
        MaterialType materialTypeTaban = (MaterialType) session.createCriteria(MaterialType.class).add(Restrictions.eq("title", "TABAN")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (materialTypeTaban == null) {
            materialTypeTaban = new MaterialType();
            materialTypeTaban.setTitle("TABAN");
            materialTypeTaban.setPutative(true);
            materialTypeTaban.setGroupName("TABAN");
            materialTypeTaban.setUpdater(felisLabsSuperAdminUser);
            materialTypeTaban.setCompany(felisLabs);
            materialTypeTaban.setUpdateDate(new DateTime());
            session.persist(materialTypeTaban);
        } else {
            materialTypeTaban.setTitle("TABAN");
            materialTypeTaban.setPutative(true);
            materialTypeTaban.setGroupName("TABAN");
            materialTypeTaban.setUpdater(felisLabsSuperAdminUser);
            materialTypeTaban.setCompany(felisLabs);
            materialTypeTaban.setUpdateDate(new DateTime());
            session.persist(materialTypeTaban);
        }
        MaterialType materialTypeFuspet = (MaterialType) session.createCriteria(MaterialType.class).add(Restrictions.eq("title", "FUSPET")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (materialTypeFuspet == null) {
            materialTypeFuspet = new MaterialType();
            materialTypeFuspet.setTitle("FUSPET");
            materialTypeFuspet.setPutative(true);
            materialTypeFuspet.setGroupName("TABAN");
            materialTypeFuspet.setUpdater(felisLabsSuperAdminUser);
            materialTypeFuspet.setCompany(felisLabs);
            materialTypeFuspet.setUpdateDate(new DateTime());
            session.persist(materialTypeFuspet);
        } else {
            materialTypeFuspet.setTitle("FUSPET");
            materialTypeFuspet.setPutative(true);
            materialTypeFuspet.setGroupName("TABAN");
            materialTypeFuspet.setUpdater(felisLabsSuperAdminUser);
            materialTypeFuspet.setCompany(felisLabs);
            materialTypeFuspet.setUpdateDate(new DateTime());
            session.persist(materialTypeFuspet);
        }
        MaterialType materialTypeMostra = (MaterialType) session.createCriteria(MaterialType.class).add(Restrictions.eq("title", "MOSTRA")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (materialTypeMostra == null) {
            materialTypeMostra = new MaterialType();
            materialTypeMostra.setTitle("MOSTRA");
            materialTypeMostra.setPutative(true);
            materialTypeMostra.setGroupName("TABAN");
            materialTypeMostra.setUpdater(felisLabsSuperAdminUser);
            materialTypeMostra.setCompany(felisLabs);
            materialTypeMostra.setUpdateDate(new DateTime());
            session.persist(materialTypeMostra);
        } else {
            materialTypeMostra.setTitle("MOSTRA");
            materialTypeMostra.setPutative(true);
            materialTypeMostra.setGroupName("TABAN");
            materialTypeMostra.setUpdater(felisLabsSuperAdminUser);
            materialTypeMostra.setCompany(felisLabs);
            materialTypeMostra.setUpdateDate(new DateTime());
            session.persist(materialTypeMostra);
        }
        MaterialType materialTypeMostraEtiketi = (MaterialType) session.createCriteria(MaterialType.class).add(Restrictions.eq("title", "MOSTRA ETIKETI")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (materialTypeMostraEtiketi == null) {
            materialTypeMostraEtiketi = new MaterialType();
            materialTypeMostraEtiketi.setTitle("MOSTRA ETIKETI");
            materialTypeMostraEtiketi.setPutative(true);
            materialTypeMostraEtiketi.setGroupName("TABAN");
            materialTypeMostraEtiketi.setUpdater(felisLabsSuperAdminUser);
            materialTypeMostraEtiketi.setCompany(felisLabs);
            materialTypeMostraEtiketi.setUpdateDate(new DateTime());
            session.persist(materialTypeMostraEtiketi);
        } else {
            materialTypeMostraEtiketi.setTitle("MOSTRA ETIKETI");
            materialTypeMostraEtiketi.setPutative(true);
            materialTypeMostraEtiketi.setGroupName("TABAN");
            materialTypeMostraEtiketi.setUpdater(felisLabsSuperAdminUser);
            materialTypeMostraEtiketi.setCompany(felisLabs);
            materialTypeMostraEtiketi.setUpdateDate(new DateTime());
            session.persist(materialTypeMostraEtiketi);
        }
        StockType stockTypeTaban = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "TBN")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeTaban == null) {
            stockTypeTaban = new StockType();
            stockTypeTaban.setCode("TBN");
            stockTypeTaban.setName("TABAN");
            stockTypeTaban.setDescription("TABAN");
            stockTypeTaban.setUpdater(felisLabsSuperAdminUser);
            stockTypeTaban.setCompany(felisLabs);
            stockTypeTaban.setUpdateDate(new DateTime());
            session.persist(stockTypeTaban);
        } else {
            stockTypeTaban.setCode("TBN");
            stockTypeTaban.setName("TABAN");
            stockTypeTaban.setDescription("TABAN");
            stockTypeTaban.setUpdater(felisLabsSuperAdminUser);
            stockTypeTaban.setCompany(felisLabs);
            stockTypeTaban.setUpdateDate(new DateTime());
            session.persist(stockTypeTaban);
        }
        StockType stockTypeKutu = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "KUT")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeKutu == null) {
            stockTypeKutu = new StockType();
            stockTypeKutu.setCode("KUT");
            stockTypeKutu.setName("KUTU");
            stockTypeKutu.setDescription("KUTU");
            stockTypeKutu.setUpdater(felisLabsSuperAdminUser);
            stockTypeKutu.setCompany(felisLabs);
            stockTypeKutu.setUpdateDate(new DateTime());
            session.persist(stockTypeKutu);
        } else {
            stockTypeKutu.setCode("KUT");
            stockTypeKutu.setName("KUTU");
            stockTypeKutu.setDescription("KUTU");
            stockTypeKutu.setUpdater(felisLabsSuperAdminUser);
            stockTypeKutu.setCompany(felisLabs);
            stockTypeKutu.setUpdateDate(new DateTime());
            session.persist(stockTypeKutu);
        }
        StockType stockTypeMostraEtiket = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "MSET")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeMostraEtiket == null) {
            stockTypeMostraEtiket = new StockType();
            stockTypeMostraEtiket.setCode("MSET");
            stockTypeMostraEtiket.setName("MOSTRA ETIKETI");
            stockTypeMostraEtiket.setDescription("MOSTRA ETIKETI");
            stockTypeMostraEtiket.setUpdater(felisLabsSuperAdminUser);
            stockTypeMostraEtiket.setCompany(felisLabs);
            stockTypeMostraEtiket.setUpdateDate(new DateTime());
            session.persist(stockTypeMostraEtiket);
        } else {
            stockTypeMostraEtiket.setCode("MSET");
            stockTypeMostraEtiket.setName("MOSTRA ETIKETI");
            stockTypeMostraEtiket.setDescription("MOSTRA ETIKETI");
            stockTypeMostraEtiket.setUpdater(felisLabsSuperAdminUser);
            stockTypeMostraEtiket.setCompany(felisLabs);
            stockTypeMostraEtiket.setUpdateDate(new DateTime());
            session.persist(stockTypeMostraEtiket);
        }
        StockType stockTypeFuspet = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "FSPT")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeFuspet == null) {
            stockTypeFuspet = new StockType();
            stockTypeFuspet.setCode("FSPT");
            stockTypeFuspet.setName("FUSPET");
            stockTypeFuspet.setDescription("FUSPET");
            stockTypeFuspet.setUpdater(felisLabsSuperAdminUser);
            stockTypeFuspet.setCompany(felisLabs);
            stockTypeFuspet.setUpdateDate(new DateTime());
            session.persist(stockTypeFuspet);
        } else {
            stockTypeFuspet.setCode("FSPT");
            stockTypeFuspet.setName("FUSPET");
            stockTypeFuspet.setDescription("FUSPET");
            stockTypeFuspet.setUpdater(felisLabsSuperAdminUser);
            stockTypeFuspet.setCompany(felisLabs);
            stockTypeFuspet.setUpdateDate(new DateTime());
            session.persist(stockTypeFuspet);
        }
        StockType stockTypeMostra = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "MSTR")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeMostra == null) {
            stockTypeMostra = new StockType();
            stockTypeMostra.setCode("MSTR");
            stockTypeMostra.setName("MOSTRA");
            stockTypeMostra.setDescription("MOSTRA");
            stockTypeMostra.setUpdater(felisLabsSuperAdminUser);
            stockTypeMostra.setCompany(felisLabs);
            stockTypeMostra.setUpdateDate(new DateTime());
            session.persist(stockTypeMostra);
        } else {
            stockTypeMostra.setCode("MSTR");
            stockTypeMostra.setName("MOSTRA");
            stockTypeMostra.setDescription("MOSTRA");
            stockTypeMostra.setUpdater(felisLabsSuperAdminUser);
            stockTypeMostra.setCompany(felisLabs);
            stockTypeMostra.setUpdateDate(new DateTime());
            session.persist(stockTypeMostra);
        }
        StockType stockTypeIlac = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "ILC")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeIlac == null) {
            stockTypeIlac = new StockType();
            stockTypeIlac.setCode("ILC");
            stockTypeIlac.setName("ILAC");
            stockTypeIlac.setDescription("ILAC");
            stockTypeIlac.setUpdater(felisLabsSuperAdminUser);
            stockTypeIlac.setCompany(felisLabs);
            stockTypeIlac.setUpdateDate(new DateTime());
            session.persist(stockTypeIlac);
        } else {
            stockTypeIlac.setCode("ILC");
            stockTypeIlac.setName("ILAC");
            stockTypeIlac.setDescription("ILAC");
            stockTypeIlac.setUpdater(felisLabsSuperAdminUser);
            stockTypeIlac.setCompany(felisLabs);
            stockTypeIlac.setUpdateDate(new DateTime());
            session.persist(stockTypeIlac);
        }
        StockType stockTypeKoli = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "KOLI")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeKoli == null) {
            stockTypeKoli = new StockType();
            stockTypeKoli.setCode("KOLI");
            stockTypeKoli.setName("KOLI");
            stockTypeKoli.setDescription("KOLI");
            stockTypeKoli.setUpdater(felisLabsSuperAdminUser);
            stockTypeKoli.setCompany(felisLabs);
            stockTypeKoli.setUpdateDate(new DateTime());
            session.persist(stockTypeKoli);
        } else {
            stockTypeKoli.setCode("KOLI");
            stockTypeKoli.setName("KOLI");
            stockTypeKoli.setDescription("KOLI");
            stockTypeKoli.setUpdater(felisLabsSuperAdminUser);
            stockTypeKoli.setCompany(felisLabs);
            stockTypeKoli.setUpdateDate(new DateTime());
            session.persist(stockTypeKoli);
        }

        StockType stockTypeAyakkabi = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "M_AYK")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeAyakkabi == null) {
            stockTypeAyakkabi = new StockType();
            stockTypeAyakkabi.setCode("M_AYK");
            stockTypeAyakkabi.setName("MAMÜL AYAKKABI");
            stockTypeAyakkabi.setDescription("BİTMİŞ MAMÜL");
            stockTypeAyakkabi.setUpdater(felisLabsSuperAdminUser);
            stockTypeAyakkabi.setCompany(felisLabs);
            stockTypeAyakkabi.setUpdateDate(new DateTime());
            session.persist(stockTypeAyakkabi);
        } else {
            stockTypeAyakkabi.setCode("M_AYK");
            stockTypeAyakkabi.setName("MAMÜL AYAKKABI");
            stockTypeAyakkabi.setDescription("BİTMİŞ MAMÜL");
            stockTypeAyakkabi.setUpdater(felisLabsSuperAdminUser);
            stockTypeAyakkabi.setCompany(felisLabs);
            stockTypeAyakkabi.setUpdateDate(new DateTime());
            session.persist(stockTypeAyakkabi);
        }

        StockType stockTypeSaya = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "M_SAY")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeSaya == null) {
            stockTypeSaya = new StockType();
            stockTypeSaya.setCode("M_SAY");
            stockTypeSaya.setName("MAMÜL SAYA");
            stockTypeSaya.setDescription("BİTMİŞ MAMÜL");
            stockTypeSaya.setUpdater(felisLabsSuperAdminUser);
            stockTypeSaya.setCompany(felisLabs);
            stockTypeSaya.setUpdateDate(new DateTime());
            session.persist(stockTypeSaya);
        } else {
            stockTypeSaya.setCode("M_SAY");
            stockTypeSaya.setName("MAMÜL SAYA");
            stockTypeSaya.setDescription("BİTMİŞ MAMÜL");
            stockTypeSaya.setUpdater(felisLabsSuperAdminUser);
            stockTypeSaya.setCompany(felisLabs);
            stockTypeSaya.setUpdateDate(new DateTime());
            session.persist(stockTypeSaya);
        }

        StockType stockTypeMamulTaban = (StockType) session.createCriteria(StockType.class).add(Restrictions.eq("code", "M_TBN")).add(Restrictions.eq("company", felisLabsSuperAdminUser)).uniqueResult();

        if (stockTypeMamulTaban == null) {
            stockTypeMamulTaban = new StockType();
            stockTypeMamulTaban.setCode("M_TBN");
            stockTypeMamulTaban.setName("MAMÜL TABAN");
            stockTypeMamulTaban.setDescription("BİTMİŞ MAMÜL");
            stockTypeMamulTaban.setUpdater(felisLabsSuperAdminUser);
            stockTypeMamulTaban.setCompany(felisLabs);
            stockTypeMamulTaban.setUpdateDate(new DateTime());
            session.persist(stockTypeMamulTaban);
        } else {
            stockTypeMamulTaban.setCode("M_TBN");
            stockTypeMamulTaban.setName("MAMÜL TABAN");
            stockTypeMamulTaban.setDescription("BİTMİŞ MAMÜL");
            stockTypeMamulTaban.setUpdater(felisLabsSuperAdminUser);
            stockTypeMamulTaban.setCompany(felisLabs);
            stockTypeMamulTaban.setUpdateDate(new DateTime());
            session.persist(stockTypeMamulTaban);
        }

        Role felisLabsAdminRole = (Role) session.createCriteria(Role.class).add(Restrictions.eq("code", "SAUADMIN")).uniqueResult();

        if (felisLabsAdminRole == null) {
            felisLabsAdminRole = new Role();
            felisLabsAdminRole.setName("SAU-Admin Role");
            felisLabsAdminRole.setCode("SAUADMIN");
            felisLabsAdminRole.setCompany(felisLabs);
            session.persist(felisLabsAdminRole);
        } else {
            felisLabsAdminRole.setName("SAU-Admin Role");
            felisLabsAdminRole.setCode("SAUADMIN");
            felisLabsAdminRole.setCompany(felisLabs);
            session.persist(felisLabsAdminRole);
        }

        List<Menu> menus = session.createCriteria(Menu.class).add(Restrictions.eq("projectCode", "MRP")).list();
        for (Menu menu : menus) {
            RoleMenu felisLabsRoleMenu = (RoleMenu) session.createCriteria(RoleMenu.class)
                    .add(Restrictions.eq("role.oid", felisLabsAdminRole.getOid()))
                    .add(Restrictions.eq("menu.oid", menu.getOid()))
                    .add(Restrictions.eq("company.oid", felisLabs.getOid()))
                    .add(Restrictions.eq("deleteStatus", false))
                    .uniqueResult();
            if (felisLabsRoleMenu == null) {
                felisLabsRoleMenu = new RoleMenu();
                felisLabsRoleMenu.setRole(felisLabsAdminRole);
                felisLabsRoleMenu.setMenu(menu);
                felisLabsRoleMenu.setCompany(felisLabs);
                session.persist(felisLabsRoleMenu);
            } else {
                felisLabsRoleMenu.setRole(felisLabsAdminRole);
                felisLabsRoleMenu.setMenu(menu);
                felisLabsRoleMenu.setCompany(felisLabs);
                session.persist(felisLabsRoleMenu);
            }
        }

        List<Permission> permissionList = session.createCriteria(Permission.class).add(Restrictions.eq("projectCode", "MRP")).list();
        for (Permission permission : permissionList) {
            RolePermission felisLabsRolePermission = (RolePermission) session.createCriteria(RolePermission.class)
                    .add(Restrictions.eq("role.oid", felisLabsAdminRole.getOid()))
                    .add(Restrictions.eq("permission.oid", permission.getOid()))
                    .add(Restrictions.eq("company.oid", felisLabs.getOid()))
                    .add(Restrictions.eq("deleteStatus", false)).uniqueResult();

            if (felisLabsRolePermission == null) {
                felisLabsRolePermission = new RolePermission();
                felisLabsRolePermission.setRole(felisLabsAdminRole);
                felisLabsRolePermission.setPermission(permission);
                felisLabsRolePermission.setCompany(felisLabs);
                session.persist(felisLabsRolePermission);
            } else {
                felisLabsRolePermission.setRole(felisLabsAdminRole);
                felisLabsRolePermission.setPermission(permission);
                felisLabsRolePermission.setCompany(felisLabs);
                session.persist(felisLabsRolePermission);
            }
        }

        RoleUser felisLabsRoleUser = (RoleUser) session.createCriteria(RoleUser.class)
                .add(Restrictions.eq("user.oid", felisLabsSuperAdminUser.getOid()))
                .add(Restrictions.eq("role.oid", felisLabsAdminRole.getOid()))
                .add(Restrictions.eq("company.oid", felisLabs.getOid()))
                .add(Restrictions.eq("deleteStatus", false)).uniqueResult();

        if (felisLabsRoleUser == null) {
            felisLabsRoleUser = new RoleUser();
            felisLabsRoleUser.setRole(felisLabsAdminRole);
            felisLabsRoleUser.setUser(felisLabsSuperAdminUser);
            felisLabsRoleUser.setCompany(felisLabs);

            session.persist(felisLabsRoleUser);
        } else {
            felisLabsRoleUser.setRole(felisLabsAdminRole);
            felisLabsRoleUser.setUser(felisLabsSuperAdminUser);
            felisLabsRoleUser.setCompany(felisLabs);

            session.persist(felisLabsRoleUser);
        }

        MrpSetting mrpSetting = (MrpSetting) session.createCriteria(MrpSetting.class)
                .add(Restrictions.eq("deleteStatus", false))
                .add(Restrictions.eq("company.oid", felisLabs.getOid())).uniqueResult();

        if (mrpSetting == null) {
            mrpSetting = new MrpSetting();
            mrpSetting.setCompany(felisLabs);
            session.persist(mrpSetting);
        } else {
            mrpSetting.setCompany(felisLabs);
            session.persist(mrpSetting);
        }

        session.flush();
    }
}
