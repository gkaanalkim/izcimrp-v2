package com.felislabs.izci.auth;

import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.*;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gunerkaanalkim on 11/09/15.
 */
public class AuthUtil {
    RoleDAO roleDAO;
    UserDAO userDAO;
    PermissionDAO permissionDAO;
    RoleUserDAO roleUserDAO;
    RolePermissionDAO rolePermissionDAO;
    UserTokenDAO userTokenDAO;
    SystemActivityDAO systemActivityDAO;

    public AuthUtil(RoleDAO roleDAO, UserDAO userDAO, PermissionDAO permissionDAO, RoleUserDAO roleUserDAO,
                    RolePermissionDAO rolePermissionDAO, UserTokenDAO userTokenDAO,
                    SystemActivityDAO systemActivityDAO) {
        this.roleDAO = roleDAO;
        this.userDAO = userDAO;
        this.permissionDAO = permissionDAO;
        this.roleUserDAO = roleUserDAO;
        this.rolePermissionDAO = rolePermissionDAO;
        this.userTokenDAO = userTokenDAO;
        this.systemActivityDAO = systemActivityDAO;
    }

    public UserToken isValidToken(String token) {
        if (token == null) {
            return null;
        } else {
            UserToken userToken = userTokenDAO.findByToken(token);

            if(userToken != null) {
                if (userToken.getStatus().equals(Boolean.FALSE)) {
                    return null;
                } else {
                    return userToken;
                }
            } else {
                return null;
            }
        }
    }

    public Boolean hasPermission(User user, PermissionEnum permissionEnum) {
        RoleUser roleUser = roleUserDAO.findByUser(user);
        List<RolePermission> rolePermissionList = rolePermissionDAO.getByRole(roleUser.getRole());

        for (RolePermission rolePermission : rolePermissionList) {
            if (rolePermission.getPermission().getCode().equals(permissionEnum.getPermissionAlias())) {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public Boolean hasCommonPermission(User user, ArrayList<String> permissionEnum) {
        RoleUser roleUser = roleUserDAO.findByUser(user);
        List<RolePermission> rolePermissionList = rolePermissionDAO.getByRole(roleUser.getRole());

        for (RolePermission rolePermission : rolePermissionList) {
            for (String permissionAlias : permissionEnum) {
                if (rolePermission.getPermission().getCode().equals(permissionAlias)) {
                    return Boolean.TRUE;
                }
            }
        }

        return Boolean.FALSE;
    }

    public Boolean hasAccountantPermission(User user, AccountingPermissionEnum permissionEnum) {
        RoleUser roleUser = roleUserDAO.findByUser(user);
        List<RolePermission> rolePermissionList = rolePermissionDAO.getByRole(roleUser.getRole());

        for (RolePermission rolePermission : rolePermissionList) {
            if (rolePermission.getPermission().getCode().equals(permissionEnum.getPermissionAlias())) {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public SystemActivity setSystemActivity(User user, Company company, String process, DateTime creationDateTime) {
        SystemActivity systemActivity = new SystemActivity();
        systemActivity.setUser(user);
        systemActivity.setCompany(company);
        systemActivity.setProcess(process);

        systemActivityDAO.create(systemActivity);

        return systemActivity;
    }
}
