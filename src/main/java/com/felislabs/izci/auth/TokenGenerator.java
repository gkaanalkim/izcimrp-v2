package com.felislabs.izci.auth;
import java.util.UUID;

/**
 * Created by gunerkaanalkim on 09/09/15.
 */
public class TokenGenerator {
    public static UUID generate() {
        return UUID.randomUUID();
    }

    public static UUID generateByString(String salt) {
        return UUID.randomUUID().fromString(salt);
    }
}
