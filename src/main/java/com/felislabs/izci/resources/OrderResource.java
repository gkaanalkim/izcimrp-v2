package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.ModelOrderDTO;
import com.felislabs.izci.representation.dto.ModelOrderSummary;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import net.coobird.thumbnailator.Thumbnails;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 29/01/2017.
 */
@Path("order")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class OrderResource {
    AuthUtil authUtil;
    ModelDAO modelDAO;
    ModelAlarmDAO modelAlarmDAO;
    ModelAssortDAO modelAssortDAO;
    ModelOrderDAO modelOrderDAO;
    CustomerShipmentDAO customerShipmentDAO;
    ModelMaterialDAO modelMaterialDAO;

    public OrderResource(AuthUtil authUtil, ModelDAO modelDAO, ModelAlarmDAO modelAlarmDAO, ModelAssortDAO modelAssortDAO, ModelOrderDAO modelOrderDAO, CustomerShipmentDAO customerShipmentDAO, ModelMaterialDAO modelMaterialDAO) {
        this.authUtil = authUtil;
        this.modelDAO = modelDAO;
        this.modelAlarmDAO = modelAlarmDAO;
        this.modelAssortDAO = modelAssortDAO;
        this.modelOrderDAO = modelOrderDAO;
        this.customerShipmentDAO = customerShipmentDAO;
        this.modelMaterialDAO = modelMaterialDAO;
    }

    @POST
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String modelOid = data.get("modelOid");
                String orderQuantity = data.get("orderQuantity");
                String shipmentInfo = data.get("shipmentInfo");
                String selectedShipmentInfoCode = data.get("selectedShipmentInfoCode");
                String terminDate = data.get("terminDate");

                String assorts = data.get("assort");
                String alarms = data.get("alarm");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime formattedTerminDate = formatter.parseDateTime(terminDate);

                Model model = modelDAO.getByOid(modelOid, company);
                CustomerShipment customerShipment = customerShipmentDAO.getByCode(selectedShipmentInfoCode, company);

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                if (modelOrder == null) {
                    modelOrder = new ModelOrder();
                    modelOrder.setCompany(company);
                    modelOrder.setUpdater(user);
                    modelOrder.setUpdateDate(new DateTime());
                    modelOrder.setModel(model);
                    modelOrder.setOrderQuantity(Integer.valueOf(orderQuantity));
                    modelOrder.setShipmentInfo(shipmentInfo);
                    modelOrder.setCustomerShipment(customerShipment);
                    modelOrder.setTerminDate(formattedTerminDate);

                    modelOrderDAO.create(modelOrder);
                    model.setIsOrderEntry(true);
                } else {
                    modelOrder.setCompany(company);
                    modelOrder.setUpdater(user);
                    modelOrder.setUpdateDate(new DateTime());
                    modelOrder.setModel(model);
                    modelOrder.setOrderQuantity(Integer.valueOf(orderQuantity));
                    modelOrder.setShipmentInfo(shipmentInfo);
                    modelOrder.setCustomerShipment(customerShipment);
                    modelOrder.setTerminDate(formattedTerminDate);

                    modelOrderDAO.update(modelOrder);
                    model.setIsOrderEntry(true);
                }

                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(model, company);

                //  Model Assorts
                //  Delete old assorts info if exist
                List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelOrder, company);
                if (modelAssorts.size() != 0) {
                    for (ModelAssort modelAssort : modelAssorts) {
                        modelAssortDAO.delete(modelAssort);
                    }
                }

                JSONArray modelAssortJsonArray = new JSONArray(assorts);

                for (int i = 0; i < modelAssortJsonArray.length(); i++) {
                    JSONObject jsonObject = modelAssortJsonArray.getJSONObject(i);

                    Integer boxNumber = Integer.valueOf(jsonObject.getString("boxNumber"));
                    Integer number = Integer.valueOf(jsonObject.getString("number"));
                    Integer assort = Integer.valueOf(jsonObject.getString("assort"));
                    String uid = jsonObject.getString("uid");

                    ModelAssort modelAssort = new ModelAssort();
                    modelAssort.setCompany(company);
                    modelAssort.setUpdater(user);
                    modelAssort.setUpdateDate(new DateTime());
                    modelAssort.setModelOrder(modelOrder);
                    modelAssort.setBoxNumber(boxNumber);
                    modelAssort.setNumber(number);
                    modelAssort.setAssort(assort);
                    modelAssort.setUid(uid);

                    modelAssortDAO.create(modelAssort);
                }

                //  Model Alarm
                //  Delete old alarms info if exist
                List<ModelAlarm> modelAlarms = modelAlarmDAO.getByModelOrder(modelOrder, company);
                if (modelAlarms.size() != 0) {
                    for (ModelAlarm modelAlarm : modelAlarms) {
                        modelAlarmDAO.delete(modelAlarm);
                    }
                }
                if (!alarms.equals("null")) {
                    JSONArray modelAlarmJsonArray = new JSONArray(alarms);

                    for (int i = 0; i < modelAlarmJsonArray.length(); i++) {
                        JSONObject jsonObject = modelAlarmJsonArray.getJSONObject(i);

                        Integer boxNumber = Integer.valueOf(jsonObject.getString("boxNumber"));
                        Integer alarmed = Integer.valueOf(jsonObject.getString("alarmed"));
                        Integer withoutAlarmed = Integer.valueOf(jsonObject.getString("withoutAlarm"));
                        String uid = jsonObject.getString("uid");

                        ModelAlarm modelAlarm = new ModelAlarm();
                        modelAlarm.setCompany(company);
                        modelAlarm.setUpdater(user);
                        modelAlarm.setUpdateDate(new DateTime());
                        modelAlarm.setModelOrder(modelOrder);
                        modelAlarm.setBoxNumber(boxNumber);
                        modelAlarm.setAlarmed(alarmed);
                        modelAlarm.setWithoutAlarm(withoutAlarmed);
                        modelAlarm.setUid(uid);

                        modelAlarmDAO.create(modelAlarm);
                    }
                }

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modele sipariş girildi/güncellendi. ", new DateTime());

                return Response.ok().entity(modelOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readOrder")
    @UnitOfWork
    public Response readOrder(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                return Response.ok().entity(modelOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readOrderSummary")
    @UnitOfWork
    public Response readOrderSummary(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                Integer orderQuantity = 0;

                if (modelOrder != null) {
                    orderQuantity = modelOrder.getOrderQuantity();
                }

                ModelOrderSummary modelOrderSummary = new ModelOrderSummary();
                modelOrderSummary.setOrderQuantity(orderQuantity);

                return Response.ok().entity(modelOrderSummary).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readAlarmInfo")
    @UnitOfWork
    public Response readAlarmInfo(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                List<ModelAlarm> modelAlarms = modelAlarmDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());

                return Response.ok().entity(modelAlarms).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readAssortInfo")
    @UnitOfWork
    public Response readAssortInfo(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());

                return Response.ok().entity(modelAssorts).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readAssortAndAlarmInfo")
    @UnitOfWork
    public Response readAssortAndAlarmInfo(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                ModelOrderDTO modelOrderDTO = new ModelOrderDTO();

                if (modelOrder == null) {
                    modelOrderDTO.setModelAlarms(null);
                    modelOrderDTO.setModelAssorts(null);
                } else {
                    List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());
                    List<ModelAlarm> modelAlarms = modelAlarmDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());

                    modelOrderDTO.setModelAlarms(modelAlarms);
                    modelOrderDTO.setModelAssorts(modelAssorts);
                }

                return Response.ok().entity(modelOrderDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
