package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.SayaciDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Sayaci;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 28/12/2016.
 */
@Path("sayaci")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SayaciResource {
    AuthUtil authUtil;
    SayaciDAO sayaciDAO;

    public SayaciResource(AuthUtil authUtil, SayaciDAO sayaciDAO) {
        this.authUtil = authUtil;
        this.sayaciDAO = sayaciDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYACI_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                Sayaci sayaci = sayaciDAO.getByCode(code, company);

                if (sayaci == null) {
                    sayaci = new Sayaci();
                    sayaci.setName(name);
                    sayaci.setCode(code);
                    sayaci.setDescription(description);
                    sayaci.setCompany(company);
                    sayaci.setUpdateDate(new DateTime());
                    sayaci.setUpdater(userToken.getUser());
                    sayaciDAO.create(sayaci);

                    authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), sayaci.getName() + " adlı sayacı oluşturuldu. ", new DateTime());

                    return Response.ok(sayaci).build();
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYACI_READ)) {
                Company company = userToken.getUser().getCompany();

                List<Sayaci> sayaciList = sayaciDAO.getAll(company);

                return Response.ok().entity(sayaciList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByName")
    @UnitOfWork
    public Response readByName(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYACI_READ)) {
                String name = data.get("name");

                Company company = userToken.getUser().getCompany();

                List<Sayaci> sayaciList = sayaciDAO.readByName(name, company);

                return Response.ok().entity(sayaciList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYACI_READ)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();
                Sayaci sayaci = sayaciDAO.getByOid(oid, company);

                return Response.ok().entity(sayaci).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYACI_CREATE)) {
                String oid = data.get("oid");
                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                Sayaci sayaci = sayaciDAO.getByOid(oid, company);

                Sayaci sayaciByCode = sayaciDAO.getByCode(code, company);

                if (sayaciByCode != null && !sayaciByCode.getCode().equals(sayaci.getCode())) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    sayaci.setName(name);
                    sayaci.setCode(code);
                    sayaci.setDescription(description);
                    sayaci.setCompany(company);
                    sayaci.setUpdateDate(new DateTime());
                    sayaci.setUpdater(userToken.getUser());
                    sayaciDAO.update(sayaci);

                    authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), sayaci.getName() + " adlı sayacı güncellendi. ", new DateTime());

                    return Response.ok(sayaci).build();
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYACI_DELETE)) {
                Company company = userToken.getUser().getCompany();
                String oid = data.get("oid");

                Sayaci sayaci = sayaciDAO.getByOid(oid, company);
                sayaci.setDeleteStatus(true);

                sayaciDAO.update(sayaci);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), sayaci.getName() + " adlı sayacı silindi. ", new DateTime());

                return Response.ok(sayaci).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
