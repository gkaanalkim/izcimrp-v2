package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 19/09/15.
 */
@Path("menu")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MenuResource {
    UserDAO userDAO;
    MenuDAO menuDAO;
    RoleDAO roleDAO;
    RoleMenuDAO roleMenuDAO;
    RoleUserDAO roleUserDAO;
    AuthUtil authUtil;
    CompanyDAO companyDAO;

    public MenuResource(UserDAO userDAO,
                        MenuDAO menuDAO,
                        RoleDAO roleDAO,
                        RoleMenuDAO roleMenuDAO,
                        RoleUserDAO roleUserDAO,
                        AuthUtil authUtil,
                        CompanyDAO companyDAO) {
        this.userDAO = userDAO;
        this.menuDAO = menuDAO;
        this.roleDAO = roleDAO;
        this.roleMenuDAO = roleMenuDAO;
        this.authUtil = authUtil;
        this.roleUserDAO = roleUserDAO;
        this.companyDAO = companyDAO;
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_DELETE)) {

                Company company = userToken.getUser().getCompany();
                String oid = menuData.get("oid");
                Menu menu = menuDAO.getById(oid);
                int order = menu.getMenuOrder();
                menuDAO.delete(menu);

                List<Menu> menus = menuDAO.getAll(Menu.class);

                for (Menu menu2 : menus) {
                    if (order < menu2.getMenuOrder()) {
                        menu2.setMenuOrder(menu2.getMenuOrder() - 1);
                        menuDAO.update(menu2);
                    }
                }

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), menu.getName() + "' Adlı menü silindi. ", new DateTime());

                return Response.ok(menu).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getAllMenus")
    @UnitOfWork
    public Response getAllMenus(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_CREATE)) {
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                String userCode = user.getCode();
                List<Menu> menus;
                if (userCode.equals("SAU")) {
                    menus = menuDAO.getAllByProject(project);
                } else {
                    menus = menuDAO.getAllExceptCompany(project);
                }
                return Response.ok(menus).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getMenuOfRole")
    @UnitOfWork
    public Response getMenuOfRole(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                String roleOid = menuData.get("role");
                Role role = roleDAO.findById(roleOid);
                Company company = userToken.getUser().getCompany();
                List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(project, role, role.getCompany());

                List<Menu> menus = new LinkedList<>();

                for (RoleMenu roleMenu : roleMenuList) {
                    menus.add(roleMenu.getMenu());
                }

                return Response.ok(menus).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getMenuOfUser")
    @UnitOfWork
    public Response getMenuOfUser(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String userOid = menuData.get("user");

                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                User user = userDAO.getById(userOid, userToken.getUser().getCompany());

                RoleUser roleUser = roleUserDAO.findByUser(user);
                Company company = userToken.getUser().getCompany();
                List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(project, roleUser.getRole(), company);

                return Response.ok(roleMenuList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getUserMenu")
    @UnitOfWork
    public Response getUserMenu(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                RoleUser roleUser = roleUserDAO.findByUser(userToken.getUser());
                Company company = userToken.getUser().getCompany();
                List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(project, roleUser.getRole(), company);

                return Response.ok(roleMenuList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("findMenuByToken")
    @UnitOfWork
    public Response findMenuByToken(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                RoleUser roleUser = roleUserDAO.findByUser(userToken.getUser());
                Company company = userToken.getUser().getCompany();
                List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(project, roleUser.getRole(), company);

                List<Menu> menus = new LinkedList<>();

                for (RoleMenu roleMenu : roleMenuList) {
                    menus.add(roleMenu.getMenu());
                }

                return Response.ok(menus).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("findSubMenu")
    @UnitOfWork
    public Response findSubMenu(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String menuOid = menuData.get("menuOid");
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                RoleUser roleUser = roleUserDAO.findByUser(userToken.getUser());
                Company company = userToken.getUser().getCompany();
                List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(project, roleUser.getRole(), company);

                List<Menu> menus = new LinkedList<>();

                for (RoleMenu roleMenu : roleMenuList) {
                    if (roleMenu.getMenu().getParentOid() != null && roleMenu.getMenu().getParentOid().equals(menuOid)) {
                        menus.add(roleMenu.getMenu());
                    }
                }

                return Response.ok(menus).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("assing")
    @UnitOfWork
    public Response assing(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                String roleOid = menuData.get("role");
                Role role = roleDAO.findById(roleOid);
                String menuOid = menuData.get("menu");
                Menu menu = menuDAO.getByIdAndProject(menuOid, project);

                RoleMenu roleMenu = roleMenuDAO.getByRoleAndMenu(role, menu, role.getCompany());

                if (roleMenu == null) {
                    roleMenu = new RoleMenu();
                    roleMenu.setMenu(menu);
                    roleMenu.setRole(role);
                    roleMenu.setCompany(role.getCompany());
                    roleMenuDAO.create(roleMenu);

                    return Response.ok(roleMenu).build();
                } else {
                    roleMenu.setDeleteStatus(false);
                    roleMenuDAO.update(roleMenu);

                    return Response.ok(roleMenu).build();
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("assingback")
    @UnitOfWork
    public Response assingback(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                String roleOid = menuData.get("role");
                Role role = roleDAO.findById(roleOid);
                String menuOid = menuData.get("menu");
                Menu menu = menuDAO.getByIdAndProject(menuOid, project);

                RoleMenu roleMenu = roleMenuDAO.getByRoleAndMenu(role, menu, role.getCompany());
                roleMenuDAO.delete(roleMenu);

                return Response.ok(roleMenu).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("checkMenu")
    @UnitOfWork
    public Response checkMenu(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                String menuCode = menuData.get("menuCode");
                Company company = userToken.getUser().getCompany();
                RoleUser roleUser = roleUserDAO.findByUser(userToken.getUser());
                Menu menu = menuDAO.getByCode(project, menuCode);

                RoleMenu roleMenu = roleMenuDAO.getByRoleAndMenu(roleUser.getRole(), menu, company);

                return Response.ok(roleMenu.getMenu()).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("setAllMenuToRole/{crmt}/{role}")
    @UnitOfWork
    public Response setAllMenuToUser(@PathParam("crmt") String crmt,
                                     @PathParam("role") String roleOid,
                                     @PathParam("p") String p) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_CREATE)) {
                if (p.equals("1")) {
                    p = "MRP";
                } else if (p.equals("2")) {
                    p = "SOLE";
                } else if (p.equals("3")) {
                    p = "CRM";
                } else if (p.equals("4")) {
                    p = "FINANCE";
                }
                Role role = roleDAO.findById(roleOid);

                List<Menu> menuList = menuDAO.getAllUndeleted(p);

                for (Menu menu : menuList) {

                    RoleMenu roleMenu = roleMenuDAO.getByRoleAndMenu(role, menu, role.getCompany());

                    if (roleMenu == null) {
                        roleMenu = new RoleMenu();

                        roleMenu.setRole(role);
                        roleMenu.setMenu(menu);
                        roleMenu.setCompany(role.getCompany());

                        roleMenuDAO.create(roleMenu);
                    } else {
                        roleMenu.setDeleteStatus(false);
                        roleMenuDAO.update(roleMenu);
                    }
                }

                return Response.ok(roleMenuDAO.getByRole(p, role, role.getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("removeAllMenuToRole/{crmt}/{role}")
    @UnitOfWork
    public Response removeAllMenuToUser(@PathParam("crmt") String crmt, @PathParam("role") String roleOid, @PathParam("p") String p) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_CREATE)) {
                if (p.equals("1")) {
                    p = "MRP";
                } else if (p.equals("2")) {
                    p = "SOLE";
                } else if (p.equals("3")) {
                    p = "CRM";
                } else if (p.equals("4")) {
                    p = "FINANCE";
                }

                Role role = roleDAO.findById(roleOid);
                List<RoleMenu> roleMenuList = roleMenuDAO.getByRole(p, role, role.getCompany());

                for (RoleMenu roleMenu : roleMenuList) {
                    roleMenuDAO.delete(roleMenu);
                }

                return Response.ok(roleMenuList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getById")
    @UnitOfWork
    public Response getById(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String oid = menuData.get("oid");
                Company company = userToken.getUser().getCompany();
                return Response.ok(menuDAO.getById(oid)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByCodeContains")
    @UnitOfWork
    public Response getByCodeContains(Map<String, String> menuData) {
        String token = menuData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MENU_READ)) {
                String project = menuData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "SOLE";
                } else if (project.equals("3")) {
                    project = "CRM";
                } else if (project.equals("4")) {
                    project = "FINANCE";
                }

                String code = menuData.get("code");
                Company company = userToken.getUser().getCompany();

                return Response.ok(menuDAO.getByCodeContains(project, code)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

}
