package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.DepartmentDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Department;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by arricie on 21.03.2017.
 */

@Path("department")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class DepartmentResource {
    AuthUtil authUtil;
    DepartmentDAO departmentDAO;

    public DepartmentResource(AuthUtil authUtil, DepartmentDAO departmentDAO) {
        this.authUtil = authUtil;
        this.departmentDAO = departmentDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.DEPARTMENT_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");
                Company company = userToken.getUser().getCompany();

                Department department = departmentDAO.getByCode(code, company);

                if (department != null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Department departmentByCode = departmentDAO.getByCode(code, userToken.getUser().getCompany());

                    if (departmentByCode != null && !departmentByCode.getCode().equals(department.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        department = new Department();
                        department.setCompany(company);
                        department.setName(name);
                        department.setCode(code);
                        department.setDescription(description);
                        departmentDAO.create(department);

                        return Response.ok().entity(department).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.DEPARTMENT_READ)) {
                List<Department> departmentList = departmentDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(departmentList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.DEPARTMENT_READ)) {
                String oid = data.get("oid");

                Department department = departmentDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(department).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.DEPARTMENT_UPDATE)) {
                String oid = data.get("oid");

                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");
                Company company = userToken.getUser().getCompany();

                Department department = departmentDAO.getByOid(oid, company);
                if (department == null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Department departmentByCode = departmentDAO.getByCode(code, userToken.getUser().getCompany());

                    if (departmentByCode != null && !departmentByCode.getCode().equals(department.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        department.setCompany(company);
                        department.setName(name);
                        department.setCode(code);
                        department.setDescription(description);
                        departmentDAO.update(department);

                        return Response.ok().entity(department).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.DEPARTMENT_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Department department = departmentDAO.getByOid(oid, company);
                department.setDeleteStatus(true);

                departmentDAO.update(department);

                return Response.ok().entity(department).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }


}
