package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.StockAcceptanceDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by dogukanyilmaz on 16/02/2017.
 */
@Path("stockacceptance")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StockAcceptanceResources {
    AuthUtil authUtil;
    CompanyDAO companyDAO;
    StockAcceptanceDAO stockAcceptanceDAO;
    StockDAO stockDAO;
    ModelDAO modelDAO;
    StockDetailDAO stockDetailDAO;
    SupplyRequirementDAO supplyRequirementDAO;

    public StockAcceptanceResources(AuthUtil authUtil, CompanyDAO companyDAO, StockAcceptanceDAO stockAcceptanceDAO, StockDAO stockDAO, ModelDAO modelDAO,
                                    StockDetailDAO stockDetailDAO, SupplyRequirementDAO supplyRequirementDAO) {
        this.authUtil = authUtil;
        this.companyDAO = companyDAO;
        this.stockAcceptanceDAO = stockAcceptanceDAO;
        this.stockDAO = stockDAO;
        this.modelDAO = modelDAO;
        this.stockDetailDAO = stockDetailDAO;
        this.supplyRequirementDAO = supplyRequirementDAO;

    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_ACCEPTANCE_READ)) {
                return Response.ok(stockAcceptanceDAO.getAll(userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_ACCEPTANCE_READ)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                StockAcceptance stockAcceptance = stockAcceptanceDAO.getById(oid, company);

                return Response.ok(stockAcceptance).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);
        Company company = userToken.getUser().getCompany();
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_ACCEPTANCE_CREATE)) {
                String stockOid = data.get("stockOid");
                Stock stock = stockDAO.getById(stockOid, company);
                String modelOid = data.get("modelOid");
                Model model = modelDAO.getByOid(modelOid, company);
                String quantity = data.get("quantity");
                String documentNumber = data.get("documentNumber");


                stock.setQuantity(stock.getQuantity() + Double.valueOf(quantity));
                stockDAO.update(stock);

                StockAcceptance stockAcceptance = new StockAcceptance();
                if (model == null) {
                    stockAcceptance.setModel(null);
                } else {
                    stockAcceptance.setModel(model);
                    if (documentNumber != "") {
                        SupplyRequirement supplyRequirement = supplyRequirementDAO.getByAllCriteria(model, stock, documentNumber, company);
                        if (supplyRequirement != null) {
                            supplyRequirement.setComingQuantity(supplyRequirement.getComingQuantity() + Double.parseDouble(quantity));
                            supplyRequirement.setTotalQuantity(supplyRequirement.getOrderQuantity() - supplyRequirement.getComingQuantity());
                            supplyRequirement.setUpdateDate(new DateTime());
                            supplyRequirementDAO.update(supplyRequirement);
                        }
                    }

                }
                stockAcceptance.setStock(stock);
                stockAcceptance.setStockDetail(null);
                stockAcceptance.setQuantity(Double.valueOf(quantity));
                stockAcceptance.setUpdater(userToken.getUser());
                stockAcceptance.setCompany(company);
                stockAcceptanceDAO.create(stockAcceptance);

                authUtil.setSystemActivity(userToken.getUser(), company, stock.getName() + "Adlı stok kabul edildi", new DateTime());
                return Response.ok().entity(stockAcceptance).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }

        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }

    }

    @POST
    @Path("createStockDetail")
    @UnitOfWork
    public Response createStockDetail(Map<String, String> data) {
        String token = data.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);
        Company company = userToken.getUser().getCompany();
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_ACCEPTANCE_CREATE)) {
                String stockOid = data.get("stockOid");
                Stock stock = stockDAO.getById(stockOid, company);
                String modelOid = data.get("modelOid");
                Model model = modelDAO.getByOid(modelOid, company);
                String stockDetails = data.get("stockDetails");
                String documentNumber = data.get("documentNumber");


                JSONArray jsonArray = new JSONArray(stockDetails);

                if (jsonArray.length() != 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String oid = jsonObject.getString("oid");
                        String quantity = jsonObject.getString("quantity");
                        if (quantity.equals("0")) {

                        } else {
                            StockDetail stockDetail = stockDetailDAO.getByOid(oid, company);
                            StockAcceptance stockAcceptance = new StockAcceptance();
                            if (model == null) {
                                stockAcceptance.setModel(null);
                            } else {
                                stockAcceptance.setModel(model);
                                if (documentNumber != "") {
                                    Double doubleQuantity = Double.valueOf(quantity);
                                    List<SupplyRequirement> supplyRequirementList = supplyRequirementDAO.getByAllCriteriaDetail(model, stock, stockDetail, documentNumber, company);
                                    for (int k = 0; k < supplyRequirementList.size(); k++) {
                                        SupplyRequirement supplyRequirement = supplyRequirementList.get(k);
                                        if (doubleQuantity <= supplyRequirement.getOrderQuantity() - supplyRequirement.getComingQuantity()) {
                                            supplyRequirement.setComingQuantity(supplyRequirement.getComingQuantity() + doubleQuantity);
                                            supplyRequirement.setTotalQuantity(supplyRequirement.getOrderQuantity() - supplyRequirement.getComingQuantity());
                                            supplyRequirement.setUpdateDate(new DateTime());
                                            supplyRequirementDAO.update(supplyRequirement);
                                            doubleQuantity = 0.0;
                                        } else {
                                            Double newQuantity = doubleQuantity - (supplyRequirement.getOrderQuantity() - supplyRequirement.getComingQuantity());
                                            supplyRequirement.setComingQuantity(supplyRequirement.getComingQuantity() - supplyRequirement.getComingQuantity());
                                            supplyRequirement.setTotalQuantity(supplyRequirement.getOrderQuantity() - supplyRequirement.getComingQuantity());
                                            supplyRequirement.setUpdateDate(new DateTime());
                                            supplyRequirementDAO.update(supplyRequirement);
                                            doubleQuantity = newQuantity;
                                        }
                                        if (supplyRequirementList.size() - 1 == k && doubleQuantity > 0) {
                                            supplyRequirement.setComingQuantity(supplyRequirement.getComingQuantity() + doubleQuantity);
                                            supplyRequirement.setTotalQuantity(supplyRequirement.getOrderQuantity() - supplyRequirement.getComingQuantity());
                                            supplyRequirement.setUpdateDate(new DateTime());
                                            supplyRequirementDAO.update(supplyRequirement);
                                        }

                                    }


                                }

                            }
                            stockAcceptance.setStock(stock);
                            stockAcceptance.setStockDetail(stockDetail);
                            stockAcceptance.setQuantity(Double.valueOf(quantity));
                            stockAcceptance.setUpdater(userToken.getUser());
                            stockAcceptance.setCompany(company);
                            stockAcceptanceDAO.create(stockAcceptance);

                            stock.setQuantity(stock.getQuantity() + Double.valueOf(quantity));
                            stockDAO.update(stock);

                            stockDetail.setQuantity(stockDetail.getQuantity() + Double.valueOf(quantity));
                            stockDetailDAO.update(stockDetail);
                        }
                    }
                }

                authUtil.setSystemActivity(userToken.getUser(), company, stock.getName() + "Adlı stok kabul edildi", new DateTime());
                return Response.ok().entity(stock).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }

        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }

    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_ACCEPTANCE_DELETE)) {
                String oid = data.get("oid");
                Company company = userToken.getUser().getCompany();

                StockAcceptance stockAcceptance = stockAcceptanceDAO.getById(oid, company);
                if (stockAcceptance.getStockDetail() == null) {
                    Stock stock = stockAcceptance.getStock();
                    stock.setQuantity(stock.getQuantity() - stockAcceptance.getQuantity());
                    stockDAO.update(stock);
                } else {
                    StockDetail stockDetail = stockAcceptance.getStockDetail();
                    stockDetail.setQuantity(stockDetail.getQuantity() - stockAcceptance.getQuantity());
                    stockDetailDAO.update(stockDetail);

                    Stock stock = stockAcceptance.getStock();
                    stock.setQuantity(stock.getQuantity() - stockAcceptance.getQuantity());
                    stockDAO.update(stock);
                }
                stockAcceptanceDAO.delete(stockAcceptance);

                authUtil.setSystemActivity(userToken.getUser(), company, stockAcceptance.getStock().getName() + "Adlı stok kabul silindi", new DateTime());


                return Response.ok(stockAcceptance).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSummary")
    @UnitOfWork
    public Response readSummary(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_ACCEPTANCE_READ)) {
                List<StockAcceptance> stockAcceptances = stockAcceptanceDAO.getAll(userToken.getUser().getCompany());

                List<StockAcceptanceDTO> stockAcceptanceDTOs = new LinkedList<>();

                for (StockAcceptance stockAcceptance : stockAcceptances) {
                    Stock stock = stockAcceptance.getStock() == null ? new Stock() : stockAcceptance.getStock();
                    StockDetail stockDetail = stockAcceptance.getStockDetail() == null ? new StockDetail() : stockAcceptance.getStockDetail();
                    Model model = stockAcceptance.getModel() == null ? new Model() : stockAcceptance.getModel();
                    User user = stockAcceptance.getUpdater();

                    StockAcceptanceDTO stockAcceptanceDTO = new StockAcceptanceDTO(stockAcceptance.getQuantity(), stock.getUnit(), model.getModelCode(), model.getCustomerModelCode(), model.getColor(), stock.getCode(), stock.getName(), stockDetail.getName(), stockDetail.getDescription(), user.getEmail(), stockAcceptance.getCreationDateTime());
                    stockAcceptanceDTOs.add(stockAcceptanceDTO);
                }

                return Response.ok().entity(stockAcceptanceDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
