package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.CuttingIncomingSummary;
import com.felislabs.izci.representation.dto.CuttingOrderDTO;
import com.felislabs.izci.representation.dto.CuttingOrderSummary;
import com.felislabs.izci.representation.dto.StockRequirementDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

/**
 * Created by gunerkaanalkim on 01/02/2017.
 */
@Path("cutting")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CuttingResource {
    AuthUtil authUtil;
    ModelDAO modelDAO;
    ModelOrderDAO modelOrderDAO;
    ModelCuttingOrderDAO modelCuttingOrderDAO;
    ModelMaterialDAO modelMaterialDAO;
    ModelAssortDAO modelAssortDAO;
    MrpSettingDAO mrpSettingDAO;
    ModelCuttingIncomingDAO modelCuttingIncomingDAO;
    StockDAO stockDAO;
    StockDetailDAO stockDetailDAO;
    CuttingRequirementDAO cuttingRequirementDAO;
    StaffDAO staffDAO;

    public CuttingResource(AuthUtil authUtil, ModelDAO modelDAO, ModelOrderDAO modelOrderDAO, ModelCuttingOrderDAO modelCuttingOrderDAO, ModelMaterialDAO modelMaterialDAO, ModelAssortDAO modelAssortDAO, MrpSettingDAO mrpSettingDAO, ModelCuttingIncomingDAO modelCuttingIncomingDAO, StockDAO stockDAO, StockDetailDAO stockDetailDAO, CuttingRequirementDAO cuttingRequirementDAO, StaffDAO staffDAO) {
        this.authUtil = authUtil;
        this.modelDAO = modelDAO;
        this.modelOrderDAO = modelOrderDAO;
        this.modelCuttingOrderDAO = modelCuttingOrderDAO;
        this.modelMaterialDAO = modelMaterialDAO;
        this.modelAssortDAO = modelAssortDAO;
        this.mrpSettingDAO = mrpSettingDAO;
        this.modelCuttingIncomingDAO = modelCuttingIncomingDAO;
        this.stockDAO = stockDAO;
        this.stockDetailDAO = stockDetailDAO;
        this.cuttingRequirementDAO = cuttingRequirementDAO;
        this.staffDAO = staffDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String modelOid = data.get("modelOid");
                String quantity = data.get("quantity");
                String description = data.get("description");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                String selectedAssort = data.get("selectedAssort");
                String stockChanges = data.get("stockChanges");
                String staffOid = data.get("staff");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                Company company = userToken.getUser().getCompany();

                Staff staff = staffDAO.getByOid(staffOid, company);

                Model model = modelDAO.getByOid(modelOid, company);

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);
                DateTime now = new DateTime();

                String documentNumber = model.getModelCode() + "/" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + mrpSetting.getCuttingOrderCounter();

                mrpSetting.setCuttingOrderCounter(mrpSetting.getCuttingOrderCounter() + 1);
                mrpSettingDAO.update(mrpSetting);

                ModelCuttingOrder modelCuttingOrder = new ModelCuttingOrder();
                modelCuttingOrder.setCompany(company);
                modelCuttingOrder.setUpdater(userToken.getUser());
                modelCuttingOrder.setUpdateDate(new DateTime());
                modelCuttingOrder.setModelOrder(modelOrder);
                modelCuttingOrder.setQuantity(Integer.valueOf(quantity));
                modelCuttingOrder.setDocumentNumber(documentNumber);
                modelCuttingOrder.setStartDate(start);
                modelCuttingOrder.setEndDate(end);
                modelCuttingOrder.setDescription(description);
                modelCuttingOrder.setSelectedAssortUid(selectedAssort);
                modelCuttingOrder.setStaff(staff);

                modelCuttingOrderDAO.create(modelCuttingOrder);

//                  Stock Changing
                JSONArray jsonArray = new JSONArray(stockChanges);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String stockCode = jsonObject.getString("stockCode");
                    Double requirement = Double.valueOf(jsonObject.getString("requirement"));

                    if (jsonObject.getString("stockDetail").equals("null")) {
                        Stock stock = stockDAO.getByCode(stockCode, company);

                        CuttingRequirement cuttingRequirement = new CuttingRequirement();
                        cuttingRequirement.setModelCuttingOrder(modelCuttingOrder);
                        cuttingRequirement.setStock(stock);
                        cuttingRequirement.setStockDetail(null);
                        cuttingRequirement.setQuantity(requirement);
                        cuttingRequirement.setUpdater(userToken.getUser());
                        cuttingRequirement.setCompany(company);

                        cuttingRequirementDAO.create(cuttingRequirement);
                    } else {
                        StockDetail stockDetail = stockDetailDAO.getByOid(jsonObject.getString("stockDetail"), company);

                        Stock stock = stockDAO.getByCode(stockCode, company);


                        CuttingRequirement cuttingRequirement = new CuttingRequirement();
                        cuttingRequirement.setModelCuttingOrder(modelCuttingOrder);
                        cuttingRequirement.setStock(stock);
                        cuttingRequirement.setStockDetail(stockDetail);
                        cuttingRequirement.setQuantity(requirement);
                        cuttingRequirement.setUpdater(userToken.getUser());
                        cuttingRequirement.setCompany(company);

                        cuttingRequirementDAO.create(cuttingRequirement);
                    }

                }

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modele kesim emri girildi. ", new DateTime());

                return Response.ok().entity(modelCuttingOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String cuttingOrderOid = data.get("cuttingOrderOid");
                String modelOid = data.get("modelOid");
                String quantity = data.get("quantity");

                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(modelOid, company);

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                ModelCuttingOrder modelCuttingOrder = modelCuttingOrderDAO.getByOid(cuttingOrderOid, company);
                modelCuttingOrder.setCompany(company);
                modelCuttingOrder.setUpdater(userToken.getUser());
                modelCuttingOrder.setUpdateDate(new DateTime());
                modelCuttingOrder.setModelOrder(modelOrder);
                modelCuttingOrder.setQuantity(Integer.valueOf(quantity));

                modelCuttingOrderDAO.create(modelCuttingOrder);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelin kesim emri güncellendi. ", new DateTime());

                return Response.ok().entity(modelCuttingOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                return Response.ok().entity(modelCuttingOrderDAO.getAll(userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String cuttingOrderOid = data.get("cuttingOrderOid");

                return Response.ok().entity(modelCuttingOrderDAO.getByOid(cuttingOrderOid, userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetailAndMaterial")
    @UnitOfWork
    public Response readDetailAndMaterial(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String cuttingOrderOid = data.get("cuttingOrderOid");

                ModelCuttingOrder modelCuttingOrder = modelCuttingOrderDAO.getByOid(cuttingOrderOid, userToken.getUser().getCompany());

                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(modelCuttingOrder.getModelOrder().getModel(), userToken.getUser().getCompany());
                List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelCuttingOrder.getModelOrder(), userToken.getUser().getCompany());

                CuttingOrderDTO cuttingOrderDTO = new CuttingOrderDTO();
                cuttingOrderDTO.setModelMaterials(modelMaterials);
                cuttingOrderDTO.setModelCuttingOrder(modelCuttingOrder);
                cuttingOrderDTO.setModelAssorts(modelAssorts);

                return Response.ok().entity(cuttingOrderDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByModel")
    @UnitOfWork
    public Response readByModel(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                List<ModelCuttingOrder> modelCuttingOrders = modelCuttingOrderDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());

                return Response.ok().entity(modelCuttingOrders).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_DELETE)) {
                String cuttingOrderOid = data.get("cuttingOrderOid");

                ModelCuttingOrder modelCuttingOrder = modelCuttingOrderDAO.getByOid(cuttingOrderOid, userToken.getUser().getCompany());
                List<ModelCuttingIncoming> modelCuttingIncomings = modelCuttingIncomingDAO.getByModelCuttingOrder(modelCuttingOrder, userToken.getUser().getCompany());
                if (modelCuttingIncomings.size() == 0) {
                    modelCuttingOrder.setDeleteStatus(true);
                    modelCuttingOrderDAO.update(modelCuttingOrder);
                    List<CuttingRequirement> cuttingRequirements = cuttingRequirementDAO.getByModelCuttingOrder(modelCuttingOrder, userToken.getUser().getCompany());
                    for (int i = 0; i < cuttingRequirements.size(); i++) {
                        CuttingRequirement cuttingRequirement = cuttingRequirements.get(i);
                        cuttingRequirement.setDeleteStatus(true);
                        cuttingRequirementDAO.update(cuttingRequirement);
                    }
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_ALREADY_ACCEPTED.getErrorAlias()).build();
                }


                List<ModelCuttingOrder> modelCuttingOrders = modelCuttingOrderDAO.getByModelOrder(modelCuttingOrder.getModelOrder(), userToken.getUser().getCompany());

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelCuttingOrder.getModelOrder().getModel().getModelCode() + " kodlu modelin kesim emri silindi. ", new DateTime());

                return Response.ok().entity(modelCuttingOrders).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("createIncoming")
    @UnitOfWork
    public Response createIncoming(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String documentNumber = data.get("documentNumber");
                String quantity = data.get("quantity");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                ModelCuttingOrder modelCuttingOrder = modelCuttingOrderDAO.getByDocumentNumber(documentNumber, company);

                ModelCuttingIncoming modelCuttingIncoming = new ModelCuttingIncoming();
                modelCuttingIncoming.setCompany(company);
                modelCuttingIncoming.setUpdater(user);
                modelCuttingIncoming.setUpdateDate(new DateTime());
                modelCuttingIncoming.setModelOrder(modelCuttingOrder.getModelOrder());
                modelCuttingIncoming.setModelCuttingOrder(modelCuttingOrder);
                modelCuttingIncoming.setQuantity(Integer.valueOf(quantity));
                modelCuttingIncoming.setDescription(description);

                modelCuttingIncomingDAO.create(modelCuttingIncoming);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelCuttingOrder.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelCuttingIncoming.getModelCuttingOrder().getDocumentNumber() + " doküman numaralı kesimi kabul edildi", new DateTime());

                return Response.ok().entity(modelCuttingIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteIncoming")
    @UnitOfWork
    public Response deleteIncoming(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_DELETE)) {
                String oid = data.get("oid");
                Company company = userToken.getUser().getCompany();

                ModelCuttingIncoming modelCuttingIncoming = modelCuttingIncomingDAO.getByOid(oid, company);
                modelCuttingIncoming.setDeleteStatus(true);
                modelCuttingIncomingDAO.update(modelCuttingIncoming);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelCuttingIncoming.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelCuttingIncoming.getModelCuttingOrder().getDocumentNumber() + " doküman numaralı kesimi silindi", new DateTime());

                return Response.ok().entity(modelCuttingIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncoming")
    @UnitOfWork
    public Response readIncoming(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String modelOid = data.get("modelOid");
                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(modelOid, company);

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                List<ModelCuttingIncoming> modelCuttingIncomings = modelCuttingIncomingDAO.getByModelOrder(modelOrder, company);

                return Response.ok().entity(modelCuttingIncomings).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingCutOrderDetail")
    @UnitOfWork
    public Response readIncomingCutOrderDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String incomingCutOrderOid = data.get("oid");

                ModelCuttingIncoming modelCuttingIncoming = modelCuttingIncomingDAO.getByOid(incomingCutOrderOid, userToken.getUser().getCompany());

                return Response.ok().entity(modelCuttingIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSummary")
    @UnitOfWork
    public Response readSummary(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                List<ModelCuttingOrder> modelCuttingOrders = modelCuttingOrderDAO.getAll(userToken.getUser().getCompany());
                List<CuttingOrderSummary> cuttingOrderSummaries = new LinkedList<>();

                for (ModelCuttingOrder modelCuttingOrder : modelCuttingOrders) {
                    List<ModelCuttingIncoming> modelCuttingIncoming = modelCuttingIncomingDAO.getByModelCuttingOrder(modelCuttingOrder, userToken.getUser().getCompany());

                    /*
                    * 1. Tüm gelen kesim kabul miktarları toplanıyor
                    * 2. Modelin kesime gönderilen miktarı, kesim kabullerinin toplamından büyük veya eşit ise terminale gönderilmiyor
                    * */
                    Integer cuttingIncomingTotal = 0;

                    for (ModelCuttingIncoming cuttingIncoming : modelCuttingIncoming) {
                        if (cuttingIncoming.getQuantity() != null) {
                            cuttingIncomingTotal += cuttingIncoming.getQuantity();
                        }
                    }

                    if (modelCuttingOrder.getQuantity() > cuttingIncomingTotal) {
                        CuttingOrderSummary cuttingOrderSummary = new CuttingOrderSummary();
                        cuttingOrderSummary.setOid(modelCuttingOrder.getOid());
                        cuttingOrderSummary.setModelCode(modelCuttingOrder.getModelOrder().getModel().getModelCode());
                        cuttingOrderSummary.setCustomerModelCode(modelCuttingOrder.getModelOrder().getModel().getCustomerModelCode());
                        cuttingOrderSummary.setCreationDatetime(modelCuttingOrder.getCreationDateTime());
                        cuttingOrderSummary.setDefaultPhoto(modelCuttingOrder.getModelOrder().getModel().getDefaultPhoto());
                        cuttingOrderSummary.setModelColor(modelCuttingOrder.getModelOrder().getModel().getColor());
                        cuttingOrderSummary.setCuttingDocumentNumber(modelCuttingOrder.getDocumentNumber());
                        cuttingOrderSummary.setCuttingOrderQuantity(modelCuttingOrder.getQuantity());
                        cuttingOrderSummary.setOrderQuantity(modelCuttingOrder.getModelOrder().getOrderQuantity());
                        cuttingOrderSummary.setStaffName(modelCuttingOrder.getStaff() == null ? "" : modelCuttingOrder.getStaff().getName());
                        cuttingOrderSummary.setStartDate(modelCuttingOrder.getStartDate());
                        cuttingOrderSummary.setEndDate(modelCuttingOrder.getEndDate());
                        cuttingOrderSummary.setCustomerName(modelCuttingOrder.getModelOrder().getModel().getCustomer().getName());
                        cuttingOrderSummary.setModelOid(modelCuttingOrder.getModelOrder().getModel().getOid());
                        cuttingOrderSummary.setSelectedAssortUid(modelCuttingOrder.getSelectedAssortUid());
                        cuttingOrderSummary.setDescription(modelCuttingOrder.getDescription());
                        cuttingOrderSummary.setIncomingQuantity(cuttingIncomingTotal);
                        cuttingOrderSummary.setSeason(modelCuttingOrder.getModelOrder().getModel().getSeason());
                        cuttingOrderSummary.setYear(modelCuttingOrder.getModelOrder().getModel().getYear());

                        cuttingOrderSummaries.add(cuttingOrderSummary);
                    }

                }

                return Response.ok().entity(cuttingOrderSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingSummary")
    @UnitOfWork
    public Response readIncomingSummary(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                List<ModelCuttingIncoming> modelCuttingIncomings = modelCuttingIncomingDAO.getAll(userToken.getUser().getCompany());
                List<CuttingIncomingSummary> cuttingIncomingSummaries = new LinkedList<>();

                for (ModelCuttingIncoming modelCuttingIncoming : modelCuttingIncomings) {
                    CuttingIncomingSummary cuttingIncomingSummary = new CuttingIncomingSummary();
                    cuttingIncomingSummary.setModelCode(modelCuttingIncoming.getModelOrder().getModel().getModelCode());
                    cuttingIncomingSummary.setCustomerModelCode(modelCuttingIncoming.getModelOrder().getModel().getCustomerModelCode());
                    cuttingIncomingSummary.setCreationDatetime(modelCuttingIncoming.getCreationDateTime());
                    cuttingIncomingSummary.setDefaultPhoto(modelCuttingIncoming.getModelOrder().getModel().getDefaultPhoto());
                    cuttingIncomingSummary.setModelColor(modelCuttingIncoming.getModelOrder().getModel().getColor());
                    cuttingIncomingSummary.setCuttingDocumentNumber(modelCuttingIncoming.getModelCuttingOrder().getDocumentNumber());
                    cuttingIncomingSummary.setCuttingIncomingQuantity(modelCuttingIncoming.getQuantity());
                    cuttingIncomingSummary.setOrderQuantity(modelCuttingIncoming.getModelOrder().getOrderQuantity());
                    cuttingIncomingSummary.setSeason(modelCuttingIncoming.getModelCuttingOrder().getModelOrder().getModel().getSeason());
                    cuttingIncomingSummary.setYear(modelCuttingIncoming.getModelCuttingOrder().getModelOrder().getModel().getYear());
                    cuttingIncomingSummaries.add(cuttingIncomingSummary);
                }

                return Response.ok().entity(cuttingIncomingSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSummaryForStaff")
    @UnitOfWork
    public Response readSummaryForStaff(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_ORDER_UPDATE)) {
                String staffOid = data.get("staff");

                Staff staff = staffDAO.getByOid(staffOid, userToken.getUser().getCompany());

                List<ModelCuttingOrder> modelCuttingOrders = modelCuttingOrderDAO.getByStaff(staff, userToken.getUser().getCompany());
                List<CuttingOrderSummary> cuttingOrderSummaries = new LinkedList<>();

                for (ModelCuttingOrder modelCuttingOrder : modelCuttingOrders) {
                    CuttingOrderSummary cuttingOrderSummary = new CuttingOrderSummary();
                    cuttingOrderSummary.setOid(modelCuttingOrder.getOid());
                    cuttingOrderSummary.setModelCode(modelCuttingOrder.getModelOrder().getModel().getModelCode());
                    cuttingOrderSummary.setCustomerModelCode(modelCuttingOrder.getModelOrder().getModel().getCustomerModelCode());
                    cuttingOrderSummary.setCreationDatetime(modelCuttingOrder.getCreationDateTime());
                    cuttingOrderSummary.setDefaultPhoto(modelCuttingOrder.getModelOrder().getModel().getDefaultPhoto());
                    cuttingOrderSummary.setModelColor(modelCuttingOrder.getModelOrder().getModel().getColor());
                    cuttingOrderSummary.setCuttingDocumentNumber(modelCuttingOrder.getDocumentNumber());
                    cuttingOrderSummary.setCuttingOrderQuantity(modelCuttingOrder.getQuantity());
                    cuttingOrderSummary.setOrderQuantity(modelCuttingOrder.getModelOrder().getOrderQuantity());
                    cuttingOrderSummary.setStaffName(modelCuttingOrder.getStaff().getName());

                    cuttingOrderSummaries.add(cuttingOrderSummary);
                }

                return Response.ok().entity(cuttingOrderSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSummaryForAllIncomings")
    @UnitOfWork
    public Response readSummaryForAllIncomings(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                List<ModelCuttingIncoming> modelCuttingIncomings = modelCuttingIncomingDAO.getAll(userToken.getUser().getCompany());
                List<CuttingIncomingSummary> cuttingIncomingSummaries = new LinkedList<>();

                for (ModelCuttingIncoming modelCuttingIncoming : modelCuttingIncomings) {
                    Model model = modelCuttingIncoming.getModelOrder().getModel();

                    CuttingIncomingSummary cuttingIncomingSummary = new CuttingIncomingSummary(
                            modelCuttingIncoming.getOid(),
                            model.getModelCode(),
                            model.getColor(),
                            model.getCustomerModelCode(),
                            model.getDefaultPhoto(),
                            modelCuttingIncoming.getModelCuttingOrder().getDocumentNumber(),
                            model.getCustomer().getName(),
                            modelCuttingIncoming.getModelCuttingOrder().getStaff() != null ? modelCuttingIncoming.getModelCuttingOrder().getStaff().getName() : "",
                            modelCuttingIncoming.getCreationDateTime(),
                            modelCuttingIncoming.getQuantity(),
                            modelCuttingIncoming.getModelCuttingOrder().getQuantity()
                    );

                    cuttingIncomingSummaries.add(cuttingIncomingSummary);
                }

                return Response.ok().entity(cuttingIncomingSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
