package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.dto.CurrencyDTO;
import com.felislabs.izci.representation.dto.ModelStatisticsDTO;
import com.felislabs.izci.representation.entities.SystemActivity;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by gunerkaanalkim on 27/09/15.
 */
@Path("statistic")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StatisticResource {
    AuthUtil authUtil;
    UserDAO userDAO;
    UserTokenDAO userTokenDAO;
    SystemActivityDAO systemActivityDAO;
    ModelDAO modelDAO;
    ModelOrderDAO modelOrderDAO;

    public StatisticResource(AuthUtil authUtil, UserDAO userDAO, UserTokenDAO userTokenDAO, SystemActivityDAO systemActivityDAO, ModelDAO modelDAO, ModelOrderDAO modelOrderDAO) {
        this.authUtil = authUtil;
        this.userDAO = userDAO;
        this.userTokenDAO = userTokenDAO;
        this.systemActivityDAO = systemActivityDAO;
        this.modelDAO = modelDAO;
        this.modelOrderDAO = modelOrderDAO;
    }

    @GET
    @Path("currency/{crmt}")
    @UnitOfWork
    public Response getCurrency(@PathParam("crmt") String crmt) throws IOException, JDOMException {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            File file = new File(System.getProperty("java.io.tmpdir") + "currency.xml");
            if (file != null) {
                SAXBuilder builder = new SAXBuilder();

                Document document = (Document) builder.build(file);
                Element rootNode = document.getRootElement();
                List list = rootNode.getChildren("Currency");

                List<CurrencyDTO> currencyDTOs = new LinkedList<>();

                for (int i = 0; i < list.size(); i++) {
                    Element node = (Element) list.get(i);
                    CurrencyDTO currencyDTO = new CurrencyDTO();
                    currencyDTO.setName(node.getChildText("Isim"));
                    currencyDTO.setCode(node.getChildText("CurrencyName"));
                    currencyDTO.setBuying(node.getChildText("ForexBuying"));
                    currencyDTO.setSelling(node.getChildText("ForexSelling"));

                    currencyDTOs.add(currencyDTO);
                }
                return Response.ok(currencyDTOs).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
        return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
    }

    @POST
    @Path("lastLogin/{crmt}")
    @UnitOfWork
    public Response lastLogin(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            List<UserToken> userTokens = userTokenDAO.findByCreationDate();

            return Response.ok(userTokens).build();
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("systemActivity/{crmt}")
    @UnitOfWork
    public Response systemActivity(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {

            List<SystemActivity> systemActivities = systemActivityDAO.getByDateRange(new DateTime().minusDays(7), new DateTime(), userToken.getUser().getCompany());

            return Response.ok(systemActivities).build();
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("model/{crmt}")
    @UnitOfWork
    public Response totalModel(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);
        if (userToken != null) {
            Integer totalModel = modelDAO.getAllCount(userToken.getUser().getCompany());
            Integer confirmedModel = modelDAO.getConfirmedCount(userToken.getUser().getCompany());
            Integer notConfirmedModel = modelDAO.getNotConfirmedCount(userToken.getUser().getCompany());
            Integer totalOrderCount = modelOrderDAO.getOrderNumber(userToken.getUser().getCompany());

            DateTime now = new DateTime();

//            Integer orderPairSum = modelOrderDAO.getOrderPairSum(now.minusMonths(6), now, userToken.getUser().getCompany());

            ModelStatisticsDTO modelStatisticDTO = new ModelStatisticsDTO();
            modelStatisticDTO.setTotalModel(totalModel);
            modelStatisticDTO.setConfirmedModel(confirmedModel);
            modelStatisticDTO.setNotConfirmedModel(notConfirmedModel);
//            modelStatisticDTO.setTotalOrder(totalOrderCount);
//            modelStatisticDTO.setOrderPairSum(orderPairSum);

            return Response.ok().entity(modelStatisticDTO).build();
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
