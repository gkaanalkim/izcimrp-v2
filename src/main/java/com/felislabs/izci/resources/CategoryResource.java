package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.BrandDAO;
import com.felislabs.izci.dao.dao.CategoryDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 08/03/2017.
 */
@Path("category")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CategoryResource {
    AuthUtil authUtil;
    CategoryDAO categoryDAO;

    public CategoryResource(AuthUtil authUtil, CategoryDAO categoryDAO) {
        this.authUtil = authUtil;
        this.categoryDAO = categoryDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Category category = categoryDAO.getByCode(code, userToken.getUser().getCompany());

                if (category != null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Category categoryByCode = categoryDAO.getByCode(code, userToken.getUser().getCompany());

                    if (categoryByCode != null && !categoryByCode.getCode().equals(category.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        category = new Category();
                        category.setCompany(company);
                        category.setUser(user);
                        category.setUpdateDate(new DateTime());
                        category.setName(name);
                        category.setCode(code);
                        category.setDescription(description);

                        categoryDAO.create(category);

                        return Response.ok().entity(category).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_CREATE)) {
                List<Category> categories = categoryDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(categories).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_CREATE)) {
                String oid = data.get("oid");

                Category category = categoryDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(category).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_UPDATE)) {
                String oid = data.get("oid");

                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Category category = categoryDAO.getByOid(oid, company);

                if (category == null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Category categoryByCode = categoryDAO.getByCode(code, userToken.getUser().getCompany());

                    if (categoryByCode != null && !categoryByCode.getCode().equals(category.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        category.setCompany(company);
                        category.setUser(user);
                        category.setUpdateDate(new DateTime());
                        category.setName(name);
                        category.setCode(code);
                        category.setDescription(description);

                        categoryDAO.update(category);

                        return Response.ok().entity(category).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Category category = categoryDAO.getByOid(oid, company);
                category.setDeleteStatus(true);

                categoryDAO.update(category);

                return Response.ok().entity(category).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
