package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.FairOrderDTO;
import com.felislabs.izci.representation.dto.FairOrderPrintDTO;
import com.felislabs.izci.representation.dto.SoleFairOrderDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 23/03/2017.
 */
@Path("fairOrder")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FairOrderResource {
    AuthUtil authUtil;
    FairOrderDAO fairOrderDAO;
    FairModelAssortDAO fairModelAssortDAO;
    FairModelPriceDAO fairModelPriceDAO;
    ModelDAO modelDAO;
    CustomerDAO customerDAO;
    FairOrderAssortDAO fairOrderAssortDAO;
    ModelMaterialDAO modelMaterialDAO;
    CustomerSoleRecipeDAO customerSoleRecipeDAO;
    MrpSettingDAO mrpSettingDAO;

    public FairOrderResource(AuthUtil authUtil, FairOrderDAO fairOrderDAO, FairModelAssortDAO fairModelAssortDAO, FairModelPriceDAO fairModelPriceDAO, ModelDAO modelDAO, CustomerDAO customerDAO, FairOrderAssortDAO fairOrderAssortDAO, ModelMaterialDAO modelMaterialDAO, CustomerSoleRecipeDAO customerSoleRecipeDAO, MrpSettingDAO mrpSettingDAO) {
        this.authUtil = authUtil;
        this.fairOrderDAO = fairOrderDAO;
        this.fairModelAssortDAO = fairModelAssortDAO;
        this.fairModelPriceDAO = fairModelPriceDAO;
        this.modelDAO = modelDAO;
        this.customerDAO = customerDAO;
        this.fairOrderAssortDAO = fairOrderAssortDAO;
        this.modelMaterialDAO = modelMaterialDAO;
        this.customerSoleRecipeDAO = customerSoleRecipeDAO;
        this.mrpSettingDAO = mrpSettingDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_CREATE)) {
                String modelOid = data.get("modelOid");
                String priceOid = data.get("priceOid");
                String description = data.get("description");
                String amount = data.get("amount");
                String customerOid = data.get("customerOid");
                String assorts = data.get("assorts");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                Model model = modelDAO.getByOid(modelOid, company);
                FairModelPrice fairModelPrice = fairModelPriceDAO.getByOid(priceOid, company);
                Customer customer = customerDAO.getById(customerOid, company);

                FairOrder fairOrder = new FairOrder();
                fairOrder.setCompany(company);
                fairOrder.setUpdater(user);
                fairOrder.setUpdateDate(new DateTime());
                fairOrder.setModel(model);
                fairOrder.setCustomer(customer);
                fairOrder.setCurrency("TL");

                if (fairModelPrice != null) {
                    amount = String.valueOf(fairModelPrice.getAmount());
                }

                fairOrder.setAmount(Double.valueOf(amount));
                fairOrder.setDescription(description);

                fairOrderDAO.create(fairOrder);

                JSONArray jsonArray = new JSONArray(assorts);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String boxNumber = jsonObject.getString("boxNumber");
                    String number = jsonObject.getString("number");
                    String assort = jsonObject.getString("assort");
                    String uid = jsonObject.getString("uid");

                    FairOrderAssort fairOrderAssort = new FairOrderAssort();
                    fairOrderAssort.setFairOrder(fairOrder);
                    fairOrderAssort.setUpdateDate(new DateTime());
                    fairOrderAssort.setUpdater(user);
                    fairOrderAssort.setAssort(Integer.valueOf(assort));
                    fairOrderAssort.setBoxNumber(Integer.valueOf(boxNumber));
                    fairOrderAssort.setNumber(Integer.valueOf(number));
                    fairOrderAssort.setUid(uid);

                    fairOrderAssortDAO.create(fairOrderAssort);
                }

                return Response.ok().entity(fairOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("createForSole")
    @UnitOfWork
    public Response createForFootbed(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_CREATE)) {
                String customerOid = data.get("customerOid");
                String modelOid = data.get("modelOid");
                String recipe = data.get("recipe");
                String description = data.get("description");
                String quantity = data.get("quantity");
                String series = data.get("series");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                Customer customer = customerDAO.getById(customerOid, company);
                Model model = modelDAO.getByOid(modelOid, company);

                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);

                DateTime now = new DateTime();

                String orderNumber = "FO" + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + mrpSetting.getFairOrderNumber();

                mrpSetting.setFairOrderNumber(mrpSetting.getFairOrderNumber() + 1);
                mrpSettingDAO.update(mrpSetting);

                FairOrder fairOrder = new FairOrder();
                fairOrder.setCompany(company);
                fairOrder.setUpdater(user);
                fairOrder.setUpdateDate(new DateTime());
                fairOrder.setModel(model);
                fairOrder.setCustomer(customer);
                fairOrder.setCurrency("TL");
                fairOrder.setAmount(0.0);
                fairOrder.setDescription(description);
                fairOrder.setOrderNumber(orderNumber);
                fairOrder.setQuantity(Integer.valueOf(quantity));
                fairOrder.setSeries(series);

                fairOrderDAO.create(fairOrder);

                JSONArray jsonArray = new JSONArray(recipe);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String materialOid = jsonObject.getString("materialOid");
                    String customerValue = jsonObject.getString("customerValue");

                    ModelMaterial modelMaterial = modelMaterialDAO.getByOid(materialOid, company);

                    CustomerSoleRecipe customerSoleRecipe = new CustomerSoleRecipe();
                    customerSoleRecipe.setCustomerValue(customerValue);
                    customerSoleRecipe.setModelMaterial(modelMaterial);
                    customerSoleRecipe.setFairOrder(fairOrder);
                    customerSoleRecipeDAO.create(customerSoleRecipe);
                }

                return Response.ok().entity(Response.Status.OK).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("updateForSole")
    @UnitOfWork
    public Response updateForSole(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_UPDATE)) {
                String fairOrderOid = data.get("fairOrderOid");
                String customerOid = data.get("customerOid");
                String modelOid = data.get("modelOid");
                String recipe = data.get("recipe");
                String description = data.get("description");
                String quantity = data.get("quantity");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                Customer customer = customerDAO.getById(customerOid, company);
                Model model = modelDAO.getByOid(modelOid, company);

                FairOrder fairOrder = fairOrderDAO.getByOid(fairOrderOid, company);
                fairOrder.setCompany(company);
                fairOrder.setUpdater(user);
                fairOrder.setUpdateDate(new DateTime());
                fairOrder.setModel(model);
                fairOrder.setCustomer(customer);
                fairOrder.setCurrency("TL");
                fairOrder.setAmount(0.0);
                fairOrder.setDescription(description);
                fairOrder.setQuantity(Integer.valueOf(quantity));

                fairOrderDAO.update(fairOrder);

                List<CustomerSoleRecipe> customerSoleRecipes = customerSoleRecipeDAO.getByFairOrder(fairOrder);
                for (CustomerSoleRecipe customerSoleRecipe : customerSoleRecipes) {
                    customerSoleRecipe.setDeleteStatus(true);
                    customerSoleRecipeDAO.update(customerSoleRecipe);
                }

                JSONArray jsonArray = new JSONArray(recipe);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String materialOid = jsonObject.getString("materialOid");
                    String customerValue = jsonObject.getString("customerValue");

                    ModelMaterial modelMaterial = modelMaterialDAO.getByOid(materialOid, company);

                    CustomerSoleRecipe customerSoleRecipe = new CustomerSoleRecipe();
                    customerSoleRecipe.setCustomerValue(customerValue);
                    customerSoleRecipe.setModelMaterial(modelMaterial);
                    customerSoleRecipe.setFairOrder(fairOrder);
                    customerSoleRecipeDAO.create(customerSoleRecipe);
                }

                return Response.ok().entity(Response.Status.OK).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("createForMobile")
    @UnitOfWork
    public Response createForMobile(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_CREATE)) {
                String orders = data.get("orders");
                String customerOid = data.get("customerOid");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                Customer customer = customerDAO.getById(customerOid, company);

                JSONArray jsonArray = new JSONArray(orders);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String modelOid = jsonObject.getString("modelOid");
                    String quantity = jsonObject.getString("quantity");

                    Model model = modelDAO.getByOid(modelOid, company);
                    FairModelPrice fairModelPrices = null;

                    if (userToken.getUser().getCompany().getCode().equals("GMS")) {
                        fairModelPrices = fairModelPriceDAO.getByPriceName("Liste Fiyatı", model, company).get(0);
                    } else {
                        fairModelPrices = fairModelPriceDAO.getByPriceName("Fiyat", model, company).get(0);
                    }

                    FairOrder fairOrder = new FairOrder();
                    fairOrder.setCompany(company);
                    fairOrder.setUpdater(user);
                    fairOrder.setUpdateDate(new DateTime());
                    fairOrder.setAmount(fairModelPrices.getAmount());
                    fairOrder.setCustomer(customer);
                    fairOrder.setModel(model);
                    fairOrder.setCurrency("TL");

                    fairOrderDAO.create(fairOrder);

                    List<FairModelAssort> fairModelAssorts = fairModelAssortDAO.getByModel(model, company);

                    for (FairModelAssort fairModelAssort : fairModelAssorts) {
                        FairOrderAssort fairOrderAssort = new FairOrderAssort();
                        fairOrderAssort.setFairOrder(fairOrder);
                        fairOrderAssort.setUpdateDate(new DateTime());
                        fairOrderAssort.setUpdater(user);
                        fairOrderAssort.setAssort(fairModelAssort.getAssort());
                        fairOrderAssort.setBoxNumber(Integer.valueOf(quantity));
                        fairOrderAssort.setNumber(fairModelAssort.getNumber());
                        fairOrderAssort.setUid(fairModelAssort.getUid());

                        fairOrderAssortDAO.create(fairOrderAssort);
                    }
                }

                return Response.ok().entity(orders).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getOtherColor")
    @UnitOfWork
    public Response getOtherColor(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_CREATE)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();

                Model oldModel = modelDAO.getByOid(modelOid, company);

                List<Model> models = modelDAO.getByModelCode(oldModel.getModelCode(), company);

                List<Model> newModelList = new LinkedList<>();

                for (Model model : models) {
                    if (model.getOid() != oldModel.getOid()) {
                        newModelList.add(model);
                    }
                }

                return Response.ok().entity(newModelList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByCustomer")
    @UnitOfWork
    public Response readByCustomer(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_READ)) {
                String customerOid = data.get("customerOid");
                Customer customer = customerDAO.getById(customerOid, userToken.getUser().getCompany());

                List<FairOrderDTO> fairOrderDTOs = new LinkedList<>();
                List<FairOrder> fairOrders = fairOrderDAO.getByCustomer(customer, userToken.getUser().getCompany());

                for (FairOrder fairOrder : fairOrders) {
                    List<FairOrderAssort> fairOrderAssorts = fairOrderAssortDAO.getByFairOrder(fairOrder);

                    Integer totalQuantity = 0;

                    for (FairOrderAssort fairOrderAssort : fairOrderAssorts) {
                        totalQuantity += fairOrderAssort.getAssort() * fairOrderAssort.getBoxNumber();
                    }

                    Double totalAmount = totalQuantity * fairOrder.getAmount();

                    FairOrderDTO fairOrderDTO = new FairOrderDTO();
                    fairOrderDTO.setFairOrder(fairOrder);
                    fairOrderDTO.setTotalAmount(totalAmount);
                    fairOrderDTO.setQuantity(totalQuantity);
                    fairOrderDTO.setCurrency(fairOrder.getCurrency());

                    fairOrderDTOs.add(fairOrderDTO);
                }

                return Response.ok().entity(fairOrderDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetailForSole")
    @UnitOfWork
    public Response readByCustomerByFootbed(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_READ)) {
                String fairOrderOid = data.get("oid");

                FairOrder fairOrder = fairOrderDAO.getByOid(fairOrderOid, userToken.getUser().getCompany());

                List<CustomerSoleRecipe> customerSoleRecipes = customerSoleRecipeDAO.getByFairOrder(fairOrder);

                SoleFairOrderDTO soleFairOrderDTO = new SoleFairOrderDTO(fairOrder, customerSoleRecipes);

                return Response.ok().entity(soleFairOrderDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readFairOrderAssort")
    @UnitOfWork
    public Response readFairOrderAssort(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_READ)) {
                String fairOrderOid = data.get("fairOrderOid");

                FairOrder fairOrder = fairOrderDAO.getByOid(fairOrderOid, userToken.getUser().getCompany());

                List<FairOrderAssort> fairOrderAssorts = fairOrderAssortDAO.getByFairOrder(fairOrder);

                return Response.ok().entity(fairOrderAssorts).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_UPDATE)) {
                String orderOid = data.get("orderOid");
                String modelOid = data.get("modelOid");
                String priceOid = data.get("priceOid");
                String description = data.get("description");
                String amount = data.get("amount");
                String customerOid = data.get("customerOid");
                String assorts = data.get("assorts");
                String currency = data.get("currency");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                Model model = modelDAO.getByOid(modelOid, company);
                FairModelPrice fairModelPrice = fairModelPriceDAO.getByOid(priceOid, company);
                Customer customer = customerDAO.getById(customerOid, company);

                FairOrder fairOrder = fairOrderDAO.getByOid(orderOid, company);
                fairOrder.setCompany(company);
                fairOrder.setUpdater(user);
                fairOrder.setUpdateDate(new DateTime());
                fairOrder.setModel(model);
//                fairOrder.setFairModelPrice(fairModelPrice);
                fairOrder.setCustomer(customer);
                fairOrder.setCurrency(currency);

                if (fairModelPrice != null) {
                    amount = String.valueOf(fairModelPrice.getAmount());
                }

                fairOrder.setAmount(Double.valueOf(amount));
                fairOrder.setDescription(description);

                fairOrderDAO.update(fairOrder);

                List<FairOrderAssort> fairOrderAssorts = fairOrderAssortDAO.getByFairOrder(fairOrder);
                if (fairOrderAssorts.size() != 0) {
                    for (FairOrderAssort fairOrderAssort : fairOrderAssorts) {
                        fairOrderAssortDAO.delete(fairOrderAssort);
                    }
                }

                JSONArray jsonArray = new JSONArray(assorts);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String boxNumber = jsonObject.getString("boxNumber");
                    String number = jsonObject.getString("number");
                    String assort = jsonObject.getString("assort");
                    String uid = jsonObject.getString("uid");

                    FairOrderAssort fairOrderAssort = new FairOrderAssort();
                    fairOrderAssort.setFairOrder(fairOrder);
                    fairOrderAssort.setUpdateDate(new DateTime());
                    fairOrderAssort.setUpdater(user);
                    fairOrderAssort.setAssort(Integer.valueOf(assort));
                    fairOrderAssort.setBoxNumber(Integer.valueOf(boxNumber));
                    fairOrderAssort.setNumber(Integer.valueOf(number));
                    fairOrderAssort.setUid(uid);

                    fairOrderAssortDAO.create(fairOrderAssort);
                }

                return Response.ok().entity(fairOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_DELETE)) {
                String orderOid = data.get("orderOid");

                Company company = userToken.getUser().getCompany();

                FairOrder fairOrder = fairOrderDAO.getByOid(orderOid, company);

                List<FairOrderAssort> fairOrderAssorts = fairOrderAssortDAO.getByFairOrder(fairOrder);
                if (fairOrderAssorts.size() != 0) {
                    for (FairOrderAssort fairOrderAssort : fairOrderAssorts) {
                        fairOrderAssortDAO.delete(fairOrderAssort);
                    }
                }

                fairOrder.setDeleteStatus(true);
                fairOrderDAO.update(fairOrder);

                return Response.ok().entity(fairOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_READ)) {
                List<FairOrderDTO> fairOrderDTOs = new LinkedList<>();
                List<FairOrder> fairOrders = fairOrderDAO.getAll(userToken.getUser().getCompany());

                for (FairOrder fairOrder : fairOrders) {
                    List<FairOrderAssort> fairOrderAssorts = fairOrderAssortDAO.getByFairOrder(fairOrder);

                    Integer quantity = 0;

                    for (FairOrderAssort fairOrderAssort : fairOrderAssorts) {
                        quantity += fairOrderAssort.getAssort() * fairOrderAssort.getBoxNumber();
                    }

                    Double totalAmount = quantity * fairOrder.getAmount();

                    FairOrderDTO fairOrderDTO = new FairOrderDTO();
                    fairOrderDTO.setFairOrder(fairOrder);
                    fairOrderDTO.setTotalAmount(totalAmount);
                    fairOrderDTO.setQuantity(quantity);

                    fairOrderDTOs.add(fairOrderDTO);
                }

                return Response.ok().entity(fairOrderDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readFairOrderByDateRange")
    @UnitOfWork
    public Response readFairOrderByDateRange(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_READ)) {
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                List<FairOrderDTO> fairOrderDTOs = new LinkedList<>();
                List<FairOrder> fairOrders = fairOrderDAO.getByDateRange(start, end, userToken.getUser().getCompany());

                for (FairOrder fairOrder : fairOrders) {
                    List<FairOrderAssort> fairOrderAssorts = fairOrderAssortDAO.getByFairOrder(fairOrder);

                    Integer quantity = 0;

                    for (FairOrderAssort fairOrderAssort : fairOrderAssorts) {
                        quantity += fairOrderAssort.getAssort() * fairOrderAssort.getBoxNumber();
                    }

                    Double totalAmount = quantity * fairOrder.getAmount();

                    FairOrderDTO fairOrderDTO = new FairOrderDTO();
                    fairOrderDTO.setFairOrder(fairOrder);
                    fairOrderDTO.setTotalAmount(totalAmount);
                    fairOrderDTO.setQuantity(quantity);

                    fairOrderDTOs.add(fairOrderDTO);
                }

                return Response.ok().entity(fairOrderDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByCustomerForPrint")
    @UnitOfWork
    public Response readByCustomerForPrint(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_READ)) {
                String customerOid = data.get("customerOid");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");


                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                Customer customer = customerDAO.getById(customerOid, userToken.getUser().getCompany());

                List<FairOrderPrintDTO> fairOrderPrintDTOs = new LinkedList<>();
                List<FairOrder> fairOrders = fairOrderDAO.getByCustomerAndDate(customer, end, start, userToken.getUser().getCompany());

                for (FairOrder fairOrder : fairOrders) {
                    List<FairOrderAssort> fairOrderAssorts = fairOrderAssortDAO.getByFairOrder(fairOrder);

                    FairOrderPrintDTO fairOrderPrintDTO = new FairOrderPrintDTO();
                    fairOrderPrintDTO.setFairOrder(fairOrder);
                    fairOrderPrintDTO.setFairOrderAssorts(fairOrderAssorts);

                    fairOrderPrintDTOs.add(fairOrderPrintDTO);

                }

                return Response.ok().entity(fairOrderPrintDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("setTermin")
    @UnitOfWork
    public Response setTermin(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_READ)) {
                String customerOid = data.get("customerOid");
                String terminDate = data.get("terminDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime td = formatter.parseDateTime(terminDate);

                Customer customer = customerDAO.getById(customerOid, userToken.getUser().getCompany());

                List<FairOrder> fairOrders = fairOrderDAO.getByCustomer(customer, userToken.getUser().getCompany());

                for (FairOrder fairOrder : fairOrders) {
                    fairOrder.setTermin(td);
                    fairOrderDAO.update(fairOrder);
                }

                return Response.ok().entity(customer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("changePrice")
    @UnitOfWork
    public Response changePrice(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_ORDER_READ)) {
                String customerOid = data.get("customerOid");
                String priceName = data.get("priceName");

                Company company = userToken.getUser().getCompany();

                Customer customer = customerDAO.getById(customerOid, company);

                List<FairOrder> fairOrders = fairOrderDAO.getByCustomer(customer, company);

                for (FairOrder fairOrder : fairOrders) {
                    Model model = fairOrder.getModel();

                    FairModelPrice fairModelPrice = fairModelPriceDAO.getByPriceName(priceName, model, company).get(0);

                    if (fairModelPrice != null) {
                        fairOrder.setAmount(fairModelPrice.getAmount());
                        fairOrderDAO.update(fairOrder);
                    } else {
                        return Response.serverError().entity(ErrorMessage.UNKNOWN_PRICE.getErrorAlias()).build();
                    }
                }

                return Response.ok().entity(customer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
