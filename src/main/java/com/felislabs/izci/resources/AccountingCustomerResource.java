package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.AccountingCustomerAddressDAO;
import com.felislabs.izci.dao.dao.AccountingCustomerContactDAO;
import com.felislabs.izci.dao.dao.AccountingCustomerDAO;
import com.felislabs.izci.dao.dao.AccountingCustomerRepresentativeDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 07/03/2017.
 */
@Path("accountingCustomer")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountingCustomerResource {
    AuthUtil authUtil;
    AccountingCustomerDAO accountingCustomerDAO;
    AccountingCustomerAddressDAO accountingCustomerAddressDAO;
    AccountingCustomerContactDAO accountingCustomerContactDAO;
    AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO;

    public AccountingCustomerResource(AuthUtil authUtil, AccountingCustomerDAO accountingCustomerDAO, AccountingCustomerAddressDAO accountingCustomerAddressDAO, AccountingCustomerContactDAO accountingCustomerContactDAO, AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO) {
        this.authUtil = authUtil;
        this.accountingCustomerDAO = accountingCustomerDAO;
        this.accountingCustomerAddressDAO = accountingCustomerAddressDAO;
        this.accountingCustomerContactDAO = accountingCustomerContactDAO;
        this.accountingCustomerRepresentativeDAO = accountingCustomerRepresentativeDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_CREATE)) {
                String title = data.get("title");
                String code = data.get("code");
                String logoUrl = data.get("logoUrl");
                String taxNo = data.get("taxNo");
                String balanceLimit = data.get("balanceLimit");
                String maturity = data.get("maturity");
                String taxOffice = data.get("taxOffice");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = new AccountingCustomer();
                accountingCustomer.setCompany(company);
                accountingCustomer.setUser(user);
                accountingCustomer.setUpdateDate(new DateTime());
                accountingCustomer.setTitle(title);
                accountingCustomer.setCode(code);
                accountingCustomer.setLogoUrl(logoUrl);
                accountingCustomer.setTaxNo(taxNo);
                accountingCustomer.setBalanceLimit(Double.valueOf(balanceLimit));
                accountingCustomer.setMaturity(Integer.valueOf(maturity));
                accountingCustomer.setDescription(description);
                accountingCustomer.setTaxOffice(taxOffice);

                accountingCustomerDAO.create(accountingCustomer);

                return Response.ok().entity(accountingCustomer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();

                List<AccountingCustomer> accountingCustomers = accountingCustomerDAO.getAll(company);

                return Response.ok().entity(accountingCustomers).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ)) {
                String oid = data.get("oid");
                Company company = userToken.getUser().getCompany();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(oid, company);

                return Response.ok().entity(accountingCustomer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_UPDATE)) {
                String oid = data.get("oid");
                String title = data.get("title");
                String code = data.get("code");
                String logoUrl = data.get("logoUrl");
                String taxNo = data.get("taxNo");
                String balanceLimit = data.get("balanceLimit");
                String maturity = data.get("maturity");
                String description = data.get("description");
                String taxOffice = data.get("taxOffice");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(oid, company);
                accountingCustomer.setCompany(company);
                accountingCustomer.setUser(user);
                accountingCustomer.setUpdateDate(new DateTime());
                accountingCustomer.setTitle(title);
                accountingCustomer.setCode(code);
                accountingCustomer.setLogoUrl(logoUrl);
                accountingCustomer.setTaxNo(taxNo);
                accountingCustomer.setBalanceLimit(Double.valueOf(balanceLimit));
                accountingCustomer.setMaturity(Integer.valueOf(maturity));
                accountingCustomer.setDescription(description);
                accountingCustomer.setTaxOffice(taxOffice);

                accountingCustomerDAO.update(accountingCustomer);

                return Response.ok().entity(accountingCustomer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(oid, company);
                accountingCustomer.setDeleteStatus(true);

                accountingCustomerDAO.update(accountingCustomer);

                return Response.ok().entity(accountingCustomer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("contact/create")
    @UnitOfWork
    public Response contactCreate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_CREATE)) {
                String customerOid = data.get("customerOid");
                String mobilePhone = data.get("mobilePhone");
                String primaryEmail = data.get("primaryEmail");
                String secondaryEmail = data.get("secondaryEmail");
                String landPhone = data.get("landPhone");
                String fax = data.get("fax");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);

                AccountingCustomerContact accountingCustomerContact = new AccountingCustomerContact();
                accountingCustomerContact.setCompany(company);
                accountingCustomerContact.setUser(user);
                accountingCustomerContact.setUpdateDate(new DateTime());
                accountingCustomerContact.setMobilePhone(mobilePhone);
                accountingCustomerContact.setPrimaryEmail(primaryEmail);
                accountingCustomerContact.setSecondaryEmail(secondaryEmail);
                accountingCustomerContact.setLandPhone(landPhone);
                accountingCustomerContact.setFax(fax);
                accountingCustomerContact.setAccountingCustomer(accountingCustomer);

                accountingCustomerContactDAO.create(accountingCustomerContact);

                return Response.ok().entity(accountingCustomerContact).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("contact/update")
    @UnitOfWork
    public Response contactUpdate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_UPDATE)) {
                String oid = data.get("oid");
                String mobilePhone = data.get("mobilePhone");
                String primaryEmail = data.get("primaryEmail");
                String secondaryEmail = data.get("secondaryEmail");
                String landPhone = data.get("landPhone");
                String fax = data.get("fax");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomerContact accountingCustomerContact = accountingCustomerContactDAO.getByOid(oid, company);
                accountingCustomerContact.setCompany(company);
                accountingCustomerContact.setUser(user);
                accountingCustomerContact.setUpdateDate(new DateTime());
                accountingCustomerContact.setMobilePhone(mobilePhone);
                accountingCustomerContact.setPrimaryEmail(primaryEmail);
                accountingCustomerContact.setSecondaryEmail(secondaryEmail);
                accountingCustomerContact.setLandPhone(landPhone);
                accountingCustomerContact.setFax(fax);

                accountingCustomerContactDAO.update(accountingCustomerContact);

                return Response.ok().entity(accountingCustomerContact).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("contact/readByCustomer")
    @UnitOfWork
    public Response readByCustomer(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ)) {
                String customerOid = data.get("customerOid");

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, userToken.getUser().getCompany());

                List<AccountingCustomerContact> accountingCustomerContactList = accountingCustomerContactDAO.getByCustomer(accountingCustomer, userToken.getUser().getCompany());

                return Response.ok().entity(accountingCustomerContactList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("contact/read")
    @UnitOfWork
    public Response contactRead(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ)) {
                List<AccountingCustomerContact> accountingCustomerContactList = accountingCustomerContactDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(accountingCustomerContactList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("contact")
    @UnitOfWork
    public Response contactDelete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_DELETE)) {
                String oid = data.get("oid");

                AccountingCustomerContact accountingCustomerContact = accountingCustomerContactDAO.getByOid(oid, userToken.getUser().getCompany());
                accountingCustomerContact.setDeleteStatus(true);

                accountingCustomerContactDAO.update(accountingCustomerContact);

                return Response.ok().entity(accountingCustomerContact).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("address/create")
    @UnitOfWork
    public Response addressCreate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_CREATE)) {
                String customerOid = data.get("customerOid");

                String streetAvenue = data.get("streetAvenue");
                String neighborhood = data.get("neighborhood");
                String buildingNumber = data.get("buildingNumber");
                String doorNo = data.get("doorNo");
                String postalCode = data.get("postalCode");
                String city = data.get("city");
                String country = data.get("country");
                String addressDetail = data.get("addressDetail");
                String isDeliveryAddress = data.get("isDeliveryAddress");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);

                AccountingCustomerAddress accountingCustomerAddress = new AccountingCustomerAddress();
                accountingCustomerAddress.setCompany(company);
                accountingCustomerAddress.setUser(user);
                accountingCustomerAddress.setUpdateDate(new DateTime());
                accountingCustomerAddress.setStreetAvenue(streetAvenue);
                accountingCustomerAddress.setNeighborhood(neighborhood);
                accountingCustomerAddress.setBuildingNumber(buildingNumber);
                accountingCustomerAddress.setDoorNo(doorNo);
                accountingCustomerAddress.setPostalCode(postalCode);
                accountingCustomerAddress.setCity(city);
                accountingCustomerAddress.setCountry(country);
                accountingCustomerAddress.setIsDeliveryAddress(Boolean.valueOf(isDeliveryAddress));
                accountingCustomerAddress.setAccountingCustomer(accountingCustomer);
                accountingCustomerAddress.setAddressDetail(addressDetail);
                accountingCustomerAddress.setAccountingCustomer(accountingCustomer);

                accountingCustomerAddressDAO.create(accountingCustomerAddress);

                return Response.ok().entity(accountingCustomerAddress).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("address/update")
    @UnitOfWork
    public Response addressUpdate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_UPDATE)) {
                String oid = data.get("oid");
                String customerOid = data.get("customerOid");

                String streetAvenue = data.get("streetAvenue");
                String neighborhood = data.get("neighborhood");
                String buildingNumber = data.get("buildingNumber");
                String doorNo = data.get("doorNo");
                String postalCode = data.get("postalCode");
                String city = data.get("city");
                String country = data.get("country");
                String isDeliveryAddress = data.get("isDeliveryAddress");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);

                AccountingCustomerAddress accountingCustomerAddress = accountingCustomerAddressDAO.getByOid(oid, company);
                accountingCustomerAddress.setCompany(company);
                accountingCustomerAddress.setUser(user);
                accountingCustomerAddress.setUpdateDate(new DateTime());
                accountingCustomerAddress.setStreetAvenue(streetAvenue);
                accountingCustomerAddress.setNeighborhood(neighborhood);
                accountingCustomerAddress.setBuildingNumber(buildingNumber);
                accountingCustomerAddress.setDoorNo(doorNo);
                accountingCustomerAddress.setPostalCode(postalCode);
                accountingCustomerAddress.setCity(city);
                accountingCustomerAddress.setCountry(country);
                accountingCustomerAddress.setIsDeliveryAddress(Boolean.valueOf(isDeliveryAddress));
                accountingCustomerAddress.setAccountingCustomer(accountingCustomer);

                accountingCustomerAddressDAO.update(accountingCustomerAddress);

                return Response.ok().entity(accountingCustomerAddress).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("address/read")
    @UnitOfWork
    public Response addressRead(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ)) {
                List<AccountingCustomerAddress> accountingCustomerAddresses = accountingCustomerAddressDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(accountingCustomerAddresses).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("address/readByCustomer")
    @UnitOfWork
    public Response addressReadByCustomer(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ)) {
                String customerOid = data.get("customerOid");

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, userToken.getUser().getCompany());

                List<AccountingCustomerAddress> accountingCustomerAddresses = accountingCustomerAddressDAO.getByCustomer(accountingCustomer, userToken.getUser().getCompany());

                return Response.ok().entity(accountingCustomerAddresses).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("address")
    @UnitOfWork
    public Response addressDelete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_DELETE)) {
                String oid = data.get("oid");
                AccountingCustomerAddress accountingCustomerAddress = accountingCustomerAddressDAO.getByOid(oid, userToken.getUser().getCompany());
                accountingCustomerAddress.setDeleteStatus(true);

                accountingCustomerAddressDAO.update(accountingCustomerAddress);

                return Response.ok().entity(accountingCustomerAddress).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("representative/create")
    @UnitOfWork
    public Response representativeCreate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_CREATE)) {
                String customerOid = data.get("customerOid");

                String nameSurname = data.get("nameSurname");
                String mobilePhone = data.get("mobilePhone");
                String landPhone = data.get("landPhoneRepresentative");
                String internalPhoneCode = data.get("internalPhoneCode");
                String email = data.get("email");
                String title = data.get("title");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);

                AccountingCustomerRepresentative accountingCustomerRepresentative = new AccountingCustomerRepresentative();
                accountingCustomerRepresentative.setCompany(company);
                accountingCustomerRepresentative.setUser(user);
                accountingCustomerRepresentative.setUpdateDate(new DateTime());
                accountingCustomerRepresentative.setNameSurname(nameSurname);
                accountingCustomerRepresentative.setMobilePhone(mobilePhone);
                accountingCustomerRepresentative.setLandPhone(landPhone);
                accountingCustomerRepresentative.setInternalPhoneCode(internalPhoneCode);
                accountingCustomerRepresentative.setEmail(email);
                accountingCustomerRepresentative.setTitle(title);
                accountingCustomerRepresentative.setDescription(description);
                accountingCustomerRepresentative.setAccountingCustomer(accountingCustomer);

                accountingCustomerRepresentativeDAO.create(accountingCustomerRepresentative);

                return Response.ok().entity(accountingCustomerRepresentative).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("representative/update")
    @UnitOfWork
    public Response representativeUpdate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_UPDATE)) {
                String oid = data.get("oid");
                String nameSurname = data.get("nameSurname");
                String mobilePhone = data.get("mobilePhone");
                String landPhone = data.get("landPhoneRepresentative");
                String internalPhoneCode = data.get("internalPhoneCode");
                String email = data.get("email");
                String title = data.get("title");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(oid, company);
                accountingCustomerRepresentative.setCompany(company);
                accountingCustomerRepresentative.setUser(user);
                accountingCustomerRepresentative.setUpdateDate(new DateTime());
                accountingCustomerRepresentative.setNameSurname(nameSurname);
                accountingCustomerRepresentative.setMobilePhone(mobilePhone);
                accountingCustomerRepresentative.setLandPhone(landPhone);
                accountingCustomerRepresentative.setInternalPhoneCode(internalPhoneCode);
                accountingCustomerRepresentative.setEmail(email);
                accountingCustomerRepresentative.setTitle(title);
                accountingCustomerRepresentative.setDescription(description);

                accountingCustomerRepresentativeDAO.update(accountingCustomerRepresentative);

                return Response.ok().entity(accountingCustomerRepresentative).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("representative/read")
    @UnitOfWork
    public Response representativeRead(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ)) {
                List<AccountingCustomerRepresentative> accountingCustomerRepresentatives = accountingCustomerRepresentativeDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(accountingCustomerRepresentatives).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("representative/readByCustomer")
    @UnitOfWork
    public Response representativeReadByCustomer(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_READ)) {
                String customerOid = data.get("customerOid");

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, userToken.getUser().getCompany());

                List<AccountingCustomerRepresentative> accountingCustomerRepresentatives = accountingCustomerRepresentativeDAO.getByCustomer(accountingCustomer, userToken.getUser().getCompany());

                return Response.ok().entity(accountingCustomerRepresentatives).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("representative")
    @UnitOfWork
    public Response representativeDelete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNTING_CUSTOMER_CREATE)) {
                String oid = data.get("oid");

                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(oid, userToken.getUser().getCompany());
                accountingCustomerRepresentative.setDeleteStatus(true);

                accountingCustomerRepresentativeDAO.update(accountingCustomerRepresentative);

                return Response.ok().entity(accountingCustomerRepresentative).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
