package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.*;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import net.coobird.thumbnailator.Thumbnails;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;
import sun.misc.BASE64Decoder;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by gunerkaanalkim on 20/01/2017.
 */
@Path("model")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ModelResource {
    AuthUtil authUtil;
    ModelDAO modelDAO;
    CustomerDAO customerDAO;
    MrpSettingDAO mrpSettingDAO;
    ModelMaterialDAO modelMaterialDAO;
    ModelInternalCriticalDAO modelInternalCriticalDAO;
    ModelExternalCriticalDAO modelExternalCriticalDAO;
    StockDAO stockDAO;
    ModelPriceDetailDAO modelPriceDetailDAO;
    ModelOrderDAO modelOrderDAO;
    StockDetailDAO stockDetailDAO;
    ModelAssortDAO modelAssortDAO;
    ModelSayaDAO modelSayaDAO;
    ModelSayaIncomingDAO modelSayaIncomingDAO;
    ModelCuttingOrderDAO modelCuttingOrderDAO;
    ModelCuttingIncomingDAO modelCuttingIncomingDAO;
    ModelMontaDAO modelMontaDAO;
    ModelMontaIncomingDAO modelMontaIncomingDAO;

    public ModelResource(AuthUtil authUtil, ModelDAO modelDAO, CustomerDAO customerDAO, MrpSettingDAO mrpSettingDAO, ModelMaterialDAO modelMaterialDAO, ModelInternalCriticalDAO modelInternalCriticalDAO, ModelExternalCriticalDAO modelExternalCriticalDAO, StockDAO stockDAO, ModelPriceDetailDAO modelPriceDetailDAO, ModelOrderDAO modelOrderDAO, StockDetailDAO stockDetailDAO, ModelAssortDAO modelAssortDAO, ModelSayaDAO modelSayaDAO, ModelSayaIncomingDAO modelSayaIncomingDAO, ModelCuttingOrderDAO modelCuttingOrder, ModelCuttingIncomingDAO modelCuttingIncomingDAO, ModelMontaDAO modelMontaDAO, ModelMontaIncomingDAO modelMontaIncomingDAO) {
        this.authUtil = authUtil;
        this.modelDAO = modelDAO;
        this.customerDAO = customerDAO;
        this.mrpSettingDAO = mrpSettingDAO;
        this.modelMaterialDAO = modelMaterialDAO;
        this.modelInternalCriticalDAO = modelInternalCriticalDAO;
        this.modelExternalCriticalDAO = modelExternalCriticalDAO;
        this.stockDAO = stockDAO;
        this.modelPriceDetailDAO = modelPriceDetailDAO;
        this.modelOrderDAO = modelOrderDAO;
        this.stockDetailDAO = stockDetailDAO;
        this.modelAssortDAO = modelAssortDAO;
        this.modelSayaDAO = modelSayaDAO;
        this.modelSayaIncomingDAO = modelSayaIncomingDAO;
        this.modelCuttingOrderDAO = modelCuttingOrder;
        this.modelCuttingIncomingDAO = modelCuttingIncomingDAO;
        this.modelMontaDAO = modelMontaDAO;
        this.modelMontaIncomingDAO = modelMontaIncomingDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String customerOid = data.get("customerOid");
                String firmModelCode = data.get("firmModelCode");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                String series = data.get("series");
                String description = data.get("description");
                String modelColor = data.get("modelColor");
                String imagePaths = data.get("imagePaths");
                String season = data.get("season");
                String year = data.get("year");

                Company company = userToken.getUser().getCompany();

                Customer customer = customerDAO.getById(customerOid, company);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                String modelCode = "";

                List<MrpSetting> mrpSettings = mrpSettingDAO.get(company);

                modelCode = mrpSettings.get(0).getWorkOrderCount() + "-" + series;

                mrpSettings.get(0).setWorkOrderCount(mrpSettings.get(0).getWorkOrderCount() + 1);

                mrpSettingDAO.update(mrpSettings.get(0));

                Model model = new Model();
                model.setCustomer(customer);
                model.setCustomerModelCode(firmModelCode);
                model.setStartDate(start);
                model.setEndDate(end);
                model.setSeries(series);
                model.setDescription(description);
                model.setColor(modelColor);
                model.setImagePaths(imagePaths);
                model.setModelCode(modelCode);
                model.setCompany(company);
                model.setUpdateDate(new DateTime());
                model.setUpdater(userToken.getUser());
                model.setSeason(season);
                model.setYear(Integer.valueOf(year));

                String from = "";
                String to = "";

                String companyImageFolder = userToken.getUser().getCompany().getImageFolder();

                if (System.getProperty("os.name").equals("Windows 10")) {
                    from = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\modelimages\\" + companyImageFolder + "/";
                    to = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\thumbnail\\" + companyImageFolder + "/";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    from = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/modelimages/" + companyImageFolder + "/";
                    to = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/thumbnail/" + companyImageFolder + "/";
                } else {
                    from = "/var/www/html/modelimages/" + companyImageFolder + "/";
                    to = "/var/www/html/thumbnail/" + companyImageFolder + "/";
                }
                if (!imagePaths.equals("[]")) {
                    JSONArray jsonArray = new JSONArray(imagePaths);

                    File dest = new File(to + jsonArray.get(0).toString());

                    try {
                        Thumbnails.of(new File(from + jsonArray.get(0)))
                                .size(100, 150).toFile(dest);

                        model.setDefaultPhoto(jsonArray.get(0).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    model.setDefaultPhoto("blankImage.png");
                }

                modelDAO.create(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu model oluşturuldu. ", new DateTime());

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_UPDATE)) {
                String modelOid = data.get("oid");
                String customerOid = data.get("customerOid");
                String firmModelCode = data.get("firmModelCode");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                String series = data.get("series");
                String description = data.get("description");
                String modelColor = data.get("modelColor");
                String imagePaths = data.get("imagePaths");
                String season = data.get("season");
                String year = data.get("year");

                Company company = userToken.getUser().getCompany();

                Customer customer = customerDAO.getById(customerOid, company);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                Model model = modelDAO.getByOid(modelOid, company);
                model.setCustomer(customer);
                model.setCustomerModelCode(firmModelCode);
                model.setStartDate(start);
                model.setEndDate(end);
                model.setSeries(series);
                model.setDescription(description);
                model.setColor(modelColor);
                model.setImagePaths(imagePaths);
                model.setUpdateDate(new DateTime());
                model.setUpdater(userToken.getUser());
                model.setSeason(season);
                model.setYear(Integer.valueOf(year));

                modelDAO.update(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu model güncellendi. ", new DateTime());

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("confirm")
    @UnitOfWork
    public Response confirm(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_UPDATE)) {
                String modelOid = data.get("oid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());

                if (model.isConfirm()) {
                    model.setConfirm(false);
                } else {
                    model.setConfirm(true);
                }

                modelDAO.update(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu model onaylandı. ", new DateTime());

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("/uploadModelImage/{crmt}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @UnitOfWork
    public Response uploadWorkOrderImages(
            @PathParam("crmt") String crmt,
            @FormDataParam("modelImage") InputStream uploadedInputStream,
            @FormDataParam("modelImage") FormDataContentDisposition fileDetail) {

        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                //  ŞİRKET BİLGİLERİ
                String companyImageFolder = userToken.getUser().getCompany().getImageFolder();
                String osBasePath = "";

                if (System.getProperty("os.name").equals("Windows 10")) {
                    osBasePath = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\modelimages\\" + companyImageFolder + "/";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    osBasePath = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/modelimages/" + companyImageFolder + "/";
                } else {
                    osBasePath = "/var/www/html/modelimages/" + companyImageFolder + "/";
                }

                String fileExtension = "";

                if (fileDetail.getFileName().endsWith(".png") || fileDetail.getFileName().endsWith(".PNG")) {
                    fileExtension = ".png";
                } else if (fileDetail.getFileName().endsWith(".jpg") || fileDetail.getFileName().endsWith(".JPG") || fileDetail.getFileName().endsWith(".JPEG") || fileDetail.getFileName().endsWith(".jpeg")) {
                    fileExtension = ".jpg";
                }

                DateTime fileNameDT = new DateTime();

                String fileName = fileNameDT.getMillis() + Math.random() + fileExtension;

                String uploadedFileLocation = osBasePath + fileName;

                // save it with thumbnail
                try {
                    BufferedImage inBuff = ImageIO.read(uploadedInputStream);
                    int w = inBuff.getWidth();
                    int h = inBuff.getHeight();
                    if (h > w) {
                        try {
                            Thumbnails.of(inBuff).rotate(90).size(500, 500).toFile(uploadedFileLocation);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Thumbnails.of(inBuff).size(500, 500).toFile(uploadedFileLocation);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                WorkOrderImageDTO workOrderImageDTO = new WorkOrderImageDTO();
                workOrderImageDTO.setPath(fileName);

                return Response.ok(workOrderImageDTO).build();
            }
        }

        return Response.ok().build();
    }

    @POST
    @Path("/fakeRemoveImageService")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @UnitOfWork
    public Response fakeRemoveImageService() {
        return Response.ok(Response.Status.OK).build();
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                return Response.ok(modelDAO.getAll(userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIfHasOrder")
    @UnitOfWork
    public Response readIfHasOrder(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                List<Model> modelList = modelDAO.getIsOrder(userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());
                    modelSummary.setYear(String.valueOf(model.getSeason()) == null ? "" : String.valueOf(model.getSeason()));

                    modelSummaries.add(modelSummary);
                }
                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIfHasOrderByDateRange")
    @UnitOfWork
    public Response readIfHasOrderByDateRange(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                List<Model> modelList = modelDAO.getIsOrderByDateRange(start, end, userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIfHasOrderByYearAndSeason")
    @UnitOfWork
    public Response readIfHasOrderByYearAndSeason(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String season = data.get("season");
                String year = data.get("year");

                List<Model> modelList = modelDAO.getIsOrderBySeasonAndYear(season, Integer.valueOf(year), userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getIsOrderAsSupplyCompleted")
    @UnitOfWork
    public Response getIsOrderAsSupplyCompleted(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                List<Model> modelList = modelDAO.getIsOrderAsSupplyCompleted(userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getIsOrderAsSupplyCompletedByDateRange")
    @UnitOfWork
    public Response getIsOrderAsSupplyCompletedByDateRange(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                List<Model> modelList = modelDAO.getIsOrderAsSupplyCompletedByDateRange(start, end, userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getIsOrderAsSupplyCompletedByYearAndSeason")
    @UnitOfWork
    public Response getIsOrderAsSupplyCompletedByYearAndSeason(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String year = data.get("year");
                String season = data.get("season");

                List<Model> modelList = modelDAO.getIsOrderAsSupplyCompletedBySeasonAndYear(season, Integer.valueOf(year), userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIfHasPricing")
    @UnitOfWork
    public Response readIfHasPricing(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                List<Model> modelList = modelDAO.getIsPrice(userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIfHasPricingByDateRange")
    @UnitOfWork
    public Response readIfHasPricingByDateRange(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                List<Model> modelList = modelDAO.getIsPriceByDateRange(start, end, userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


    @POST
    @Path("readIfHasPricingBySeasonAndYear")
    @UnitOfWork
    public Response readIfHasPricingBySeasonAndYear(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String season = data.get("season");
                String year = data.get("year");

                List<Model> modelList = modelDAO.getIsPriceBySeasonAndYear(season, Integer.valueOf(year), userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIfHasMaterial")
    @UnitOfWork
    public Response readIfHasMaterial(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                List<Model> modelList = modelDAO.getIsMaterial(userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }
                return Response.ok(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIfHasMaterialByDateRange")
    @UnitOfWork
    public Response readIfHasMaterialByDateRange(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                List<Model> modelList = modelDAO.getIsMaterialByDateRange(start, end, userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());
                    modelSummaries.add(modelSummary);
                }
                return Response.ok(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIfHasMaterialByYearAndSeason")
    @UnitOfWork
    public Response readIfHasMaterialByYearAndSeason(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String season = data.get("season");
                String year = data.get("year");

                List<Model> modelList = modelDAO.getIsMaterialBySeasonAndYear(season, Integer.valueOf(year), userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());
                    modelSummaries.add(modelSummary);
                }
                return Response.ok(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByOrder")
    @UnitOfWork
    public Response readByOrder(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                List<Model> models = modelDAO.getAll(userToken.getUser().getCompany());

                List<Model> modelList = new LinkedList<>();

                for (Model model : models) {
                    ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                    if (modelOrder != null) {
                        modelList.add(model);
                    }
                }

                return Response.ok().entity(modelList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(oid, company);

                return Response.ok(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_DELETE)) {
                String modelOid = data.get("oid");
                String reason = data.get("reason");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                model.setDeleteStatus(true);
                model.setDeleteReason(reason);

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());
                if (modelOrder != null) {
                    modelOrder.setDeleteStatus(true);
                    modelOrderDAO.update(modelOrder);

                    //                Cutting Order
                    List<ModelCuttingOrder> modelCuttingOrders = modelCuttingOrderDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());
                    if (modelCuttingOrders.size() != 0) {
                        for (ModelCuttingOrder modelCuttingOrder : modelCuttingOrders) {
                            modelCuttingOrder.setDeleteStatus(true);
                            modelCuttingOrderDAO.update(modelCuttingOrder);
                        }
                    }

//                Cutting Incomings
                    List<ModelCuttingIncoming> modelCuttingIncomings = modelCuttingIncomingDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());
                    if (modelCuttingIncomings.size() != 0) {
                        for (ModelCuttingIncoming modelCuttingIncoming : modelCuttingIncomings) {
                            modelCuttingIncoming.setDeleteStatus(true);
                            modelCuttingIncomingDAO.update(modelCuttingIncoming);
                        }
                    }

//                Saya Order
                    List<ModelSaya> modelSayas = modelSayaDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());
                    if (modelSayas.size() != 0) {
                        for (ModelSaya modelSaya : modelSayas) {
                            modelSaya.setDeleteStatus(true);
                            modelSayaDAO.update(modelSaya);
                        }
                    }

//                Saya Incomings
                    List<ModelSayaIncoming> modelSayaIncomings = modelSayaIncomingDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());
                    if (modelSayaIncomings.size() != 0) {
                        for (ModelSayaIncoming modelSayaIncoming : modelSayaIncomings) {
                            modelSayaIncoming.setDeleteStatus(true);
                            modelSayaIncomingDAO.update(modelSayaIncoming);
                        }
                    }

//                Monta Order
                    List<ModelMonta> modelMontas = modelMontaDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());
                    if (modelMontas.size() != 0) {
                        for (ModelMonta modelMonta : modelMontas) {
                            modelMonta.setDeleteStatus(true);
                            modelMontaDAO.update(modelMonta);
                        }
                    }

//                Monta Incomings
                    List<ModelMontaIncoming> modelMontaIncomings = modelMontaIncomingDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());
                    if (modelMontaIncomings.size() != 0) {
                        for (ModelMontaIncoming modelMontaIncoming : modelMontaIncomings) {
                            modelMontaIncoming.setDeleteStatus(true);
                            modelMontaIncomingDAO.update(modelMontaIncoming);
                        }
                    }
                }

                modelDAO.update(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu model silindi. ", new DateTime());

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("setDefaultPhoto")
    @UnitOfWork
    public Response setDefaultPhoto(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String photoName = data.get("photoName");
                String modelOid = data.get("oid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());

                String from = "";
                String to = "";
                String companyImageFolder = userToken.getUser().getCompany().getImageFolder();

                if (System.getProperty("os.name").equals("Windows 10")) {
                    from = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\modelimages\\" + companyImageFolder + "/";
                    to = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\thumbnail\\" + companyImageFolder + "/";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    from = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/modelimages/" + companyImageFolder + "/";
                    to = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/thumbnail/" + companyImageFolder + "/";
                } else {
                    from = "/var/www/html/modelimages/" + companyImageFolder + "/";
                    to = "/var/www/html/thumbnail/" + companyImageFolder + "/";
                }

                File dest = new File(to + photoName);
                // save it with thumbnail
                try {
                    BufferedImage inBuff = ImageIO.read(new File(from + photoName));
                    int w = inBuff.getWidth();
                    int h = inBuff.getHeight();
                    if (h > w) {
                        try {
                            Thumbnails.of(inBuff)
                                    .size(100, 150).rotate(90).toFile(dest);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Thumbnails.of(inBuff)
                                    .size(100, 150).toFile(dest);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                model.setDefaultPhoto(photoName);
                modelDAO.update(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelin varsayılan görseli belirlendi. ", new DateTime());

                return Response.ok(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deletePhoto")
    @UnitOfWork
    public Response deletePhoto(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String imageName = data.get("photoName");
                String modelOid = data.get("oid");
                String companyImageFolder = userToken.getUser().getCompany().getImageFolder();

                String osBasePath = "";

                if (System.getProperty("os.name").equals("Windows 10")) {
                    osBasePath = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\modelimages\\" + companyImageFolder + "/";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    osBasePath = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/modelimages/" + companyImageFolder + "/";
                } else {
                    osBasePath = "/var/www/html/modelimages/" + companyImageFolder + "/";
                }

                String uploadedFileLocation = osBasePath + imageName;

                Model model = null;

                try {
                    File file = new File(uploadedFileLocation);

                    if (file.delete()) {
                        System.out.println(file.getName() + " is deleted!");

                        model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                        JSONArray jsonArray = new JSONArray(model.getImagePaths());

                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (jsonArray.get(i).equals(imageName)) {
                                System.out.print(imageName);
                                jsonArray.remove(i);
                            }
                        }

                        model.setImagePaths(jsonArray.toString());
                        modelDAO.update(model);
                    } else {
                        System.out.println("Delete operation is failed.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                WorkOrderImageDTO workOrderImageDTO = new WorkOrderImageDTO();
                workOrderImageDTO.setPath(imageName);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelden bir fotoğraf silindi. ", new DateTime());

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteModelImage")
    @UnitOfWork
    public Response deleteModelImage(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String imageName = data.get("photoName");
                String modelOid = data.get("oid");
                String companyImageFolder = userToken.getUser().getCompany().getImageFolder();

                String osBasePath = "";

                if (System.getProperty("os.name").equals("Windows 10")) {
                    osBasePath = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\modelimages\\" + companyImageFolder + "/";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    osBasePath = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/modelimages/" + companyImageFolder + "/";
                } else {
                    osBasePath = "/var/www/html/modelimages/" + companyImageFolder + "/";
                }


                String uploadedFileLocation = osBasePath + imageName;

                Model model = null;

                try {
                    File file = new File(uploadedFileLocation);

                    if (file.delete()) {
                        System.out.println(file.getName() + " is deleted!");

                        model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                        JSONArray jsonArray = new JSONArray(model.getModelImagePaths());

                        for (int i = 0; i < jsonArray.length(); i++) {
                            if (jsonArray.get(i).equals(imageName)) {
                                System.out.print(imageName);
                                jsonArray.remove(i);
                            }
                        }

                        model.setModelImagePaths(jsonArray.toString());
                        modelDAO.update(model);

                        authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelden bir fotoğraf silindi. ", new DateTime());

                    } else {
                        System.out.println("Delete operation is failed.");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("updateForOwnModels")
    @UnitOfWork
    public Response updateForOwnModels(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_UPDATE)) {
                String modelOid = data.get("modelOid");
                String moldCode = data.get("moldCode");
                String footbedCode = data.get("footbedCode");
                String number = data.get("number");
                String knifeCode = data.get("knifeCode");
                String mostraKnifeCode = data.get("mostraKnifeCode");
                String salpaKnifeCode = data.get("salpaKnifeCode");
                String materials = data.get("materials");
                String modelImages = data.get("modelImages");
                String moldSerial = data.get("moldSerial");
                String footbedSerial = data.get("footbedSerial");
                String knifeSerial = data.get("knifeSerial");
                String mostraKnifeSerial = data.get("mostraKnifeSerial");
                String salpaKnifeSerial = data.get("salpaKnifeSerial");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                //  Model Updating
                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                model.setMoldCode(moldCode);
                model.setFootbedCode(footbedCode);
                model.setNumber(number);
                model.setKnifeCode(knifeCode);
                model.setMostraKnifeCode(mostraKnifeCode);
                model.setSalpaKnifeCode(salpaKnifeCode);
                model.setModelImagePaths(modelImages);
                model.setMoldSerial(moldSerial);
                model.setFootbedSerial(footbedSerial);
                model.setKnifeSerial(knifeSerial);
                model.setMostraKnifeSerial(mostraKnifeSerial);
                model.setSalpaKnifeSerial(salpaKnifeSerial);
                model.setMaterialEntry(true);

                modelDAO.update(model);

                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(model, company);
                List<ModelMaterial> newModelMaterials = new LinkedList<>();

                //  Creating materials of model
                JSONArray jsonArray = new JSONArray(materials);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String placeOfUse = jsonObject.get("placeOfUse").toString();
                    String materialName = jsonObject.get("name").toString();
                    String uid = jsonObject.get("uid").toString();

                    ModelMaterial modelMaterial = modelMaterialDAO.getByUid(model, uid, placeOfUse, company);

                    if (modelMaterial != null) {
                        modelMaterialDAO.detach(modelMaterial);
                        modelMaterial.setCompany(company);
                        modelMaterial.setUpdater(user);
                        modelMaterial.setUpdateDate(new DateTime());
                        modelMaterial.setMaterialName(materialName);
                        modelMaterial.setPlaceOfUse(placeOfUse);
                        modelMaterial.setModel(model);
                        modelMaterial.setUid(uid);
                        newModelMaterials.add(modelMaterial);
                    } else {
                        modelMaterial = new ModelMaterial();
                        modelMaterial.setCompany(company);
                        modelMaterial.setUpdater(user);
                        modelMaterial.setUpdateDate(new DateTime());
                        modelMaterial.setMaterialName(materialName);
                        modelMaterial.setPlaceOfUse(placeOfUse);
                        modelMaterial.setModel(model);
                        modelMaterial.setUid(uid);
                        newModelMaterials.add(modelMaterial);
                    }
                }

                //  Delete All Old Materials(if exist)
                if (modelMaterials.size() != 0) {
                    for (ModelMaterial modelMaterial : modelMaterials) {
                        modelMaterialDAO.delete(modelMaterial);
                    }
                }

                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);

                //  Updating model subtotal
                Double subTotal = 0.0;

                for (ModelMaterial newModelMaterial : newModelMaterials) {
                    Double constant = 0.0;
                    Double unitPrice = 0.0;

                    if (newModelMaterial.getConstant() != null) {
                        constant = newModelMaterial.getConstant();
                    } else {
                        constant = 0.0;
                    }

                    if (newModelMaterial.getUnitPrice() != null) {
                        unitPrice = newModelMaterial.getUnitPrice();
                    } else {
                        unitPrice = 0.0;
                    }

                    String unit = "";

                    if (newModelMaterial.getStock() != null) {
                        unit = newModelMaterial.getStock().getUnit();
                    }

                    Double lineTotal = 0.0;

                    String calcType = mrpSetting.getCalculationType();

                    if (calcType == null || calcType == "") {
                        calcType = "constant";
                    }

                    if (!unit.equals("Paket") && !unit.equals("Grose")) {
                        if (calcType.equals("constant")) {
                            if (constant == 0.0) {
                                lineTotal = 0.0;
                            } else {
                                lineTotal = unitPrice / constant;
                            }
                        } else if (calcType.equals("area")) {
                            lineTotal = constant * unitPrice;
                        }
                    } else {
                        lineTotal = constant * unitPrice;
                    }

                    subTotal += lineTotal;

                    newModelMaterial.setOid(null);
                    modelMaterialDAO.create(newModelMaterial);
                }

                List<ModelPriceDetail> modelPriceDetails = modelPriceDetailDAO.getByModel(model, company);

                for (ModelPriceDetail modelPriceDetail : modelPriceDetails) {
                    Double mpd = modelPriceDetail.getPrice();

                    if (mpd == null) {
                        mpd = 0.0;
                    }

                    subTotal += mpd;
                }

                model.setModelPrice(subTotal);
                modelDAO.update(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelin malzeme bilgisi güncellendi. ", new DateTime());

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("updatePrice")
    @UnitOfWork
    public Response updatePrice(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PRICE)) {
                String modelOid = data.get("modelOid");
                String materialPrices = data.get("materialPrices");
                String priceDetail = data.get("priceDetail");
                String modelPrice = data.get("modelPrice");
                String profitRatio = data.get("profitRatio");
                String calculationType = data.get("calculationType");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                Model model = modelDAO.getByOid(modelOid, company);
                model.setModelPrice(Double.valueOf(modelPrice));
                model.setProfitRatio(Double.valueOf(profitRatio));
                model.setCalculationType(calculationType);
                model.setPricingEntry(true);

                modelDAO.update(model);

                JSONArray jsonArray = new JSONArray(materialPrices);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String materialOid = jsonObject.getString("materialOid");
                    String stockCode = jsonObject.getString("stockCode");
                    String stockDetailOid = jsonObject.getString("stockDetailOid");
                    String constant = jsonObject.getString("constant");
                    String unitPrice = jsonObject.getString("unitPrice");
                    String lineTotal = jsonObject.getString("lineTotal");
                    String lineUnit = jsonObject.getString("lineUnit");

                    Stock stock = stockDAO.getByCode(stockCode, company);
                    StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);

                    ModelMaterial modelMaterial = modelMaterialDAO.getByOid(materialOid, company);
                    modelMaterial.setStock(stock);
                    modelMaterial.setConstant(Double.valueOf(constant));
                    modelMaterial.setUnitPrice(Double.valueOf(unitPrice));
                    modelMaterial.setLineTotal(Double.valueOf(lineTotal));
                    modelMaterial.setUpdateDate(new DateTime());
                    modelMaterial.setUpdater(user);
                    modelMaterial.setStockDetail(stockDetail == null ? null : stockDetail);
                    modelMaterial.setLineUnit(lineUnit);
                    modelMaterialDAO.update(modelMaterial);
                }

                List<ModelPriceDetail> modelPriceDetails = modelPriceDetailDAO.getByModel(model, company);

                //  Delete if exist
                if (modelPriceDetails.size() != 0) {
                    for (ModelPriceDetail modelPriceDetail : modelPriceDetails) {
                        modelPriceDetail.setDeleteStatus(true);
                        modelPriceDetailDAO.update(modelPriceDetail);
                    }
                }

                JSONArray modelPriceDetailArray = new JSONArray(priceDetail);

                for (int i = 0; i < modelPriceDetailArray.length(); i++) {
                    JSONObject jsonObject = modelPriceDetailArray.getJSONObject(i);

                    String description = jsonObject.getString("description");
                    String price = jsonObject.getString("price");
                    String type = jsonObject.getString("type");

                    ModelPriceDetail modelPriceDetail = new ModelPriceDetail();
                    modelPriceDetail.setModel(model);
                    modelPriceDetail.setCompany(company);
                    modelPriceDetail.setUpdater(user);
                    modelPriceDetail.setUpdateDate(new DateTime());
                    modelPriceDetail.setDescription(description);
                    modelPriceDetail.setPrice(Double.valueOf(price));
                    modelPriceDetail.setType(type);

                    modelPriceDetailDAO.create(modelPriceDetail);
                }

//                  Updating model subtotal
                Double subTotal = 0.0;
                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);

                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(model, company);

                for (ModelMaterial newModelMaterial : modelMaterials) {
                    Double constant = 0.0;
                    Double unitPrice = 0.0;

                    if (newModelMaterial.getConstant() != null) {
                        constant = newModelMaterial.getConstant();
                    }

                    if (newModelMaterial.getUnitPrice() != null) {
                        unitPrice = newModelMaterial.getUnitPrice();
                    }

                    String unit = "";

                    if (newModelMaterial.getStock() != null) {
                        unit = newModelMaterial.getStock().getUnit();
                    }

                    Double lineTotal = 0.0;

                    String calcType = mrpSetting.getCalculationType();

                    if (calcType == null || calcType == "") {
                        calcType = "constant";
                    }
                    if (!unit.equals("Paket") && !unit.equals("Grose")) {
                        if (calcType.equals("constant")) {
                            if (constant == 0.0) {
                                lineTotal = 0.0;
                            } else {
                                lineTotal = unitPrice / constant;
                            }
                        } else if (calcType.equals("area")) {
                            lineTotal = constant * unitPrice;
                        }
                    } else {
                        lineTotal = constant * unitPrice;
                    }

                    subTotal += lineTotal;
                }

                List<ModelPriceDetail> newModelPriceDetails = modelPriceDetailDAO.getByModel(model, company);

                for (ModelPriceDetail modelPriceDetail : newModelPriceDetails) {
                    Double mpd = modelPriceDetail.getPrice();

                    if (mpd == null) {
                        mpd = 0.0;
                    }

                    subTotal += mpd;
                }
                System.out.println(subTotal);

                model.setModelPrice(subTotal);
                modelDAO.update(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelin fiyat bilgisi oluşturuldu/güncelledi. ", new DateTime());

                return Response.ok(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readModelMaterial")
    @UnitOfWork
    public Response readModelMaterial(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(oid, company);
                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(model, company);

                return Response.ok(modelMaterials).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("createSerie")
    @UnitOfWork
    public Response createSerie(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String modelOid = data.get("modelOid");
                String color = data.get("color");
                String series = data.get("series");
                String image = data.get("image");
                String defaultImage = data.get("defaultImage");
                if (defaultImage == "") {
                    defaultImage = "blankImage.png";
                }

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Model masterModel = modelDAO.getByOid(modelOid, company);
                modelDAO.detach(masterModel);

                Model newModel = masterModel;

                String newModelCode = masterModel.getModelCode().substring(0, masterModel.getModelCode().length() - 1) + series;

                newModel.setColor(color);
                newModel.setImagePaths(image);
                newModel.setDefaultPhoto(image);
                newModel.setUpdater(user);
                newModel.setUpdateDate(new DateTime());
                newModel.setCreationDateTime(new DateTime());
                newModel.setModelCode(newModelCode);
                newModel.setMoldCode(masterModel.getMoldCode());
                newModel.setFootbedCode(masterModel.getFootbedCode());
                newModel.setNumber(masterModel.getNumber());
                newModel.setKnifeCode(masterModel.getKnifeCode());
                newModel.setMostraKnifeCode(masterModel.getMostraKnifeCode());
                newModel.setSalpaKnifeCode(masterModel.getSalpaKnifeCode());
                newModel.setMoldSerial(masterModel.getMoldSerial());
                newModel.setFootbedSerial(masterModel.getFootbedSerial());
                newModel.setKnifeSerial(masterModel.getKnifeSerial());
                newModel.setMostraKnifeSerial(masterModel.getMostraKnifeSerial());
                newModel.setSalpaKnifeSerial(masterModel.getSalpaKnifeSerial());
                newModel.setSupplyCompleted(false);
                newModel.setOid(null);


                String from = "";
                String to = "";

                String companyImageFolder = userToken.getUser().getCompany().getImageFolder();

                if (System.getProperty("os.name").equals("Windows 10")) {
                    from = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\modelimages\\" + companyImageFolder + "/";
                    to = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\thumbnail\\" + companyImageFolder + "/";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    from = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/modelimages/" + companyImageFolder + "/";
                    to = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/thumbnail/" + companyImageFolder + "/";
                } else {
                    from = "/var/www/html/modelimages/" + companyImageFolder + "/";
                    to = "/var/www/html/thumbnail/" + companyImageFolder + "/";
                }

                File dest = new File(to + defaultImage);

                try {
                    Thumbnails.of(new File(from + defaultImage))
                            .size(100, 150).toFile(dest);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                newModel.setDefaultPhoto(defaultImage);


                modelDAO.create(newModel);

                Model masterModelForPriceDetail = modelDAO.getByOid(modelOid, company);
                modelDAO.detach(masterModelForPriceDetail);

                List<ModelPriceDetail> modelPriceDetails = modelPriceDetailDAO.getByModel(masterModelForPriceDetail, company);
                if (modelPriceDetails.size() != 0) {
                    for (ModelPriceDetail modelPriceDetail : modelPriceDetails) {
                        modelPriceDetailDAO.detach(modelPriceDetail);
                        ModelPriceDetail modelPriceDetailNewModel = modelPriceDetail;
                        modelPriceDetailNewModel.setOid(null);
                        modelPriceDetailNewModel.setCreationDateTime(new DateTime());
                        modelPriceDetailNewModel.setUpdater(user);
                        modelPriceDetailNewModel.setModel(newModel);
                        modelPriceDetailNewModel.setCompany(company);
                        modelPriceDetailDAO.create(modelPriceDetailNewModel);

                    }
                }
                Model masterModelForMaterials = modelDAO.getByOid(modelOid, company);
                modelDAO.detach(masterModelForMaterials);
                //  Getting Materials
                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(masterModelForMaterials, company);
                if (modelMaterials.size() != 0) {
                    for (ModelMaterial modelMaterial : modelMaterials) {
                        modelMaterialDAO.detach(modelMaterial);

                        ModelMaterial modelMaterialForNewModel = modelMaterial;
                        modelMaterialForNewModel.setOid(null);
                        modelMaterialForNewModel.setUpdateDate(new DateTime());
                        modelMaterialForNewModel.setUpdater(user);
                        modelMaterialForNewModel.setCreationDateTime(new DateTime());
                        modelMaterialForNewModel.setModel(newModel);

                        modelMaterialDAO.create(modelMaterialForNewModel);
                    }
                }

                Model masterModelForExternalCriticals = modelDAO.getByOid(modelOid, company);
                modelDAO.detach(masterModelForExternalCriticals);

                List<ModelExternalCritical> modelExternalCriticals = modelExternalCriticalDAO.getByModel(masterModelForExternalCriticals, company);
                if (modelExternalCriticals.size() != 0) {
                    for (ModelExternalCritical modelExternalCritical : modelExternalCriticals) {
                        ModelExternalCritical modelExternalCriticalForNewModel = new ModelExternalCritical();
                        modelExternalCriticalForNewModel.setModel(newModel);
                        modelExternalCriticalForNewModel.setDescription(modelExternalCritical.getDescription());
                        modelExternalCriticalForNewModel.setCompany(modelExternalCritical.getCompany());
                        modelExternalCriticalForNewModel.setUpdater(modelExternalCritical.getUpdater());
                        modelExternalCriticalForNewModel.setUpdateDate(new DateTime());

                        modelExternalCriticalDAO.create(modelExternalCriticalForNewModel);
                    }
                }

                Model masterModelForInternalCriticals = modelDAO.getByOid(modelOid, company);
                modelDAO.detach(masterModelForInternalCriticals);

                List<ModelInternalCritical> modelInternalCriticals = modelInternalCriticalDAO.getByModel(masterModelForInternalCriticals, company);
                if (modelInternalCriticals.size() != 0) {
                    for (ModelInternalCritical modelInternalCritical : modelInternalCriticals) {
                        ModelInternalCritical modelInternalCriticalForNewModel = new ModelInternalCritical();
                        modelInternalCriticalForNewModel.setModel(newModel);
                        modelInternalCriticalForNewModel.setTitle(modelInternalCritical.getTitle());
                        modelInternalCriticalForNewModel.setAnswer(modelInternalCritical.getAnswer());
                        modelInternalCriticalForNewModel.setCompany(modelInternalCritical.getCompany());
                        modelInternalCriticalForNewModel.setUpdater(modelInternalCritical.getUpdater());
                        modelInternalCriticalForNewModel.setUpdateDate(new DateTime());

                        modelInternalCriticalDAO.create(modelInternalCriticalForNewModel);
                    }
                }

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), masterModel.getModelCode() + " kodlu modelden seri oluşturuldu. ", new DateTime());

                return Response.ok(newModel).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("updateInternalCritical")
    @UnitOfWork
    public Response updateInternalCritical(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String modelOid = data.get("modelOid");
                String internalCritical = data.get("ic");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Model model = modelDAO.getByOid(modelOid, company);

                //  Delete old internal critical(if exist)
                List<ModelInternalCritical> modelInternalCriticals = modelInternalCriticalDAO.getByModel(model, company);
                for (ModelInternalCritical modelInternalCritical : modelInternalCriticals) {
                    modelInternalCriticalDAO.delete(modelInternalCritical);
                }

                JSONArray jsonArray = new JSONArray(internalCritical);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    ModelInternalCritical modelInternalCritical = new ModelInternalCritical();
                    modelInternalCritical.setCompany(company);
                    modelInternalCritical.setUpdater(user);
                    modelInternalCritical.setUpdateDate(new DateTime());
                    modelInternalCritical.setModel(model);
                    modelInternalCritical.setTitle(jsonObject.getString("title"));
                    modelInternalCritical.setAnswer(jsonObject.getString("answer"));

                    modelInternalCriticalDAO.create(modelInternalCritical);
                }

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelin iç kritikleri güncellendi. ", new DateTime());

                return Response.ok(internalCritical).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readInternalCritical")
    @UnitOfWork
    public Response readInternalCritical(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(modelOid, company);

                List<ModelInternalCritical> modelInternalCriticals = modelInternalCriticalDAO.getByModel(model, company);

                return Response.ok(modelInternalCriticals).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("updateExternalCritical")
    @UnitOfWork
    public Response updateExternalCritical(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String modelOid = data.get("modelOid");
                String externalCritical = data.get("ec");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Model model = modelDAO.getByOid(modelOid, company);

                //  Delete old external critical(if exist)
                List<ModelExternalCritical> modelExternalCriticals = modelExternalCriticalDAO.getByModel(model, company);
                for (ModelExternalCritical modelExternalCritical : modelExternalCriticals) {
                    modelExternalCriticalDAO.delete(modelExternalCritical);
                }

                JSONArray jsonArray = new JSONArray(externalCritical);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    ModelExternalCritical modelExternalCritical = new ModelExternalCritical();
                    modelExternalCritical.setCompany(company);
                    modelExternalCritical.setUpdater(user);
                    modelExternalCritical.setUpdateDate(new DateTime());
                    modelExternalCritical.setModel(model);
                    modelExternalCritical.setDescription(jsonObject.getString("description"));

                    modelExternalCriticalDAO.create(modelExternalCritical);
                }
                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelin dış kritikleri güncellendi. ", new DateTime());

                return Response.ok(externalCritical).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readExternalCritical")
    @UnitOfWork
    public Response readExternalCritical(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(modelOid, company);

                List<ModelExternalCritical> modelExternalCriticals = modelExternalCriticalDAO.getByModel(model, company);

                return Response.ok(modelExternalCriticals).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readModelPriceDetail")
    @UnitOfWork
    public Response readModelPriceDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PRICE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                Company company = userToken.getUser().getCompany();

                List<ModelPriceDetail> modelPriceDetails = modelPriceDetailDAO.getByModel(model, company);

                return Response.ok(modelPriceDetails).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSupplyList")
    @UnitOfWork
    public Response readForSupply(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PRICE)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);
                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(model, company);

                SupplyDTO supplyDTO = new SupplyDTO();
                supplyDTO.setModelMaterials(modelMaterials);
                supplyDTO.setModelOrder(modelOrder);

                return Response.ok(supplyDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    // Stok-kabul.html KendoAutoComplete için.
    @POST
    @Path("read/{crmt}/{modelCode}")
    @UnitOfWork
    public Response readDetailByCode(@PathParam("crmt") String crmt, @PathParam("modelCode") String modelCode) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                Company company = userToken.getUser().getCompany();
                List<ModelOrder> modelOrders = modelOrderDAO.getByContainsInCode(modelCode, company);
                List<ModelNameGenaratorDTO> modelNameGenaratorDTOList = new ArrayList<ModelNameGenaratorDTO>();
                for (int i = 0; i < modelOrders.size(); i++) {
                    ModelNameGenaratorDTO modelNameGenaratorDTO = new ModelNameGenaratorDTO();
                    modelNameGenaratorDTO.setModelCode(modelOrders.get(i).getModel().getModelCode() + " / " + modelOrders.get(i).getModel().getColor() + " / " + modelOrders.get(i).getOrderQuantity());
                    modelNameGenaratorDTO.setOid(modelOrders.get(i).getModel().getOid());
                    modelNameGenaratorDTOList.add(modelNameGenaratorDTO);
                }
                return Response.ok(modelNameGenaratorDTOList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    //returns model list only model which have order
    @POST
    @Path("getModelOrderByCustomer")
    @UnitOfWork
    public Response getModelOrderByCustomer(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String customerOid = data.get("customerOid");
                Company company = userToken.getUser().getCompany();
                Customer customer = customerDAO.getById(customerOid, company);
                List<ModelOrder> modelOrders = modelOrderDAO.getByCustomer(customer, company);

                List<Model> models = new ArrayList<>();
                for (ModelOrder modelOrder : modelOrders) {
                    models.add(modelOrder.getModel());
                }
                return Response.ok(models).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    //returns model list only model which have order
    @POST
    @Path("readAssortsAndMaterials")
    @UnitOfWork
    public Response getAssortsAndMaterials(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(model, company);
                List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelOrder, company);

                ModelDTO modelDTO = new ModelDTO();
                modelDTO.setModelMaterials(modelMaterials);
                modelDTO.setModelAssorts(modelAssorts);

                return Response.ok(modelDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readAssortByUid")
    @UnitOfWork
    public Response readAssortByUid(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String uid = data.get("uid");

                Company company = userToken.getUser().getCompany();

                List<ModelAssort> modelAssorts = modelAssortDAO.getByUid(uid, company);

                return Response.ok(modelAssorts).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByCustomerAndDateRange")
    @UnitOfWork
    public Response getByCustomerAndDateRange(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String customerOid = data.get("customerOid");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                String modelCode = data.get("modelCode");
                Integer maxResult = Integer.parseInt(data.get("maxResult"));

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }
                Company company = userToken.getUser().getCompany();
                if (!modelCode.equals("")) {
                    List<Model> models = modelDAO.getByContainsInCode(modelCode, company);
                    return Response.ok(models).build();

                } else {
                    if (customerOid == "" || customerOid.equals("")) {
                        List<Model> models = modelDAO.getByDateRange(start, end, company, maxResult);

                        return Response.ok(models).build();
                    } else {
                        Customer customer = customerDAO.getById(customerOid, company);

                        List<Model> models = modelDAO.getByCustomerAndDateRange(customer, start, end, company, maxResult);
                        return Response.ok(models).build();
                    }
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByDateRange")
    @UnitOfWork
    public Response getByDateRange(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                List<Model> models = modelDAO.getByCreationDateTime(start, end, userToken.getUser().getCompany());

                List<ModelSummary> modelSummaries = new LinkedList<>();

                for (Model model : models) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());
                    modelSummary.setYear(String.valueOf(model.getYear()) == null ? "" : String.valueOf(model.getYear()));

                    modelSummaries.add(modelSummary);
                }

                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getSeasonAndYear")
    @UnitOfWork
    public Response getSeasonAndYear(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String season = data.get("season");
                String year = data.get("year");

                List<Model> models = modelDAO.getSeasonAndYear(season, Integer.valueOf(year), userToken.getUser().getCompany());

                List<ModelSummary> modelSummaries = new LinkedList<>();

                for (Model model : models) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());
                    modelSummary.setYear(String.valueOf(model.getYear()) == null ? "" : String.valueOf(model.getYear()));

                    modelSummaries.add(modelSummary);
                }

                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


    @POST
    @Path("getByCustomerAndDateRangeHasOrder")
    @UnitOfWork
    public Response getByCustomerAndDateRangeHasOrder(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String customerOid = data.get("customerOid");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                Integer maxResult = Integer.parseInt(data.get("maxResult"));
                String modelCode = data.get("modelCode");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                Company company = userToken.getUser().getCompany();

                if (!modelCode.equals("")) {

                    List<Model> models = modelDAO.getByContainsInCode(modelCode, company);

                    List<Model> modelList = new LinkedList<>();

                    for (Model model : models) {
                        ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                        if (modelOrder != null) {
                            modelList.add(model);
                        }
                    }
                    return Response.ok(modelList).build();

                } else {
                    if (customerOid == "" || customerOid.equals("")) {

                        List<Model> models = modelDAO.getByDateRange(start, end, company, maxResult);
                        List<Model> modelList = new LinkedList<>();

                        for (Model model : models) {
                            ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                            if (modelOrder != null) {
                                modelList.add(model);
                            }
                        }
                        return Response.ok(modelList).build();
                    } else {
                        Customer customer = customerDAO.getById(customerOid, company);
                        List<Model> models = modelDAO.getByCustomerAndDateRange(customer, start, end, company, maxResult);
                        List<Model> modelList = new LinkedList<>();

                        for (Model model : models) {
                            ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                            if (modelOrder != null) {
                                modelList.add(model);
                            }
                        }
                        return Response.ok(modelList).build();


                    }
                }


            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readModelSummary")
    @UnitOfWork
    public Response readModelSummary(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                List<Model> models = modelDAO.readAllModelsAsSupplyCompleted(userToken.getUser().getCompany());

                List<ModelSummary> modelSummaries = new LinkedList<>();

                for (Model model : models) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummary.setSeason(model.getSeason() == null ? "" : model.getSeason());

                    modelSummaries.add(modelSummary);
                }

                return Response.ok(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("copyToModel")
    @UnitOfWork
    public Response copyToModel(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_UPDATE)) {
                String masterModelOid = data.get("masterModelOid");
                String targetModels = data.get("targetModels");

                Company company = userToken.getUser().getCompany();

                Model masterModel = modelDAO.getByOid(masterModelOid, company);
                List<ModelMaterial> masterModelMaterials = modelMaterialDAO.getByModel(masterModel, company);
                List<ModelPriceDetail> modelPriceDetails = modelPriceDetailDAO.getByModel(masterModel, company);

                JSONArray jsonArray = new JSONArray(targetModels);

                for (int i = 0; i < jsonArray.length(); i++) {
                    Object o = jsonArray.get(i);

                    String oid = o.toString();

                    Model model = modelDAO.getByOid(oid, company);

                    if (!masterModelOid.equals(model.getOid())) {
                        List<ModelMaterial> oldModelMaterials = modelMaterialDAO.getByModel(model, company);
                        List<ModelPriceDetail> oldModelPriceDetail = modelPriceDetailDAO.getByModel(model, company);

                        //  Deleting old model material of target model
                        for (ModelMaterial modelMaterial : oldModelMaterials) {
                            modelMaterial.setDeleteStatus(true);
                            modelMaterialDAO.update(modelMaterial);
                        }

                        //  Deleting old model price detail of target model
                        for (ModelPriceDetail modelPriceDetail : oldModelPriceDetail) {
                            modelPriceDetail.setDeleteStatus(true);
                            modelPriceDetailDAO.update(modelPriceDetail);
                        }

                        for (ModelMaterial masterModelMaterial : masterModelMaterials) {
                            modelMaterialDAO.detach(masterModelMaterial);
                            masterModelMaterial.setOid(null);

                            ModelMaterial modelMaterial = masterModelMaterial;
                            modelMaterial.setModel(model);
                            modelMaterialDAO.create(masterModelMaterial);
                        }

                        for (ModelPriceDetail masterModelPriceDetail : modelPriceDetails) {
                            modelPriceDetailDAO.detach(masterModelPriceDetail);
                            masterModelPriceDetail.setOid(null);

                            ModelPriceDetail modelPriceDetail = masterModelPriceDetail;
                            modelPriceDetail.setModel(model);
                            modelPriceDetailDAO.create(modelPriceDetail);
                        }

                        model.setMoldCode(masterModel.getMoldCode());
                        model.setFootbedCode(masterModel.getFootbedCode());
                        model.setNumber(masterModel.getNumber());
                        model.setKnifeCode(masterModel.getKnifeCode());
                        model.setMostraKnifeCode(masterModel.getMostraKnifeCode());
                        model.setSalpaKnifeCode(masterModel.getSalpaKnifeCode());
                        model.setMoldSerial(masterModel.getMoldSerial());
                        model.setFootbedSerial(masterModel.getFootbedSerial());
                        model.setKnifeSerial(masterModel.getKnifeSerial());
                        model.setMostraKnifeSerial(masterModel.getMostraKnifeSerial());
                        model.setSalpaKnifeSerial(masterModel.getSalpaKnifeSerial());
                        model.setSupplyCompleted(false);

                        if (masterModel.getMaterialEntry() == null) {
                            model.setMaterialEntry(false);
                        }

                        if (masterModel.getMaterialEntry() != null && masterModel.getMaterialEntry().equals(true)) {
                            model.setMaterialEntry(true);
                        } else {
                            model.setMaterialEntry(false);
                        }

                        model.setCalculationType(masterModel.getCalculationType());

                        if (masterModel.getPricingEntry() == null) {
                            model.setPricingEntry(false);
                        }

                        if (masterModel.getPricingEntry() != null && masterModel.getPricingEntry().equals(true)) {
                            model.setPricingEntry(true);
                        } else {
                            model.setPricingEntry(false);
                        }
                        model.setModelPrice(masterModel.getModelPrice());
                        modelDAO.update(model);
                    }
                }

                return Response.ok(masterModel).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("supplyComplete")
    @UnitOfWork
    public Response supplyComplete(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_UPDATE)) {
                String oid = data.get("oid");
                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(oid, company);
                model.setSupplyCompleted(true);
                modelDAO.update(model);

                List<Model> modelList = modelDAO.getIsOrderAsSupplyCompleted(userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();
                for (Model model2 : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());
                    modelSummaries.add(modelSummary);
                }
                return Response.ok(modelList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("/uploadModelImageForMobile")
    @UnitOfWork
    public Response uploadModelImageForMobile(Map<String, String> data) {

        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                //  ŞİRKET BİLGİLERİ
                String osBasePath = "";
                String companyImageFolder = userToken.getUser().getCompany().getImageFolder();

                if (System.getProperty("os.name").equals("Windows 10")) {
                    osBasePath = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\modelimages\\" + companyImageFolder + "/";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    osBasePath = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/modelimages/" + companyImageFolder + "/";
                } else {
                    osBasePath = "/var/www/html/modelimages/" + companyImageFolder + "/";
                }

                String base64Image = data.get("image");

                BufferedImage inBuff = null;
                byte[] imageByte;
                try {
                    BASE64Decoder decoder = new BASE64Decoder();
                    imageByte = decoder.decodeBuffer(base64Image.substring(base64Image.indexOf(",") + 1));
                    ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
                    inBuff = ImageIO.read(bis);
                    bis.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                DateTime fileNameDT = new DateTime();

                String fileName = fileNameDT.getMillis() + Math.random() + ".jpg";

                String uploadedFileLocation = osBasePath + fileName;

                // save it with thumbnail
                try {
                    int w = inBuff.getWidth();
                    int h = inBuff.getHeight();
                    if (h > w) {
                        try {
                            Thumbnails.of(inBuff).rotate(90).size(500, 500).toFile(uploadedFileLocation);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Thumbnails.of(inBuff).size(500, 500).toFile(uploadedFileLocation);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                WorkOrderImageDTO workOrderImageDTO = new WorkOrderImageDTO();
                workOrderImageDTO.setPath(fileName);

                return Response.ok(workOrderImageDTO).build();
            }
        }

        return Response.ok().build();
    }

    @POST
    @Path("readModelSummaryForMobileSearch")
    @UnitOfWork
    public Response readModelSummaryForMobileSearch(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String searchString = data.get("ss");

                List<Model> modelsByCustomer = modelDAO.getByCustomer(searchString, userToken.getUser().getCompany());
                List<Model> modelsByModelCode = modelDAO.getByModelCodeLike(searchString, userToken.getUser().getCompany());
                List<Model> modelsByCustomerModelCode = modelDAO.getByCustomerModelCodeLike(searchString, userToken.getUser().getCompany());
                List<Model> modelsByColor = modelDAO.getByColor(searchString, userToken.getUser().getCompany());
                List<ModelSummary> modelSummaries = new LinkedList<>();

                if (modelsByCustomer.size() != 0) {
                    for (Model model : modelsByCustomer) {
                        ModelSummary modelSummary = new ModelSummary();
                        modelSummary.setOid(model.getOid());
                        modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                        modelSummary.setModelCode(model.getModelCode());
                        modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                        modelSummary.setColor(model.getColor());
                        modelSummary.setCustomer(model.getCustomer().getName());

                        modelSummaries.add(modelSummary);
                    }
                } else if (modelsByModelCode.size() != 0) {
                    for (Model model : modelsByModelCode) {
                        ModelSummary modelSummary = new ModelSummary();
                        modelSummary.setOid(model.getOid());
                        modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                        modelSummary.setModelCode(model.getModelCode());
                        modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                        modelSummary.setColor(model.getColor());
                        modelSummary.setCustomer(model.getCustomer().getName());

                        modelSummaries.add(modelSummary);
                    }
                } else if (modelsByCustomerModelCode.size() != 0) {
                    for (Model model : modelsByCustomerModelCode) {
                        ModelSummary modelSummary = new ModelSummary();
                        modelSummary.setOid(model.getOid());
                        modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                        modelSummary.setModelCode(model.getModelCode());
                        modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                        modelSummary.setColor(model.getColor());
                        modelSummary.setCustomer(model.getCustomer().getName());

                        modelSummaries.add(modelSummary);
                    }
                } else if (modelsByColor.size() != 0) {
                    for (Model model : modelsByColor) {
                        ModelSummary modelSummary = new ModelSummary();
                        modelSummary.setOid(model.getOid());
                        modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                        modelSummary.setModelCode(model.getModelCode());
                        modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                        modelSummary.setColor(model.getColor());
                        modelSummary.setCustomer(model.getCustomer().getName());

                        modelSummaries.add(modelSummary);
                    }
                }

                return Response.ok(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readRecipe")
    @UnitOfWork
    public Response readRecipe(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(modelOid, company);
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelOrder, company);
                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(model, company);
                List<ModelInternalCritical> modelInternalCriticals = modelInternalCriticalDAO.getByModel(model, company);
                List<ModelExternalCritical> modelExternalCriticals = modelExternalCriticalDAO.getByModel(model, company);

                ModelDTO modelDTO = new ModelDTO();
                modelDTO.setModelAssorts(modelAssorts);
                modelDTO.setModelOrder(modelOrder);
                modelDTO.setModelMaterials(modelMaterials);
                modelDTO.setModelInternalCriticals(modelInternalCriticals);
                modelDTO.setModelExternalCriticals(modelExternalCriticals);

                return Response.ok(modelDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
