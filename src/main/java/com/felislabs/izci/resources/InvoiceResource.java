package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
@Path("invoice")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class InvoiceResource {
    AuthUtil authUtil;
    BidDAO bidDAO;
    InvoiceDAO invoiceDAO;
    AccountingCustomerDAO accountingCustomerDAO;
    AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO;
    TaxDAO taxDAO;
    ProductDAO productDAO;
    InvoiceDetailDAO invoiceDetailDAO;

    public InvoiceResource(AuthUtil authUtil, BidDAO bidDAO, InvoiceDAO invoiceDAO, AccountingCustomerDAO accountingCustomerDAO, AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO, TaxDAO taxDAO, ProductDAO productDAO, InvoiceDetailDAO invoiceDetailDAO) {
        this.authUtil = authUtil;
        this.bidDAO = bidDAO;
        this.invoiceDAO = invoiceDAO;
        this.accountingCustomerDAO = accountingCustomerDAO;
        this.accountingCustomerRepresentativeDAO = accountingCustomerRepresentativeDAO;
        this.taxDAO = taxDAO;
        this.productDAO = productDAO;
        this.invoiceDetailDAO = invoiceDetailDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.INVOICE_CREATE)) {
                String customerOid = data.get("customerOid");
                String customerRepresentativeOid = data.get("customerRepresentativeOid");

                String billingDate = data.get("billingDate");
                String deliveryNote = data.get("deliveryNote");
                String paymentNote = data.get("paymentNote");
                String currencyNote = data.get("currencyNote");
                String currency = data.get("currency");
                String description = data.get("description");
                String details = data.get("details");
                String invoiceNumber = data.get("invoiceNumber");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime id = formatter.parseDateTime(billingDate);

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(customerRepresentativeOid, company);

                DateTime now = new DateTime();

                String documentNumber = accountingCustomer.getCode() + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + accountingCustomer.getBidNumber();

                accountingCustomer.setBidNumber(accountingCustomer.getBidNumber() + 1);
                accountingCustomerDAO.update(accountingCustomer);

                Invoice invoice = new Invoice();
                invoice.setCompany(company);
                invoice.setUser(user);
                invoice.setUpdateDate(new DateTime());
                invoice.setAccountingCustomer(accountingCustomer);
                invoice.setAccountingCustomerRepresentative(accountingCustomerRepresentative);
                invoice.setInvoiceDate(id);
                invoice.setDeliveryNote(deliveryNote);
                invoice.setPaymentNote(paymentNote);
                invoice.setCurrencyNote(currencyNote);
                invoice.setCurrency(currency);
                invoice.setDescription(description);
                invoice.setDocumentNumber(documentNumber);
                invoice.setInvoiceNumber(invoiceNumber);

                invoiceDAO.create(invoice);

                Double subTotal = 0.0;
                Double discountIncluded = 0.0;
                Double taxIncluded = 0.0;

                JSONArray jsonArray = new JSONArray(details);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String code = jsonObject.getString("code");
                    Double quantity = Double.valueOf(jsonObject.getString("quantity"));
                    Double price = Double.valueOf(jsonObject.getString("price"));
                    Double discount = Double.valueOf(jsonObject.getString("discount"));
                    String deadline = jsonObject.getString("deadline");
                    String detailTaxOid = jsonObject.getString("tax");

                    Double lineTotal = quantity * price;
                    Double discountIncludedPrice = lineTotal - (lineTotal * (discount / 100));

                    Product product = productDAO.getByCode(code, company);
                    Tax detailTax = taxDAO.getByOid(detailTaxOid, company);

                    Double taxIncludedPrice = discountIncludedPrice + (discountIncludedPrice * (detailTax.getRatio() / 100));

                    InvoiceDetail invoiceDetail = new InvoiceDetail();
                    invoiceDetail.setUser(user);
                    invoiceDetail.setUpdateDate(new DateTime());
                    invoiceDetail.setQuantity(quantity);
                    invoiceDetail.setPrice(price);
                    invoiceDetail.setDiscount(discount);
                    invoiceDetail.setDeadline(deadline);
                    invoiceDetail.setLineTotal(lineTotal);
                    invoiceDetail.setProduct(product);
                    invoiceDetail.setDiscountIncluded(discountIncludedPrice);
                    invoiceDetail.setTaxIncluded(taxIncludedPrice);
                    invoiceDetail.setInvoice(invoice);
                    invoiceDetail.setTax(detailTax);

                    invoiceDetailDAO.create(invoiceDetail);

                    subTotal += lineTotal;
                    discountIncluded += discountIncludedPrice;
                    taxIncluded += taxIncludedPrice;
                }

                invoice.setSubTotal(subTotal);
                invoice.setTaxIncluded(taxIncluded);
                invoice.setDiscountIncluded(discountIncluded);

                invoiceDAO.update(invoice);

                return Response.ok().entity(invoice).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.INVOICE_READ)) {
                List<Invoice> invoices = invoiceDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(invoices).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.INVOICE_READ)) {
                String oid = data.get("oid");

                Invoice invoice = invoiceDAO.getByOid(oid, userToken.getUser().getCompany());

                List<InvoiceDetail> invoiceDetails = invoiceDetailDAO.getByInvoice(invoice);

                return Response.ok().entity(invoiceDetails).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.INVOICE_UPDATE)) {
                String oid = data.get("oid");
                String customerOid = data.get("customerOid");
                String customerRepresentativeOid = data.get("customerRepresentativeOid");

                String invoiceDate = data.get("invoiceDate");
                String deliveryNote = data.get("deliveryNote");
                String paymentNote = data.get("paymentNote");
                String currencyNote = data.get("currencyNote");
                String currency = data.get("currency");
                String description = data.get("description");
                String invoiceNumber = data.get("invoiceNumber");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime id = formatter.parseDateTime(invoiceDate);

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(customerRepresentativeOid, company);

                DateTime now = new DateTime();

                String documentNumber = accountingCustomer.getCode() + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + accountingCustomer.getBidNumber();

                accountingCustomer.setBidNumber(accountingCustomer.getBidNumber() + 1);
                accountingCustomerDAO.update(accountingCustomer);

                Invoice invoice = invoiceDAO.getByOid(oid, company);
                invoice.setCompany(company);
                invoice.setUser(user);
                invoice.setUpdateDate(new DateTime());
                invoice.setAccountingCustomer(accountingCustomer);
                invoice.setAccountingCustomerRepresentative(accountingCustomerRepresentative);
                invoice.setInvoiceDate(id);
                invoice.setDeliveryNote(deliveryNote);
                invoice.setPaymentNote(paymentNote);
                invoice.setCurrencyNote(currencyNote);
                invoice.setCurrency(currency);
                invoice.setDescription(description);
                invoice.setDocumentNumber(documentNumber);
                invoice.setInvoiceNumber(invoiceNumber);

                invoiceDAO.update(invoice);

                return Response.ok().entity(invoice).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("detail/update")
    @UnitOfWork
    public Response detailUpdate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BID_CREATE)) {
                String oid = data.get("oid");
                String details = data.get("details");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Invoice invoice = invoiceDAO.getByOid(oid, company);

                // Delete invoice's details if exist
                List<InvoiceDetail> invoiceDetails = invoiceDetailDAO.getByInvoice(invoice);
                if (invoiceDetails.size() != 0) {
                    for (InvoiceDetail invoiceDetail : invoiceDetails) {
                        invoiceDetailDAO.delete(invoiceDetail);
                    }
                }

                Double subTotal = 0.0;
                Double discountIncluded = 0.0;
                Double taxIncluded = 0.0;

                JSONArray jsonArray = new JSONArray(details);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String code = jsonObject.getString("code");
                    Double quantity = Double.valueOf(jsonObject.getString("quantity"));
                    Double price = Double.valueOf(jsonObject.getString("price"));
                    Double discount = Double.valueOf(jsonObject.getString("discount"));
                    String deadline = jsonObject.getString("deadline");
                    String detailTaxOid = jsonObject.getString("tax");

                    Double lineTotal = quantity * price;
                    Double discountIncludedPrice = lineTotal - (lineTotal * (discount / 100));

                    Product product = productDAO.getByCode(code, company);
                    Tax detailTax = taxDAO.getByOid(detailTaxOid, company);

                    Double taxIncludedPrice = discountIncludedPrice + (discountIncludedPrice * (detailTax.getRatio() / 100));

                    InvoiceDetail invoiceDetail = new InvoiceDetail();
                    invoiceDetail.setUser(user);
                    invoiceDetail.setUpdateDate(new DateTime());
                    invoiceDetail.setQuantity(quantity);
                    invoiceDetail.setPrice(price);
                    invoiceDetail.setDiscount(discount);
                    invoiceDetail.setDeadline(deadline);
                    invoiceDetail.setLineTotal(lineTotal);
                    invoiceDetail.setProduct(product);
                    invoiceDetail.setDiscountIncluded(discountIncludedPrice);
                    invoiceDetail.setTaxIncluded(taxIncludedPrice);
                    invoiceDetail.setInvoice(invoice);
                    invoiceDetail.setTax(detailTax);

                    invoiceDetailDAO.create(invoiceDetail);

                    subTotal += lineTotal;
                    discountIncluded += discountIncludedPrice;
                    taxIncluded += taxIncludedPrice;
                }

                invoice.setSubTotal(subTotal);
                invoice.setTaxIncluded(taxIncluded);
                invoice.setDiscountIncluded(discountIncluded);

                invoiceDAO.update(invoice);

                return Response.ok().entity(invoice).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.INVOICE_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Invoice invoice = invoiceDAO.getByOid(oid, company);
                invoice.setDeleteStatus(true);
                invoiceDAO.update(invoice);

                return Response.ok().entity(invoice).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
