package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.FairModelAssortDAO;
import com.felislabs.izci.dao.dao.FairModelPriceDAO;
import com.felislabs.izci.dao.dao.ModelDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 18/03/2017.
 */
@Path("fair")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FairResource {
    AuthUtil authUtil;
    FairModelAssortDAO fairModelAssortDAO;
    FairModelPriceDAO fairModelPriceDAO;
    ModelDAO modelDAO;

    public FairResource(AuthUtil authUtil, FairModelAssortDAO fairModelAssortDAO, FairModelPriceDAO fairModelPriceDAO, ModelDAO modelDAO) {
        this.authUtil = authUtil;
        this.fairModelAssortDAO = fairModelAssortDAO;
        this.fairModelPriceDAO = fairModelPriceDAO;
        this.modelDAO = modelDAO;
    }

    @POST
    @Path("assort/update")
    @UnitOfWork
    public Response assortCreate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_MODEL_ASSORT_CREATE)) {
                String modelOid = data.get("modelOid");
                String assorts = data.get("assorts");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                Model model = modelDAO.getByOid(modelOid, company);

//                Delete all assorts of fair model
                List<FairModelAssort> fairModelAssorts = fairModelAssortDAO.getByModel(model, company);

                if (fairModelAssorts.size() != 0) {
                    for (FairModelAssort fairModelAssort : fairModelAssorts) {
                        fairModelAssortDAO.delete(fairModelAssort);
                    }
                }

                JSONArray jsonArray = new JSONArray(assorts);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String uid = jsonObject.getString("uid");
                    String number = jsonObject.getString("number");
                    String assort = jsonObject.getString("assort");

                    FairModelAssort fairModelAssort = new FairModelAssort();
                    fairModelAssort.setCompany(company);
                    fairModelAssort.setUpdater(user);
                    fairModelAssort.setUpdateDate(new DateTime());
                    fairModelAssort.setUid(uid);
                    fairModelAssort.setNumber(Integer.valueOf(number));
                    fairModelAssort.setAssort(Integer.valueOf(assort));
                    fairModelAssort.setModel(model);

                    fairModelAssortDAO.create(fairModelAssort);
                }

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("assort/readByModel")
    @UnitOfWork
    public Response readAssortByModel(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_MODEL_ASSORT_READ)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);

                List<FairModelAssort> fairModelAssorts = fairModelAssortDAO.getByModel(model, company);

                return Response.ok().entity(fairModelAssorts).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("price/update")
    @UnitOfWork
    public Response priceCreate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_MODEL_PRICE_CREATE)) {
                String modelOid = data.get("modelOid");
                String prices = data.get("prices");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();
                Model model = modelDAO.getByOid(modelOid, company);

//                Delete all price of fair model
                List<FairModelPrice> fairModelPrices = fairModelPriceDAO.getByModel(model, company);

                if (fairModelPrices.size() != 0) {
                    for (FairModelPrice fairModelPrice : fairModelPrices) {
                        fairModelPriceDAO.delete(fairModelPrice);
                    }
                }

                JSONArray jsonArray = new JSONArray(prices);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String name = jsonObject.getString("description");
                    String amount = jsonObject.getString("amount");

                    FairModelPrice fairModelPrice = new FairModelPrice();
                    fairModelPrice.setCompany(company);
                    fairModelPrice.setUpdater(user);
                    fairModelPrice.setUpdateDate(new DateTime());
                    fairModelPrice.setName(name);
                    fairModelPrice.setAmount(Double.valueOf(amount));
                    fairModelPrice.setModel(model);

                    fairModelPriceDAO.create(fairModelPrice);
                }

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("price/readByModel")
    @UnitOfWork
    public Response readPriceByModel(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.FAIR_MODEL_PRICE_READ)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);

                List<FairModelPrice> fairModelPrices = fairModelPriceDAO.getByModel(model, company);

                return Response.ok().entity(fairModelPrices).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("assort/copy")
    @UnitOfWork
    public Response copyAssort(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COPY_ASSORT)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);

                List<FairModelAssort> fairModelAssorts = fairModelAssortDAO.getByModel(model, company);

                List<Model> models = modelDAO.getBySystemCode(model.getModelCode(), company);

                for(Model model1 : models) {
                    if(model.getOid() != model1.getOid()) {

                        List<FairModelAssort> oldFairModelAssorts = fairModelAssortDAO.getByModel(model1, company);
                        for (FairModelAssort oldFairModelAssort : oldFairModelAssorts) {
                            fairModelAssortDAO.delete(oldFairModelAssort);
                        }

                        for (FairModelAssort fairModelAssort : fairModelAssorts) {
                            FairModelAssort newFairModelAssort = new FairModelAssort();
                            newFairModelAssort.setCompany(fairModelAssort.getCompany());
                            newFairModelAssort.setUpdater(fairModelAssort.getUpdater());
                            newFairModelAssort.setUpdateDate(new DateTime());
                            newFairModelAssort.setUid(fairModelAssort.getUid());
                            newFairModelAssort.setNumber(Integer.valueOf(fairModelAssort.getNumber()));
                            newFairModelAssort.setAssort(Integer.valueOf(fairModelAssort.getAssort()));
                            newFairModelAssort.setModel(model1);

                            fairModelAssortDAO.create(newFairModelAssort);
                        }
                    }
                }

                return Response.ok().entity(models).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("price/copy")
    @UnitOfWork
    public Response copyPrice(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COPY_PRICE)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);

                List<FairModelPrice> fairModelPrices = fairModelPriceDAO.getByModel(model, company);

                List<Model> models = modelDAO.getBySystemCode(model.getModelCode(), company);

                for(Model model1 : models) {
                    if(model.getOid() != model1.getOid()) {

                        List<FairModelPrice> oldFairModelPrices = fairModelPriceDAO.getByModel(model1, company);
                        for (FairModelPrice fairModelPrice : oldFairModelPrices) {
                            fairModelPriceDAO.delete(fairModelPrice);
                        }

                        for (FairModelPrice fairModelPrice : fairModelPrices) {
                            FairModelPrice fairModelPrice1 = new FairModelPrice();
                            fairModelPrice1.setCompany(fairModelPrice.getCompany());
                            fairModelPrice1.setUpdater(fairModelPrice.getUpdater());
                            fairModelPrice1.setUpdateDate(new DateTime());
                            fairModelPrice1.setName(fairModelPrice.getName());
                            fairModelPrice1.setAmount(Double.valueOf(fairModelPrice.getAmount()));
                            fairModelPrice1.setModel(model1);

                            fairModelPriceDAO.create(fairModelPrice1);
                        }
                    }
                }

                return Response.ok().entity(models).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readFairPrice")
    @UnitOfWork
    public Response readFairPrice(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COPY_PRICE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());

                List<FairModelPrice> fairModelPrices = fairModelPriceDAO.getByModel(model, userToken.getUser().getCompany());

                return Response.ok().entity(fairModelPrices).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
