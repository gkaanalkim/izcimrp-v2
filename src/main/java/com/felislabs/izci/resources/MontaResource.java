package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.*;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 02/02/2017.
 */
@Path("monta")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MontaResource {
    AuthUtil authUtil;
    ModelOrderDAO modelOrderDAO;
    ModelMontaDAO modelMontaDAO;
    ProductionLineDAO productionLineDAO;
    MrpSettingDAO mrpSettingDAO;
    ModelDAO modelDAO;
    ModelAssortDAO modelAssortDAO;
    ModelAlarmDAO modelAlarmDAO;
    ModelInternalCriticalDAO modelInternalCriticalDAO;
    ModelExternalCriticalDAO modelExternalCriticalDAO;
    ModelMontaIncomingDAO modelMontaIncomingDAO;
    StockDAO stockDAO;
    StockDetailDAO stockDetailDAO;
    ModelMaterialDAO modelMaterialDAO;
    StockTypeDAO stockTypeDAO;
    SupplierDAO supplierDAO;
    MontaRequirementDAO montaRequirementDAO;
    ManufacturingDefectDAO manufacturingDefectDAO;

    public MontaResource(AuthUtil authUtil, ModelOrderDAO modelOrderDAO, ModelMontaDAO modelMontaDAO, ProductionLineDAO productionLineDAO, MrpSettingDAO mrpSettingDAO, ModelDAO modelDAO, ModelAssortDAO modelAssortDAO, ModelAlarmDAO modelAlarmDAO, ModelInternalCriticalDAO modelInternalCriticalDAO, ModelExternalCriticalDAO modelExternalCriticalDAO, ModelMontaIncomingDAO modelMontaIncomingDAO, StockDAO stockDAO, StockDetailDAO stockDetailDAO, ModelMaterialDAO modelMaterialDAO, StockTypeDAO stockTypeDAO, SupplierDAO supplierDAO, MontaRequirementDAO montaRequirementDAO, ManufacturingDefectDAO manufacturingDefectDAO) {
        this.authUtil = authUtil;
        this.modelOrderDAO = modelOrderDAO;
        this.modelMontaDAO = modelMontaDAO;
        this.productionLineDAO = productionLineDAO;
        this.mrpSettingDAO = mrpSettingDAO;
        this.modelDAO = modelDAO;
        this.modelAssortDAO = modelAssortDAO;
        this.modelAlarmDAO = modelAlarmDAO;
        this.modelInternalCriticalDAO = modelInternalCriticalDAO;
        this.modelExternalCriticalDAO = modelExternalCriticalDAO;
        this.modelMontaIncomingDAO = modelMontaIncomingDAO;
        this.stockDAO = stockDAO;
        this.stockDetailDAO = stockDetailDAO;
        this.modelMaterialDAO = modelMaterialDAO;
        this.stockTypeDAO = stockTypeDAO;
        this.supplierDAO = supplierDAO;
        this.montaRequirementDAO = montaRequirementDAO;
        this.manufacturingDefectDAO = manufacturingDefectDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String modelOid = data.get("modelOid");
                String formalStartDate = data.get("formalStartDate");
                String formalEndDate = data.get("formalEndDate");
                String montaType = data.get("montaType");
                String productionLineCode = data.get("productionLineCode");
                String montaQuantity = data.get("montaQuantity");
                String selectedAssort = data.get("selectedAssort");
                String shoesLace = data.get("shoesLace");
                String pictogram = data.get("pictogram");
                String article = data.get("article");
                String description = data.get("description");
                String requirements = data.get("requirements");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                ProductionLine productionLine = productionLineDAO.findByCode(productionLineCode, company);

                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);

                Model model = modelDAO.getByOid(modelOid, company);
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);
                DateTime now = new DateTime();

                String documentNumber = model.getModelCode() + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "/" + mrpSetting.getMontaOrderCounter();

                mrpSetting.setMontaOrderCounter(mrpSetting.getMontaOrderCounter() + 1);
                mrpSettingDAO.update(mrpSetting);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime fsd = formatter.parseDateTime(formalStartDate);
                DateTime fed = formatter.parseDateTime(formalEndDate);
                if (fed.isBefore(fsd)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                ModelMonta modelMonta = new ModelMonta();
                modelMonta.setCompany(company);
                modelMonta.setUpdater(user);
                modelMonta.setUpdateDate(new DateTime());
                modelMonta.setFormalStart(fsd);
                modelMonta.setFormalEnd(fed);
                modelMonta.setType(montaType);
                modelMonta.setProductionLine(productionLine);
                modelMonta.setQuantity(Integer.valueOf(montaQuantity));
                modelMonta.setModelOrder(modelOrder);
                modelMonta.setDocumentNumber(documentNumber);
                modelMonta.setSelectedAssortUid(selectedAssort);
                modelMonta.setShoesLace(shoesLace);
                modelMonta.setPictogram(pictogram);
                modelMonta.setArticle(article);
                modelMonta.setDescription(description);

                modelMontaDAO.create(modelMonta);


                JSONArray requirementArray = new JSONArray(requirements);
//
                for (int i = 0; i < requirementArray.length(); i++) {
                    JSONObject jsonObject = requirementArray.getJSONObject(i);

                    String number = jsonObject.getString("number");

                    if (number.equals("")) {
                        String stockOid = jsonObject.getString("stockOid");

                        Stock stock = stockDAO.getById(stockOid, company);
                        MontaRequirement montaRequirement = new MontaRequirement();
                        montaRequirement.setModelMonta(modelMonta);
                        montaRequirement.setStock(stock);
                        montaRequirement.setStockDetail(null);
                        montaRequirement.setQuantity(Double.valueOf(jsonObject.getString("req")));
                        montaRequirement.setUpdater(user);
                        montaRequirement.setCompany(company);

                        montaRequirementDAO.create(montaRequirement);

                    } else {

                        String stockDetailOid = jsonObject.getString("stockOid");

                        StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);

                        MontaRequirement montaRequirement = new MontaRequirement();
                        montaRequirement.setModelMonta(modelMonta);
                        montaRequirement.setStock(stockDetail.getStock());
                        montaRequirement.setStockDetail(stockDetail);
                        montaRequirement.setQuantity(Double.valueOf(jsonObject.getString("req")));
                        montaRequirement.setUpdater(user);
                        montaRequirement.setCompany(company);

                        montaRequirementDAO.create(montaRequirement);

                    }
                }


                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelMonta.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelMonta.getQuantity() + " çifti montaya verildi.", new DateTime());

                return Response.ok().entity(modelMonta).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByModel")
    @UnitOfWork
    public Response readByModel(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String modelOid = data.get("oid");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Model model = modelDAO.getByOid(modelOid, company);
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                List<ModelMonta> modelMontas = modelMontaDAO.getByModelOrder(modelOrder, company);

                return Response.ok().entity(modelMontas).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readAll")
    @UnitOfWork
    public Response readAll(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_READ)) {
                List<ModelMonta> modelMontas = modelMontaDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(modelMontas).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByType")
    @UnitOfWork
    public Response readByType(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_READ)) {
                List<ModelMonta> modelMontas = modelMontaDAO.getByType(data.get("type"), userToken.getUser().getCompany());

                return Response.ok().entity(modelMontas).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetailByModel(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String montaOid = data.get("oid");

                ModelMonta modelMonta = modelMontaDAO.getByOid(montaOid, userToken.getUser().getCompany());

                return Response.ok().entity(modelMonta).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readMontaAndAssort")
    @UnitOfWork
    public Response readMontaAndAssort(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String montaOid = data.get("oid");

                ModelMonta modelMonta = modelMontaDAO.getByOid(montaOid, userToken.getUser().getCompany());
                List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelMonta.getModelOrder(), userToken.getUser().getCompany());
                List<ModelAlarm> modelAlarms = modelAlarmDAO.getByModelOrder(modelMonta.getModelOrder(), userToken.getUser().getCompany());
                List<ModelInternalCritical> modelInternalCriticals = modelInternalCriticalDAO.getByModel(modelMonta.getModelOrder().getModel(), userToken.getUser().getCompany());
                List<ModelExternalCritical> modelExternalCriticals = modelExternalCriticalDAO.getByModel(modelMonta.getModelOrder().getModel(), userToken.getUser().getCompany());

                MontaDTO montaDTO = new MontaDTO(modelMonta, modelAssorts, modelAlarms, modelInternalCriticals, modelExternalCriticals);

                return Response.ok().entity(montaDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByProductionLineAndType")
    @UnitOfWork
    public Response readByProductionLine(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String productionLineCode = data.get("productionLineCode");
                String type = data.get("type");

                ProductionLine productionLine = productionLineDAO.findByCode(productionLineCode, userToken.getUser().getCompany());

                List<ModelMonta> modelMontas = modelMontaDAO.getByProductionLineAndCode(productionLine, type, userToken.getUser().getCompany());

                List<EventDTO> eventDTOs = new LinkedList<>();

                for (ModelMonta modelMonta : modelMontas) {
                    EventDTO eventDTO = new EventDTO(modelMonta.getOid(), modelMonta.getFormalStart(), modelMonta.getFormalEnd(), modelMonta.getModelOrder().getModel().getModelCode());
                    eventDTOs.add(eventDTO);
                }

                return Response.ok().entity(eventDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                ModelMonta modelMonta = modelMontaDAO.getByOid(oid, company);
                List<ModelMontaIncoming> modelMontaIncomings = modelMontaIncomingDAO.getByModelMonta(modelMonta, company);
                if (modelMontaIncomings.size() == 0) {
                    modelMonta.setDeleteStatus(true);
                    modelMontaDAO.update(modelMonta);
                    List<MontaRequirement> montaRequirements = montaRequirementDAO.getByModelMonta(modelMonta, company);
                    for (int i = 0; i < montaRequirements.size(); i++) {
                        MontaRequirement montaRequirement = montaRequirements.get(i);
                        montaRequirement.setDeleteStatus(true);
                        montaRequirementDAO.update(montaRequirement);
                    }

                } else {
                    return Response.serverError().entity(ErrorMessage.TR_ALREADY_ACCEPTED.getErrorAlias()).build();
                }

                List<ModelMonta> modelMontas = modelMontaDAO.getByModelOrder(modelMonta.getModelOrder(), company);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelMonta.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelMonta.getQuantity() + " çiftlik monta emri silindi.", new DateTime());

                return Response.ok().entity(modelMontas).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("createIncoming")
    @UnitOfWork
    public Response createIncoming(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String montaDocumentNumber = data.get("montaDocumentNumber");
                String quantity = data.get("quantity");
                String description = data.get("description");
                String extraAttribute = data.get("extraAttribute");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                ModelMonta modelMonta = modelMontaDAO.getByDocumentNumber(montaDocumentNumber, company);

                ModelMontaIncoming modelMontaIncoming = new ModelMontaIncoming();
                modelMontaIncoming.setCompany(company);
                modelMontaIncoming.setUpdateDate(new DateTime());
                modelMontaIncoming.setUpdater(user);
                modelMontaIncoming.setModelMonta(modelMonta);
                modelMontaIncoming.setModelOrder(modelMonta.getModelOrder());
                modelMontaIncoming.setQuantity(Integer.valueOf(quantity));
                modelMontaIncoming.setDescription(description);
                modelMontaIncoming.setExtraAttribute(extraAttribute);

                modelMontaIncomingDAO.create(modelMontaIncoming);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelMontaIncoming.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelMontaIncoming.getQuantity() + " çiftlik gelen monta bilgisi oluşturuldu.", new DateTime());

                return Response.ok().entity(modelMontaIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingByModel")
    @UnitOfWork
    public Response readIncomingByModel(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String modelOid = data.get("modelOid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                List<ModelMontaIncoming> modelMontaIncomings = modelMontaIncomingDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());

                return Response.ok().entity(modelMontaIncomings).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingByCount")
    @UnitOfWork
    public Response readAllIncoming(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                Integer count = Integer.valueOf(data.get("count"));

                List<ModelMontaIncoming> modelMontaIncomings = modelMontaIncomingDAO.getByCount(count, userToken.getUser().getCompany());

                return Response.ok().entity(modelMontaIncomings).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingDetail")
    @UnitOfWork
    public Response readIncomingDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String incomingOid = data.get("oid");

                ModelMontaIncoming modelMontaIncoming = modelMontaIncomingDAO.getByOid(incomingOid, userToken.getUser().getCompany());

                return Response.ok().entity(modelMontaIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteIncoming")
    @UnitOfWork
    public Response deleteIncoming(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String oid = data.get("oid");

                ModelMontaIncoming modelMontaIncoming = modelMontaIncomingDAO.getByOid(oid, userToken.getUser().getCompany());

                modelMontaIncomingDAO.delete(modelMontaIncoming);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelMontaIncoming.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelMontaIncoming.getQuantity() + " çiftlik gelen monta bilgisi silindi.", new DateTime());

                return Response.ok().entity(modelMontaIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readParcelAndBoxMaterial")
    @UnitOfWork
    public Response readParcelAndBoxStock(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_READ)) {
                String modelMontaOid = data.get("selectedModelMonta");

                ModelMonta modelMonta = modelMontaDAO.getByOid(modelMontaOid, userToken.getUser().getCompany());

                Model model = modelMonta.getModelOrder().getModel();

                ModelMaterial boxMaterial = modelMaterialDAO.getByPlaceOfUse("KUTU", model, userToken.getUser().getCompany());
                ModelMaterial parcelMaterial = modelMaterialDAO.getByPlaceOfUse("KOLİ", model, userToken.getUser().getCompany());
                ModelMaterial baseMaterial = modelMaterialDAO.getByPlaceOfUse("TABAN", model, userToken.getUser().getCompany());

                BoxAndParcelDTO boxAndParcelDTO = new BoxAndParcelDTO();
                boxAndParcelDTO.setBoxCode(boxMaterial.getStock() != null ? boxMaterial.getStock().getName() : "");
                boxAndParcelDTO.setBaseCode(baseMaterial.getStock() != null ? baseMaterial.getStock().getCode() : "");
                boxAndParcelDTO.setParcelCode(parcelMaterial.getStock() != null ? parcelMaterial.getStock().getName() : "");

                return Response.ok(boxAndParcelDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSummary")
    @UnitOfWork
    public Response readSummary(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_READ)) {
                List<ModelMonta> modelMontas = modelMontaDAO.getAll(userToken.getUser().getCompany());
                List<MontaOrderSummary> montaOrderSummaries = new LinkedList<>();

                for (ModelMonta modelMonta : modelMontas) {
                    List<ModelMontaIncoming> modelMontaIncomings = modelMontaIncomingDAO.getByModelMonta(modelMonta, userToken.getUser().getCompany());

                    /*
                    * 1. Tüm gelen monta kabul miktarları toplanıyor
                    * 2. Modelin montaya gönderilen miktarı, monta kabullerinin toplamından büyük veya eşit ise terminale gönderilmiyor
                    * */

                    Integer montaIncomingTotal = 0;

                    for (ModelMontaIncoming modelMontaIncoming : modelMontaIncomings) {
                        if (modelMontaIncoming.getQuantity() != null) {
                            montaIncomingTotal += modelMontaIncoming.getQuantity();
                        }
                    }

                    if (modelMonta.getQuantity() > montaIncomingTotal) {
                        MontaOrderSummary montaSummary = new MontaOrderSummary();
                        montaSummary.setMontaOid(modelMonta.getOid());
                        montaSummary.setModelOid(modelMonta.getModelOrder().getModel().getOid());
                        montaSummary.setModelCode(modelMonta.getModelOrder().getModel().getModelCode());
                        montaSummary.setCustomer(modelMonta.getModelOrder().getModel().getCustomer().getName());
                        montaSummary.setCustomerModelCode(modelMonta.getModelOrder().getModel().getCustomerModelCode());
                        montaSummary.setCreationDatetime(modelMonta.getCreationDateTime());
                        montaSummary.setStartDate(modelMonta.getFormalStart());
                        montaSummary.setEndDate(modelMonta.getFormalEnd());
                        montaSummary.setDefaultPhoto(modelMonta.getModelOrder().getModel().getDefaultPhoto());
                        montaSummary.setModelColor(modelMonta.getModelOrder().getModel().getColor());
                        montaSummary.setMontaDocumentNumber(modelMonta.getDocumentNumber());
                        montaSummary.setMontaOrderQuantity(modelMonta.getQuantity());
                        montaSummary.setOrderQuantity(modelMonta.getModelOrder().getOrderQuantity());
                        montaSummary.setProductionLine(modelMonta.getProductionLine().getName());
                        montaSummary.setMontaIncomingQuantity(montaIncomingTotal);
                        montaSummary.setSelectedAssortUid(modelMonta.getSelectedAssortUid());
                        montaSummary.setMontaDescription(modelMonta.getDescription());
                        montaSummary.setPictogram(modelMonta.getPictogram());
                        montaSummary.setSeason(modelMonta.getModelOrder().getModel().getSeason());
                        montaSummary.setYear(modelMonta.getModelOrder().getModel().getYear());

                        montaOrderSummaries.add(montaSummary);
                    }
                }

                return Response.ok().entity(montaOrderSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingSummary")
    @UnitOfWork
    public Response readIncomingSummary(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_READ)) {
                List<ModelMontaIncoming> modelMontaIncomings = modelMontaIncomingDAO.getAll(userToken.getUser().getCompany());
                List<MontaIncomingSummary> montaIncomingSummaries = new LinkedList<>();

                for (ModelMontaIncoming modelMontaIncoming : modelMontaIncomings) {
                    MontaIncomingSummary montaIncomingSummary = new MontaIncomingSummary();
                    montaIncomingSummary.setModelCode(modelMontaIncoming.getModelOrder().getModel().getModelCode());
                    montaIncomingSummary.setCustomerModelCode(modelMontaIncoming.getModelOrder().getModel().getCustomerModelCode());
                    montaIncomingSummary.setCreationDatetime(modelMontaIncoming.getCreationDateTime());
                    montaIncomingSummary.setDefaultPhoto(modelMontaIncoming.getModelOrder().getModel().getDefaultPhoto());
                    montaIncomingSummary.setModelColor(modelMontaIncoming.getModelOrder().getModel().getColor());
                    montaIncomingSummary.setMontaDocumentNumber(modelMontaIncoming.getModelMonta().getDocumentNumber());
                    montaIncomingSummary.setMontaIncomingQuantity(modelMontaIncoming.getQuantity());
                    montaIncomingSummary.setOrderQuantity(modelMontaIncoming.getModelOrder().getOrderQuantity());
                    montaIncomingSummary.setCustomer(modelMontaIncoming.getModelOrder().getModel().getCustomer().getName());
                    montaIncomingSummary.setProductionLine(modelMontaIncoming.getModelMonta().getProductionLine().getName());
                    montaIncomingSummary.setMontaOrderQuantity(modelMontaIncoming.getModelMonta().getQuantity());
                    montaIncomingSummary.setMontaIncomingOid(modelMontaIncoming.getOid());
                    montaIncomingSummary.setExtraAttribute(modelMontaIncoming.getExtraAttribute() == null ? "" : modelMontaIncoming.getExtraAttribute());
                    montaIncomingSummary.setSeason(modelMontaIncoming.getModelOrder().getModel().getSeason());
                    montaIncomingSummary.setYear(modelMontaIncoming.getModelOrder().getModel().getYear());

                    montaIncomingSummaries.add(montaIncomingSummary);
                }

                return Response.ok().entity(montaIncomingSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("createManufacturingDefect")
    @UnitOfWork
    public Response createManufacturingDefect(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String modelMontaOid = data.get("modelMontaOid");
                String details = data.get("details");
                String sayaDefect = data.get("sayaDefect");
                String montaDefect = data.get("montaDefect");

                Company company = userToken.getUser().getCompany();
                ModelMonta modelMonta = modelMontaDAO.getByOid(modelMontaOid, company);

                JSONArray jsonArray = new JSONArray(details);

                Integer montaQuantity = 0;

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String number = jsonObject.getString("number");
                    String quantity = jsonObject.getString("quantity");

                    ManufacturingDefect manufacturingDefect = new ManufacturingDefect();
                    manufacturingDefect.setCompany(company);
                    manufacturingDefect.setUpdater(userToken.getUser());
                    manufacturingDefect.setUpdateDate(new DateTime());
                    manufacturingDefect.setModelMonta(modelMonta);
                    manufacturingDefect.setNumber(number);
                    manufacturingDefect.setQuantity(quantity);
                    manufacturingDefect.setSayaDefect(sayaDefect);
                    manufacturingDefect.setMontaDefect(montaDefect);

                    manufacturingDefectDAO.create(manufacturingDefect);

                    montaQuantity += Integer.parseInt(quantity);
                }

                ModelMontaIncoming modelMontaIncoming = new ModelMontaIncoming();
                modelMontaIncoming.setCompany(company);
                modelMontaIncoming.setUpdateDate(new DateTime());
                modelMontaIncoming.setUpdater(userToken.getUser());
                modelMontaIncoming.setModelMonta(modelMonta);
                modelMontaIncoming.setModelOrder(modelMonta.getModelOrder());
                modelMontaIncoming.setQuantity(Integer.valueOf(montaQuantity));
                modelMontaIncoming.setDescription("Hatalı üretim miktarı");

                modelMontaIncomingDAO.create(modelMontaIncoming);

                return Response.ok().entity(modelMonta).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readManufacturingDefectSummary")
    @UnitOfWork
    public Response readManufacturingDefectSummary(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                List<ManufacturingDefect> manufacturingDefects = manufacturingDefectDAO.getAll(userToken.getUser().getCompany());
                List<ManufacturingDefectSummaryDTO> manufacturingDefectSummaryDTOs = new LinkedList<>();

                for (ManufacturingDefect manufacturingDefect : manufacturingDefects) {
                    ManufacturingDefectSummaryDTO manufacturingDefectSummaryDTO = new ManufacturingDefectSummaryDTO(
                            manufacturingDefect.getOid(),
                            manufacturingDefect.getModelMonta().getModelOrder().getModel().getDefaultPhoto(),
                            manufacturingDefect.getModelMonta().getDocumentNumber(),
                            manufacturingDefect.getModelMonta().getModelOrder().getModel().getModelCode(),
                            manufacturingDefect.getModelMonta().getModelOrder().getModel().getCustomerModelCode(),
                            manufacturingDefect.getNumber(),
                            manufacturingDefect.getQuantity(),
                            manufacturingDefect.getSayaDefect(),
                            manufacturingDefect.getMontaDefect()
                    );

                    manufacturingDefectSummaryDTOs.add(manufacturingDefectSummaryDTO);
                }

                return Response.ok().entity(manufacturingDefectSummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readManufacturingDefect")
    @UnitOfWork
    public Response readManufacturingDefect(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String mdo = data.get("oid");

                ManufacturingDefect manufacturingDefect = manufacturingDefectDAO.getByOid(mdo, userToken.getUser().getCompany());

                return Response.ok().entity(manufacturingDefect).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteManufacturingDefect")
    @UnitOfWork
    public Response deleteManufacturingDefect(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_CREATE)) {
                String mdo = data.get("oid");

                ManufacturingDefect manufacturingDefect = manufacturingDefectDAO.getByOid(mdo, userToken.getUser().getCompany());
                manufacturingDefect.setDeleteStatus(true);
                manufacturingDefectDAO.update(manufacturingDefect);

                List<ModelMontaIncoming> modelMontaIncomings = modelMontaIncomingDAO.getByModelOrderAndQuantity(Integer.valueOf(manufacturingDefect.getQuantity()), manufacturingDefect.getModelMonta().getModelOrder(), userToken.getUser().getCompany());

                if (modelMontaIncomings != null && modelMontaIncomings.size() != 0) {
                    modelMontaIncomings.get(0).setDeleteStatus(true);
                    modelMontaIncomingDAO.update(modelMontaIncomings.get(0));
                }

                return Response.ok().entity(manufacturingDefect).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}

