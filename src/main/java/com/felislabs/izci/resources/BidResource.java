package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.dto.BidDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 09/03/2017.
 */
@Path("bid")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BidResource {
    AuthUtil authUtil;
    BidDAO bidDAO;
    AccountingCustomerDAO accountingCustomerDAO;
    AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO;
    TaxDAO taxDAO;
    ProductDAO productDAO;
    BidDetailDAO bidDetailDAO;
    InvoiceDAO invoiceDAO;
    InvoiceDetailDAO invoiceDetailDAO;
    ProformaDAO proformaDAO;
    ProformaDetailDAO proformaDetailDAO;
    ConsignmentInvoiceDAO consignmentInvoiceDAO;
    ConsignmentInvoiceDetailDAO consignmentInvoiceDetailDAO;

    public BidResource(AuthUtil authUtil, BidDAO bidDAO, AccountingCustomerDAO accountingCustomerDAO, AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO, TaxDAO taxDAO, ProductDAO productDAO, BidDetailDAO bidDetailDAO, InvoiceDAO invoiceDAO, InvoiceDetailDAO invoiceDetailDAO, ProformaDAO proformaDAO, ProformaDetailDAO proformaDetailDAO, ConsignmentInvoiceDAO consignmentInvoiceDAO, ConsignmentInvoiceDetailDAO consignmentInvoiceDetailDAO) {
        this.authUtil = authUtil;
        this.bidDAO = bidDAO;
        this.accountingCustomerDAO = accountingCustomerDAO;
        this.accountingCustomerRepresentativeDAO = accountingCustomerRepresentativeDAO;
        this.taxDAO = taxDAO;
        this.productDAO = productDAO;
        this.bidDetailDAO = bidDetailDAO;
        this.invoiceDAO = invoiceDAO;
        this.invoiceDetailDAO = invoiceDetailDAO;
        this.proformaDAO = proformaDAO;
        this.proformaDetailDAO = proformaDetailDAO;
        this.consignmentInvoiceDAO = consignmentInvoiceDAO;
        this.consignmentInvoiceDetailDAO = consignmentInvoiceDetailDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BID_CREATE)) {
                String customerOid = data.get("customerOid");
                String customerRepresentativeOid = data.get("customerRepresentativeOid");

                String bidDate = data.get("bidDate");
                String deliveryNote = data.get("deliveryNote");
                String paymentNote = data.get("paymentNote");
                String currencyNote = data.get("currencyNote");
                String currency = data.get("currency");
                String description = data.get("description");
                String details = data.get("details");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime bd = formatter.parseDateTime(bidDate);

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(customerRepresentativeOid, company);

                DateTime now = new DateTime();

                String documentNumber = accountingCustomer.getCode() + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + accountingCustomer.getBidNumber();

                accountingCustomer.setBidNumber(accountingCustomer.getBidNumber() + 1);
                accountingCustomerDAO.update(accountingCustomer);

                Bid bid = new Bid();
                bid.setCompany(company);
                bid.setUser(user);
                bid.setUpdateDate(new DateTime());
                bid.setAccountingCustomer(accountingCustomer);
                bid.setAccountingCustomerRepresentative(accountingCustomerRepresentative);
                bid.setBidDate(bd);
                bid.setDeliveryNote(deliveryNote);
                bid.setPaymentNote(paymentNote);
                bid.setCurrencyNote(currencyNote);
                bid.setCurrency(currency);
                bid.setDescription(description);
                bid.setDocumentNumber(documentNumber);

                bidDAO.create(bid);

                Double subTotal = 0.0;
                Double discountIncluded = 0.0;
                Double taxIncluded = 0.0;

                JSONArray jsonArray = new JSONArray(details);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String code = jsonObject.getString("code");
                    Double quantity = Double.valueOf(jsonObject.getString("quantity"));
                    Double price = Double.valueOf(jsonObject.getString("price"));
                    Double discount = Double.valueOf(jsonObject.getString("discount"));
                    String deadline = jsonObject.getString("deadline");
                    String detailTaxOid = jsonObject.getString("tax");

                    Double lineTotal = quantity * price;
                    Double discountIncludedPrice = lineTotal - (lineTotal * (discount / 100));

                    Product product = productDAO.getByCode(code, company);
                    Tax detailTax = taxDAO.getByOid(detailTaxOid, company);

                    Double taxIncludedPrice = discountIncludedPrice + (discountIncludedPrice * (detailTax.getRatio() / 100));

                    BidDetail bidDetails = new BidDetail();
                    bidDetails.setUser(user);
                    bidDetails.setUpdateDate(new DateTime());
                    bidDetails.setQuantity(quantity);
                    bidDetails.setPrice(price);
                    bidDetails.setDiscount(discount);
                    bidDetails.setDeadline(deadline);
                    bidDetails.setLineTotal(lineTotal);
                    bidDetails.setProduct(product);
                    bidDetails.setDiscountIncluded(discountIncludedPrice);
                    bidDetails.setTaxIncluded(taxIncludedPrice);
                    bidDetails.setBid(bid);
                    bidDetails.setTax(detailTax);

                    bidDetailDAO.create(bidDetails);

                    subTotal += lineTotal;
                    discountIncluded += discountIncludedPrice;
                    taxIncluded += taxIncludedPrice;
                }

                bid.setSubTotal(subTotal);
                bid.setTaxIncluded(taxIncluded);
                bid.setDiscountIncluded(discountIncluded);

                bidDAO.update(bid);

                return Response.ok().entity(bid).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BID_READ)) {
                List<Bid> bids = bidDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(bids).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BID_READ)) {
                String oid = data.get("oid");

                Bid bid = bidDAO.getByOid(oid, userToken.getUser().getCompany());

                List<BidDetail> bidDetails = bidDetailDAO.getByBid(bid);

                return Response.ok().entity(bidDetails).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BID_CREATE)) {
                String oid = data.get("oid");
                String customerOid = data.get("customerOid");
                String customerRepresentativeOid = data.get("customerRepresentativeOid");

                String bidDate = data.get("bidDate");
                String deliveryNote = data.get("deliveryNote");
                String paymentNote = data.get("paymentNote");
                String currencyNote = data.get("currencyNote");
                String currency = data.get("currency");
                String description = data.get("description");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime bd = formatter.parseDateTime(bidDate);

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(customerRepresentativeOid, company);

                Bid bid = bidDAO.getByOid(oid, company);
                bid.setUser(user);
                bid.setUpdateDate(new DateTime());
                bid.setAccountingCustomer(accountingCustomer);
                bid.setAccountingCustomerRepresentative(accountingCustomerRepresentative);
                bid.setBidDate(bd);
                bid.setDeliveryNote(deliveryNote);
                bid.setPaymentNote(paymentNote);
                bid.setCurrencyNote(currencyNote);
                bid.setCurrency(currency);
                bid.setDescription(description);

                bidDAO.update(bid);

                return Response.ok().entity(bid).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("detail/update")
    @UnitOfWork
    public Response detailUpdate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BID_CREATE)) {
                String oid = data.get("oid");
                String details = data.get("details");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Bid bid = bidDAO.getByOid(oid, company);

                Double subTotal = 0.0;
                Double discountIncluded = 0.0;
                Double taxIncluded = 0.0;

                // Delete bid's details if exist
                List<BidDetail> bidDetails = bidDetailDAO.getByBid(bid);
                if (bidDetails.size() != 0) {
                    for (BidDetail bidDetail : bidDetails) {
                        bidDetailDAO.delete(bidDetail);
                    }
                }

                JSONArray jsonArray = new JSONArray(details);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String code = jsonObject.getString("code");
                    Double quantity = Double.valueOf(jsonObject.getString("quantity"));
                    Double price = Double.valueOf(jsonObject.getString("price"));
                    Double discount = Double.valueOf(jsonObject.getString("discount"));
                    String deadline = jsonObject.getString("deadline");
                    String detailTaxOid = jsonObject.getString("tax");

                    Double lineTotal = quantity * price;
                    Double discountIncludedPrice = lineTotal - (lineTotal * (discount / 100));

                    Product product = productDAO.getByCode(code, company);
                    Tax detailTax = taxDAO.getByOid(detailTaxOid, company);

                    Double taxIncludedPrice = discountIncludedPrice + (discountIncludedPrice * (detailTax.getRatio() / 100));

                    BidDetail bidDetail = new BidDetail();
                    bidDetail.setUser(user);
                    bidDetail.setUpdateDate(new DateTime());
                    bidDetail.setQuantity(quantity);
                    bidDetail.setPrice(price);
                    bidDetail.setDiscount(discount);
                    bidDetail.setDeadline(deadline);
                    bidDetail.setLineTotal(lineTotal);
                    bidDetail.setProduct(product);
                    bidDetail.setDiscountIncluded(discountIncludedPrice);
                    bidDetail.setTaxIncluded(taxIncludedPrice);
                    bidDetail.setBid(bid);
                    bidDetail.setTax(detailTax);

                    bidDetailDAO.create(bidDetail);

                    subTotal += lineTotal;
                    discountIncluded += discountIncludedPrice;
                    taxIncluded += taxIncludedPrice;
                }

                bid.setSubTotal(subTotal);
                bid.setTaxIncluded(taxIncluded);
                bid.setDiscountIncluded(discountIncluded);

                bidDAO.update(bid);

                return Response.ok().entity(bid).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BID_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Bid bid = bidDAO.getByOid(oid, company);
                bid.setDeleteStatus(true);
                bidDAO.update(bid);

                return Response.ok().entity(bid).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("saveAs")
    @UnitOfWork
    public Response saveAs(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.INVOICE_CREATE)) {
                String type = data.get("type");
                String bidOid = data.get("oid");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Bid bid = bidDAO.getByOid(bidOid, company);

                if (type.equals("invoice")) {
                    Invoice invoice = invoiceDAO.getByBid(bid, company);

                    if (invoice == null) {
                        invoice = new Invoice();
                        invoice.setCompany(company);
                        invoice.setUser(user);
                        invoice.setUpdateDate(new DateTime());
                        invoice.setAccountingCustomer(bid.getAccountingCustomer());
                        invoice.setAccountingCustomerRepresentative(bid.getAccountingCustomerRepresentative());
                        invoice.setInvoiceDate(new DateTime());
                        invoice.setDeliveryNote(bid.getDeliveryNote());
                        invoice.setPaymentNote(bid.getPaymentNote());
                        invoice.setCurrencyNote(bid.getCurrencyNote());
                        invoice.setCurrency(bid.getCurrency());
                        invoice.setDescription(bid.getDescription());
                        invoice.setDocumentNumber(bid.getDocumentNumber());
                        invoice.setSubTotal(bid.getSubTotal());
                        invoice.setTaxIncluded(bid.getTaxIncluded());
                        invoice.setDiscountIncluded(bid.getDiscountIncluded());
                        invoice.setBid(bid);

                        invoiceDAO.create(invoice);

                        List<BidDetail> bidDetails = bidDetailDAO.getByBid(bid);

                        for (BidDetail bidDetail : bidDetails) {
                            InvoiceDetail invoiceDetail = new InvoiceDetail();
                            invoiceDetail.setUser(user);
                            invoiceDetail.setUpdateDate(new DateTime());
                            invoiceDetail.setQuantity(bidDetail.getQuantity());
                            invoiceDetail.setPrice(bidDetail.getPrice());
                            invoiceDetail.setDiscount(bidDetail.getDiscount());
                            invoiceDetail.setDeadline(bidDetail.getDeadline());
                            invoiceDetail.setLineTotal(bidDetail.getLineTotal());
                            invoiceDetail.setProduct(bidDetail.getProduct());
                            invoiceDetail.setDiscountIncluded(bidDetail.getDiscountIncluded());
                            invoiceDetail.setTaxIncluded(bidDetail.getTaxIncluded());
                            invoiceDetail.setInvoice(invoice);
                            invoiceDetail.setTax(bidDetail.getTax());

                            invoiceDetailDAO.create(invoiceDetail);
                        }

                        return Response.ok().entity(invoice).build();
                    } else {
                        return Response.serverError().entity(ErrorMessage.DUPLICATE_INVOICE.getErrorAlias()).build();
                    }
                } else if (type.equals("proforma")) {
                    Proforma proforma = proformaDAO.getByBid(bid, company);

                    if (proforma == null) {
                        proforma = new Proforma();
                        proforma.setCompany(company);
                        proforma.setUser(user);
                        proforma.setUpdateDate(new DateTime());
                        proforma.setAccountingCustomer(bid.getAccountingCustomer());
                        proforma.setAccountingCustomerRepresentative(bid.getAccountingCustomerRepresentative());
                        proforma.setProformaDate(new DateTime());
                        proforma.setDeliveryNote(bid.getDeliveryNote());
                        proforma.setPaymentNote(bid.getPaymentNote());
                        proforma.setCurrencyNote(bid.getCurrencyNote());
                        proforma.setCurrency(bid.getCurrency());
                        proforma.setDescription(bid.getDescription());
                        proforma.setDocumentNumber(bid.getDocumentNumber());
                        proforma.setSubTotal(bid.getSubTotal());
                        proforma.setTaxIncluded(bid.getTaxIncluded());
                        proforma.setDiscountIncluded(bid.getDiscountIncluded());
                        proforma.setBid(bid);

                        proformaDAO.create(proforma);

                        List<BidDetail> bidDetails = bidDetailDAO.getByBid(bid);

                        for (BidDetail bidDetail : bidDetails) {
                            ProformaDetail proformaDetail = new ProformaDetail();
                            proformaDetail.setUser(user);
                            proformaDetail.setUpdateDate(new DateTime());
                            proformaDetail.setQuantity(bidDetail.getQuantity());
                            proformaDetail.setPrice(bidDetail.getPrice());
                            proformaDetail.setDiscount(bidDetail.getDiscount());
                            proformaDetail.setDeadline(bidDetail.getDeadline());
                            proformaDetail.setLineTotal(bidDetail.getLineTotal());
                            proformaDetail.setProduct(bidDetail.getProduct());
                            proformaDetail.setDiscountIncluded(bidDetail.getDiscountIncluded());
                            proformaDetail.setTaxIncluded(bidDetail.getTaxIncluded());
                            proformaDetail.setProforma(proforma);
                            proformaDetail.setTax(bidDetail.getTax());

                            proformaDetailDAO.create(proformaDetail);
                        }

                        return Response.ok().entity(proforma).build();
                    } else {
                        return Response.serverError().entity(ErrorMessage.DUPLICATE_PROFORMA.getErrorAlias()).build();
                    }
                } else {
                    ConsignmentInvoice consignmentInvoice = consignmentInvoiceDAO.getByBid(bid, company);

                    if (consignmentInvoice == null) {
                        consignmentInvoice = new ConsignmentInvoice();
                        consignmentInvoice.setCompany(company);
                        consignmentInvoice.setUser(user);
                        consignmentInvoice.setUpdateDate(new DateTime());
                        consignmentInvoice.setAccountingCustomer(bid.getAccountingCustomer());
                        consignmentInvoice.setAccountingCustomerRepresentative(bid.getAccountingCustomerRepresentative());
                        consignmentInvoice.setConsignmentInvoiceDate(new DateTime());
                        consignmentInvoice.setDeliveryNote(bid.getDeliveryNote());
                        consignmentInvoice.setPaymentNote(bid.getPaymentNote());
                        consignmentInvoice.setCurrencyNote(bid.getCurrencyNote());
                        consignmentInvoice.setCurrency(bid.getCurrency());
                        consignmentInvoice.setDescription(bid.getDescription());
                        consignmentInvoice.setDocumentNumber(bid.getDocumentNumber());
                        consignmentInvoice.setSubTotal(bid.getSubTotal());
                        consignmentInvoice.setTaxIncluded(bid.getTaxIncluded());
                        consignmentInvoice.setDiscountIncluded(bid.getDiscountIncluded());
                        consignmentInvoice.setBid(bid);

                        consignmentInvoiceDAO.create(consignmentInvoice);

                        List<BidDetail> bidDetails = bidDetailDAO.getByBid(bid);

                        for (BidDetail bidDetail : bidDetails) {
                            ConsignmentInvoiceDetail consignmentInvoiceDetail = new ConsignmentInvoiceDetail();
                            consignmentInvoiceDetail.setUser(user);
                            consignmentInvoiceDetail.setUpdateDate(new DateTime());
                            consignmentInvoiceDetail.setQuantity(bidDetail.getQuantity());
                            consignmentInvoiceDetail.setPrice(bidDetail.getPrice());
                            consignmentInvoiceDetail.setDiscount(bidDetail.getDiscount());
                            consignmentInvoiceDetail.setDeadline(bidDetail.getDeadline());
                            consignmentInvoiceDetail.setLineTotal(bidDetail.getLineTotal());
                            consignmentInvoiceDetail.setProduct(bidDetail.getProduct());
                            consignmentInvoiceDetail.setDiscountIncluded(bidDetail.getDiscountIncluded());
                            consignmentInvoiceDetail.setTaxIncluded(bidDetail.getTaxIncluded());
                            consignmentInvoiceDetail.setConsignmentInvoice(consignmentInvoice);
                            consignmentInvoiceDetail.setTax(bidDetail.getTax());

                            consignmentInvoiceDetailDAO.create(consignmentInvoiceDetail);
                        }

                        return Response.ok().entity(consignmentInvoice).build();
                    } else {
                        return Response.serverError().entity(ErrorMessage.DUPLICATE_CONSIGMENT_INVOCE.getErrorAlias()).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readForPreview")
    @UnitOfWork
    public Response readForPreview(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BID_READ)) {
                String bidOid = data.get("oid");

                Bid bid = bidDAO.getByOid(bidOid, userToken.getUser().getCompany());
                List<BidDetail> bidDetails = bidDetailDAO.getByBid(bid);

                BidDTO bidDTO = new BidDTO();
                bidDTO.setBid(bid);
                bidDTO.setBidDetails(bidDetails);

                return Response.ok().entity(bidDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

}
