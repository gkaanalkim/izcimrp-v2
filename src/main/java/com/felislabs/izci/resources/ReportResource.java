package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.*;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.Days;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by arricie on 16.02.2017.
 */

@Path("report")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ReportResource {
    AuthUtil authUtil;
    ModelDAO modelDAO;
    CustomerDAO customerDAO;
    ModelOrderDAO modelOrderDAO;
    ModelAssortDAO modelAssortDAO;
    ModelMontaDAO modelMontaDAO;
    ModelMontaIncomingDAO modelMontaIncomingDAO;
    ModelCuttingOrderDAO modelCuttingOrderDAO;
    ModelCuttingIncomingDAO modelCuttingIncomingDAO;
    ModelSayaDAO modelSayaDAO;
    ModelSayaIncomingDAO modelSayaIncomingDAO;
    SupplyFormDAO supplyFormDAO;
    SystemActivityDAO systemActivityDAO;
    StockDAO stockDAO;
    ModelMaterialDAO modelMaterialDAO;
    SupplyRequirementDAO supplyRequirementDAO;
    SayaciDAO sayaciDAO;
    SupplierDAO supplierDAO;

    public ReportResource(AuthUtil authUtil, ModelDAO modelDAO, CustomerDAO customerDAO, ModelOrderDAO modelOrderDAO, ModelAssortDAO modelAssortDAO, ModelMontaDAO modelMontaDAO, ModelMontaIncomingDAO modelMontaIncomingDAO, ModelCuttingOrderDAO modelCuttingOrderDAO, ModelCuttingIncomingDAO modelCuttingIncomingDAO, ModelSayaDAO modelSayaDAO, ModelSayaIncomingDAO modelSayaIncomingDAO, SupplyFormDAO supplyFormDAO, SystemActivityDAO systemActivityDAO, StockDAO stockDAO, ModelMaterialDAO modelMaterialDAO, SupplyRequirementDAO supplyRequirementDAO, SayaciDAO sayaciDAO, SupplierDAO supplierDAO) {
        this.authUtil = authUtil;
        this.modelDAO = modelDAO;
        this.customerDAO = customerDAO;
        this.modelOrderDAO = modelOrderDAO;
        this.modelAssortDAO = modelAssortDAO;
        this.modelMontaDAO = modelMontaDAO;
        this.modelMontaIncomingDAO = modelMontaIncomingDAO;
        this.modelCuttingOrderDAO = modelCuttingOrderDAO;
        this.modelCuttingIncomingDAO = modelCuttingIncomingDAO;
        this.modelSayaDAO = modelSayaDAO;
        this.modelSayaIncomingDAO = modelSayaIncomingDAO;
        this.supplyFormDAO = supplyFormDAO;
        this.systemActivityDAO = systemActivityDAO;
        this.stockDAO = stockDAO;
        this.modelMaterialDAO = modelMaterialDAO;
        this.supplyRequirementDAO = supplyRequirementDAO;
        this.sayaciDAO = sayaciDAO;
        this.supplierDAO = supplierDAO;
    }

    @POST
    @Path("getAllModelOrders")
    @UnitOfWork
    public Response getAllModelOrders(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.REPORT_READ)) {
                Company company = userToken.getUser().getCompany();

                List<ModelOrder> modelOrders = modelOrderDAO.getAll(company);
                List<ModelOrderSummaryDTO> modelOrderSummaryDTOs = new LinkedList<>();

                for (ModelOrder modelOrder : modelOrders) {
                    Model model = modelOrder.getModel();
                    Customer customer = modelOrder.getModel().getCustomer();

                    ModelOrderSummaryDTO modelOrderSummaryDTO = new ModelOrderSummaryDTO(
                            model.getDefaultPhoto(),
                            modelOrder.getCreationDateTime(),
                            modelOrder.getTerminDate(),
                            customer.getName(),
                            model.getModelCode(),
                            model.getCustomerModelCode(),
                            modelOrder.getOrderQuantity()
                    );

                    modelOrderSummaryDTOs.add(modelOrderSummaryDTO);
                }

                return Response.ok().entity(modelOrderSummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getAllModelBySupply")
    @UnitOfWork
    public Response getAllModelBySupply(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.REPORT_READ)) {
                String modelOid = data.get("modelOid");
                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);

                List<SupplyRequirement> supplyRequirementList = supplyRequirementDAO.getByModel(model, company);

                return Response.ok().entity(supplyRequirementList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getSupplyBySupplier")
    @UnitOfWork
    public Response getSupplyBySupplier(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.REPORT_READ)) {
                String supplierOid = data.get("supplierOid");
                Company company = userToken.getUser().getCompany();
                Supplier supplier = supplierDAO.getById(supplierOid, company);
                List<SupplyRequirement> supplyRequirementList = supplyRequirementDAO.getBySupplier(supplier, company);
                List<SupplierDTO> supplierDTOS = new ArrayList<SupplierDTO>();
                for (int i = 0; i < supplyRequirementList.size(); i++) {
                    SupplierDTO supplierDTO = new SupplierDTO();
                    supplierDTO.setSupplyRequirement(supplyRequirementList.get(i));
                    DateTime termin = new DateTime(supplyRequirementList.get(i).getTerminDate().toDate()).withTime(0, 0, 0, 0);
                    DateTime nowDate = new DateTime(new DateTime().toDate()).withTime(0, 0, 0, 0);
                    int day = Days.daysBetween(termin, nowDate).getDays();
                    if (termin.isAfter(nowDate)) {
                        supplierDTO.setOnTerminDate(-1 * day);

                    } else {
                        int newDate = 0 - day;
                        supplierDTO.setOnTerminDate(newDate);

                    }
                    supplierDTOS.add(supplierDTO);
                }

                return Response.ok().entity(supplierDTOS).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getAllSayaBySayaci")
    @UnitOfWork
    public Response getAllSayaBySayaci(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.REPORT_READ)) {
                String sayaciOid = data.get("sayaciOid");
                Company company = userToken.getUser().getCompany();
                Sayaci sayaci = sayaciDAO.getByOid(sayaciOid, company);

                List<SayaciDTO> sayaciDTOS = new ArrayList<SayaciDTO>();
                List<ModelSaya> modelSayas = modelSayaDAO.getBySayaci(sayaci, company);
                if (modelSayas.size() > 0) {
                    for (int i = 0; i < modelSayas.size(); i++) {
                        List<ModelSayaIncoming> modelSayaIncomings = modelSayaIncomingDAO.getBySaya(modelSayas.get(i), company);
                        if (modelSayaIncomings.size() > 0) {
                            for (int j = 0; j < modelSayaIncomings.size(); j++) {
                                SayaciDTO sayaciDTO = new SayaciDTO();
                                sayaciDTO.setModelSaya(modelSayas.get(i));
                                sayaciDTO.setModelSayaIncoming(modelSayaIncomings.get(j));
                                DateTime endDate = modelSayas.get(i).getEndDate();
                                DateTime date = modelSayaIncomings.get(j).getCreationDateTime();
                                sayaciDTO.setModelSayaIncomingQuantity(modelSayaIncomings.get(j).getQuantity());
                                if (endDate.isBefore(date)) {
                                    if (modelSayas.get(i).getQuantity() == modelSayaIncomings.get(j).getQuantity()) {
                                        sayaciDTO.setOnTime("Zamanında Tamamlandı");
                                    } else if (modelSayas.get(i).getQuantity() > modelSayaIncomings.get(j).getQuantity()) {
                                        sayaciDTO.setOnTime("Zamanında Eksik Geldi");
                                    } else if (modelSayas.get(i).getQuantity() < modelSayaIncomings.get(j).getQuantity()) {
                                        sayaciDTO.setOnTime("Zamanında Fazla Geldi");
                                    }

                                } else {
                                    if (modelSayas.get(i).getQuantity() == modelSayaIncomings.get(j).getQuantity()) {
                                        sayaciDTO.setOnTime("Zamanın Dışında Tamamlandı");
                                    } else if (modelSayas.get(i).getQuantity() > modelSayaIncomings.get(j).getQuantity()) {
                                        sayaciDTO.setOnTime("Zamanın Dışında Eksik Geldi");
                                    } else if (modelSayas.get(i).getQuantity() < modelSayaIncomings.get(j).getQuantity()) {
                                        sayaciDTO.setOnTime("Zamanın Dışında Fazla Geldi");
                                    }

                                }

                                sayaciDTOS.add(sayaciDTO);
                            }
                        } else {
                            SayaciDTO sayaciDTO = new SayaciDTO();
                            sayaciDTO.setModelSaya(modelSayas.get(i));
                            sayaciDTO.setModelSayaIncoming(null);
                            sayaciDTO.setModelSayaIncomingQuantity(0);
                            sayaciDTO.setOnTime("Saya Kabul Yapılmadı");
                        }

                    }
                }


                return Response.ok().entity(sayaciDTOS).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getModelMaterialsByStock")
    @UnitOfWork
    public Response getByStock(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_READ)) {
                String oid = data.get("stockOid");
                Stock stock = stockDAO.getById(oid, userToken.getUser().getCompany());
                List<ModelMaterial> modelMaterials = modelMaterialDAO.getByStock(stock, userToken.getUser().getCompany());
                return Response.ok(modelMaterials).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSystemActivity")
    @UnitOfWork
    public Response readSystemActivity(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.REPORT_READ)) {
                Company company = userToken.getUser().getCompany();

                List<SystemActivity> systemActivities = systemActivityDAO.getAll(company);
                List<SystemActivitySummaryDTO> systemActivitySummaryDTOs = new LinkedList<>();

                for (SystemActivity systemActivity : systemActivities) {
                    SystemActivitySummaryDTO systemActivitySummaryDTO = new SystemActivitySummaryDTO(systemActivity.getCreationDateTime(), systemActivity.getUser().getName() + " " + systemActivity.getUser().getSurname(), systemActivity.getProcess());

                    systemActivitySummaryDTOs.add(systemActivitySummaryDTO);
                }

                return Response.ok().entity(systemActivitySummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readGeneralSummary")
    @UnitOfWork
    public Response readGeneralSummary(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.REPORT_READ)) {
                Company company = userToken.getUser().getCompany();

                List<ModelOrder> modelOrders = modelOrderDAO.getAll(company);
                List<GeneralSummaryReportDTO> generalSummaryReportDTOs = new LinkedList<>();

                for (ModelOrder modelOrder : modelOrders) {
                    List<ModelCuttingIncoming> modelCuttingIncomings = modelCuttingIncomingDAO.getByModelOrder(modelOrder, company);
                    List<ModelSayaIncoming> modelSayaIncomings = modelSayaIncomingDAO.getByModelOrder(modelOrder, company);
                    List<ModelMontaIncoming> modelMontaIncomings = modelMontaIncomingDAO.getByModelOrder(modelOrder, company);

                    Integer cuttingIncomingTotal = 0;

                    for (ModelCuttingIncoming modelCuttingIncoming : modelCuttingIncomings) {
                        cuttingIncomingTotal += modelCuttingIncoming.getQuantity();
                    }

                    Integer sayaIncomingTotal = 0;

                    for (ModelSayaIncoming modelSayaIncoming : modelSayaIncomings) {
                        sayaIncomingTotal += modelSayaIncoming.getQuantity();
                    }

                    Integer montaIncomingTotal = 0;

                    for (ModelMontaIncoming modelMontaIncoming : modelMontaIncomings) {
                        montaIncomingTotal += modelMontaIncoming.getQuantity();
                    }

                    GeneralSummaryReportDTO generalSummaryReportDTO = new GeneralSummaryReportDTO(
                            modelOrder.getCreationDateTime(),
                            modelOrder.getTerminDate(),
                            modelOrder.getModel().getCustomer().getName(),
                            modelOrder.getModel().getDefaultPhoto(),
                            modelOrder.getModel().getModelCode(),
                            modelOrder.getModel().getCustomerModelCode(),
                            modelOrder.getModel().getColor(),
                            modelOrder.getOrderQuantity(),
                            cuttingIncomingTotal,
                            sayaIncomingTotal,
                            montaIncomingTotal,
                            modelOrder.getModel().getSeason(),
                            modelOrder.getModel().getYear()
                            );

                    generalSummaryReportDTOs.add(generalSummaryReportDTO);
                }

                return Response.ok().entity(generalSummaryReportDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
