package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
@Path("proforma")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProformaResource {
    AuthUtil authUtil;
    BidDAO bidDAO;
    ProformaDAO proformaDAO;
    AccountingCustomerDAO accountingCustomerDAO;
    AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO;
    TaxDAO taxDAO;
    ProductDAO productDAO;
    ProformaDetailDAO proformaDetailDAO;

    public ProformaResource(AuthUtil authUtil, BidDAO bidDAO, ProformaDAO proformaDAO, AccountingCustomerDAO accountingCustomerDAO, AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO, TaxDAO taxDAO, ProductDAO productDAO, ProformaDetailDAO proformaDetailDAO) {
        this.authUtil = authUtil;
        this.bidDAO = bidDAO;
        this.proformaDAO = proformaDAO;
        this.accountingCustomerDAO = accountingCustomerDAO;
        this.accountingCustomerRepresentativeDAO = accountingCustomerRepresentativeDAO;
        this.taxDAO = taxDAO;
        this.productDAO = productDAO;
        this.proformaDetailDAO = proformaDetailDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROFORMA_CREATE)) {
                String customerOid = data.get("customerOid");
                String customerRepresentativeOid = data.get("customerRepresentativeOid");

                String billingDate = data.get("billingDate");
                String deliveryNote = data.get("deliveryNote");
                String paymentNote = data.get("paymentNote");
                String currencyNote = data.get("currencyNote");
                String currency = data.get("currency");
                String description = data.get("description");
                String details = data.get("details");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime pd = formatter.parseDateTime(billingDate);

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(customerRepresentativeOid, company);

                DateTime now = new DateTime();

                String documentNumber = accountingCustomer.getCode() + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + accountingCustomer.getBidNumber();

                accountingCustomer.setBidNumber(accountingCustomer.getBidNumber() + 1);
                accountingCustomerDAO.update(accountingCustomer);

                Proforma proforma = new Proforma();
                proforma.setCompany(company);
                proforma.setUser(user);
                proforma.setUpdateDate(new DateTime());
                proforma.setAccountingCustomer(accountingCustomer);
                proforma.setAccountingCustomerRepresentative(accountingCustomerRepresentative);
                proforma.setProformaDate(pd);
                proforma.setDeliveryNote(deliveryNote);
                proforma.setPaymentNote(paymentNote);
                proforma.setCurrencyNote(currencyNote);
                proforma.setCurrency(currency);
                proforma.setDescription(description);
                proforma.setDocumentNumber(documentNumber);

                proformaDAO.create(proforma);

                Double subTotal = 0.0;
                Double discountIncluded = 0.0;
                Double taxIncluded = 0.0;

                JSONArray jsonArray = new JSONArray(details);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String code = jsonObject.getString("code");
                    Double quantity = Double.valueOf(jsonObject.getString("quantity"));
                    Double price = Double.valueOf(jsonObject.getString("price"));
                    Double discount = Double.valueOf(jsonObject.getString("discount"));
                    String deadline = jsonObject.getString("deadline");
                    String detailTaxOid = jsonObject.getString("tax");

                    Double lineTotal = quantity * price;
                    Double discountIncludedPrice = lineTotal - (lineTotal * (discount / 100));

                    Product product = productDAO.getByCode(code, company);
                    Tax detailTax = taxDAO.getByOid(detailTaxOid, company);

                    Double taxIncludedPrice = discountIncludedPrice + (discountIncludedPrice * (detailTax.getRatio() / 100));

                    ProformaDetail proformaDetail = new ProformaDetail();
                    proformaDetail.setUser(user);
                    proformaDetail.setUpdateDate(new DateTime());
                    proformaDetail.setQuantity(quantity);
                    proformaDetail.setPrice(price);
                    proformaDetail.setDiscount(discount);
                    proformaDetail.setDeadline(deadline);
                    proformaDetail.setLineTotal(lineTotal);
                    proformaDetail.setProduct(product);
                    proformaDetail.setDiscountIncluded(discountIncludedPrice);
                    proformaDetail.setTaxIncluded(taxIncludedPrice);
                    proformaDetail.setProforma(proforma);
                    proformaDetail.setTax(detailTax);

                    proformaDetailDAO.create(proformaDetail);

                    subTotal += lineTotal;
                    discountIncluded += discountIncludedPrice;
                    taxIncluded += taxIncludedPrice;
                }

                proforma.setSubTotal(subTotal);
                proforma.setTaxIncluded(taxIncluded);
                proforma.setDiscountIncluded(discountIncluded);

                proformaDAO.update(proforma);

                return Response.ok().entity(proforma).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROFORMA_READ)) {
                List<Proforma> proformas = proformaDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(proformas).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROFORMA_READ)) {
                String oid = data.get("oid");

                Proforma proforma = proformaDAO.getByOid(oid, userToken.getUser().getCompany());

                List<ProformaDetail> proformaDetails = proformaDetailDAO.getByProforma(proforma);

                return Response.ok().entity(proformaDetails).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROFORMA_UPDATE)) {
                String oid = data.get("oid");
                String customerOid = data.get("customerOid");
                String customerRepresentativeOid = data.get("customerRepresentativeOid");

                String proformaDate = data.get("proformaDate");
                String deliveryNote = data.get("deliveryNote");
                String paymentNote = data.get("paymentNote");
                String currencyNote = data.get("currencyNote");
                String currency = data.get("currency");
                String description = data.get("description");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime pd = formatter.parseDateTime(proformaDate);

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(customerRepresentativeOid, company);

                DateTime now = new DateTime();

                String documentNumber = accountingCustomer.getCode() + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + accountingCustomer.getBidNumber();

                accountingCustomer.setBidNumber(accountingCustomer.getBidNumber() + 1);
                accountingCustomerDAO.update(accountingCustomer);

                Proforma proforma = proformaDAO.getByOid(oid, company);
                proforma.setCompany(company);
                proforma.setUser(user);
                proforma.setUpdateDate(new DateTime());
                proforma.setAccountingCustomer(accountingCustomer);
                proforma.setAccountingCustomerRepresentative(accountingCustomerRepresentative);
                proforma.setProformaDate(pd);
                proforma.setDeliveryNote(deliveryNote);
                proforma.setPaymentNote(paymentNote);
                proforma.setCurrencyNote(currencyNote);
                proforma.setCurrency(currency);
                proforma.setDescription(description);
                proforma.setDocumentNumber(documentNumber);

                proformaDAO.update(proforma);

                return Response.ok().entity(proforma).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("detail/update")
    @UnitOfWork
    public Response detailUpdate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROFORMA_UPDATE)) {
                String oid = data.get("oid");
                String details = data.get("details");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Proforma proforma = proformaDAO.getByOid(oid, company);

                // Delete invoice's details if exist
                List<ProformaDetail> proformaDetails = proformaDetailDAO.getByProforma(proforma);
                if (proformaDetails.size() != 0) {
                    for (ProformaDetail proformaDetail : proformaDetails) {
                        proformaDetailDAO.delete(proformaDetail);
                    }
                }

                Double subTotal = 0.0;
                Double discountIncluded = 0.0;
                Double taxIncluded = 0.0;

                JSONArray jsonArray = new JSONArray(details);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String code = jsonObject.getString("code");
                    Double quantity = Double.valueOf(jsonObject.getString("quantity"));
                    Double price = Double.valueOf(jsonObject.getString("price"));
                    Double discount = Double.valueOf(jsonObject.getString("discount"));
                    String deadline = jsonObject.getString("deadline");
                    String detailTaxOid = jsonObject.getString("tax");

                    Double lineTotal = quantity * price;
                    Double discountIncludedPrice = lineTotal - (lineTotal * (discount / 100));

                    Product product = productDAO.getByCode(code, company);
                    Tax detailTax = taxDAO.getByOid(detailTaxOid, company);

                    Double taxIncludedPrice = discountIncludedPrice + (discountIncludedPrice * (detailTax.getRatio() / 100));

                    ProformaDetail proformaDetail = new ProformaDetail();
                    proformaDetail.setUser(user);
                    proformaDetail.setUpdateDate(new DateTime());
                    proformaDetail.setQuantity(quantity);
                    proformaDetail.setPrice(price);
                    proformaDetail.setDiscount(discount);
                    proformaDetail.setDeadline(deadline);
                    proformaDetail.setLineTotal(lineTotal);
                    proformaDetail.setProduct(product);
                    proformaDetail.setDiscountIncluded(discountIncludedPrice);
                    proformaDetail.setTaxIncluded(taxIncludedPrice);
                    proformaDetail.setProforma(proforma);
                    proformaDetail.setTax(detailTax);

                    proformaDetailDAO.create(proformaDetail);

                    subTotal += lineTotal;
                    discountIncluded += discountIncludedPrice;
                    taxIncluded += taxIncludedPrice;
                }

                proforma.setSubTotal(subTotal);
                proforma.setTaxIncluded(taxIncluded);
                proforma.setDiscountIncluded(discountIncluded);

                proformaDAO.update(proforma);

                return Response.ok().entity(proforma).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROFORMA_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Proforma proforma = proformaDAO.getByOid(oid, company);
                proforma.setDeleteStatus(true);
                proformaDAO.update(proforma);

                return Response.ok().entity(proforma).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
