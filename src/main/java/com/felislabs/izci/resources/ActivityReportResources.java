package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.ActivityReportDAO;
import com.felislabs.izci.dao.dao.CompanyDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.ActivityReport;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by dogukanyilmaz on 06/02/2017.
 */
@Path("activityreport")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ActivityReportResources {
    ActivityReportDAO activityReportDAO;
    CompanyDAO companyDAO;
    AuthUtil authUtil;

    public ActivityReportResources(ActivityReportDAO activityReportDAO, CompanyDAO companyDAO, AuthUtil authUtil) {
        this.activityReportDAO = activityReportDAO;
        this.companyDAO = companyDAO;
        this.authUtil = authUtil;
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ACTIVITY_REPORT_READ)) {
                Company company = userToken.getUser().getCompany();

                List<ActivityReport> activityReports = activityReportDAO.getAll(company);

                return Response.ok().entity(activityReports).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }


    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> activityReportData) {
        String token = activityReportData.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ACTIVITY_REPORT_READ)) {
                String oid = activityReportData.get("oid");
                Company company = userToken.getUser().getCompany();
                return Response.ok(activityReportDAO.getById(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> activityReportData) {
        String token = activityReportData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ACTIVITY_REPORT_CREATE)) {
                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                Company company = userToken.getUser().getCompany();

                DateTime start = formatter.parseDateTime(activityReportData.get("start"));
                DateTime end = formatter.parseDateTime(activityReportData.get("end"));
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }
                String description = activityReportData.get("description");
                String title = activityReportData.get("title");

                if (start.isAfter(end)) {
                    return Response.serverError().entity(ErrorMessage.TR_STARTDATE_GT_ENDDATE.getErrorAlias()).build();
                }

                ActivityReport activityReport = new ActivityReport();
                activityReport.setStart(start);
                activityReport.setEnd(end);
                activityReport.setDescription(description);
                activityReport.setTitle(title);
                activityReport.setCompany(company);
                activityReportDAO.create(activityReport);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), title + " başlıklı faaliyet oluşturuldu.", new DateTime());

                return Response.ok(activityReport).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> activityReportData) {
        String token = activityReportData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ACTIVITY_REPORT_UPDATE)) {
                Company company = userToken.getUser().getCompany();

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");

                DateTime dateStart = formatter.parseDateTime(activityReportData.get("start"));
                DateTime dateEnd = formatter.parseDateTime(activityReportData.get("end"));
                if (dateEnd.isBefore(dateStart)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }
                String description = activityReportData.get("description");
                String title = activityReportData.get("title");
                String activityReportOid = activityReportData.get("oid");


                ActivityReport activityReport = activityReportDAO.getById(activityReportOid, company);
                activityReport.setStart(dateStart);
                activityReport.setEnd(dateEnd);
                activityReport.setDescription(description);
                activityReport.setTitle(title);
                activityReport.setCompany(company);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), title + " başlıklı faaliyet düzenlendi.", new DateTime());

                activityReportDAO.update(activityReport);

                return Response.ok(activityReport).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> activityReportData) {
        String token = activityReportData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ACTIVITY_REPORT_DELETE)) {
                String activiyReportOid = activityReportData.get("oid");
                Company company = userToken.getUser().getCompany();
                ActivityReport activityReport = activityReportDAO.getById(activiyReportOid, company);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), activityReport.getTitle() + " başlıklı faaliyet silindi.", new DateTime());

                activityReportDAO.delete(activityReport);

                return Response.ok(activityReport).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    @Path("getActivityReportsByDateRange")
    public Response getActivityReportsByDateRange(Map<String, String> activityReportData) throws ParseException {
        String token = activityReportData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ACTIVITY_REPORT_READ)) {
                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime startDate = formatter.parseDateTime(activityReportData.get("start"));
                DateTime endDate = formatter.parseDateTime(activityReportData.get("end"));

                if (endDate.isBefore(startDate)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                List<ActivityReport> activityReports = activityReportDAO.getByDateRange(startDate, endDate, userToken.getUser().getCompany());

                return Response.ok(activityReports).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

}
