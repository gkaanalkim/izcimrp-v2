package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.CustomerShipment;
import com.felislabs.izci.representation.entities.CuttingRequirement;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by dogukanyilmaz on 04/05/2017.
 */
@Path("cuttingRequirement")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CuttingRequirementResource {
    AuthUtil authUtil;
    CompanyDAO companyDAO;
    CuttingRequirementDAO cuttingRequirementDAO;
    ModelCuttingOrderDAO modelCuttingOrderDAO;
    StockDAO stockDAO;
    StockDetailDAO stockDetailDAO;

    public CuttingRequirementResource(AuthUtil authUtil, CompanyDAO companyDAO, CuttingRequirementDAO cuttingRequirementDAO, ModelCuttingOrderDAO modelCuttingOrderDAO,
                                      StockDAO stockDAO, StockDetailDAO stockDetailDAO) {
        this.authUtil = authUtil;
        this.companyDAO = companyDAO;
        this.cuttingRequirementDAO = cuttingRequirementDAO;
        this.modelCuttingOrderDAO = modelCuttingOrderDAO;
        this.stockDAO = stockDAO;
        this.stockDetailDAO = stockDetailDAO;
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_REQUIREMENT_READ)) {
                List<CuttingRequirement> cuttingRequirements = cuttingRequirementDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(cuttingRequirements).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUTTING_REQUIREMENT_READ)) {
                String cuttingRequirementOid = data.get("oid");

                CuttingRequirement cuttingRequirement = cuttingRequirementDAO.getByOid(cuttingRequirementOid, userToken.getUser().getCompany());

                return Response.ok().entity(cuttingRequirement).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
