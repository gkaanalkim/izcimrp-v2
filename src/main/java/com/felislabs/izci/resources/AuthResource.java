package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.auth.TokenGenerator;
import com.felislabs.izci.dao.dao.UserDAO;
import com.felislabs.izci.dao.dao.UserTokenDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.TokenDTO;
import com.felislabs.izci.representation.dto.UserDTO;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.User;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 09/09/15.
 */
@Path("auth")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthResource {
    UserDAO userDAO;
    UserTokenDAO userTokenDAO;
    AuthUtil authUtil;

    public AuthResource(UserDAO userDAO, UserTokenDAO userTokenDAO, AuthUtil authUtil) {
        this.userDAO = userDAO;
        this.userTokenDAO = userTokenDAO;
        this.authUtil = authUtil;
    }

    @POST
    @Path("login")
    @UnitOfWork
    public Response login(Map<String, String> userData) {
        String pass = userData.get("pass");
        String email = userData.get("email");

        if (pass.equals("") || email.equals("")) {
            return Response.serverError().entity(ErrorMessage.TR_INVALID_CREDENTIAL.getErrorAlias()).build();
        } else {
            User user = userDAO.findByEmail(email, pass);

            if (user != null) {
                UserToken userToken = userTokenDAO.getByUser(user);

                if (userToken != null && userToken.getStatus().equals(Boolean.TRUE)) {
                    userTokenDAO.delete(userToken);

                    userToken = new UserToken();
                    userToken.setUser(user);
                    userToken.setToken(TokenGenerator.generate().toString());
                    userToken.setStatus(Boolean.TRUE);

                    userTokenDAO.create(userToken);

                    TokenDTO tokenDTO = new TokenDTO();
                    tokenDTO.setToken(userToken.getToken());

                    return Response.ok(tokenDTO).build();
                } else {
                    userToken = new UserToken();
                    userToken.setUser(user);
                    userToken.setToken(TokenGenerator.generate().toString());
                    userToken.setStatus(Boolean.TRUE);

                    userTokenDAO.create(userToken);

                    TokenDTO tokenDTO = new TokenDTO();
                    tokenDTO.setToken(userToken.getToken());

                    return Response.ok(tokenDTO).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_INVALID_CREDENTIAL.getErrorAlias()).build();
            }
        }
    }

    @POST
    @Path("logout")
    @UnitOfWork
    public Response logout(Map<String, String> tokenData) {
        String token = tokenData.get("token");

        UserToken userToken = userTokenDAO.findByToken(token);
        userTokenDAO.delete(userToken);

        return Response.ok().build();
    }

    @POST
    @Path("getProfile/{crmt}")
    @UnitOfWork
    public Response getProfile(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PROFILE_READ)) {
                User user = userToken.getUser();

                UserDTO userDTO = new UserDTO();
                userDTO.setDeleteStatus(user.getDeleteStatus());
                userDTO.setName(user.getName());
                userDTO.setSurname(user.getSurname());
                userDTO.setInternalPhoneCode(user.getInternalPhoneCode());
                userDTO.setMobilePhone(user.getMobilePhone());
                userDTO.setEmail(user.getEmail());
                userDTO.setPassword(user.getPassword());

                return Response.ok(userDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    @Path("updateProfile")
    public Response updateProfile(Map<String, String> userData) {
        String token = userData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PROFILE_UPDATE)) {
                Company company = userToken.getUser().getCompany();

                String name = userData.get("name");
                String surname = userData.get("surname");
                String mobilePhone = userData.get("mobilePhone");
                String internalPhoneCode = userData.get("internalPhoneCode");
                String email = userData.get("email");
                String password = userData.get("password");

                User user = userToken.getUser();
                user.setName(name);
                user.setSurname(surname);
                user.setMobilePhone(mobilePhone);
                user.setInternalPhoneCode(internalPhoneCode);
                user.setEmail(email);
                user.setPassword(password);
                user.setCompany(company);

                userDAO.update(user);

                return Response.ok(user).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
