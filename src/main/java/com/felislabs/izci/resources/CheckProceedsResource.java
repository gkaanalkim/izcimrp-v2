package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.AccountDAO;
import com.felislabs.izci.dao.dao.AccountingCustomerDAO;
import com.felislabs.izci.dao.dao.CheckDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 14/03/2017.
 */
@Path("check")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CheckProceedsResource {
    AuthUtil authUtil;
    CheckDAO checkDAO;
    AccountingCustomerDAO accountingCustomerDAO;
    AccountDAO accountDAO;

    public CheckProceedsResource(AuthUtil authUtil, CheckDAO checkDAO, AccountingCustomerDAO accountingCustomerDAO, AccountDAO accountDAO) {
        this.authUtil = authUtil;
        this.checkDAO = checkDAO;
        this.accountingCustomerDAO = accountingCustomerDAO;
        this.accountDAO = accountDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CHECK_AND_BILL_CREATE)) {
                String customerOid = data.get("customerOid");
                String checkDate = data.get("checkDate");
                String maturity = data.get("maturity");
                String amount = data.get("amount");
                String currency = data.get("currency");
                String serialNo = data.get("serialNo");
                String bank = data.get("bank");
                String checkType = data.get("checkType");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date cd = null;
                Date md = null;
                try {
                    cd = sdf.parse(checkDate);
                    md = sdf.parse(maturity);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                CheckProceeds check = new CheckProceeds();
                check.setCompany(company);
                check.setUpdateDate(new DateTime());
                check.setUser(user);
                check.setAccountingCustomer(accountingCustomer);
                check.setAmount(Double.valueOf(amount));
                check.setCheckDate(cd);
                check.setMaturity(md);
                check.setCurrency(currency);
                check.setBank(bank);
                check.setSerialNo(serialNo);
                check.setCheckType(checkType);

                checkDAO.create(check);

                Double calculatedAmount = 0.0;

                if (checkType.equals("Müşteriden Tahsilat")) {
                    calculatedAmount = -1 * Double.valueOf(amount);
                } else if (checkType.equals("Müşteriye Ödeme")) {
                    calculatedAmount = Double.valueOf(amount);
                }

                accountingCustomer.setBalance(accountingCustomer.getBalance() + calculatedAmount);
                accountingCustomerDAO.update(accountingCustomer);

                return Response.ok().entity(check).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CHECK_AND_BILL_CREATE)) {
                String oid = data.get("oid");
                String customerOid = data.get("customerOid");
                String accountOid = data.get("accountOid");
                String checkDate = data.get("checkDate");
                String maturity = data.get("maturity");
                String amount = data.get("amount");
                String currency = data.get("currency");
                String serialNo = data.get("serialNo");
                String bank = data.get("bank");
                String proceedsType = data.get("proceedsType");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                Account account = accountDAO.getByOid(accountOid, company);

                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date cd = null;
                Date md = null;
                try {
                    cd = sdf.parse(checkDate);
                    md = sdf.parse(maturity);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                CheckProceeds check = checkDAO.getByOid(oid, company);
                check.setCompany(company);
                check.setUpdateDate(new DateTime());
                check.setUser(user);
                check.setAccount(account);
                check.setAccountingCustomer(accountingCustomer);
                check.setAmount(Double.valueOf(amount));
                check.setCheckDate(cd);
                check.setMaturity(md);
                check.setCurrency(currency);
                check.setBank(bank);
                check.setSerialNo(serialNo);

                checkDAO.update(check);

                Double calculatedAmount = 0.0;

                if (proceedsType.equals("Müşteriden Tahsilat")) {
                    account.setBalance(account.getBalance() + Double.valueOf(amount));
                    accountDAO.update(account);

                    calculatedAmount = -1 * Double.valueOf(amount);
                } else if (proceedsType.equals("Müşteriye Ödeme")) {
                    account.setBalance(account.getBalance() - Double.valueOf(amount));
                    accountDAO.update(account);

                    calculatedAmount = Double.valueOf(amount);
                }

                accountingCustomer.setBalance(accountingCustomer.getBalance() + calculatedAmount);
                accountingCustomerDAO.update(accountingCustomer);

                return Response.ok().entity("").build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CHECK_AND_BILL_UPDATE)) {
                List<CheckProceeds> checks = checkDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(checks).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByCustomer")
    @UnitOfWork
    public Response readByCustomer(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CHECK_AND_BILL_UPDATE)) {
                String oid = data.get("customerOid");

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(oid, userToken.getUser().getCompany());

                List<CheckProceeds> checks = checkDAO.getByAccountingCustomer(accountingCustomer, userToken.getUser().getCompany());

                return Response.ok().entity(checks).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_READ)) {
                String oid = data.get("oid");

                CheckProceeds check = checkDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(check).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_DELETE)) {
                String oid = data.get("oid");

                CheckProceeds check = checkDAO.getByOid(oid, userToken.getUser().getCompany());
                check.setDeleteStatus(true);

                checkDAO.update(check);

                return Response.ok().entity(check).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

}
