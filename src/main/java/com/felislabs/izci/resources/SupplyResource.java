package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.BulkSupplyDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
/**
 * Created by gunerkaanalkim on 30/01/2017.
 */
@Path("supply")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SupplyResource {
    AuthUtil authUtil;
    ModelDAO modelDAO;
    ModelOrderDAO modelOrderDAO;
    ModelSupplyDAO modelSupplyDAO;
    StockDAO stockDAO;
    ModelMaterialDAO modelMaterialDAO;
    ModelAssortDAO modelAssortDAO;
    SupplierDAO supplierDAO;
    SupplyFormDAO supplyFormDAO;
    MrpSettingDAO mrpSettingDAO;
    SupplyRequirementDAO supplyRequirementDAO;
    StockDetailDAO stockDetailDAO;

    public SupplyResource(
            AuthUtil authUtil,
            ModelDAO modelDAO,
            ModelOrderDAO modelOrderDAO,
            ModelSupplyDAO modelSupplyDAO,
            StockDAO stockDAO,
            ModelMaterialDAO modelMaterialDAO,
            ModelAssortDAO modelAssortDAO,
            SupplierDAO supplierDAO,
            SupplyFormDAO supplyFormDAO,
            MrpSettingDAO mrpSettingDAO,
            SupplyRequirementDAO supplyRequirementDAO,
            StockDetailDAO stockDetailDAO
    ) {
        this.authUtil = authUtil;
        this.modelDAO = modelDAO;
        this.modelOrderDAO = modelOrderDAO;
        this.modelSupplyDAO = modelSupplyDAO;
        this.stockDAO = stockDAO;
        this.modelMaterialDAO = modelMaterialDAO;
        this.modelAssortDAO = modelAssortDAO;
        this.supplierDAO = supplierDAO;
        this.supplyFormDAO = supplyFormDAO;
        this.mrpSettingDAO = mrpSettingDAO;
        this.supplyRequirementDAO = supplyRequirementDAO;
        this.stockDetailDAO = stockDetailDAO;
    }

    @POST
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY)) {
                String modelOid = data.get("modelOid");
                String supplyList = data.get("supplyList");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Model model = modelDAO.getByOid(modelOid, company);

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);
                List<ModelSupply> modelSupplies = new LinkedList<>();

                JSONArray jsonArray = new JSONArray(supplyList);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String stockOid = jsonObject.getString("stockOid");
                    String quantity = jsonObject.getString("quantity");
                    Boolean confirm = jsonObject.getBoolean("confirm");
                    String type = jsonObject.getString("type");

                    Stock stock = stockDAO.getById(stockOid, company);

                    ModelSupply modelSupply = new ModelSupply();
                    modelSupply.setCompany(company);
                    modelSupply.setUpdater(user);
                    modelSupply.setUpdateDate(new DateTime());
                    modelSupply.setModelOrder(modelOrder);
                    modelSupply.setStock(stock);
                    modelSupply.setQuantity(Double.valueOf(quantity));
                    modelSupply.setConfirm(confirm);
                    modelSupply.setType(type);

                    modelSupplyDAO.create(modelSupply);

                    modelSupplies.add(modelSupply);
                }

                return Response.ok(modelSupplies).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("updateForBulkSupply")
    @UnitOfWork
    public Response updateForBulkSupply(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY)) {
                String supplyList = data.get("supplyList");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                List<ModelSupply> modelSupplies = new LinkedList<>();

                JSONArray jsonArray = new JSONArray(supplyList);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String modelOid = jsonObject.getString("modelOid");
                    String stockOid = jsonObject.getString("stockOid");
                    String quantity = jsonObject.getString("quantity");
                    Boolean confirm = jsonObject.getBoolean("confirm");
                    String type = jsonObject.getString("type");

                    Stock stock = stockDAO.getById(stockOid, company);
                    Model model = modelDAO.getByOid(modelOid, company);
                    ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                    ModelSupply modelSupply = new ModelSupply();
                    modelSupply.setCompany(company);
                    modelSupply.setUpdater(user);
                    modelSupply.setUpdateDate(new DateTime());
                    modelSupply.setModelOrder(modelOrder);
                    modelSupply.setStock(stock);
                    modelSupply.setQuantity(Double.valueOf(quantity));
                    modelSupply.setConfirm(confirm);
                    modelSupply.setType(type);

                    modelSupplyDAO.create(modelSupply);

                    modelSupplies.add(modelSupply);
                }

                return Response.ok(modelSupplies).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readModelForBulkSupply")
    @UnitOfWork
    public Response readModelForBulkSupply(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PRICE)) {
                List<Model> models = modelDAO.getAll(userToken.getUser().getCompany());

                List<ModelOrder> modelOrders = new LinkedList<>();

                for (Model model : models) {
                    ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                    if (modelOrder != null) {
                        modelOrders.add(modelOrder);
                    }
                }

                List<ModelMaterial> modelMaterialList = new LinkedList<>();

                for (ModelOrder modelOrder : modelOrders) {
                    List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModel(modelOrder.getModel(), userToken.getUser().getCompany());

                    if (modelMaterials.size() != 0) {
                        for (ModelMaterial modelMaterial : modelMaterials) {
                            modelMaterialList.add(modelMaterial);
                        }
                    }
                }

                return Response.ok(modelMaterialList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readOrderByMaterials")
    @UnitOfWork
    public Response readAssortAndOrderByMaterials(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PRICE)) {
                String materials = data.get("materials");

                Company company = userToken.getUser().getCompany();

                List<ModelOrder> modelOrders = new LinkedList<>();

                JSONArray jsonArray = new JSONArray(materials);

                for (int i = 0; i < jsonArray.length(); i++) {
                    Object o = jsonArray.get(i);

                    ModelMaterial modelMaterial = modelMaterialDAO.getByOid(o.toString(), company);

                    ModelOrder modelOrder = modelOrderDAO.getByModel(modelMaterial.getModel(), company);

                    modelOrders.add(modelOrder);
                }

                Set<ModelOrder> modelOrderSet = new HashSet<>(modelOrders);

                return Response.ok().entity(modelOrderSet).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readAssortByMaterials")
    @UnitOfWork
    public Response readAssortByMaterials(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PRICE)) {
                String materials = data.get("materials");

                Company company = userToken.getUser().getCompany();

                List<ModelOrder> modelOrders = new LinkedList<>();

                JSONArray jsonArray = new JSONArray(materials);

                for (int i = 0; i < jsonArray.length(); i++) {
                    Object o = jsonArray.get(i);

                    ModelMaterial modelMaterial = modelMaterialDAO.getByOid(o.toString(), company);

                    ModelOrder modelOrder = modelOrderDAO.getByModel(modelMaterial.getModel(), company);

                    modelOrders.add(modelOrder);
                }

                Set<ModelOrder> modelOrderSet = new HashSet<>(modelOrders);

                return Response.ok().entity(modelOrderSet).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readOrderAndMaterials")
    @UnitOfWork
    public Response readOrderAndMaterials(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PRICE)) {
                String modelOids = data.get("modelOids");

                Company company = userToken.getUser().getCompany();

                List<BulkSupplyDTO> bulkSupplyDTOs = new LinkedList<>();

                JSONArray jsonArray = new JSONArray(modelOids);

                for (int i = 0; i < jsonArray.length(); i++) {
                    Object o = jsonArray.get(i);

                    Model model = modelDAO.getByOid(o.toString(), company);
                    ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);
                    List<ModelMaterial> modelMaterials = modelMaterialDAO.getByModelForSupply(model, company);
                    List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelOrder, company);

                    BulkSupplyDTO bulkSupplyDTO = new BulkSupplyDTO();
                    bulkSupplyDTO.setModelOrder(modelOrder);
                    bulkSupplyDTO.setModelMaterials(modelMaterials);
                    bulkSupplyDTO.setModelAssorts(modelAssorts);
                    bulkSupplyDTOs.add(bulkSupplyDTO);
                }

                return Response.ok(bulkSupplyDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("supplyFormCreate")
    @UnitOfWork
    public Response supplyFormCreate(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_CREATE)) {
                String terminDate = data.get("terminDate");
                String description = data.get("description");
                String supplierCode = data.get("supplierCode");
                String materials = data.get("materials");
                String models = data.get("models");
                String type = data.get("type");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                DateTime now = new DateTime();
                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);

                String documentNumber = type + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + mrpSetting.getSupplyFormCounter();

                mrpSetting.setSupplyFormCounter(mrpSetting.getSupplyFormCounter() + 1);
                mrpSettingDAO.update(mrpSetting);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime termin = formatter.parseDateTime(terminDate);

                Supplier supplier = supplierDAO.getByCode(supplierCode, company);

                SupplyForm supplyForm = new SupplyForm();
                supplyForm.setCompany(company);
                supplyForm.setUpdateDate(new DateTime());
                supplyForm.setUpdater(user);
                supplyForm.setTerminDate(termin);
                supplyForm.setDescription(description);
                supplyForm.setSupplier(supplier);
                supplyForm.setMaterials(materials);
                supplyForm.setModels(models);
                supplyForm.setDocumentNumber(documentNumber);
                supplyForm.setType(type);

                JSONArray jsonArray = new JSONArray(models);
                JSONArray jsonMaterials = new JSONArray(materials);

                if (type.equals("SS")) {
                    for (int i = 0; i < jsonMaterials.length(); i++) {
                        SupplyRequirement supplyRequirement = new SupplyRequirement();

                        supplyRequirement.setCompany(company);
                        supplyRequirement.setDescription(description);
                        supplyRequirement.setUpdateDate(new DateTime());
                        supplyRequirement.setUpdater(user);
                        supplyRequirement.setTerminDate(termin);
                        supplyRequirement.setSupplier(supplier);
                        supplyRequirement.setDocumentNumber(documentNumber);
                        supplyRequirement.setType(type);
                        supplyRequirement.setModel(modelDAO.getByOid(jsonArray.getString(0), company));

                        JSONObject jsonMaterial = jsonMaterials.getJSONObject(i);

                        String stockDetailName = jsonMaterial.getString("stockDetail");
                        String stockOid = jsonMaterial.getString("stockOid");
                        Stock stock = stockDAO.getById(stockOid, company);
                        StockDetail stockDetail = stockDetailDAO.getByNameAndStock(stockDetailName, stock, company);
                        Double stockRequirement = Double.parseDouble(jsonMaterial.getString("stockRequirement"));
                        String outerHtml = data.get("outerHtml");
                        supplyRequirement.setStock(stock);
                        supplyRequirement.setStockDetail(stockDetail);
                        supplyRequirement.setOrderQuantity(stockRequirement);
                        supplyRequirement.setOuterHtml(outerHtml);
                        supplyRequirementDAO.create(supplyRequirement);
                    }
                    supplyFormDAO.create(supplyForm);
                }

                return Response.ok().entity(supplyForm).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("bulkSupplyFormCreate")
    @UnitOfWork
    public Response bulkSupplyFormCreate(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_CREATE)) {
                String terminDate = data.get("terminDate");
                String description = data.get("description");
                String supplierCode = data.get("supplierCode");
                String materials = data.get("materials");
                String models = data.get("models");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                DateTime now = new DateTime();
                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);

                String documentNumber = "BS" + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + mrpSetting.getSupplyFormCounter();

                mrpSetting.setSupplyFormCounter(mrpSetting.getSupplyFormCounter() + 1);
                mrpSettingDAO.update(mrpSetting);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime termin = formatter.parseDateTime(terminDate);

                Supplier supplier = supplierDAO.getByCode(supplierCode, company);

                SupplyForm supplyForm = new SupplyForm();
                supplyForm.setCompany(company);
                supplyForm.setUpdateDate(new DateTime());
                supplyForm.setUpdater(user);
                supplyForm.setTerminDate(termin);
                supplyForm.setDescription(description);
                supplyForm.setSupplier(supplier);
                supplyForm.setMaterials(materials);
                supplyForm.setModels(models);
                supplyForm.setDocumentNumber(documentNumber);

                JSONArray jsonArray = new JSONArray(models);
                JSONArray jsonMaterials = new JSONArray(materials);

                now = new DateTime();
                mrpSetting = mrpSettingDAO.get(company).get(0);

                documentNumber = "BS" + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + mrpSetting.getSupplyFormCounter();

                mrpSetting.setSupplyFormCounter(mrpSetting.getSupplyFormCounter() + 1);
                mrpSettingDAO.update(mrpSetting);

                for (int j = 0; j < jsonArray.length(); j++) {
                    for (int i = 0; i < jsonMaterials.getJSONArray(j).length(); i++) {

                        SupplyRequirement supplyRequirement = new SupplyRequirement();



                        supplyRequirement.setCompany(company);
                        supplyRequirement.setDescription(description);
                        supplyRequirement.setUpdateDate(new DateTime());
                        supplyRequirement.setUpdater(user);
                        supplyRequirement.setTerminDate(termin);
                        supplyRequirement.setSupplier(supplier);
                        supplyRequirement.setDocumentNumber(documentNumber);
                        supplyRequirement.setType("BS");
                        supplyRequirement.setModel(modelDAO.getByOid(jsonArray.getString(j), company));
                        JSONObject jsonMaterial = jsonMaterials.getJSONArray(j).getJSONObject(i);

                        String stockDetailName = jsonMaterial.getString("stockDetail");
                        String stockOid = jsonMaterial.getString("stockOid");
                        Stock stock = stockDAO.getById(stockOid, company);

                        StockDetail stockDetail;
                        if(!stockDetailName.equals("-")){
                            stockDetail = stockDetailDAO.getByNameAndStock(stockDetailName, stock, company);
                            supplyRequirement.setStockDetail(stockDetail);
                        }

                        Double stockRequirement = jsonMaterial.getDouble("stockRequirement");
                        String outerHtml = data.get("outerHtml");
                        supplyRequirement.setStock(stock);
                        supplyRequirement.setOrderQuantity(stockRequirement);
                        supplyRequirement.setOuterHtml(outerHtml);
                        supplyRequirementDAO.create(supplyRequirement);
                    }
                }


                supplyFormDAO.create(supplyForm);

                return Response.ok().entity(supplyForm).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("supplyFormRead")
    @UnitOfWork
    public Response supplyFormRead(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_CREATE)) {
                String type = data.get("type");

                if (type.equals("ALL")) {
                    List<SupplyForm> supplyForms = supplyFormDAO.getAll(userToken.getUser().getCompany());
                    return Response.ok().entity(supplyForms).build();
                } else {
                    List<SupplyForm> supplyForms = supplyFormDAO.getByType(type, userToken.getUser().getCompany());
                    return Response.ok().entity(supplyForms).build();
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> customerData) {
        UserToken userToken = authUtil.isValidToken(customerData.get("crmt"));
        String oid = customerData.get("oid");

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(supplyFormDAO.getById(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSupplyDetail")
    @UnitOfWork
    public Response readSupplyDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_READ)) {
                Company company = userToken.getUser().getCompany();
                String oid = data.get("oid");
                SupplyRequirement supplyRequirement = supplyRequirementDAO.readSupplyDetail(oid, company);
                return Response.ok().entity(supplyRequirement).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("supplyFormUpdate")
    @UnitOfWork
    public Response supplyFormUpdate(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_UPDATE)) {
                String oid = data.get("oid");
                String description = data.get("description");
                String terminDate = data.get("terminDate");
                String materials = data.get("materials");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime termin = formatter.parseDateTime(terminDate);


                SupplyForm supplyForm = supplyFormDAO.getById(oid, userToken.getUser().getCompany());
                supplyForm.setDescription(description);
                supplyForm.setTerminDate(termin);
                supplyForm.setMaterials(materials);
                supplyFormDAO.update(supplyForm);

                return Response.ok().entity(supplyForm).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_DELETE)) {

                String oid = data.get("oid");
                Company company = userToken.getUser().getCompany();
                SupplyForm supplyForm = supplyFormDAO.getById(oid, company);

                supplyFormDAO.delete(supplyForm);

                if (data.get("type").equals("SS")) {
                    String documentNumber = data.get("documentNumber");
                    List<SupplyRequirement> supplyRequirements = supplyRequirementDAO.getSupplyByDocumentNumber(documentNumber, company);
                    for (SupplyRequirement supplyRequirement : supplyRequirements) {
                        supplyRequirementDAO.delete(supplyRequirement);
                    }
                }
                return Response.ok(supplyForm).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getDocumentNumber")
    @UnitOfWork
    public Response getDocumentNumber(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(data.get("modelOid"), company);
                Stock stock = stockDAO.getById(data.get("stockOid"), company);
                List<SupplyRequirement> supplyRequirements = supplyRequirementDAO.getSupplyRequirement(model, stock, company);

                return Response.ok(supplyRequirements).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readMaterialOfModelBySupplier")
    @UnitOfWork
    public Response readMaterialOfModelBySupplier(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PRICE)) {
                String modelOids = data.get("modelOids");
                String supplierCode = data.get("supplierCode");

                Company company = userToken.getUser().getCompany();

                List<BulkSupplyDTO> bulkSupplyDTOs = new LinkedList<>();

                JSONArray jsonArray = new JSONArray(modelOids);

                for (int i = 0; i < jsonArray.length(); i++) {
                    Object o = jsonArray.get(i);

                    Model model = modelDAO.getByOid(o.toString(), company);
                    ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);
                    List<ModelMaterial> modelMaterials = modelMaterialDAO.getBySupplierAndModel(supplierCode, model, company);
                    List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelOrder, company);

                    BulkSupplyDTO bulkSupplyDTO = new BulkSupplyDTO();
                    bulkSupplyDTO.setModelOrder(modelOrder);
                    bulkSupplyDTO.setModelMaterials(modelMaterials);
                    bulkSupplyDTO.setModelAssorts(modelAssorts);
                    bulkSupplyDTOs.add(bulkSupplyDTO);
                }

                return Response.ok(bulkSupplyDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getBulkSupply")
    @UnitOfWork
    public Response getBulkSupply(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_READ)) {
                Company company = userToken.getUser().getCompany();
                List<SupplyRequirement> supplyRequirementList = supplyRequirementDAO.getBulkSupply(company);
                return Response.ok().entity(supplyRequirementList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
    @POST
    @Path("getSupply")
    @UnitOfWork
    public Response getSupply(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_READ)) {
                Company company = userToken.getUser().getCompany();
                List<SupplyRequirement> supplyRequirementList = supplyRequirementDAO.getSupply(company);
                return Response.ok().entity(supplyRequirementList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readBulkDetail")
    @UnitOfWork
    public Response readBulkDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_READ)) {
                String oid=data.get("oid");
                Company company = userToken.getUser().getCompany();
                SupplyRequirement supplyRequirement= supplyRequirementDAO.readBulkDetail(oid,company);
                return Response.ok().entity(supplyRequirement).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("deleteBulkSupply")
    @UnitOfWork
    public Response deleteBulkSupply(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_READ)) {
                String oid=data.get("oid");
                Company company = userToken.getUser().getCompany();
                SupplyRequirement supplyRequirement= supplyRequirementDAO.readBulkDetail(oid,company);
                supplyRequirement.setDeleteStatus(true);
                supplyRequirementDAO.update(supplyRequirement);
                return Response.ok().entity(supplyRequirement).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("deleteSupply")
    @UnitOfWork
    public Response deleteSupply(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_READ)) {
                String oid=data.get("oid");
                Company company = userToken.getUser().getCompany();
                SupplyRequirement supplyRequirement= supplyRequirementDAO.readSupplyDetail(oid,company);
                supplyRequirement.setDeleteStatus(true);
                supplyRequirementDAO.update(supplyRequirement);
                return Response.ok().entity(supplyRequirement).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByDocumentNumber")
    @UnitOfWork
    public Response getByDocumentNumber(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLY_READ)) {
                String documentNumber=data.get("documentNumber");
                Company company = userToken.getUser().getCompany();

                List<SupplyRequirement> supplyRequirementList= supplyRequirementDAO.getByDocumentNumber(documentNumber,company);

                return Response.ok().entity(supplyRequirementList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

}