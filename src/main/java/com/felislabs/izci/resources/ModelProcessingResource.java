package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.ModelProcessingDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by arricie on 21.03.2017.
 */
@Path("modelProcessing")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ModelProcessingResource {
    AuthUtil authUtil;
    ModelDAO modelDAO;
    ModelOrderDAO modelOrderDAO;
    ModelProcessingDAO modelProcessingDAO;
    ContractedManufacturerDAO contractedManufacturerDAO;
    ModelAssortDAO modelAssortDAO;
    ModelProcessingIncomingDAO modelProcessingIncomingDAO;
    StaffDAO staffDAO;

    public ModelProcessingResource(
            AuthUtil authUtil,
            ModelDAO modelDAO,
            ModelOrderDAO modelOrderDAO,
            ModelProcessingDAO modelProcessingDAO,
            ContractedManufacturerDAO contractedManufacturerDAO,
            ModelAssortDAO modelAssortDAO,
            ModelProcessingIncomingDAO modelProcessingIncomingDAO,
            StaffDAO staffDAO
    ) {
        this.authUtil = authUtil;
        this.modelDAO = modelDAO;
        this.modelOrderDAO = modelOrderDAO;
        this.modelProcessingDAO = modelProcessingDAO;
        this.contractedManufacturerDAO = contractedManufacturerDAO;
        this.modelAssortDAO = modelAssortDAO;
        this.modelProcessingIncomingDAO = modelProcessingIncomingDAO;
        this.staffDAO=staffDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_CREATE)) {
                Company company = userToken.getUser().getCompany();

                String modelOid = data.get("modelOid");
                String staffOrContracted = data.get("staffOrContracted");
//                String staffOid = data.get("staff");
                String quantity = data.get("quantity");
                String description = data.get("description");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                String selectedAssort = data.get("selectedAssort");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                Model model = modelDAO.getByOid(modelOid, company);
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);
                ContractedManufacturer contractedManufacturer = contractedManufacturerDAO.getByCode(staffOrContracted, company);
                Staff staff=staffDAO.getByOid(staffOrContracted,company);
                String documentCode="";
                ModelProcessing modelProcessing = new ModelProcessing();
                if(staff==null){
                    modelProcessing.setContractedManufacturer(contractedManufacturer);
                    documentCode=contractedManufacturer.getCode();
                }else if(contractedManufacturer==null){
                    modelProcessing.setStaff(staff);
                    documentCode=staff.getCode();
                }
                modelProcessing.setCompany(company);
                modelProcessing.setUpdater(userToken.getUser());
                modelProcessing.setUpdateDate(new DateTime());
                modelProcessing.setModelOrder(modelOrder);
                modelProcessing.setQuantity(Integer.valueOf(quantity));
                modelProcessing.setStartDate(start);
                modelProcessing.setEndDate(end);
                modelProcessing.setDescription(description);
                modelProcessing.setSelectedAssortUid(selectedAssort);

                DateTime now = new DateTime();

                String documentNumber = documentCode + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + contractedManufacturer.getProcessingOrderCounter();

                modelProcessing.setDocumentNumber(documentNumber);

                modelProcessingDAO.create(modelProcessing);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelin " + modelProcessing.getQuantity() + " çifti prosese verildi.", new DateTime());

                return Response.ok().entity(modelProcessing).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok().entity(modelProcessingDAO.getAll(company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_READ)) {
                String oid = data.get("oid");
                Company company = userToken.getUser().getCompany();
                return Response.ok().entity(modelProcessingDAO.getByOid(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByModel")
    @UnitOfWork
    public Response readByModel(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_READ)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(modelOid, company);
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                List<ModelProcessing> modelProcessings = modelProcessingDAO.getByModelOrder(modelOrder, company);

                return Response.ok().entity(modelProcessings).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readProcessingOrderAndAssort")
    @UnitOfWork
    public Response readProcessingOrderAndAssort(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_READ)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                ModelProcessing modelProcessing = modelProcessingDAO.getByOid(oid, company);

                List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelProcessing.getModelOrder(), company);

                ModelProcessingDTO modelProcessingDTO = new ModelProcessingDTO();
                modelProcessingDTO.setModelProcessing(modelProcessing);
                modelProcessingDTO.setModelAssorts(modelAssorts);

                return Response.ok().entity(modelProcessingDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                ModelProcessing modelProcessing = modelProcessingDAO.getByOid(oid, company);
                List<ModelProcessingIncoming> modelProcessingIncoming=modelProcessingIncomingDAO.getByModelProcessing(modelProcessing, company);
                if (modelProcessingIncoming.size()==0) {
                    modelProcessingDAO.delete(modelProcessing);
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_ALREADY_ACCEPTED.getErrorAlias()).build();
                }
                List<ModelProcessing> modelProcessings = modelProcessingDAO.getByModelOrder(modelProcessing.getModelOrder(), company);

                return Response.ok().entity(modelProcessings).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("acceptProcessing")
    @UnitOfWork
    public Response acceptProcessing(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_UPDATE)) {
                String oid = data.get("oid");
                String quantity = data.get("quantity");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();

                ModelProcessing modelProcessing = modelProcessingDAO.getByOid(oid, company);

                ModelProcessingIncoming modelProcessingIncoming = new ModelProcessingIncoming();
                modelProcessingIncoming.setCompany(company);
                modelProcessingIncoming.setUpdater(userToken.getUser());
                modelProcessingIncoming.setUpdateDate(new DateTime());
                modelProcessingIncoming.setQuantity(Integer.valueOf(quantity));
                modelProcessingIncoming.setDescription(description);
                modelProcessingIncoming.setModelProcessing(modelProcessing);
                modelProcessingIncoming.setModelOrder(modelProcessing.getModelOrder());

                modelProcessingIncomingDAO.update(modelProcessingIncoming);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelProcessingIncoming.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelProcessing.getQuantity() + " çifti proses geldi.", new DateTime());

                return Response.ok().entity(modelProcessingIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingProcessingOrder")
    @UnitOfWork
    public Response readIncomingProcessingOrder(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_UPDATE)) {
                String modelOid = data.get("oid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                List<ModelProcessingIncoming> modelProcessingIncomings = modelProcessingIncomingDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());

                return Response.ok().entity(modelProcessingIncomings).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingProcessingOrderDetail")
    @UnitOfWork
    public Response readIncomingProcessingOrderDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_UPDATE)) {
                String incomingProcessingOrderOid = data.get("oid");

                ModelProcessingIncoming modelProcessingIncoming = modelProcessingIncomingDAO.getByOid(incomingProcessingOrderOid, userToken.getUser().getCompany());

                return Response.ok().entity(modelProcessingIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteIncomingProcessing")
    @UnitOfWork
    public Response deleteIncomingProcessing(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_PROCESSING_UPDATE)) {
                String oid = data.get("oid");

                ModelProcessingIncoming modelProcessingIncoming = modelProcessingIncomingDAO.getByOid(oid, userToken.getUser().getCompany());

                modelProcessingIncomingDAO.delete(modelProcessingIncoming);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelProcessingIncoming.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelProcessingIncoming.getModelProcessing().getQuantity() + " çiftlik gelen proses bilgisi silindi.", new DateTime());

                return Response.ok().entity(modelProcessingIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
