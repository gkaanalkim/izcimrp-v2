package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.WarehouseIncomingSummaryDTO;
import com.felislabs.izci.representation.dto.WarehouseOutgoingSummaryDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by dogukanyilmaz on 04/05/2017.
 */
@Path("warehouse")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class WarehouseResource {
    AuthUtil authUtil;
    CompanyDAO companyDAO;
    WarehouseDAO warehouseDAO;
    StockDetailDAO stockDetailDAO;
    StockDAO stockDAO;
    WarehouseIncomingDAO warehouseIncomingDAO;
    ModelPurcaseOrderDAO modelPurcaseOrderDAO;
    ModelDAO modelDAO;
    WarehouseOutgoingDAO warehouseOutgoingDAO;
    PurcaseOrderDAO purcaseOrderDAO;

    public WarehouseResource(AuthUtil authUtil, CompanyDAO companyDAO, WarehouseDAO warehouseDAO, StockDetailDAO stockDetailDAO, StockDAO stockDAO, WarehouseIncomingDAO warehouseIncomingDAO, ModelPurcaseOrderDAO modelPurcaseOrderDAO, ModelDAO modelDAO, WarehouseOutgoingDAO warehouseOutgoingDAO, PurcaseOrderDAO purcaseOrderDAO) {
        this.authUtil = authUtil;
        this.companyDAO = companyDAO;
        this.warehouseDAO = warehouseDAO;
        this.stockDetailDAO = stockDetailDAO;
        this.stockDAO = stockDAO;
        this.warehouseIncomingDAO = warehouseIncomingDAO;
        this.modelPurcaseOrderDAO = modelPurcaseOrderDAO;
        this.modelDAO = modelDAO;
        this.warehouseOutgoingDAO = warehouseOutgoingDAO;
        this.purcaseOrderDAO = purcaseOrderDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.WAREHOUSE_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String address = data.get("address");
                String phone = data.get("phone");
                String warehouseRepresentative = data.get("warehouseRepresentative");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                Warehouse warehouse = warehouseDAO.getByCode(code, company);

                if (warehouse == null) {
                    warehouse = new Warehouse();
                    warehouse.setName(name);
                    warehouse.setCode(code);
                    warehouse.setDescription(description);
                    warehouse.setWarehouseRepresentative(warehouseRepresentative);
                    warehouse.setPhone(phone);
                    warehouse.setAddress(address);
                    warehouse.setCompany(company);
                    warehouse.setUpdateDate(new DateTime());
                    warehouse.setUpdater(userToken.getUser());
                    warehouseDAO.create(warehouse);

                    authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), warehouse.getName() + " adlı depo oluşturuldu. ", new DateTime());

                    return Response.ok(warehouse).build();
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.WAREHOUSE_READ)) {
                Company company = userToken.getUser().getCompany();

                List<Warehouse> warehouseList = warehouseDAO.getAll(company);

                return Response.ok().entity(warehouseList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }


    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYACI_READ)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();
                Warehouse warehouse = warehouseDAO.getById(oid, company);

                return Response.ok().entity(warehouse).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.WAREHOUSE_UPDATE)) {
                String oid = data.get("oid");
                String name = data.get("name");
                String code = data.get("code");
                String address = data.get("address");
                String phone = data.get("phone");
                String warehouseRepresentative = data.get("warehouseRepresentative");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                Warehouse warehouse = warehouseDAO.getById(oid, company);

                Warehouse warehouseByCode = warehouseDAO.getByCode(code, company);

                if (warehouseByCode != null && !warehouseByCode.getCode().equals(warehouse.getCode())) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    warehouse.setName(name);
                    warehouse.setCode(code);
                    warehouse.setDescription(description);
                    warehouse.setWarehouseRepresentative(warehouseRepresentative);
                    warehouse.setPhone(phone);
                    warehouse.setAddress(address);
                    warehouse.setCompany(company);
                    warehouse.setUpdateDate(new DateTime());
                    warehouse.setUpdater(userToken.getUser());
                    warehouseDAO.update(warehouse);

                    authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), warehouse.getName() + " adlı depo güncellendi. ", new DateTime());

                    return Response.ok(warehouse).build();
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.WAREHOUSE_DELETE)) {
                Company company = userToken.getUser().getCompany();
                String oid = data.get("oid");

                Warehouse warehouse = warehouseDAO.getById(oid, company);
                warehouse.setDeleteStatus(true);

                warehouseDAO.update(warehouse);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), warehouse.getName() + " adlı depo silindi. ", new DateTime());

                return Response.ok(warehouse).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("stockDetailIncrease")
    @UnitOfWork
    public Response stockDetailIncrease(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_INCREASE)) {
                String poNumber = data.get("poNumber");
                String details = data.get("details");
                String sOid = data.get("stock");

                Company company = userToken.getUser().getCompany();

                JSONArray jsonArray = new JSONArray(details);

                Stock stock = null;
                //  Stok detayı varsa
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String stockOid = jsonObject.getString("stockOid");
                    String stockDetailOid = jsonObject.getString("stockDetailOid");
                    String quantity = jsonObject.getString("quantity");

                    Double q = 0.0;

                    if (quantity != null && !quantity.equals("")) {
                        q = Double.valueOf(quantity);
                    }

                    stock = stockDAO.getById(stockOid, company);
                    StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);
                    ModelPurcaseOrder modelPurcaseOrder = modelPurcaseOrderDAO.getByPONumber(poNumber, company);

                    //  Transaction kaydediliyor
                    WarehouseIncoming warehouseIncoming = new WarehouseIncoming();
                    warehouseIncoming.setCompany(company);
                    warehouseIncoming.setUpdater(userToken.getUser());
                    warehouseIncoming.setUpdateDate(new DateTime());
                    warehouseIncoming.setStock(stock);
                    warehouseIncoming.setStockDetail(stockDetail);
                    warehouseIncoming.setModelPurcaseOrder(modelPurcaseOrder);
                    warehouseIncoming.setIncomingQuantity(q);

                    warehouseIncomingDAO.create(warehouseIncoming);

                    //  Stok detaylarının miktarları güncelleniyor
                    stockDetail.setQuantity(stockDetail.getQuantity() + q);
                    stockDetailDAO.update(stockDetail);
                }

                List<StockDetail> stockDetails = stockDetailDAO.getByStock(stock, company);
                Double stockQuantity = 0.0;
                for (StockDetail stockDetail : stockDetails) {
                    stockQuantity += stockDetail.getQuantity();
                }

                //  Stok miktarı güncelleniyor
                stock.setQuantity(stockQuantity);
                stockDAO.update(stock);

                return Response.ok().entity(company).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncreases")
    @UnitOfWork
    public Response readIncreases(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_INCREASE)) {
                List<WarehouseIncoming> warehouseIncomings = warehouseIncomingDAO.getAll(userToken.getUser().getCompany());
                List<WarehouseIncomingSummaryDTO> warehouseIncomingSummaryDTOs = new LinkedList<>();

                for (WarehouseIncoming warehouseIncoming : warehouseIncomings) {
                    WarehouseIncomingSummaryDTO warehouseIncomingSummaryDTO = new WarehouseIncomingSummaryDTO(
                            warehouseIncoming.getOid(),
                            warehouseIncoming.getStock().getSupplier() != null ? warehouseIncoming.getStock().getSupplier().getName() : "",
                            warehouseIncoming.getStock() != null ? warehouseIncoming.getStock().getCode() : "",
                            warehouseIncoming.getStock() != null ? warehouseIncoming.getStock().getName() : "",
                            warehouseIncoming.getStockDetail() != null ? warehouseIncoming.getStockDetail().getName() : "",
                            warehouseIncoming.getIncomingQuantity(),
                            warehouseIncoming.getModelPurcaseOrder() != null ? warehouseIncoming.getModelPurcaseOrder().getPoNumber() : ""
                    );

                    warehouseIncomingSummaryDTOs.add(warehouseIncomingSummaryDTO);
                }

                return Response.ok().entity(warehouseIncomingSummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteIncrease")
    @UnitOfWork
    public Response deleteIncrease(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_INCREASE)) {
                String increaseOid = data.get("oid");

                WarehouseIncoming warehouseIncoming = warehouseIncomingDAO.getByOid(increaseOid, userToken.getUser().getCompany());

                StockDetail stockDetail = warehouseIncoming.getStockDetail();
                Stock stock = warehouseIncoming.getStock();

                if (stockDetail != null) {
                    stockDetail.setQuantity(stockDetail.getQuantity() - warehouseIncoming.getIncomingQuantity());
                    stockDetailDAO.update(stockDetail);

                    stock.setQuantity(stock.getQuantity() - warehouseIncoming.getIncomingQuantity());
                    stockDAO.update(stock);
                } else {
                    stock.setQuantity(stock.getQuantity() - warehouseIncoming.getIncomingQuantity());
                    stockDAO.update(stock);
                }

                warehouseIncoming.setDeleteStatus(true);
                warehouseIncomingDAO.update(warehouseIncoming);

                return Response.ok().entity(warehouseIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("stockIncrease")
    @UnitOfWork
    public Response stockIncrease(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_INCREASE)) {
                String poNumber = data.get("poNumber");
                String quantity = data.get("quantity");
                String stockOid = data.get("stockOid");

                Company company = userToken.getUser().getCompany();
                Stock stock = stockDAO.getById(stockOid, company);
                ModelPurcaseOrder modelPurcaseOrder = modelPurcaseOrderDAO.getByPONumber(poNumber, company);

                Double q = 0.0;

                if (quantity != null && !quantity.equals("")) {
                    q = Double.valueOf(quantity);
                }

                //  Transaction kaydediliyor
                WarehouseIncoming warehouseIncoming = new WarehouseIncoming();
                warehouseIncoming.setCompany(company);
                warehouseIncoming.setUpdater(userToken.getUser());
                warehouseIncoming.setUpdateDate(new DateTime());
                warehouseIncoming.setStock(stock);
                warehouseIncoming.setModelPurcaseOrder(modelPurcaseOrder);
                warehouseIncoming.setIncomingQuantity(q);

                warehouseIncomingDAO.create(warehouseIncoming);

                //  Stok miktarı güncelleniyor
                stock.setQuantity(stock.getQuantity() + q);
                stockDAO.update(stock);

                return Response.ok().entity(company).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("stockDecrease")
    @UnitOfWork
    public Response stockDecrease(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_DECREASE)) {
                String modelOid = data.get("modelOid");
                String details = data.get("details");
                String type = data.get("type");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);
                if (type.equals("cutting")) {
                    JSONArray jsonArray = new JSONArray(details);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String stockOid = jsonObject.getString("stockOid");
                        String stockDetailOid = jsonObject.getString("stockDetailOid");
                        String calculatedRequirement = jsonObject.getString("calculatedRequirement");

                        Stock stock = stockDAO.getById(stockOid, company);
                        StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);

                        WarehouseOutgoing warehouseOutgoing = new WarehouseOutgoing();
                        warehouseOutgoing.setCompany(company);
                        warehouseOutgoing.setUpdater(userToken.getUser());
                        warehouseOutgoing.setUpdateDate(new DateTime());
                        warehouseOutgoing.setModel(model);
                        warehouseOutgoing.setOutgoingQuantity(Double.valueOf(calculatedRequirement));
                        warehouseOutgoing.setStockDetail(stockDetail);
                        warehouseOutgoing.setStock(stock);
                        warehouseOutgoing.setOutgoingType(type);

                        warehouseOutgoingDAO.create(warehouseOutgoing);

                        //  Stok detay miktarı gücelleniyor
                        if (stockDetail != null) {
                            stockDetail.setQuantity(stockDetail.getQuantity() - Double.valueOf(calculatedRequirement));
                            stockDetailDAO.update(stockDetail);
                        }

                        //  Stok miktarı güncelleniyor
                        stock.setQuantity(stock.getQuantity() - Double.valueOf(calculatedRequirement));
                        stockDAO.update(stock);
                    }

                    return Response.ok().entity(company).build();
                } else if (type.equals("monta")) {
                    String assortArray = data.get("assortArray");

                    JSONArray jsonArray = new JSONArray(details);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String stockOid = jsonObject.getString("stockOid");
                        String stockDetailOid = jsonObject.getString("stockDetailOid");
                        String calculatedRequirement = jsonObject.getString("calculatedRequirement");

                        Stock stock = stockDAO.getById(stockOid, company);
                        StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);

                        WarehouseOutgoing warehouseOutgoing = new WarehouseOutgoing();
                        warehouseOutgoing.setCompany(company);
                        warehouseOutgoing.setUpdater(userToken.getUser());
                        warehouseOutgoing.setUpdateDate(new DateTime());
                        warehouseOutgoing.setModel(model);
                        warehouseOutgoing.setOutgoingQuantity(Double.valueOf(calculatedRequirement));
                        warehouseOutgoing.setStockDetail(stockDetail);
                        warehouseOutgoing.setStock(stock);
                        warehouseOutgoing.setOutgoingType(type);

                        warehouseOutgoingDAO.create(warehouseOutgoing);

                        //  Stok detay miktarı gücelleniyor
                        if (stockDetail != null) {
                            stockDetail.setQuantity(stockDetail.getQuantity() - Double.valueOf(calculatedRequirement));
                            stockDetailDAO.update(stockDetail);
                        }

                        //  Stok miktarı güncelleniyor
                        stock.setQuantity(stock.getQuantity() - Double.valueOf(calculatedRequirement));
                        stockDAO.update(stock);

                        if (stock.getStockType().getName().equals("TABAN")) {
                            JSONArray ja = new JSONArray(assortArray);

                            for (int j = 0; j < ja.length(); j++) {
                                JSONObject js = ja.getJSONObject(j);

                                String number = js.getString("number");
                                String quantity = js.getString("quantity");

                                StockDetail sd = stockDetailDAO.getByNameAndStock(number, stock, company);
                                if (sd != null) {
                                    sd.setQuantity(sd.getQuantity() - Double.valueOf(quantity));
                                    stockDetailDAO.update(sd);
                                }
                            }
                        }
                    }

                    return Response.ok().entity(company).build();
                } else {
                    String quantity = data.get("quantity");
                    String stockOid = data.get("stockOid");

                    Stock stock = stockDAO.getById(stockOid, company);

                    Double q = 0.0;

                    if (quantity != null && !quantity.equals("")) {
                        q = Double.valueOf(quantity);
                    }

                    //  Transaction kaydediliyor
                    WarehouseOutgoing warehouseOutgoing = new WarehouseOutgoing();
                    warehouseOutgoing.setCompany(company);
                    warehouseOutgoing.setUpdater(userToken.getUser());
                    warehouseOutgoing.setUpdateDate(new DateTime());
                    warehouseOutgoing.setModel(model);
                    warehouseOutgoing.setOutgoingQuantity(q);
                    warehouseOutgoing.setStock(stock);
                    warehouseOutgoing.setOutgoingType(type);

                    warehouseOutgoingDAO.create(warehouseOutgoing);

                    //  Stok miktarı güncelleniyor
                    stock.setQuantity(stock.getQuantity() - q);
                    stockDAO.update(stock);

                    return Response.ok().entity(warehouseOutgoing).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteDecrease")
    @UnitOfWork
    public Response deleteDecrease(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_DECREASE)) {
                String modelOid = data.get("modelOid");
                String details = data.get("details");
                String type = data.get("type");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);

                if (type.equals("cutting")) {
                    JSONArray jsonArray = new JSONArray(details);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String stockOid = jsonObject.getString("stockOid");
                        String stockDetailOid = jsonObject.getString("stockDetailOid");
                        String calculatedRequirement = jsonObject.getString("calculatedRequirement");

                        Stock stock = stockDAO.getById(stockOid, company);
                        StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);

                        List<WarehouseOutgoing> warehouseOutgoing = warehouseOutgoingDAO.getByOutgoingTypeAndModelStock(type, model, stock, company);

                        for (WarehouseOutgoing outgoing : warehouseOutgoing) {
                            outgoing.setDeleteStatus(true);
                            warehouseOutgoingDAO.update(outgoing);

                            //  Stok detay miktarı gücelleniyor
                            if (stockDetail != null) {
                                stockDetail.setQuantity(stockDetail.getQuantity() + outgoing.getOutgoingQuantity());
                                stockDetailDAO.update(stockDetail);
                            }

                            //  Stok miktarı güncelleniyor
                            stock.setQuantity(stock.getQuantity() + outgoing.getOutgoingQuantity());
                            stockDAO.update(stock);
                        }
                    }

                    return Response.ok().entity(company).build();
                } else if (type.equals("monta")) {
                    String assortArray = data.get("assortArray");

                    JSONArray jsonArray = new JSONArray(details);

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String stockOid = jsonObject.getString("stockOid");
                        String stockDetailOid = jsonObject.getString("stockDetailOid");
                        String calculatedRequirement = jsonObject.getString("calculatedRequirement");

                        Stock stock = stockDAO.getById(stockOid, company);
                        StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);

                        List<WarehouseOutgoing> warehouseOutgoing = warehouseOutgoingDAO.getByOutgoingTypeAndModelStock(type, model, stock, company);
                        for (WarehouseOutgoing outgoing : warehouseOutgoing) {
                            outgoing.setDeleteStatus(true);
                            warehouseOutgoingDAO.update(outgoing);

                            //  Stok detay miktarı gücelleniyor
                            if (stockDetail != null) {
                                stockDetail.setQuantity(stockDetail.getQuantity() + outgoing.getOutgoingQuantity());
                                stockDetailDAO.update(stockDetail);
                            }

                            //  Stok miktarı güncelleniyor
                            stock.setQuantity(stock.getQuantity() + outgoing.getOutgoingQuantity());
                            stockDAO.update(stock);

                            //  Stok detay miktarı gücelleniyor
                            if (stock.getStockType().getName().equals("TABAN")) {
                                JSONArray ja = new JSONArray(assortArray);

                                for (int j = 0; j < ja.length(); j++) {
                                    JSONObject js = ja.getJSONObject(j);

                                    String number = js.getString("number");
                                    String quantity = js.getString("quantity");
                                    System.out.println(js.toString());

                                    StockDetail sd = stockDetailDAO.getByNameAndStock(number, stock, company);
                                    if (sd != null) {
                                        sd.setQuantity(sd.getQuantity() + Double.valueOf(quantity));
                                        stockDetailDAO.update(sd);
                                    }
                                }
                            }
                        }
                    }

                    return Response.ok().entity(company).build();
                } else {
                    String decreaseOid = data.get("oid");

                    WarehouseOutgoing warehouseOutgoing = warehouseOutgoingDAO.getByOid(decreaseOid, userToken.getUser().getCompany());

                    StockDetail stockDetail = warehouseOutgoing.getStockDetail();
                    Stock stock = warehouseOutgoing.getStock();

                    if (stockDetail != null) {
                        stockDetail.setQuantity(stockDetail.getQuantity() + warehouseOutgoing.getOutgoingQuantity());
                        stockDetailDAO.update(stockDetail);

                        stock.setQuantity(stock.getQuantity() + warehouseOutgoing.getOutgoingQuantity());
                        stockDAO.update(stock);
                    } else {
                        stock.setQuantity(stock.getQuantity() + warehouseOutgoing.getOutgoingQuantity());
                        stockDAO.update(stock);
                    }

                    warehouseOutgoing.setDeleteStatus(true);
                    warehouseOutgoingDAO.update(warehouseOutgoing);

                    return Response.ok().entity(warehouseOutgoing).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readWarehouseOutgoings")
    @UnitOfWork
    public Response readWarehouseOutgoings(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_DECREASE)) {
                String modelOid = data.get("modelOid");
                String type = data.get("type");

                Company company = userToken.getUser().getCompany();
                Model model = modelDAO.getByOid(modelOid, company);

                List<WarehouseOutgoing> warehouseOutgoings = warehouseOutgoingDAO.getByOutgoingTypeAndModel(type, model, company);

                return Response.ok().entity(warehouseOutgoings).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("stockDetailDecrease")
    @UnitOfWork
    public Response stockDetailDecrease(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_DECREASE)) {
                String details = data.get("details");

                Company company = userToken.getUser().getCompany();

                JSONArray jsonArray = new JSONArray(details);

                Stock stock = null;
                //  Stok detayı varsa
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String stockOid = jsonObject.getString("stockOid");
                    String stockDetailOid = jsonObject.getString("stockDetailOid");
                    String quantity = jsonObject.getString("quantity");

                    Double q = 0.0;

                    if (quantity != null && !quantity.equals("")) {
                        q = Double.valueOf(quantity);
                    }

                    stock = stockDAO.getById(stockOid, company);
                    StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);

                    //  Transaction kaydediliyor
                    WarehouseOutgoing warehouseOutgoing = new WarehouseOutgoing();
                    warehouseOutgoing.setCompany(company);
                    warehouseOutgoing.setUpdater(userToken.getUser());
                    warehouseOutgoing.setUpdateDate(new DateTime());
                    warehouseOutgoing.setStock(stock);
                    warehouseOutgoing.setStockDetail(stockDetail);
                    warehouseOutgoing.setOutgoingQuantity(q);
                    warehouseOutgoing.setOutgoingType("normal");

                    warehouseOutgoingDAO.create(warehouseOutgoing);

                    //  Stok detaylarının miktarları güncelleniyor
                    stockDetail.setQuantity(stockDetail.getQuantity() - q);
                    stockDetailDAO.update(stockDetail);
                }

                List<StockDetail> stockDetails = stockDetailDAO.getByStock(stock, company);
                Double stockQuantity = 0.0;
                for (StockDetail stockDetail : stockDetails) {
                    stockQuantity += stockDetail.getQuantity();
                }

                //  Stok miktarı güncelleniyor
                stock.setQuantity(stockQuantity);
                stockDAO.update(stock);

                return Response.ok().entity(company).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDecreases")
    @UnitOfWork
    public Response readDecreases(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_DECREASE)) {
                String type = data.get("type");

                List<WarehouseOutgoing> warehouseOutgoings = warehouseOutgoingDAO.getByOutgoingType(type, userToken.getUser().getCompany());
                List<WarehouseOutgoingSummaryDTO> warehouseOutgoingSummaryDTOs = new LinkedList<>();

                for (WarehouseOutgoing warehouseOutgoing : warehouseOutgoings) {
                    WarehouseOutgoingSummaryDTO warehouseOutgoingSummaryDTO = new WarehouseOutgoingSummaryDTO(
                            warehouseOutgoing.getOid(),
                            warehouseOutgoing.getStock().getSupplier() != null ? warehouseOutgoing.getStock().getSupplier().getName() : "",
                            warehouseOutgoing.getStock() != null ? warehouseOutgoing.getStock().getCode() : "",
                            warehouseOutgoing.getStock() != null ? warehouseOutgoing.getStock().getName() : "",
                            warehouseOutgoing.getStockDetail() != null ? warehouseOutgoing.getStockDetail().getName() : "",
                            warehouseOutgoing.getOutgoingQuantity(),
                            warehouseOutgoing.getModel() != null ? warehouseOutgoing.getModel().getModelCode() : "",
                            warehouseOutgoing.getModel() != null ? warehouseOutgoing.getModel().getCustomerModelCode() : ""
                    );

                    warehouseOutgoingSummaryDTOs.add(warehouseOutgoingSummaryDTO);
                }

                return Response.ok().entity(warehouseOutgoingSummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
