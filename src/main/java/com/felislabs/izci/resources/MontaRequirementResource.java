package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.ModelMonta;
import com.felislabs.izci.representation.entities.MontaRequirement;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by dogukanyilmaz on 04/05/2017.
 */
@Path("montaRequirement")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MontaRequirementResource {
    AuthUtil authUtil;
    CompanyDAO companyDAO;
    MontaRequirementDAO montaRequirementDAO;
    ModelMontaDAO modelMontaDAO;
    StockDAO stockDAO;
    StockDetailDAO stockDetailDAO;

    public MontaRequirementResource(AuthUtil authUtil, CompanyDAO companyDAO, MontaRequirementDAO montaRequirementDAO, ModelMontaDAO modelMontaDAO, StockDAO stockDAO, StockDetailDAO stockDetailDAO) {
        this.authUtil = authUtil;
        this.companyDAO = companyDAO;
        this.montaRequirementDAO = montaRequirementDAO;
        this.modelMontaDAO = modelMontaDAO;
        this.stockDAO = stockDAO;
        this.stockDetailDAO = stockDetailDAO;
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_REQUIREMENT_READ)) {
                List<MontaRequirement> montaRequirements = montaRequirementDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(montaRequirements).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_REQUIREMENT_READ)) {
                String montaRequirementOid = data.get("oid");

                MontaRequirement montaRequirement = montaRequirementDAO.getByOid(montaRequirementOid, userToken.getUser().getCompany());

                return Response.ok().entity(montaRequirement).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByModelRequirements")
    @UnitOfWork
    public Response getByModelRequirements(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MONTA_REQUIREMENT_READ)) {
                String documentNumber = data.get("documentNumber");
                ModelMonta modelMonta = modelMontaDAO.getByDocumentNumber(documentNumber,userToken.getUser().getCompany());

                List<MontaRequirement> montaRequirements = montaRequirementDAO.getByModelMonta(modelMonta,userToken.getUser().getCompany());

                return Response.ok().entity(montaRequirements).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

}
