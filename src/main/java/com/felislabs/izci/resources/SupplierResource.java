package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.SupplierDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Supplier;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by aykutarici on 06/12/16.
 */
@Path("/supplier")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SupplierResource {
    SupplierDAO supplierDAO;
    AuthUtil authUtil;

    public SupplierResource(SupplierDAO supplierDAO, AuthUtil authUtil) {
        this.supplierDAO = supplierDAO;
        this.authUtil = authUtil;
    }

    @POST
    @Path("get")
    @UnitOfWork
    public Response getAll(Map<String, String> suppliersData) {
        String token = suppliersData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(supplierDAO.getAll(Supplier.class, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_READ)) {
                String oid = data.get("oid");
                Supplier supplier = supplierDAO.getById(oid, userToken.getUser().getCompany());
                return Response.ok(supplier).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> suppliersData) {
        String token = suppliersData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_CREATE)) {

                String code = suppliersData.get("code");
                String name = suppliersData.get("name");
                String phone = suppliersData.get("phone");
                String email = suppliersData.get("email");
                String address = suppliersData.get("address");
                String description = suppliersData.get("description");

                Company company = userToken.getUser().getCompany();
                Supplier supplier = supplierDAO.getByCode(code, company);

                if (supplier == null) {
                    supplier = new Supplier();

                    supplier.setCompany(company);
                    supplier.setName(name);
                    supplier.setCode(code);
                    supplier.setPhone(phone);
                    supplier.setEmail(email);
                    supplier.setAddress(address);
                    supplier.setDescription(description);
                    supplier.setUpdater(userToken.getUser());
                    supplier.setUpdateDate(new DateTime());

                    supplierDAO.create(supplier);

                    authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), supplier.getCode() + " Başlıklı tedarikçi oluşturuldu. ", new DateTime());


                    return Response.ok(supplier).build();
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_SUPPLIER.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> suppliersData) {
        String token = suppliersData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_UPDATE)) {
                Company company = userToken.getUser().getCompany();

                String oid = suppliersData.get("oid");
                String code = suppliersData.get("code");
                String name = suppliersData.get("name");
                String phone = suppliersData.get("phone");
                String email = suppliersData.get("email");
                String address = suppliersData.get("address");
                String description = suppliersData.get("description");


                String oldCode = supplierDAO.getById(oid, company).getCode();
                Supplier codeControl = supplierDAO.getByCode(code, company);

                if (codeControl != null && codeControl.getCode() != oldCode) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_SUPPLIER.getErrorAlias()).build();
                }

                Supplier supplier = supplierDAO.getById(oid, company);

                supplier.setCompany(company);
                supplier.setName(name);
                supplier.setCode(code);
                supplier.setPhone(phone);
                supplier.setEmail(email);
                supplier.setAddress(address);
                supplier.setDescription(description);
                supplier.setUpdater(userToken.getUser());
                supplier.setUpdateDate(new DateTime());

                supplierDAO.update(supplier);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), supplier.getCode() + " Başlıklı tedarikçi güncellendi. ", new DateTime());

                return Response.ok(supplier).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> supplierData) {
        String token = supplierData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_DELETE)) {
                Company company = userToken.getUser().getCompany();
                String oid = supplierData.get("oid");
                Supplier supplier = supplierDAO.getById(oid, company);
                supplierDAO.delete(supplier);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), supplier.getCode() + " Başlıklı tedarikçi silindi. ", new DateTime());

                return Response.ok(supplier).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getById")
    @UnitOfWork
    public Response getById(Map<String, String> supplierData) {
        String token = supplierData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_READ)) {
                String oid = supplierData.get("oid");
                Company company = userToken.getUser().getCompany();
                return Response.ok(supplierDAO.getById(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByNameContains")
    @UnitOfWork
    public Response getByNameContains(Map<String, String> supplierData) {
        String token = supplierData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_READ)) {
                String name = supplierData.get("name");
                Company company = userToken.getUser().getCompany();
                List<Supplier> suppliers = supplierDAO.getByNameContains(name, company);


                return Response.ok(suppliers).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
    @POST
    @Path("getByCodeContains")
    @UnitOfWork
    public Response getByCodeContains(Map<String, String> supplierData) {
        String token = supplierData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SUPPLIER_READ)) {
                String code = supplierData.get("code");
                Company company = userToken.getUser().getCompany();
                List<Supplier> suppliers = supplierDAO.getByCodeContains(code, company);


                return Response.ok(suppliers).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
