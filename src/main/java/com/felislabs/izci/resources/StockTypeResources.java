package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.StockTypeDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.StockType;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by aykutarici on 26/12/16.
 */
@Path("/stockType")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StockTypeResources {
    AuthUtil authUtil;
    StockTypeDAO stockTypeDAO;

    public StockTypeResources(AuthUtil authUtil, StockTypeDAO stockTypeDAO) {
        this.authUtil = authUtil;
        this.stockTypeDAO = stockTypeDAO;
    }

    @POST
    @Path("get")
    @UnitOfWork
    public Response getAll(Map<String, String> stockTypesData) {
        String token = stockTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_TYPE_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(stockTypeDAO.getAll(StockType.class, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_TYPE_READ)) {
                String oid = data.get("oid");
                StockType stockType = stockTypeDAO.getById(oid, userToken.getUser().getCompany());
                return Response.ok(stockType).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> stockTypesData) {
        String token = stockTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_TYPE_CREATE)) {
                String name = stockTypesData.get("name");
                String code = stockTypesData.get("code");
                String description = stockTypesData.get("description");

                Company company = userToken.getUser().getCompany();
                StockType stockType = stockTypeDAO.getByCode(code, company);

                if (stockType == null) {
                    stockType = new StockType();

                    stockType.setName(name);
                    stockType.setCode(code);
                    stockType.setDescription(description);
                    stockType.setCompany(company);
                    stockType.setUpdateDate(new DateTime());
                    stockType.setUpdater(userToken.getUser());
                    stockTypeDAO.create(stockType);

                    authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), stockType.getName() + " başlıklı stok tipi oluşturuldu. ", new DateTime());


                    return Response.ok(stockType).build();
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_STOCK_TYPE.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> stockTypesData) {
        String token = stockTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_TYPE_UPDATE)) {
                Company company = userToken.getUser().getCompany();

                String oid = stockTypesData.get("oid");
                String name = stockTypesData.get("name");
                String code = stockTypesData.get("code");
                String description = stockTypesData.get("description");

                String oldTitle = stockTypeDAO.getById(oid, company).getCode();
                StockType codeControl = stockTypeDAO.getByCode(code, company);

                if (codeControl != null && codeControl.getCode() != oldTitle) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_STOCK_TYPE.getErrorAlias()).build();
                }

                StockType stockType = stockTypeDAO.getById(oid, company);

                stockType.setName(name);
                stockType.setCode(code);
                stockType.setDescription(description);
                stockType.setCompany(company);
                stockType.setUpdateDate(new DateTime());
                stockType.setUpdater(userToken.getUser());

                stockTypeDAO.update(stockType);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), stockType.getName() + " başlıklı stok tipi güncellendi. ", new DateTime());

                return Response.ok(stockType).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> stockTypesData) {
        String token = stockTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_TYPE_DELETE)) {
                Company company = userToken.getUser().getCompany();
                String oid = stockTypesData.get("oid");
                StockType stockTypes = stockTypeDAO.getById(oid, company);
                stockTypeDAO.delete(stockTypes);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), stockTypes.getName() + " başlıklı stok tipi silindi. ", new DateTime());

                return Response.ok(stockTypes).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getById")
    @UnitOfWork
    public Response getById(Map<String, String> stockTypesData) {
        String token = stockTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_TYPE_READ)) {
                String oid = stockTypesData.get("oid");
                Company company = userToken.getUser().getCompany();
                return Response.ok(stockTypeDAO.getById(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByNameContains")
    @UnitOfWork
    public Response getByNameContains(Map<String, String> stockTypesData) {
        String token = stockTypesData.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_TYPE_READ)) {
                String name = stockTypesData.get("name");
                Company company = userToken.getUser().getCompany();
                List<StockType> stockTypes = stockTypeDAO.getByNameContains(name, company);

                return Response.ok(stockTypes).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByCodeContains")
    @UnitOfWork
    public Response getByCodeContains(Map<String, String> stockTypesData) {
        String token = stockTypesData.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_TYPE_READ)) {
                String code = stockTypesData.get("code");
                Company company = userToken.getUser().getCompany();
                List<StockType> stockTypes = stockTypeDAO.getByNameContains(code, company);
                return Response.ok(stockTypes).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


}
