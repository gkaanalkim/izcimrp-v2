package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.WorkOrderImageDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import net.coobird.thumbnailator.Thumbnails;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.*;

/**
 * Created by gunerkaanalkim on 09/02/2017.
 */
@Path("company")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CompanyResource {
    AuthUtil authUtil;
    CompanyDAO companyDAO;
    MenuDAO menuDAO;
    RoleDAO roleDAO;
    PermissionDAO permissionDAO;
    RoleUserDAO roleUserDAO;
    RolePermissionDAO rolePermissionDAO;
    RoleMenuDAO roleMenuDAO;
    MrpSettingDAO mrpSettingDAO;
    UserDAO userDAO;
    MaterialTypeDAO materialTypeDAO;
    StockTypeDAO stockTypeDAO;
    SupplierDAO supplierDAO;
    WarehouseDAO warehouseDAO;

    public CompanyResource(
            AuthUtil authUtil,
            CompanyDAO companyDAO,
            MenuDAO menuDAO,
            RoleDAO roleDAO,
            PermissionDAO permissionDAO,
            RoleUserDAO roleUserDAO,
            RolePermissionDAO rolePermissionDAO,
            RoleMenuDAO roleMenuDAO,
            MrpSettingDAO mrpSettingDAO,
            UserDAO userDAO,
            MaterialTypeDAO materialTypeDAO,
            StockTypeDAO stockTypeDAO,
            SupplierDAO supplierDAO,
            WarehouseDAO warehouseDAO
    ) {
        this.authUtil = authUtil;
        this.companyDAO = companyDAO;
        this.menuDAO = menuDAO;
        this.roleDAO = roleDAO;
        this.permissionDAO = permissionDAO;
        this.roleUserDAO = roleUserDAO;
        this.rolePermissionDAO = rolePermissionDAO;
        this.roleMenuDAO = roleMenuDAO;
        this.mrpSettingDAO = mrpSettingDAO;
        this.userDAO = userDAO;
        this.materialTypeDAO = materialTypeDAO;
        this.stockTypeDAO = stockTypeDAO;
        this.supplierDAO = supplierDAO;
        this.warehouseDAO = warehouseDAO;
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String licenseStartDate = data.get("licenseStartDate");
                String bankInfo = data.get("bankInfo");
                String language = data.get("language");
                String logoUrl = data.get("logoUrl");
                String address = data.get("address");
                String phone = data.get("phone");
                String fax = data.get("fax");
                String email = data.get("email");
                String website = data.get("website");
                String facebook = data.get("facebook");
                String twitter = data.get("twitter");
                String instagram = data.get("instagram");
                String orderSpecialNotes = data.get("orderSpecialNotes");
                String project = data.get("p");
                User user = userDAO.getByEmail(email, userToken.getUser().getCompany());
                if (user == null) {

                    if (project.equals("1")) {
                        project = "MRP";
                    } else if (project.equals("2")) {
                        project = "SOLE";
                    } else if (project.equals("3")) {
                        project = "CRM";
                    } else if (project.equals("4")) {
                        project = "FINANCE";
                    }
                    org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                    DateTime licenseStartDateFormatted = formatter.parseDateTime(licenseStartDate);

                    Company company = companyDAO.findByCode(code);

                    if (company != null) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        company = new Company();
                        company.setName(name);
                        company.setCode(code);
                        company.setLicenseStartDate(licenseStartDateFormatted);
                        company.setBankInfo(bankInfo);
                        company.setLanguage(language);
                        company.setLogoUrl(logoUrl);
                        company.setAddress(address);
                        company.setPhone(phone);
                        company.setFax(fax);
                        company.setEmail(email);
                        company.setWebsite(website);
                        company.setFacebook(facebook);
                        company.setTwitter(twitter);
                        company.setInstagram(instagram);
                        company.setOrderSpecialNotes(orderSpecialNotes);

                        companyDAO.create(company);

                        MrpSetting mrpSetting = new MrpSetting();
                        mrpSetting.setCuttingOrderCounter(1);
                        mrpSetting.setWorkOrderCount(1);
                        mrpSetting.setStockCodeCounter(1);
                        mrpSetting.setMontaOrderCounter(1);
                        mrpSetting.setCompany(company);

                        mrpSettingDAO.create(mrpSetting);

                        //  Admin User Creating
                        user = new User();
                        user.setName(company.getName());
                        user.setSurname(" Admin");
                        user.setCode(company.getCode());
                        user.setCompany(company);
                        user.setEmail(company.getEmail());
                        user.setPassword("03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4"); //1234

                        userDAO.create(user);

                        Warehouse warehouse=new Warehouse();
                        warehouse.setCompany(company);
                        warehouse.setAddress(company.getAddress());
                        warehouse.setCode(company.getCode());
                        warehouse.setPhone(company.getPhone());
                        warehouse.setName(company.getName());
                        warehouseDAO.create(warehouse);

                        //  Admin Role Creating
                        Role role = new Role();
                        role.setCompany(company);
                        role.setName(company.getCode() + "-Admin-Role");
                        role.setCode(company.getCode() + "-AdmR");

                        roleDAO.create(role);

                        //  Admin RoleUser Creating
                        RoleUser roleUser = new RoleUser();
                        roleUser.setCompany(company);
                        roleUser.setRole(role);
                        roleUser.setUser(user);
                        roleUserDAO.create(roleUser);

                        List<Permission> permissions = permissionDAO.getAll(project, Permission.class, company);
                        for (int i = 0; i < permissions.size(); i++) {
                            Permission permission = permissions.get(i);
                            if (!permission.getCode().equals("CompanyCreate") && !permission.getCode().equals("CompanyUpdate") && !permission.getCode().equals("CompanyDelete")) {
                                RolePermission rolePermission = new RolePermission();
                                rolePermission.setRole(role);
                                rolePermission.setPermission(permission);
                                rolePermission.setCompany(company);
                                rolePermissionDAO.create(rolePermission);
                            }
                        }

                        List<Menu> menus = menuDAO.getAllByProject(project);
                        for (int i = 0; i < menus.size(); i++) {
                            Menu menu = menus.get(i);
                            if (!menu.getCode().equals("Company")) {
                                RoleMenu roleMenu = new RoleMenu();
                                roleMenu.setCompany(company);
                                roleMenu.setRole(role);
                                roleMenu.setMenu(menu);
                                roleMenuDAO.create(roleMenu);
                            }

                            MaterialType materialTypeKutu = new MaterialType();
                            materialTypeKutu = new MaterialType();
                            materialTypeKutu.setTitle("KUTU");
                            materialTypeKutu.setGroupName("AKSESUAR");
                            materialTypeKutu.setUpdater(userToken.getUser());
                            materialTypeKutu.setCompany(company);
                            materialTypeKutu.setUpdateDate(new DateTime());
                            materialTypeDAO.create(materialTypeKutu);

                            MaterialType materialTypeKoli = new MaterialType();
                            materialTypeKoli.setTitle("KOLİ");
                            materialTypeKoli.setGroupName("AKSESUAR");
                            materialTypeKoli.setCompany(company);
                            materialTypeKoli.setUpdateDate(new DateTime());
                            materialTypeKoli.setUpdater(userToken.getUser());
                            materialTypeDAO.create(materialTypeKoli);

                            StockType stockTypeSaya = new StockType();
                            stockTypeSaya.setCode("M_SAY");
                            stockTypeSaya.setName("SAYA");
                            stockTypeSaya.setDescription("BİTMİŞ MAMÜL");
                            stockTypeSaya.setUpdater(userToken.getUser());
                            stockTypeSaya.setCompany(company);
                            stockTypeSaya.setUpdateDate(new DateTime());
                            stockTypeDAO.create(stockTypeSaya);

                            StockType stockTypeTaban = new StockType();
                            stockTypeTaban.setCode("M_TBN");
                            stockTypeTaban.setName("TABAN");
                            stockTypeTaban.setDescription("BİTMİŞ MAMÜL");
                            stockTypeTaban.setUpdater(userToken.getUser());
                            stockTypeTaban.setCompany(company);
                            stockTypeTaban.setUpdateDate(new DateTime());
                            stockTypeDAO.create(stockTypeTaban);

                            StockType stockTypeAyakkabi = new StockType();
                            stockTypeAyakkabi.setCode("M_AYK");
                            stockTypeAyakkabi.setName("AYAKKABI");
                            stockTypeAyakkabi.setDescription("BİTMİŞ MAMÜL");
                            stockTypeAyakkabi.setUpdater(userToken.getUser());
                            stockTypeAyakkabi.setCompany(company);
                            stockTypeAyakkabi.setUpdateDate(new DateTime());
                            stockTypeDAO.create(stockTypeAyakkabi);

                            Supplier supplier = new Supplier();
                            supplier.setUpdateDate(new DateTime());
                            supplier.setAddress(company.getAddress());
                            supplier.setCode(company.getAddress());
                            supplier.setName(company.getName());
                            supplier.setCompany(company);
                            supplier.setUpdater(userToken.getUser());
                            supplier.setDescription("Firma içi tedarik");
                            supplierDAO.create(supplier);
                        }

                        return Response.ok().entity(company).build();
                    }
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_EMAIL.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read/{crmt}")
    @UnitOfWork
    public Response getAll(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_READ)) {
                List<Company> companies = companyDAO.findAll(Company.class);

                return Response.ok(companies).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByOid")
    @UnitOfWork
    public Response getByOid(Map<String, String> companyData) {
        String token = companyData.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(company).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> companyData) {
        String token = companyData.get("crmt");
        String oid = companyData.get("oid");
        UserToken userToken = authUtil.isValidToken(token);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_READ)) {
                Company company = companyDAO.findById(oid);
                return Response.ok(company).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("companySelfUpdate")
    @UnitOfWork
    public Response companyUpdater(Map<String, String> companyData) {
        String token = companyData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_SELF_UPDATE)) {
                Company company = userToken.getUser().getCompany();

                String language = companyData.get("language");
                String facebook = companyData.get("facebook");
                String twitter = companyData.get("twitter");
                String instagram = companyData.get("instagram");
                String bankInfo = companyData.get("bankInfo");
                String address = companyData.get("address");
                String email = companyData.get("email");
                String website = companyData.get("website");
                String phone = companyData.get("phone");
                String fax = companyData.get("fax");
                String orderSpecialNotes = companyData.get("orderSpecialNotes");

                company.setLanguage(language);
                company.setFacebook(facebook);
                company.setTwitter(twitter);
                company.setInstagram(instagram);
                company.setBankInfo(bankInfo);
                company.setAddress(address);
                company.setEmail(email);
                company.setWebsite(website);
                company.setPhone(phone);
                company.setFax(fax);
                company.setOrderSpecialNotes(orderSpecialNotes);
                companyDAO.update(company);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), "", new DateTime());

                return Response.ok(company).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");
        String oid = data.get("oid");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_UPDATE)) {
                Company company = companyDAO.findById(oid);

                if (company == null) {
                    return Response.serverError().entity(ErrorMessage.UNKNOWN_COMPANY.getErrorAlias()).build();
                } else {
                    String code = data.get("code");

                    Company CompanyByCode = companyDAO.findByCode(code);

                    if (CompanyByCode != null && !CompanyByCode.getCode().equals(company.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        String name = data.get("name");
                        String licenseStartDate = data.get("licenseStartDate");
                        String orderSpecialNotes = data.get("orderSpecialNotes");
                        String language = data.get("language");
                        String logoUrl = data.get("logoUrl");
                        String phone = data.get("phone");
                        String fax = data.get("fax");
                        String website = data.get("website");
                        String facebook = data.get("facebook");
                        String twitter = data.get("twitter");
                        String instagram = data.get("instagram");
                        String email = data.get("email");
                        String address = data.get("address");
                        String bankInfo = data.get("bankInfo");


                        org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                        DateTime licenseStart = formatter.parseDateTime(licenseStartDate);

                        company.setCode(code);
                        company.setName(name);
                        company.setLicenseStartDate(licenseStart);
                        company.setLogoUrl(logoUrl);
                        company.setLanguage(language);
                        company.setFacebook(facebook);
                        company.setTwitter(twitter);
                        company.setInstagram(instagram);
                        company.setBankInfo(bankInfo);
                        company.setAddress(address);
                        company.setEmail(email);
                        company.setWebsite(website);
                        company.setPhone(phone);
                        company.setFax(fax);
                        company.setOrderSpecialNotes(orderSpecialNotes);
                        companyDAO.update(company);

                        return Response.ok(company).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> companyData) {
        String token = companyData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_DELETE)) {

                String oid = companyData.get("oid");
                Company company = companyDAO.findById(oid);

                companyDAO.delete(company);

                return Response.ok(company).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("findByName")
    @UnitOfWork
    public Response findByName(Map<String, String> companyData) {
        String token = companyData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_READ)) {
                String name = companyData.get("name");
                List<Company> companys = companyDAO.getByContainsInTitle(name);

                if (companys.isEmpty()) {
                    return Response.serverError().entity(ErrorMessage.UNKNOWN_COMPANY.getErrorAlias()).build();
                } else {
                    return Response.ok(companys).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("findByCode")
    @UnitOfWork
    public Response findByCode(Map<String, String> companyData) {
        String token = companyData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_READ)) {
                String code = companyData.get("code");
                List<Company> companys = companyDAO.getByCodeList(code);

                if (companys.isEmpty()) {
                    return Response.serverError().entity(ErrorMessage.UNKNOWN_COMPANY.getErrorAlias()).build();
                } else {
                    return Response.ok(companys).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("/uploadCustomerLogo/{crmt}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @UnitOfWork
    public Response uploadWorkOrderImages(
            @PathParam("crmt") String crmt,
            @FormDataParam("customerLogo") InputStream uploadedInputStream,
            @FormDataParam("customerLogo") FormDataContentDisposition fileDetail) {

        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_SELF_UPDATE)) {
                //  ŞİRKET BİLGİLERİ
                String osBasePath = "";

                if (System.getProperty("os.name").equals("Windows 10")) {
                    osBasePath = "C:\\xampp\\htdocs\\izci-muhasebe\\logo\\";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    osBasePath = "/Applications/XAMPP/xamppfiles/htdocs/izci-muhasebe/logo/";
                } else {
                    osBasePath = "/var/www/html/logo/";
                }

                String fileExtension = "";

                if (fileDetail.getFileName().endsWith(".png") || fileDetail.getFileName().endsWith(".PNG")) {
                    fileExtension = ".png";
                } else if (fileDetail.getFileName().endsWith(".jpg") || fileDetail.getFileName().endsWith(".JPG") || fileDetail.getFileName().endsWith(".JPEG") || fileDetail.getFileName().endsWith(".jpeg")) {
                    fileExtension = ".jpg";
                }

                DateTime fileNameDT = new DateTime();

                String fileName = fileNameDT.getMillis() + Math.random() + fileExtension;

                String uploadedFileLocation = osBasePath + fileName;

                // save it with thumbnail

                try {
                    Thumbnails.of(uploadedInputStream)
                            .size(250, 250).toFile(uploadedFileLocation);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                WorkOrderImageDTO workOrderImageDTO = new WorkOrderImageDTO();
                workOrderImageDTO.setPath(fileName);

                return Response.ok(workOrderImageDTO).build();
            }
        }

        return Response.ok().build();
    }

    @POST
    @Path("getMrpSettings")
    @UnitOfWork
    public Response getMrpSettings(Map<String, String> companyData) {
        String token = companyData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_READ)) {
                MrpSetting mrpSetting = mrpSettingDAO.get(userToken.getUser().getCompany()).get(0);

                return Response.ok().entity(mrpSetting).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("setMrpSettings")
    @UnitOfWork
    public Response setMrpSettings(Map<String, String> companyData) {
        String token = companyData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.COMPANY_SELF_UPDATE)) {
                String calculationType = companyData.get("calculationType");

                MrpSetting mrpSetting = mrpSettingDAO.get(userToken.getUser().getCompany()).get(0);
                mrpSetting.setCalculationType(calculationType);
                mrpSettingDAO.update(mrpSetting);

                return Response.ok().entity(mrpSetting).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
