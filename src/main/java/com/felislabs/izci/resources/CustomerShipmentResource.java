package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.CustomerDAO;
import com.felislabs.izci.dao.dao.CustomerShipmentDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Customer;
import com.felislabs.izci.representation.entities.CustomerShipment;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 14/01/2017.
 */
@Path("/customerShipment")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerShipmentResource {
    AuthUtil authUtil;
    CustomerShipmentDAO customerShipmentDAO;
    CustomerDAO customerDAO;

    public CustomerShipmentResource(AuthUtil authUtil, CustomerShipmentDAO customerShipmentDAO, CustomerDAO customerDAO) {
        this.authUtil = authUtil;
        this.customerShipmentDAO = customerShipmentDAO;
        this.customerDAO = customerDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_INFO_CREATE)) {
                String oid = data.get("customerOid");
                String code = data.get("code");
                String shipmentInfo = data.get("shipmentInfo");

                Company company = userToken.getUser().getCompany();
                Customer customer = customerDAO.getById(oid, company);

                CustomerShipment customerShipment = customerShipmentDAO.getByCode(code, userToken.getUser().getCompany());

                if (customerShipment != null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    CustomerShipment customerShipmentByCode = customerShipmentDAO.getByCode(code, userToken.getUser().getCompany());

                    if (customerShipmentByCode != null && !customerShipmentByCode.getCode().equals(customerShipment.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {

                        customerShipment = new CustomerShipment();
                        customerShipment.setCompany(company);
                        customerShipment.setUpdater(userToken.getUser());
                        customerShipment.setUpdateDate(new DateTime());
                        customerShipment.setShipmentInfo(shipmentInfo);
                        customerShipment.setCustomer(customer);
                        customerShipment.setCode(code);

                        customerShipmentDAO.create(customerShipment);

                        authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), customerShipment.getCode() + " kodlu sevkiyat bilgisi oluşturuldu. ", new DateTime());

                        return Response.ok().entity(customerShipment).build();

                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_INFO_CREATE)) {
                String customerShipmentOid = data.get("customerShipmentOid");
                String oid = data.get("customerOid");
                String shipmentInfo = data.get("shipmentInfo");
                String code = data.get("code");

                Company company = userToken.getUser().getCompany();
                Customer customer = customerDAO.getById(oid, company);


                CustomerShipment customerShipment = customerShipmentDAO.getByOid(customerShipmentOid, company);
                if (customerShipment == null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    CustomerShipment customerShipmentByCode = customerShipmentDAO.getByCode(code, userToken.getUser().getCompany());

                    if (customerShipmentByCode != null && !customerShipmentByCode.getCode().equals(customerShipment.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        customerShipment.setUpdater(userToken.getUser());
                        customerShipment.setUpdateDate(new DateTime());
                        customerShipment.setShipmentInfo(shipmentInfo);
                        customerShipment.setCustomer(customer);
                        customerShipment.setCode(code);

                        customerShipmentDAO.update(customerShipment);

                        authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), customerShipment.getCode() + " kodlu sevkiyat bilgisi güncelledi. ", new DateTime());

                        return Response.ok().entity(customerShipment).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_INFO_CREATE)) {
                List<CustomerShipment> customerShipment = customerShipmentDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(customerShipment).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_INFO_CREATE)) {
                String customerShipmentOid = data.get("oid");

                CustomerShipment customerShipment = customerShipmentDAO.getByOid(customerShipmentOid, userToken.getUser().getCompany());

                return Response.ok().entity(customerShipment).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_INFO_CREATE)) {
                String customerShipmentOid = data.get("oid");

                CustomerShipment customerShipment = customerShipmentDAO.getByOid(customerShipmentOid, userToken.getUser().getCompany());
                customerShipment.setDeleteStatus(true);

                customerShipmentDAO.update(customerShipment);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), customerShipment.getCode() + " kodlu sevkiyat bilgisi silindi. ", new DateTime());

                return Response.ok().entity(customerShipment).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
