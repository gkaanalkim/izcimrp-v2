package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 11/03/2017.
 */
@Path("consignmentInvoice")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ConsignmentInvoiceResource {
    AuthUtil authUtil;
    BidDAO bidDAO;
    ConsignmentInvoiceDAO consignmentInvoiceDAO;
    AccountingCustomerDAO accountingCustomerDAO;
    AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO;
    TaxDAO taxDAO;
    ProductDAO productDAO;
    ConsignmentInvoiceDetailDAO consignmentInvoiceDetailDAO;

    public ConsignmentInvoiceResource(AuthUtil authUtil, BidDAO bidDAO, ConsignmentInvoiceDAO consignmentInvoiceDAO, AccountingCustomerDAO accountingCustomerDAO, AccountingCustomerRepresentativeDAO accountingCustomerRepresentativeDAO, TaxDAO taxDAO, ProductDAO productDAO, ConsignmentInvoiceDetailDAO consignmentInvoiceDetailDAO) {
        this.authUtil = authUtil;
        this.bidDAO = bidDAO;
        this.consignmentInvoiceDAO = consignmentInvoiceDAO;
        this.accountingCustomerDAO = accountingCustomerDAO;
        this.accountingCustomerRepresentativeDAO = accountingCustomerRepresentativeDAO;
        this.taxDAO = taxDAO;
        this.productDAO = productDAO;
        this.consignmentInvoiceDetailDAO = consignmentInvoiceDetailDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CONSIGNMENT_INVOCE_CREATE)) {
                String customerOid = data.get("customerOid");
                String customerRepresentativeOid = data.get("customerRepresentativeOid");

                String billingDate = data.get("billingDate");
                String deliveryNote = data.get("deliveryNote");
                String paymentNote = data.get("paymentNote");
                String currencyNote = data.get("currencyNote");
                String currency = data.get("currency");
                String description = data.get("description");
                String details = data.get("details");
                String invoiceNumber = data.get("invoiceNumber");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime cid = formatter.parseDateTime(billingDate);

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(customerRepresentativeOid, company);

                DateTime now = new DateTime();

                String documentNumber = accountingCustomer.getCode() + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + accountingCustomer.getBidNumber();

                accountingCustomer.setBidNumber(accountingCustomer.getBidNumber() + 1);
                accountingCustomerDAO.update(accountingCustomer);

                ConsignmentInvoice consignmentInvoice = new ConsignmentInvoice();
                consignmentInvoice.setCompany(company);
                consignmentInvoice.setUser(user);
                consignmentInvoice.setUpdateDate(new DateTime());
                consignmentInvoice.setAccountingCustomer(accountingCustomer);
                consignmentInvoice.setAccountingCustomerRepresentative(accountingCustomerRepresentative);
                consignmentInvoice.setConsignmentInvoiceDate(cid);
                consignmentInvoice.setDeliveryNote(deliveryNote);
                consignmentInvoice.setPaymentNote(paymentNote);
                consignmentInvoice.setCurrencyNote(currencyNote);
                consignmentInvoice.setCurrency(currency);
                consignmentInvoice.setDescription(description);
                consignmentInvoice.setDocumentNumber(documentNumber);
                consignmentInvoice.setInvoiceNumber(invoiceNumber);

                consignmentInvoiceDAO.create(consignmentInvoice);

                Double subTotal = 0.0;
                Double discountIncluded = 0.0;
                Double taxIncluded = 0.0;

                JSONArray jsonArray = new JSONArray(details);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String code = jsonObject.getString("code");
                    Double quantity = Double.valueOf(jsonObject.getString("quantity"));
                    Double price = Double.valueOf(jsonObject.getString("price"));
                    Double discount = Double.valueOf(jsonObject.getString("discount"));
                    String deadline = jsonObject.getString("deadline");
                    String detailTaxOid = jsonObject.getString("tax");

                    Double lineTotal = quantity * price;
                    Double discountIncludedPrice = lineTotal - (lineTotal * (discount / 100));

                    Product product = productDAO.getByCode(code, company);
                    Tax detailTax = taxDAO.getByOid(detailTaxOid, company);

                    Double taxIncludedPrice = discountIncludedPrice + (discountIncludedPrice * (detailTax.getRatio() / 100));

                    ConsignmentInvoiceDetail consignmentInvoiceDetail = new ConsignmentInvoiceDetail();
                    consignmentInvoiceDetail.setUser(user);
                    consignmentInvoiceDetail.setUpdateDate(new DateTime());
                    consignmentInvoiceDetail.setQuantity(quantity);
                    consignmentInvoiceDetail.setPrice(price);
                    consignmentInvoiceDetail.setDiscount(discount);
                    consignmentInvoiceDetail.setDeadline(deadline);
                    consignmentInvoiceDetail.setLineTotal(lineTotal);
                    consignmentInvoiceDetail.setProduct(product);
                    consignmentInvoiceDetail.setDiscountIncluded(discountIncludedPrice);
                    consignmentInvoiceDetail.setTaxIncluded(taxIncludedPrice);
                    consignmentInvoiceDetail.setConsignmentInvoice(consignmentInvoice);
                    consignmentInvoiceDetail.setTax(detailTax);

                    consignmentInvoiceDetailDAO.create(consignmentInvoiceDetail);

                    subTotal += lineTotal;
                    discountIncluded += discountIncludedPrice;
                    taxIncluded += taxIncludedPrice;
                }

                consignmentInvoice.setSubTotal(subTotal);
                consignmentInvoice.setTaxIncluded(taxIncluded);
                consignmentInvoice.setDiscountIncluded(discountIncluded);

                consignmentInvoiceDAO.update(consignmentInvoice);

                return Response.ok().entity(consignmentInvoice).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CONSIGNMENT_INVOCE_READ)) {
                List<ConsignmentInvoice> consignmentInvoices = consignmentInvoiceDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(consignmentInvoices).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CONSIGNMENT_INVOCE_READ)) {
                String oid = data.get("oid");

                ConsignmentInvoice consignmentInvoice = consignmentInvoiceDAO.getByOid(oid, userToken.getUser().getCompany());

                List<ConsignmentInvoiceDetail> consignmentInvoiceDetails = consignmentInvoiceDetailDAO.getByConsignmentInvoice(consignmentInvoice);

                return Response.ok().entity(consignmentInvoiceDetails).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CONSIGNMENT_INVOCE_UPDATE)) {
                String oid = data.get("oid");
                String customerOid = data.get("customerOid");
                String customerRepresentativeOid = data.get("customerRepresentativeOid");

                String billingDate = data.get("billingDate");
                String deliveryNote = data.get("deliveryNote");
                String paymentNote = data.get("paymentNote");
                String currencyNote = data.get("currencyNote");
                String currency = data.get("currency");
                String description = data.get("description");
                String invoiceNumber = data.get("invoiceNumber");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime cid = formatter.parseDateTime(billingDate);

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                AccountingCustomerRepresentative accountingCustomerRepresentative = accountingCustomerRepresentativeDAO.getByOid(customerRepresentativeOid, company);

                DateTime now = new DateTime();

                String documentNumber = accountingCustomer.getCode() + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + accountingCustomer.getBidNumber();

                accountingCustomer.setBidNumber(accountingCustomer.getBidNumber() + 1);
                accountingCustomerDAO.update(accountingCustomer);

                ConsignmentInvoice consignmentInvoice = consignmentInvoiceDAO.getByOid(oid, company);
                consignmentInvoice.setCompany(company);
                consignmentInvoice.setUser(user);
                consignmentInvoice.setUpdateDate(new DateTime());
                consignmentInvoice.setAccountingCustomer(accountingCustomer);
                consignmentInvoice.setAccountingCustomerRepresentative(accountingCustomerRepresentative);
                consignmentInvoice.setConsignmentInvoiceDate(cid);
                consignmentInvoice.setDeliveryNote(deliveryNote);
                consignmentInvoice.setPaymentNote(paymentNote);
                consignmentInvoice.setCurrencyNote(currencyNote);
                consignmentInvoice.setCurrency(currency);
                consignmentInvoice.setDescription(description);
                consignmentInvoice.setDocumentNumber(documentNumber);
                consignmentInvoice.setInvoiceNumber(invoiceNumber);

                consignmentInvoiceDAO.update(consignmentInvoice);

                return Response.ok().entity(consignmentInvoice).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("detail/update")
    @UnitOfWork
    public Response detailUpdate(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CONSIGNMENT_INVOCE_UPDATE)) {
                String oid = data.get("oid");
                String details = data.get("details");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                ConsignmentInvoice consignmentInvoice = consignmentInvoiceDAO.getByOid(oid, company);

                // Delete consignment invoice's details if exist
                List<ConsignmentInvoiceDetail> consignmentInvoiceDetails = consignmentInvoiceDetailDAO.getByConsignmentInvoice(consignmentInvoice);
                if (consignmentInvoiceDetails.size() != 0) {
                    for (ConsignmentInvoiceDetail consignmentInvoiceDetail : consignmentInvoiceDetails) {
                        consignmentInvoiceDetailDAO.delete(consignmentInvoiceDetail);
                    }
                }

                Double subTotal = 0.0;
                Double discountIncluded = 0.0;
                Double taxIncluded = 0.0;

                JSONArray jsonArray = new JSONArray(details);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String code = jsonObject.getString("code");
                    Double quantity = Double.valueOf(jsonObject.getString("quantity"));
                    Double price = Double.valueOf(jsonObject.getString("price"));
                    Double discount = Double.valueOf(jsonObject.getString("discount"));
                    String deadline = jsonObject.getString("deadline");
                    String detailTaxOid = jsonObject.getString("tax");

                    Double lineTotal = quantity * price;
                    Double discountIncludedPrice = lineTotal - (lineTotal * (discount / 100));

                    Product product = productDAO.getByCode(code, company);
                    Tax detailTax = taxDAO.getByOid(detailTaxOid, company);

                    Double taxIncludedPrice = discountIncludedPrice + (discountIncludedPrice * (detailTax.getRatio() / 100));

                    ConsignmentInvoiceDetail consignmentInvoiceDetail = new ConsignmentInvoiceDetail();
                    consignmentInvoiceDetail.setUser(user);
                    consignmentInvoiceDetail.setUpdateDate(new DateTime());
                    consignmentInvoiceDetail.setQuantity(quantity);
                    consignmentInvoiceDetail.setPrice(price);
                    consignmentInvoiceDetail.setDiscount(discount);
                    consignmentInvoiceDetail.setDeadline(deadline);
                    consignmentInvoiceDetail.setLineTotal(lineTotal);
                    consignmentInvoiceDetail.setProduct(product);
                    consignmentInvoiceDetail.setDiscountIncluded(discountIncludedPrice);
                    consignmentInvoiceDetail.setTaxIncluded(taxIncludedPrice);
                    consignmentInvoiceDetail.setConsignmentInvoice(consignmentInvoice);
                    consignmentInvoiceDetail.setTax(detailTax);

                    consignmentInvoiceDetailDAO.create(consignmentInvoiceDetail);

                    subTotal += lineTotal;
                    discountIncluded += discountIncludedPrice;
                    taxIncluded += taxIncludedPrice;
                }

                consignmentInvoice.setSubTotal(subTotal);
                consignmentInvoice.setTaxIncluded(taxIncluded);
                consignmentInvoice.setDiscountIncluded(discountIncluded);

                consignmentInvoiceDAO.update(consignmentInvoice);

                return Response.ok().entity(consignmentInvoice).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.CONSIGNMENT_INVOCE_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                ConsignmentInvoice consignmentInvoice = consignmentInvoiceDAO.getByOid(oid, company);
                consignmentInvoice.setDeleteStatus(true);
                consignmentInvoiceDAO.update(consignmentInvoice);

                return Response.ok().entity(consignmentInvoice).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
