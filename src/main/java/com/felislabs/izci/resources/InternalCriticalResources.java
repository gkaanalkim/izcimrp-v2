package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.InternalCriticalDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.InternalCritical;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;


/**
 * Created by dogukanyilmaz on 06/12/2016.
 */
@Path("/internalCritical")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class InternalCriticalResources {
    AuthUtil authUtil;
    InternalCriticalDAO internalCriticalDAO;

    public InternalCriticalResources(AuthUtil authUtil, InternalCriticalDAO internalCriticalDAO) {
        this.authUtil = authUtil;
        this.internalCriticalDAO = internalCriticalDAO;
    }


    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_CREATE)) {
                String title = data.get("title");
                String description = data.get("description");
                Company company = userToken.getUser().getCompany();

                InternalCritical internalCritical = new InternalCritical();

                internalCritical.setCompany(company);
                internalCritical.setUpdater(userToken.getUser());
                internalCritical.setTitle(title);
                internalCritical.setDescription(description);

                internalCriticalDAO.create(internalCritical);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), internalCritical.getTitle() + " başlıklı iç kritik oluşturuldu. ", new DateTime());

                return Response.ok(internalCritical).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_READ)) {
                List<InternalCritical> internalCriticals = internalCriticalDAO.getAll(InternalCritical.class, userToken.getUser().getCompany());
                return Response.ok(internalCriticals).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read/{crmt}")
    @UnitOfWork
    public Response read(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_READ)) {
                List<InternalCritical> internalCriticals = internalCriticalDAO.getAll(InternalCritical.class, userToken.getUser().getCompany());
                return Response.ok(internalCriticals).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_UPDATE)) {
                String oid = data.get("oid");
                String title = data.get("title");
                String description = data.get("description");


                Company company = userToken.getUser().getCompany();
                InternalCritical internalCritical = internalCriticalDAO.getById(oid, userToken.getUser().getCompany());

                internalCritical.setCompany(company);
                internalCritical.setUpdater(userToken.getUser());
                internalCritical.setTitle(title);
                internalCritical.setDescription(description);
                internalCriticalDAO.update(internalCritical);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), internalCritical.getTitle() + " başlıklı iç kritik güncellendi. ", new DateTime());

                return Response.ok(internalCritical).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_DELETE)) {
                String oid = data.get("oid");
                InternalCritical internalCritical = internalCriticalDAO.getById(oid, userToken.getUser().getCompany());
                internalCritical.setDeleteStatus(true);

                internalCriticalDAO.update(internalCritical);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), internalCritical.getTitle() + " başlıklı iç kritik silindi. ", new DateTime());


                return Response.ok(internalCritical).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_READ)) {
                String oid = data.get("oid");
                InternalCritical internalCritical = internalCriticalDAO.getById(oid, userToken.getUser().getCompany());
                return Response.ok(internalCritical).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetailList")
    @UnitOfWork
    public Response readDetailForSearch(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_READ)) {
                String oid = data.get("oid");
                List<InternalCritical> internalCriticals = internalCriticalDAO.getByOidforSearch(oid, userToken.getUser().getCompany());
                return Response.ok(internalCriticals).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByTitle")
    @UnitOfWork
    public Response getByTitle(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_READ)) {
                String title = data.get("title");
                List<InternalCritical> internalCriticals = internalCriticalDAO.getByTitle(title, userToken.getUser().getCompany());
                return Response.ok(internalCriticals).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
