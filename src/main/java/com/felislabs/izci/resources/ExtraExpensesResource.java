package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.ExtraExpensesDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ExtraExpenses;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by dogukanyilmaz on 25/04/2017.
 */
@Path("extraExpenses")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ExtraExpensesResource {
    AuthUtil authUtil;
    ExtraExpensesDAO extraExpensesDAO;

    public ExtraExpensesResource(AuthUtil authUtil, ExtraExpensesDAO extraExpensesDAO) {
        this.authUtil = authUtil;
        this.extraExpensesDAO = extraExpensesDAO;
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.EXTRA_EXPENSES_READ)) {
                List<ExtraExpenses> extraExpenses = extraExpensesDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(extraExpenses).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.EXTRA_EXPENSES_READ)) {
                String oid = data.get("oid");
                ExtraExpenses extraExpenses = extraExpensesDAO.getByOid(oid, userToken.getUser().getCompany());
                return Response.ok(extraExpenses).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.EXTRA_EXPENSES_CREATE)) {
                String title = data.get("title");
                String description = data.get("description");
                String isDefault = data.get("isDefault");

                Company company = userToken.getUser().getCompany();
                ExtraExpenses extraExpenses = extraExpensesDAO.getByTitle(title, company);

                if (extraExpenses == null) {
                    extraExpenses = new ExtraExpenses();

                    extraExpenses.setTitle(title);
                    extraExpenses.setDescription(description);
                    extraExpenses.setCompany(company);
                    extraExpenses.setPutative(Boolean.valueOf(isDefault));
                    extraExpenses.setUpdateDate(new DateTime());
                    extraExpenses.setUpdater(userToken.getUser());
                    extraExpensesDAO.create(extraExpenses);

                    authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), extraExpenses.getTitle() + " başlıklı gider oluşturuldu. ", new DateTime());


                    return Response.ok(extraExpenses).build();
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_EXTRA_EXPENSES.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.EXTRA_EXPENSES_UPDATE)) {
                Company company = userToken.getUser().getCompany();

                String oid = data.get("oid");
                String title = data.get("title");
                String description = data.get("description");
                String isDefault = data.get("isDefault");

                String oldTitle = extraExpensesDAO.getByOid(oid, company).getTitle();
                ExtraExpenses titleControl = extraExpensesDAO.getByTitle(title, company);

                if (titleControl != null && titleControl.getTitle() != oldTitle) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_EXTRA_EXPENSES.getErrorAlias()).build();
                }

                ExtraExpenses extraExpenses = extraExpensesDAO.getByOid(oid, company);

                extraExpenses.setTitle(title);
                extraExpenses.setDescription(description);
                extraExpenses.setCompany(company);
                extraExpenses.setPutative(Boolean.valueOf(isDefault));
                extraExpenses.setUpdateDate(new DateTime());
                extraExpenses.setUpdater(userToken.getUser());

                extraExpensesDAO.update(extraExpenses);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), extraExpenses.getTitle() + " başlıklı gider güncellendi. ", new DateTime());

                return Response.ok(extraExpenses).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.EXTRA_EXPENSES_DELETE)) {
                Company company = userToken.getUser().getCompany();
                String oid = data.get("oid");
                ExtraExpenses extraExpenses = extraExpensesDAO.getByOid(oid, company);
                extraExpensesDAO.delete(extraExpenses);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), extraExpenses.getTitle() + " başlıklı gider silindi. ", new DateTime());

                return Response.ok(extraExpenses).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

}
