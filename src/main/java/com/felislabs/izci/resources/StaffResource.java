package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.DepartmentDAO;
import com.felislabs.izci.dao.dao.StaffDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Department;
import com.felislabs.izci.representation.entities.Staff;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by arricie on 21.03.2017.
 */

@Path("staff")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StaffResource {
    AuthUtil authUtil;
    StaffDAO staffDAO;
    DepartmentDAO departmentDAO;

    public StaffResource(AuthUtil authUtil, StaffDAO staffDAO, DepartmentDAO departmentDAO) {
        this.authUtil = authUtil;
        this.staffDAO = staffDAO;
        this.departmentDAO = departmentDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STAFF_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String phone = data.get("phone");
                String internal = data.get("internal");
                String address = data.get("address");
                String departmentOid = data.get("departmentOid");
                Company company = userToken.getUser().getCompany();
                Department department = departmentDAO.getByOid(departmentOid, company);

                Staff staff = staffDAO.getByCode(code, userToken.getUser().getCompany());

                if (staff != null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Staff staffByCode = staffDAO.getByCode(code, userToken.getUser().getCompany());

                    if (staffByCode != null && !staffByCode.getCode().equals(staff.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {

                        staff = new Staff();
                        staff.setCompany(company);
                        staff.setName(name);
                        staff.setCode(code);
                        staff.setPhone(phone);
                        staff.setInternal(internal);
                        staff.setAddress(address);
                        staff.setDepartment(department);

                    }
                }
                staffDAO.create(staff);

                return Response.ok().entity(staff).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STAFF_READ)) {
                List<Staff> staffList = staffDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(staffList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STAFF_READ)) {
                String oid = data.get("oid");

                Staff staff = staffDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(staff).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STAFF_UPDATE)) {
                String oid = data.get("oid");

                String name = data.get("name");
                String code = data.get("code");
                String phone = data.get("phone");
                String internal = data.get("internal");
                String address = data.get("address");
                String departmentOid = data.get("departmentOid");
                Company company = userToken.getUser().getCompany();
                Department department = departmentDAO.getByOid(departmentOid, company);


                Staff staff = staffDAO.getByOid(oid, company);

                if (staff == null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Staff staffByCode = staffDAO.getByCode(code, userToken.getUser().getCompany());

                    if (staffByCode != null && !staffByCode.getCode().equals(staff.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        staff.setCompany(company);
                        staff.setName(name);
                        staff.setCode(code);
                        staff.setPhone(phone);
                        staff.setInternal(internal);
                        staff.setAddress(address);
                        staff.setDepartment(department);
                        staffDAO.update(staff);


                        return Response.ok().entity(staff).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STAFF_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Staff staff = staffDAO.getByOid(oid, company);
                staff.setDeleteStatus(true);

                staffDAO.update(staff);

                return Response.ok().entity(staff).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }


}
