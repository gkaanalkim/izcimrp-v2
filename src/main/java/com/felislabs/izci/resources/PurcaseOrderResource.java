package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.ModelPurcaseOrderSummaryDTO;
import com.felislabs.izci.representation.dto.ModelSummary;
import com.felislabs.izci.representation.dto.PurcaseOrderCompareSummaryDTO;
import com.felislabs.izci.representation.dto.PurcaseOrderSummaryDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 15/06/2017.
 */
@Path("purcaseOrder")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PurcaseOrderResource {
    AuthUtil authUtil;
    StockDAO stockDAO;
    StockDetailDAO stockDetailDAO;
    ModelDAO modelDAO;
    PurcaseOrderDAO purcaseOrderDAO;
    SupplierDAO supplierDAO;
    MrpSettingDAO mrpSettingDAO;
    ModelPurcaseOrderDAO modelPurcaseOrderDAO;
    WarehouseIncomingDAO warehouseIncomingDAO;

    public PurcaseOrderResource(AuthUtil authUtil, StockDAO stockDAO, StockDetailDAO stockDetailDAO, ModelDAO modelDAO, PurcaseOrderDAO purcaseOrderDAO, SupplierDAO supplierDAO, MrpSettingDAO mrpSettingDAO, ModelPurcaseOrderDAO modelPurcaseOrderDAO, WarehouseIncomingDAO warehouseIncomingDAO) {
        this.authUtil = authUtil;
        this.stockDAO = stockDAO;
        this.stockDetailDAO = stockDetailDAO;
        this.modelDAO = modelDAO;
        this.purcaseOrderDAO = purcaseOrderDAO;
        this.supplierDAO = supplierDAO;
        this.mrpSettingDAO = mrpSettingDAO;
        this.modelPurcaseOrderDAO = modelPurcaseOrderDAO;
        this.warehouseIncomingDAO = warehouseIncomingDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PURCASE_ORDER_CREATE)) {
                String screenshot = data.get("screenshot");
                String termin = data.get("termin");
                String po = data.get("po");
                String supplierOid = data.get("supplierOid");
                String models = data.get("models");

                Company company = userToken.getUser().getCompany();

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime td = formatter.parseDateTime(termin);

                Supplier supplier = supplierDAO.getById(supplierOid, company);
                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);

                DateTime now = new DateTime();
                String poNumber = supplier.getCode() + "-" + now.getYear() + "" + now.getMonthOfYear() + "" + now.getDayOfMonth() + "-" + mrpSetting.getPoNumber();

                mrpSetting.setPoNumber(mrpSetting.getPoNumber() + 1);
                mrpSettingDAO.update(mrpSetting);

                List<PurcaseOrder> purcaseOrders = new LinkedList<>();

                JSONArray jsonArray = new JSONArray(po);

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String stockDetailOid = jsonObject.getString("stockDetailOid");
                    String stockOid = jsonObject.getString("stockOid");
                    String calculatedRequirement = jsonObject.getString("calculatedRequirement");
                    String requirement = jsonObject.getString("requirement");
                    String stockType = jsonObject.getString("stockType");
                    String stockDetailText = jsonObject.getString("stockDetailText");

                    Stock stock = stockDAO.getById(stockOid, company);

                    if (stock.getStockType().getName().equals("TABAN")) {
                        StockDetail stockDetail = stockDetailDAO.getByNameAndStock(stockDetailText, stock, company);

                        PurcaseOrder purcaseOrder = new PurcaseOrder();
                        purcaseOrder.setCompany(company);
                        purcaseOrder.setUpdater(userToken.getUser());
                        purcaseOrder.setUpdateDate(new DateTime());
                        purcaseOrder.setCalculatedRequirement(Double.valueOf(calculatedRequirement));
                        purcaseOrder.setRequirement(Double.valueOf(requirement));
                        purcaseOrder.setStock(stock);
                        purcaseOrder.setStockDetail(stockDetail);
                        purcaseOrder.setSupplier(supplier);
                        purcaseOrder.setTerminDate(td);
                        purcaseOrder.setPoNumber(poNumber);

                        purcaseOrderDAO.create(purcaseOrder);

                        purcaseOrders.add(purcaseOrder);
                    } else {
                        StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, company);

                        PurcaseOrder purcaseOrder = new PurcaseOrder();
                        purcaseOrder.setCompany(company);
                        purcaseOrder.setUpdater(userToken.getUser());
                        purcaseOrder.setUpdateDate(new DateTime());
                        purcaseOrder.setCalculatedRequirement(Double.valueOf(calculatedRequirement));
                        purcaseOrder.setRequirement(Double.valueOf(requirement));
                        purcaseOrder.setStock(stock);
                        purcaseOrder.setStockDetail(stockDetail);
                        purcaseOrder.setSupplier(supplier);
                        purcaseOrder.setTerminDate(td);
                        purcaseOrder.setPoNumber(poNumber);

                        purcaseOrderDAO.create(purcaseOrder);

                        purcaseOrders.add(purcaseOrder);
                    }
                }

                ModelPurcaseOrder modelPurcaseOrder = new ModelPurcaseOrder();
                modelPurcaseOrder.setPoNumber(poNumber);
                modelPurcaseOrder.setModels(models);
                modelPurcaseOrder.setCompany(company);
                modelPurcaseOrder.setUpdater(userToken.getUser());
                modelPurcaseOrder.setUpdateDate(now);
                modelPurcaseOrder.setScreenshot(screenshot);
                modelPurcaseOrderDAO.create(modelPurcaseOrder);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), poNumber + " numaralı satınalma emrini oluşturuldu. ", now);

                return Response.ok().entity(purcaseOrders).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readModelPurcaseOrder")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PURCASE_ORDER_READ)) {
                Company company = userToken.getUser().getCompany();

                List<ModelPurcaseOrder> modelPurcaseOrders = modelPurcaseOrderDAO.getAll(company);
                List<ModelPurcaseOrderSummaryDTO> purcaseOrderSummaryDTOs = new LinkedList<>();

                for (ModelPurcaseOrder modelPurcaseOrder : modelPurcaseOrders) {
                    ModelPurcaseOrderSummaryDTO purcaseOrderSummaryDTO = new ModelPurcaseOrderSummaryDTO();
                    purcaseOrderSummaryDTO.setOid(modelPurcaseOrder.getOid());
                    purcaseOrderSummaryDTO.setPoNumber(modelPurcaseOrder.getPoNumber());
                    purcaseOrderSummaryDTO.setCreationDateTime(modelPurcaseOrder.getCreationDateTime());

                    purcaseOrderSummaryDTOs.add(purcaseOrderSummaryDTO);
                }

                return Response.ok().entity(purcaseOrderSummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByPoNumber")
    @UnitOfWork
    public Response readByPoNumber(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PURCASE_ORDER_READ)) {
                String poNumber = data.get("poNumber");

                List<PurcaseOrder> purcaseOrders = purcaseOrderDAO.getByPONumber(poNumber, userToken.getUser().getCompany());
                List<PurcaseOrderSummaryDTO> purcaseOrderSummaryDTOs = new LinkedList<>();

                for (PurcaseOrder purcaseOrder : purcaseOrders) {
                    StockDetail stockDetail = purcaseOrder.getStockDetail();
                    Stock stock = purcaseOrder.getStock();
                    Supplier supplier = purcaseOrder.getSupplier();

                    PurcaseOrderSummaryDTO purcaseOrderSummaryDTO = new PurcaseOrderSummaryDTO(
                            purcaseOrder.getTerminDate(),
                            stock != null ? stock.getName() : "",
                            stockDetail != null ? stockDetail.getName() : "",
                            supplier != null ? supplier.getName() : "",
                            purcaseOrder.getStockDetailText(),
                            purcaseOrder.getStockType(),
                            purcaseOrder.getRequirement(),
                            purcaseOrder.getCalculatedRequirement()
                    );

                    purcaseOrderSummaryDTOs.add(purcaseOrderSummaryDTO);
                }

                return Response.ok().entity(purcaseOrderSummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readModelsByPoNumber")
    @UnitOfWork
    public Response readModelsByPoNumber(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PURCASE_ORDER_READ)) {
                String poNumber = data.get("poNumber");

                ModelPurcaseOrder modelPurcaseOrder = modelPurcaseOrderDAO.getByPONumber(poNumber, userToken.getUser().getCompany());

                JSONArray jsonArray = new JSONArray(modelPurcaseOrder.getModels());

                List<Model> modelList = new LinkedList<>();

                for (int i = 0; i < jsonArray.length(); i++) {
                    Object o = jsonArray.get(i);

                    Model model = modelDAO.getByOid(o.toString(), userToken.getUser().getCompany());

                    modelList.add(model);
                }

                List<ModelSummary> modelSummaries = new LinkedList<>();

                for (Model model : modelList) {
                    ModelSummary modelSummary = new ModelSummary();
                    modelSummary.setOid(model.getOid());
                    modelSummary.setDefaultPhoto(model.getDefaultPhoto());
                    modelSummary.setModelCode(model.getModelCode());
                    modelSummary.setCustomerModelCode(model.getCustomerModelCode());
                    modelSummary.setColor(model.getColor());
                    modelSummary.setCustomer(model.getCustomer().getName());

                    modelSummaries.add(modelSummary);
                }

                return Response.ok().entity(modelSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readScreenshot")
    @UnitOfWork
    public Response readScreenshot(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PURCASE_ORDER_READ)) {
                String poNumber = data.get("poNumber");

                ModelPurcaseOrder modelPurcaseOrder = modelPurcaseOrderDAO.getByPONumber(poNumber, userToken.getUser().getCompany());

                return Response.ok().entity(modelPurcaseOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PURCASE_ORDER_DELETE)) {
                String poNumber = data.get("poNumber");

                ModelPurcaseOrder modelPurcaseOrder = modelPurcaseOrderDAO.getByPONumber(poNumber, userToken.getUser().getCompany());

                List<PurcaseOrder> purcaseOrders = purcaseOrderDAO.getByPONumber(modelPurcaseOrder.getPoNumber(), userToken.getUser().getCompany());

                for (PurcaseOrder purcaseOrder : purcaseOrders) {
                    purcaseOrder.setDeleteStatus(true);
                    purcaseOrderDAO.update(purcaseOrder);
                }

                modelPurcaseOrder.setDeleteStatus(true);
                modelPurcaseOrderDAO.update(modelPurcaseOrder);

                return Response.ok().entity(modelPurcaseOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("waitingPurcaseOrderByWarehouseIncoming")
    @UnitOfWork
    public Response checkPurcaseOrderByWarehouseIncoming(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PURCASE_ORDER_READ)) {
                Company company = userToken.getUser().getCompany();

                List<PurcaseOrder> purcaseOrders = purcaseOrderDAO.getAll(company);

                List<PurcaseOrderCompareSummaryDTO> purcaseOrderCompareSummaryDTOs = new LinkedList<>();

                for (PurcaseOrder purcaseOrder : purcaseOrders) {
                    if (purcaseOrder.getStockDetail() != null) {
                        List<WarehouseIncoming> warehouseIncomings = warehouseIncomingDAO.getByPoNumberAndStockAndStockDetail(
                                purcaseOrder.getPoNumber(),
                                purcaseOrder.getStock(),
                                purcaseOrder.getStockDetail(),
                                company);

                        Double totalIncomingQuantity = 0.0;

                        for (WarehouseIncoming warehouseIncoming : warehouseIncomings) {
                            totalIncomingQuantity += warehouseIncoming.getIncomingQuantity();
                        }

                        Double calculation = purcaseOrder.getRequirement() - totalIncomingQuantity;

                        if (calculation > 0) {
                            PurcaseOrderCompareSummaryDTO purcaseOrderCompareSummaryDTO = new PurcaseOrderCompareSummaryDTO(
                                    purcaseOrder.getCreationDateTime(),
                                    purcaseOrder.getPoNumber(),
                                    purcaseOrder.getStock().getSupplier().getName(),
                                    purcaseOrder.getStock().getName(),
                                    purcaseOrder.getStock().getCode(),
                                    purcaseOrder.getStockDetail().getName(),
                                    purcaseOrder.getCalculatedRequirement(),
                                    purcaseOrder.getRequirement(),
                                    totalIncomingQuantity,
                                    calculation,
                                    0.0
                            );

                            purcaseOrderCompareSummaryDTOs.add(purcaseOrderCompareSummaryDTO);
                        }

                    } else {
                        List<WarehouseIncoming> warehouseIncomings = warehouseIncomingDAO.getByPoNumberAndStock(
                                purcaseOrder.getPoNumber(),
                                purcaseOrder.getStock(),
                                company);

                        Double totalIncomingQuantity = 0.0;

                        for (WarehouseIncoming warehouseIncoming : warehouseIncomings) {
                            totalIncomingQuantity += warehouseIncoming.getIncomingQuantity();
                        }

                        Double calculation = purcaseOrder.getRequirement() - totalIncomingQuantity;

                        if (calculation > 0) {
                            PurcaseOrderCompareSummaryDTO purcaseOrderCompareSummaryDTO = new PurcaseOrderCompareSummaryDTO(
                                    purcaseOrder.getCreationDateTime(),
                                    purcaseOrder.getPoNumber(),
                                    purcaseOrder.getStock().getSupplier().getName(),
                                    purcaseOrder.getStock().getName(),
                                    purcaseOrder.getStock().getCode(),
                                    null,
                                    purcaseOrder.getCalculatedRequirement(),
                                    purcaseOrder.getRequirement(),
                                    totalIncomingQuantity,
                                    calculation,
                                    0.0
                            );

                            purcaseOrderCompareSummaryDTOs.add(purcaseOrderCompareSummaryDTO);
                        }
                    }
                }

                return Response.ok().entity(purcaseOrderCompareSummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("completedPurcaseOrderByWarehouseIncoming")
    @UnitOfWork
    public Response completedPurcaseOrderByWarehouseIncoming(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PURCASE_ORDER_READ)) {
                Company company = userToken.getUser().getCompany();

                List<PurcaseOrder> purcaseOrders = purcaseOrderDAO.getAll(company);

                List<PurcaseOrderCompareSummaryDTO> purcaseOrderCompareSummaryDTOs = new LinkedList<>();

                for (PurcaseOrder purcaseOrder : purcaseOrders) {
                    if (purcaseOrder.getStockDetail() != null) {
                        List<WarehouseIncoming> warehouseIncomings = warehouseIncomingDAO.getByPoNumberAndStockAndStockDetail(
                                purcaseOrder.getPoNumber(),
                                purcaseOrder.getStock(),
                                purcaseOrder.getStockDetail(),
                                company);

                        Double totalIncomingQuantity = 0.0;

                        for (WarehouseIncoming warehouseIncoming : warehouseIncomings) {
                            totalIncomingQuantity += warehouseIncoming.getIncomingQuantity();
                        }

                        Double calculation = purcaseOrder.getRequirement() - totalIncomingQuantity;

                        if (calculation <= 0) {
                            PurcaseOrderCompareSummaryDTO purcaseOrderCompareSummaryDTO = new PurcaseOrderCompareSummaryDTO(
                                    purcaseOrder.getCreationDateTime(),
                                    purcaseOrder.getPoNumber(),
                                    purcaseOrder.getStock().getSupplier().getName(),
                                    purcaseOrder.getStock().getName(),
                                    purcaseOrder.getStock().getCode(),
                                    purcaseOrder.getStockDetail().getName(),
                                    purcaseOrder.getCalculatedRequirement(),
                                    purcaseOrder.getRequirement(),
                                    totalIncomingQuantity,
                                    0.0,
                                    Math.abs(calculation)
                            );

                            purcaseOrderCompareSummaryDTOs.add(purcaseOrderCompareSummaryDTO);
                        }

                    } else {
                        List<WarehouseIncoming> warehouseIncomings = warehouseIncomingDAO.getByPoNumberAndStock(
                                purcaseOrder.getPoNumber(),
                                purcaseOrder.getStock(),
                                company);

                        Double totalIncomingQuantity = 0.0;

                        for (WarehouseIncoming warehouseIncoming : warehouseIncomings) {
                            totalIncomingQuantity += warehouseIncoming.getIncomingQuantity();
                        }

                        Double calculation = purcaseOrder.getRequirement() - totalIncomingQuantity;

                        if (calculation <= 0) {
                            PurcaseOrderCompareSummaryDTO purcaseOrderCompareSummaryDTO = new PurcaseOrderCompareSummaryDTO(
                                    purcaseOrder.getCreationDateTime(),
                                    purcaseOrder.getPoNumber(),
                                    purcaseOrder.getStock().getSupplier().getName(),
                                    purcaseOrder.getStock().getName(),
                                    purcaseOrder.getStock().getCode(),
                                    null,
                                    purcaseOrder.getCalculatedRequirement(),
                                    purcaseOrder.getRequirement(),
                                    totalIncomingQuantity,
                                    0.0,
                                    Math.abs(calculation)
                            );

                            purcaseOrderCompareSummaryDTOs.add(purcaseOrderCompareSummaryDTO);
                        }
                    }
                }

                return Response.ok().entity(purcaseOrderCompareSummaryDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
