package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.PermissionDAO;
import com.felislabs.izci.dao.dao.RoleDAO;
import com.felislabs.izci.dao.dao.RolePermissionDAO;
import com.felislabs.izci.dao.dao.RoleUserDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 10/09/15.
 */
@Path("permission")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class PermissionResource {
    PermissionDAO permissionDAO;
    RoleDAO roleDAO;
    RolePermissionDAO rolePermissionDAO;
    AuthUtil authUtil;
    RoleUserDAO roleUserDAO;


    public PermissionResource(PermissionDAO permissionDAO, RoleDAO roleDAO, RolePermissionDAO rolePermissionDAO, AuthUtil authUtil, RoleUserDAO roleUserDAO) {
        this.permissionDAO = permissionDAO;
        this.roleDAO = roleDAO;
        this.rolePermissionDAO = rolePermissionDAO;
        this.authUtil = authUtil;
        this.roleUserDAO = roleUserDAO;
    }


    @POST
    @Path("get")
    @UnitOfWork
    public Response get(Map<String, String> permissionData) {
        String token = permissionData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_READ)) {
                String project = permissionData.get("p");

                if (project.equals("1")) {
                    project = "MRP";
                } else if (project.equals("2")) {
                    project = "CRM";
                } else if (project.equals("3")) {
                    project = "FINANCE";
                } else if (project.equals("4")) {
                    project = "FOOTBED";
                }

                Company company = userToken.getUser().getCompany();
                String userCode = userToken.getUser().getCode();

                List<Permission> permissions;

                if (userCode.equals("SAU")) {
                    permissions = permissionDAO.getAll(project, Permission.class, company);
                } else {
                    permissions = permissionDAO.getAllExceptAdminMenus(project, Permission.class, company);
                }

                return Response.ok().entity(permissions).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("assing")
    @UnitOfWork
    public Response assingPermissionToRole(Map<String, String> permissionData) {
        String token = permissionData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_ASSING)) {
                String permissionOid = permissionData.get("selectedPermission");
                String roleOid = permissionData.get("selectedRole");

                Permission permission = permissionDAO.findById(permissionOid);
                Role role = roleDAO.findById(roleOid);

                RolePermission rolePermission = new RolePermission();
                rolePermission.setPermission(permission);
                rolePermission.setRole(role);

                rolePermissionDAO.create(rolePermission);

                return Response.ok().entity(rolePermission).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("assingback")
    @UnitOfWork
    public Response assingBackPermissionFromRole(Map<String, String> permissionData) {
        String token = permissionData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_ASSING)) {
                String permissionOid = permissionData.get("selectedPermission");
                String roleOid = permissionData.get("selectedRole");

                Permission permission = permissionDAO.findById(permissionOid);
                Role role = roleDAO.findById(roleOid);
                RolePermission rolePermission = rolePermissionDAO.getByRoleAndPermission(role, permission);

                rolePermissionDAO.delete(rolePermission);

                return Response.ok().entity(rolePermission).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getpermissionsbyrole")
    @UnitOfWork
    public Response getpermissionsbyrole(Map<String, String> permissionData) {
        String token = permissionData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_READ)) {
                String roleOid = permissionData.get("roleOid");
                List<RolePermission> rolePermissionList = rolePermissionDAO.getByRole(roleDAO.findById(roleOid));

                List<Permission> permissionList = new LinkedList<>();

                for (RolePermission rolePermission : rolePermissionList) {
                    permissionList.add(rolePermission.getPermission());
                }

                return Response.ok().entity(permissionList).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getPermissionByUser/{crmt}")
    @UnitOfWork
    public Response getPermissionByUser(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_READ)) {
                RoleUser roleUser = roleUserDAO.getByUser(userToken.getUser(), userToken.getUser().getCompany());
                List<RolePermission> rolePermissionList = rolePermissionDAO.getByRole(roleUser.getRole());

                List<Permission> permissionList = new LinkedList<>();

                for (RolePermission rolePermission : rolePermissionList) {
                    permissionList.add(rolePermission.getPermission());
                }

                return Response.ok().entity(permissionList).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("setAllPermissionToRole")
    @UnitOfWork
    public Response setAllPermissionToRole(Map<String, String> permissionData) {
        String token = permissionData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_READ)) {
                String roleOid = permissionData.get("roleOid");

                List<Permission> permissionList = permissionDAO.findAll(Permission.class);
                Role role = roleDAO.findById(roleOid);
                for (Permission permission : permissionList) {
                    RolePermission rolePermission = rolePermissionDAO.getByRoleAndPermission(role, permission);

                    if (rolePermission == null) {
                        rolePermission = new RolePermission();
                        rolePermission.setRole(role);
                        rolePermission.setPermission(permission);

                        rolePermissionDAO.create(rolePermission);
                    } else {
                        rolePermission.setDeleteStatus(false);
                        rolePermissionDAO.update(rolePermission);
                    }
                }

                return Response.ok(rolePermissionDAO.getByRole(role)).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("removeAllPermissionToRole")
    @UnitOfWork
    public Response removeAllPermissionToRole(Map<String, String> permissionData) {
        String token = permissionData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_READ)) {
                String roleOid = permissionData.get("roleOid");
                List<RolePermission> rolePermissionList = rolePermissionDAO.getByRole(roleDAO.findById(roleOid));

                for (RolePermission rolePermission : rolePermissionList) {
                    rolePermission.setDeleteStatus(true);
                    rolePermissionDAO.update(rolePermission);
                }

                return Response.ok(rolePermissionDAO.getByRoleUndeleted(roleDAO.findById(roleOid))).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getById")
    @UnitOfWork
    public Response getById(Map<String, String> permissionData) {
        String token = permissionData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_READ)) {
                String oid = permissionData.get("oid");
                Company company = userToken.getUser().getCompany();
                return Response.ok(permissionDAO.getById(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByCode")
    @UnitOfWork
    public Response getByCode(Map<String, String> permissionData) {
        String token = permissionData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PERMISSION_READ)) {
                String code = permissionData.get("code");
                Company company = userToken.getUser().getCompany();
                return Response.ok(permissionDAO.getByCodeContains(code, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
