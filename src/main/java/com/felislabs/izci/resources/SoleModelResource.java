package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import net.coobird.thumbnailator.Thumbnails;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.json.JSONArray;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 26/04/2017.
 */
@Path("soleModel")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SoleModelResource {
    AuthUtil authUtil;
    ModelDAO modelDAO;
    CustomerDAO customerDAO;
    MrpSettingDAO mrpSettingDAO;
    ModelMaterialDAO modelMaterialDAO;
    ModelInternalCriticalDAO modelInternalCriticalDAO;
    ModelExternalCriticalDAO modelExternalCriticalDAO;
    StockDAO stockDAO;
    ModelPriceDetailDAO modelPriceDetailDAO;
    ModelOrderDAO modelOrderDAO;
    StockDetailDAO stockDetailDAO;
    ModelAssortDAO modelAssortDAO;

    public SoleModelResource(AuthUtil authUtil, ModelDAO modelDAO, CustomerDAO customerDAO, MrpSettingDAO mrpSettingDAO, ModelMaterialDAO modelMaterialDAO, ModelInternalCriticalDAO modelInternalCriticalDAO, ModelExternalCriticalDAO modelExternalCriticalDAO, StockDAO stockDAO, ModelPriceDetailDAO modelPriceDetailDAO, ModelOrderDAO modelOrderDAO, StockDetailDAO stockDetailDAO, ModelAssortDAO modelAssortDAO) {
        this.authUtil = authUtil;
        this.modelDAO = modelDAO;
        this.customerDAO = customerDAO;
        this.mrpSettingDAO = mrpSettingDAO;
        this.modelMaterialDAO = modelMaterialDAO;
        this.modelInternalCriticalDAO = modelInternalCriticalDAO;
        this.modelExternalCriticalDAO = modelExternalCriticalDAO;
        this.stockDAO = stockDAO;
        this.modelPriceDetailDAO = modelPriceDetailDAO;
        this.modelOrderDAO = modelOrderDAO;
        this.stockDetailDAO = stockDetailDAO;
        this.modelAssortDAO = modelAssortDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                String customerOid = data.get("customerOid");
                String firmModelCode = data.get("firmModelCode");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                String description = data.get("description");
                String modelColor = data.get("modelColor");
                String imagePaths = data.get("imagePaths");
                String series = data.get("series");

                Company company = userToken.getUser().getCompany();

                Customer customer = customerDAO.getById(customerOid, company);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                String modelCode = "";

                List<MrpSetting> mrpSettings = mrpSettingDAO.get(company);

                modelCode = mrpSettings.get(0).getWorkOrderCount() + "-" + series;

                mrpSettings.get(0).setWorkOrderCount(mrpSettings.get(0).getWorkOrderCount() + 1);

                mrpSettingDAO.update(mrpSettings.get(0));

                Model model = new Model();
                model.setCustomer(customer);
                model.setCustomerModelCode(firmModelCode);
                model.setStartDate(start);
                model.setEndDate(end);
                model.setDescription(description);
                model.setColor(modelColor);
                model.setImagePaths(imagePaths);
                model.setModelCode(modelCode);
                model.setCompany(company);
                model.setUpdateDate(new DateTime());
                model.setUpdater(userToken.getUser());
                model.setSeries(series);

                String from = "";
                String to = "";

                if (System.getProperty("os.name").equals("Windows 10")) {
                    from = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\modelimages\\";
                    to = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\thumbnail\\";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    from = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/modelimages/";
                    to = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/thumbnail/";
                } else {
                    from = "/var/www/html/modelimages/";
                    to = "/var/www/html/thumbnail/";
                }
                if (!imagePaths.equals("[]")) {
                    JSONArray jsonArray = new JSONArray(imagePaths);

                    File dest = new File(to + jsonArray.get(0).toString());

                    try {
                        Thumbnails.of(new File(from + jsonArray.get(0)))
                                .size(100, 150).toFile(dest);

                        model.setDefaultPhoto(jsonArray.get(0).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    model.setDefaultPhoto("blankImage.png");
                }

                modelDAO.create(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu model oluşturuldu. ", new DateTime());

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_UPDATE)) {
                String modelOid = data.get("oid");
                String customerOid = data.get("customerOid");
                String firmModelCode = data.get("firmModelCode");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                String description = data.get("description");
                String modelColor = data.get("modelColor");
                String imagePaths = data.get("imagePaths");
                String series = data.get("series");

                Company company = userToken.getUser().getCompany();

                Customer customer = customerDAO.getById(customerOid, company);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                Model model = modelDAO.getByOid(modelOid, company);

                String modelCode = model.getModelCode().split("-")[0];

                modelCode = modelCode + "-" + series;

                model.setCustomer(customer);
                model.setCustomerModelCode(firmModelCode);
                model.setStartDate(start);
                model.setEndDate(end);
                model.setDescription(description);
                model.setColor(modelColor);
                model.setImagePaths(imagePaths);
                model.setUpdateDate(new DateTime());
                model.setUpdater(userToken.getUser());
                model.setSeries(series);
                model.setModelCode(modelCode);

                modelDAO.update(model);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu model güncellendi. ", new DateTime());

                return Response.ok().entity(model).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

}
