package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.ProcessingDAO;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Processing;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by arricie on 21.03.2017.
 */

@Path("processing")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProcessingResource {
    AuthUtil authUtil;
    ProcessingDAO processingDAO;

    public ProcessingResource(AuthUtil authUtil, ProcessingDAO processingDAO) {
        this.authUtil = authUtil;
        this.processingDAO = processingDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PROCESSING_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");
                Boolean insideCompany = Boolean.parseBoolean(data.get("insideCompany"));
                Company company = userToken.getUser().getCompany();

                Processing processing = processingDAO.getByCode(code, company);

                if (processing != null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Processing processingByCode = processingDAO.getByCode(code, userToken.getUser().getCompany());

                    if (processingByCode != null && !processingByCode.getCode().equals(processingByCode.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        processing = new Processing();
                        processing.setCompany(company);
                        processing.setName(name);
                        processing.setCode(code);
                        processing.setDescription(description);
                        processing.setInsideCompany(insideCompany);
                        processingDAO.create(processing);

                        return Response.ok().entity(processing).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PROCESSING_READ)) {
                List<Processing> processingList = processingDAO.getAll(userToken.getUser().getCompany());
                return Response.ok().entity(processingList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readOutCompany")
    @UnitOfWork
    public Response readOutCompany(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PROCESSING_READ)) {
                List<Processing> processingList = processingDAO.getAllReadOutCompany(userToken.getUser().getCompany());
                return Response.ok().entity(processingList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PROCESSING_READ)) {
                String oid = data.get("oid");

                Processing processing = processingDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(processing).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PROCESSING_UPDATE)) {
                String oid = data.get("oid");

                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");
                Boolean insideCompany = Boolean.parseBoolean(data.get("insideCompany"));
                Company company = userToken.getUser().getCompany();

                Processing processing = processingDAO.getByOid(oid, company);

                if (processing == null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Processing processingByCode = processingDAO.getByCode(code, userToken.getUser().getCompany());

                    if (processingByCode != null && !processingByCode.getCode().equals(processing.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        processing.setCompany(company);
                        processing.setName(name);
                        processing.setCode(code);
                        processing.setDescription(description);
                        processing.setInsideCompany(insideCompany);
                        processingDAO.update(processing);

                        return Response.ok().entity(processing).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PROCESSING_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Processing processing = processingDAO.getByOid(oid, company);
                processing.setDeleteStatus(true);

                processingDAO.update(processing);

                return Response.ok().entity(processing).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }


}
