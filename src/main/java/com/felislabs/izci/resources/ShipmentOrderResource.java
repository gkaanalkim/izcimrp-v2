package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.ModelDAO;
import com.felislabs.izci.dao.dao.ShipmentOrderDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.ShipmentOrderSummaryDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Path("shipmentOrder")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ShipmentOrderResource {
    AuthUtil authUtil;
    ShipmentOrderDAO shipmentOrderDAO;
    ModelDAO modelDAO;

    public ShipmentOrderResource(AuthUtil authUtil, ShipmentOrderDAO shipmentOrderDAO, ModelDAO modelDAO) {
        this.authUtil = authUtil;
        this.shipmentOrderDAO = shipmentOrderDAO;
        this.modelDAO = modelDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_ORDER_CREATE)) {
                String modelOid = data.get("modelOid");
                String parcelType = data.get("parcelType");
                String parcelQuantity = data.get("parcelQuantity");
                String warehouseShipTo = data.get("warehouseShipTo");
                String shipmentDate = data.get("shipmentDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime sd = formatter.parseDateTime(shipmentDate);

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());

                ShipmentOrder shipmentOrder = new ShipmentOrder();
                shipmentOrder.setCompany(userToken.getUser().getCompany());
                shipmentOrder.setUpdater(userToken.getUser());
                shipmentOrder.setUpdateDate(new DateTime());
                shipmentOrder.setParcelType(parcelType);
                shipmentOrder.setParcelQuantity(parcelQuantity);
                shipmentOrder.setWarehouseShipTo(warehouseShipTo);
                shipmentOrder.setShipmentDate(sd);
                shipmentOrder.setModel(model);

                shipmentOrderDAO.create(shipmentOrder);

                return Response.ok().entity(shipmentOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_ORDER_UPDATE)) {
                String shipmentOrderOid = data.get("shipmentOrderOid");

                String modelOid = data.get("modelOid");
                String parcelType = data.get("parcelType");
                String parcelQuantity = data.get("parcelQuantity");
                String warehouseShipTo = data.get("warehouseShipTo");
                String shipmentDate = data.get("shipmentDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime sd = formatter.parseDateTime(shipmentDate);

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());

                ShipmentOrder shipmentOrder = shipmentOrderDAO.getByOid(shipmentOrderOid, userToken.getUser().getCompany());
                shipmentOrder.setCompany(userToken.getUser().getCompany());
                shipmentOrder.setUpdater(userToken.getUser());
                shipmentOrder.setUpdateDate(new DateTime());
                shipmentOrder.setParcelType(parcelType);
                shipmentOrder.setParcelQuantity(parcelQuantity);
                shipmentOrder.setWarehouseShipTo(warehouseShipTo);
                shipmentOrder.setShipmentDate(sd);
                shipmentOrder.setModel(model);

                shipmentOrderDAO.update(shipmentOrder);

                return Response.ok().entity(shipmentOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByOid")
    @UnitOfWork
    public Response readByOid(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_ORDER_READ)) {
                String shipmentOrderOid = data.get("shipmentOrderOid");

                ShipmentOrder shipmentOrder = shipmentOrderDAO.getByOid(shipmentOrderOid, userToken.getUser().getCompany());

                return Response.ok().entity(shipmentOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_ORDER_READ)) {
                List<ShipmentOrder> shipmentOrders = shipmentOrderDAO.getAll(userToken.getUser().getCompany());

                List<ShipmentOrderSummaryDTO> shipmentOrderSummaryDTOS = new LinkedList<>();

                for (ShipmentOrder shipmentOrder : shipmentOrders) {
                    ShipmentOrderSummaryDTO shipmentOrderSummaryDTO = new ShipmentOrderSummaryDTO(
                            shipmentOrder.getOid(),
                            shipmentOrder.getModel().getDefaultPhoto(),
                            shipmentOrder.getModel().getCustomer().getName(),
                            shipmentOrder.getModel().getOid(),
                            shipmentOrder.getModel().getModelCode(),
                            shipmentOrder.getModel().getColor(),
                            shipmentOrder.getModel().getCustomerModelCode(),
                            shipmentOrder.getParcelType(),
                            shipmentOrder.getParcelQuantity(),
                            shipmentOrder.getWarehouseShipTo(),
                            shipmentOrder.getShipmentDate()
                    );

                    shipmentOrderSummaryDTOS.add(shipmentOrderSummaryDTO);
                }

                return Response.ok().entity(shipmentOrderSummaryDTOS).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByDateRange")
    @UnitOfWork
    public Response readByDateRange(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_ORDER_READ)) {
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                List<ShipmentOrder> shipmentOrders = shipmentOrderDAO.getByDateRange(start, end, userToken.getUser().getCompany());

                List<ShipmentOrderSummaryDTO> shipmentOrderSummaryDTOS = new LinkedList<>();

                for (ShipmentOrder shipmentOrder : shipmentOrders) {
                    ShipmentOrderSummaryDTO shipmentOrderSummaryDTO = new ShipmentOrderSummaryDTO(
                            shipmentOrder.getOid(),
                            shipmentOrder.getModel().getDefaultPhoto(),
                            shipmentOrder.getModel().getCustomer().getName(),
                            shipmentOrder.getModel().getOid(),
                            shipmentOrder.getModel().getModelCode(),
                            shipmentOrder.getModel().getColor(),
                            shipmentOrder.getModel().getCustomerModelCode(),
                            shipmentOrder.getParcelType(),
                            shipmentOrder.getParcelQuantity(),
                            shipmentOrder.getWarehouseShipTo(),
                            shipmentOrder.getShipmentDate()
                    );

                    shipmentOrderSummaryDTOS.add(shipmentOrderSummaryDTO);
                }

                return Response.ok().entity(shipmentOrderSummaryDTOS).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("delete")
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SHIPMENT_ORDER_DELETE)) {
                String shipmentOrderOid = data.get("shipmentOrderOid");

                ShipmentOrder shipmentOrder = shipmentOrderDAO.getByOid(shipmentOrderOid, userToken.getUser().getCompany());
                shipmentOrder.setDeleteStatus(true);

                shipmentOrderDAO.update(shipmentOrder);

                return Response.ok().entity(shipmentOrder).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
