package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.MontaRequirementDTO;
import com.felislabs.izci.representation.dto.StockAndStockDetailDTO;
import com.felislabs.izci.representation.dto.StockDTO;
import com.felislabs.izci.representation.dto.WorkOrderImageDTO;
import com.felislabs.izci.representation.entities.*;
import com.felislabs.izci.util.FileUtil;
import com.opencsv.CSVReader;
import io.dropwizard.hibernate.UnitOfWork;
import net.coobird.thumbnailator.Thumbnails;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

/**
 * Created by aykutarici on 19/12/16.
 */

@Path("stock")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class StockResources {
    StockDAO stockDAO;
    AuthUtil authUtil;
    SupplierDAO supplierDAO;
    StockTypeDAO stockTypeDAO;
    MrpSettingDAO mrpSettingDAO;
    StockDetailDAO stockDetailDAO;
    WarehouseDAO warehouseDAO;
    CuttingRequirementDAO cuttingRequirementDAO;
    MontaRequirementDAO montaRequirementDAO;
    ModelCuttingOrderDAO modelCuttingOrderDAO;
    ModelMontaDAO modelMontaDAO;

    public StockResources(StockDAO stockDAO, AuthUtil authUtil, SupplierDAO supplierDAO, StockTypeDAO stockTypeDAO, MrpSettingDAO mrpSettingDAO, StockDetailDAO stockDetailDAO,
                          WarehouseDAO warehouseDAO, CuttingRequirementDAO cuttingRequirementDAO, MontaRequirementDAO montaRequirementDAO, ModelCuttingOrderDAO modelCuttingOrderDAO, ModelMontaDAO modelMontaDAO) {
        this.stockDAO = stockDAO;
        this.authUtil = authUtil;
        this.supplierDAO = supplierDAO;
        this.stockTypeDAO = stockTypeDAO;
        this.mrpSettingDAO = mrpSettingDAO;
        this.stockDetailDAO = stockDetailDAO;
        this.warehouseDAO = warehouseDAO;
        this.cuttingRequirementDAO = cuttingRequirementDAO;
        this.montaRequirementDAO = montaRequirementDAO;
        this.modelCuttingOrderDAO = modelCuttingOrderDAO;
        this.modelMontaDAO = modelMontaDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_CREATE)) {
                String supplierOid = data.get("supplierOid");
                String stockTypeOid = data.get("stockTypeOid");
                String stockName = data.get("stockName");
                String stockCode = data.get("stockCode");
                String warehouseOid = data.get("warehouseOid");
                String unit = data.get("stockUnit");
                String price = data.get("stockPrice");
                String stockQuantity = data.get("stockQuantity");
                String stockCurrency = data.get("stockCurrency");
                String stockCriticalThreshold = data.get("stockCriticalThreshold");
                String stockDescription = data.get("stockDescription");
                String stockDetails = data.get("stockDetails");
                String imagePath = data.get("imagePath");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Supplier supplier = null;
                if (supplierOid != null && !supplierOid.equals("")) {
                    supplier = supplierDAO.getById(supplierOid, company);
                }

                StockType stockType = stockTypeDAO.getById(stockTypeOid, company);

                Warehouse warehouse = null;
                if (warehouseOid != null && !warehouseOid.equals("")) {
                    warehouse = warehouseDAO.getById(warehouseOid, company);
                }

                MrpSetting mrpSetting = mrpSettingDAO.get(company).get(0);
                String newStockCode = stockType.getCode() + "-" + stockCode + "-" + mrpSetting.getStockCodeCounter();

                Stock stock = new Stock();
                stock.setCompany(company);
                stock.setUpdater(user);
                stock.setUpdateDate(new DateTime());
                stock.setSupplier(supplier);
                stock.setStockType(stockType);
                stock.setWarehouse(warehouse);
                stock.setName(stockName);
                stock.setCode(stockCode);
                stock.setUnit(unit);
                stock.setImagePath(imagePath);
                if (price.equals("")) {
                    stock.setPrice(0.0);
                } else {
                    stock.setPrice(Double.valueOf(price));

                }
                if (stockQuantity.equals("")) {
                    stock.setQuantity(0.0);
                } else {
                    stock.setQuantity(Double.valueOf(stockQuantity));
                }

                stock.setCurrency(stockCurrency);
                if (stockCriticalThreshold.equals("")) {
                    stock.setCriticalThreshold(0.0);
                } else {
                    stock.setCriticalThreshold(Double.valueOf(stockCriticalThreshold));

                }
                stock.setDescription(stockDescription);
                stock.setCode(newStockCode);

                stockDAO.create(stock);

                JSONArray jsonArray = new JSONArray(stockDetails);

                if (jsonArray.length() != 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String detail = jsonObject.getString("description");
                        String name = jsonObject.getString("name");
                        String quantity = jsonObject.getString("quantity");

                        StockDetail stockDetail = new StockDetail();
                        stockDetail.setCompany(company);
                        stockDetail.setUpdateDate(new DateTime());
                        stockDetail.setUpdater(user);
                        stockDetail.setDescription(detail);
                        stockDetail.setName(name);
                        stockDetail.setQuantity(Double.parseDouble(quantity));
                        stockDetail.setStock(stock);

                        stockDetailDAO.create(stockDetail);
                    }
                }

                mrpSetting.setStockCodeCounter(mrpSetting.getStockCodeCounter() + 1);
                mrpSettingDAO.update(mrpSetting);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), stock.getCode() + " kodlu stok oluşturuldu. ", new DateTime());

                return Response.ok().entity(stock).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_DELETE)) {
                String stockOid = data.get("stockOid");
                String supplierOid = data.get("supplierOid");
                String stockTypeOid = data.get("stockTypeOid");
                String stockName = data.get("stockName");
                String stockCode = data.get("stockCode");
                String warehouseOid = data.get("warehouseOid");
                String unit = data.get("stockUnit");
                String price = data.get("stockPrice");
                String stockQuantity = data.get("stockQuantity");
                String stockCurrency = data.get("stockCurrency");
                String stockCriticalThreshold = data.get("stockCriticalThreshold");
                String stockDescription = data.get("stockDescription");
                String stockDetails = data.get("stockDetails");
                String imagePath = data.get("imagePath");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Supplier supplier = supplierDAO.getById(supplierOid, company);
                StockType stockType = stockTypeDAO.getById(stockTypeOid, company);
                Warehouse warehouse = warehouseDAO.getById(warehouseOid, company);

                Stock stock = stockDAO.getById(stockOid, company);
                stock.setCompany(company);
                stock.setUpdater(user);
                stock.setUpdateDate(new DateTime());
                stock.setSupplier(supplier);
                stock.setStockType(stockType);
                stock.setWarehouse(warehouse);
                stock.setName(stockName);
                stock.setCode(stockCode);
                stock.setUnit(unit);
                stock.setPrice(Double.valueOf(price));
                stock.setQuantity(Double.valueOf(stockQuantity));
                stock.setCurrency(stockCurrency);
                stock.setCriticalThreshold(Double.valueOf(stockCriticalThreshold));
                stock.setDescription(stockDescription);
                stock.setImagePath(imagePath);

                stockDAO.update(stock);

                JSONArray jsonArray = new JSONArray(stockDetails);

                if (jsonArray.length() != 0) {
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        String oid = jsonObject.getString("oid");
                        String detail = jsonObject.getString("description");
                        String name = jsonObject.getString("name");
                        String quantity = jsonObject.getString("quantity");

                        StockDetail stockDetail = stockDetailDAO.getByOid(oid, company);

                        if (stockDetail != null) {
                            stockDetail.setCompany(company);
                            stockDetail.setUpdateDate(new DateTime());
                            stockDetail.setUpdater(user);
                            stockDetail.setDescription(detail);
                            stockDetail.setName(name);
                            stockDetail.setQuantity(Double.parseDouble(quantity));
                            stockDetail.setStock(stock);

                            stockDetailDAO.update(stockDetail);
                        } else {
                            stockDetail = new StockDetail();
                            stockDetail.setCompany(company);
                            stockDetail.setUpdateDate(new DateTime());
                            stockDetail.setUpdater(user);
                            stockDetail.setDescription(detail);
                            stockDetail.setName(name);
                            stockDetail.setQuantity(Double.parseDouble(quantity));
                            stockDetail.setStock(stock);

                            stockDetailDAO.create(stockDetail);
                        }
                    }
                }

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), stock.getCode() + " kodlu stok güncellendi. ", new DateTime());

                return Response.ok().entity(stock).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                List<Stock> stocks = stockDAO.getAll(userToken.getUser().getCompany());
                List<StockDTO> stockDTOs = new LinkedList();
                for (Stock stock : stocks) {
                    StockDTO stockDTO = new StockDTO();
                    stockDTO.setOid(stock.getOid());
                    stockDTO.setSupplierName(stock.getSupplier() != null ? stock.getSupplier().getName() : null);
                    stockDTO.setSupplierCode(stock.getSupplier() != null ? stock.getSupplier().getCode() : null);
                    stockDTO.setStockTypeName(stock.getStockType().getName());
                    stockDTO.setName(stock.getName());
                    stockDTO.setCode(stock.getCode());
                    stockDTO.setPrice(stock.getPrice());
                    stockDTO.setCurrency(stock.getCurrency());
                    stockDTO.setQuantity(stock.getQuantity());
                    stockDTO.setUnit(stock.getUnit());
                    stockDTO.setCriticalThreshold(stock.getCriticalThreshold());
                    if (stock.getWarehouse() == null) {
                        stockDTO.setWarehouseCode("");
                        stockDTO.setWarehouseName("");
                    } else {
                        stockDTO.setWarehouseCode(stock.getWarehouse().getCode());
                        stockDTO.setWarehouseName(stock.getWarehouse().getName());
                    }
                    stockDTOs.add(stockDTO);
                }
                return Response.ok().entity(stockDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readFinishedStock")
    @UnitOfWork
    public Response readFinishedStock(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                List<Stock> stocks = stockDAO.getFinishedStock(userToken.getUser().getCompany());

                return Response.ok().entity(stocks).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                String stockOid = data.get("stockOid");

                Stock stock = stockDAO.getById(stockOid, userToken.getUser().getCompany());

                return Response.ok().entity(stock).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readStockDetail")
    @UnitOfWork
    public Response readStockDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                String stockOid = data.get("stockOid");

                Stock stock = stockDAO.getById(stockOid, userToken.getUser().getCompany());

                List<StockDetail> stockDetails = stockDetailDAO.getByStock(stock, userToken.getUser().getCompany());

                return Response.ok().entity(stockDetails).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_DELETE)) {
                String stockOid = data.get("stockOid");

                Stock stock = stockDAO.getById(stockOid, userToken.getUser().getCompany());

                List<StockDetail> stockDetails = stockDetailDAO.getByStock(stock, userToken.getUser().getCompany());

                if (stockDetails.size() != 0) {
                    for (StockDetail stockDetail : stockDetails) {
                        stockDetail.setDeleteStatus(true);
                        stockDetailDAO.update(stockDetail);
                    }
                }

                stock.setDeleteStatus(true);
                stockDAO.update(stock);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), stock.getCode() + " kodlu stok silindi. ", new DateTime());

                return Response.ok().entity(stock).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    // Stok-kabul.html KendoAutoComplete için.
    @POST
    @Path("read/{crmt}/{stockCode}")
    @UnitOfWork
    public Response readDetailByCode(@PathParam("crmt") String crmt, @PathParam("stockCode") String stockCode) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                Company company = userToken.getUser().getCompany();
                List<Stock> stocks = stockDAO.getByContainsInCode(stockCode, company);

                return Response.ok(stocks).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteStockDetail")
    @UnitOfWork
    public Response deleteStockDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_DELETE)) {
                String stockDetailOid = data.get("oid");

                StockDetail stockDetail = stockDetailDAO.getByOid(stockDetailOid, userToken.getUser().getCompany());

                stockDetail.setDeleteStatus(true);
                stockDetailDAO.update(stockDetail);

                return Response.ok(stockDetail).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readForGrid")
    @UnitOfWork
    public Response readForGrid(Map<String, String> data) {
        String token = data.get("crmt");
        String oid = data.get("stockOid");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                List<Stock> stocks = stockDAO.stockList(oid, userToken.getUser().getCompany());

                return Response.ok().entity(stocks).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("warehouseOutgoingForCutting")
    @UnitOfWork
    public Response warehouseOutgoingForCutting(Map<String, String> data) {
        String token = data.get("crmt");
        String documentNumber = data.get("documentNumber");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                Company company = userToken.getUser().getCompany();
                ModelCuttingOrder modelCuttingOrder = modelCuttingOrderDAO.getByDocumentNumber(documentNumber, company);

                if (modelCuttingOrder != null) {
                    List<CuttingRequirement> cuttingRequirements = cuttingRequirementDAO.getByModelCuttingOrder(modelCuttingOrder, company);

                    if (cuttingRequirements.get(2).getCompleted().equals(false)) {

                        ArrayList<String> errorArray = new ArrayList<String>();
                        for (int i = 0; i < cuttingRequirements.size(); i++) {
                            String error = "";
                            if (cuttingRequirements.get(i).getStockDetail() == null) {
                                if (cuttingRequirements.get(i).getStock().getQuantity() < cuttingRequirements.get(i).getQuantity()) {
                                    double requiremet = cuttingRequirements.get(i).getQuantity() - cuttingRequirements.get(i).getStock().getQuantity();
                                    error = cuttingRequirements.get(i).getStock().getCode() + "'kodlu Stok'dan " + requiremet + "(" + cuttingRequirements.get(i).getStock().getUnit() + ") Eksik";
                                    errorArray.add(error);

                                }
                            } else {
                                if (cuttingRequirements.get(i).getStockDetail().getQuantity() < cuttingRequirements.get(i).getQuantity()) {
                                    double requiremet = cuttingRequirements.get(i).getQuantity() - cuttingRequirements.get(i).getStock().getQuantity();
                                    error = cuttingRequirements.get(i).getStock().getCode() + "/" + cuttingRequirements.get(i).getStockDetail().getName() + "'kodlu Stok'dan " + requiremet + "(" + cuttingRequirements.get(i).getStock().getUnit() + ") Eksik";
                                    errorArray.add(error);
                                }
                            }
                        }
                        if (errorArray.size() == 0) {
                            for (int j = 0; j < cuttingRequirements.size(); j++) {
                                if (cuttingRequirements.get(j).getStockDetail() == null) {
                                    Stock stock = cuttingRequirements.get(j).getStock();
                                    stock.setQuantity(stock.getQuantity() - cuttingRequirements.get(j).getQuantity());
                                    stockDAO.update(stock);

                                    CuttingRequirement cuttingRequirement = cuttingRequirements.get(j);
                                    cuttingRequirement.setCompleted(true);
                                    cuttingRequirementDAO.update(cuttingRequirement);
                                } else {
                                    StockDetail stockDetail = cuttingRequirements.get(j).getStockDetail();
                                    stockDetail.setQuantity(stockDetail.getQuantity() - cuttingRequirements.get(j).getQuantity());
                                    stockDetailDAO.update(stockDetail);

                                    Stock stock = cuttingRequirements.get(j).getStock();
                                    stock.setQuantity(stock.getQuantity() - cuttingRequirements.get(j).getQuantity());
                                    stockDAO.update(stock);

                                    CuttingRequirement cuttingRequirement = cuttingRequirements.get(j);
                                    cuttingRequirement.setCompleted(true);
                                    cuttingRequirementDAO.update(cuttingRequirement);
                                }
                            }
                            return Response.ok().build();
                        } else {
                            return Response.ok().entity(errorArray).build();
                        }
                    } else {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_REQUIREMENT.getErrorAlias()).build();
                    }
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_CONTROL_CUTTING_REQUIREMENT.getErrorAlias()).build();
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("warehouseOutgoingForMonta")
    @UnitOfWork
    public Response warehouseOutgoingForMonta(Map<String, String> data) {
        String token = data.get("crmt");
        String documentNumber = data.get("documentNumber");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                Company company = userToken.getUser().getCompany();
                ModelMonta modelMonta = modelMontaDAO.getByDocumentNumber(documentNumber, company);

                if (modelMonta != null) {
                    List<MontaRequirement> montaRequirements = montaRequirementDAO.getByModelMonta(modelMonta, company);

                    if (montaRequirements.get(2).getCompleted().equals(false)) {

                        ArrayList<String> errorArray = new ArrayList<String>();
                        for (int i = 0; i < montaRequirements.size(); i++) {
                            String error = "";
                            if (montaRequirements.get(i).getStockDetail() == null) {
                                if (montaRequirements.get(i).getStock().getQuantity() < montaRequirements.get(i).getQuantity()) {
                                    double requiremet = montaRequirements.get(i).getQuantity() - montaRequirements.get(i).getStock().getQuantity();
                                    error = montaRequirements.get(i).getStock().getCode() + "'kodlu Stok'dan " + requiremet + "(" + montaRequirements.get(i).getStock().getUnit() + ") Eksik";
                                    errorArray.add(error);

                                }
                            } else {
                                if (montaRequirements.get(i).getStockDetail().getQuantity() < montaRequirements.get(i).getQuantity()) {
                                    double requiremet = montaRequirements.get(i).getQuantity() - montaRequirements.get(i).getStock().getQuantity();
                                    error = montaRequirements.get(i).getStock().getCode() + "/" + montaRequirements.get(i).getStockDetail().getName() + "'kodlu Stok'dan " + requiremet + "(" + montaRequirements.get(i).getStock().getUnit() + ") Eksik";
                                    errorArray.add(error);
                                }
                            }
                        }

                        if (errorArray.size() == 0) {
                            for (int j = 0; j < montaRequirements.size(); j++) {
                                if (montaRequirements.get(j).getStockDetail() == null) {
                                    Stock stock = montaRequirements.get(j).getStock();
                                    stock.setQuantity(stock.getQuantity() - montaRequirements.get(j).getQuantity());
                                    stockDAO.update(stock);

                                    MontaRequirement montaRequirement = montaRequirements.get(j);
                                    montaRequirement.setCompleted(true);
                                    montaRequirementDAO.update(montaRequirement);
                                } else {
                                    StockDetail stockDetail = montaRequirements.get(j).getStockDetail();
                                    stockDetail.setQuantity(stockDetail.getQuantity() - montaRequirements.get(j).getQuantity());
                                    stockDetailDAO.update(stockDetail);

                                    Stock stock = montaRequirements.get(j).getStock();
                                    stock.setQuantity(stock.getQuantity() - montaRequirements.get(j).getQuantity());
                                    stockDAO.update(stock);


                                    MontaRequirement montaRequirement = montaRequirements.get(j);
                                    montaRequirement.setCompleted(true);
                                    montaRequirementDAO.update(montaRequirement);
                                }
                            }
                            return Response.ok().build();
                        } else {
                            return Response.ok().entity(errorArray).build();
                        }
                    } else {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_REQUIREMENT.getErrorAlias()).build();
                    }
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_CONTROL_MONTA_REQUIREMENT.getErrorAlias()).build();
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readStockDetailForMonta")
    @UnitOfWork
    public Response readStockDetailForMonta(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                String stockOid = data.get("stockOids");

                List<MontaRequirementDTO> montaRequirementDTOS = new LinkedList<MontaRequirementDTO>();

                JSONArray arry = new JSONArray(stockOid);
                for (int i = 0; i < arry.length(); i++) {
                    JSONObject jsonObject = arry.getJSONObject(i);
                    Stock stock = stockDAO.getById(jsonObject.getString("stockOid"), userToken.getUser().getCompany());

                    List<StockDetail> stockDetails = stockDetailDAO.getByStock(stock, userToken.getUser().getCompany());
                    MontaRequirementDTO montaRequirementDTO = new MontaRequirementDTO();
                    montaRequirementDTO.setStock(stock);
                    montaRequirementDTO.setStockDetailList(stockDetails);

                    montaRequirementDTOS.add(montaRequirementDTO);
                }
                return Response.ok().entity(montaRequirementDTOS).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("/uploadImage/{crmt}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @UnitOfWork
    public Response uploadImage(
            @PathParam("crmt") String crmt,
            @FormDataParam("stockImage") InputStream uploadedInputStream,
            @FormDataParam("stockImage") FormDataContentDisposition fileDetail) {

        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MODEL_CREATE)) {
                //  ŞİRKET BİLGİLERİ
                String osBasePath = "";
                String companyImageFolder = userToken.getUser().getCompany().getImageFolder();

                if (System.getProperty("os.name").equals("Windows 10")) {
                    osBasePath = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\stockimages\\" + companyImageFolder + "/";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    osBasePath = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/stockimages/" + companyImageFolder + "/";
                } else {
                    osBasePath = "/var/www/html/stockimages/" + companyImageFolder + "/";
                }

                String fileExtension = "";

                if (fileDetail.getFileName().endsWith(".png") || fileDetail.getFileName().endsWith(".PNG")) {
                    fileExtension = ".png";
                } else if (fileDetail.getFileName().endsWith(".jpg") || fileDetail.getFileName().endsWith(".JPG") || fileDetail.getFileName().endsWith(".JPEG") || fileDetail.getFileName().endsWith(".jpeg")) {
                    fileExtension = ".jpg";
                }

                DateTime fileNameDT = new DateTime();

                String fileName = fileNameDT.getMillis() + Math.random() + fileExtension;

                String uploadedFileLocation = osBasePath + fileName;

                // save it with thumbnail
                try {
                    BufferedImage inBuff = ImageIO.read(uploadedInputStream);
                    int w = inBuff.getWidth();
                    int h = inBuff.getHeight();
                    if (h > w) {
                        try {
                            Thumbnails.of(inBuff).rotate(90).size(750, 750).toFile(uploadedFileLocation);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Thumbnails.of(inBuff).size(750, 750).toFile(uploadedFileLocation);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                WorkOrderImageDTO workOrderImageDTO = new WorkOrderImageDTO();
                workOrderImageDTO.setPath(fileName);

                return Response.ok(workOrderImageDTO).build();
            }
        }

        return Response.ok().build();
    }

    @POST
    @Path("readStockAndStockDetail")
    @UnitOfWork
    public Response readStockAndStockDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.STOCK_READ)) {
                String stockOid = data.get("stockOid");

                Stock stock = stockDAO.getById(stockOid, userToken.getUser().getCompany());

                List<StockDetail> stockDetails = stockDetailDAO.getByStock(stock, userToken.getUser().getCompany());

                StockAndStockDetailDTO stockAndStockDetailDTO = new StockAndStockDetailDTO(stock, stockDetails);

                return Response.ok().entity(stockAndStockDetailDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
