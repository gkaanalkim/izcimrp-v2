package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.MaterialTypeDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.MaterialType;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by aykutarici on 06/12/16.
 */
@Path("/materialType")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MaterialTypeResource {
    MaterialTypeDAO materialTypeDAO;
    AuthUtil authUtil;

    public MaterialTypeResource(MaterialTypeDAO materialTypeDAO, AuthUtil authUtil) {
        this.materialTypeDAO = materialTypeDAO;
        this.authUtil = authUtil;
    }

    @POST
    @Path("get")
    @UnitOfWork
    public Response getAll(Map<String, String> materialTypesData) {
        String token = materialTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MATERIAL_TYPE_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(materialTypeDAO.getAll(MaterialType.class, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MATERIAL_TYPE_READ)) {
                String oid = data.get("oid");
                MaterialType materialType = materialTypeDAO.getById(oid, userToken.getUser().getCompany());
                return Response.ok(materialType).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> materialTypesData) {
        String token = materialTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MATERIAL_TYPE_CREATE)) {
                String title = materialTypesData.get("title");
                String group = materialTypesData.get("group");
                String isDefault = materialTypesData.get("isDefault");

                Company company = userToken.getUser().getCompany();
                MaterialType materialType = materialTypeDAO.getByTitle(title, company);

                if (materialType == null) {
                    materialType = new MaterialType();

                    materialType.setTitle(title);
                    materialType.setGroupName(group);
                    materialType.setCompany(company);
                    materialType.setPutative(Boolean.valueOf(isDefault));
                    materialType.setUpdateDate(new DateTime());
                    materialType.setUpdater(userToken.getUser());
                    materialTypeDAO.create(materialType);

                    authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), materialType.getTitle() + " başlıklı malzeme türü oluşturuldu. ", new DateTime());


                    return Response.ok(materialType).build();
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_MATERIAL_TYPE.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> materialTypesData) {
        String token = materialTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MATERIAL_TYPE_UPDATE)) {
                Company company = userToken.getUser().getCompany();

                String oid = materialTypesData.get("oid");
                String title = materialTypesData.get("title");
                String group = materialTypesData.get("group");
                String isDefault = materialTypesData.get("isDefault");

                String oldTitle = materialTypeDAO.getById(oid, company).getTitle();
                MaterialType titleControl = materialTypeDAO.getByTitle(title, company);

                if (titleControl != null && titleControl.getTitle() != oldTitle) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_MATERIAL_TYPE.getErrorAlias()).build();
                }

                MaterialType materialType = materialTypeDAO.getById(oid, company);

                materialType.setTitle(title);
                materialType.setGroupName(group);
                materialType.setCompany(company);
                materialType.setPutative(Boolean.valueOf(isDefault));
                materialType.setUpdateDate(new DateTime());
                materialType.setUpdater(userToken.getUser());

                materialTypeDAO.update(materialType);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), materialType.getTitle() + " başlıklı malzeme türü güncellendi. ", new DateTime());

                return Response.ok(materialType).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> materialTypesData) {
        String token = materialTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MATERIAL_TYPE_DELETE)) {
                Company company = userToken.getUser().getCompany();
                String oid = materialTypesData.get("oid");
                MaterialType materialTypes = materialTypeDAO.getById(oid, company);
                materialTypeDAO.delete(materialTypes);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), materialTypes.getTitle() + " başlıklı malzeme türü silindi. ", new DateTime());

                return Response.ok(materialTypes).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getById")
    @UnitOfWork
    public Response getById(Map<String, String> materialTypesData) {
        String token = materialTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MATERIAL_TYPE_READ)) {
                String oid = materialTypesData.get("oid");
                Company company = userToken.getUser().getCompany();
                return Response.ok(materialTypeDAO.getById(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByTitle")
    @UnitOfWork
    public Response getByTitle(Map<String, String> materialTypesData) {
        String token = materialTypesData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MATERIAL_TYPE_READ)) {
                String code = materialTypesData.get("title");
                Company company = userToken.getUser().getCompany();
                List<MaterialType> materialTypes = materialTypeDAO.getByInContainsTitle(code, company);


                return Response.ok(materialTypes).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


    @POST
    @Path("read/{crmt}")
    @UnitOfWork
    public Response read(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_READ)) {
                List<MaterialType> materialTypes = materialTypeDAO.getAll(MaterialType.class, userToken.getUser().getCompany());
                return Response.ok(materialTypes).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetailList")
    @UnitOfWork
    public Response readDetailForSearch(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.INTERNAL_CRITICAL_READ)) {
                String oid = data.get("oid");
                List<MaterialType> materialTypes = materialTypeDAO.getByOidforSearch(oid, userToken.getUser().getCompany());
                return Response.ok(materialTypes).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

}
