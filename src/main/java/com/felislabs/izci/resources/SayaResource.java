package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.*;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.SayaDTO;
import com.felislabs.izci.representation.dto.SayaIncomingSummary;
import com.felislabs.izci.representation.dto.SayaOrderSummary;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 01/02/2017.
 */
@Path("saya")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SayaResource {
    AuthUtil authUtil;
    ModelDAO modelDAO;
    ModelOrderDAO modelOrderDAO;
    ModelSayaDAO modelSayaDAO;
    SayaciDAO sayaciDAO;
    ModelAssortDAO modelAssortDAO;
    ModelSayaIncomingDAO modelSayaIncomingDAO;
    ModelPriceDetailDAO modelPriceDetailDAO;

    public SayaResource(AuthUtil authUtil, ModelDAO modelDAO, ModelOrderDAO modelOrderDAO, ModelSayaDAO modelSayaDAO, SayaciDAO sayaciDAO, ModelAssortDAO modelAssortDAO, ModelSayaIncomingDAO modelSayaIncomingDAO, ModelPriceDetailDAO modelPriceDetailDAO) {
        this.authUtil = authUtil;
        this.modelDAO = modelDAO;
        this.modelOrderDAO = modelOrderDAO;
        this.modelSayaDAO = modelSayaDAO;
        this.sayaciDAO = sayaciDAO;
        this.modelAssortDAO = modelAssortDAO;
        this.modelSayaIncomingDAO = modelSayaIncomingDAO;
        this.modelPriceDetailDAO = modelPriceDetailDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_CREATE)) {
                Company company = userToken.getUser().getCompany();

                String modelOid = data.get("modelOid");
                String sayaciCode = data.get("sayaciCode");
                String quantity = data.get("quantity");
                String description = data.get("description");
                String startDate = data.get("startDate");
                String endDate = data.get("endDate");
                String selectedAssort = data.get("selectedAssort");

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime start = formatter.parseDateTime(startDate);
                DateTime end = formatter.parseDateTime(endDate);
                if (end.isBefore(start)) {
                    return Response.serverError().entity(ErrorMessage.TR_DATE_CONTROL.getErrorAlias()).build();
                }

                Model model = modelDAO.getByOid(modelOid, company);
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);
                Sayaci sayaci = sayaciDAO.getByCode(sayaciCode, company);

                ModelSaya modelSaya = new ModelSaya();
                modelSaya.setCompany(company);
                modelSaya.setUpdater(userToken.getUser());
                modelSaya.setUpdateDate(new DateTime());
                modelSaya.setModelOrder(modelOrder);
                modelSaya.setQuantity(Integer.valueOf(quantity));
                modelSaya.setSayaci(sayaci);
                modelSaya.setStartDate(start);
                modelSaya.setEndDate(end);
                modelSaya.setDescription(description);
                modelSaya.setSelectedAssortUid(selectedAssort);

                DateTime now = new DateTime();

                String documentNumber = sayaciCode + "-" + now.getYear() + now.getMonthOfYear() + now.getDayOfMonth() + "-" + sayaci.getSayaOrderCounter();
                sayaci.setSayaOrderCounter(sayaci.getSayaOrderCounter() + 1);
                sayaciDAO.update(sayaci);

                modelSaya.setDocumentNumber(documentNumber);

                modelSayaDAO.create(modelSaya);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), model.getModelCode() + " kodlu modelin " + modelSaya.getQuantity() + " çifti sayaya verildi.", new DateTime());

                return Response.ok().entity(modelSaya).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_READ)) {
                Company company = userToken.getUser().getCompany();

                return Response.ok().entity(modelSayaDAO.getAll(userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_READ)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                return Response.ok().entity(modelSayaDAO.getByOid(oid, userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByModel")
    @UnitOfWork
    public Response readByModel(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_READ)) {
                String modelOid = data.get("modelOid");

                Company company = userToken.getUser().getCompany();

                Model model = modelDAO.getByOid(modelOid, company);
                ModelOrder modelOrder = modelOrderDAO.getByModel(model, company);

                List<ModelSaya> modelSayas = modelSayaDAO.getByModelOrder(modelOrder, company);

                return Response.ok().entity(modelSayas).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSayaOrderAndAssort")
    @UnitOfWork
    public Response readSayaOrderAndAssort(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_READ)) {
                String sayaOrderOid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                ModelSaya modelSaya = modelSayaDAO.getByOid(sayaOrderOid, company);

                List<ModelAssort> modelAssorts = modelAssortDAO.getByModelOrder(modelSaya.getModelOrder(), company);

                SayaDTO sayaDTO = new SayaDTO();
                sayaDTO.setModelSaya(modelSaya);
                sayaDTO.setModelAssorts(modelAssorts);

                return Response.ok().entity(sayaDTO).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                ModelSaya modelSaya = modelSayaDAO.getByOid(oid, company);
                List<ModelSayaIncoming> modelSayaIncomings = modelSayaIncomingDAO.getBySaya(modelSaya, company);
                if (modelSayaIncomings.size() == 0) {
                    modelSayaDAO.delete(modelSaya);
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_ALREADY_ACCEPTED.getErrorAlias()).build();
                }
                List<ModelSaya> modelSayas = modelSayaDAO.getByModelOrder(modelSaya.getModelOrder(), company);

                return Response.ok().entity(modelSayas).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("acceptSaya")
    @UnitOfWork
    public Response acceptSaya(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_UPDATE)) {
                String sayaOrderOid = data.get("oid");
                String quantity = data.get("quantity");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();

                ModelSaya modelSaya = modelSayaDAO.getByOid(sayaOrderOid, company);

                ModelSayaIncoming modelSayaIncoming = new ModelSayaIncoming();
                modelSayaIncoming.setCompany(company);
                modelSayaIncoming.setUpdater(userToken.getUser());
                modelSayaIncoming.setUpdateDate(new DateTime());
                modelSayaIncoming.setQuantity(Integer.valueOf(quantity));
                modelSayaIncoming.setDescription(description);
                modelSayaIncoming.setModelSaya(modelSaya);
                modelSayaIncoming.setModelOrder(modelSaya.getModelOrder());

                modelSayaIncomingDAO.update(modelSayaIncoming);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelSayaIncoming.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelSaya.getQuantity() + " çifti sayadan geldi.", new DateTime());

                return Response.ok().entity(modelSayaIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingSayaOrder")
    @UnitOfWork
    public Response readIncomingSayaOrder(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_UPDATE)) {
                String modelOid = data.get("oid");

                Model model = modelDAO.getByOid(modelOid, userToken.getUser().getCompany());

                ModelOrder modelOrder = modelOrderDAO.getByModel(model, userToken.getUser().getCompany());

                List<ModelSayaIncoming> modelSayaIncomings = modelSayaIncomingDAO.getByModelOrder(modelOrder, userToken.getUser().getCompany());

                return Response.ok().entity(modelSayaIncomings).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingSayaOrderDetail")
    @UnitOfWork
    public Response readIncomingSayaOrderDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_UPDATE)) {
                String incomingSayaOrderOid = data.get("oid");

                ModelSayaIncoming modelSayaIncoming = modelSayaIncomingDAO.getByOid(incomingSayaOrderOid, userToken.getUser().getCompany());

                return Response.ok().entity(modelSayaIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @Path("deleteIncomingSaya")
    @UnitOfWork
    public Response deleteIncomingSaya(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_UPDATE)) {
                String sayaOid = data.get("oid");

                ModelSayaIncoming modelSayaIncoming = modelSayaIncomingDAO.getByOid(sayaOid, userToken.getUser().getCompany());

                modelSayaIncomingDAO.delete(modelSayaIncoming);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), modelSayaIncoming.getModelOrder().getModel().getModelCode() + " kodlu modelin " + modelSayaIncoming.getModelSaya().getQuantity() + " çiftlik gelen saya bilgisi silindi.", new DateTime());

                return Response.ok().entity(modelSayaIncoming).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readSummary")
    @UnitOfWork
    public Response readSummary(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_READ)) {
                List<ModelSaya> modelSayas = modelSayaDAO.getAll(userToken.getUser().getCompany());
                List<SayaOrderSummary> sayaOrderSummaries = new LinkedList<>();

                for (ModelSaya modelSaya : modelSayas) {

                    List<ModelSayaIncoming> modelSayaIncomings = modelSayaIncomingDAO.getBySaya(modelSaya, userToken.getUser().getCompany());

                    Integer totalSayaIncoming = 0;

                    for (ModelSayaIncoming modelSayaIncoming : modelSayaIncomings) {
                        if (modelSayaIncoming.getQuantity() != null) {
                            totalSayaIncoming += modelSayaIncoming.getQuantity();
                        }
                    }

                    if (modelSaya.getQuantity() > totalSayaIncoming) {
                        List<ModelPriceDetail> modelPriceDetail = modelPriceDetailDAO.getByModelAndDescription(modelSaya.getModelOrder().getModel(), "Saya", userToken.getUser().getCompany());

                        SayaOrderSummary sayaOrderSummary = new SayaOrderSummary();
                        sayaOrderSummary.setModelCode(modelSaya.getModelOrder().getModel().getModelCode());
                        sayaOrderSummary.setCustomerModelCode(modelSaya.getModelOrder().getModel().getCustomerModelCode());
                        sayaOrderSummary.setCreationDatetime(modelSaya.getCreationDateTime());
                        sayaOrderSummary.setDefaultPhoto(modelSaya.getModelOrder().getModel().getDefaultPhoto());
                        sayaOrderSummary.setModelColor(modelSaya.getModelOrder().getModel().getColor());
                        sayaOrderSummary.setSayaDocumentNumber(modelSaya.getDocumentNumber());
                        sayaOrderSummary.setSayaOrderQuantity(modelSaya.getQuantity());
                        sayaOrderSummary.setOrderQuantity(modelSaya.getModelOrder().getOrderQuantity());
                        sayaOrderSummary.setSayaciName(modelSaya.getSayaci().getName());
                        sayaOrderSummary.setSayaIncomingQuantity(totalSayaIncoming);
                        sayaOrderSummary.setYear(modelSaya.getModelOrder().getModel().getYear());
                        sayaOrderSummary.setSeason(modelSaya.getModelOrder().getModel().getSeason());

                        if(modelPriceDetail.size() != 0) {
                            sayaOrderSummary.setSayaPrice(modelPriceDetail.get(0) == null ? 0.0 : modelPriceDetail.get(0).getPrice());
                        } else {
                            sayaOrderSummary.setSayaPrice(0.0);
                        }

                        sayaOrderSummaries.add(sayaOrderSummary);
                    }
                }

                return Response.ok().entity(sayaOrderSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readIncomingSummary")
    @UnitOfWork
    public Response readIncomingSummary(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.SAYA_READ)) {
                List<ModelSayaIncoming> modelSayaIncomings = modelSayaIncomingDAO.getAll(userToken.getUser().getCompany());
                List<SayaIncomingSummary> sayaIncomingSummaries = new LinkedList<>();

                for (ModelSayaIncoming modelSayaIncoming : modelSayaIncomings) {
                    List<ModelPriceDetail> modelPriceDetail = modelPriceDetailDAO.getByModelAndDescription(modelSayaIncoming.getModelOrder().getModel(), "Saya", userToken.getUser().getCompany());

                    SayaIncomingSummary sayaIncomingSummary = new SayaIncomingSummary();
                    sayaIncomingSummary.setModelCode(modelSayaIncoming.getModelOrder().getModel().getModelCode());
                    sayaIncomingSummary.setCustomerModelCode(modelSayaIncoming.getModelOrder().getModel().getCustomerModelCode());
                    sayaIncomingSummary.setCreationDatetime(modelSayaIncoming.getCreationDateTime());
                    sayaIncomingSummary.setDefaultPhoto(modelSayaIncoming.getModelOrder().getModel().getDefaultPhoto());
                    sayaIncomingSummary.setModelColor(modelSayaIncoming.getModelOrder().getModel().getColor());
                    sayaIncomingSummary.setSayaDocumentNumber(modelSayaIncoming.getModelSaya().getDocumentNumber());
                    sayaIncomingSummary.setSayaIncomingQuantity(modelSayaIncoming.getQuantity());
                    sayaIncomingSummary.setOrderQuantity(modelSayaIncoming.getModelOrder().getOrderQuantity());
                    sayaIncomingSummary.setSayaciName(modelSayaIncoming.getModelSaya().getSayaci().getName());
                    sayaIncomingSummary.setOutgoingDateTime(modelSayaIncoming.getModelSaya().getCreationDateTime());
                    sayaIncomingSummary.setYear(modelSayaIncoming.getModelOrder().getModel().getYear());
                    sayaIncomingSummary.setSeason(modelSayaIncoming.getModelOrder().getModel().getSeason());

                    if(modelPriceDetail.size() != 0) {
                        sayaIncomingSummary.setSayaPrice(modelPriceDetail.get(0) == null ? 0.0 : modelPriceDetail.get(0).getPrice());
                    } else {
                        sayaIncomingSummary.setSayaPrice(0.0);
                    }

                    sayaIncomingSummaries.add(sayaIncomingSummary);
                }

                return Response.ok().entity(sayaIncomingSummaries).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
