package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.ProductionLineDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ProductionLine;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 01/12/2016.
 */
@Path("/productionLine")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductionLineResource {
    AuthUtil authUtil;
    ProductionLineDAO productionLineDAO;

    public ProductionLineResource(AuthUtil authUtil, ProductionLineDAO productionLineDAO) {
        this.authUtil = authUtil;
        this.productionLineDAO = productionLineDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_CREATE)) {
                String code = data.get("code");
                String name = data.get("name");
                String description = data.get("description");
                String color = data.get("color");
                Integer lineOrder = Integer.parseInt(data.get("lineOrder"));

                Company company = userToken.getUser().getCompany();
                ProductionLine productionLine = productionLineDAO.findByCode(code, company);
                if (productionLine == null) {
                    productionLine = productionLineDAO.findByLineOrder(lineOrder, company);
                    if (productionLine == null) {
                        productionLine = new ProductionLine();
                        productionLine.setCompany(company);
                        productionLine.setUpdateDate(new DateTime());
                        productionLine.setUpdater(userToken.getUser());
                        productionLine.setName(name);
                        productionLine.setDescription(description);
                        productionLine.setColor(color);
                        productionLine.setCode(code);
                        productionLine.setLineOrder(lineOrder);

                        productionLineDAO.create(productionLine);

                        authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), productionLine.getName() + " adlı üretim hattı oluşturuldu. ", new DateTime());


                        return Response.ok(productionLine).build();
                    } else {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_LINEORDER.getErrorAlias()).build();
                    }
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_READ)) {
                List<ProductionLine> productionLines = productionLineDAO.getAll(userToken.getUser().getCompany());

                return Response.ok(productionLines).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read/{crmt}")
    @UnitOfWork
    public Response read(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_READ)) {
                List<ProductionLine> productionLines = productionLineDAO.getAll(userToken.getUser().getCompany());
                return Response.ok(productionLines).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByName/")
    @UnitOfWork
    public Response getByName(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            String name = data.get("name");
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_READ)) {
                List<ProductionLine> productionLines = productionLineDAO.getByName(name, userToken.getUser().getCompany());
                return Response.ok(productionLines).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_READ)) {
                String oid = data.get("oid");

                ProductionLine productionLine = productionLineDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok(productionLine).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetailList")
    @UnitOfWork
    public Response readDetailForSearch(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));
        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_READ)) {
                String oid = data.get("oid");
                List<ProductionLine> productionLine = productionLineDAO.getByOidforSearch(oid, userToken.getUser().getCompany());
                return Response.ok(productionLine).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_CREATE)) {
                String oid = data.get("oid");
                String code = data.get("code");
                String name = data.get("name");
                String description = data.get("description");
                Integer lineOrder = Integer.parseInt(data.get("lineOrder"));
                String color = data.get("color");

                Company company = userToken.getUser().getCompany();
                ProductionLine productionLine = productionLineDAO.getByOid(oid, userToken.getUser().getCompany());

                if (productionLine == null) {
                    return Response.serverError().entity(ErrorMessage.UNKNOWN_CUSTOMER.getErrorAlias()).build();
                } else {
                    ProductionLine productionLineCode = productionLineDAO.findByCode(code, userToken.getUser().getCompany());
                    if (productionLineCode != null && !productionLineCode.getCode().equals(productionLine.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        ProductionLine productionLineOrder = productionLineDAO.findByLineOrder(lineOrder, userToken.getUser().getCompany());
                        if (productionLineOrder != null && !productionLineOrder.getLineOrder().equals(productionLine.getLineOrder())) {
                            return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_LINEORDER.getErrorAlias()).build();
                        } else {
                            productionLine.setCompany(company);
                            productionLine.setUpdateDate(new DateTime());
                            productionLine.setUpdater(userToken.getUser());
                            productionLine.setName(name);
                            productionLine.setDescription(description);
                            productionLine.setColor(color);
                            productionLine.setCode(code);
                            productionLine.setLineOrder(lineOrder);

                            productionLineDAO.update(productionLine);

                            authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), productionLine.getName() + " adlı üretim hattı güncellendi. ", new DateTime());

                            return Response.ok(productionLine).build();
                        }
                    }
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_READ)) {
                String oid = data.get("oid");

                ProductionLine productionLine = productionLineDAO.getByOid(oid, userToken.getUser().getCompany());
                productionLine.setDeleteStatus(true);

                productionLineDAO.update(productionLine);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), productionLine.getName() + " adlı üretim hattı silindi. ", new DateTime());

                return Response.ok(productionLine).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByCode")
    @UnitOfWork
    public Response getByCode(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.PRODUCTION_LINE_READ)) {
                String code = data.get("code");

                List<ProductionLine> productionLine = productionLineDAO.findByContainsCode(code, userToken.getUser().getCompany());

                return Response.ok(productionLine).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
