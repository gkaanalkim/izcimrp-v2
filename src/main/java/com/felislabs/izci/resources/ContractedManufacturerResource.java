package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.ContractedManufacturerDAO;
import com.felislabs.izci.dao.dao.ProcessingDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.ContractedManufacturer;
import com.felislabs.izci.representation.entities.Processing;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by arricie on 21.03.2017.
 */

@Path("contractedManufacturer")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ContractedManufacturerResource {
    AuthUtil authUtil;
    ContractedManufacturerDAO contractedManufacturerDAO;
    ProcessingDAO processingDAO;

    public ContractedManufacturerResource(AuthUtil authUtil, ContractedManufacturerDAO contractedManufacturerDAO, ProcessingDAO processingDAO) {
        this.authUtil = authUtil;
        this.contractedManufacturerDAO = contractedManufacturerDAO;
        this.processingDAO = processingDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CONTRACTED_MANUFACTURER_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");
                String address = data.get("address");
                String phone = data.get("phone");
                String representative = data.get("representative");
                String processing = data.get("processing");
                Company company = userToken.getUser().getCompany();

                ContractedManufacturer contractedManufacturer = contractedManufacturerDAO.getByCode(code, company);

                if (contractedManufacturer != null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    ContractedManufacturer contractedManufacturerByCode = contractedManufacturerDAO.getByCode(code, userToken.getUser().getCompany());

                    if (contractedManufacturerByCode != null && !contractedManufacturerByCode.getCode().equals(contractedManufacturer.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        contractedManufacturer = new ContractedManufacturer();
                        contractedManufacturer.setCompany(company);
                        contractedManufacturer.setName(name);
                        contractedManufacturer.setCode(code);
                        contractedManufacturer.setDescription(description);
                        contractedManufacturer.setAddress(address);
                        contractedManufacturer.setPhone(phone);
                        contractedManufacturer.setProcessing(processingDAO.getByOid(processing, company));
                        contractedManufacturer.setRepresentative(representative);

                        contractedManufacturerDAO.create(contractedManufacturer);

                        return Response.ok().entity(contractedManufacturer).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CONTRACTED_MANUFACTURER_READ)) {
                List<ContractedManufacturer> contractedManufacturerList = contractedManufacturerDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(contractedManufacturerList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CONTRACTED_MANUFACTURER_READ)) {
                String oid = data.get("oid");

                ContractedManufacturer contractedManufacturer = contractedManufacturerDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(contractedManufacturer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CONTRACTED_MANUFACTURER_UPDATE)) {
                String oid = data.get("oid");

                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");
                String address = data.get("address");
                String phone = data.get("phone");
                String processing = data.get("processing");
                String representative = data.get("representative");
                Company company = userToken.getUser().getCompany();

                ContractedManufacturer contractedManufacturer = contractedManufacturerDAO.getByOid(oid, company);

                if (contractedManufacturer == null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    ContractedManufacturer contractedManufacturerByCode = contractedManufacturerDAO.getByCode(code, userToken.getUser().getCompany());

                    if (contractedManufacturerByCode != null && !contractedManufacturerByCode.getCode().equals(contractedManufacturer.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {

                        contractedManufacturer.setCompany(company);
                        contractedManufacturer.setName(name);
                        contractedManufacturer.setCode(code);
                        contractedManufacturer.setDescription(description);
                        contractedManufacturer.setAddress(address);
                        contractedManufacturer.setPhone(phone);
                        contractedManufacturer.setProcessing(processingDAO.getByOid(processing, company));
                        contractedManufacturer.setRepresentative(representative);

                        contractedManufacturerDAO.update(contractedManufacturer);

                        return Response.ok().entity(contractedManufacturer).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CONTRACTED_MANUFACTURER_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                ContractedManufacturer contractedManufacturer = contractedManufacturerDAO.getByOid(oid, company);
                contractedManufacturer.setDeleteStatus(true);

                contractedManufacturerDAO.update(contractedManufacturer);

                return Response.ok().entity(contractedManufacturer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByProcessing")
    @UnitOfWork
    public Response readByProcessing(Map<String, String> data) {
        String token = data.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);
        if (userToken != null) {
            String processingOid = data.get("processing");
            Company company = userToken.getUser().getCompany();
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CONTRACTED_MANUFACTURER_READ)) {
                Processing processing = processingDAO.getByOid(processingOid, company);
                List<ContractedManufacturer> contractedManufacturerList = contractedManufacturerDAO.readByProcessing(processing, company);

                return Response.ok().entity(contractedManufacturerList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

}
