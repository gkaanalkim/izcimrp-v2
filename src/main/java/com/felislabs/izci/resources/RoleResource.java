package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.RoleDAO;
import com.felislabs.izci.dao.dao.RoleUserDAO;
import com.felislabs.izci.dao.dao.UserDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 07/09/15.
 */
@Path("role")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class RoleResource {
    RoleDAO roleDAO;
    UserDAO userDAO;
    RoleUserDAO roleUserDAO;
    AuthUtil authUtil;

    public RoleResource(RoleDAO roleDAO, UserDAO userDAO, RoleUserDAO roleUserDAO, AuthUtil authUtil) {
        this.roleDAO = roleDAO;
        this.userDAO = userDAO;
        this.roleUserDAO = roleUserDAO;
        this.authUtil = authUtil;
    }

    @POST
    @Path("get")
    @UnitOfWork
    public Response get(Map<String, String> roleData) {
        String token = roleData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_READ)) {
                List<Role> roles;
                Company company = userToken.getUser().getCompany();
                String userCode = userToken.getUser().getCode();
                if (userCode.equals("SAU")) {
                    roles = roleDAO.findAll(Role.class);

                } else {
                    roles = roleDAO.getAllExceptString(Role.class, "SAUADMIN", company);
                }
                return Response.ok(roles).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByOid")
    @UnitOfWork
    public Response getByOid(Map<String, String> roleData) {
        String token = roleData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_READ)) {
                String oid = roleData.get("oid");

                return Response.ok(roleDAO.getById(oid, userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> roleData) {
        String token = roleData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_CREATE)) {
                String name = roleData.get("name");
                String code = roleData.get("code");
                Company company = userToken.getUser().getCompany();
                Role role = roleDAO.getByCode(code, company);

                if (role == null) {
                    role = new Role();

                    role.setName(name);
                    role.setCode(code);
                    role.setCompany(company);
                    roleDAO.create(role);
                    return Response.ok(role).build();
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> roleData) {
        String token = roleData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_UPDATE)) {
                Company company = userToken.getUser().getCompany();

                String oid = roleData.get("oid");
                String name = roleData.get("name");
                String code = roleData.get("code");

                Role role = roleDAO.getById(oid, company);

                String oldCode = roleDAO.getById(oid, company).getCode();
                Role codeControl = roleDAO.getByCode(code, company);

                if ((codeControl != null) && (codeControl.getCode() != oldCode)) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                }

                if (!role.getCode().equals("Admin")) {
                    role.setName(name);
                    role.setCode(code);
                    role.setCompany(company);

                    roleDAO.update(role);

                    return Response.ok(role).build();
                } else {
                    return Response.ok("Bu bir sistem rolüdür, güncelleyemezsiniz.").build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> roleData) {
        String token = roleData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_DELETE)) {
                String oid = roleData.get("oid");

                Role role = roleDAO.getById(oid, userToken.getUser().getCompany());

                if (!role.getCode().equals("Admin")) {
                    roleDAO.delete(role);

                    return Response.ok(role).build();
                } else {
                    return Response.ok("Bu bir sistem rolüdür, silemezsiniz.").build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    @Path("assingroletouser")
    public Response assingroletouser(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_ASSING)) {
                String roleOid = data.get("selectedRole");
                String userOid = data.get("selectedUser");

                User user = userDAO.getById(userOid, userToken.getUser().getCompany());
                Role role = roleDAO.getById(roleOid, userToken.getUser().getCompany());

                RoleUser roleUser = roleUserDAO.getByUser(user, userToken.getUser().getCompany());

                if (roleUser == null) {
                    roleUser = new RoleUser();
                    roleUser.setRole(role);
                    roleUser.setUser(user);
                    roleUser.setCompany(userToken.getUser().getCompany());

                    roleUserDAO.create(roleUser);

                    return Response.ok(roleUser).build();
                } else {
                    roleUser.setRole(role);

                    roleUserDAO.update(roleUser);

                    return Response.ok(roleUser).build();
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getroleofuser")
    @UnitOfWork
    public Response getRoleOfUser(Map<String, String> data) {
        UserToken userToken = authUtil.isValidToken(data.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.ROLE_READ)) {
                User user = userDAO.getById(data.get("userOid"), userToken.getUser().getCompany());

                RoleUser roleUser = roleUserDAO.getByUser(user, userToken.getUser().getCompany());

                return Response.ok(roleUser == null ? new Role() : roleUser.getRole()).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
