package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.CustomerDAO;
import com.felislabs.izci.dao.dao.CustomerShipmentDAO;
import com.felislabs.izci.dao.dao.SystemActivityDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.dto.WorkOrderImageDTO;
import com.felislabs.izci.representation.entities.*;
import com.felislabs.izci.util.FileUtil;
import com.opencsv.CSVReader;
import io.dropwizard.hibernate.UnitOfWork;
import net.coobird.thumbnailator.Thumbnails;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.joda.time.DateTime;

import javax.imageio.ImageIO;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 14/08/15.
 */
@Path("customer")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {
    CustomerDAO customerDAO;
    AuthUtil authUtil;
    SystemActivityDAO systemActivityDAO;
    CustomerShipmentDAO customerShipmentDAO;

    public CustomerResource(CustomerDAO customerDAO, AuthUtil authUtil, SystemActivityDAO systemActivityDAO, CustomerShipmentDAO customerShipmentDAO) {
        this.customerDAO = customerDAO;
        this.authUtil = authUtil;
        this.systemActivityDAO = systemActivityDAO;
        this.customerShipmentDAO = customerShipmentDAO;
    }

    @POST
    @Path("get")
    @UnitOfWork
    public Response getAll(Map<String, String> customerData) {
        UserToken userToken = authUtil.isValidToken(customerData.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(customerDAO.getAll(Customer.class, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> customerData) {
        UserToken userToken = authUtil.isValidToken(customerData.get("crmt"));
        String oid = customerData.get("oid");

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(customerDAO.getById(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("get/{crmt}")
    @UnitOfWork
    public Response getAll(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(customerDAO.getAll(Customer.class, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @GET
    @Path("get/{crmt}")
    @UnitOfWork
    public Response getAllByGet(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(customerDAO.getAll(Customer.class, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("get/{crmt}/{customerName}")
    @UnitOfWork
    public Response getAll(@PathParam("crmt") String crmt, @PathParam("customerName") String customerName) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();
                List<Customer> customers = customerDAO.getByContainsInTitle(customerName, company);

                return Response.ok(customers).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByPath")
    @UnitOfWork
    public Response getByPath(Map<String, String> customerData) {
        String token = customerData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(customerDAO.getByContains(customerData.get("customer"), company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_CREATE)) {
                String code = data.get("code");
                String name = data.get("name");
                String description = data.get("description");
                String phone = data.get("phone");
                String email = data.get("email");
                String address = data.get("address");
                String taxOffice = data.get("taxOffice");
                String taxNo = data.get("taxNo");
                String imagePath = data.get("imagePath");

                Customer customer = customerDAO.getByCode(code, userToken.getUser().getCompany());

                //Kod Kontrolü
                if (customer != null) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                } else {
                    Customer customerByCode = customerDAO.getByCode(code, userToken.getUser().getCompany());

                    if (customerByCode != null && !customerByCode.getCode().equals(customer.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        customer = new Customer();
                        customer.setName(name);
                        customer.setCode(code);
                        customer.setDescription(description);
                        customer.setPhone(phone);
                        customer.setEmail(email);
                        customer.setAddress(address);
                        customer.setCompany(userToken.getUser().getCompany());
                        customer.setTaxNo(taxNo);
                        customer.setTaxOffice(taxOffice);
                        customer.setImagePath(imagePath);

                        customerDAO.create(customer);

                        authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), customer.getName() + " Adlı müşteri oluşturuldu. ", new DateTime());

                        return Response.ok().entity(customer).build();
                    }
                }

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response updateNew(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_CREATE)) {
                String oid = data.get("oid");
                String code = data.get("code");
                String name = data.get("name");
                String description = data.get("description");
                String phone = data.get("phone");
                String email = data.get("email");
                String address = data.get("address");
                String imagePath = data.get("imagePath");

                Customer customer = customerDAO.getById(oid, userToken.getUser().getCompany());

                if (customer == null) {
                    return Response.serverError().entity(ErrorMessage.UNKNOWN_CUSTOMER.getErrorAlias()).build();
                } else {
                    Customer customerByCode = customerDAO.getByCode(code, userToken.getUser().getCompany());

                    if (customerByCode != null && !customerByCode.getCode().equals(customer.getCode())) {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                    } else {
                        customer.setName(name);
                        customer.setCode(code);
                        customer.setDescription(description);
                        customer.setPhone(phone);
                        customer.setEmail(email);
                        customer.setAddress(address);
                        customer.setCompany(userToken.getUser().getCompany());
                        customer.setImagePath(imagePath);

                        customerDAO.update(customer);

                        authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), customer.getName() + " Adlı müşteri güncellendi. ", new DateTime());

                        return Response.ok().entity(customer).build();
                    }
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> customerData) {
        String token = customerData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_DELETE)) {

                String oid = customerData.get("oid");
                Company company = userToken.getUser().getCompany();
                Customer customer = customerDAO.getById(oid, company);
                customer.setDeleteStatus(true);
                List<CustomerShipment> customerShipmentList = customerShipmentDAO.getByCustomer(customer, company);
                if (!customerShipmentList.isEmpty()) {

                    for (int i = 0; i < customerShipmentList.size(); i++) {
                        CustomerShipment customerShipment = customerShipmentList.get(i);
                        customerShipment.setDeleteStatus(true);
                        customerShipmentDAO.update(customerShipment);
                    }
                }

                customerDAO.update(customer);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), customer.getName() + " Adlı müşteri silindi. ", new DateTime());

                return Response.ok(customer).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("findByTitle")
    @UnitOfWork
    public Response findByTitle(Map<String, String> customerData) {
        String token = customerData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                String title = customerData.get("title");
                Company company = userToken.getUser().getCompany();
                List<Customer> customers = customerDAO.getByContainsInTitle(title, company);

                if (customers.isEmpty()) {
                    return Response.serverError().entity(ErrorMessage.TR_CUSTOMER_NOT_FOUND.getErrorAlias()).build();
                } else {
                    return Response.ok(customers).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("findByAddress/{crmt}/{address}")
    @UnitOfWork
    public Response findByAddress(@PathParam("crmt") String crmt, @PathParam("address") String address) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                Company company = userToken.getUser().getCompany();
                List<Customer> customers = customerDAO.getByAddressContains(address, company);

                return Response.ok(customers).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("findBySector")
    @UnitOfWork
    public Response findBySector(Map<String, String> customerData) {
        String token = customerData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                String sector = customerData.get("sector");
                Company company = userToken.getUser().getCompany();
                List<Customer> customers = customerDAO.getBySectorContains(sector, company);

                return Response.ok(customers).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByOid")
    @UnitOfWork
    public Response getByOid(Map<String, String> customerData) {
        String token = customerData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                String oid = customerData.get("oid");
                Company company = userToken.getUser().getCompany();

                return Response.ok(customerDAO.getById(oid, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByOidForGrid")
    @UnitOfWork
    public Response getByOidForGrid(Map<String, String> customerData) {
        String token = customerData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                String oid = customerData.get("oid");
                Company company = userToken.getUser().getCompany();

                List<Customer> customers = new LinkedList<>();
                customers.add(customerDAO.getById(oid, company));

                return Response.ok(customers).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getAllForKendo/{crmt}")
    @UnitOfWork
    public Response getAllForKendo(@PathParam("crmt") String crmt, @PathParam("asdsadsaa") String name) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_READ)) {
                return Response.ok(customerDAO.getAll(Customer.class, userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("importer/{crmt}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @UnitOfWork
    public Response importer(@FormDataParam("customerFile") InputStream uploadedInputStream,
                             @FormDataParam("customerFile") FormDataContentDisposition fileDetail,
                             @PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_CREATE)) {

                String uploadedFileLocation = System.getProperty("java.io.tmpdir") + File.separator + fileDetail.getFileName();

                // save it
                FileUtil.writeToFile(uploadedInputStream, uploadedFileLocation);

                CSVReader reader = null;
                try {
//                    reader = new CSVReader(new FileReader(uploadedFileLocation));
                    reader = new CSVReader(new InputStreamReader(new FileInputStream(uploadedFileLocation), "UTF-8"));
                    String[] line;
                    while ((line = reader.readNext()) != null) {
                        String code = line[0];
                        String name = line[1];
                        String taxOffice = line[2];
                        String taxNo = line[3];
                        String address = line[4] + " " + line[5] + " " + line[6] + " " + line[7] + " " + line[8] + " " + line[9];

                        if (code.length() > 100) {
                            return Response.serverError().entity(ErrorMessage.CUSTOMER_CODE_TOO_LONG.getErrorAlias()).build();
                        } else if (name.length() > 200) {
                            return Response.serverError().entity(ErrorMessage.CUSTOMER_NAME_TOO_LONG.getErrorAlias()).build();
                        } else if (taxOffice.length() > 100) {
                            return Response.serverError().entity(ErrorMessage.CUSTOMER_TAX_OFFICE_TOO_LONG.getErrorAlias()).build();
                        } else if (address.length() > 100) {
                            return Response.serverError().entity(ErrorMessage.CUSTOMER_ADDRESS_TOO_LONG.getErrorAlias()).build();
                        } else {
                            Customer customer = customerDAO.getByCode(code, userToken.getUser().getCompany());

                            if (customer == null) {
                                customer = new Customer();
                                customer.setName(name);
                                customer.setCode(code);
                                customer.setTaxOffice(taxOffice);
                                customer.setTaxNo(taxNo);
                                customer.setAddress(address);
                                customer.setCompany(userToken.getUser().getCompany());

                                customerDAO.create(customer);
                            } else {
                                return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return Response.ok().entity(new Customer()).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("/uploadCustomerImage/{crmt}")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @UnitOfWork
    public Response uploadCustomerImage(
            @PathParam("crmt") String crmt,
            @FormDataParam("customerImage") InputStream uploadedInputStream,
            @FormDataParam("customerImage") FormDataContentDisposition fileDetail) {

        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.CUSTOMER_CREATE)) {
                //  ŞİRKET BİLGİLERİ
                String osBasePath = "";

                if (System.getProperty("os.name").equals("Windows 10")) {
                    osBasePath = "C:\\xampp\\htdocs\\izcimrp-ui-v2\\customerimages\\";
                } else if (System.getProperty("os.name").equals("Mac OS X")) {
                    osBasePath = "/Applications/XAMPP/xamppfiles/htdocs/izcimrp-ui-v2/customerimages/";
                } else {
                    osBasePath = "/var/www/html/customerimages/";
                }

                String fileExtension = "";

                if (fileDetail.getFileName().endsWith(".png") || fileDetail.getFileName().endsWith(".PNG")) {
                    fileExtension = ".png";
                } else if (fileDetail.getFileName().endsWith(".jpg") || fileDetail.getFileName().endsWith(".JPG") || fileDetail.getFileName().endsWith(".JPEG") || fileDetail.getFileName().endsWith(".jpeg")) {
                    fileExtension = ".jpg";
                }

                DateTime fileNameDT = new DateTime();

                String fileName = fileNameDT.getMillis() + Math.random() + fileExtension;

                String uploadedFileLocation = osBasePath + fileName;

                // save it with thumbnail
                try {
                    BufferedImage inBuff = ImageIO.read(uploadedInputStream);
                    int w = inBuff.getWidth();
                    int h = inBuff.getHeight();
                    if (h > w) {
                        try {
                            Thumbnails.of(inBuff).rotate(90).size(750, 750).toFile(uploadedFileLocation);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        try {
                            Thumbnails.of(inBuff).size(750, 750).toFile(uploadedFileLocation);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                WorkOrderImageDTO workOrderImageDTO = new WorkOrderImageDTO();
                workOrderImageDTO.setPath(fileName);

                return Response.ok(workOrderImageDTO).build();
            }
        }

        return Response.ok().build();
    }
}
