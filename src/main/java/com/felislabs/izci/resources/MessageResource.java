package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.MessageDAO;
import com.felislabs.izci.dao.dao.UserDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.Message;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by dogukanyilmaz on 10/11/2016.
 */
@Path("message")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MessageResource {
    MessageDAO messageDAO;
    AuthUtil authUtil;
    UserDAO userDAO;
    public MessageResource(MessageDAO messageDAO, AuthUtil authUtil, UserDAO userDAO){
        this.messageDAO =messageDAO;
        this.authUtil=authUtil;
        this.userDAO=userDAO;
    }

    @POST
    @Path("get")
    @UnitOfWork
    public Response getAll(Map<String, String> messageData) {
        UserToken userToken = authUtil.isValidToken(messageData.get("crmt"));

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MESSAGE_READ)) {
                Company company = userToken.getUser().getCompany();
                return Response.ok(messageDAO.getAll(Message.class, company)).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


    @POST
    @UnitOfWork
    public Response create(Map<String, String> messageData) {
        String token = messageData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if(userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MESSAGE_CREATE)) {
                Company company = userToken.getUser().getCompany();


                String title=messageData.get("title");
                String description = messageData.get("description");
                Message message=new Message();

                message.setFrom(userToken.getUser());
                message.setCompany(company);
                message.setTitle(title);
                message.setDescription(description);
                messageDAO.create(message);
                return Response.ok(message).build();

            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByOid")
    @UnitOfWork
    public Response getByOid(Map<String, String> messageData) {
        String token = messageData.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MESSAGE_READ)) {
                String oid = messageData.get("oid");
                Company company = userToken.getUser().getCompany();
                Message message = messageDAO.getByOid(oid, company);

                return Response.ok(message).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }


    @POST
    @Path("getTodaysMessages")
    @UnitOfWork
    public Response getTodaysMessages(Map<String, String> messageData) {
        String token = messageData.get("crmt");
        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.MESSAGE_READ)) {
                Company company = userToken.getUser().getCompany();

                Date date = new Date();

                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                cal.add(Calendar.DAY_OF_YEAR,-1);
                Date oneDayBefore= cal.getTime();

                DateTime today = new DateTime(date);
                DateTime yesterday = new DateTime(oneDayBefore);
                List<Message> messages = messageDAO.getByDateRange(yesterday,today,company);

                return Response.ok(messages).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }
}
