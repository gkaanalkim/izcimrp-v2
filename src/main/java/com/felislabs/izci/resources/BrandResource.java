package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.BrandDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 08/03/2017.
 */
@Path("brand")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BrandResource {
    AuthUtil authUtil;
    BrandDAO brandDAO;

    public BrandResource(AuthUtil authUtil, BrandDAO brandDAO) {
        this.authUtil = authUtil;
        this.brandDAO = brandDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Brand brand = new Brand();
                brand.setCompany(company);
                brand.setUser(user);
                brand.setUpdateDate(new DateTime());
                brand.setName(name);
                brand.setCode(code);
                brand.setDescription(description);

                brandDAO.create(brand);

                return Response.ok().entity(brand).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_READ)) {
                List<Brand> brandList = brandDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(brandList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_READ)) {
                String oid = data.get("oid");

                Brand brand = brandDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(brand).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_UPDATE)) {
                String oid = data.get("oid");

                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Brand  brand = brandDAO.getByOid(oid, company);
                brand.setCompany(company);
                brand.setUser(user);
                brand.setUpdateDate(new DateTime());
                brand.setName(name);
                brand.setCode(code);
                brand.setDescription(description);

                brandDAO.update(brand);

                return Response.ok().entity(brand).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Brand brand = brandDAO.getByOid(oid, company);
                brand.setDeleteStatus(true);

                brandDAO.update(brand);

                return Response.ok().entity(brand).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
