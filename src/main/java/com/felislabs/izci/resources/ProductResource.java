package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.BrandDAO;
import com.felislabs.izci.dao.dao.CategoryDAO;
import com.felislabs.izci.dao.dao.ProductDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 08/03/2017.
 */
@Path("product")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource {
    AuthUtil authUtil;
    ProductDAO productDAO;
    CategoryDAO categoryDAO;
    BrandDAO brandDAO;

    public ProductResource(AuthUtil authUtil, ProductDAO productDAO, CategoryDAO categoryDAO, BrandDAO brandDAO) {
        this.authUtil = authUtil;
        this.productDAO = productDAO;
        this.categoryDAO = categoryDAO;
        this.brandDAO = brandDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PRODUCT_CREATE)) {
                String categoryOid = data.get("categoryOid");
                String brandOid = data.get("brandOid");
                String name = data.get("name");
                String code = data.get("code");
                String type = data.get("type");
                String unit = data.get("unit");
                String sellingPrice = data.get("sellingPrice");
                String buyingPrice = data.get("buyingPrice");
                String barcode = data.get("barcode");
                String serialNo = data.get("serialNo");
                String criticalThreshold = data.get("criticalThreshold");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Category category = categoryDAO.getByOid(categoryOid, company);
                Brand brand = brandDAO.getByOid(brandOid, company);

                Product product = new Product();
                product.setCompany(company);
                product.setUser(user);
                product.setUpdateDate(new DateTime());
                product.setCategory(category);
                product.setBrand(brand);
                product.setName(name);
                product.setCode(code);
                product.setType(type);
                product.setUnit(unit);
                product.setSellingPrice(Double.valueOf(sellingPrice));
                product.setBuyingPrice(Double.valueOf(buyingPrice));
                product.setBarcode(barcode);
                product.setSerialNo(serialNo);
                product.setCriticalThreshold(Double.valueOf(criticalThreshold));
                product.setDescription(description);

                productDAO.create(product);

                return Response.ok().entity(product).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PRODUCT_READ)) {
                List<Product> products = productDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(products).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PRODUCT_READ)) {
                String oid = data.get("oid");

                Product product = productDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(product).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PRODUCT_CREATE)) {
                String oid = data.get("oid");
                String categoryOid = data.get("categoryOid");
                String brandOid = data.get("brandOid");
                String name = data.get("name");
                String code = data.get("code");
                String type = data.get("type");
                String unit = data.get("unit");
                String sellingPrice = data.get("sellingPrice");
                String buyingPrice = data.get("buyingPrice");
                String barcode = data.get("barcode");
                String serialNo = data.get("serialNo");
                String criticalThreshold = data.get("criticalThreshold");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Category category = categoryDAO.getByOid(categoryOid, company);
                Brand brand = brandDAO.getByOid(brandOid, company);

                Product product = productDAO.getByOid(oid, company);
                product.setCompany(company);
                product.setUser(user);
                product.setUpdateDate(new DateTime());
                product.setCategory(category);
                product.setBrand(brand);
                product.setName(name);
                product.setCode(code);
                product.setType(type);
                product.setUnit(unit);
                product.setSellingPrice(Double.valueOf(sellingPrice));
                product.setBuyingPrice(Double.valueOf(buyingPrice));
                product.setBarcode(barcode);
                product.setSerialNo(serialNo);
                product.setCriticalThreshold(Double.valueOf(criticalThreshold));
                product.setDescription(description);

                productDAO.update(product);

                return Response.ok().entity(product).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PRODUCT_READ)) {
                String oid = data.get("oid");

                Product product = productDAO.getByOid(oid, userToken.getUser().getCompany());
                product.setDeleteStatus(true);
                productDAO.update(product);

                return Response.ok().entity(product).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
