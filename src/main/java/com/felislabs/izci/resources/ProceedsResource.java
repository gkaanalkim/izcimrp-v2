package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.AccountDAO;
import com.felislabs.izci.dao.dao.AccountingCustomerDAO;
import com.felislabs.izci.dao.dao.CheckDAO;
import com.felislabs.izci.dao.dao.ProceedsDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.dto.AccountingCustomerAccountStatemensDTO;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 14/03/2017.
 */
@Path("proceeds")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProceedsResource {
    AuthUtil authUtil;
    ProceedsDAO proceedsDAO;
    AccountingCustomerDAO accountingCustomerDAO;
    AccountDAO accountDAO;

    public ProceedsResource(AuthUtil authUtil, ProceedsDAO proceedsDAO, AccountingCustomerDAO accountingCustomerDAO, AccountDAO accountDAO) {
        this.authUtil = authUtil;
        this.proceedsDAO = proceedsDAO;
        this.accountingCustomerDAO = accountingCustomerDAO;
        this.accountDAO = accountDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_CREATE)) {
                String customerOid = data.get("customerOid");
                String accountOid = data.get("accountOid");
                String type = data.get("type");
                String proceedsType = data.get("proceedsType");
                String amount = data.get("amount");
                String proceedsDate = data.get("proceedsDate");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                Account account = accountDAO.getByOid(accountOid, company);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime pd = formatter.parseDateTime(proceedsDate);

                Double calculatedAmount = 0.0;

                if (proceedsType.equals("Müşteriden Tahsilat")) {
                    calculatedAmount = -1 * Double.valueOf(amount);
                } else if (proceedsType.equals("Müşteriye Ödeme")) {
                    calculatedAmount = Double.valueOf(amount);
                }

                Proceeds proceeds = new Proceeds();
                proceeds.setCompany(company);
                proceeds.setUpdateDate(new DateTime());
                proceeds.setUser(user);
                proceeds.setAccountingCustomer(accountingCustomer);
                proceeds.setAccount(account);
                proceeds.setType(type);
                proceeds.setAmount(calculatedAmount);
                proceeds.setProceedsDate(pd);
                proceeds.setDescription(description);
                proceeds.setProceedsType(proceedsType);

                proceedsDAO.create(proceeds);

                if (proceedsType.equals("Müşteriden Tahsilat")) {
                    account.setBalance(account.getBalance() + Double.valueOf(amount));
                    accountDAO.update(account);

                } else if (proceedsType.equals("Müşteriye Ödeme")) {
                    account.setBalance(account.getBalance() - Double.valueOf(amount));
                    accountDAO.update(account);
                }

                accountingCustomer.setBalance(accountingCustomer.getBalance() + calculatedAmount);
                accountingCustomerDAO.update(accountingCustomer);

                return Response.ok().entity(proceeds).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_READ)) {
                List<Proceeds> proceedses = proceedsDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(proceedses).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readByCustomer")
    @UnitOfWork
    public Response readByCustomer(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_READ)) {
                String oid = data.get("customerOid");

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(oid, userToken.getUser().getCompany());

                List<Proceeds> proceedses = proceedsDAO.getByAccountingCustomer(accountingCustomer, userToken.getUser().getCompany());

                return Response.ok().entity(proceedses).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_READ)) {
                String oid = data.get("oid");

                Proceeds proceeds = proceedsDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(proceeds).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_UPDATE)) {
                String oid = data.get("oid");
                String customerOid = data.get("customerOid");
                String accountOid = data.get("accountOid");
                String type = data.get("type");
                String amount = data.get("amount");
                String proceedsDate = data.get("proceedsDate");
                String description = data.get("description");
                String proceedsType = data.get("proceedsType");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(customerOid, company);
                Account account = accountDAO.getByOid(accountOid, company);

                org.joda.time.format.DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy HH:mm:ss");
                DateTime pd = formatter.parseDateTime(proceedsDate);

                Proceeds proceeds = proceedsDAO.getByOid(oid, company);
                proceeds.setCompany(company);
                proceeds.setUpdateDate(new DateTime());
                proceeds.setUser(user);
                proceeds.setAccountingCustomer(accountingCustomer);
                proceeds.setAccount(account);
                proceeds.setType(type);
                proceeds.setAmount(Double.valueOf(amount));
                proceeds.setProceedsDate(pd);
                proceeds.setDescription(description);
                proceeds.setProceedsType(proceedsType);

                proceedsDAO.update(proceeds);

                return Response.ok().entity(proceeds).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_DELETE)) {
                String oid = data.get("oid");

                Proceeds proceeds = proceedsDAO.getByOid(oid, userToken.getUser().getCompany());
                proceeds.setDeleteStatus(true);

                proceedsDAO.update(proceeds);

                return Response.ok().entity(proceeds).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readAccountStatementByCustomer")
    @UnitOfWork
    public Response readAccountStatementByCustomer(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.PROCEEDS_READ)) {
                String oid = data.get("customerOid");

                AccountingCustomer accountingCustomer = accountingCustomerDAO.getByOid(oid, userToken.getUser().getCompany());

                List<Proceeds> proceedses = proceedsDAO.getByAccountingCustomer(accountingCustomer, userToken.getUser().getCompany());

                List<AccountingCustomerAccountStatemensDTO> accountingCustomerAccountStatemensDTOs = new LinkedList<>();
                Double balance = 0.0;

                for (Proceeds proceeds : proceedses) {
                    balance += proceeds.getAmount();

                    AccountingCustomerAccountStatemensDTO accountingCustomerAccountStatemensDTO = new AccountingCustomerAccountStatemensDTO();
                    accountingCustomerAccountStatemensDTO.setProceedsDate(proceeds.getProceedsDate());
                    accountingCustomerAccountStatemensDTO.setProceedsType(proceeds.getProceedsType());
                    accountingCustomerAccountStatemensDTO.setDescription(proceeds.getDescription());
                    accountingCustomerAccountStatemensDTO.setType(proceeds.getType());

                    if (proceeds.getProceedsType().equals("Müşteriden Tahsilat")) {
                        accountingCustomerAccountStatemensDTO.setDebt(proceeds.getAmount());
                        accountingCustomerAccountStatemensDTO.setDue(0.0);
                    } else if (proceeds.getProceedsType().equals("Müşteriye Ödeme")) {
                        accountingCustomerAccountStatemensDTO.setDue(proceeds.getAmount());
                        accountingCustomerAccountStatemensDTO.setDebt(0.0);
                    }
                    accountingCustomerAccountStatemensDTO.setBalance(balance);

                    accountingCustomerAccountStatemensDTOs.add(accountingCustomerAccountStatemensDTO);
                }

                return Response.ok().entity(accountingCustomerAccountStatemensDTOs).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
