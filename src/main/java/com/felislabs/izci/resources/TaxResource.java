package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.TaxDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.*;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 08/03/2017.
 */
@Path("tax")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TaxResource {
    AuthUtil authUtil;
    TaxDAO taxDAO;

    public TaxResource(AuthUtil authUtil, TaxDAO taxDAO) {
        this.authUtil = authUtil;
        this.taxDAO = taxDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String ratio = data.get("ratio");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Tax tax = new Tax();
                tax.setCompany(company);
                tax.setUser(user);
                tax.setUpdateDate(new DateTime());
                tax.setName(name);
                tax.setCode(code);
                tax.setRatio(Double.valueOf(ratio));
                tax.setDescription(description);

                taxDAO.create(tax);

                return Response.ok().entity(tax).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_CREATE)) {
                List<Tax> taxes = taxDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(taxes).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_CREATE)) {
                String oid = data.get("oid");

                Tax tax = taxDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(tax).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_UPDATE)) {
                String oid = data.get("oid");

                String name = data.get("name");
                String code = data.get("code");
                String ratio = data.get("ratio");
                String description = data.get("description");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Tax tax = taxDAO.getByOid(oid, company);
                tax.setCompany(company);
                tax.setUser(user);
                tax.setUpdateDate(new DateTime());
                tax.setName(name);
                tax.setCode(code);
                tax.setRatio(Double.valueOf(ratio));
                tax.setDescription(description);

                taxDAO.update(tax);

                return Response.ok().entity(tax).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.TAX_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Tax tax = taxDAO.getByOid(oid, company);
                tax.setDeleteStatus(true);

                taxDAO.update(tax);

                return Response.ok().entity(tax).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
