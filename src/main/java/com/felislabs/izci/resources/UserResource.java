package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.UserDAO;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.enums.PermissionEnum;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.User;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 10/09/15.
 */
@Path("user")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {
    UserDAO userDAO;
    AuthUtil authUtil;

    public UserResource(UserDAO userDAO, AuthUtil authUtil) {
        this.userDAO = userDAO;
        this.authUtil = authUtil;
    }

    @POST
    @Path("get")
    @UnitOfWork
    public Response get(Map<String, String> staffData) {
        String token = staffData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.USER_READ)) {
                return Response.ok(userDAO.getAll(User.class, userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByOid")
    @UnitOfWork
    public Response getByOid(Map<String, String> staffData) {
        String token = staffData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.USER_READ)) {
                String oid = staffData.get("oid");

                return Response.ok(userDAO.getById(oid, userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("whoAmI/{crmt}")
    @UnitOfWork
    public Response whoAmI(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.USER_READ)) {
                return Response.ok(userToken.getUser()).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @Path("getByName/{crmt}")
    @UnitOfWork
    public Response getAll(@PathParam("crmt") String crmt, @PathParam("name") String name) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.USER_READ)) {
                return Response.ok(userDAO.getAll(User.class, userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @GET
    @Path("get/{crmt}")
    @UnitOfWork
    public Response getAll(@PathParam("crmt") String crmt) {
        UserToken userToken = authUtil.isValidToken(crmt);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.USER_READ)) {
                return Response.ok(userDAO.getAll(User.class, userToken.getUser().getCompany())).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @POST
    @UnitOfWork
    public Response create(Map<String, String> staffData) {
        String token = staffData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.USER_CREATE)) {
                String code = staffData.get("code");
                String email = staffData.get("email");
                Company company = userToken.getUser().getCompany();
                User user = userDAO.getByCode(code, company);
                if (user == null) {
                    user = userDAO.getByEmail(email, company);
                    if (user == null) {

                        String name = staffData.get("name");
                        String surname = staffData.get("surname");
                        String mobilePhone = staffData.get("mobilePhone");
                        String internalPhoneCode = staffData.get("internalPhoneCode");

                        String password = staffData.get("password");

                        user = new User();
                        user.setName(name);
                        user.setSurname(surname);
                        user.setCode(code);
                        user.setMobilePhone(mobilePhone);
                        user.setInternalPhoneCode(internalPhoneCode);
                        user.setEmail(email);
                        user.setPassword(password);
                        user.setCompany(company);
                        userDAO.create(user);

                        authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), user.getName() + "' Adlı kullanıcı oluşturuldu. ", new DateTime());


                        return Response.ok(user).build();
                    } else {
                        return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_EMAIL.getErrorAlias()).build();
                    }
                } else {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_STAFFPOSITION.getErrorAlias()).build();
                }
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @PUT
    @UnitOfWork
    public Response update(Map<String, String> userData) {
        String token = userData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.USER_UPDATE)) {
                Company company = userToken.getUser().getCompany();


                String oldUserOid = userData.get("userOid");
                String name = userData.get("name");
                String surname = userData.get("surname");
                String mobilePhone = userData.get("mobilePhone");
                String code = userData.get("code");
                String internalPhoneCode = userData.get("internalPhoneCode");
                String email = userData.get("email");

                String oldCode = userDAO.getById(oldUserOid, company).getCode();
                User codeControl = userDAO.getByCode(code, company);

                if (codeControl != null && codeControl.getCode() != oldCode) {
                    return Response.serverError().entity(ErrorMessage.TR_DUPLICATE_CODE.getErrorAlias()).build();
                }

                User user = userDAO.getById(oldUserOid, userToken.getUser().getCompany());
                user.setName(name);
                user.setSurname(surname);
                user.setCode(code);
                user.setMobilePhone(mobilePhone);
                user.setInternalPhoneCode(internalPhoneCode);
                user.setEmail(email);
                user.setPassword(user.getPassword());
                user.setCompany(company);

                userDAO.update(user);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), user.getName() + "' Adlı kullanıcı güncellendi. ", new DateTime());

                return Response.ok(user).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> userData) {
        String token = userData.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasPermission(userToken.getUser(), PermissionEnum.USER_DELETE)) {
                String oldUserOid = userData.get("oid");
                User user = userDAO.getById(oldUserOid, userToken.getUser().getCompany());

                userDAO.delete(user);

                authUtil.setSystemActivity(userToken.getUser(), userToken.getUser().getCompany(), user.getName() + "' Adlı kullanıcı silindi. ", new DateTime());

                return Response.ok(user).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_NOTLOGIN.getErrorAlias()).build();
        }
    }

}
