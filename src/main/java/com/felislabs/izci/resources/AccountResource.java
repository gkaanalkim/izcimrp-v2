package com.felislabs.izci.resources;

import com.felislabs.izci.auth.AuthUtil;
import com.felislabs.izci.dao.dao.AccountDAO;
import com.felislabs.izci.enums.AccountingPermissionEnum;
import com.felislabs.izci.enums.ErrorMessage;
import com.felislabs.izci.representation.entities.Account;
import com.felislabs.izci.representation.entities.Company;
import com.felislabs.izci.representation.entities.User;
import com.felislabs.izci.representation.entities.UserToken;
import io.dropwizard.hibernate.UnitOfWork;
import org.joda.time.DateTime;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;

/**
 * Created by gunerkaanalkim on 13/03/2017.
 */
@Path("account")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {
    AuthUtil authUtil;
    AccountDAO accountDAO;

    public AccountResource(AuthUtil authUtil, AccountDAO accountDAO) {
        this.authUtil = authUtil;
        this.accountDAO = accountDAO;
    }

    @POST
    @Path("create")
    @UnitOfWork
    public Response create(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.ACCOUNT_CREATE)) {
                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");
                String currency = data.get("currency");
                String balance = data.get("balance");
                String type = data.get("type");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Account account = new Account();
                account.setCompany(company);
                account.setUser(user);
                account.setUpdateDate(new DateTime());
                account.setName(name);
                account.setCode(code);
                account.setDescription(description);
                account.setCurrency(currency);
                account.setBalance(Double.valueOf(balance));
                account.setType(type);

                accountDAO.create(account);

                return Response.ok().entity(account).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("read")
    @UnitOfWork
    public Response read(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_READ)) {
                List<Account> accountList = accountDAO.getAll(userToken.getUser().getCompany());

                return Response.ok().entity(accountList).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @POST
    @Path("readDetail")
    @UnitOfWork
    public Response readDetail(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_READ)) {
                String oid = data.get("oid");

                Account account = accountDAO.getByOid(oid, userToken.getUser().getCompany());

                return Response.ok().entity(account).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @PUT
    @Path("update")
    @UnitOfWork
    public Response update(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_UPDATE)) {
                String oid = data.get("oid");

                String name = data.get("name");
                String code = data.get("code");
                String description = data.get("description");
                String currency = data.get("currency");
                String balance = data.get("balance");
                String type = data.get("type");

                Company company = userToken.getUser().getCompany();
                User user = userToken.getUser();

                Account account = accountDAO.getByOid(oid,company);
                account.setCompany(company);
                account.setUser(user);
                account.setUpdateDate(new DateTime());
                account.setName(name);
                account.setCode(code);
                account.setDescription(description);
                account.setCurrency(currency);
                account.setBalance(Double.valueOf(balance));
                account.setType(type);

                accountDAO.update(account);

                return Response.ok().entity(account).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }

    @DELETE
    @UnitOfWork
    public Response delete(Map<String, String> data) {
        String token = data.get("crmt");

        UserToken userToken = authUtil.isValidToken(token);

        if (userToken != null) {
            if (authUtil.hasAccountantPermission(userToken.getUser(), AccountingPermissionEnum.BRAND_DELETE)) {
                String oid = data.get("oid");

                Company company = userToken.getUser().getCompany();

                Account account = accountDAO.getByOid(oid,company);
                account.setDeleteStatus(true);

                accountDAO.update(account);

                return Response.ok().entity(account).build();
            } else {
                return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
            }
        } else {
            return Response.serverError().entity(ErrorMessage.TR_UNAUTHORIZED.getErrorAlias()).build();
        }
    }
}
