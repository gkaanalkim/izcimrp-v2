package com.felislabs.izci.util;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;

import javax.ws.rs.core.Response;
import java.io.*;
import java.util.List;

/**
 * Created by gunerkaanalkim on 28/08/15.
 */
public class FileUtil {

    public Response saveFile(String filePath, String fileName, String fileResponseName) {
        File file = new File(filePath + fileName);
        Response.ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition", "attachment; filename=\"" + fileResponseName + "\"");

        return response.build();
    }

    public void createHeaderRowForExcel(List<String> cellList, HSSFRow row, CellStyle cellStyle) {

        for (Integer i = 0; i < cellList.size(); i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(cellList.get(i));
            cell.setCellStyle(cellStyle);
        }
    }

    public void createHeaderRowForExcel(List<String> cellList, HSSFRow row, CellStyle cellStyle, Integer startIndex) {

        for (Integer i = 0; i < cellList.size(); i++) {
            Cell cell = row.createCell(startIndex++);
            cell.setCellValue(cellList.get(i));
            cell.setCellStyle(cellStyle);
        }
    }

    // save uploaded file to new location
    public static void writeToFile(InputStream uploadedInputStream, String uploadedFileLocation) {
        try {
            OutputStream out = new FileOutputStream(new File(uploadedFileLocation));
            int read = 0;
            byte[] bytes = new byte[1024];

            out = new FileOutputStream(new File(uploadedFileLocation));
            while ((read = uploadedInputStream.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
